package sare.ModelToText.Generator.services;

import org.eclipse.emf.common.util.EList;
import org.eclipse.uml2.uml.Association;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Comment;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Generalization;
import org.eclipse.uml2.uml.Operation;
import org.eclipse.uml2.uml.Parameter;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.Stereotype;
import org.eclipse.uml2.uml.internal.impl.EnumerationLiteralImpl;
import org.eclipse.uml2.uml.internal.impl.PropertyImpl;

public class UMLServices {
  
 
//para EB
    
public String toPath(String packageName){
    return packageName.trim().replaceAll("\\.","/");
}
public String getInstanceType(String etype){
    
    if (etype.equals("ELong")){
         return "long";
    }
    if (etype.equals("EInt")){
        return "int";
    }
    if (etype.equals("Integer")){
        return "int";
    }
    if (etype.equals("EFloat")){
        return "float";
    }
    if (etype.equals("EDouble")){
        return "double";
    }
    if (etype.equals("Boolean")){
        return "boolean";
    }
    if (etype.equals("EChar")){
        return "char";
    }
    
    return etype;
 
}

public boolean isPrimitive(String etype){
    
    if (etype.equals("ELong")||etype.equals("EInt")||etype.equals("EFloat")||etype.equals("EDouble")||etype.equals("Boolean")||etype.equals("Integer")||etype.equals("EFloat")||etype.equals("String")){
        return true;
    }
    else{
        return false;
    }
}

public boolean hasType(Element elt){
    
    if(elt.eClass().getName().equals("Class")){
        return true;
    }
    if(elt.eClass().getName().equals("PrimitiveType")){
        return true;
    }
    return false;
}
/*
 *Return true if the parameter is a element's comment 
 *
 * */    
public boolean hasComment(Element elt,String comment){
    String name="";
    EList<Comment> commentFound = elt.getOwnedComments();
    for (Comment nodo: commentFound){
       name =nodo.getBody();
        if (comment.equals(name)){
            return true;
        }
    }
    return false;
} 
public String getParameterSet(Element elt){
    Operation op=(Operation)elt;
    EList<Parameter> parameros =op.getOwnedParameters();
    String name=op.getName();
    String tipo="";
    for (Parameter p:parameros){
        //if (p.getName().equals(name.substring(4,name.length()))){
            //tipo=p.getType().getName();
            tipo+=p.getName()+"   "+name.substring(4,name.length());
       // }
    }
    return tipo;
}

/*
 * Return the  method unique parameter multiplicity 
 * 
 * */
public Integer getParameterMultiplicity(Element elt){
    EList <Element> paramenters=elt.getOwnedElements();
    if (paramenters.size()==0){
        return 0;
    }
    else{
        Parameter p= (Parameter)paramenters.get(0);
        return  p.getUpper();
    }
}


public boolean  hasStereotype(Element elt, String stereotype)  {
    String name="";
    EList<Stereotype> stereotypeFound = elt.getAppliedStereotypes();
    for (Stereotype nodo: stereotypeFound){
       name =nodo.getName();
        if (stereotype.equals(name)){
            return true;
        }
    }
   return  false;    
} 

public String getStereotype(Element elt,String property)  {
    String strategy="";
    EList<Stereotype> stereotypeFound = elt.getAppliedStereotypes();
    EnumerationLiteralImpl object=null;
    for(Stereotype estereotype:stereotypeFound){
        if(estereotype.getName().equals("javax.persistence.Inheritance")){
         object=(EnumerationLiteralImpl)elt.getValue(estereotype, property);
         strategy=object.getLabel();
        }
    }
    
   return  strategy;    
}  

public String getAnotations(Element elt){
    String anotation="";
    String prefix="";
    EList<Stereotype> stereotypeFound = elt.getAppliedStereotypes();
    for (Stereotype nodo: stereotypeFound){
        String aux=splitAnotation(nodo.getName());
        prefix="@"+aux+" ";
        if (aux.equals("OneToMany")){
            anotation=getAttributeData(elt);
            anotation=getMappedbyOneToMany(elt,elt.getNearestPackage(),anotation,prefix);
        }else{
            if (aux.equals("OneToOne") || aux.equals("ManyToMany")){
                PropertyImpl property=(PropertyImpl)elt;
                
                anotation=getMappedByManyToMany(elt,property.getType().getLabel());
                anotation= prefix+anotation;
            }else{
            anotation +="@"+aux+" ";
            }
        }
     }
    return anotation;
}

/*
 * Usado para obtener la anotacion @OneToMany(mappedBy="x", cascade=CascadeType.ALL)
 * */

public String getMappedbyOneToMany(Element atributo,Element elt,String clases,String prefix){
   String result="";
   Association a;
   int index=clases.indexOf("."); 
   String class1=clases.substring(0,index);
   String class2=clases.substring(index+1,clases.length()-1);
   EList<Element> elem=elt.getOwnedElements();
   for(Element e:elem){
       if(e.getClass().getSimpleName().equals("AssociationImpl")){
           EList <Element> atributosAsociacion=e.allOwnedElements(); 
           int cont=0;
           String nombre="";
           String typeA="";
           String typeB="";
           for(Element as:atributosAsociacion){
               if(as.getClass().getSimpleName().equals("PropertyImpl")){
                   Property p=(Property)as;
                   if (p.getUpper()==1 && p.getLower()==1){
                       nombre=p.getName();
                       typeA=p.getType().getName();
                   }else{
                       typeB=p.getType().getName();
                   }
                   if (p.getType().getName().equals(class1) ){
                        cont++;
                   }
                   if (p.getType().getName().equals(class2) ){
                       cont++;
                  }
                   
               }
           }
           if(class1.equals(class2)){
               if(cont==4){
                   result=prefix+"(mappedBy=\""+nombre+"\", cascade=CascadeType.ALL)";  
                   return result;
               }
         }else{
         if(cont==2 && !typeA.equals(typeB)){
                 result=prefix+"(mappedBy=\""+nombre+"\", cascade=CascadeType.ALL)";
                 return result;
            }
          }
       }
   }
   return result;
}

/*
 * Usado para obtener la anotacion @OneToMany(mappedBy="x", cascade=CascadeType.ALL)
 * */
public String getMappedbyOTM2(Element attribute,Element elt,String class1,String class2){
    String result="";
    EList<Element> elem=elt.getOwnedElements();
    for(Element e:elem){
        if(e.getClass().getSimpleName().equals("AssociationImpl")){
            EList <Element> atributosAsociacion=e.allOwnedElements(); 
            int cont=0;
            String nombre="";
            String typeA="";
            String typeB="";
            for(Element as:atributosAsociacion){
                if(as.getClass().getSimpleName().equals("PropertyImpl")){
                    Property p=(Property)as;
                    if (p.getUpper()==1 && p.getLower()==1){
                        nombre=p.getName();
                        typeA=p.getType().getName();
                    }else{
                        typeB=p.getType().getName();
                    }
                    if (p.getType().getName().equals(class1) ){
                         cont++;
                    }
                    if (p.getType().getName().equals(class2) ){
                        cont++;
                   }
                    
                }
            }
            if(class1.equals(class2)){
                  if(cont==4){
                      result=nombre;  
                      return result;
                  }
            }else{
            if(cont==2 && !typeA.equals(typeB)){
                result=nombre;
                return result;
            }  
            }   
           }
        }
    return result;
}

/*
 * Usado para obtener la anotacion @OneToOne(mappedBy="x")
 * */
public String getMappedbyOTO2(Element attribute,Element elt,String class1,String class2){
    String result="";
    EList<Element> elem=elt.getOwnedElements();
    for(Element e:elem){
        if(e.getClass().getSimpleName().equals("AssociationImpl")){
            EList <Element> atributosAsociacion=e.allOwnedElements(); 
            int cont=0;
            String nombre="";
            String typeA="";
            String typeB="";
            for(Element as:atributosAsociacion){
                if(as.getClass().getSimpleName().equals("PropertyImpl")){
                    Property p=(Property)as;
                    if (p.getType().getName().equals(class1) ){
                         cont++;
                    }
                    if (p.getType().getName().equals(class2) ){
                        cont++;
                        nombre=p.getName();
                   }
                    
                }
            }
            if(class1.equals(class2)){
                      result=nombre;  
                      return result;
            }
            if (cont==2){
                result=nombre;  
                return result;
            }  
           }
        }
    return result;
 }

/*
 * Usado para obtener la anotacion @OneToOne(mappedBy="x")
 * */
public String getMappedByOneToOne(Element attribute,Element elt,String class1,String class2){
    
    String result="";
    EList<Element> elem=elt.getOwnedElements();
    for(Element e:elem){
        if(e.getClass().getSimpleName().equals("AssociationImpl")){
            EList <Element> atributosAsociacion=e.allOwnedElements(); 
            int cont=0;
            String nombre="";
            for(Element as:atributosAsociacion){
                if(as.getClass().getSimpleName().equals("PropertyImpl")){
                    Property p=(Property)as;
                    if (p.getUpper()==1 && p.getLower()==1){
                        nombre=p.getName();
                    }
                    if (p.getType().getName().equals(class1) || p.getType().getName().equals(class2) ){
                        cont++;
                    }
                }
                if (cont==2){    
                    result=nombre;
                }
            }
            }
        }
    return result;
 }
/*
 * Parte un estereotipo para obtener la anotacion que se pondra sobre el elemento.
 * */
public String splitAnotation(String anotation){
    String anotationSplited="";
    char[] secuence=anotation.toCharArray();
    int cont=0;
        for(int i=0;i<anotation.length();i++){
            if(cont==2){
                anotationSplited+=secuence[i];
            }
            if (secuence[i]=='.'){
               cont++; 
            }
            
        }
    return anotationSplited;
}


public String getHierarchy(Element elt){
    String result="";
    EList<Element> oe=elt.getOwnedElements();
    for(Element e:oe){
        if(e.getClass().getSimpleName().equals("GeneralizationImpl")){
         Generalization gen=(Generalization) e;
         result ="extends "+gen.getGeneral().getName();
        }
    }
    return result;
}
   
public boolean isHierarchy(Element elt){
    
    EList<Element> oe=elt.getOwnedElements();
    for(Element e:oe){
        if(e.getClass().getSimpleName().equals("GeneralizationImpl")){
            return true;
         
        }
    }
    return false;
}

/*
*Retorna el nombre de las tipos de atributos que se involucran en la asociacion 
*/
public String getAttributeData(Element elt){
    String data="";
    PropertyImpl property=(PropertyImpl)elt;
    if (property.getType().getClass().getSimpleName().equals("AssociationImpl")){
        data=getTypeAttributes(elt,property.getType().getLabel());
    } 
    return data;
}
/*
*Retorna la clase de un elemento relacionado en una asociacion. 
*/
public Class getAsociationClass(Element elt){
    Class result=null;
    String name=getType2(elt);
    return result;
    
}

public String getType2(Element elt){
    String type="";
    if(elt.getClass().getSimpleName().equals("PropertyImpl")){
        PropertyImpl property=(PropertyImpl)elt;
        if (property.getType().getClass().getSimpleName().equals("PrimitiveTypeImpl")){
           type=property.getType().getName();
        }
        if (property.getType().getClass().getSimpleName().equals("AssociationImpl")){
            type=getTypeAssociation(elt,property.getType().getLabel());
        }
        
    }
    return type;
}
/*
*Retorna la clase de un elemento relacionado en una asociacion. 
*/
public String getTypeAssociation(Element elt,String associationName){
    String type="";
    Class classOne=(Class)elt.getOwner();
    String className=classOne.getLabel();
    EList<Association> associations = classOne.getAssociations();
    for(Association association: associations){
        if (association.getName().equals(associationName)){
           EList<Element> properties= association.getOwnedElements();
           Property first=(Property)properties.get(0);
           Property second=(Property)properties.get(1);
           if(first.getType().getLabel()==second.getType().getLabel()){
               type=first.getType().getName();
           }
           for(Element element: properties){
               Property p=(Property)element;
               if(!p.getType().getLabel().equals(className)){
                   type=p.getType().getLabel();
               }      
           }
        }  
    }
    return type;
}
/*
*Clase usada para obtener el tipo de un atributo de una asociacion 
*/
public String getTypeAttributes(Element elt,String associationName){
    String type="";
    Class classOne=(Class)elt.getOwner();
    EList<Association> associations = classOne.getAssociations();
    for(Association association: associations){
        if (association.getName().equals(associationName)){
           EList<Element> properties= association.getOwnedElements();
           for(Element element: properties){
               Property p=(Property)element;
                   type+=p.getType().getLabel()+".";       
           }
        }  
    }
    return type;
}

/*
 * Usado para obtener la anotacion @ManyToMany(mappedBy="x")
 * */
public String getMappedByManyToMany(Element elt,String associationName){
    String result="";
    Class classOne=(Class)elt.getOwner();
    Property property=(Property)elt;
    Element ownerE=property.getOwner();
    Class ownerC=(Class)ownerE;
    String type=ownerC.getName();
    EList<Association> associations = classOne.getAssociations();
    for(Association association: associations){
        if (association.getName().equals(associationName)){
           EList<Element> properties= association.getOwnedElements();
           Property first=(Property)properties.get(0);
           if (first.getType().getLabel().equals(type)){
               //Property second=(Property)properties.get(1);  
               result+="(mappedBy=\""+first.getName()+"\")";;
           }   
           }
        }  
    return result;
}


// para SBinscriptions

public String getAsosiationsType(Element elt){
    String asosiation="";
    EList<Stereotype> stereotypeFound = elt.getAppliedStereotypes();
    for (Stereotype nodo: stereotypeFound){
        String aux=splitAnotation(nodo.getName());
        if (aux.equals("ManyToOne") || aux.equals("OneToOne") || aux.equals("ManyToMany")){
            asosiation=aux;
        }
    }
    return asosiation;
}


public boolean isPrimitive(Element elt){
    Property property=(Property)elt;
   if (property.getType().getName().equals("Integer") || property.getType().getName().equals("Long") || property.getType().getName().equals("Double" ) || property.getType().getName().equals("Float") || property.getType().getName().equals("Char") || property.getType().getName().equals("String")){
      return true;
    }
    return false;
}

public String getIdAttribute(Element elt){
    String idname="";
    Class clase=(Class)elt;
    EList <Property> properties= clase.getAllAttributes();
    for (Property prop: properties){
        if (hasStereotype(prop,"javax.persistence.Id")){
            idname=prop.getName();
        }
    } 
    return idname;
}

//----- desde model


public String getClassName(Element elt){
    Class clase=(Class)elt;
    String  name=clase.getName().toString();
    return name;
}

public boolean isClass(Element elt){
    boolean name=false;
    if (elt.getClass().getSimpleName().equals("ClassImpl")){
    name=true;        
    }
    return name;
}

public String getPropertyName(Element elt){
    Property property=(Property)elt;
    String  name=property.getName().toString();
    return name;
}

public boolean isProperty(Element elt){
    boolean name=false;
    if (elt.getClass().getSimpleName().equals("PropertyImpl")){
    name=true;        
    }
    return name;
}
public Property getProperty(Element elt){
    Property property=null;
    if (elt.getClass().getSimpleName().equals("PropertyImpl")){
        property=(Property)elt;        
    }
    return property;
}
public Class getClass(Element elt){
    Class clase=null;
    PropertyImpl prop=null;
    String name="";
    String name1="";
    if (elt.getClass().getSimpleName().equals("ClassImpl")){
        clase=(Class)elt;        
    }
    if (elt.getClass().getSimpleName().equals("PropertyImpl")){
        if(hasStereotype(elt,"javax.persistence.OneToMany") || hasStereotype(elt,"javax.persistence.ManyToOne")||hasStereotype(elt,"javax.persistence.OneToOne")||hasStereotype(elt,"javax.persistence.ManyToMany"))
            prop=(PropertyImpl)elt;
            name=getTypeAssociation(elt,prop.getType().getLabel());
    }
    EList<Element> list=elt.getNearestPackage().allOwnedElements();
    for(Element e: list){
        if (e.getClass().getSimpleName().equals("ClassImpl")){
           
            Class c=(Class)e;
            
            if(c.getName().toString().equals(name)){
                
               return c;
            }
        }
    }
    
    return clase;
}

}