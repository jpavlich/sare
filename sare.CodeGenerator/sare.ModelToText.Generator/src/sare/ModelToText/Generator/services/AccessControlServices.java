package sare.ModelToText.Generator.services;

import org.eclipse.emf.common.util.EList;
import acm.Permission;
import acm.Role;

public class AccessControlServices {
    
    
    
public String getId(String elt){
        char[] c=new char[elt.length()];
        c=elt.toCharArray();
        int i=0;
        int j=0;
        String r1="";
        
        while(c[j]!=' '){
               r1+=c[j];
               j++;
        }
        i=0;
        while(c[i]!='@'){
            i++;
        }
        r1=r1.substring(i+1,j);
        return r1;
}
    
public boolean findPermiso(Role rol,String idClase,String idMetodo){
    
        EList<Permission> permisos= rol.getPermissions();
        for(Permission p:permisos){
            if(p.eClass().getName().equals("Class") && getId(p.getElement().toString()).equals(idClase) ){
                EList<Permission> permisosClase=p.getChildPermissions();
                for(Permission p1:permisosClase){
                    if(p1.getElement().eClass().getName().equals("Operation") && getId(p1.getElement().toString()).equals(idMetodo) ){
                       return true;
                    }
                }
            }else{
                return findPermisoR(idClase,p,idMetodo);
            }
        }
        return false;
}
    
public boolean findPermisoR(String idClase,Permission permiso,String idMetodo){
        
    for(Permission p:permiso.getChildPermissions()){
            
            if(p.getElement().eClass().getName().equals("Class") && getId(p.getElement().toString()).equals(idClase) ){
                EList<Permission> permisosClase=p.getChildPermissions();
                
                for(Permission p1:permisosClase){
                    if(p1.getElement().eClass().getName().equals("Operation") && getId(p1.getElement().toString()).equals(idMetodo) ){
                        return true;
                    }
                }
               
             }else{
                 findPermisoR(idClase,p,idMetodo);
             }
        }
        return false;
}
    
}
