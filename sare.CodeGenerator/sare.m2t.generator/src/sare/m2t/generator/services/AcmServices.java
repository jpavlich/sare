package sare.m2t.generator.services;

import java.lang.reflect.Method;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.uml2.uml.*;

import acm.AccessControlModel;
import acm.Permission;
import acm.Role;
import acm.Subject;
import acm.impl.AccessControlModelImpl;
import acm.impl.RoleImpl;
import org.eclipse.uml2.uml.Operation;
import org.eclipse.uml2.uml.Class;

public class AcmServices {

/*
 * Return the xmi id of the current element
 * 
 * */   
   
public String getId(Element elt){

    char[] c=new char[elt.toString().length()];
    c=elt.toString().toCharArray();
    int i=0;
    int j=0;
    String r1="";
    
    while(c[j]!=' '){
           r1+=c[j];
           j++;
    }
    i=0;
    while(c[i]!='@'){
        i++;
    }
    r1=r1.substring(i+1,j);
    return r1;
}

public String getPermissionString (Element elt1,Element elt2){
    String permission="";
    if(elt2.getModel().getOwner().getClass().equals(AccessControlModelImpl.class)){
    AccessControlModel model = (AccessControlModel)elt2.getModel().getOwner();
    Operation eMethod= (Operation)elt1; 
    Class eClass=(Class)elt2;
    EList<Subject> roles=model.getElements();
    String idClass=getId(eClass);
    String idMethod=getId(eMethod);
    for(Subject subject: roles){
        if (subject.getClass().equals(RoleImpl.class)){
            Role role= (Role)subject;
            if(findPermiso(role,idClass, idMethod)){
                permission=permission+','+'\''+role.getName()+'_'+eMethod.getName()+'\'';
            }
        }
    }
    permission="@Restrict(\"#{s:hasRole("+permission.substring(1)+")}\")";
    }
    return permission;
}

/*
 * Return true if the current  role  has a permission over the current method
 * 
 * */
public boolean findPermiso(Role rol,String idClase,String idMetodo){

    EList<Permission> permisos= rol.getPermissions();
    for(Permission p:permisos){
        if(p.eClass().getName().equals("Class") && getId(p.getElement()).equals(idClase) ){
            EList<Permission> permisosClase=p.getChildPermissions();
            for(Permission p1:permisosClase){
                if(p1.getElement().eClass().getName().equals("Operation") && getId(p1.getElement()).equals(idMetodo) ){
                   return true;
                }
            }
        }else{
            return findPermisoR(idClase,p,idMetodo);
        }
    }
    return false;
}

/*
 * Return true if the current  role  has a permission over the current method. this method is invoked 
 * by findPermiso recursively 
 * 
 * */

public boolean findPermisoR(String idClase,Permission permiso,String idMetodo){
    
for(Permission p:permiso.getChildPermissions()){
        
        if(p.getElement().eClass().getName().equals("Class") && getId(p.getElement()).equals(idClase) ){
            EList<Permission> permisosClase=p.getChildPermissions();
            
            for(Permission p1:permisosClase){
                if(p1.getElement().eClass().getName().equals("Operation") && getId(p1.getElement()).equals(idMetodo) ){
                    return true;
                }
            }
           
         }else{
             findPermisoR(idClase,p,idMetodo);
         }
    }
    return false;
}



}
