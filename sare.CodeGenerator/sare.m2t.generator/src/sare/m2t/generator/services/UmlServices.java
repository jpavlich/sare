package sare.m2t.generator.services;

import org.eclipse.emf.common.util.EList;
import org.eclipse.uml2.uml.Association;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Comment;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Generalization;
import org.eclipse.uml2.uml.Parameter;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.Stereotype;
import org.eclipse.uml2.uml.internal.impl.ParameterImpl;
import org.eclipse.uml2.uml.internal.impl.PropertyImpl;

public class UmlServices {
    /*
     * Return the package restructure from the package name received
     * 
     * */
    
    public String toPath(String packageName){
        return packageName.trim().replaceAll("\\.","/");
    }
    
    /*
     *Return true if the parameter is a element's comment 
     *
     * */    
    public boolean hasComment(Element elt,String comment){
        String name="";
        EList<Comment> commentFound = elt.getOwnedComments();
        for (Comment nodo: commentFound){
           name =nodo.getBody();
            if (comment.equals(name)){
                return true;
            }
        }
        return false;
    } 
    /*
     * Return the class parent class 
     * 
     * */
    public String getHierarchy(Element elt){
        String result="";
        EList<Element> oe=elt.getOwnedElements();
        for(Element e:oe){
            if(e.getClass().getSimpleName().equals("GeneralizationImpl")){
             Generalization gen=(Generalization) e;
             result ="extends "+gen.getGeneral().getName();
            }
        }
        return result;
    }
   
    /*
     * Return true if the class is a generalization 
     * 
     * */
    public boolean isHierarchy(Element elt){
        
        EList<Element> oe=elt.getOwnedElements();
        for(Element e:oe){
            if(e.getClass().getSimpleName().equals("GeneralizationImpl")){
                return true;
             
            }
        }
        return false;
    }
    /*
     * Return the attribute type, either if the attribute is a primitive type or part of an 
     * association 
     * */
    
    public String getType2(Element elt){
        String type="";
        if(elt.getClass().getSimpleName().equals("PropertyImpl")){
            PropertyImpl property=(PropertyImpl)elt;
            if (property.getType().getClass().getSimpleName().equals("PrimitiveTypeImpl")){
               type=property.getType().getName();
            }
            if (property.getType().getClass().getSimpleName().equals("AssociationImpl")){
                type=getTypeAssociation(elt,property.getType().getLabel());
            }
            if (property.getType().getClass().getSimpleName().equals("ClassImpl")){
                type=property.getType().getName();
            }
            
        }
        if(elt.getClass().getSimpleName().equals("ParameterImpl")){
            ParameterImpl p=(ParameterImpl)elt;
            type=p.getType().getLabel();
        }
        
        return type;
    }
    /*
     * Return the attribute type. This method is used if the attribute is part of an 
     * association 
     * */
    public String getTypeAssociation(Element elt,String associationName){
        String type="";
        Class classOne=(Class)elt.getOwner();
        String className=classOne.getLabel();
        EList<Association> associations = classOne.getAssociations();
        for(Association association: associations){
            if (association.getName().equals(associationName)){
               EList<Element> properties= association.getOwnedElements();
               Property first=(Property)properties.get(0);
               Property second=(Property)properties.get(1);
               if(first.getType().getLabel()==second.getType().getLabel()){
                   type=first.getType().getName();
               }
               for(Element element: properties){
                   Property p=(Property)element;
                   if(!p.getType().getLabel().equals(className)){
                       type=p.getType().getLabel();
                   }      
               }
            }  
        }
        return type;
    }
   /*
    * Return true if the element has a defined type. 
    * 
    * */ 
    public boolean hasType(Element elt){
        if (elt!=null){
        return true;
        }
        return false;
    }
    /*
     * Return the  method unique parameter multiplicity 
     * 
     * */
    public Integer getParameterMultiplicity(Element elt){
        EList <Element> paramenters=elt.getOwnedElements();
        if (paramenters.size()==0){
            return 0;
        }
        else{
            Parameter p= (Parameter)paramenters.get(0);
            return  p.getUpper();
        }
    }
    /*
     * Return the comment that has a a body the relation type passed as a parameter 
     * 
     * */
    public Comment getComment(Element elt,String comment){
       EList <Comment> comments= elt.getOwnedComments();
       for(Comment c: comments){
           if (c.getBody().equals(comment)){
               return c;
           }
       }
       return null; 
    }
    public Comment getParamComment(Element elt,String comment){
        EList <Comment> comments= elt.getOwnedComments();
        for(Comment c: comments){
            if (c.getBody().contains(comment)){
                return c;
            }
        }
        return null; 
     }

    /*
     * Build the attributes values of an annotation
     * 
     * */
    public String builAnotationAttributes(Element elt){
       EList <Comment> comments= elt.getOwnedComments();
       String attributes="";
       for(Comment c: comments){
           attributes+=c.getBody()+",";
       }
       attributes=attributes.substring(0,attributes.length()-1);
       return "("+attributes+")";
    }
    /*
     * Return the association type
     * 
     * */
    public String getAsosiationsType(Element elt){
        String asosiation="";
        EList<Comment> commentFound = elt.getOwnedComments();
        for (Comment nodo: commentFound){
            String aux=nodo.getBody();
            if (aux.equals("ManyToOne") || aux.equals("OneToOne") || aux.equals("ManyToMany")){
                asosiation=aux;
            }
        }
        return asosiation;
    }
    /*
     * Return true if the parameter type is a primitive type
     * 
     * */
    public boolean isPrimitive(Element elt){
        Property property=(Property)elt;
       if (property.getType().getName().equals("Integer") || property.getType().getName().equals("Long") || property.getType().getName().equals("Double" ) || property.getType().getName().equals("Float") || property.getType().getName().equals("Char") || property.getType().getName().equals("String")){
          return true;
        }
        return false;
    }
    /*
     * Return the name of the class Id attribute 
     * 
     * */
    public String getIdAttribute(Element elt){
        String idname="";
        Class clase=(Class)elt;
        EList <Property> properties= clase.getAllAttributes();
        for (Property prop: properties){
            if (hasComment(prop,"Id")){
                idname=prop.getName();
            }
        } 
        return idname;
    }
    /*
     * Return the current classs name 
     * 
     * */
    public String getClassName(Element elt){
        Class clase=(Class)elt;
        String  name=clase.getName().toString();
        return name;
    }
    /*
     * Return true if the current element is a typed as class 
     * 
     * */
    public boolean isClass(Element elt){
        boolean name=false;
        if (elt.getClass().getSimpleName().equals("ClassImpl")){
        name=true;        
        }
        return name;
    }
    /*
     * Return the current property name
     * 
     * */
    public String getPropertyName(Element elt){
        Property property=(Property)elt;
        String  name=property.getName().toString();
        return name;
    }
    /*
     * Return true if the current element is a typed as Property
     * 
     * */
    public boolean isProperty(Element elt){
        boolean name=false;
        if (elt.getClass().getSimpleName().equals("PropertyImpl")){
        name=true;        
        }
        return name;
    }
    /*
     * Cast the current element to a Property
     * 
     * */
    public Property getProperty(Element elt){
        Property property=null;
        if (elt.getClass().getSimpleName().equals("PropertyImpl")){
            property=(Property)elt;        
        }
        return property;
    }
    /*
     * Return the type of the association OneToMany mappedBy attribute.
     * 
     * */
    public String getMappedbyOTM2(Element attribute,Element elt,String class1,String class2){
        String result="";
        EList<Element> elem=elt.getOwnedElements();
        for(Element e:elem){
            if(e.getClass().getSimpleName().equals("AssociationImpl")){
                EList <Element> atributosAsociacion=e.allOwnedElements(); 
                int cont=0;
                String nombre="";
                String typeA="";
                String typeB="";
                for(Element as:atributosAsociacion){
                    if(as.getClass().getSimpleName().equals("PropertyImpl")){
                        Property p=(Property)as;
                        if (p.getUpper()==1 && p.getLower()==1){
                            nombre=p.getName();
                            typeA=p.getType().getName();
                        }else{
                            typeB=p.getType().getName();
                        }
                        if (p.getType().getName().equals(class1) ){
                             cont++;
                        }
                        if (p.getType().getName().equals(class2) ){
                            cont++;
                       }
                        
                    }
                }
                if(class1.equals(class2)){
                      if(cont==4){
                          result=nombre;  
                          return result;
                      }
                }else{
                if(cont==2 && !typeA.equals(typeB)){
                    result=nombre;
                    return result;
                }  
                }   
               }
            }
        return result;
    }

    /*
     * Return the type of the association OneToMany mappedBy attribute.
     * 
     * */
    public String getMappedbyOTO2(Element attribute,Element elt,String class1,String class2){
        String result="";
        EList<Element> elem=elt.getOwnedElements();
        for(Element e:elem){
            if(e.getClass().getSimpleName().equals("AssociationImpl")){
                EList <Element> atributosAsociacion=e.allOwnedElements(); 
                int cont=0;
                String nombre="";
                String typeA="";
                String typeB="";
                for(Element as:atributosAsociacion){
                    if(as.getClass().getSimpleName().equals("PropertyImpl")){
                        Property p=(Property)as;
                        if (p.getType().getName().equals(class1) ){
                             cont++;
                        }
                        if (p.getType().getName().equals(class2) ){
                            cont++;
                            nombre=p.getName();
                       }
                        
                    }
                }
                if(class1.equals(class2)){
                          result=nombre;  
                          return result;
                }
                if (cont==2){
                    result=nombre;  
                    return result;
                }  
               }
            }
        return result;
     }
    
    public Class getClass(Element elt){
        Class clase=null;
        PropertyImpl prop=null;
        String name="";
        String name1="";
        if (elt.getClass().getSimpleName().equals("ClassImpl")){
            clase=(Class)elt;        
        }
        if (elt.getClass().getSimpleName().equals("PropertyImpl")){
            if(hasComment(elt,"OneToMany") || hasComment(elt,"ManyToOne")||hasComment(elt,"OneToOne")||hasComment(elt,"ManyToMany"))
                prop=(PropertyImpl)elt;
                name=getTypeAssociation(elt,prop.getType().getLabel());
        }
        EList<Element> list=elt.getNearestPackage().allOwnedElements();
        for(Element e: list){
            if (e.getClass().getSimpleName().equals("ClassImpl")){
               
                Class c=(Class)e;
                
                if(c.getName().toString().equals(name)){
                    
                   return c;
                }
            }
        }
        return clase;
    }
}
