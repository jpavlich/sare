/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package acm;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Role</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link acm.Role#getParentRoles <em>Parent Roles</em>}</li>
 *   <li>{@link acm.Role#getPermissions <em>Permissions</em>}</li>
 * </ul>
 * </p>
 *
 * @see acm.AcmPackage#getRole()
 * @model
 * @generated
 */
public interface Role extends Subject {
	/**
	 * Returns the value of the '<em><b>Parent Roles</b></em>' reference list.
	 * The list contents are of type {@link acm.Role}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parent Roles</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parent Roles</em>' reference list.
	 * @see acm.AcmPackage#getRole_ParentRoles()
	 * @model
	 * @generated
	 */
	EList<Role> getParentRoles();

	/**
	 * Returns the value of the '<em><b>Permissions</b></em>' containment reference list.
	 * The list contents are of type {@link acm.Permission}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Permissions</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Permissions</em>' containment reference list.
	 * @see acm.AcmPackage#getRole_Permissions()
	 * @model containment="true"
	 * @generated
	 */
	EList<Permission> getPermissions();

} // Role
