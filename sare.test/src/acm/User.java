/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package acm;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>User</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link acm.User#getRoles <em>Roles</em>}</li>
 * </ul>
 * </p>
 *
 * @see acm.AcmPackage#getUser()
 * @model
 * @generated
 */
public interface User extends Subject {
	/**
	 * Returns the value of the '<em><b>Roles</b></em>' reference list.
	 * The list contents are of type {@link acm.Role}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Roles</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Roles</em>' reference list.
	 * @see acm.AcmPackage#getUser_Roles()
	 * @model
	 * @generated
	 */
	EList<Role> getRoles();

} // User
