/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package acm.impl;

import acm.AccessControlModel;
import acm.AcmPackage;
import acm.Subject;

import java.util.Collection;
import java.util.Map;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.EObjectValidator;
import org.eclipse.emf.ecore.util.InternalEList;

import org.eclipse.uml2.uml.Constraint;
import org.eclipse.uml2.uml.ElementImport;
import org.eclipse.uml2.uml.Enumeration;
import org.eclipse.uml2.uml.Interface;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.Namespace;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.PackageImport;
import org.eclipse.uml2.uml.PackageMerge;
import org.eclipse.uml2.uml.PackageableElement;
import org.eclipse.uml2.uml.ParameterableElement;
import org.eclipse.uml2.uml.PrimitiveType;
import org.eclipse.uml2.uml.Profile;
import org.eclipse.uml2.uml.ProfileApplication;
import org.eclipse.uml2.uml.TemplateBinding;
import org.eclipse.uml2.uml.TemplateSignature;
import org.eclipse.uml2.uml.TemplateableElement;
import org.eclipse.uml2.uml.Type;
import org.eclipse.uml2.uml.UMLPackage;
import org.eclipse.uml2.uml.VisibilityKind;

import org.eclipse.uml2.uml.internal.impl.PackageableElementImpl;

import org.eclipse.uml2.uml.util.UMLValidator;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Access Control Model</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link acm.impl.AccessControlModelImpl#getElementImports <em>Element Import</em>}</li>
 *   <li>{@link acm.impl.AccessControlModelImpl#getPackageImports <em>Package Import</em>}</li>
 *   <li>{@link acm.impl.AccessControlModelImpl#getOwnedRules <em>Owned Rule</em>}</li>
 *   <li>{@link acm.impl.AccessControlModelImpl#getMembers <em>Member</em>}</li>
 *   <li>{@link acm.impl.AccessControlModelImpl#getImportedMembers <em>Imported Member</em>}</li>
 *   <li>{@link acm.impl.AccessControlModelImpl#getOwnedMembers <em>Owned Member</em>}</li>
 *   <li>{@link acm.impl.AccessControlModelImpl#getTemplateBindings <em>Template Binding</em>}</li>
 *   <li>{@link acm.impl.AccessControlModelImpl#getOwnedTemplateSignature <em>Owned Template Signature</em>}</li>
 *   <li>{@link acm.impl.AccessControlModelImpl#getOwnedTypes <em>Owned Type</em>}</li>
 *   <li>{@link acm.impl.AccessControlModelImpl#getPackageMerges <em>Package Merge</em>}</li>
 *   <li>{@link acm.impl.AccessControlModelImpl#getPackagedElements <em>Packaged Element</em>}</li>
 *   <li>{@link acm.impl.AccessControlModelImpl#getNestedPackages <em>Nested Package</em>}</li>
 *   <li>{@link acm.impl.AccessControlModelImpl#getNestingPackage <em>Nesting Package</em>}</li>
 *   <li>{@link acm.impl.AccessControlModelImpl#getProfileApplications <em>Profile Application</em>}</li>
 *   <li>{@link acm.impl.AccessControlModelImpl#getElements <em>Elements</em>}</li>
 *   <li>{@link acm.impl.AccessControlModelImpl#getImports <em>Imports</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class AccessControlModelImpl extends PackageableElementImpl implements AccessControlModel {
	/**
	 * The cached value of the '{@link #getElementImports() <em>Element Import</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getElementImports()
	 * @generated
	 * @ordered
	 */
	protected EList<ElementImport> elementImports;

	/**
	 * The cached value of the '{@link #getPackageImports() <em>Package Import</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPackageImports()
	 * @generated
	 * @ordered
	 */
	protected EList<PackageImport> packageImports;

	/**
	 * The cached value of the '{@link #getOwnedRules() <em>Owned Rule</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOwnedRules()
	 * @generated
	 * @ordered
	 */
	protected EList<Constraint> ownedRules;

	/**
	 * The cached value of the '{@link #getTemplateBindings() <em>Template Binding</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTemplateBindings()
	 * @generated
	 * @ordered
	 */
	protected EList<TemplateBinding> templateBindings;

	/**
	 * The cached value of the '{@link #getOwnedTemplateSignature() <em>Owned Template Signature</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOwnedTemplateSignature()
	 * @generated
	 * @ordered
	 */
	protected TemplateSignature ownedTemplateSignature;

	/**
	 * The cached value of the '{@link #getPackageMerges() <em>Package Merge</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPackageMerges()
	 * @generated
	 * @ordered
	 */
	protected EList<PackageMerge> packageMerges;

	/**
	 * The cached value of the '{@link #getPackagedElements() <em>Packaged Element</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPackagedElements()
	 * @generated
	 * @ordered
	 */
	protected EList<PackageableElement> packagedElements;

	/**
	 * The cached value of the '{@link #getProfileApplications() <em>Profile Application</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProfileApplications()
	 * @generated
	 * @ordered
	 */
	protected EList<ProfileApplication> profileApplications;

	/**
	 * The cached value of the '{@link #getElements() <em>Elements</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getElements()
	 * @generated
	 * @ordered
	 */
	protected EList<Subject> elements;

	/**
	 * The cached value of the '{@link #getImports() <em>Imports</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getImports()
	 * @generated
	 * @ordered
	 */
	protected EList<EObject> imports;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AccessControlModelImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AcmPackage.Literals.ACCESS_CONTROL_MODEL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ElementImport> getElementImports() {
		if (elementImports == null) {
			elementImports = new EObjectContainmentWithInverseEList.Resolving<ElementImport>(ElementImport.class, this, AcmPackage.ACCESS_CONTROL_MODEL__ELEMENT_IMPORT, UMLPackage.ELEMENT_IMPORT__IMPORTING_NAMESPACE);
		}
		return elementImports;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PackageImport> getPackageImports() {
		if (packageImports == null) {
			packageImports = new EObjectContainmentWithInverseEList.Resolving<PackageImport>(PackageImport.class, this, AcmPackage.ACCESS_CONTROL_MODEL__PACKAGE_IMPORT, UMLPackage.PACKAGE_IMPORT__IMPORTING_NAMESPACE);
		}
		return packageImports;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Constraint> getOwnedRules() {
		if (ownedRules == null) {
			ownedRules = new EObjectContainmentWithInverseEList.Resolving<Constraint>(Constraint.class, this, AcmPackage.ACCESS_CONTROL_MODEL__OWNED_RULE, UMLPackage.CONSTRAINT__CONTEXT);
		}
		return ownedRules;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<NamedElement> getMembers() {
		// TODO: implement this method to return the 'Member' reference list
		// Ensure that you remove @generated or mark it @generated NOT
		// The list is expected to implement org.eclipse.emf.ecore.util.InternalEList and org.eclipse.emf.ecore.EStructuralFeature.Setting
		// so it's likely that an appropriate subclass of org.eclipse.emf.ecore.util.EcoreEList should be used.
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PackageableElement> getImportedMembers() {
		// TODO: implement this method to return the 'Imported Member' reference list
		// Ensure that you remove @generated or mark it @generated NOT
		// The list is expected to implement org.eclipse.emf.ecore.util.InternalEList and org.eclipse.emf.ecore.EStructuralFeature.Setting
		// so it's likely that an appropriate subclass of org.eclipse.emf.ecore.util.EcoreEList should be used.
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<NamedElement> getOwnedMembers() {
		// TODO: implement this method to return the 'Owned Member' reference list
		// Ensure that you remove @generated or mark it @generated NOT
		// The list is expected to implement org.eclipse.emf.ecore.util.InternalEList and org.eclipse.emf.ecore.EStructuralFeature.Setting
		// so it's likely that an appropriate subclass of org.eclipse.emf.ecore.util.EcoreEList should be used.
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TemplateBinding> getTemplateBindings() {
		if (templateBindings == null) {
			templateBindings = new EObjectContainmentWithInverseEList.Resolving<TemplateBinding>(TemplateBinding.class, this, AcmPackage.ACCESS_CONTROL_MODEL__TEMPLATE_BINDING, UMLPackage.TEMPLATE_BINDING__BOUND_ELEMENT);
		}
		return templateBindings;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TemplateSignature getOwnedTemplateSignature() {
		if (ownedTemplateSignature != null && ownedTemplateSignature.eIsProxy()) {
			InternalEObject oldOwnedTemplateSignature = (InternalEObject)ownedTemplateSignature;
			ownedTemplateSignature = (TemplateSignature)eResolveProxy(oldOwnedTemplateSignature);
			if (ownedTemplateSignature != oldOwnedTemplateSignature) {
				InternalEObject newOwnedTemplateSignature = (InternalEObject)ownedTemplateSignature;
				NotificationChain msgs =  oldOwnedTemplateSignature.eInverseRemove(this, UMLPackage.TEMPLATE_SIGNATURE__TEMPLATE, TemplateSignature.class, null);
				if (newOwnedTemplateSignature.eInternalContainer() == null) {
					msgs =  newOwnedTemplateSignature.eInverseAdd(this, UMLPackage.TEMPLATE_SIGNATURE__TEMPLATE, TemplateSignature.class, msgs);
				}
				if (msgs != null) msgs.dispatch();
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, AcmPackage.ACCESS_CONTROL_MODEL__OWNED_TEMPLATE_SIGNATURE, oldOwnedTemplateSignature, ownedTemplateSignature));
			}
		}
		return ownedTemplateSignature;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TemplateSignature basicGetOwnedTemplateSignature() {
		return ownedTemplateSignature;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetOwnedTemplateSignature(TemplateSignature newOwnedTemplateSignature, NotificationChain msgs) {
		TemplateSignature oldOwnedTemplateSignature = ownedTemplateSignature;
		ownedTemplateSignature = newOwnedTemplateSignature;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AcmPackage.ACCESS_CONTROL_MODEL__OWNED_TEMPLATE_SIGNATURE, oldOwnedTemplateSignature, newOwnedTemplateSignature);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOwnedTemplateSignature(TemplateSignature newOwnedTemplateSignature) {
		if (newOwnedTemplateSignature != ownedTemplateSignature) {
			NotificationChain msgs = null;
			if (ownedTemplateSignature != null)
				msgs = ((InternalEObject)ownedTemplateSignature).eInverseRemove(this, UMLPackage.TEMPLATE_SIGNATURE__TEMPLATE, TemplateSignature.class, msgs);
			if (newOwnedTemplateSignature != null)
				msgs = ((InternalEObject)newOwnedTemplateSignature).eInverseAdd(this, UMLPackage.TEMPLATE_SIGNATURE__TEMPLATE, TemplateSignature.class, msgs);
			msgs = basicSetOwnedTemplateSignature(newOwnedTemplateSignature, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AcmPackage.ACCESS_CONTROL_MODEL__OWNED_TEMPLATE_SIGNATURE, newOwnedTemplateSignature, newOwnedTemplateSignature));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Type> getOwnedTypes() {
		// TODO: implement this method to return the 'Owned Type' reference list
		// Ensure that you remove @generated or mark it @generated NOT
		// The list is expected to implement org.eclipse.emf.ecore.util.InternalEList and org.eclipse.emf.ecore.EStructuralFeature.Setting
		// so it's likely that an appropriate subclass of org.eclipse.emf.ecore.util.EcoreEList should be used.
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PackageMerge> getPackageMerges() {
		if (packageMerges == null) {
			packageMerges = new EObjectContainmentWithInverseEList.Resolving<PackageMerge>(PackageMerge.class, this, AcmPackage.ACCESS_CONTROL_MODEL__PACKAGE_MERGE, UMLPackage.PACKAGE_MERGE__RECEIVING_PACKAGE);
		}
		return packageMerges;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PackageableElement> getPackagedElements() {
		if (packagedElements == null) {
			packagedElements = new EObjectContainmentEList.Resolving<PackageableElement>(PackageableElement.class, this, AcmPackage.ACCESS_CONTROL_MODEL__PACKAGED_ELEMENT);
		}
		return packagedElements;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<org.eclipse.uml2.uml.Package> getNestedPackages() {
		// TODO: implement this method to return the 'Nested Package' reference list
		// Ensure that you remove @generated or mark it @generated NOT
		// The list is expected to implement org.eclipse.emf.ecore.util.InternalEList and org.eclipse.emf.ecore.EStructuralFeature.Setting
		// so it's likely that an appropriate subclass of org.eclipse.emf.ecore.util.EcoreEList should be used.
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public org.eclipse.uml2.uml.Package getNestingPackage() {
		org.eclipse.uml2.uml.Package nestingPackage = basicGetNestingPackage();
		return nestingPackage != null && nestingPackage.eIsProxy() ? (org.eclipse.uml2.uml.Package)eResolveProxy((InternalEObject)nestingPackage) : nestingPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public org.eclipse.uml2.uml.Package basicGetNestingPackage() {
		// TODO: implement this method to return the 'Nesting Package' reference
		// -> do not perform proxy resolution
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNestingPackage(org.eclipse.uml2.uml.Package newNestingPackage) {
		// TODO: implement this method to set the 'Nesting Package' reference
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ProfileApplication> getProfileApplications() {
		if (profileApplications == null) {
			profileApplications = new EObjectContainmentWithInverseEList.Resolving<ProfileApplication>(ProfileApplication.class, this, AcmPackage.ACCESS_CONTROL_MODEL__PROFILE_APPLICATION, UMLPackage.PROFILE_APPLICATION__APPLYING_PACKAGE);
		}
		return profileApplications;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Subject> getElements() {
		if (elements == null) {
			elements = new EObjectContainmentEList<Subject>(Subject.class, this, AcmPackage.ACCESS_CONTROL_MODEL__ELEMENTS);
		}
		return elements;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EObject> getImports() {
		if (imports == null) {
			imports = new EObjectResolvingEList<EObject>(EObject.class, this, AcmPackage.ACCESS_CONTROL_MODEL__IMPORTS);
		}
		return imports;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateElementsPublicOrPrivate(DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		if (false) {
			if (diagnostics != null) {
				diagnostics.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 UMLValidator.DIAGNOSTIC_SOURCE,
						 UMLValidator.PACKAGE__ELEMENTS_PUBLIC_OR_PRIVATE,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "validateElementsPublicOrPrivate", EObjectValidator.getObjectLabel(this, context) }),
						 new Object [] { this }));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public org.eclipse.uml2.uml.Class createOwnedClass(String name, boolean isAbstract) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Enumeration createOwnedEnumeration(String name) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PrimitiveType createOwnedPrimitiveType(String name) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Interface createOwnedInterface(String name) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isProfileApplied(Profile profile) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EObject> applyProfile(Profile profile) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EObject> unapplyProfile(Profile profile) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Profile> getAppliedProfiles() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Profile> getAllAppliedProfiles() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Profile getAppliedProfile(String qualifiedName) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Profile getAppliedProfile(String qualifiedName, boolean recurse) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ProfileApplication> getAllProfileApplications() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProfileApplication getProfileApplication(Profile profile) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProfileApplication getProfileApplication(Profile profile, boolean recurse) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isModelLibrary() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PackageableElement> visibleMembers() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean makesVisible(NamedElement el) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ParameterableElement> parameterableElements() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isTemplate() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMembersDistinguishable(DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		if (false) {
			if (diagnostics != null) {
				diagnostics.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 UMLValidator.DIAGNOSTIC_SOURCE,
						 UMLValidator.NAMESPACE__MEMBERS_DISTINGUISHABLE,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "validateMembersDistinguishable", EObjectValidator.getObjectLabel(this, context) }),
						 new Object [] { this }));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ElementImport createElementImport(PackageableElement element, VisibilityKind visibility) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PackageImport createPackageImport(org.eclipse.uml2.uml.Package package_, VisibilityKind visibility) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PackageableElement> getImportedElements() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<org.eclipse.uml2.uml.Package> getImportedPackages() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<String> getNamesOfMember(NamedElement element) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean membersAreDistinguishable() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PackageableElement> importMembers(EList<PackageableElement> imps) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PackageableElement> excludeCollisions(EList<PackageableElement> imps) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AcmPackage.ACCESS_CONTROL_MODEL__ELEMENT_IMPORT:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getElementImports()).basicAdd(otherEnd, msgs);
			case AcmPackage.ACCESS_CONTROL_MODEL__PACKAGE_IMPORT:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getPackageImports()).basicAdd(otherEnd, msgs);
			case AcmPackage.ACCESS_CONTROL_MODEL__OWNED_RULE:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getOwnedRules()).basicAdd(otherEnd, msgs);
			case AcmPackage.ACCESS_CONTROL_MODEL__TEMPLATE_BINDING:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getTemplateBindings()).basicAdd(otherEnd, msgs);
			case AcmPackage.ACCESS_CONTROL_MODEL__OWNED_TEMPLATE_SIGNATURE:
				if (ownedTemplateSignature != null)
					msgs = ((InternalEObject)ownedTemplateSignature).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AcmPackage.ACCESS_CONTROL_MODEL__OWNED_TEMPLATE_SIGNATURE, null, msgs);
				return basicSetOwnedTemplateSignature((TemplateSignature)otherEnd, msgs);
			case AcmPackage.ACCESS_CONTROL_MODEL__PACKAGE_MERGE:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getPackageMerges()).basicAdd(otherEnd, msgs);
			case AcmPackage.ACCESS_CONTROL_MODEL__PROFILE_APPLICATION:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getProfileApplications()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AcmPackage.ACCESS_CONTROL_MODEL__ELEMENT_IMPORT:
				return ((InternalEList<?>)getElementImports()).basicRemove(otherEnd, msgs);
			case AcmPackage.ACCESS_CONTROL_MODEL__PACKAGE_IMPORT:
				return ((InternalEList<?>)getPackageImports()).basicRemove(otherEnd, msgs);
			case AcmPackage.ACCESS_CONTROL_MODEL__OWNED_RULE:
				return ((InternalEList<?>)getOwnedRules()).basicRemove(otherEnd, msgs);
			case AcmPackage.ACCESS_CONTROL_MODEL__TEMPLATE_BINDING:
				return ((InternalEList<?>)getTemplateBindings()).basicRemove(otherEnd, msgs);
			case AcmPackage.ACCESS_CONTROL_MODEL__OWNED_TEMPLATE_SIGNATURE:
				return basicSetOwnedTemplateSignature(null, msgs);
			case AcmPackage.ACCESS_CONTROL_MODEL__PACKAGE_MERGE:
				return ((InternalEList<?>)getPackageMerges()).basicRemove(otherEnd, msgs);
			case AcmPackage.ACCESS_CONTROL_MODEL__PACKAGED_ELEMENT:
				return ((InternalEList<?>)getPackagedElements()).basicRemove(otherEnd, msgs);
			case AcmPackage.ACCESS_CONTROL_MODEL__PROFILE_APPLICATION:
				return ((InternalEList<?>)getProfileApplications()).basicRemove(otherEnd, msgs);
			case AcmPackage.ACCESS_CONTROL_MODEL__ELEMENTS:
				return ((InternalEList<?>)getElements()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AcmPackage.ACCESS_CONTROL_MODEL__ELEMENT_IMPORT:
				return getElementImports();
			case AcmPackage.ACCESS_CONTROL_MODEL__PACKAGE_IMPORT:
				return getPackageImports();
			case AcmPackage.ACCESS_CONTROL_MODEL__OWNED_RULE:
				return getOwnedRules();
			case AcmPackage.ACCESS_CONTROL_MODEL__MEMBER:
				return getMembers();
			case AcmPackage.ACCESS_CONTROL_MODEL__IMPORTED_MEMBER:
				return getImportedMembers();
			case AcmPackage.ACCESS_CONTROL_MODEL__OWNED_MEMBER:
				return getOwnedMembers();
			case AcmPackage.ACCESS_CONTROL_MODEL__TEMPLATE_BINDING:
				return getTemplateBindings();
			case AcmPackage.ACCESS_CONTROL_MODEL__OWNED_TEMPLATE_SIGNATURE:
				if (resolve) return getOwnedTemplateSignature();
				return basicGetOwnedTemplateSignature();
			case AcmPackage.ACCESS_CONTROL_MODEL__OWNED_TYPE:
				return getOwnedTypes();
			case AcmPackage.ACCESS_CONTROL_MODEL__PACKAGE_MERGE:
				return getPackageMerges();
			case AcmPackage.ACCESS_CONTROL_MODEL__PACKAGED_ELEMENT:
				return getPackagedElements();
			case AcmPackage.ACCESS_CONTROL_MODEL__NESTED_PACKAGE:
				return getNestedPackages();
			case AcmPackage.ACCESS_CONTROL_MODEL__NESTING_PACKAGE:
				if (resolve) return getNestingPackage();
				return basicGetNestingPackage();
			case AcmPackage.ACCESS_CONTROL_MODEL__PROFILE_APPLICATION:
				return getProfileApplications();
			case AcmPackage.ACCESS_CONTROL_MODEL__ELEMENTS:
				return getElements();
			case AcmPackage.ACCESS_CONTROL_MODEL__IMPORTS:
				return getImports();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AcmPackage.ACCESS_CONTROL_MODEL__ELEMENT_IMPORT:
				getElementImports().clear();
				getElementImports().addAll((Collection<? extends ElementImport>)newValue);
				return;
			case AcmPackage.ACCESS_CONTROL_MODEL__PACKAGE_IMPORT:
				getPackageImports().clear();
				getPackageImports().addAll((Collection<? extends PackageImport>)newValue);
				return;
			case AcmPackage.ACCESS_CONTROL_MODEL__OWNED_RULE:
				getOwnedRules().clear();
				getOwnedRules().addAll((Collection<? extends Constraint>)newValue);
				return;
			case AcmPackage.ACCESS_CONTROL_MODEL__TEMPLATE_BINDING:
				getTemplateBindings().clear();
				getTemplateBindings().addAll((Collection<? extends TemplateBinding>)newValue);
				return;
			case AcmPackage.ACCESS_CONTROL_MODEL__OWNED_TEMPLATE_SIGNATURE:
				setOwnedTemplateSignature((TemplateSignature)newValue);
				return;
			case AcmPackage.ACCESS_CONTROL_MODEL__OWNED_TYPE:
				getOwnedTypes().clear();
				getOwnedTypes().addAll((Collection<? extends Type>)newValue);
				return;
			case AcmPackage.ACCESS_CONTROL_MODEL__PACKAGE_MERGE:
				getPackageMerges().clear();
				getPackageMerges().addAll((Collection<? extends PackageMerge>)newValue);
				return;
			case AcmPackage.ACCESS_CONTROL_MODEL__PACKAGED_ELEMENT:
				getPackagedElements().clear();
				getPackagedElements().addAll((Collection<? extends PackageableElement>)newValue);
				return;
			case AcmPackage.ACCESS_CONTROL_MODEL__NESTED_PACKAGE:
				getNestedPackages().clear();
				getNestedPackages().addAll((Collection<? extends org.eclipse.uml2.uml.Package>)newValue);
				return;
			case AcmPackage.ACCESS_CONTROL_MODEL__NESTING_PACKAGE:
				setNestingPackage((org.eclipse.uml2.uml.Package)newValue);
				return;
			case AcmPackage.ACCESS_CONTROL_MODEL__PROFILE_APPLICATION:
				getProfileApplications().clear();
				getProfileApplications().addAll((Collection<? extends ProfileApplication>)newValue);
				return;
			case AcmPackage.ACCESS_CONTROL_MODEL__ELEMENTS:
				getElements().clear();
				getElements().addAll((Collection<? extends Subject>)newValue);
				return;
			case AcmPackage.ACCESS_CONTROL_MODEL__IMPORTS:
				getImports().clear();
				getImports().addAll((Collection<? extends EObject>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AcmPackage.ACCESS_CONTROL_MODEL__ELEMENT_IMPORT:
				getElementImports().clear();
				return;
			case AcmPackage.ACCESS_CONTROL_MODEL__PACKAGE_IMPORT:
				getPackageImports().clear();
				return;
			case AcmPackage.ACCESS_CONTROL_MODEL__OWNED_RULE:
				getOwnedRules().clear();
				return;
			case AcmPackage.ACCESS_CONTROL_MODEL__TEMPLATE_BINDING:
				getTemplateBindings().clear();
				return;
			case AcmPackage.ACCESS_CONTROL_MODEL__OWNED_TEMPLATE_SIGNATURE:
				setOwnedTemplateSignature((TemplateSignature)null);
				return;
			case AcmPackage.ACCESS_CONTROL_MODEL__OWNED_TYPE:
				getOwnedTypes().clear();
				return;
			case AcmPackage.ACCESS_CONTROL_MODEL__PACKAGE_MERGE:
				getPackageMerges().clear();
				return;
			case AcmPackage.ACCESS_CONTROL_MODEL__PACKAGED_ELEMENT:
				getPackagedElements().clear();
				return;
			case AcmPackage.ACCESS_CONTROL_MODEL__NESTED_PACKAGE:
				getNestedPackages().clear();
				return;
			case AcmPackage.ACCESS_CONTROL_MODEL__NESTING_PACKAGE:
				setNestingPackage((org.eclipse.uml2.uml.Package)null);
				return;
			case AcmPackage.ACCESS_CONTROL_MODEL__PROFILE_APPLICATION:
				getProfileApplications().clear();
				return;
			case AcmPackage.ACCESS_CONTROL_MODEL__ELEMENTS:
				getElements().clear();
				return;
			case AcmPackage.ACCESS_CONTROL_MODEL__IMPORTS:
				getImports().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AcmPackage.ACCESS_CONTROL_MODEL__ELEMENT_IMPORT:
				return elementImports != null && !elementImports.isEmpty();
			case AcmPackage.ACCESS_CONTROL_MODEL__PACKAGE_IMPORT:
				return packageImports != null && !packageImports.isEmpty();
			case AcmPackage.ACCESS_CONTROL_MODEL__OWNED_RULE:
				return ownedRules != null && !ownedRules.isEmpty();
			case AcmPackage.ACCESS_CONTROL_MODEL__MEMBER:
				return !getMembers().isEmpty();
			case AcmPackage.ACCESS_CONTROL_MODEL__IMPORTED_MEMBER:
				return !getImportedMembers().isEmpty();
			case AcmPackage.ACCESS_CONTROL_MODEL__OWNED_MEMBER:
				return !getOwnedMembers().isEmpty();
			case AcmPackage.ACCESS_CONTROL_MODEL__TEMPLATE_BINDING:
				return templateBindings != null && !templateBindings.isEmpty();
			case AcmPackage.ACCESS_CONTROL_MODEL__OWNED_TEMPLATE_SIGNATURE:
				return ownedTemplateSignature != null;
			case AcmPackage.ACCESS_CONTROL_MODEL__OWNED_TYPE:
				return !getOwnedTypes().isEmpty();
			case AcmPackage.ACCESS_CONTROL_MODEL__PACKAGE_MERGE:
				return packageMerges != null && !packageMerges.isEmpty();
			case AcmPackage.ACCESS_CONTROL_MODEL__PACKAGED_ELEMENT:
				return packagedElements != null && !packagedElements.isEmpty();
			case AcmPackage.ACCESS_CONTROL_MODEL__NESTED_PACKAGE:
				return !getNestedPackages().isEmpty();
			case AcmPackage.ACCESS_CONTROL_MODEL__NESTING_PACKAGE:
				return basicGetNestingPackage() != null;
			case AcmPackage.ACCESS_CONTROL_MODEL__PROFILE_APPLICATION:
				return profileApplications != null && !profileApplications.isEmpty();
			case AcmPackage.ACCESS_CONTROL_MODEL__ELEMENTS:
				return elements != null && !elements.isEmpty();
			case AcmPackage.ACCESS_CONTROL_MODEL__IMPORTS:
				return imports != null && !imports.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == Namespace.class) {
			switch (derivedFeatureID) {
				case AcmPackage.ACCESS_CONTROL_MODEL__ELEMENT_IMPORT: return UMLPackage.NAMESPACE__ELEMENT_IMPORT;
				case AcmPackage.ACCESS_CONTROL_MODEL__PACKAGE_IMPORT: return UMLPackage.NAMESPACE__PACKAGE_IMPORT;
				case AcmPackage.ACCESS_CONTROL_MODEL__OWNED_RULE: return UMLPackage.NAMESPACE__OWNED_RULE;
				case AcmPackage.ACCESS_CONTROL_MODEL__MEMBER: return UMLPackage.NAMESPACE__MEMBER;
				case AcmPackage.ACCESS_CONTROL_MODEL__IMPORTED_MEMBER: return UMLPackage.NAMESPACE__IMPORTED_MEMBER;
				case AcmPackage.ACCESS_CONTROL_MODEL__OWNED_MEMBER: return UMLPackage.NAMESPACE__OWNED_MEMBER;
				default: return -1;
			}
		}
		if (baseClass == TemplateableElement.class) {
			switch (derivedFeatureID) {
				case AcmPackage.ACCESS_CONTROL_MODEL__TEMPLATE_BINDING: return UMLPackage.TEMPLATEABLE_ELEMENT__TEMPLATE_BINDING;
				case AcmPackage.ACCESS_CONTROL_MODEL__OWNED_TEMPLATE_SIGNATURE: return UMLPackage.TEMPLATEABLE_ELEMENT__OWNED_TEMPLATE_SIGNATURE;
				default: return -1;
			}
		}
		if (baseClass == org.eclipse.uml2.uml.Package.class) {
			switch (derivedFeatureID) {
				case AcmPackage.ACCESS_CONTROL_MODEL__OWNED_TYPE: return UMLPackage.PACKAGE__OWNED_TYPE;
				case AcmPackage.ACCESS_CONTROL_MODEL__PACKAGE_MERGE: return UMLPackage.PACKAGE__PACKAGE_MERGE;
				case AcmPackage.ACCESS_CONTROL_MODEL__PACKAGED_ELEMENT: return UMLPackage.PACKAGE__PACKAGED_ELEMENT;
				case AcmPackage.ACCESS_CONTROL_MODEL__NESTED_PACKAGE: return UMLPackage.PACKAGE__NESTED_PACKAGE;
				case AcmPackage.ACCESS_CONTROL_MODEL__NESTING_PACKAGE: return UMLPackage.PACKAGE__NESTING_PACKAGE;
				case AcmPackage.ACCESS_CONTROL_MODEL__PROFILE_APPLICATION: return UMLPackage.PACKAGE__PROFILE_APPLICATION;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == Namespace.class) {
			switch (baseFeatureID) {
				case UMLPackage.NAMESPACE__ELEMENT_IMPORT: return AcmPackage.ACCESS_CONTROL_MODEL__ELEMENT_IMPORT;
				case UMLPackage.NAMESPACE__PACKAGE_IMPORT: return AcmPackage.ACCESS_CONTROL_MODEL__PACKAGE_IMPORT;
				case UMLPackage.NAMESPACE__OWNED_RULE: return AcmPackage.ACCESS_CONTROL_MODEL__OWNED_RULE;
				case UMLPackage.NAMESPACE__MEMBER: return AcmPackage.ACCESS_CONTROL_MODEL__MEMBER;
				case UMLPackage.NAMESPACE__IMPORTED_MEMBER: return AcmPackage.ACCESS_CONTROL_MODEL__IMPORTED_MEMBER;
				case UMLPackage.NAMESPACE__OWNED_MEMBER: return AcmPackage.ACCESS_CONTROL_MODEL__OWNED_MEMBER;
				default: return -1;
			}
		}
		if (baseClass == TemplateableElement.class) {
			switch (baseFeatureID) {
				case UMLPackage.TEMPLATEABLE_ELEMENT__TEMPLATE_BINDING: return AcmPackage.ACCESS_CONTROL_MODEL__TEMPLATE_BINDING;
				case UMLPackage.TEMPLATEABLE_ELEMENT__OWNED_TEMPLATE_SIGNATURE: return AcmPackage.ACCESS_CONTROL_MODEL__OWNED_TEMPLATE_SIGNATURE;
				default: return -1;
			}
		}
		if (baseClass == org.eclipse.uml2.uml.Package.class) {
			switch (baseFeatureID) {
				case UMLPackage.PACKAGE__OWNED_TYPE: return AcmPackage.ACCESS_CONTROL_MODEL__OWNED_TYPE;
				case UMLPackage.PACKAGE__PACKAGE_MERGE: return AcmPackage.ACCESS_CONTROL_MODEL__PACKAGE_MERGE;
				case UMLPackage.PACKAGE__PACKAGED_ELEMENT: return AcmPackage.ACCESS_CONTROL_MODEL__PACKAGED_ELEMENT;
				case UMLPackage.PACKAGE__NESTED_PACKAGE: return AcmPackage.ACCESS_CONTROL_MODEL__NESTED_PACKAGE;
				case UMLPackage.PACKAGE__NESTING_PACKAGE: return AcmPackage.ACCESS_CONTROL_MODEL__NESTING_PACKAGE;
				case UMLPackage.PACKAGE__PROFILE_APPLICATION: return AcmPackage.ACCESS_CONTROL_MODEL__PROFILE_APPLICATION;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	@Override
	public PackageMerge createPackageMerge(Package mergedPackage) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public PackageMerge getPackageMerge(Package mergedPackage) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public PackageMerge getPackageMerge(Package mergedPackage,
			boolean createOnDemand) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public PackageableElement createPackagedElement(String name, EClass eClass) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public PackageableElement getPackagedElement(String name) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public PackageableElement getPackagedElement(String name,
			boolean ignoreCase, EClass eClass, boolean createOnDemand) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Type createOwnedType(String name, EClass eClass) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Type getOwnedType(String name) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Type getOwnedType(String name, boolean ignoreCase, EClass eClass,
			boolean createOnDemand) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Package createNestedPackage(String name, EClass eClass) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Package getNestedPackage(String name) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Package getNestedPackage(String name, boolean ignoreCase,
			EClass eClass, boolean createOnDemand) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ProfileApplication createProfileApplication() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Package createNestedPackage(String name) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ElementImport createElementImport(PackageableElement importedElement) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ElementImport getElementImport(PackageableElement importedElement) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ElementImport getElementImport(PackageableElement importedElement,
			boolean createOnDemand) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public PackageImport createPackageImport(Package importedPackage) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public PackageImport getPackageImport(Package importedPackage) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public PackageImport getPackageImport(Package importedPackage,
			boolean createOnDemand) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Constraint createOwnedRule(String name, EClass eClass) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Constraint createOwnedRule(String name) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Constraint getOwnedRule(String name) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Constraint getOwnedRule(String name, boolean ignoreCase,
			EClass eClass, boolean createOnDemand) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public NamedElement getMember(String name) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public NamedElement getMember(String name, boolean ignoreCase, EClass eClass) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public PackageableElement getImportedMember(String name) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public PackageableElement getImportedMember(String name,
			boolean ignoreCase, EClass eClass) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public NamedElement getOwnedMember(String name) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public NamedElement getOwnedMember(String name, boolean ignoreCase,
			EClass eClass) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TemplateBinding createTemplateBinding(TemplateSignature signature) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TemplateBinding getTemplateBinding(TemplateSignature signature) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TemplateBinding getTemplateBinding(TemplateSignature signature,
			boolean createOnDemand) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TemplateSignature createOwnedTemplateSignature(EClass eClass) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TemplateSignature createOwnedTemplateSignature() {
		// TODO Auto-generated method stub
		return null;
	}

} //AccessControlModelImpl
