/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package sare.language.acm.resource.acm.ui;

/**
 * An enumeration of all position categories.
 */
public enum AcmPositionCategory {
	BRACKET, DEFINTION, PROXY;
}
