/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package sare.language.acm.resource.acm.ui;

public class AcmCompletionProcessor implements org.eclipse.jface.text.contentassist.IContentAssistProcessor {
	
	private sare.language.acm.resource.acm.IAcmResourceProvider resourceProvider;
	private sare.language.acm.resource.acm.ui.IAcmBracketHandlerProvider bracketHandlerProvider;
	
	public AcmCompletionProcessor(sare.language.acm.resource.acm.IAcmResourceProvider resourceProvider, sare.language.acm.resource.acm.ui.IAcmBracketHandlerProvider bracketHandlerProvider) {
		this.resourceProvider = resourceProvider;
		this.bracketHandlerProvider = bracketHandlerProvider;
	}
	
	public org.eclipse.jface.text.contentassist.ICompletionProposal[] computeCompletionProposals(org.eclipse.jface.text.ITextViewer viewer, int offset) {
		sare.language.acm.resource.acm.IAcmTextResource textResource = resourceProvider.getResource();
		String content = viewer.getDocument().get();
		sare.language.acm.resource.acm.ui.AcmCodeCompletionHelper helper = new sare.language.acm.resource.acm.ui.AcmCodeCompletionHelper();
		sare.language.acm.resource.acm.ui.AcmCompletionProposal[] computedProposals = helper.computeCompletionProposals(textResource, content, offset);
		
		// call completion proposal post processor to allow for customizing the proposals
		sare.language.acm.resource.acm.ui.AcmProposalPostProcessor proposalPostProcessor = new sare.language.acm.resource.acm.ui.AcmProposalPostProcessor();
		java.util.List<sare.language.acm.resource.acm.ui.AcmCompletionProposal> computedProposalList = java.util.Arrays.asList(computedProposals);
		java.util.List<sare.language.acm.resource.acm.ui.AcmCompletionProposal> extendedProposalList = proposalPostProcessor.process(computedProposalList);
		if (extendedProposalList == null) {
			extendedProposalList = java.util.Collections.emptyList();
		}
		java.util.List<sare.language.acm.resource.acm.ui.AcmCompletionProposal> finalProposalList = new java.util.ArrayList<sare.language.acm.resource.acm.ui.AcmCompletionProposal>();
		for (sare.language.acm.resource.acm.ui.AcmCompletionProposal proposal : extendedProposalList) {
			if (proposal.getMatchesPrefix()) {
				finalProposalList.add(proposal);
			}
		}
		org.eclipse.jface.text.contentassist.ICompletionProposal[] result = new org.eclipse.jface.text.contentassist.ICompletionProposal[finalProposalList.size()];
		int i = 0;
		for (sare.language.acm.resource.acm.ui.AcmCompletionProposal proposal : finalProposalList) {
			String proposalString = proposal.getInsertString();
			String displayString = proposal.getDisplayString();
			String prefix = proposal.getPrefix();
			org.eclipse.swt.graphics.Image image = proposal.getImage();
			org.eclipse.jface.text.contentassist.IContextInformation info;
			info = new org.eclipse.jface.text.contentassist.ContextInformation(image, proposalString, proposalString);
			int begin = offset - prefix.length();
			int replacementLength = prefix.length();
			// if a closing bracket was automatically inserted right before, we enlarge the
			// replacement length in order to overwrite the bracket.
			sare.language.acm.resource.acm.ui.IAcmBracketHandler bracketHandler = bracketHandlerProvider.getBracketHandler();
			String closingBracket = bracketHandler.getClosingBracket();
			if (bracketHandler.addedClosingBracket() && proposalString.endsWith(closingBracket)) {
				replacementLength += closingBracket.length();
			}
			result[i++] = new org.eclipse.jface.text.contentassist.CompletionProposal(proposalString, begin, replacementLength, proposalString.length(), image, displayString, info, proposalString);
		}
		return result;
	}
	
	public org.eclipse.jface.text.contentassist.IContextInformation[] computeContextInformation(org.eclipse.jface.text.ITextViewer viewer, int offset) {
		return null;
	}
	
	public char[] getCompletionProposalAutoActivationCharacters() {
		return null;
	}
	
	public char[] getContextInformationAutoActivationCharacters() {
		return null;
	}
	
	public org.eclipse.jface.text.contentassist.IContextInformationValidator getContextInformationValidator() {
		return null;
	}
	
	public String getErrorMessage() {
		return null;
	}
}
