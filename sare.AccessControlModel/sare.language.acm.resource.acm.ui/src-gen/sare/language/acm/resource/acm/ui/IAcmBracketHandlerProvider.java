/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package sare.language.acm.resource.acm.ui;

/**
 * A provider for BracketHandler objects.
 */
public interface IAcmBracketHandlerProvider {
	
	/**
	 * Returns the bracket handler.
	 */
	public sare.language.acm.resource.acm.ui.IAcmBracketHandler getBracketHandler();
	
}
