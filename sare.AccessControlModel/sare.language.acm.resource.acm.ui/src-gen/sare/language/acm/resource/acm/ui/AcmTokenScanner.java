/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package sare.language.acm.resource.acm.ui;

/**
 * An adapter from the Eclipse
 * <code>org.eclipse.jface.text.rules.ITokenScanner</code> interface to the
 * generated lexer.
 */
public class AcmTokenScanner implements org.eclipse.jface.text.rules.ITokenScanner {
	
	private sare.language.acm.resource.acm.IAcmTextScanner lexer;
	private sare.language.acm.resource.acm.IAcmTextToken currentToken;
	private int offset;
	private String languageId;
	private org.eclipse.jface.preference.IPreferenceStore store;
	private sare.language.acm.resource.acm.ui.AcmColorManager colorManager;
	private sare.language.acm.resource.acm.IAcmTextResource resource;
	
	/**
	 * 
	 * @param colorManager A manager to obtain color objects
	 */
	public AcmTokenScanner(sare.language.acm.resource.acm.IAcmTextResource resource, sare.language.acm.resource.acm.ui.AcmColorManager colorManager) {
		this.resource = resource;
		this.colorManager = colorManager;
		this.lexer = new sare.language.acm.resource.acm.mopp.AcmMetaInformation().createLexer();
		this.languageId = new sare.language.acm.resource.acm.mopp.AcmMetaInformation().getSyntaxName();
		this.store = sare.language.acm.resource.acm.ui.AcmUIPlugin.getDefault().getPreferenceStore();
	}
	
	public int getTokenLength() {
		return currentToken.getLength();
	}
	
	public int getTokenOffset() {
		return offset + currentToken.getOffset();
	}
	
	public org.eclipse.jface.text.rules.IToken nextToken() {
		sare.language.acm.resource.acm.mopp.AcmDynamicTokenStyler dynamicTokenStyler = new sare.language.acm.resource.acm.mopp.AcmDynamicTokenStyler();
		currentToken = lexer.getNextToken();
		if (currentToken == null || !currentToken.canBeUsedForSyntaxHighlighting()) {
			return org.eclipse.jface.text.rules.Token.EOF;
		}
		org.eclipse.jface.text.TextAttribute ta = null;
		String tokenName = currentToken.getName();
		if (tokenName != null) {
			String enableKey = sare.language.acm.resource.acm.ui.AcmSyntaxColoringHelper.getPreferenceKey(languageId, tokenName, sare.language.acm.resource.acm.ui.AcmSyntaxColoringHelper.StyleProperty.ENABLE);
			boolean enabled = store.getBoolean(enableKey);
			sare.language.acm.resource.acm.IAcmTokenStyle staticStyle = null;
			if (enabled) {
				String colorKey = sare.language.acm.resource.acm.ui.AcmSyntaxColoringHelper.getPreferenceKey(languageId, tokenName, sare.language.acm.resource.acm.ui.AcmSyntaxColoringHelper.StyleProperty.COLOR);
				org.eclipse.swt.graphics.RGB foregroundRGB = org.eclipse.jface.preference.PreferenceConverter.getColor(store, colorKey);
				org.eclipse.swt.graphics.RGB backgroundRGB = null;
				boolean bold = store.getBoolean(sare.language.acm.resource.acm.ui.AcmSyntaxColoringHelper.getPreferenceKey(languageId, tokenName, sare.language.acm.resource.acm.ui.AcmSyntaxColoringHelper.StyleProperty.BOLD));
				boolean italic = store.getBoolean(sare.language.acm.resource.acm.ui.AcmSyntaxColoringHelper.getPreferenceKey(languageId, tokenName, sare.language.acm.resource.acm.ui.AcmSyntaxColoringHelper.StyleProperty.ITALIC));
				boolean strikethrough = store.getBoolean(sare.language.acm.resource.acm.ui.AcmSyntaxColoringHelper.getPreferenceKey(languageId, tokenName, sare.language.acm.resource.acm.ui.AcmSyntaxColoringHelper.StyleProperty.STRIKETHROUGH));
				boolean underline = store.getBoolean(sare.language.acm.resource.acm.ui.AcmSyntaxColoringHelper.getPreferenceKey(languageId, tokenName, sare.language.acm.resource.acm.ui.AcmSyntaxColoringHelper.StyleProperty.UNDERLINE));
				// now call dynamic token styler to allow to apply modifications to the static
				// style
				staticStyle = new sare.language.acm.resource.acm.mopp.AcmTokenStyle(convertToIntArray(foregroundRGB), convertToIntArray(backgroundRGB), bold, italic, strikethrough, underline);
			}
			sare.language.acm.resource.acm.IAcmTokenStyle dynamicStyle = dynamicTokenStyler.getDynamicTokenStyle(resource, currentToken, staticStyle);
			if (dynamicStyle != null) {
				int[] foregroundColorArray = dynamicStyle.getColorAsRGB();
				org.eclipse.swt.graphics.Color foregroundColor = colorManager.getColor(new org.eclipse.swt.graphics.RGB(foregroundColorArray[0], foregroundColorArray[1], foregroundColorArray[2]));
				int[] backgroundColorArray = dynamicStyle.getBackgroundColorAsRGB();
				org.eclipse.swt.graphics.Color backgroundColor = null;
				if (backgroundColorArray != null) {
					org.eclipse.swt.graphics.RGB backgroundRGB = new org.eclipse.swt.graphics.RGB(backgroundColorArray[0], backgroundColorArray[1], backgroundColorArray[2]);
					backgroundColor = colorManager.getColor(backgroundRGB);
				}
				int style = org.eclipse.swt.SWT.NORMAL;
				if (dynamicStyle.isBold()) {
					style = style | org.eclipse.swt.SWT.BOLD;
				}
				if (dynamicStyle.isItalic()) {
					style = style | org.eclipse.swt.SWT.ITALIC;
				}
				if (dynamicStyle.isStrikethrough()) {
					style = style | org.eclipse.jface.text.TextAttribute.STRIKETHROUGH;
				}
				if (dynamicStyle.isUnderline()) {
					style = style | org.eclipse.jface.text.TextAttribute.UNDERLINE;
				}
				ta = new org.eclipse.jface.text.TextAttribute(foregroundColor, backgroundColor, style);
			}
		}
		return new org.eclipse.jface.text.rules.Token(ta);
	}
	
	public void setRange(org.eclipse.jface.text.IDocument document, int offset, int length) {
		this.offset = offset;
		try {
			lexer.setText(document.get(offset, length));
		} catch (org.eclipse.jface.text.BadLocationException e) {
			// ignore this error. It might occur during editing when locations are outdated
			// quickly.
		}
	}
	
	public String getTokenText() {
		return currentToken.getText();
	}
	
	public int[] convertToIntArray(org.eclipse.swt.graphics.RGB rgb) {
		if (rgb == null) {
			return null;
		}
		return new int[] {rgb.red, rgb.green, rgb.blue};
	}
	
}
