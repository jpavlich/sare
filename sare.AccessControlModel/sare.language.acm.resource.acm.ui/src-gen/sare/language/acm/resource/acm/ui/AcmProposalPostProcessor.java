/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package sare.language.acm.resource.acm.ui;

/**
 * A class which can be overridden to customize code completion proposals.
 */
public class AcmProposalPostProcessor {
	
	public java.util.List<sare.language.acm.resource.acm.ui.AcmCompletionProposal> process(java.util.List<sare.language.acm.resource.acm.ui.AcmCompletionProposal> proposals) {
		// the default implementation does returns the proposals as they are
		return proposals;
	}
	
}
