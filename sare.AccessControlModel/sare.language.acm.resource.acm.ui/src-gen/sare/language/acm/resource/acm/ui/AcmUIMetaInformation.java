/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package sare.language.acm.resource.acm.ui;

public class AcmUIMetaInformation extends sare.language.acm.resource.acm.mopp.AcmMetaInformation {
	
	public sare.language.acm.resource.acm.IAcmHoverTextProvider getHoverTextProvider() {
		return new sare.language.acm.resource.acm.ui.AcmHoverTextProvider();
	}
	
	public sare.language.acm.resource.acm.ui.AcmImageProvider getImageProvider() {
		return sare.language.acm.resource.acm.ui.AcmImageProvider.INSTANCE;
	}
	
	public sare.language.acm.resource.acm.ui.AcmColorManager createColorManager() {
		return new sare.language.acm.resource.acm.ui.AcmColorManager();
	}
	
	/**
	 * @deprecated this method is only provided to preserve API compatibility. Use
	 * createTokenScanner(sare.language.acm.resource.acm.IAcmTextResource,
	 * sare.language.acm.resource.acm.ui.AcmColorManager) instead.
	 */
	public sare.language.acm.resource.acm.ui.AcmTokenScanner createTokenScanner(sare.language.acm.resource.acm.ui.AcmColorManager colorManager) {
		return createTokenScanner(null, colorManager);
	}
	
	public sare.language.acm.resource.acm.ui.AcmTokenScanner createTokenScanner(sare.language.acm.resource.acm.IAcmTextResource resource, sare.language.acm.resource.acm.ui.AcmColorManager colorManager) {
		return new sare.language.acm.resource.acm.ui.AcmTokenScanner(resource, colorManager);
	}
	
	public sare.language.acm.resource.acm.ui.AcmCodeCompletionHelper createCodeCompletionHelper() {
		return new sare.language.acm.resource.acm.ui.AcmCodeCompletionHelper();
	}
	
}
