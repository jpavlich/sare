/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package sare.language.acm.resource.acm.ui;

/**
 * A class used to initialize default preference values.
 */
public class AcmPreferenceInitializer extends org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer {
	
	private final static sare.language.acm.resource.acm.ui.AcmAntlrTokenHelper tokenHelper = new sare.language.acm.resource.acm.ui.AcmAntlrTokenHelper();
	
	public void initializeDefaultPreferences() {
		
		initializeDefaultSyntaxHighlighting();
		initializeDefaultBrackets();
		
		org.eclipse.jface.preference.IPreferenceStore store = sare.language.acm.resource.acm.ui.AcmUIPlugin.getDefault().getPreferenceStore();
		// Set default value for matching brackets
		store.setDefault(sare.language.acm.resource.acm.ui.AcmPreferenceConstants.EDITOR_MATCHING_BRACKETS_COLOR, "192,192,192");
		store.setDefault(sare.language.acm.resource.acm.ui.AcmPreferenceConstants.EDITOR_MATCHING_BRACKETS_CHECKBOX, true);
		
	}
	
	private void initializeDefaultBrackets() {
		org.eclipse.jface.preference.IPreferenceStore store = sare.language.acm.resource.acm.ui.AcmUIPlugin.getDefault().getPreferenceStore();
		initializeDefaultBrackets(store, new sare.language.acm.resource.acm.mopp.AcmMetaInformation());
	}
	
	public void initializeDefaultSyntaxHighlighting() {
		org.eclipse.jface.preference.IPreferenceStore store = sare.language.acm.resource.acm.ui.AcmUIPlugin.getDefault().getPreferenceStore();
		initializeDefaultSyntaxHighlighting(store, new sare.language.acm.resource.acm.mopp.AcmMetaInformation());
	}
	
	private void initializeDefaultBrackets(org.eclipse.jface.preference.IPreferenceStore store, sare.language.acm.resource.acm.IAcmMetaInformation metaInformation) {
		String languageId = metaInformation.getSyntaxName();
		// set default brackets for ITextResource bracket set
		sare.language.acm.resource.acm.ui.AcmBracketSet bracketSet = new sare.language.acm.resource.acm.ui.AcmBracketSet(null, null);
		final java.util.Collection<sare.language.acm.resource.acm.IAcmBracketPair> bracketPairs = metaInformation.getBracketPairs();
		if (bracketPairs != null) {
			for (sare.language.acm.resource.acm.IAcmBracketPair bracketPair : bracketPairs) {
				bracketSet.addBracketPair(bracketPair.getOpeningBracket(), bracketPair.getClosingBracket(), bracketPair.isClosingEnabledInside());
			}
		}
		store.setDefault(languageId + sare.language.acm.resource.acm.ui.AcmPreferenceConstants.EDITOR_BRACKETS_SUFFIX, bracketSet.getBracketString());
	}
	
	private void initializeDefaultSyntaxHighlighting(org.eclipse.jface.preference.IPreferenceStore store, sare.language.acm.resource.acm.IAcmMetaInformation metaInformation) {
		String languageId = metaInformation.getSyntaxName();
		String[] tokenNames = metaInformation.getTokenNames();
		if (tokenNames == null) {
			return;
		}
		for (int i = 0; i < tokenNames.length; i++) {
			if (!tokenHelper.canBeUsedForSyntaxHighlighting(i)) {
				continue;
			}
			
			String tokenName = tokenHelper.getTokenName(tokenNames, i);
			if (tokenName == null) {
				continue;
			}
			sare.language.acm.resource.acm.IAcmTokenStyle style = metaInformation.getDefaultTokenStyle(tokenName);
			if (style != null) {
				String color = getColorString(style.getColorAsRGB());
				setProperties(store, languageId, tokenName, color, style.isBold(), true, style.isItalic(), style.isStrikethrough(), style.isUnderline());
			} else {
				setProperties(store, languageId, tokenName, "0,0,0", false, false, false, false, false);
			}
		}
	}
	
	private void setProperties(org.eclipse.jface.preference.IPreferenceStore store, String languageID, String tokenName, String color, boolean bold, boolean enable, boolean italic, boolean strikethrough, boolean underline) {
		store.setDefault(sare.language.acm.resource.acm.ui.AcmSyntaxColoringHelper.getPreferenceKey(languageID, tokenName, sare.language.acm.resource.acm.ui.AcmSyntaxColoringHelper.StyleProperty.BOLD), bold);
		store.setDefault(sare.language.acm.resource.acm.ui.AcmSyntaxColoringHelper.getPreferenceKey(languageID, tokenName, sare.language.acm.resource.acm.ui.AcmSyntaxColoringHelper.StyleProperty.COLOR), color);
		store.setDefault(sare.language.acm.resource.acm.ui.AcmSyntaxColoringHelper.getPreferenceKey(languageID, tokenName, sare.language.acm.resource.acm.ui.AcmSyntaxColoringHelper.StyleProperty.ENABLE), enable);
		store.setDefault(sare.language.acm.resource.acm.ui.AcmSyntaxColoringHelper.getPreferenceKey(languageID, tokenName, sare.language.acm.resource.acm.ui.AcmSyntaxColoringHelper.StyleProperty.ITALIC), italic);
		store.setDefault(sare.language.acm.resource.acm.ui.AcmSyntaxColoringHelper.getPreferenceKey(languageID, tokenName, sare.language.acm.resource.acm.ui.AcmSyntaxColoringHelper.StyleProperty.STRIKETHROUGH), strikethrough);
		store.setDefault(sare.language.acm.resource.acm.ui.AcmSyntaxColoringHelper.getPreferenceKey(languageID, tokenName, sare.language.acm.resource.acm.ui.AcmSyntaxColoringHelper.StyleProperty.UNDERLINE), underline);
	}
	
	private String getColorString(int[] colorAsRGB) {
		if (colorAsRGB == null) {
			return "0,0,0";
		}
		if (colorAsRGB.length != 3) {
			return "0,0,0";
		}
		return colorAsRGB[0] + "," +colorAsRGB[1] + ","+ colorAsRGB[2];
	}
}
