SYNTAXDEF acm
FOR <http://sare/language/acm>
START AccessControlModel

// To load external resources: http://www.emftext.org/archive-emftext-users-list/2011/000822.html

IMPORTS {
	org.eclipse.emf.ecore:<http://www.eclipse.org/emf/2002/Ecore>
	org.eclipse.uml2.uml:<http://www.eclipse.org/uml2/3.0.0/UML> WITH SYNTAX miniuml <miniuml.cs>
}

OPTIONS {
	generateCodeFromGeneratorModel = "true";
	reloadGeneratorModel = "true";
	overrideReferenceResolvers="false";
}

TOKENS {
	DEFINE NAMED_ELEMENT_ID $ ($ + TEXT + $|'.'|'('|')'|','|'~')+$;
}

RULES {
	@SuppressWarnings
	AccessControlModel ::= ("import" imports['"','"'])* "policy" name[] "{" elements* "}";

	Role ::= "role" name[] ("extends" parentRoles[] ("," parentRoles[])*)? "{" permissions* "}";
	
	User ::= "user" name[] "hasRoles" ("," roles[])*;

	Permission ::=  (element[NAMED_ELEMENT_ID] | element[TEXT]) ("if" constraint[])? ("{" childPermissions* "}")?	;
	
}