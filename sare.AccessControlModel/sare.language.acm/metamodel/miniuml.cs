@SuppressWarnings
SYNTAXDEF miniuml 
FOR <http://www.eclipse.org/uml2/3.0.0/UML>
START Model
OPTIONS {
	generateCodeFromGeneratorModel = "true";
}
RULES {
	Model ::= ("import" packageImport)* "model" name[] "{" packagedElement* "}";
	
	PackageImport ::= importedPackage['"','"'];

	Package ::= "package" name[] "{" packagedElement* "}";

	Class ::= ownedComment*  "class" name[] ("extends" superClass[] ("," superClass[])*)? "{" (ownedAttribute | ownedOperation)* "}";

	Operation ::= ownedComment*  name[] "(" (ownedParameter ("," ownedParameter)*)? ")" (":" type[])?;

	Property ::= ownedComment*  name[] ":" type[]; 

	Parameter ::= ownedComment*  name[] ":" type[];
	
	Comment ::= "«" body[] "»" | "<<" body[] ">>";
}