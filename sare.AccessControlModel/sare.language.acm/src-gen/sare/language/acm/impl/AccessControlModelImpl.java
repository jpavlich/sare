/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package sare.language.acm.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.eclipse.uml2.uml.internal.impl.PackageableElementImpl;

import sare.language.acm.AccessControlModel;
import sare.language.acm.AcmPackage;
import sare.language.acm.Subject;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Access Control Model</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link sare.language.acm.impl.AccessControlModelImpl#getElements <em>Elements</em>}</li>
 *   <li>{@link sare.language.acm.impl.AccessControlModelImpl#getImports <em>Imports</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class AccessControlModelImpl extends PackageableElementImpl implements AccessControlModel
{
  /**
   * The cached value of the '{@link #getElements() <em>Elements</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getElements()
   * @generated
   * @ordered
   */
  protected EList<Subject> elements;

  /**
   * The cached value of the '{@link #getImports() <em>Imports</em>}' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getImports()
   * @generated
   * @ordered
   */
  protected EList<EObject> imports;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected AccessControlModelImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return AcmPackage.Literals.ACCESS_CONTROL_MODEL;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<Subject> getElements()
  {
    if (elements == null)
    {
      elements = new EObjectContainmentEList<Subject>(Subject.class, this, AcmPackage.ACCESS_CONTROL_MODEL__ELEMENTS);
    }
    return elements;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<EObject> getImports()
  {
    if (imports == null)
    {
      imports = new EObjectResolvingEList<EObject>(EObject.class, this, AcmPackage.ACCESS_CONTROL_MODEL__IMPORTS);
    }
    return imports;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case AcmPackage.ACCESS_CONTROL_MODEL__ELEMENTS:
        return ((InternalEList<?>)getElements()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case AcmPackage.ACCESS_CONTROL_MODEL__ELEMENTS:
        return getElements();
      case AcmPackage.ACCESS_CONTROL_MODEL__IMPORTS:
        return getImports();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case AcmPackage.ACCESS_CONTROL_MODEL__ELEMENTS:
        getElements().clear();
        getElements().addAll((Collection<? extends Subject>)newValue);
        return;
      case AcmPackage.ACCESS_CONTROL_MODEL__IMPORTS:
        getImports().clear();
        getImports().addAll((Collection<? extends EObject>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case AcmPackage.ACCESS_CONTROL_MODEL__ELEMENTS:
        getElements().clear();
        return;
      case AcmPackage.ACCESS_CONTROL_MODEL__IMPORTS:
        getImports().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case AcmPackage.ACCESS_CONTROL_MODEL__ELEMENTS:
        return elements != null && !elements.isEmpty();
      case AcmPackage.ACCESS_CONTROL_MODEL__IMPORTS:
        return imports != null && !imports.isEmpty();
    }
    return super.eIsSet(featureID);
  }

} //AccessControlModelImpl
