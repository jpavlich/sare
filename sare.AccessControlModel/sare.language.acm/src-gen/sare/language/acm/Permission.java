/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package sare.language.acm;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.uml2.uml.NamedElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Permission</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link sare.language.acm.Permission#getConstraint <em>Constraint</em>}</li>
 *   <li>{@link sare.language.acm.Permission#getElement <em>Element</em>}</li>
 *   <li>{@link sare.language.acm.Permission#getChildPermissions <em>Child Permissions</em>}</li>
 * </ul>
 * </p>
 *
 * @see sare.language.acm.AcmPackage#getPermission()
 * @model
 * @generated
 */
public interface Permission extends EObject
{
  /**
   * Returns the value of the '<em><b>Constraint</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Constraint</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Constraint</em>' attribute.
   * @see #setConstraint(String)
   * @see sare.language.acm.AcmPackage#getPermission_Constraint()
   * @model
   * @generated
   */
  String getConstraint();

  /**
   * Sets the value of the '{@link sare.language.acm.Permission#getConstraint <em>Constraint</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Constraint</em>' attribute.
   * @see #getConstraint()
   * @generated
   */
  void setConstraint(String value);

  /**
   * Returns the value of the '<em><b>Element</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Element</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Element</em>' reference.
   * @see #setElement(NamedElement)
   * @see sare.language.acm.AcmPackage#getPermission_Element()
   * @model required="true"
   * @generated
   */
  NamedElement getElement();

  /**
   * Sets the value of the '{@link sare.language.acm.Permission#getElement <em>Element</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Element</em>' reference.
   * @see #getElement()
   * @generated
   */
  void setElement(NamedElement value);

  /**
   * Returns the value of the '<em><b>Child Permissions</b></em>' containment reference list.
   * The list contents are of type {@link sare.language.acm.Permission}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Child Permissions</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Child Permissions</em>' containment reference list.
   * @see sare.language.acm.AcmPackage#getPermission_ChildPermissions()
   * @model containment="true"
   * @generated
   */
  EList<Permission> getChildPermissions();

} // Permission
