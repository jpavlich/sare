/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package sare.language.acm;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.uml2.uml.UMLPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see sare.language.acm.AcmFactory
 * @model kind="package"
 * @generated
 */
public interface AcmPackage extends EPackage
{
  /**
   * The package name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNAME = "acm";

  /**
   * The package namespace URI.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_URI = "http://sare/language/acm";

  /**
   * The package namespace name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_PREFIX = "acm";

  /**
   * The singleton instance of the package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  AcmPackage eINSTANCE = sare.language.acm.impl.AcmPackageImpl.init();

  /**
   * The meta object id for the '{@link sare.language.acm.impl.SubjectImpl <em>Subject</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see sare.language.acm.impl.SubjectImpl
   * @see sare.language.acm.impl.AcmPackageImpl#getSubject()
   * @generated
   */
  int SUBJECT = 4;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SUBJECT__NAME = 0;

  /**
   * The number of structural features of the '<em>Subject</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SUBJECT_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link sare.language.acm.impl.RoleImpl <em>Role</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see sare.language.acm.impl.RoleImpl
   * @see sare.language.acm.impl.AcmPackageImpl#getRole()
   * @generated
   */
  int ROLE = 0;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ROLE__NAME = SUBJECT__NAME;

  /**
   * The feature id for the '<em><b>Parent Roles</b></em>' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ROLE__PARENT_ROLES = SUBJECT_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Permissions</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ROLE__PERMISSIONS = SUBJECT_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Role</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ROLE_FEATURE_COUNT = SUBJECT_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link sare.language.acm.impl.UserImpl <em>User</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see sare.language.acm.impl.UserImpl
   * @see sare.language.acm.impl.AcmPackageImpl#getUser()
   * @generated
   */
  int USER = 1;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int USER__NAME = SUBJECT__NAME;

  /**
   * The feature id for the '<em><b>Roles</b></em>' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int USER__ROLES = SUBJECT_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>User</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int USER_FEATURE_COUNT = SUBJECT_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link sare.language.acm.impl.AccessControlModelImpl <em>Access Control Model</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see sare.language.acm.impl.AccessControlModelImpl
   * @see sare.language.acm.impl.AcmPackageImpl#getAccessControlModel()
   * @generated
   */
  int ACCESS_CONTROL_MODEL = 2;

  /**
   * The feature id for the '<em><b>EAnnotations</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ACCESS_CONTROL_MODEL__EANNOTATIONS = UMLPackage.PACKAGEABLE_ELEMENT__EANNOTATIONS;

  /**
   * The feature id for the '<em><b>Owned Element</b></em>' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ACCESS_CONTROL_MODEL__OWNED_ELEMENT = UMLPackage.PACKAGEABLE_ELEMENT__OWNED_ELEMENT;

  /**
   * The feature id for the '<em><b>Owner</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ACCESS_CONTROL_MODEL__OWNER = UMLPackage.PACKAGEABLE_ELEMENT__OWNER;

  /**
   * The feature id for the '<em><b>Owned Comment</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ACCESS_CONTROL_MODEL__OWNED_COMMENT = UMLPackage.PACKAGEABLE_ELEMENT__OWNED_COMMENT;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ACCESS_CONTROL_MODEL__NAME = UMLPackage.PACKAGEABLE_ELEMENT__NAME;

  /**
   * The feature id for the '<em><b>Visibility</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ACCESS_CONTROL_MODEL__VISIBILITY = UMLPackage.PACKAGEABLE_ELEMENT__VISIBILITY;

  /**
   * The feature id for the '<em><b>Qualified Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ACCESS_CONTROL_MODEL__QUALIFIED_NAME = UMLPackage.PACKAGEABLE_ELEMENT__QUALIFIED_NAME;

  /**
   * The feature id for the '<em><b>Client Dependency</b></em>' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ACCESS_CONTROL_MODEL__CLIENT_DEPENDENCY = UMLPackage.PACKAGEABLE_ELEMENT__CLIENT_DEPENDENCY;

  /**
   * The feature id for the '<em><b>Namespace</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ACCESS_CONTROL_MODEL__NAMESPACE = UMLPackage.PACKAGEABLE_ELEMENT__NAMESPACE;

  /**
   * The feature id for the '<em><b>Name Expression</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ACCESS_CONTROL_MODEL__NAME_EXPRESSION = UMLPackage.PACKAGEABLE_ELEMENT__NAME_EXPRESSION;

  /**
   * The feature id for the '<em><b>Owning Template Parameter</b></em>' container reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ACCESS_CONTROL_MODEL__OWNING_TEMPLATE_PARAMETER = UMLPackage.PACKAGEABLE_ELEMENT__OWNING_TEMPLATE_PARAMETER;

  /**
   * The feature id for the '<em><b>Template Parameter</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ACCESS_CONTROL_MODEL__TEMPLATE_PARAMETER = UMLPackage.PACKAGEABLE_ELEMENT__TEMPLATE_PARAMETER;

  /**
   * The feature id for the '<em><b>Elements</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ACCESS_CONTROL_MODEL__ELEMENTS = UMLPackage.PACKAGEABLE_ELEMENT_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Imports</b></em>' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ACCESS_CONTROL_MODEL__IMPORTS = UMLPackage.PACKAGEABLE_ELEMENT_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Access Control Model</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ACCESS_CONTROL_MODEL_FEATURE_COUNT = UMLPackage.PACKAGEABLE_ELEMENT_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link sare.language.acm.impl.PermissionImpl <em>Permission</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see sare.language.acm.impl.PermissionImpl
   * @see sare.language.acm.impl.AcmPackageImpl#getPermission()
   * @generated
   */
  int PERMISSION = 3;

  /**
   * The feature id for the '<em><b>Constraint</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PERMISSION__CONSTRAINT = 0;

  /**
   * The feature id for the '<em><b>Element</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PERMISSION__ELEMENT = 1;

  /**
   * The feature id for the '<em><b>Child Permissions</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PERMISSION__CHILD_PERMISSIONS = 2;

  /**
   * The number of structural features of the '<em>Permission</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PERMISSION_FEATURE_COUNT = 3;


  /**
   * Returns the meta object for class '{@link sare.language.acm.Role <em>Role</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Role</em>'.
   * @see sare.language.acm.Role
   * @generated
   */
  EClass getRole();

  /**
   * Returns the meta object for the reference list '{@link sare.language.acm.Role#getParentRoles <em>Parent Roles</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference list '<em>Parent Roles</em>'.
   * @see sare.language.acm.Role#getParentRoles()
   * @see #getRole()
   * @generated
   */
  EReference getRole_ParentRoles();

  /**
   * Returns the meta object for the containment reference list '{@link sare.language.acm.Role#getPermissions <em>Permissions</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Permissions</em>'.
   * @see sare.language.acm.Role#getPermissions()
   * @see #getRole()
   * @generated
   */
  EReference getRole_Permissions();

  /**
   * Returns the meta object for class '{@link sare.language.acm.User <em>User</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>User</em>'.
   * @see sare.language.acm.User
   * @generated
   */
  EClass getUser();

  /**
   * Returns the meta object for the reference list '{@link sare.language.acm.User#getRoles <em>Roles</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference list '<em>Roles</em>'.
   * @see sare.language.acm.User#getRoles()
   * @see #getUser()
   * @generated
   */
  EReference getUser_Roles();

  /**
   * Returns the meta object for class '{@link sare.language.acm.AccessControlModel <em>Access Control Model</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Access Control Model</em>'.
   * @see sare.language.acm.AccessControlModel
   * @generated
   */
  EClass getAccessControlModel();

  /**
   * Returns the meta object for the containment reference list '{@link sare.language.acm.AccessControlModel#getElements <em>Elements</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Elements</em>'.
   * @see sare.language.acm.AccessControlModel#getElements()
   * @see #getAccessControlModel()
   * @generated
   */
  EReference getAccessControlModel_Elements();

  /**
   * Returns the meta object for the reference list '{@link sare.language.acm.AccessControlModel#getImports <em>Imports</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference list '<em>Imports</em>'.
   * @see sare.language.acm.AccessControlModel#getImports()
   * @see #getAccessControlModel()
   * @generated
   */
  EReference getAccessControlModel_Imports();

  /**
   * Returns the meta object for class '{@link sare.language.acm.Permission <em>Permission</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Permission</em>'.
   * @see sare.language.acm.Permission
   * @generated
   */
  EClass getPermission();

  /**
   * Returns the meta object for the attribute '{@link sare.language.acm.Permission#getConstraint <em>Constraint</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Constraint</em>'.
   * @see sare.language.acm.Permission#getConstraint()
   * @see #getPermission()
   * @generated
   */
  EAttribute getPermission_Constraint();

  /**
   * Returns the meta object for the reference '{@link sare.language.acm.Permission#getElement <em>Element</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Element</em>'.
   * @see sare.language.acm.Permission#getElement()
   * @see #getPermission()
   * @generated
   */
  EReference getPermission_Element();

  /**
   * Returns the meta object for the containment reference list '{@link sare.language.acm.Permission#getChildPermissions <em>Child Permissions</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Child Permissions</em>'.
   * @see sare.language.acm.Permission#getChildPermissions()
   * @see #getPermission()
   * @generated
   */
  EReference getPermission_ChildPermissions();

  /**
   * Returns the meta object for class '{@link sare.language.acm.Subject <em>Subject</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Subject</em>'.
   * @see sare.language.acm.Subject
   * @generated
   */
  EClass getSubject();

  /**
   * Returns the meta object for the attribute '{@link sare.language.acm.Subject#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see sare.language.acm.Subject#getName()
   * @see #getSubject()
   * @generated
   */
  EAttribute getSubject_Name();

  /**
   * Returns the factory that creates the instances of the model.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the factory that creates the instances of the model.
   * @generated
   */
  AcmFactory getAcmFactory();

  /**
   * <!-- begin-user-doc -->
   * Defines literals for the meta objects that represent
   * <ul>
   *   <li>each class,</li>
   *   <li>each feature of each class,</li>
   *   <li>each enum,</li>
   *   <li>and each data type</li>
   * </ul>
   * <!-- end-user-doc -->
   * @generated
   */
  interface Literals
  {
    /**
     * The meta object literal for the '{@link sare.language.acm.impl.RoleImpl <em>Role</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see sare.language.acm.impl.RoleImpl
     * @see sare.language.acm.impl.AcmPackageImpl#getRole()
     * @generated
     */
    EClass ROLE = eINSTANCE.getRole();

    /**
     * The meta object literal for the '<em><b>Parent Roles</b></em>' reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ROLE__PARENT_ROLES = eINSTANCE.getRole_ParentRoles();

    /**
     * The meta object literal for the '<em><b>Permissions</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ROLE__PERMISSIONS = eINSTANCE.getRole_Permissions();

    /**
     * The meta object literal for the '{@link sare.language.acm.impl.UserImpl <em>User</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see sare.language.acm.impl.UserImpl
     * @see sare.language.acm.impl.AcmPackageImpl#getUser()
     * @generated
     */
    EClass USER = eINSTANCE.getUser();

    /**
     * The meta object literal for the '<em><b>Roles</b></em>' reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference USER__ROLES = eINSTANCE.getUser_Roles();

    /**
     * The meta object literal for the '{@link sare.language.acm.impl.AccessControlModelImpl <em>Access Control Model</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see sare.language.acm.impl.AccessControlModelImpl
     * @see sare.language.acm.impl.AcmPackageImpl#getAccessControlModel()
     * @generated
     */
    EClass ACCESS_CONTROL_MODEL = eINSTANCE.getAccessControlModel();

    /**
     * The meta object literal for the '<em><b>Elements</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ACCESS_CONTROL_MODEL__ELEMENTS = eINSTANCE.getAccessControlModel_Elements();

    /**
     * The meta object literal for the '<em><b>Imports</b></em>' reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ACCESS_CONTROL_MODEL__IMPORTS = eINSTANCE.getAccessControlModel_Imports();

    /**
     * The meta object literal for the '{@link sare.language.acm.impl.PermissionImpl <em>Permission</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see sare.language.acm.impl.PermissionImpl
     * @see sare.language.acm.impl.AcmPackageImpl#getPermission()
     * @generated
     */
    EClass PERMISSION = eINSTANCE.getPermission();

    /**
     * The meta object literal for the '<em><b>Constraint</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute PERMISSION__CONSTRAINT = eINSTANCE.getPermission_Constraint();

    /**
     * The meta object literal for the '<em><b>Element</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference PERMISSION__ELEMENT = eINSTANCE.getPermission_Element();

    /**
     * The meta object literal for the '<em><b>Child Permissions</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference PERMISSION__CHILD_PERMISSIONS = eINSTANCE.getPermission_ChildPermissions();

    /**
     * The meta object literal for the '{@link sare.language.acm.impl.SubjectImpl <em>Subject</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see sare.language.acm.impl.SubjectImpl
     * @see sare.language.acm.impl.AcmPackageImpl#getSubject()
     * @generated
     */
    EClass SUBJECT = eINSTANCE.getSubject();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute SUBJECT__NAME = eINSTANCE.getSubject_Name();

  }

} //AcmPackage
