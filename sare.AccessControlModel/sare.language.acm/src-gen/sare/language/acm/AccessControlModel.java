/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package sare.language.acm;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.uml2.uml.PackageableElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Access Control Model</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link sare.language.acm.AccessControlModel#getElements <em>Elements</em>}</li>
 *   <li>{@link sare.language.acm.AccessControlModel#getImports <em>Imports</em>}</li>
 * </ul>
 * </p>
 *
 * @see sare.language.acm.AcmPackage#getAccessControlModel()
 * @model
 * @generated
 */
public interface AccessControlModel extends PackageableElement
{
  /**
   * Returns the value of the '<em><b>Elements</b></em>' containment reference list.
   * The list contents are of type {@link sare.language.acm.Subject}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Elements</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Elements</em>' containment reference list.
   * @see sare.language.acm.AcmPackage#getAccessControlModel_Elements()
   * @model containment="true"
   * @generated
   */
  EList<Subject> getElements();

  /**
   * Returns the value of the '<em><b>Imports</b></em>' reference list.
   * The list contents are of type {@link org.eclipse.emf.ecore.EObject}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Imports</em>' reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Imports</em>' reference list.
   * @see sare.language.acm.AcmPackage#getAccessControlModel_Imports()
   * @model
   * @generated
   */
  EList<EObject> getImports();

} // AccessControlModel
