/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package sare.language.acm.resource.acm.analysis;

public class OperationTypeReferenceResolver implements sare.language.acm.resource.acm.IAcmReferenceResolver<org.eclipse.uml2.uml.Operation, org.eclipse.uml2.uml.Type> {
	
	private org.eclipse.uml2.uml.resource.miniuml.analysis.OperationTypeReferenceResolver delegate = new org.eclipse.uml2.uml.resource.miniuml.analysis.OperationTypeReferenceResolver();
	
	public void resolve(String identifier, org.eclipse.uml2.uml.Operation container, org.eclipse.emf.ecore.EReference reference, int position, boolean resolveFuzzy, final sare.language.acm.resource.acm.IAcmReferenceResolveResult<org.eclipse.uml2.uml.Type> result) {
		delegate.resolve(identifier, container, reference, position, resolveFuzzy, new org.eclipse.uml2.uml.resource.miniuml.IMiniumlReferenceResolveResult<org.eclipse.uml2.uml.Type>() {
			
			public boolean wasResolvedUniquely() {
				return result.wasResolvedUniquely();
			}
			
			public boolean wasResolvedMultiple() {
				return result.wasResolvedMultiple();
			}
			
			public boolean wasResolved() {
				return result.wasResolved();
			}
			
			public void setErrorMessage(String message) {
				result.setErrorMessage(message);
			}
			
			public java.util.Collection<org.eclipse.uml2.uml.resource.miniuml.IMiniumlReferenceMapping<org.eclipse.uml2.uml.Type>> getMappings() {
				throw new UnsupportedOperationException();
			}
			
			public String getErrorMessage() {
				return result.getErrorMessage();
			}
			
			public void addMapping(String identifier, org.eclipse.emf.common.util.URI newIdentifier) {
				result.addMapping(identifier, newIdentifier);
			}
			
			public void addMapping(String identifier, org.eclipse.emf.common.util.URI newIdentifier, String warning) {
				result.addMapping(identifier, newIdentifier, warning);
			}
			
			public void addMapping(String identifier, org.eclipse.uml2.uml.Type target) {
				result.addMapping(identifier, target);
			}
			
			public void addMapping(String identifier, org.eclipse.uml2.uml.Type target, String warning) {
				result.addMapping(identifier, target, warning);
			}
			
			public java.util.Collection<org.eclipse.uml2.uml.resource.miniuml.IMiniumlQuickFix> getQuickFixes() {
				return java.util.Collections.emptySet();
			}
			
			public void addQuickFix(final org.eclipse.uml2.uml.resource.miniuml.IMiniumlQuickFix quickFix) {
				result.addQuickFix(new sare.language.acm.resource.acm.IAcmQuickFix() {
					
					public String getImageKey() {
						return quickFix.getImageKey();
					}
					
					public String getDisplayString() {
						return quickFix.getDisplayString();
					}
					
					public java.util.Collection<org.eclipse.emf.ecore.EObject> getContextObjects() {
						return quickFix.getContextObjects();
					}
					
					public String getContextAsString() {
						return quickFix.getContextAsString();
					}
					
					public String apply(String currentText) {
						return quickFix.apply(currentText);
					}
				});
			}
		});
		
	}
	
	public String deResolve(org.eclipse.uml2.uml.Type element, org.eclipse.uml2.uml.Operation container, org.eclipse.emf.ecore.EReference reference) {
		return delegate.deResolve(element, container, reference);
	}
	
	public void setOptions(java.util.Map<?,?> options) {
		// save options in a field or leave method empty if this resolver does not depend
		// on any option
	}
	
}
