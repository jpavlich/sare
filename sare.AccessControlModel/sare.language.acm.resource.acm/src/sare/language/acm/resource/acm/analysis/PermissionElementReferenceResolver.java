/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package sare.language.acm.resource.acm.analysis;

public class PermissionElementReferenceResolver implements sare.language.acm.resource.acm.IAcmReferenceResolver<sare.language.acm.Permission, org.eclipse.uml2.uml.NamedElement> {
	
	private sare.language.acm.resource.acm.analysis.AcmDefaultResolverDelegate<sare.language.acm.Permission, org.eclipse.uml2.uml.NamedElement> delegate = new sare.language.acm.resource.acm.analysis.AcmDefaultResolverDelegate<sare.language.acm.Permission, org.eclipse.uml2.uml.NamedElement>();
	
	public void resolve(String identifier, sare.language.acm.Permission container, org.eclipse.emf.ecore.EReference reference, int position, boolean resolveFuzzy, final sare.language.acm.resource.acm.IAcmReferenceResolveResult<org.eclipse.uml2.uml.NamedElement> result) {
		delegate.resolve(identifier, container, reference, position, resolveFuzzy, result);
	}
	
	public String deResolve(org.eclipse.uml2.uml.NamedElement element, sare.language.acm.Permission container, org.eclipse.emf.ecore.EReference reference) {
		return delegate.deResolve(element, container, reference);
	}
	
	public void setOptions(java.util.Map<?,?> options) {
		// save options in a field or leave method empty if this resolver does not depend
		// on any option
	}
	
}
