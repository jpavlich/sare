/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package sare.language.acm.resource.acm.analysis;

public class AccessControlModelImportsReferenceResolver implements sare.language.acm.resource.acm.IAcmReferenceResolver<sare.language.acm.AccessControlModel, org.eclipse.emf.ecore.EObject> {
	
	private sare.language.acm.resource.acm.analysis.AcmDefaultResolverDelegate<sare.language.acm.AccessControlModel, org.eclipse.emf.ecore.EObject> delegate = new sare.language.acm.resource.acm.analysis.AcmDefaultResolverDelegate<sare.language.acm.AccessControlModel, org.eclipse.emf.ecore.EObject>();
	
	public void resolve(String identifier, sare.language.acm.AccessControlModel container, org.eclipse.emf.ecore.EReference reference, int position, boolean resolveFuzzy, final sare.language.acm.resource.acm.IAcmReferenceResolveResult<org.eclipse.emf.ecore.EObject> result) {
		delegate.resolve(identifier, container, reference, position, resolveFuzzy, result);
	}
	
	public String deResolve(org.eclipse.emf.ecore.EObject element, sare.language.acm.AccessControlModel container, org.eclipse.emf.ecore.EReference reference) {
		return delegate.deResolve(element, container, reference);
	}
	
	public void setOptions(java.util.Map<?,?> options) {
		// save options in a field or leave method empty if this resolver does not depend
		// on any option
	}
	
}
