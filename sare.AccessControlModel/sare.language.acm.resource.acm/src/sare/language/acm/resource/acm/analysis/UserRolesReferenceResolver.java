/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package sare.language.acm.resource.acm.analysis;

public class UserRolesReferenceResolver implements sare.language.acm.resource.acm.IAcmReferenceResolver<sare.language.acm.User, sare.language.acm.Role> {
	
	private sare.language.acm.resource.acm.analysis.AcmDefaultResolverDelegate<sare.language.acm.User, sare.language.acm.Role> delegate = new sare.language.acm.resource.acm.analysis.AcmDefaultResolverDelegate<sare.language.acm.User, sare.language.acm.Role>();
	
	public void resolve(String identifier, sare.language.acm.User container, org.eclipse.emf.ecore.EReference reference, int position, boolean resolveFuzzy, final sare.language.acm.resource.acm.IAcmReferenceResolveResult<sare.language.acm.Role> result) {
		delegate.resolve(identifier, container, reference, position, resolveFuzzy, result);
	}
	
	public String deResolve(sare.language.acm.Role element, sare.language.acm.User container, org.eclipse.emf.ecore.EReference reference) {
		return delegate.deResolve(element, container, reference);
	}
	
	public void setOptions(java.util.Map<?,?> options) {
		// save options in a field or leave method empty if this resolver does not depend
		// on any option
	}
	
}
