/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package sare.language.acm.resource.acm.mopp;

public class AcmParseResult implements sare.language.acm.resource.acm.IAcmParseResult {
	
	private org.eclipse.emf.ecore.EObject root;
	private java.util.Collection<sare.language.acm.resource.acm.IAcmCommand<sare.language.acm.resource.acm.IAcmTextResource>> commands = new java.util.ArrayList<sare.language.acm.resource.acm.IAcmCommand<sare.language.acm.resource.acm.IAcmTextResource>>();
	
	public AcmParseResult() {
		super();
	}
	
	public void setRoot(org.eclipse.emf.ecore.EObject root) {
		this.root = root;
	}
	
	public org.eclipse.emf.ecore.EObject getRoot() {
		return root;
	}
	
	public java.util.Collection<sare.language.acm.resource.acm.IAcmCommand<sare.language.acm.resource.acm.IAcmTextResource>> getPostParseCommands() {
		return commands;
	}
	
}
