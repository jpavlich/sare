/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package sare.language.acm.resource.acm.mopp;

public class AcmReferenceResolverSwitch implements sare.language.acm.resource.acm.IAcmReferenceResolverSwitch {
	
	protected sare.language.acm.resource.acm.analysis.AccessControlModelImportsReferenceResolver accessControlModelImportsReferenceResolver = new sare.language.acm.resource.acm.analysis.AccessControlModelImportsReferenceResolver();
	protected sare.language.acm.resource.acm.analysis.RoleParentRolesReferenceResolver roleParentRolesReferenceResolver = new sare.language.acm.resource.acm.analysis.RoleParentRolesReferenceResolver();
	protected sare.language.acm.resource.acm.analysis.UserRolesReferenceResolver userRolesReferenceResolver = new sare.language.acm.resource.acm.analysis.UserRolesReferenceResolver();
	protected sare.language.acm.resource.acm.analysis.PermissionElementReferenceResolver permissionElementReferenceResolver = new sare.language.acm.resource.acm.analysis.PermissionElementReferenceResolver();
	protected sare.language.acm.resource.acm.analysis.PackageImportImportedPackageReferenceResolver packageImportImportedPackageReferenceResolver = new sare.language.acm.resource.acm.analysis.PackageImportImportedPackageReferenceResolver();
	protected sare.language.acm.resource.acm.analysis.ClassSuperClassReferenceResolver classSuperClassReferenceResolver = new sare.language.acm.resource.acm.analysis.ClassSuperClassReferenceResolver();
	protected sare.language.acm.resource.acm.analysis.OperationTypeReferenceResolver operationTypeReferenceResolver = new sare.language.acm.resource.acm.analysis.OperationTypeReferenceResolver();
	protected sare.language.acm.resource.acm.analysis.TypedElementTypeReferenceResolver typedElementTypeReferenceResolver = new sare.language.acm.resource.acm.analysis.TypedElementTypeReferenceResolver();
	
	public sare.language.acm.resource.acm.analysis.AccessControlModelImportsReferenceResolver getAccessControlModelImportsReferenceResolver() {
		return accessControlModelImportsReferenceResolver;
	}
	
	public sare.language.acm.resource.acm.analysis.RoleParentRolesReferenceResolver getRoleParentRolesReferenceResolver() {
		return roleParentRolesReferenceResolver;
	}
	
	public sare.language.acm.resource.acm.analysis.UserRolesReferenceResolver getUserRolesReferenceResolver() {
		return userRolesReferenceResolver;
	}
	
	public sare.language.acm.resource.acm.analysis.PermissionElementReferenceResolver getPermissionElementReferenceResolver() {
		return permissionElementReferenceResolver;
	}
	
	public sare.language.acm.resource.acm.analysis.PackageImportImportedPackageReferenceResolver getPackageImportImportedPackageReferenceResolver() {
		return packageImportImportedPackageReferenceResolver;
	}
	
	public sare.language.acm.resource.acm.analysis.ClassSuperClassReferenceResolver getClassSuperClassReferenceResolver() {
		return classSuperClassReferenceResolver;
	}
	
	public sare.language.acm.resource.acm.analysis.OperationTypeReferenceResolver getOperationTypeReferenceResolver() {
		return operationTypeReferenceResolver;
	}
	
	public sare.language.acm.resource.acm.analysis.TypedElementTypeReferenceResolver getTypedElementTypeReferenceResolver() {
		return typedElementTypeReferenceResolver;
	}
	
	public void setOptions(java.util.Map<?, ?> options) {
		accessControlModelImportsReferenceResolver.setOptions(options);
		roleParentRolesReferenceResolver.setOptions(options);
		userRolesReferenceResolver.setOptions(options);
		permissionElementReferenceResolver.setOptions(options);
		packageImportImportedPackageReferenceResolver.setOptions(options);
		classSuperClassReferenceResolver.setOptions(options);
		operationTypeReferenceResolver.setOptions(options);
		typedElementTypeReferenceResolver.setOptions(options);
	}
	
	public void resolveFuzzy(String identifier, org.eclipse.emf.ecore.EObject container, org.eclipse.emf.ecore.EReference reference, int position, sare.language.acm.resource.acm.IAcmReferenceResolveResult<org.eclipse.emf.ecore.EObject> result) {
		if (container == null) {
			return;
		}
		if (sare.language.acm.AcmPackage.eINSTANCE.getAccessControlModel().isInstance(container)) {
			AcmFuzzyResolveResult<org.eclipse.emf.ecore.EObject> frr = new AcmFuzzyResolveResult<org.eclipse.emf.ecore.EObject>(result);
			String referenceName = reference.getName();
			org.eclipse.emf.ecore.EStructuralFeature feature = container.eClass().getEStructuralFeature(referenceName);
			if (feature != null && feature instanceof org.eclipse.emf.ecore.EReference && referenceName != null && referenceName.equals("imports")) {
				accessControlModelImportsReferenceResolver.resolve(identifier, (sare.language.acm.AccessControlModel) container, (org.eclipse.emf.ecore.EReference) feature, position, true, frr);
			}
		}
		if (sare.language.acm.AcmPackage.eINSTANCE.getRole().isInstance(container)) {
			AcmFuzzyResolveResult<sare.language.acm.Role> frr = new AcmFuzzyResolveResult<sare.language.acm.Role>(result);
			String referenceName = reference.getName();
			org.eclipse.emf.ecore.EStructuralFeature feature = container.eClass().getEStructuralFeature(referenceName);
			if (feature != null && feature instanceof org.eclipse.emf.ecore.EReference && referenceName != null && referenceName.equals("parentRoles")) {
				roleParentRolesReferenceResolver.resolve(identifier, (sare.language.acm.Role) container, (org.eclipse.emf.ecore.EReference) feature, position, true, frr);
			}
		}
		if (sare.language.acm.AcmPackage.eINSTANCE.getUser().isInstance(container)) {
			AcmFuzzyResolveResult<sare.language.acm.Role> frr = new AcmFuzzyResolveResult<sare.language.acm.Role>(result);
			String referenceName = reference.getName();
			org.eclipse.emf.ecore.EStructuralFeature feature = container.eClass().getEStructuralFeature(referenceName);
			if (feature != null && feature instanceof org.eclipse.emf.ecore.EReference && referenceName != null && referenceName.equals("roles")) {
				userRolesReferenceResolver.resolve(identifier, (sare.language.acm.User) container, (org.eclipse.emf.ecore.EReference) feature, position, true, frr);
			}
		}
		if (sare.language.acm.AcmPackage.eINSTANCE.getPermission().isInstance(container)) {
			AcmFuzzyResolveResult<org.eclipse.uml2.uml.NamedElement> frr = new AcmFuzzyResolveResult<org.eclipse.uml2.uml.NamedElement>(result);
			String referenceName = reference.getName();
			org.eclipse.emf.ecore.EStructuralFeature feature = container.eClass().getEStructuralFeature(referenceName);
			if (feature != null && feature instanceof org.eclipse.emf.ecore.EReference && referenceName != null && referenceName.equals("element")) {
				permissionElementReferenceResolver.resolve(identifier, (sare.language.acm.Permission) container, (org.eclipse.emf.ecore.EReference) feature, position, true, frr);
			}
		}
		if (org.eclipse.uml2.uml.UMLPackage.eINSTANCE.getPackageImport().isInstance(container)) {
			AcmFuzzyResolveResult<org.eclipse.uml2.uml.Package> frr = new AcmFuzzyResolveResult<org.eclipse.uml2.uml.Package>(result);
			String referenceName = reference.getName();
			org.eclipse.emf.ecore.EStructuralFeature feature = container.eClass().getEStructuralFeature(referenceName);
			if (feature != null && feature instanceof org.eclipse.emf.ecore.EReference && referenceName != null && referenceName.equals("importedPackage")) {
				packageImportImportedPackageReferenceResolver.resolve(identifier, (org.eclipse.uml2.uml.PackageImport) container, (org.eclipse.emf.ecore.EReference) feature, position, true, frr);
			}
		}
		if (org.eclipse.uml2.uml.UMLPackage.eINSTANCE.getClass().isInstance(container)) {
			AcmFuzzyResolveResult<org.eclipse.uml2.uml.Class> frr = new AcmFuzzyResolveResult<org.eclipse.uml2.uml.Class>(result);
			String referenceName = reference.getName();
			org.eclipse.emf.ecore.EStructuralFeature feature = container.eClass().getEStructuralFeature(referenceName);
			if (feature != null && feature instanceof org.eclipse.emf.ecore.EReference && referenceName != null && referenceName.equals("superClass")) {
				classSuperClassReferenceResolver.resolve(identifier, (org.eclipse.uml2.uml.Class) container, (org.eclipse.emf.ecore.EReference) feature, position, true, frr);
			}
		}
		if (org.eclipse.uml2.uml.UMLPackage.eINSTANCE.getOperation().isInstance(container)) {
			AcmFuzzyResolveResult<org.eclipse.uml2.uml.Type> frr = new AcmFuzzyResolveResult<org.eclipse.uml2.uml.Type>(result);
			String referenceName = reference.getName();
			org.eclipse.emf.ecore.EStructuralFeature feature = container.eClass().getEStructuralFeature(referenceName);
			if (feature != null && feature instanceof org.eclipse.emf.ecore.EReference && referenceName != null && referenceName.equals("type")) {
				operationTypeReferenceResolver.resolve(identifier, (org.eclipse.uml2.uml.Operation) container, (org.eclipse.emf.ecore.EReference) feature, position, true, frr);
			}
		}
		if (org.eclipse.uml2.uml.UMLPackage.eINSTANCE.getTypedElement().isInstance(container)) {
			AcmFuzzyResolveResult<org.eclipse.uml2.uml.Type> frr = new AcmFuzzyResolveResult<org.eclipse.uml2.uml.Type>(result);
			String referenceName = reference.getName();
			org.eclipse.emf.ecore.EStructuralFeature feature = container.eClass().getEStructuralFeature(referenceName);
			if (feature != null && feature instanceof org.eclipse.emf.ecore.EReference && referenceName != null && referenceName.equals("type")) {
				typedElementTypeReferenceResolver.resolve(identifier, (org.eclipse.uml2.uml.TypedElement) container, (org.eclipse.emf.ecore.EReference) feature, position, true, frr);
			}
		}
	}
	
	public sare.language.acm.resource.acm.IAcmReferenceResolver<? extends org.eclipse.emf.ecore.EObject, ? extends org.eclipse.emf.ecore.EObject> getResolver(org.eclipse.emf.ecore.EStructuralFeature reference) {
		if (reference == sare.language.acm.AcmPackage.eINSTANCE.getAccessControlModel_Imports()) {
			return accessControlModelImportsReferenceResolver;
		}
		if (reference == sare.language.acm.AcmPackage.eINSTANCE.getRole_ParentRoles()) {
			return roleParentRolesReferenceResolver;
		}
		if (reference == sare.language.acm.AcmPackage.eINSTANCE.getUser_Roles()) {
			return userRolesReferenceResolver;
		}
		if (reference == sare.language.acm.AcmPackage.eINSTANCE.getPermission_Element()) {
			return permissionElementReferenceResolver;
		}
		if (reference == org.eclipse.uml2.uml.UMLPackage.eINSTANCE.getPackageImport_ImportedPackage()) {
			return packageImportImportedPackageReferenceResolver;
		}
		if (reference == org.eclipse.uml2.uml.UMLPackage.eINSTANCE.getClass_SuperClass()) {
			return classSuperClassReferenceResolver;
		}
		if (reference == org.eclipse.uml2.uml.UMLPackage.eINSTANCE.getOperation_Type()) {
			return operationTypeReferenceResolver;
		}
		if (reference == org.eclipse.uml2.uml.UMLPackage.eINSTANCE.getTypedElement_Type()) {
			return typedElementTypeReferenceResolver;
		}
		return null;
	}
	
}
