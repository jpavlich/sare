/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package sare.language.acm.resource.acm;

public interface IAcmBuilder {
	
	public boolean isBuildingNeeded(org.eclipse.emf.common.util.URI uri);
	
	public org.eclipse.core.runtime.IStatus build(sare.language.acm.resource.acm.mopp.AcmResource resource, org.eclipse.core.runtime.IProgressMonitor monitor);
}
