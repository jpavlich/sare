/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package sare.language.acm.resource.acm.mopp;

/**
 * A representation for a range in a document where a keyword (i.e., a static
 * string) is expected.
 */
public class AcmExpectedCsString extends sare.language.acm.resource.acm.mopp.AcmAbstractExpectedElement {
	
	private sare.language.acm.resource.acm.grammar.AcmKeyword keyword;
	
	public AcmExpectedCsString(sare.language.acm.resource.acm.grammar.AcmKeyword keyword) {
		super(keyword.getMetaclass());
		this.keyword = keyword;
	}
	
	public String getValue() {
		return keyword.getValue();
	}
	
	public java.util.Set<String> getTokenNames() {
		return java.util.Collections.singleton("'" + getValue() + "'");
	}
	
	public String toString() {
		return "CsString \"" + getValue() + "\"";
	}
	
	public boolean equals(Object o) {
		if (o instanceof AcmExpectedCsString) {
			return getValue().equals(((AcmExpectedCsString) o).getValue());
		}
		return false;
	}
	
}
