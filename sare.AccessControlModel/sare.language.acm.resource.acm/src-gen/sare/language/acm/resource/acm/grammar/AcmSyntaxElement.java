/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package sare.language.acm.resource.acm.grammar;

/**
 * The abstract super class for all elements of a grammar. This class provides
 * methods to traverse the grammar rules.
 */
public abstract class AcmSyntaxElement {
	
	private AcmSyntaxElement[] children;
	private AcmSyntaxElement parent;
	private sare.language.acm.resource.acm.grammar.AcmCardinality cardinality;
	
	public AcmSyntaxElement(sare.language.acm.resource.acm.grammar.AcmCardinality cardinality, AcmSyntaxElement[] children) {
		this.cardinality = cardinality;
		this.children = children;
		if (this.children != null) {
			for (AcmSyntaxElement child : this.children) {
				child.setParent(this);
			}
		}
	}
	
	public void setParent(AcmSyntaxElement parent) {
		assert this.parent == null;
		this.parent = parent;
	}
	
	public AcmSyntaxElement[] getChildren() {
		if (children == null) {
			return new AcmSyntaxElement[0];
		}
		return children;
	}
	
	public org.eclipse.emf.ecore.EClass getMetaclass() {
		return parent.getMetaclass();
	}
	
	public sare.language.acm.resource.acm.grammar.AcmCardinality getCardinality() {
		return cardinality;
	}
	
}
