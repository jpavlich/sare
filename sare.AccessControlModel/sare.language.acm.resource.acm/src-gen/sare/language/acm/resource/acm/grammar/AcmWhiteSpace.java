/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package sare.language.acm.resource.acm.grammar;

public class AcmWhiteSpace extends sare.language.acm.resource.acm.grammar.AcmFormattingElement {
	
	private final int amount;
	
	public AcmWhiteSpace(int amount, sare.language.acm.resource.acm.grammar.AcmCardinality cardinality) {
		super(cardinality);
		this.amount = amount;
	}
	
	public int getAmount() {
		return amount;
	}
	
	public String toString() {
		return "#" + getAmount();
	}
	
}
