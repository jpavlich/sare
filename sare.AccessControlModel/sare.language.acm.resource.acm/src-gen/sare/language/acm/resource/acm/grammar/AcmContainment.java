/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package sare.language.acm.resource.acm.grammar;

public class AcmContainment extends sare.language.acm.resource.acm.grammar.AcmTerminal {
	
	public AcmContainment(org.eclipse.emf.ecore.EStructuralFeature feature, sare.language.acm.resource.acm.grammar.AcmCardinality cardinality, int mandatoryOccurencesAfter) {
		super(feature, cardinality, mandatoryOccurencesAfter);
	}
	
	public String toString() {
		return getFeature().getName();
	}
	
}
