/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package sare.language.acm.resource.acm;

/**
 * Implementors of this interface provide an EMF resource.
 */
public interface IAcmResourceProvider {
	
	/**
	 * Returns the resource.
	 */
	public sare.language.acm.resource.acm.IAcmTextResource getResource();
	
}
