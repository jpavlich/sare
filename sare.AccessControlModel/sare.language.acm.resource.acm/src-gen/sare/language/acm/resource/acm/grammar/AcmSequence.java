/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package sare.language.acm.resource.acm.grammar;

public class AcmSequence extends sare.language.acm.resource.acm.grammar.AcmSyntaxElement {
	
	public AcmSequence(sare.language.acm.resource.acm.grammar.AcmCardinality cardinality, sare.language.acm.resource.acm.grammar.AcmSyntaxElement... elements) {
		super(cardinality, elements);
	}
	
	public String toString() {
		return sare.language.acm.resource.acm.util.AcmStringUtil.explode(getChildren(), " ");
	}
	
}
