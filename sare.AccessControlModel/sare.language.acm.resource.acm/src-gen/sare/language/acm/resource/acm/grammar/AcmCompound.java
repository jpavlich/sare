/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package sare.language.acm.resource.acm.grammar;

public class AcmCompound extends sare.language.acm.resource.acm.grammar.AcmSyntaxElement {
	
	public AcmCompound(sare.language.acm.resource.acm.grammar.AcmChoice choice, sare.language.acm.resource.acm.grammar.AcmCardinality cardinality) {
		super(cardinality, new sare.language.acm.resource.acm.grammar.AcmSyntaxElement[] {choice});
	}
	
	public String toString() {
		return "(" + getChildren()[0] + ")";
	}
	
}
