/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package sare.language.acm.resource.acm.grammar;

public class AcmChoice extends sare.language.acm.resource.acm.grammar.AcmSyntaxElement {
	
	public AcmChoice(sare.language.acm.resource.acm.grammar.AcmCardinality cardinality, sare.language.acm.resource.acm.grammar.AcmSyntaxElement... choices) {
		super(cardinality, choices);
	}
	
	public String toString() {
		return sare.language.acm.resource.acm.util.AcmStringUtil.explode(getChildren(), "|");
	}
	
}
