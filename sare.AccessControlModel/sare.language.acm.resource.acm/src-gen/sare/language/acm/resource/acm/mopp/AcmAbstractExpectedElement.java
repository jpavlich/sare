/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package sare.language.acm.resource.acm.mopp;

/**
 * Abstract super class for all expected elements. Provides methods to add
 * followers.
 */
public abstract class AcmAbstractExpectedElement implements sare.language.acm.resource.acm.IAcmExpectedElement {
	
	private org.eclipse.emf.ecore.EClass ruleMetaclass;
	private java.util.Set<sare.language.acm.resource.acm.util.AcmPair<sare.language.acm.resource.acm.IAcmExpectedElement, org.eclipse.emf.ecore.EStructuralFeature[]>> followers = new java.util.LinkedHashSet<sare.language.acm.resource.acm.util.AcmPair<sare.language.acm.resource.acm.IAcmExpectedElement, org.eclipse.emf.ecore.EStructuralFeature[]>>();
	
	public AcmAbstractExpectedElement(org.eclipse.emf.ecore.EClass ruleMetaclass) {
		super();
		this.ruleMetaclass = ruleMetaclass;
	}
	
	public org.eclipse.emf.ecore.EClass getRuleMetaclass() {
		return ruleMetaclass;
	}
	
	public void addFollower(sare.language.acm.resource.acm.IAcmExpectedElement follower, org.eclipse.emf.ecore.EStructuralFeature[] path) {
		followers.add(new sare.language.acm.resource.acm.util.AcmPair<sare.language.acm.resource.acm.IAcmExpectedElement, org.eclipse.emf.ecore.EStructuralFeature[]>(follower, path));
	}
	
	public java.util.Collection<sare.language.acm.resource.acm.util.AcmPair<sare.language.acm.resource.acm.IAcmExpectedElement, org.eclipse.emf.ecore.EStructuralFeature[]>> getFollowers() {
		return followers;
	}
	
}
