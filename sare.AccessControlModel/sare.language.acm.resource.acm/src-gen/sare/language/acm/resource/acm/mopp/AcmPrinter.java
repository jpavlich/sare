/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package sare.language.acm.resource.acm.mopp;

public class AcmPrinter implements sare.language.acm.resource.acm.IAcmTextPrinter {
	
	protected sare.language.acm.resource.acm.IAcmTokenResolverFactory tokenResolverFactory = new sare.language.acm.resource.acm.mopp.AcmTokenResolverFactory();
	
	protected java.io.OutputStream outputStream;
	
	/**
	 * Holds the resource that is associated with this printer. This may be null if
	 * the printer is used stand alone.
	 */
	private sare.language.acm.resource.acm.IAcmTextResource resource;
	
	private java.util.Map<?, ?> options;
	
	public AcmPrinter(java.io.OutputStream outputStream, sare.language.acm.resource.acm.IAcmTextResource resource) {
		super();
		this.outputStream = outputStream;
		this.resource = resource;
	}
	
	protected int matchCount(java.util.Map<String, Integer> featureCounter, java.util.Collection<String> needed) {
		int pos = 0;
		int neg = 0;
		
		for (String featureName : featureCounter.keySet()) {
			if (needed.contains(featureName)) {
				int value = featureCounter.get(featureName);
				if (value == 0) {
					neg += 1;
				} else {
					pos += 1;
				}
			}
		}
		return neg > 0 ? -neg : pos;
	}
	
	protected void doPrint(org.eclipse.emf.ecore.EObject element, java.io.PrintWriter out, String globaltab) {
		if (element == null) {
			throw new java.lang.IllegalArgumentException("Nothing to write.");
		}
		if (out == null) {
			throw new java.lang.IllegalArgumentException("Nothing to write on.");
		}
		
		if (element instanceof sare.language.acm.AccessControlModel) {
			print_sare_language_acm_AccessControlModel((sare.language.acm.AccessControlModel) element, globaltab, out);
			return;
		}
		if (element instanceof sare.language.acm.Role) {
			print_sare_language_acm_Role((sare.language.acm.Role) element, globaltab, out);
			return;
		}
		if (element instanceof sare.language.acm.User) {
			print_sare_language_acm_User((sare.language.acm.User) element, globaltab, out);
			return;
		}
		if (element instanceof sare.language.acm.Permission) {
			print_sare_language_acm_Permission((sare.language.acm.Permission) element, globaltab, out);
			return;
		}
		if (element instanceof org.eclipse.uml2.uml.Model) {
			print_org_eclipse_uml2_uml_Model((org.eclipse.uml2.uml.Model) element, globaltab, out);
			return;
		}
		if (element instanceof org.eclipse.uml2.uml.PackageImport) {
			print_org_eclipse_uml2_uml_PackageImport((org.eclipse.uml2.uml.PackageImport) element, globaltab, out);
			return;
		}
		if (element instanceof org.eclipse.uml2.uml.Package) {
			print_org_eclipse_uml2_uml_Package((org.eclipse.uml2.uml.Package) element, globaltab, out);
			return;
		}
		if (element instanceof org.eclipse.uml2.uml.Class) {
			print_org_eclipse_uml2_uml_Class((org.eclipse.uml2.uml.Class) element, globaltab, out);
			return;
		}
		if (element instanceof org.eclipse.uml2.uml.Operation) {
			print_org_eclipse_uml2_uml_Operation((org.eclipse.uml2.uml.Operation) element, globaltab, out);
			return;
		}
		if (element instanceof org.eclipse.uml2.uml.Property) {
			print_org_eclipse_uml2_uml_Property((org.eclipse.uml2.uml.Property) element, globaltab, out);
			return;
		}
		if (element instanceof org.eclipse.uml2.uml.Parameter) {
			print_org_eclipse_uml2_uml_Parameter((org.eclipse.uml2.uml.Parameter) element, globaltab, out);
			return;
		}
		if (element instanceof org.eclipse.uml2.uml.Comment) {
			print_org_eclipse_uml2_uml_Comment((org.eclipse.uml2.uml.Comment) element, globaltab, out);
			return;
		}
		
		addWarningToResource("The printer can not handle " + element.eClass().getName() + " elements", element);
	}
	
	protected sare.language.acm.resource.acm.mopp.AcmReferenceResolverSwitch getReferenceResolverSwitch() {
		return (sare.language.acm.resource.acm.mopp.AcmReferenceResolverSwitch) new sare.language.acm.resource.acm.mopp.AcmMetaInformation().getReferenceResolverSwitch();
	}
	
	protected void addWarningToResource(final String errorMessage, org.eclipse.emf.ecore.EObject cause) {
		sare.language.acm.resource.acm.IAcmTextResource resource = getResource();
		if (resource == null) {
			// the resource can be null if the printer is used stand alone
			return;
		}
		resource.addProblem(new sare.language.acm.resource.acm.mopp.AcmProblem(errorMessage, sare.language.acm.resource.acm.AcmEProblemType.PRINT_PROBLEM, sare.language.acm.resource.acm.AcmEProblemSeverity.WARNING), cause);
	}
	
	public void setOptions(java.util.Map<?,?> options) {
		this.options = options;
	}
	
	public java.util.Map<?,?> getOptions() {
		return options;
	}
	
	public sare.language.acm.resource.acm.IAcmTextResource getResource() {
		return resource;
	}
	
	/**
	 * Calls {@link #doPrint(EObject, PrintWriter, String)} and writes the result to
	 * the underlying output stream.
	 */
	public void print(org.eclipse.emf.ecore.EObject element) {
		java.io.PrintWriter out = new java.io.PrintWriter(new java.io.BufferedOutputStream(outputStream));
		doPrint(element, out, "");
		out.flush();
		out.close();
	}
	
	public void print_sare_language_acm_AccessControlModel(sare.language.acm.AccessControlModel element, String outertab, java.io.PrintWriter out) {
		String localtab = outertab;
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		java.util.Map<String, Integer> printCountingMap = new java.util.LinkedHashMap<String, Integer>(14);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(sare.language.acm.AcmPackage.ACCESS_CONTROL_MODEL__EANNOTATIONS));
		printCountingMap.put("eAnnotations", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		temp = element.eGet(element.eClass().getEStructuralFeature(sare.language.acm.AcmPackage.ACCESS_CONTROL_MODEL__OWNED_COMMENT));
		printCountingMap.put("ownedComment", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		temp = element.eGet(element.eClass().getEStructuralFeature(sare.language.acm.AcmPackage.ACCESS_CONTROL_MODEL__NAME));
		printCountingMap.put("name", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(sare.language.acm.AcmPackage.ACCESS_CONTROL_MODEL__VISIBILITY));
		printCountingMap.put("visibility", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(sare.language.acm.AcmPackage.ACCESS_CONTROL_MODEL__CLIENT_DEPENDENCY));
		printCountingMap.put("clientDependency", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		temp = element.eGet(element.eClass().getEStructuralFeature(sare.language.acm.AcmPackage.ACCESS_CONTROL_MODEL__NAME_EXPRESSION));
		printCountingMap.put("nameExpression", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(sare.language.acm.AcmPackage.ACCESS_CONTROL_MODEL__OWNING_TEMPLATE_PARAMETER));
		printCountingMap.put("owningTemplateParameter", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(sare.language.acm.AcmPackage.ACCESS_CONTROL_MODEL__TEMPLATE_PARAMETER));
		printCountingMap.put("templateParameter", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(sare.language.acm.AcmPackage.ACCESS_CONTROL_MODEL__ELEMENTS));
		printCountingMap.put("elements", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		temp = element.eGet(element.eClass().getEStructuralFeature(sare.language.acm.AcmPackage.ACCESS_CONTROL_MODEL__IMPORTS));
		printCountingMap.put("imports", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		// print collected hidden tokens
		int count;
		boolean iterate = true;
		java.io.StringWriter sWriter = null;
		java.io.PrintWriter out1 = null;
		java.util.Map<String, Integer> printCountingMap1 = null;
		// DEFINITION PART BEGINS (CompoundDefinition)
		iterate = true;
		while (iterate) {
			sWriter = new java.io.StringWriter();
			out1 = new java.io.PrintWriter(sWriter);
			printCountingMap1 = new java.util.LinkedHashMap<String, Integer>(printCountingMap);
			print_sare_language_acm_AccessControlModel_0(element, localtab, out1, printCountingMap1);
			if (printCountingMap.equals(printCountingMap1)) {
				iterate = false;
				out1.close();
			} else {
				out1.flush();
				out1.close();
				out.print(sWriter.toString());
				printCountingMap.putAll(printCountingMap1);
			}
		}
		// DEFINITION PART BEGINS (CsString)
		out.print("policy");
		out.print(" ");
		// DEFINITION PART BEGINS (PlaceholderUsingDefaultToken)
		count = printCountingMap.get("name");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(sare.language.acm.AcmPackage.ACCESS_CONTROL_MODEL__NAME));
			if (o != null) {
				sare.language.acm.resource.acm.IAcmTokenResolver resolver = tokenResolverFactory.createTokenResolver("TEXT");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve((Object) o, element.eClass().getEStructuralFeature(sare.language.acm.AcmPackage.ACCESS_CONTROL_MODEL__NAME), element));
				out.print(" ");
			}
			printCountingMap.put("name", count - 1);
		}
		// DEFINITION PART BEGINS (CsString)
		out.print("{");
		out.print(" ");
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("elements");
		if (count > 0) {
			java.util.List<?> list = (java.util.List<?>)element.eGet(element.eClass().getEStructuralFeature(sare.language.acm.AcmPackage.ACCESS_CONTROL_MODEL__ELEMENTS));
			int index  = list.size() - count;
			if (index < 0) {
				index = 0;
			}
			java.util.ListIterator<?> it  = list.listIterator(index);
			while (it.hasNext()) {
				Object o = it.next();
				doPrint((org.eclipse.emf.ecore.EObject) o, out, localtab);
			}
			printCountingMap.put("elements", 0);
		}
		// DEFINITION PART BEGINS (CsString)
		out.print("}");
		out.print(" ");
	}
	
	public void print_sare_language_acm_AccessControlModel_0(sare.language.acm.AccessControlModel element, String outertab, java.io.PrintWriter out, java.util.Map<String, Integer> printCountingMap) {
		int count;
		// DEFINITION PART BEGINS (CsString)
		out.print("import");
		out.print(" ");
		// DEFINITION PART BEGINS (PlaceholderInQuotes)
		count = printCountingMap.get("imports");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(sare.language.acm.AcmPackage.ACCESS_CONTROL_MODEL__IMPORTS));
			java.util.List<?> list = (java.util.List<?>) o;
			int index = list.size() - count;
			if (index >= 0) {
				o = list.get(index);
			} else {
				o = null;
			}
			if (o != null) {
				sare.language.acm.resource.acm.IAcmTokenResolver resolver = tokenResolverFactory.createTokenResolver("QUOTED_34_34");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getAccessControlModelImportsReferenceResolver().deResolve((org.eclipse.emf.ecore.EObject) o, element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(sare.language.acm.AcmPackage.ACCESS_CONTROL_MODEL__IMPORTS)), element.eClass().getEStructuralFeature(sare.language.acm.AcmPackage.ACCESS_CONTROL_MODEL__IMPORTS), element));
				out.print(" ");
			}
			printCountingMap.put("imports", count - 1);
		}
	}
	
	
	public void print_sare_language_acm_Role(sare.language.acm.Role element, String outertab, java.io.PrintWriter out) {
		String localtab = outertab;
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		java.util.Map<String, Integer> printCountingMap = new java.util.LinkedHashMap<String, Integer>(3);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(sare.language.acm.AcmPackage.ROLE__NAME));
		printCountingMap.put("name", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(sare.language.acm.AcmPackage.ROLE__PARENT_ROLES));
		printCountingMap.put("parentRoles", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		temp = element.eGet(element.eClass().getEStructuralFeature(sare.language.acm.AcmPackage.ROLE__PERMISSIONS));
		printCountingMap.put("permissions", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		// print collected hidden tokens
		int count;
		java.io.StringWriter sWriter = null;
		java.io.PrintWriter out1 = null;
		java.util.Map<String, Integer> printCountingMap1 = null;
		// DEFINITION PART BEGINS (CsString)
		out.print("role");
		out.print(" ");
		// DEFINITION PART BEGINS (PlaceholderUsingDefaultToken)
		count = printCountingMap.get("name");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(sare.language.acm.AcmPackage.ROLE__NAME));
			if (o != null) {
				sare.language.acm.resource.acm.IAcmTokenResolver resolver = tokenResolverFactory.createTokenResolver("TEXT");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve((Object) o, element.eClass().getEStructuralFeature(sare.language.acm.AcmPackage.ROLE__NAME), element));
				out.print(" ");
			}
			printCountingMap.put("name", count - 1);
		}
		// DEFINITION PART BEGINS (CompoundDefinition)
		sWriter = new java.io.StringWriter();
		out1 = new java.io.PrintWriter(sWriter);
		printCountingMap1 = new java.util.LinkedHashMap<String, Integer>(printCountingMap);
		print_sare_language_acm_Role_0(element, localtab, out1, printCountingMap1);
		if (printCountingMap.equals(printCountingMap1)) {
			out1.close();
		} else {
			out1.flush();
			out1.close();
			out.print(sWriter.toString());
			printCountingMap.putAll(printCountingMap1);
		}
		// DEFINITION PART BEGINS (CsString)
		out.print("{");
		out.print(" ");
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("permissions");
		if (count > 0) {
			java.util.List<?> list = (java.util.List<?>)element.eGet(element.eClass().getEStructuralFeature(sare.language.acm.AcmPackage.ROLE__PERMISSIONS));
			int index  = list.size() - count;
			if (index < 0) {
				index = 0;
			}
			java.util.ListIterator<?> it  = list.listIterator(index);
			while (it.hasNext()) {
				Object o = it.next();
				doPrint((org.eclipse.emf.ecore.EObject) o, out, localtab);
			}
			printCountingMap.put("permissions", 0);
		}
		// DEFINITION PART BEGINS (CsString)
		out.print("}");
		out.print(" ");
	}
	
	public void print_sare_language_acm_Role_0(sare.language.acm.Role element, String outertab, java.io.PrintWriter out, java.util.Map<String, Integer> printCountingMap) {
		String localtab = outertab;
		int count;
		boolean iterate = true;
		java.io.StringWriter sWriter = null;
		java.io.PrintWriter out1 = null;
		java.util.Map<String, Integer> printCountingMap1 = null;
		// DEFINITION PART BEGINS (CsString)
		out.print("extends");
		out.print(" ");
		// DEFINITION PART BEGINS (PlaceholderUsingDefaultToken)
		count = printCountingMap.get("parentRoles");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(sare.language.acm.AcmPackage.ROLE__PARENT_ROLES));
			java.util.List<?> list = (java.util.List<?>) o;
			int index = list.size() - count;
			if (index >= 0) {
				o = list.get(index);
			} else {
				o = null;
			}
			if (o != null) {
				sare.language.acm.resource.acm.IAcmTokenResolver resolver = tokenResolverFactory.createTokenResolver("TEXT");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getRoleParentRolesReferenceResolver().deResolve((sare.language.acm.Role) o, element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(sare.language.acm.AcmPackage.ROLE__PARENT_ROLES)), element.eClass().getEStructuralFeature(sare.language.acm.AcmPackage.ROLE__PARENT_ROLES), element));
				out.print(" ");
			}
			printCountingMap.put("parentRoles", count - 1);
		}
		// DEFINITION PART BEGINS (CompoundDefinition)
		iterate = true;
		while (iterate) {
			sWriter = new java.io.StringWriter();
			out1 = new java.io.PrintWriter(sWriter);
			printCountingMap1 = new java.util.LinkedHashMap<String, Integer>(printCountingMap);
			print_sare_language_acm_Role_0_0(element, localtab, out1, printCountingMap1);
			if (printCountingMap.equals(printCountingMap1)) {
				iterate = false;
				out1.close();
			} else {
				out1.flush();
				out1.close();
				out.print(sWriter.toString());
				printCountingMap.putAll(printCountingMap1);
			}
		}
	}
	
	public void print_sare_language_acm_Role_0_0(sare.language.acm.Role element, String outertab, java.io.PrintWriter out, java.util.Map<String, Integer> printCountingMap) {
		int count;
		// DEFINITION PART BEGINS (CsString)
		out.print(",");
		out.print(" ");
		// DEFINITION PART BEGINS (PlaceholderUsingDefaultToken)
		count = printCountingMap.get("parentRoles");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(sare.language.acm.AcmPackage.ROLE__PARENT_ROLES));
			java.util.List<?> list = (java.util.List<?>) o;
			int index = list.size() - count;
			if (index >= 0) {
				o = list.get(index);
			} else {
				o = null;
			}
			if (o != null) {
				sare.language.acm.resource.acm.IAcmTokenResolver resolver = tokenResolverFactory.createTokenResolver("TEXT");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getRoleParentRolesReferenceResolver().deResolve((sare.language.acm.Role) o, element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(sare.language.acm.AcmPackage.ROLE__PARENT_ROLES)), element.eClass().getEStructuralFeature(sare.language.acm.AcmPackage.ROLE__PARENT_ROLES), element));
				out.print(" ");
			}
			printCountingMap.put("parentRoles", count - 1);
		}
	}
	
	
	public void print_sare_language_acm_User(sare.language.acm.User element, String outertab, java.io.PrintWriter out) {
		String localtab = outertab;
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		java.util.Map<String, Integer> printCountingMap = new java.util.LinkedHashMap<String, Integer>(2);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(sare.language.acm.AcmPackage.USER__NAME));
		printCountingMap.put("name", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(sare.language.acm.AcmPackage.USER__ROLES));
		printCountingMap.put("roles", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		// print collected hidden tokens
		int count;
		boolean iterate = true;
		java.io.StringWriter sWriter = null;
		java.io.PrintWriter out1 = null;
		java.util.Map<String, Integer> printCountingMap1 = null;
		// DEFINITION PART BEGINS (CsString)
		out.print("user");
		out.print(" ");
		// DEFINITION PART BEGINS (PlaceholderUsingDefaultToken)
		count = printCountingMap.get("name");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(sare.language.acm.AcmPackage.USER__NAME));
			if (o != null) {
				sare.language.acm.resource.acm.IAcmTokenResolver resolver = tokenResolverFactory.createTokenResolver("TEXT");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve((Object) o, element.eClass().getEStructuralFeature(sare.language.acm.AcmPackage.USER__NAME), element));
				out.print(" ");
			}
			printCountingMap.put("name", count - 1);
		}
		// DEFINITION PART BEGINS (CsString)
		out.print("hasRoles");
		out.print(" ");
		// DEFINITION PART BEGINS (CompoundDefinition)
		iterate = true;
		while (iterate) {
			sWriter = new java.io.StringWriter();
			out1 = new java.io.PrintWriter(sWriter);
			printCountingMap1 = new java.util.LinkedHashMap<String, Integer>(printCountingMap);
			print_sare_language_acm_User_0(element, localtab, out1, printCountingMap1);
			if (printCountingMap.equals(printCountingMap1)) {
				iterate = false;
				out1.close();
			} else {
				out1.flush();
				out1.close();
				out.print(sWriter.toString());
				printCountingMap.putAll(printCountingMap1);
			}
		}
	}
	
	public void print_sare_language_acm_User_0(sare.language.acm.User element, String outertab, java.io.PrintWriter out, java.util.Map<String, Integer> printCountingMap) {
		int count;
		// DEFINITION PART BEGINS (CsString)
		out.print(",");
		out.print(" ");
		// DEFINITION PART BEGINS (PlaceholderUsingDefaultToken)
		count = printCountingMap.get("roles");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(sare.language.acm.AcmPackage.USER__ROLES));
			java.util.List<?> list = (java.util.List<?>) o;
			int index = list.size() - count;
			if (index >= 0) {
				o = list.get(index);
			} else {
				o = null;
			}
			if (o != null) {
				sare.language.acm.resource.acm.IAcmTokenResolver resolver = tokenResolverFactory.createTokenResolver("TEXT");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getUserRolesReferenceResolver().deResolve((sare.language.acm.Role) o, element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(sare.language.acm.AcmPackage.USER__ROLES)), element.eClass().getEStructuralFeature(sare.language.acm.AcmPackage.USER__ROLES), element));
				out.print(" ");
			}
			printCountingMap.put("roles", count - 1);
		}
	}
	
	
	public void print_sare_language_acm_Permission(sare.language.acm.Permission element, String outertab, java.io.PrintWriter out) {
		String localtab = outertab;
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		java.util.Map<String, Integer> printCountingMap = new java.util.LinkedHashMap<String, Integer>(3);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(sare.language.acm.AcmPackage.PERMISSION__CONSTRAINT));
		printCountingMap.put("constraint", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(sare.language.acm.AcmPackage.PERMISSION__ELEMENT));
		printCountingMap.put("element", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(sare.language.acm.AcmPackage.PERMISSION__CHILD_PERMISSIONS));
		printCountingMap.put("childPermissions", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		// print collected hidden tokens
		java.io.StringWriter sWriter = null;
		java.io.PrintWriter out1 = null;
		java.util.Map<String, Integer> printCountingMap1 = null;
		// DEFINITION PART BEGINS (CompoundDefinition)
		print_sare_language_acm_Permission_0(element, localtab, out, printCountingMap);
		// DEFINITION PART BEGINS (CompoundDefinition)
		sWriter = new java.io.StringWriter();
		out1 = new java.io.PrintWriter(sWriter);
		printCountingMap1 = new java.util.LinkedHashMap<String, Integer>(printCountingMap);
		print_sare_language_acm_Permission_1(element, localtab, out1, printCountingMap1);
		if (printCountingMap.equals(printCountingMap1)) {
			out1.close();
		} else {
			out1.flush();
			out1.close();
			out.print(sWriter.toString());
			printCountingMap.putAll(printCountingMap1);
		}
		// DEFINITION PART BEGINS (CompoundDefinition)
		sWriter = new java.io.StringWriter();
		out1 = new java.io.PrintWriter(sWriter);
		printCountingMap1 = new java.util.LinkedHashMap<String, Integer>(printCountingMap);
		print_sare_language_acm_Permission_2(element, localtab, out1, printCountingMap1);
		if (printCountingMap.equals(printCountingMap1)) {
			out1.close();
		} else {
			out1.flush();
			out1.close();
			out.print(sWriter.toString());
			printCountingMap.putAll(printCountingMap1);
		}
	}
	
	public void print_sare_language_acm_Permission_0(sare.language.acm.Permission element, String outertab, java.io.PrintWriter out, java.util.Map<String, Integer> printCountingMap) {
		int count;
		int alt = -1;
		alt = 0;
		int matches = 		matchCount(printCountingMap, java.util.Arrays.asList(		"element"		));
		int tempMatchCount;
		tempMatchCount = 		matchCount(printCountingMap, java.util.Arrays.asList(		"element"		));
		if (tempMatchCount > matches) {
			alt = 1;
			matches = tempMatchCount;
		}
		switch(alt) {
			case 1:			{
				// DEFINITION PART BEGINS (PlaceholderUsingSpecifiedToken)
				count = printCountingMap.get("element");
				if (count > 0) {
					Object o = element.eGet(element.eClass().getEStructuralFeature(sare.language.acm.AcmPackage.PERMISSION__ELEMENT));
					if (o != null) {
						sare.language.acm.resource.acm.IAcmTokenResolver resolver = tokenResolverFactory.createTokenResolver("TEXT");
						resolver.setOptions(getOptions());
						out.print(resolver.deResolve(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getPermissionElementReferenceResolver().deResolve((org.eclipse.uml2.uml.NamedElement) o, element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(sare.language.acm.AcmPackage.PERMISSION__ELEMENT)), element.eClass().getEStructuralFeature(sare.language.acm.AcmPackage.PERMISSION__ELEMENT), element));
						out.print(" ");
					}
					printCountingMap.put("element", count - 1);
				}
			}
			break;
			default:			// DEFINITION PART BEGINS (PlaceholderUsingSpecifiedToken)
			count = printCountingMap.get("element");
			if (count > 0) {
				Object o = element.eGet(element.eClass().getEStructuralFeature(sare.language.acm.AcmPackage.PERMISSION__ELEMENT));
				if (o != null) {
					sare.language.acm.resource.acm.IAcmTokenResolver resolver = tokenResolverFactory.createTokenResolver("NAMED_ELEMENT_ID");
					resolver.setOptions(getOptions());
					out.print(resolver.deResolve(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getPermissionElementReferenceResolver().deResolve((org.eclipse.uml2.uml.NamedElement) o, element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(sare.language.acm.AcmPackage.PERMISSION__ELEMENT)), element.eClass().getEStructuralFeature(sare.language.acm.AcmPackage.PERMISSION__ELEMENT), element));
					out.print(" ");
				}
				printCountingMap.put("element", count - 1);
			}
		}
	}
	
	public void print_sare_language_acm_Permission_1(sare.language.acm.Permission element, String outertab, java.io.PrintWriter out, java.util.Map<String, Integer> printCountingMap) {
		int count;
		// DEFINITION PART BEGINS (CsString)
		out.print("if");
		out.print(" ");
		// DEFINITION PART BEGINS (PlaceholderUsingDefaultToken)
		count = printCountingMap.get("constraint");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(sare.language.acm.AcmPackage.PERMISSION__CONSTRAINT));
			if (o != null) {
				sare.language.acm.resource.acm.IAcmTokenResolver resolver = tokenResolverFactory.createTokenResolver("TEXT");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve((Object) o, element.eClass().getEStructuralFeature(sare.language.acm.AcmPackage.PERMISSION__CONSTRAINT), element));
				out.print(" ");
			}
			printCountingMap.put("constraint", count - 1);
		}
	}
	
	public void print_sare_language_acm_Permission_2(sare.language.acm.Permission element, String outertab, java.io.PrintWriter out, java.util.Map<String, Integer> printCountingMap) {
		String localtab = outertab;
		int count;
		// DEFINITION PART BEGINS (CsString)
		out.print("{");
		out.print(" ");
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("childPermissions");
		if (count > 0) {
			java.util.List<?> list = (java.util.List<?>)element.eGet(element.eClass().getEStructuralFeature(sare.language.acm.AcmPackage.PERMISSION__CHILD_PERMISSIONS));
			int index  = list.size() - count;
			if (index < 0) {
				index = 0;
			}
			java.util.ListIterator<?> it  = list.listIterator(index);
			while (it.hasNext()) {
				Object o = it.next();
				doPrint((org.eclipse.emf.ecore.EObject) o, out, localtab);
			}
			printCountingMap.put("childPermissions", 0);
		}
		// DEFINITION PART BEGINS (CsString)
		out.print("}");
		out.print(" ");
	}
	
	
	public void print_org_eclipse_uml2_uml_Model(org.eclipse.uml2.uml.Model element, String outertab, java.io.PrintWriter out) {
		String localtab = outertab;
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		java.util.Map<String, Integer> printCountingMap = new java.util.LinkedHashMap<String, Integer>(27);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.MODEL__EANNOTATIONS));
		printCountingMap.put("eAnnotations", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.MODEL__OWNED_COMMENT));
		printCountingMap.put("ownedComment", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.MODEL__NAME));
		printCountingMap.put("name", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.MODEL__VISIBILITY));
		printCountingMap.put("visibility", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.MODEL__CLIENT_DEPENDENCY));
		printCountingMap.put("clientDependency", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.MODEL__NAME_EXPRESSION));
		printCountingMap.put("nameExpression", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.MODEL__ELEMENT_IMPORT));
		printCountingMap.put("elementImport", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.MODEL__PACKAGE_IMPORT));
		printCountingMap.put("packageImport", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.MODEL__OWNED_RULE));
		printCountingMap.put("ownedRule", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.MODEL__OWNING_TEMPLATE_PARAMETER));
		printCountingMap.put("owningTemplateParameter", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.MODEL__TEMPLATE_PARAMETER));
		printCountingMap.put("templateParameter", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.MODEL__TEMPLATE_BINDING));
		printCountingMap.put("templateBinding", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.MODEL__OWNED_TEMPLATE_SIGNATURE));
		printCountingMap.put("ownedTemplateSignature", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.MODEL__PACKAGE_MERGE));
		printCountingMap.put("packageMerge", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.MODEL__PACKAGED_ELEMENT));
		printCountingMap.put("packagedElement", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.MODEL__PROFILE_APPLICATION));
		printCountingMap.put("profileApplication", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.MODEL__VIEWPOINT));
		printCountingMap.put("viewpoint", temp == null ? 0 : 1);
		// print collected hidden tokens
		int count;
		boolean iterate = true;
		java.io.StringWriter sWriter = null;
		java.io.PrintWriter out1 = null;
		java.util.Map<String, Integer> printCountingMap1 = null;
		// DEFINITION PART BEGINS (CompoundDefinition)
		iterate = true;
		while (iterate) {
			sWriter = new java.io.StringWriter();
			out1 = new java.io.PrintWriter(sWriter);
			printCountingMap1 = new java.util.LinkedHashMap<String, Integer>(printCountingMap);
			print_org_eclipse_uml2_uml_Model_0(element, localtab, out1, printCountingMap1);
			if (printCountingMap.equals(printCountingMap1)) {
				iterate = false;
				out1.close();
			} else {
				out1.flush();
				out1.close();
				out.print(sWriter.toString());
				printCountingMap.putAll(printCountingMap1);
			}
		}
		// DEFINITION PART BEGINS (CsString)
		out.print("model");
		out.print(" ");
		// DEFINITION PART BEGINS (PlaceholderUsingDefaultToken)
		count = printCountingMap.get("name");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.MODEL__NAME));
			if (o != null) {
				sare.language.acm.resource.acm.IAcmTokenResolver resolver = tokenResolverFactory.createTokenResolver("TEXT");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve((Object) o, element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.MODEL__NAME), element));
				out.print(" ");
			}
			printCountingMap.put("name", count - 1);
		}
		// DEFINITION PART BEGINS (CsString)
		out.print("{");
		out.print(" ");
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("packagedElement");
		if (count > 0) {
			java.util.List<?> list = (java.util.List<?>)element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.MODEL__PACKAGED_ELEMENT));
			int index  = list.size() - count;
			if (index < 0) {
				index = 0;
			}
			java.util.ListIterator<?> it  = list.listIterator(index);
			while (it.hasNext()) {
				Object o = it.next();
				doPrint((org.eclipse.emf.ecore.EObject) o, out, localtab);
			}
			printCountingMap.put("packagedElement", 0);
		}
		// DEFINITION PART BEGINS (CsString)
		out.print("}");
		out.print(" ");
	}
	
	public void print_org_eclipse_uml2_uml_Model_0(org.eclipse.uml2.uml.Model element, String outertab, java.io.PrintWriter out, java.util.Map<String, Integer> printCountingMap) {
		String localtab = outertab;
		int count;
		// DEFINITION PART BEGINS (CsString)
		out.print("import");
		out.print(" ");
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("packageImport");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.MODEL__PACKAGE_IMPORT));
			java.util.List<?> list = (java.util.List<?>) o;
			int index = list.size() - count;
			if (index >= 0) {
				o = list.get(index);
			} else {
				o = null;
			}
			if (o != null) {
				doPrint((org.eclipse.emf.ecore.EObject) o, out, localtab);
			}
			printCountingMap.put("packageImport", count - 1);
		}
	}
	
	
	public void print_org_eclipse_uml2_uml_PackageImport(org.eclipse.uml2.uml.PackageImport element, String outertab, java.io.PrintWriter out) {
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		java.util.Map<String, Integer> printCountingMap = new java.util.LinkedHashMap<String, Integer>(10);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.PACKAGE_IMPORT__EANNOTATIONS));
		printCountingMap.put("eAnnotations", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.PACKAGE_IMPORT__OWNED_COMMENT));
		printCountingMap.put("ownedComment", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.PACKAGE_IMPORT__VISIBILITY));
		printCountingMap.put("visibility", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.PACKAGE_IMPORT__IMPORTED_PACKAGE));
		printCountingMap.put("importedPackage", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.PACKAGE_IMPORT__IMPORTING_NAMESPACE));
		printCountingMap.put("importingNamespace", temp == null ? 0 : 1);
		// print collected hidden tokens
		int count;
		// DEFINITION PART BEGINS (PlaceholderInQuotes)
		count = printCountingMap.get("importedPackage");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.PACKAGE_IMPORT__IMPORTED_PACKAGE));
			if (o != null) {
				sare.language.acm.resource.acm.IAcmTokenResolver resolver = tokenResolverFactory.createTokenResolver("QUOTED_34_34");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getPackageImportImportedPackageReferenceResolver().deResolve((org.eclipse.uml2.uml.Package) o, element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.PACKAGE_IMPORT__IMPORTED_PACKAGE)), element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.PACKAGE_IMPORT__IMPORTED_PACKAGE), element));
				out.print(" ");
			}
			printCountingMap.put("importedPackage", count - 1);
		}
	}
	
	
	public void print_org_eclipse_uml2_uml_Package(org.eclipse.uml2.uml.Package element, String outertab, java.io.PrintWriter out) {
		String localtab = outertab;
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		java.util.Map<String, Integer> printCountingMap = new java.util.LinkedHashMap<String, Integer>(26);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.PACKAGE__EANNOTATIONS));
		printCountingMap.put("eAnnotations", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.PACKAGE__OWNED_COMMENT));
		printCountingMap.put("ownedComment", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.PACKAGE__NAME));
		printCountingMap.put("name", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.PACKAGE__VISIBILITY));
		printCountingMap.put("visibility", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.PACKAGE__CLIENT_DEPENDENCY));
		printCountingMap.put("clientDependency", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.PACKAGE__NAME_EXPRESSION));
		printCountingMap.put("nameExpression", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.PACKAGE__ELEMENT_IMPORT));
		printCountingMap.put("elementImport", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.PACKAGE__PACKAGE_IMPORT));
		printCountingMap.put("packageImport", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.PACKAGE__OWNED_RULE));
		printCountingMap.put("ownedRule", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.PACKAGE__OWNING_TEMPLATE_PARAMETER));
		printCountingMap.put("owningTemplateParameter", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.PACKAGE__TEMPLATE_PARAMETER));
		printCountingMap.put("templateParameter", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.PACKAGE__TEMPLATE_BINDING));
		printCountingMap.put("templateBinding", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.PACKAGE__OWNED_TEMPLATE_SIGNATURE));
		printCountingMap.put("ownedTemplateSignature", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.PACKAGE__PACKAGE_MERGE));
		printCountingMap.put("packageMerge", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.PACKAGE__PACKAGED_ELEMENT));
		printCountingMap.put("packagedElement", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.PACKAGE__PROFILE_APPLICATION));
		printCountingMap.put("profileApplication", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		// print collected hidden tokens
		int count;
		// DEFINITION PART BEGINS (CsString)
		out.print("package");
		out.print(" ");
		// DEFINITION PART BEGINS (PlaceholderUsingDefaultToken)
		count = printCountingMap.get("name");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.PACKAGE__NAME));
			if (o != null) {
				sare.language.acm.resource.acm.IAcmTokenResolver resolver = tokenResolverFactory.createTokenResolver("TEXT");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve((Object) o, element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.PACKAGE__NAME), element));
				out.print(" ");
			}
			printCountingMap.put("name", count - 1);
		}
		// DEFINITION PART BEGINS (CsString)
		out.print("{");
		out.print(" ");
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("packagedElement");
		if (count > 0) {
			java.util.List<?> list = (java.util.List<?>)element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.PACKAGE__PACKAGED_ELEMENT));
			int index  = list.size() - count;
			if (index < 0) {
				index = 0;
			}
			java.util.ListIterator<?> it  = list.listIterator(index);
			while (it.hasNext()) {
				Object o = it.next();
				doPrint((org.eclipse.emf.ecore.EObject) o, out, localtab);
			}
			printCountingMap.put("packagedElement", 0);
		}
		// DEFINITION PART BEGINS (CsString)
		out.print("}");
		out.print(" ");
	}
	
	
	public void print_org_eclipse_uml2_uml_Class(org.eclipse.uml2.uml.Class element, String outertab, java.io.PrintWriter out) {
		String localtab = outertab;
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		java.util.Map<String, Integer> printCountingMap = new java.util.LinkedHashMap<String, Integer>(52);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.CLASS__EANNOTATIONS));
		printCountingMap.put("eAnnotations", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.CLASS__OWNED_COMMENT));
		printCountingMap.put("ownedComment", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.CLASS__NAME));
		printCountingMap.put("name", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.CLASS__VISIBILITY));
		printCountingMap.put("visibility", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.CLASS__CLIENT_DEPENDENCY));
		printCountingMap.put("clientDependency", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.CLASS__NAME_EXPRESSION));
		printCountingMap.put("nameExpression", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.CLASS__ELEMENT_IMPORT));
		printCountingMap.put("elementImport", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.CLASS__PACKAGE_IMPORT));
		printCountingMap.put("packageImport", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.CLASS__OWNED_RULE));
		printCountingMap.put("ownedRule", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.CLASS__IS_LEAF));
		printCountingMap.put("isLeaf", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.CLASS__OWNING_TEMPLATE_PARAMETER));
		printCountingMap.put("owningTemplateParameter", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.CLASS__TEMPLATE_PARAMETER));
		printCountingMap.put("templateParameter", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.CLASS__TEMPLATE_BINDING));
		printCountingMap.put("templateBinding", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.CLASS__OWNED_TEMPLATE_SIGNATURE));
		printCountingMap.put("ownedTemplateSignature", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.CLASS__IS_ABSTRACT));
		printCountingMap.put("isAbstract", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.CLASS__GENERALIZATION));
		printCountingMap.put("generalization", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.CLASS__POWERTYPE_EXTENT));
		printCountingMap.put("powertypeExtent", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.CLASS__REDEFINED_CLASSIFIER));
		printCountingMap.put("redefinedClassifier", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.CLASS__SUBSTITUTION));
		printCountingMap.put("substitution", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.CLASS__REPRESENTATION));
		printCountingMap.put("representation", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.CLASS__COLLABORATION_USE));
		printCountingMap.put("collaborationUse", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.CLASS__OWNED_USE_CASE));
		printCountingMap.put("ownedUseCase", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.CLASS__USE_CASE));
		printCountingMap.put("useCase", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.CLASS__OWNED_ATTRIBUTE));
		printCountingMap.put("ownedAttribute", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.CLASS__OWNED_CONNECTOR));
		printCountingMap.put("ownedConnector", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.CLASS__OWNED_BEHAVIOR));
		printCountingMap.put("ownedBehavior", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.CLASS__CLASSIFIER_BEHAVIOR));
		printCountingMap.put("classifierBehavior", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.CLASS__INTERFACE_REALIZATION));
		printCountingMap.put("interfaceRealization", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.CLASS__OWNED_TRIGGER));
		printCountingMap.put("ownedTrigger", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.CLASS__NESTED_CLASSIFIER));
		printCountingMap.put("nestedClassifier", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.CLASS__OWNED_OPERATION));
		printCountingMap.put("ownedOperation", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.CLASS__IS_ACTIVE));
		printCountingMap.put("isActive", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.CLASS__OWNED_RECEPTION));
		printCountingMap.put("ownedReception", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		// print collected hidden tokens
		int count;
		boolean iterate = true;
		java.io.StringWriter sWriter = null;
		java.io.PrintWriter out1 = null;
		java.util.Map<String, Integer> printCountingMap1 = null;
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("ownedComment");
		if (count > 0) {
			java.util.List<?> list = (java.util.List<?>)element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.CLASS__OWNED_COMMENT));
			int index  = list.size() - count;
			if (index < 0) {
				index = 0;
			}
			java.util.ListIterator<?> it  = list.listIterator(index);
			while (it.hasNext()) {
				Object o = it.next();
				doPrint((org.eclipse.emf.ecore.EObject) o, out, localtab);
			}
			printCountingMap.put("ownedComment", 0);
		}
		// DEFINITION PART BEGINS (CsString)
		out.print("class");
		out.print(" ");
		// DEFINITION PART BEGINS (PlaceholderUsingDefaultToken)
		count = printCountingMap.get("name");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.CLASS__NAME));
			if (o != null) {
				sare.language.acm.resource.acm.IAcmTokenResolver resolver = tokenResolverFactory.createTokenResolver("TEXT");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve((Object) o, element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.CLASS__NAME), element));
				out.print(" ");
			}
			printCountingMap.put("name", count - 1);
		}
		// DEFINITION PART BEGINS (CompoundDefinition)
		sWriter = new java.io.StringWriter();
		out1 = new java.io.PrintWriter(sWriter);
		printCountingMap1 = new java.util.LinkedHashMap<String, Integer>(printCountingMap);
		print_org_eclipse_uml2_uml_Class_0(element, localtab, out1, printCountingMap1);
		if (printCountingMap.equals(printCountingMap1)) {
			out1.close();
		} else {
			out1.flush();
			out1.close();
			out.print(sWriter.toString());
			printCountingMap.putAll(printCountingMap1);
		}
		// DEFINITION PART BEGINS (CsString)
		out.print("{");
		out.print(" ");
		// DEFINITION PART BEGINS (CompoundDefinition)
		iterate = true;
		while (iterate) {
			sWriter = new java.io.StringWriter();
			out1 = new java.io.PrintWriter(sWriter);
			printCountingMap1 = new java.util.LinkedHashMap<String, Integer>(printCountingMap);
			print_org_eclipse_uml2_uml_Class_1(element, localtab, out1, printCountingMap1);
			if (printCountingMap.equals(printCountingMap1)) {
				iterate = false;
				out1.close();
			} else {
				out1.flush();
				out1.close();
				out.print(sWriter.toString());
				printCountingMap.putAll(printCountingMap1);
			}
		}
		// DEFINITION PART BEGINS (CsString)
		out.print("}");
		out.print(" ");
	}
	
	public void print_org_eclipse_uml2_uml_Class_0(org.eclipse.uml2.uml.Class element, String outertab, java.io.PrintWriter out, java.util.Map<String, Integer> printCountingMap) {
		String localtab = outertab;
		int count;
		boolean iterate = true;
		java.io.StringWriter sWriter = null;
		java.io.PrintWriter out1 = null;
		java.util.Map<String, Integer> printCountingMap1 = null;
		// DEFINITION PART BEGINS (CsString)
		out.print("extends");
		out.print(" ");
		// DEFINITION PART BEGINS (PlaceholderUsingDefaultToken)
		count = printCountingMap.get("superClass");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.CLASS__SUPER_CLASS));
			java.util.List<?> list = (java.util.List<?>) o;
			int index = list.size() - count;
			if (index >= 0) {
				o = list.get(index);
			} else {
				o = null;
			}
			if (o != null) {
				sare.language.acm.resource.acm.IAcmTokenResolver resolver = tokenResolverFactory.createTokenResolver("TEXT");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getClassSuperClassReferenceResolver().deResolve((org.eclipse.uml2.uml.Class) o, element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.CLASS__SUPER_CLASS)), element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.CLASS__SUPER_CLASS), element));
				out.print(" ");
			}
			printCountingMap.put("superClass", count - 1);
		}
		// DEFINITION PART BEGINS (CompoundDefinition)
		iterate = true;
		while (iterate) {
			sWriter = new java.io.StringWriter();
			out1 = new java.io.PrintWriter(sWriter);
			printCountingMap1 = new java.util.LinkedHashMap<String, Integer>(printCountingMap);
			print_org_eclipse_uml2_uml_Class_0_0(element, localtab, out1, printCountingMap1);
			if (printCountingMap.equals(printCountingMap1)) {
				iterate = false;
				out1.close();
			} else {
				out1.flush();
				out1.close();
				out.print(sWriter.toString());
				printCountingMap.putAll(printCountingMap1);
			}
		}
	}
	
	public void print_org_eclipse_uml2_uml_Class_0_0(org.eclipse.uml2.uml.Class element, String outertab, java.io.PrintWriter out, java.util.Map<String, Integer> printCountingMap) {
		int count;
		// DEFINITION PART BEGINS (CsString)
		out.print(",");
		out.print(" ");
		// DEFINITION PART BEGINS (PlaceholderUsingDefaultToken)
		count = printCountingMap.get("superClass");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.CLASS__SUPER_CLASS));
			java.util.List<?> list = (java.util.List<?>) o;
			int index = list.size() - count;
			if (index >= 0) {
				o = list.get(index);
			} else {
				o = null;
			}
			if (o != null) {
				sare.language.acm.resource.acm.IAcmTokenResolver resolver = tokenResolverFactory.createTokenResolver("TEXT");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getClassSuperClassReferenceResolver().deResolve((org.eclipse.uml2.uml.Class) o, element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.CLASS__SUPER_CLASS)), element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.CLASS__SUPER_CLASS), element));
				out.print(" ");
			}
			printCountingMap.put("superClass", count - 1);
		}
	}
	
	public void print_org_eclipse_uml2_uml_Class_1(org.eclipse.uml2.uml.Class element, String outertab, java.io.PrintWriter out, java.util.Map<String, Integer> printCountingMap) {
		String localtab = outertab;
		int count;
		int alt = -1;
		alt = 0;
		int matches = 		matchCount(printCountingMap, java.util.Arrays.asList(		"ownedAttribute"		));
		int tempMatchCount;
		tempMatchCount = 		matchCount(printCountingMap, java.util.Arrays.asList(		"ownedOperation"		));
		if (tempMatchCount > matches) {
			alt = 1;
			matches = tempMatchCount;
		}
		switch(alt) {
			case 1:			{
				// DEFINITION PART BEGINS (Containment)
				count = printCountingMap.get("ownedOperation");
				if (count > 0) {
					Object o = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.CLASS__OWNED_OPERATION));
					java.util.List<?> list = (java.util.List<?>) o;
					int index = list.size() - count;
					if (index >= 0) {
						o = list.get(index);
					} else {
						o = null;
					}
					if (o != null) {
						doPrint((org.eclipse.emf.ecore.EObject) o, out, localtab);
					}
					printCountingMap.put("ownedOperation", count - 1);
				}
			}
			break;
			default:			// DEFINITION PART BEGINS (Containment)
			count = printCountingMap.get("ownedAttribute");
			if (count > 0) {
				Object o = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.CLASS__OWNED_ATTRIBUTE));
				java.util.List<?> list = (java.util.List<?>) o;
				int index = list.size() - count;
				if (index >= 0) {
					o = list.get(index);
				} else {
					o = null;
				}
				if (o != null) {
					doPrint((org.eclipse.emf.ecore.EObject) o, out, localtab);
				}
				printCountingMap.put("ownedAttribute", count - 1);
			}
		}
	}
	
	
	public void print_org_eclipse_uml2_uml_Operation(org.eclipse.uml2.uml.Operation element, String outertab, java.io.PrintWriter out) {
		String localtab = outertab;
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		java.util.Map<String, Integer> printCountingMap = new java.util.LinkedHashMap<String, Integer>(44);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.OPERATION__EANNOTATIONS));
		printCountingMap.put("eAnnotations", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.OPERATION__OWNED_COMMENT));
		printCountingMap.put("ownedComment", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.OPERATION__NAME));
		printCountingMap.put("name", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.OPERATION__VISIBILITY));
		printCountingMap.put("visibility", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.OPERATION__CLIENT_DEPENDENCY));
		printCountingMap.put("clientDependency", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.OPERATION__NAME_EXPRESSION));
		printCountingMap.put("nameExpression", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.OPERATION__ELEMENT_IMPORT));
		printCountingMap.put("elementImport", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.OPERATION__PACKAGE_IMPORT));
		printCountingMap.put("packageImport", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.OPERATION__OWNED_RULE));
		printCountingMap.put("ownedRule", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.OPERATION__IS_LEAF));
		printCountingMap.put("isLeaf", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.OPERATION__IS_STATIC));
		printCountingMap.put("isStatic", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.OPERATION__OWNED_PARAMETER));
		printCountingMap.put("ownedParameter", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.OPERATION__IS_ABSTRACT));
		printCountingMap.put("isAbstract", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.OPERATION__METHOD));
		printCountingMap.put("method", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.OPERATION__CONCURRENCY));
		printCountingMap.put("concurrency", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.OPERATION__RAISED_EXCEPTION));
		printCountingMap.put("raisedException", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.OPERATION__OWNED_PARAMETER_SET));
		printCountingMap.put("ownedParameterSet", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.OPERATION__OWNING_TEMPLATE_PARAMETER));
		printCountingMap.put("owningTemplateParameter", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.OPERATION__TEMPLATE_PARAMETER));
		printCountingMap.put("templateParameter", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.OPERATION__TEMPLATE_BINDING));
		printCountingMap.put("templateBinding", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.OPERATION__OWNED_TEMPLATE_SIGNATURE));
		printCountingMap.put("ownedTemplateSignature", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.OPERATION__INTERFACE));
		printCountingMap.put("interface", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.OPERATION__CLASS));
		printCountingMap.put("class", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.OPERATION__IS_QUERY));
		printCountingMap.put("isQuery", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.OPERATION__PRECONDITION));
		printCountingMap.put("precondition", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.OPERATION__POSTCONDITION));
		printCountingMap.put("postcondition", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.OPERATION__REDEFINED_OPERATION));
		printCountingMap.put("redefinedOperation", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.OPERATION__DATATYPE));
		printCountingMap.put("datatype", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.OPERATION__BODY_CONDITION));
		printCountingMap.put("bodyCondition", temp == null ? 0 : 1);
		// print collected hidden tokens
		int count;
		java.io.StringWriter sWriter = null;
		java.io.PrintWriter out1 = null;
		java.util.Map<String, Integer> printCountingMap1 = null;
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("ownedComment");
		if (count > 0) {
			java.util.List<?> list = (java.util.List<?>)element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.OPERATION__OWNED_COMMENT));
			int index  = list.size() - count;
			if (index < 0) {
				index = 0;
			}
			java.util.ListIterator<?> it  = list.listIterator(index);
			while (it.hasNext()) {
				Object o = it.next();
				doPrint((org.eclipse.emf.ecore.EObject) o, out, localtab);
			}
			printCountingMap.put("ownedComment", 0);
		}
		// DEFINITION PART BEGINS (PlaceholderUsingDefaultToken)
		count = printCountingMap.get("name");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.OPERATION__NAME));
			if (o != null) {
				sare.language.acm.resource.acm.IAcmTokenResolver resolver = tokenResolverFactory.createTokenResolver("TEXT");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve((Object) o, element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.OPERATION__NAME), element));
				out.print(" ");
			}
			printCountingMap.put("name", count - 1);
		}
		// DEFINITION PART BEGINS (CsString)
		out.print("(");
		out.print(" ");
		// DEFINITION PART BEGINS (CompoundDefinition)
		sWriter = new java.io.StringWriter();
		out1 = new java.io.PrintWriter(sWriter);
		printCountingMap1 = new java.util.LinkedHashMap<String, Integer>(printCountingMap);
		print_org_eclipse_uml2_uml_Operation_0(element, localtab, out1, printCountingMap1);
		if (printCountingMap.equals(printCountingMap1)) {
			out1.close();
		} else {
			out1.flush();
			out1.close();
			out.print(sWriter.toString());
			printCountingMap.putAll(printCountingMap1);
		}
		// DEFINITION PART BEGINS (CsString)
		out.print(")");
		out.print(" ");
		// DEFINITION PART BEGINS (CompoundDefinition)
		sWriter = new java.io.StringWriter();
		out1 = new java.io.PrintWriter(sWriter);
		printCountingMap1 = new java.util.LinkedHashMap<String, Integer>(printCountingMap);
		print_org_eclipse_uml2_uml_Operation_1(element, localtab, out1, printCountingMap1);
		if (printCountingMap.equals(printCountingMap1)) {
			out1.close();
		} else {
			out1.flush();
			out1.close();
			out.print(sWriter.toString());
			printCountingMap.putAll(printCountingMap1);
		}
	}
	
	public void print_org_eclipse_uml2_uml_Operation_0(org.eclipse.uml2.uml.Operation element, String outertab, java.io.PrintWriter out, java.util.Map<String, Integer> printCountingMap) {
		String localtab = outertab;
		int count;
		boolean iterate = true;
		java.io.StringWriter sWriter = null;
		java.io.PrintWriter out1 = null;
		java.util.Map<String, Integer> printCountingMap1 = null;
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("ownedParameter");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.OPERATION__OWNED_PARAMETER));
			java.util.List<?> list = (java.util.List<?>) o;
			int index = list.size() - count;
			if (index >= 0) {
				o = list.get(index);
			} else {
				o = null;
			}
			if (o != null) {
				doPrint((org.eclipse.emf.ecore.EObject) o, out, localtab);
			}
			printCountingMap.put("ownedParameter", count - 1);
		}
		// DEFINITION PART BEGINS (CompoundDefinition)
		iterate = true;
		while (iterate) {
			sWriter = new java.io.StringWriter();
			out1 = new java.io.PrintWriter(sWriter);
			printCountingMap1 = new java.util.LinkedHashMap<String, Integer>(printCountingMap);
			print_org_eclipse_uml2_uml_Operation_0_0(element, localtab, out1, printCountingMap1);
			if (printCountingMap.equals(printCountingMap1)) {
				iterate = false;
				out1.close();
			} else {
				out1.flush();
				out1.close();
				out.print(sWriter.toString());
				printCountingMap.putAll(printCountingMap1);
			}
		}
	}
	
	public void print_org_eclipse_uml2_uml_Operation_0_0(org.eclipse.uml2.uml.Operation element, String outertab, java.io.PrintWriter out, java.util.Map<String, Integer> printCountingMap) {
		String localtab = outertab;
		int count;
		// DEFINITION PART BEGINS (CsString)
		out.print(",");
		out.print(" ");
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("ownedParameter");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.OPERATION__OWNED_PARAMETER));
			java.util.List<?> list = (java.util.List<?>) o;
			int index = list.size() - count;
			if (index >= 0) {
				o = list.get(index);
			} else {
				o = null;
			}
			if (o != null) {
				doPrint((org.eclipse.emf.ecore.EObject) o, out, localtab);
			}
			printCountingMap.put("ownedParameter", count - 1);
		}
	}
	
	public void print_org_eclipse_uml2_uml_Operation_1(org.eclipse.uml2.uml.Operation element, String outertab, java.io.PrintWriter out, java.util.Map<String, Integer> printCountingMap) {
		int count;
		// DEFINITION PART BEGINS (CsString)
		out.print(":");
		out.print(" ");
		// DEFINITION PART BEGINS (PlaceholderUsingDefaultToken)
		count = printCountingMap.get("type");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.OPERATION__TYPE));
			if (o != null) {
				sare.language.acm.resource.acm.IAcmTokenResolver resolver = tokenResolverFactory.createTokenResolver("TEXT");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getOperationTypeReferenceResolver().deResolve((org.eclipse.uml2.uml.Type) o, element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.OPERATION__TYPE)), element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.OPERATION__TYPE), element));
				out.print(" ");
			}
			printCountingMap.put("type", count - 1);
		}
	}
	
	
	public void print_org_eclipse_uml2_uml_Property(org.eclipse.uml2.uml.Property element, String outertab, java.io.PrintWriter out) {
		String localtab = outertab;
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		java.util.Map<String, Integer> printCountingMap = new java.util.LinkedHashMap<String, Integer>(43);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.PROPERTY__EANNOTATIONS));
		printCountingMap.put("eAnnotations", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.PROPERTY__OWNED_COMMENT));
		printCountingMap.put("ownedComment", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.PROPERTY__NAME));
		printCountingMap.put("name", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.PROPERTY__VISIBILITY));
		printCountingMap.put("visibility", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.PROPERTY__CLIENT_DEPENDENCY));
		printCountingMap.put("clientDependency", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.PROPERTY__NAME_EXPRESSION));
		printCountingMap.put("nameExpression", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.PROPERTY__IS_LEAF));
		printCountingMap.put("isLeaf", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.PROPERTY__IS_STATIC));
		printCountingMap.put("isStatic", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.PROPERTY__TYPE));
		printCountingMap.put("type", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.PROPERTY__IS_ORDERED));
		printCountingMap.put("isOrdered", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.PROPERTY__IS_UNIQUE));
		printCountingMap.put("isUnique", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.PROPERTY__UPPER_VALUE));
		printCountingMap.put("upperValue", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.PROPERTY__LOWER_VALUE));
		printCountingMap.put("lowerValue", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.PROPERTY__IS_READ_ONLY));
		printCountingMap.put("isReadOnly", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.PROPERTY__OWNING_TEMPLATE_PARAMETER));
		printCountingMap.put("owningTemplateParameter", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.PROPERTY__TEMPLATE_PARAMETER));
		printCountingMap.put("templateParameter", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.PROPERTY__DEPLOYMENT));
		printCountingMap.put("deployment", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.PROPERTY__CLASS));
		printCountingMap.put("class", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.PROPERTY__DATATYPE));
		printCountingMap.put("datatype", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.PROPERTY__IS_DERIVED));
		printCountingMap.put("isDerived", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.PROPERTY__IS_DERIVED_UNION));
		printCountingMap.put("isDerivedUnion", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.PROPERTY__AGGREGATION));
		printCountingMap.put("aggregation", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.PROPERTY__REDEFINED_PROPERTY));
		printCountingMap.put("redefinedProperty", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.PROPERTY__OWNING_ASSOCIATION));
		printCountingMap.put("owningAssociation", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.PROPERTY__DEFAULT_VALUE));
		printCountingMap.put("defaultValue", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.PROPERTY__SUBSETTED_PROPERTY));
		printCountingMap.put("subsettedProperty", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.PROPERTY__ASSOCIATION));
		printCountingMap.put("association", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.PROPERTY__QUALIFIER));
		printCountingMap.put("qualifier", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.PROPERTY__ASSOCIATION_END));
		printCountingMap.put("associationEnd", temp == null ? 0 : 1);
		// print collected hidden tokens
		int count;
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("ownedComment");
		if (count > 0) {
			java.util.List<?> list = (java.util.List<?>)element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.PROPERTY__OWNED_COMMENT));
			int index  = list.size() - count;
			if (index < 0) {
				index = 0;
			}
			java.util.ListIterator<?> it  = list.listIterator(index);
			while (it.hasNext()) {
				Object o = it.next();
				doPrint((org.eclipse.emf.ecore.EObject) o, out, localtab);
			}
			printCountingMap.put("ownedComment", 0);
		}
		// DEFINITION PART BEGINS (PlaceholderUsingDefaultToken)
		count = printCountingMap.get("name");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.PROPERTY__NAME));
			if (o != null) {
				sare.language.acm.resource.acm.IAcmTokenResolver resolver = tokenResolverFactory.createTokenResolver("TEXT");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve((Object) o, element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.PROPERTY__NAME), element));
				out.print(" ");
			}
			printCountingMap.put("name", count - 1);
		}
		// DEFINITION PART BEGINS (CsString)
		out.print(":");
		out.print(" ");
		// DEFINITION PART BEGINS (PlaceholderUsingDefaultToken)
		count = printCountingMap.get("type");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.PROPERTY__TYPE));
			if (o != null) {
				sare.language.acm.resource.acm.IAcmTokenResolver resolver = tokenResolverFactory.createTokenResolver("TEXT");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getTypedElementTypeReferenceResolver().deResolve((org.eclipse.uml2.uml.Type) o, element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.PROPERTY__TYPE)), element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.PROPERTY__TYPE), element));
				out.print(" ");
			}
			printCountingMap.put("type", count - 1);
		}
	}
	
	
	public void print_org_eclipse_uml2_uml_Parameter(org.eclipse.uml2.uml.Parameter element, String outertab, java.io.PrintWriter out) {
		String localtab = outertab;
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		java.util.Map<String, Integer> printCountingMap = new java.util.LinkedHashMap<String, Integer>(28);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.PARAMETER__EANNOTATIONS));
		printCountingMap.put("eAnnotations", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.PARAMETER__OWNED_COMMENT));
		printCountingMap.put("ownedComment", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.PARAMETER__NAME));
		printCountingMap.put("name", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.PARAMETER__VISIBILITY));
		printCountingMap.put("visibility", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.PARAMETER__CLIENT_DEPENDENCY));
		printCountingMap.put("clientDependency", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.PARAMETER__NAME_EXPRESSION));
		printCountingMap.put("nameExpression", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.PARAMETER__TYPE));
		printCountingMap.put("type", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.PARAMETER__OWNING_TEMPLATE_PARAMETER));
		printCountingMap.put("owningTemplateParameter", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.PARAMETER__TEMPLATE_PARAMETER));
		printCountingMap.put("templateParameter", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.PARAMETER__IS_ORDERED));
		printCountingMap.put("isOrdered", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.PARAMETER__IS_UNIQUE));
		printCountingMap.put("isUnique", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.PARAMETER__UPPER_VALUE));
		printCountingMap.put("upperValue", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.PARAMETER__LOWER_VALUE));
		printCountingMap.put("lowerValue", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.PARAMETER__PARAMETER_SET));
		printCountingMap.put("parameterSet", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.PARAMETER__OPERATION));
		printCountingMap.put("operation", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.PARAMETER__DIRECTION));
		printCountingMap.put("direction", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.PARAMETER__DEFAULT_VALUE));
		printCountingMap.put("defaultValue", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.PARAMETER__IS_EXCEPTION));
		printCountingMap.put("isException", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.PARAMETER__IS_STREAM));
		printCountingMap.put("isStream", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.PARAMETER__EFFECT));
		printCountingMap.put("effect", temp == null ? 0 : 1);
		// print collected hidden tokens
		int count;
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("ownedComment");
		if (count > 0) {
			java.util.List<?> list = (java.util.List<?>)element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.PARAMETER__OWNED_COMMENT));
			int index  = list.size() - count;
			if (index < 0) {
				index = 0;
			}
			java.util.ListIterator<?> it  = list.listIterator(index);
			while (it.hasNext()) {
				Object o = it.next();
				doPrint((org.eclipse.emf.ecore.EObject) o, out, localtab);
			}
			printCountingMap.put("ownedComment", 0);
		}
		// DEFINITION PART BEGINS (PlaceholderUsingDefaultToken)
		count = printCountingMap.get("name");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.PARAMETER__NAME));
			if (o != null) {
				sare.language.acm.resource.acm.IAcmTokenResolver resolver = tokenResolverFactory.createTokenResolver("TEXT");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve((Object) o, element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.PARAMETER__NAME), element));
				out.print(" ");
			}
			printCountingMap.put("name", count - 1);
		}
		// DEFINITION PART BEGINS (CsString)
		out.print(":");
		out.print(" ");
		// DEFINITION PART BEGINS (PlaceholderUsingDefaultToken)
		count = printCountingMap.get("type");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.PARAMETER__TYPE));
			if (o != null) {
				sare.language.acm.resource.acm.IAcmTokenResolver resolver = tokenResolverFactory.createTokenResolver("TEXT");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getTypedElementTypeReferenceResolver().deResolve((org.eclipse.uml2.uml.Type) o, element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.PARAMETER__TYPE)), element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.PARAMETER__TYPE), element));
				out.print(" ");
			}
			printCountingMap.put("type", count - 1);
		}
	}
	
	
	public void print_org_eclipse_uml2_uml_Comment(org.eclipse.uml2.uml.Comment element, String outertab, java.io.PrintWriter out) {
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		java.util.Map<String, Integer> printCountingMap = new java.util.LinkedHashMap<String, Integer>(6);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.COMMENT__EANNOTATIONS));
		printCountingMap.put("eAnnotations", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.COMMENT__OWNED_COMMENT));
		printCountingMap.put("ownedComment", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.COMMENT__BODY));
		printCountingMap.put("body", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.COMMENT__ANNOTATED_ELEMENT));
		printCountingMap.put("annotatedElement", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		// print collected hidden tokens
		int count;
		int alt = -1;
		alt = 0;
		int matches = 		matchCount(printCountingMap, java.util.Arrays.asList(		"body"		));
		int tempMatchCount;
		tempMatchCount = 		matchCount(printCountingMap, java.util.Arrays.asList(		"body"		));
		if (tempMatchCount > matches) {
			alt = 1;
			matches = tempMatchCount;
		}
		switch(alt) {
			case 1:			{
				// DEFINITION PART BEGINS (CsString)
				out.print("<<");
				out.print(" ");
				// DEFINITION PART BEGINS (PlaceholderUsingDefaultToken)
				count = printCountingMap.get("body");
				if (count > 0) {
					Object o = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.COMMENT__BODY));
					if (o != null) {
						sare.language.acm.resource.acm.IAcmTokenResolver resolver = tokenResolverFactory.createTokenResolver("TEXT");
						resolver.setOptions(getOptions());
						out.print(resolver.deResolve((Object) o, element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.COMMENT__BODY), element));
						out.print(" ");
					}
					printCountingMap.put("body", count - 1);
				}
				// DEFINITION PART BEGINS (CsString)
				out.print(">>");
				out.print(" ");
			}
			break;
			default:			// DEFINITION PART BEGINS (CsString)
			out.print("\u00ab");
			out.print(" ");
			// DEFINITION PART BEGINS (PlaceholderUsingDefaultToken)
			count = printCountingMap.get("body");
			if (count > 0) {
				Object o = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.COMMENT__BODY));
				if (o != null) {
					sare.language.acm.resource.acm.IAcmTokenResolver resolver = tokenResolverFactory.createTokenResolver("TEXT");
					resolver.setOptions(getOptions());
					out.print(resolver.deResolve((Object) o, element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.COMMENT__BODY), element));
					out.print(" ");
				}
				printCountingMap.put("body", count - 1);
			}
			// DEFINITION PART BEGINS (CsString)
			out.print("\u00bb");
			out.print(" ");
		}
	}
	
	
}
