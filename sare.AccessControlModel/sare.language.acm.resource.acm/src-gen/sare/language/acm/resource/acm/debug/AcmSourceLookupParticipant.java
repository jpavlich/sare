/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package sare.language.acm.resource.acm.debug;

public class AcmSourceLookupParticipant extends org.eclipse.debug.core.sourcelookup.AbstractSourceLookupParticipant {
	
	public String getSourceName(Object object) throws org.eclipse.core.runtime.CoreException {
		if (object instanceof sare.language.acm.resource.acm.debug.AcmStackFrame) {
			sare.language.acm.resource.acm.debug.AcmStackFrame frame = (sare.language.acm.resource.acm.debug.AcmStackFrame) object;
			return frame.getResourceURI();
		}
		return null;
	}
	
}
