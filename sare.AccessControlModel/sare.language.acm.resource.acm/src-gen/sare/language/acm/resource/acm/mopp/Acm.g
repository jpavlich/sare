grammar Acm;

options {
	superClass = AcmANTLRParserBase;
	backtrack = true;
	memoize = true;
}

@lexer::header {
	package sare.language.acm.resource.acm.mopp;
}

@lexer::members {
	public java.util.List<org.antlr.runtime3_3_0.RecognitionException> lexerExceptions  = new java.util.ArrayList<org.antlr.runtime3_3_0.RecognitionException>();
	public java.util.List<Integer> lexerExceptionsPosition = new java.util.ArrayList<Integer>();
	
	public void reportError(org.antlr.runtime3_3_0.RecognitionException e) {
		lexerExceptions.add(e);
		lexerExceptionsPosition.add(((org.antlr.runtime3_3_0.ANTLRStringStream) input).index());
	}
}
@header{
	package sare.language.acm.resource.acm.mopp;
}

@members{
	private sare.language.acm.resource.acm.IAcmTokenResolverFactory tokenResolverFactory = new sare.language.acm.resource.acm.mopp.AcmTokenResolverFactory();
	
	/**
	 * the index of the last token that was handled by collectHiddenTokens()
	 */
	private int lastPosition;
	
	/**
	 * A flag that indicates whether the parser should remember all expected elements.
	 * This flag is set to true when using the parse for code completion. Otherwise it
	 * is set to false.
	 */
	private boolean rememberExpectedElements = false;
	
	private Object parseToIndexTypeObject;
	private int lastTokenIndex = 0;
	
	/**
	 * A list of expected elements the were collected while parsing the input stream.
	 * This list is only filled if <code>rememberExpectedElements</code> is set to
	 * true.
	 */
	private java.util.List<sare.language.acm.resource.acm.mopp.AcmExpectedTerminal> expectedElements = new java.util.ArrayList<sare.language.acm.resource.acm.mopp.AcmExpectedTerminal>();
	
	private int mismatchedTokenRecoveryTries = 0;
	/**
	 * A helper list to allow a lexer to pass errors to its parser
	 */
	protected java.util.List<org.antlr.runtime3_3_0.RecognitionException> lexerExceptions = java.util.Collections.synchronizedList(new java.util.ArrayList<org.antlr.runtime3_3_0.RecognitionException>());
	
	/**
	 * Another helper list to allow a lexer to pass positions of errors to its parser
	 */
	protected java.util.List<Integer> lexerExceptionsPosition = java.util.Collections.synchronizedList(new java.util.ArrayList<Integer>());
	
	/**
	 * A stack for incomplete objects. This stack is used filled when the parser is
	 * used for code completion. Whenever the parser starts to read an object it is
	 * pushed on the stack. Once the element was parser completely it is popped from
	 * the stack.
	 */
	protected java.util.Stack<org.eclipse.emf.ecore.EObject> incompleteObjects = new java.util.Stack<org.eclipse.emf.ecore.EObject>();
	
	private int stopIncludingHiddenTokens;
	private int stopExcludingHiddenTokens;
	private int tokenIndexOfLastCompleteElement;
	
	private int expectedElementsIndexOfLastCompleteElement;
	
	/**
	 * The offset indicating the cursor position when the parser is used for code
	 * completion by calling parseToExpectedElements().
	 */
	private int cursorOffset;
	
	/**
	 * The offset of the first hidden token of the last expected element. This offset
	 * is used to discard expected elements, which are not needed for code completion.
	 */
	private int lastStartIncludingHidden;
	
	protected void addErrorToResource(final String errorMessage, final int column, final int line, final int startIndex, final int stopIndex) {
		postParseCommands.add(new sare.language.acm.resource.acm.IAcmCommand<sare.language.acm.resource.acm.IAcmTextResource>() {
			public boolean execute(sare.language.acm.resource.acm.IAcmTextResource resource) {
				if (resource == null) {
					// the resource can be null if the parser is used for code completion
					return true;
				}
				resource.addProblem(new sare.language.acm.resource.acm.IAcmProblem() {
					public sare.language.acm.resource.acm.AcmEProblemSeverity getSeverity() {
						return sare.language.acm.resource.acm.AcmEProblemSeverity.ERROR;
					}
					public sare.language.acm.resource.acm.AcmEProblemType getType() {
						return sare.language.acm.resource.acm.AcmEProblemType.SYNTAX_ERROR;
					}
					public String getMessage() {
						return errorMessage;
					}
					public java.util.Collection<sare.language.acm.resource.acm.IAcmQuickFix> getQuickFixes() {
						return null;
					}
				}, column, line, startIndex, stopIndex);
				return true;
			}
		});
	}
	
	public void addExpectedElement(sare.language.acm.resource.acm.IAcmExpectedElement terminal, int followSetID, org.eclipse.emf.ecore.EStructuralFeature... containmentTrace) {
		if (!this.rememberExpectedElements) {
			return;
		}
		sare.language.acm.resource.acm.mopp.AcmExpectedTerminal expectedElement = new sare.language.acm.resource.acm.mopp.AcmExpectedTerminal(terminal, followSetID, containmentTrace);
		setPosition(expectedElement, input.index());
		int startIncludingHiddenTokens = expectedElement.getStartIncludingHiddenTokens();
		if (lastStartIncludingHidden >= 0 && lastStartIncludingHidden < startIncludingHiddenTokens && cursorOffset > startIncludingHiddenTokens) {
			// clear list of expected elements
			this.expectedElements.clear();
		}
		lastStartIncludingHidden = startIncludingHiddenTokens;
		this.expectedElements.add(expectedElement);
	}
	
	protected void collectHiddenTokens(org.eclipse.emf.ecore.EObject element) {
	}
	
	protected void copyLocalizationInfos(final org.eclipse.emf.ecore.EObject source, final org.eclipse.emf.ecore.EObject target) {
		postParseCommands.add(new sare.language.acm.resource.acm.IAcmCommand<sare.language.acm.resource.acm.IAcmTextResource>() {
			public boolean execute(sare.language.acm.resource.acm.IAcmTextResource resource) {
				sare.language.acm.resource.acm.IAcmLocationMap locationMap = resource.getLocationMap();
				if (locationMap == null) {
					// the locationMap can be null if the parser is used for code completion
					return true;
				}
				locationMap.setCharStart(target, locationMap.getCharStart(source));
				locationMap.setCharEnd(target, locationMap.getCharEnd(source));
				locationMap.setColumn(target, locationMap.getColumn(source));
				locationMap.setLine(target, locationMap.getLine(source));
				return true;
			}
		});
	}
	
	protected void copyLocalizationInfos(final org.antlr.runtime3_3_0.CommonToken source, final org.eclipse.emf.ecore.EObject target) {
		postParseCommands.add(new sare.language.acm.resource.acm.IAcmCommand<sare.language.acm.resource.acm.IAcmTextResource>() {
			public boolean execute(sare.language.acm.resource.acm.IAcmTextResource resource) {
				sare.language.acm.resource.acm.IAcmLocationMap locationMap = resource.getLocationMap();
				if (locationMap == null) {
					// the locationMap can be null if the parser is used for code completion
					return true;
				}
				if (source == null) {
					return true;
				}
				locationMap.setCharStart(target, source.getStartIndex());
				locationMap.setCharEnd(target, source.getStopIndex());
				locationMap.setColumn(target, source.getCharPositionInLine());
				locationMap.setLine(target, source.getLine());
				return true;
			}
		});
	}
	
	/**
	 * Sets the end character index and the last line for the given object in the
	 * location map.
	 */
	protected void setLocalizationEnd(java.util.Collection<sare.language.acm.resource.acm.IAcmCommand<sare.language.acm.resource.acm.IAcmTextResource>> postParseCommands , final org.eclipse.emf.ecore.EObject object, final int endChar, final int endLine) {
		postParseCommands.add(new sare.language.acm.resource.acm.IAcmCommand<sare.language.acm.resource.acm.IAcmTextResource>() {
			public boolean execute(sare.language.acm.resource.acm.IAcmTextResource resource) {
				sare.language.acm.resource.acm.IAcmLocationMap locationMap = resource.getLocationMap();
				if (locationMap == null) {
					// the locationMap can be null if the parser is used for code completion
					return true;
				}
				locationMap.setCharEnd(object, endChar);
				locationMap.setLine(object, endLine);
				return true;
			}
		});
	}
	
	public sare.language.acm.resource.acm.IAcmTextParser createInstance(java.io.InputStream actualInputStream, String encoding) {
		try {
			if (encoding == null) {
				return new AcmParser(new org.antlr.runtime3_3_0.CommonTokenStream(new AcmLexer(new org.antlr.runtime3_3_0.ANTLRInputStream(actualInputStream))));
			} else {
				return new AcmParser(new org.antlr.runtime3_3_0.CommonTokenStream(new AcmLexer(new org.antlr.runtime3_3_0.ANTLRInputStream(actualInputStream, encoding))));
			}
		} catch (java.io.IOException e) {
			sare.language.acm.resource.acm.mopp.AcmPlugin.logError("Error while creating parser.", e);
			return null;
		}
	}
	
	/**
	 * This default constructor is only used to call createInstance() on it.
	 */
	public AcmParser() {
		super(null);
	}
	
	protected org.eclipse.emf.ecore.EObject doParse() throws org.antlr.runtime3_3_0.RecognitionException {
		this.lastPosition = 0;
		// required because the lexer class can not be subclassed
		((AcmLexer) getTokenStream().getTokenSource()).lexerExceptions = lexerExceptions;
		((AcmLexer) getTokenStream().getTokenSource()).lexerExceptionsPosition = lexerExceptionsPosition;
		Object typeObject = getTypeObject();
		if (typeObject == null) {
			return start();
		} else if (typeObject instanceof org.eclipse.emf.ecore.EClass) {
			org.eclipse.emf.ecore.EClass type = (org.eclipse.emf.ecore.EClass) typeObject;
			if (type.getInstanceClass() == sare.language.acm.AccessControlModel.class) {
				return parse_sare_language_acm_AccessControlModel();
			}
			if (type.getInstanceClass() == sare.language.acm.Role.class) {
				return parse_sare_language_acm_Role();
			}
			if (type.getInstanceClass() == sare.language.acm.User.class) {
				return parse_sare_language_acm_User();
			}
			if (type.getInstanceClass() == sare.language.acm.Permission.class) {
				return parse_sare_language_acm_Permission();
			}
			if (type.getInstanceClass() == org.eclipse.uml2.uml.Model.class) {
				return parse_org_eclipse_uml2_uml_Model();
			}
			if (type.getInstanceClass() == org.eclipse.uml2.uml.PackageImport.class) {
				return parse_org_eclipse_uml2_uml_PackageImport();
			}
			if (type.getInstanceClass() == org.eclipse.uml2.uml.Package.class) {
				return parse_org_eclipse_uml2_uml_Package();
			}
			if (type.getInstanceClass() == org.eclipse.uml2.uml.Class.class) {
				return parse_org_eclipse_uml2_uml_Class();
			}
			if (type.getInstanceClass() == org.eclipse.uml2.uml.Operation.class) {
				return parse_org_eclipse_uml2_uml_Operation();
			}
			if (type.getInstanceClass() == org.eclipse.uml2.uml.Property.class) {
				return parse_org_eclipse_uml2_uml_Property();
			}
			if (type.getInstanceClass() == org.eclipse.uml2.uml.Parameter.class) {
				return parse_org_eclipse_uml2_uml_Parameter();
			}
			if (type.getInstanceClass() == org.eclipse.uml2.uml.Comment.class) {
				return parse_org_eclipse_uml2_uml_Comment();
			}
		}
		throw new sare.language.acm.resource.acm.mopp.AcmUnexpectedContentTypeException(typeObject);
	}
	
	public int getMismatchedTokenRecoveryTries() {
		return mismatchedTokenRecoveryTries;
	}
	
	public Object getMissingSymbol(org.antlr.runtime3_3_0.IntStream arg0, org.antlr.runtime3_3_0.RecognitionException arg1, int arg2, org.antlr.runtime3_3_0.BitSet arg3) {
		mismatchedTokenRecoveryTries++;
		return super.getMissingSymbol(arg0, arg1, arg2, arg3);
	}
	
	public Object getParseToIndexTypeObject() {
		return parseToIndexTypeObject;
	}
	
	protected Object getTypeObject() {
		Object typeObject = getParseToIndexTypeObject();
		if (typeObject != null) {
			return typeObject;
		}
		java.util.Map<?,?> options = getOptions();
		if (options != null) {
			typeObject = options.get(sare.language.acm.resource.acm.IAcmOptions.RESOURCE_CONTENT_TYPE);
		}
		return typeObject;
	}
	
	/**
	 * Implementation that calls {@link #doParse()} and handles the thrown
	 * RecognitionExceptions.
	 */
	public sare.language.acm.resource.acm.IAcmParseResult parse() {
		terminateParsing = false;
		postParseCommands = new java.util.ArrayList<sare.language.acm.resource.acm.IAcmCommand<sare.language.acm.resource.acm.IAcmTextResource>>();
		sare.language.acm.resource.acm.mopp.AcmParseResult parseResult = new sare.language.acm.resource.acm.mopp.AcmParseResult();
		try {
			org.eclipse.emf.ecore.EObject result =  doParse();
			if (lexerExceptions.isEmpty()) {
				parseResult.setRoot(result);
			}
		} catch (org.antlr.runtime3_3_0.RecognitionException re) {
			reportError(re);
		} catch (java.lang.IllegalArgumentException iae) {
			if ("The 'no null' constraint is violated".equals(iae.getMessage())) {
				// can be caused if a null is set on EMF models where not allowed. this will just
				// happen if other errors occurred before
			} else {
				iae.printStackTrace();
			}
		}
		for (org.antlr.runtime3_3_0.RecognitionException re : lexerExceptions) {
			reportLexicalError(re);
		}
		parseResult.getPostParseCommands().addAll(postParseCommands);
		return parseResult;
	}
	
	public java.util.List<sare.language.acm.resource.acm.mopp.AcmExpectedTerminal> parseToExpectedElements(org.eclipse.emf.ecore.EClass type, sare.language.acm.resource.acm.IAcmTextResource dummyResource, int cursorOffset) {
		this.rememberExpectedElements = true;
		this.parseToIndexTypeObject = type;
		this.cursorOffset = cursorOffset;
		this.lastStartIncludingHidden = -1;
		final org.antlr.runtime3_3_0.CommonTokenStream tokenStream = (org.antlr.runtime3_3_0.CommonTokenStream) getTokenStream();
		sare.language.acm.resource.acm.IAcmParseResult result = parse();
		for (org.eclipse.emf.ecore.EObject incompleteObject : incompleteObjects) {
			org.antlr.runtime3_3_0.Lexer lexer = (org.antlr.runtime3_3_0.Lexer) tokenStream.getTokenSource();
			int endChar = lexer.getCharIndex();
			int endLine = lexer.getLine();
			setLocalizationEnd(result.getPostParseCommands(), incompleteObject, endChar, endLine);
		}
		if (result != null) {
			org.eclipse.emf.ecore.EObject root = result.getRoot();
			if (root != null) {
				dummyResource.getContentsInternal().add(root);
			}
			for (sare.language.acm.resource.acm.IAcmCommand<sare.language.acm.resource.acm.IAcmTextResource> command : result.getPostParseCommands()) {
				command.execute(dummyResource);
			}
		}
		// remove all expected elements that were added after the last complete element
		expectedElements = expectedElements.subList(0, expectedElementsIndexOfLastCompleteElement + 1);
		int lastFollowSetID = expectedElements.get(expectedElementsIndexOfLastCompleteElement).getFollowSetID();
		java.util.Set<sare.language.acm.resource.acm.mopp.AcmExpectedTerminal> currentFollowSet = new java.util.LinkedHashSet<sare.language.acm.resource.acm.mopp.AcmExpectedTerminal>();
		java.util.List<sare.language.acm.resource.acm.mopp.AcmExpectedTerminal> newFollowSet = new java.util.ArrayList<sare.language.acm.resource.acm.mopp.AcmExpectedTerminal>();
		for (int i = expectedElementsIndexOfLastCompleteElement; i >= 0; i--) {
			sare.language.acm.resource.acm.mopp.AcmExpectedTerminal expectedElementI = expectedElements.get(i);
			if (expectedElementI.getFollowSetID() == lastFollowSetID) {
				currentFollowSet.add(expectedElementI);
			} else {
				break;
			}
		}
		int followSetID = 90;
		int i;
		for (i = tokenIndexOfLastCompleteElement; i < tokenStream.size(); i++) {
			org.antlr.runtime3_3_0.CommonToken nextToken = (org.antlr.runtime3_3_0.CommonToken) tokenStream.get(i);
			if (nextToken.getType() < 0) {
				break;
			}
			if (nextToken.getChannel() == 99) {
				// hidden tokens do not reduce the follow set
			} else {
				// now that we have found the next visible token the position for that expected
				// terminals can be set
				for (sare.language.acm.resource.acm.mopp.AcmExpectedTerminal nextFollow : newFollowSet) {
					lastTokenIndex = 0;
					setPosition(nextFollow, i);
				}
				newFollowSet.clear();
				// normal tokens do reduce the follow set - only elements that match the token are
				// kept
				for (sare.language.acm.resource.acm.mopp.AcmExpectedTerminal nextFollow : currentFollowSet) {
					if (nextFollow.getTerminal().getTokenNames().contains(getTokenNames()[nextToken.getType()])) {
						// keep this one - it matches
						java.util.Collection<sare.language.acm.resource.acm.util.AcmPair<sare.language.acm.resource.acm.IAcmExpectedElement, org.eclipse.emf.ecore.EStructuralFeature[]>> newFollowers = nextFollow.getTerminal().getFollowers();
						for (sare.language.acm.resource.acm.util.AcmPair<sare.language.acm.resource.acm.IAcmExpectedElement, org.eclipse.emf.ecore.EStructuralFeature[]> newFollowerPair : newFollowers) {
							sare.language.acm.resource.acm.IAcmExpectedElement newFollower = newFollowerPair.getLeft();
							sare.language.acm.resource.acm.mopp.AcmExpectedTerminal newFollowTerminal = new sare.language.acm.resource.acm.mopp.AcmExpectedTerminal(newFollower, followSetID, newFollowerPair.getRight());
							newFollowSet.add(newFollowTerminal);
							expectedElements.add(newFollowTerminal);
						}
					}
				}
				currentFollowSet.clear();
				currentFollowSet.addAll(newFollowSet);
			}
			followSetID++;
		}
		// after the last token in the stream we must set the position for the elements
		// that were added during the last iteration of the loop
		for (sare.language.acm.resource.acm.mopp.AcmExpectedTerminal nextFollow : newFollowSet) {
			lastTokenIndex = 0;
			setPosition(nextFollow, i);
		}
		return this.expectedElements;
	}
	
	public void setPosition(sare.language.acm.resource.acm.mopp.AcmExpectedTerminal expectedElement, int tokenIndex) {
		int currentIndex = Math.max(0, tokenIndex);
		for (int index = lastTokenIndex; index < currentIndex; index++) {
			if (index >= input.size()) {
				break;
			}
			org.antlr.runtime3_3_0.CommonToken tokenAtIndex = (org.antlr.runtime3_3_0.CommonToken) input.get(index);
			stopIncludingHiddenTokens = tokenAtIndex.getStopIndex() + 1;
			if (tokenAtIndex.getChannel() != 99) {
				stopExcludingHiddenTokens = tokenAtIndex.getStopIndex() + 1;
			}
		}
		lastTokenIndex = Math.max(0, currentIndex);
		expectedElement.setPosition(stopExcludingHiddenTokens, stopIncludingHiddenTokens);
	}
	
	public Object recoverFromMismatchedToken(org.antlr.runtime3_3_0.IntStream input, int ttype, org.antlr.runtime3_3_0.BitSet follow) throws org.antlr.runtime3_3_0.RecognitionException {
		if (!rememberExpectedElements) {
			return super.recoverFromMismatchedToken(input, ttype, follow);
		} else {
			return null;
		}
	}
	
	/**
	 * Translates errors thrown by the parser into human readable messages.
	 */
	public void reportError(final org.antlr.runtime3_3_0.RecognitionException e)  {
		String message = e.getMessage();
		if (e instanceof org.antlr.runtime3_3_0.MismatchedTokenException) {
			org.antlr.runtime3_3_0.MismatchedTokenException mte = (org.antlr.runtime3_3_0.MismatchedTokenException) e;
			String expectedTokenName = formatTokenName(mte.expecting);
			String actualTokenName = formatTokenName(e.token.getType());
			message = "Syntax error on token \"" + e.token.getText() + " (" + actualTokenName + ")\", \"" + expectedTokenName + "\" expected";
		} else if (e instanceof org.antlr.runtime3_3_0.MismatchedTreeNodeException) {
			org.antlr.runtime3_3_0.MismatchedTreeNodeException mtne = (org.antlr.runtime3_3_0.MismatchedTreeNodeException) e;
			String expectedTokenName = formatTokenName(mtne.expecting);
			message = "mismatched tree node: " + "xxx" + "; tokenName " + expectedTokenName;
		} else if (e instanceof org.antlr.runtime3_3_0.NoViableAltException) {
			message = "Syntax error on token \"" + e.token.getText() + "\", check following tokens";
		} else if (e instanceof org.antlr.runtime3_3_0.EarlyExitException) {
			message = "Syntax error on token \"" + e.token.getText() + "\", delete this token";
		} else if (e instanceof org.antlr.runtime3_3_0.MismatchedSetException) {
			org.antlr.runtime3_3_0.MismatchedSetException mse = (org.antlr.runtime3_3_0.MismatchedSetException) e;
			message = "mismatched token: " + e.token + "; expecting set " + mse.expecting;
		} else if (e instanceof org.antlr.runtime3_3_0.MismatchedNotSetException) {
			org.antlr.runtime3_3_0.MismatchedNotSetException mse = (org.antlr.runtime3_3_0.MismatchedNotSetException) e;
			message = "mismatched token: " +  e.token + "; expecting set " + mse.expecting;
		} else if (e instanceof org.antlr.runtime3_3_0.FailedPredicateException) {
			org.antlr.runtime3_3_0.FailedPredicateException fpe = (org.antlr.runtime3_3_0.FailedPredicateException) e;
			message = "rule " + fpe.ruleName + " failed predicate: {" +  fpe.predicateText + "}?";
		}
		// the resource may be null if the parser is used for code completion
		final String finalMessage = message;
		if (e.token instanceof org.antlr.runtime3_3_0.CommonToken) {
			final org.antlr.runtime3_3_0.CommonToken ct = (org.antlr.runtime3_3_0.CommonToken) e.token;
			addErrorToResource(finalMessage, ct.getCharPositionInLine(), ct.getLine(), ct.getStartIndex(), ct.getStopIndex());
		} else {
			addErrorToResource(finalMessage, e.token.getCharPositionInLine(), e.token.getLine(), 1, 5);
		}
	}
	
	/**
	 * Translates errors thrown by the lexer into human readable messages.
	 */
	public void reportLexicalError(final org.antlr.runtime3_3_0.RecognitionException e)  {
		String message = "";
		if (e instanceof org.antlr.runtime3_3_0.MismatchedTokenException) {
			org.antlr.runtime3_3_0.MismatchedTokenException mte = (org.antlr.runtime3_3_0.MismatchedTokenException) e;
			message = "Syntax error on token \"" + ((char) e.c) + "\", \"" + (char) mte.expecting + "\" expected";
		} else if (e instanceof org.antlr.runtime3_3_0.NoViableAltException) {
			message = "Syntax error on token \"" + ((char) e.c) + "\", delete this token";
		} else if (e instanceof org.antlr.runtime3_3_0.EarlyExitException) {
			org.antlr.runtime3_3_0.EarlyExitException eee = (org.antlr.runtime3_3_0.EarlyExitException) e;
			message = "required (...)+ loop (decision=" + eee.decisionNumber + ") did not match anything; on line " + e.line + ":" + e.charPositionInLine + " char=" + ((char) e.c) + "'";
		} else if (e instanceof org.antlr.runtime3_3_0.MismatchedSetException) {
			org.antlr.runtime3_3_0.MismatchedSetException mse = (org.antlr.runtime3_3_0.MismatchedSetException) e;
			message = "mismatched char: '" + ((char) e.c) + "' on line " + e.line + ":" + e.charPositionInLine + "; expecting set " + mse.expecting;
		} else if (e instanceof org.antlr.runtime3_3_0.MismatchedNotSetException) {
			org.antlr.runtime3_3_0.MismatchedNotSetException mse = (org.antlr.runtime3_3_0.MismatchedNotSetException) e;
			message = "mismatched char: '" + ((char) e.c) + "' on line " + e.line + ":" + e.charPositionInLine + "; expecting set " + mse.expecting;
		} else if (e instanceof org.antlr.runtime3_3_0.MismatchedRangeException) {
			org.antlr.runtime3_3_0.MismatchedRangeException mre = (org.antlr.runtime3_3_0.MismatchedRangeException) e;
			message = "mismatched char: '" + ((char) e.c) + "' on line " + e.line + ":" + e.charPositionInLine + "; expecting set '" + (char) mre.a + "'..'" + (char) mre.b + "'";
		} else if (e instanceof org.antlr.runtime3_3_0.FailedPredicateException) {
			org.antlr.runtime3_3_0.FailedPredicateException fpe = (org.antlr.runtime3_3_0.FailedPredicateException) e;
			message = "rule " + fpe.ruleName + " failed predicate: {" + fpe.predicateText + "}?";
		}
		addErrorToResource(message, e.charPositionInLine, e.line, lexerExceptionsPosition.get(lexerExceptions.indexOf(e)), lexerExceptionsPosition.get(lexerExceptions.indexOf(e)));
	}
	
	protected void completedElement(Object object, boolean isContainment) {
		if (isContainment && !this.incompleteObjects.isEmpty()) {
			this.incompleteObjects.pop();
		}
		if (object instanceof org.eclipse.emf.ecore.EObject) {
			this.tokenIndexOfLastCompleteElement = getTokenStream().index();
			this.expectedElementsIndexOfLastCompleteElement = expectedElements.size() - 1;
		}
	}
	
}

start returns [ org.eclipse.emf.ecore.EObject element = null]
:
	{
		// follow set for start rule(s)
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_0, 0);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_1, 0);
		expectedElementsIndexOfLastCompleteElement = 0;
	}
	(
		c0 = parse_sare_language_acm_AccessControlModel{ element = c0; }
	)
	EOF	{
		retrieveLayoutInformation(element, null, null, false);
	}
	
;

parse_sare_language_acm_AccessControlModel returns [sare.language.acm.AccessControlModel element = null]
@init{
}
:
	(
		(
			a0 = 'import' {
				if (element == null) {
					element = sare.language.acm.AcmFactory.eINSTANCE.createAccessControlModel();
					incompleteObjects.push(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.ACM_0_0_0_0_0_0_0, null, true);
				copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken)a0, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_2, 1);
			}
			
			(
				a1 = QUOTED_34_34				
				{
					if (terminateParsing) {
						throw new sare.language.acm.resource.acm.mopp.AcmTerminateParsingException();
					}
					if (element == null) {
						element = sare.language.acm.AcmFactory.eINSTANCE.createAccessControlModel();
						incompleteObjects.push(element);
					}
					if (a1 != null) {
						sare.language.acm.resource.acm.IAcmTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("QUOTED_34_34");
						tokenResolver.setOptions(getOptions());
						sare.language.acm.resource.acm.IAcmTokenResolveResult result = getFreshTokenResolveResult();
						tokenResolver.resolve(a1.getText(), element.eClass().getEStructuralFeature(sare.language.acm.AcmPackage.ACCESS_CONTROL_MODEL__IMPORTS), result);
						Object resolvedObject = result.getResolvedToken();
						if (resolvedObject == null) {
							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_3_0.CommonToken) a1).getLine(), ((org.antlr.runtime3_3_0.CommonToken) a1).getCharPositionInLine(), ((org.antlr.runtime3_3_0.CommonToken) a1).getStartIndex(), ((org.antlr.runtime3_3_0.CommonToken) a1).getStopIndex());
						}
						String resolved = (String) resolvedObject;
						org.eclipse.emf.ecore.EObject proxy = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEObject();
						collectHiddenTokens(element);
						registerContextDependentProxy(new sare.language.acm.resource.acm.mopp.AcmContextDependentURIFragmentFactory<sare.language.acm.AccessControlModel, org.eclipse.emf.ecore.EObject>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getAccessControlModelImportsReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(sare.language.acm.AcmPackage.ACCESS_CONTROL_MODEL__IMPORTS), resolved, proxy);
						if (proxy != null) {
							Object value = proxy;
							addObjectToList(element, sare.language.acm.AcmPackage.ACCESS_CONTROL_MODEL__IMPORTS, value);
							completedElement(value, false);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.ACM_0_0_0_0_0_0_1, proxy, true);
						copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken) a1, element);
						copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken) a1, proxy);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_0, 2);
				addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_1, 2);
			}
			
		)
		
	)*	{
		// expected elements (follow set)
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_0, 3);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_1, 3);
	}
	
	a2 = 'policy' {
		if (element == null) {
			element = sare.language.acm.AcmFactory.eINSTANCE.createAccessControlModel();
			incompleteObjects.push(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.ACM_0_0_0_1, null, true);
		copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken)a2, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_3, 4);
	}
	
	(
		a3 = TEXT		
		{
			if (terminateParsing) {
				throw new sare.language.acm.resource.acm.mopp.AcmTerminateParsingException();
			}
			if (element == null) {
				element = sare.language.acm.AcmFactory.eINSTANCE.createAccessControlModel();
				incompleteObjects.push(element);
			}
			if (a3 != null) {
				sare.language.acm.resource.acm.IAcmTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
				tokenResolver.setOptions(getOptions());
				sare.language.acm.resource.acm.IAcmTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a3.getText(), element.eClass().getEStructuralFeature(sare.language.acm.AcmPackage.ACCESS_CONTROL_MODEL__NAME), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_3_0.CommonToken) a3).getLine(), ((org.antlr.runtime3_3_0.CommonToken) a3).getCharPositionInLine(), ((org.antlr.runtime3_3_0.CommonToken) a3).getStartIndex(), ((org.antlr.runtime3_3_0.CommonToken) a3).getStopIndex());
				}
				java.lang.String resolved = (java.lang.String) resolvedObject;
				if (resolved != null) {
					Object value = resolved;
					element.eSet(element.eClass().getEStructuralFeature(sare.language.acm.AcmPackage.ACCESS_CONTROL_MODEL__NAME), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.ACM_0_0_0_2, resolved, true);
				copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken) a3, element);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_4, 5);
	}
	
	a4 = '{' {
		if (element == null) {
			element = sare.language.acm.AcmFactory.eINSTANCE.createAccessControlModel();
			incompleteObjects.push(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.ACM_0_0_0_3, null, true);
		copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken)a4, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_5, 6, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_0);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_6, 6, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_0);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_7, 6);
	}
	
	(
		(
			a5_0 = parse_sare_language_acm_Subject			{
				if (terminateParsing) {
					throw new sare.language.acm.resource.acm.mopp.AcmTerminateParsingException();
				}
				if (element == null) {
					element = sare.language.acm.AcmFactory.eINSTANCE.createAccessControlModel();
					incompleteObjects.push(element);
				}
				if (a5_0 != null) {
					if (a5_0 != null) {
						Object value = a5_0;
						addObjectToList(element, sare.language.acm.AcmPackage.ACCESS_CONTROL_MODEL__ELEMENTS, value);
						completedElement(value, true);
					}
					collectHiddenTokens(element);
					retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.ACM_0_0_0_4, a5_0, true);
					copyLocalizationInfos(a5_0, element);
				}
			}
		)
		
	)*	{
		// expected elements (follow set)
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_5, 7, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_0);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_6, 7, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_0);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_7, 7);
	}
	
	a6 = '}' {
		if (element == null) {
			element = sare.language.acm.AcmFactory.eINSTANCE.createAccessControlModel();
			incompleteObjects.push(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.ACM_0_0_0_5, null, true);
		copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken)a6, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_0, 8, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_1);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_1, 8, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_1);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_8, 8, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_1);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_9, 8, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_1);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_10, 8, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_1);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_11, 8, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_2, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_1);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_12, 8, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_2, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_1);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_13, 8, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_1);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_14, 8);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_15, 8);
	}
	
;

parse_sare_language_acm_Role returns [sare.language.acm.Role element = null]
@init{
}
:
	a0 = 'role' {
		if (element == null) {
			element = sare.language.acm.AcmFactory.eINSTANCE.createRole();
			incompleteObjects.push(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.ACM_1_0_0_0, null, true);
		copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken)a0, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_16, 9);
	}
	
	(
		a1 = TEXT		
		{
			if (terminateParsing) {
				throw new sare.language.acm.resource.acm.mopp.AcmTerminateParsingException();
			}
			if (element == null) {
				element = sare.language.acm.AcmFactory.eINSTANCE.createRole();
				incompleteObjects.push(element);
			}
			if (a1 != null) {
				sare.language.acm.resource.acm.IAcmTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
				tokenResolver.setOptions(getOptions());
				sare.language.acm.resource.acm.IAcmTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a1.getText(), element.eClass().getEStructuralFeature(sare.language.acm.AcmPackage.ROLE__NAME), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_3_0.CommonToken) a1).getLine(), ((org.antlr.runtime3_3_0.CommonToken) a1).getCharPositionInLine(), ((org.antlr.runtime3_3_0.CommonToken) a1).getStartIndex(), ((org.antlr.runtime3_3_0.CommonToken) a1).getStopIndex());
				}
				java.lang.String resolved = (java.lang.String) resolvedObject;
				if (resolved != null) {
					Object value = resolved;
					element.eSet(element.eClass().getEStructuralFeature(sare.language.acm.AcmPackage.ROLE__NAME), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.ACM_1_0_0_1, resolved, true);
				copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken) a1, element);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_17, 10);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_18, 10);
	}
	
	(
		(
			a2 = 'extends' {
				if (element == null) {
					element = sare.language.acm.AcmFactory.eINSTANCE.createRole();
					incompleteObjects.push(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.ACM_1_0_0_2_0_0_0, null, true);
				copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken)a2, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_19, 11);
			}
			
			(
				a3 = TEXT				
				{
					if (terminateParsing) {
						throw new sare.language.acm.resource.acm.mopp.AcmTerminateParsingException();
					}
					if (element == null) {
						element = sare.language.acm.AcmFactory.eINSTANCE.createRole();
						incompleteObjects.push(element);
					}
					if (a3 != null) {
						sare.language.acm.resource.acm.IAcmTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
						tokenResolver.setOptions(getOptions());
						sare.language.acm.resource.acm.IAcmTokenResolveResult result = getFreshTokenResolveResult();
						tokenResolver.resolve(a3.getText(), element.eClass().getEStructuralFeature(sare.language.acm.AcmPackage.ROLE__PARENT_ROLES), result);
						Object resolvedObject = result.getResolvedToken();
						if (resolvedObject == null) {
							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_3_0.CommonToken) a3).getLine(), ((org.antlr.runtime3_3_0.CommonToken) a3).getCharPositionInLine(), ((org.antlr.runtime3_3_0.CommonToken) a3).getStartIndex(), ((org.antlr.runtime3_3_0.CommonToken) a3).getStopIndex());
						}
						String resolved = (String) resolvedObject;
						sare.language.acm.Role proxy = sare.language.acm.AcmFactory.eINSTANCE.createRole();
						collectHiddenTokens(element);
						registerContextDependentProxy(new sare.language.acm.resource.acm.mopp.AcmContextDependentURIFragmentFactory<sare.language.acm.Role, sare.language.acm.Role>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getRoleParentRolesReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(sare.language.acm.AcmPackage.ROLE__PARENT_ROLES), resolved, proxy);
						if (proxy != null) {
							Object value = proxy;
							addObjectToList(element, sare.language.acm.AcmPackage.ROLE__PARENT_ROLES, value);
							completedElement(value, false);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.ACM_1_0_0_2_0_0_1, proxy, true);
						copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken) a3, element);
						copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken) a3, proxy);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_20, 12);
				addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_18, 12);
			}
			
			(
				(
					a4 = ',' {
						if (element == null) {
							element = sare.language.acm.AcmFactory.eINSTANCE.createRole();
							incompleteObjects.push(element);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.ACM_1_0_0_2_0_0_2_0_0_0, null, true);
						copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken)a4, element);
					}
					{
						// expected elements (follow set)
						addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_21, 13);
					}
					
					(
						a5 = TEXT						
						{
							if (terminateParsing) {
								throw new sare.language.acm.resource.acm.mopp.AcmTerminateParsingException();
							}
							if (element == null) {
								element = sare.language.acm.AcmFactory.eINSTANCE.createRole();
								incompleteObjects.push(element);
							}
							if (a5 != null) {
								sare.language.acm.resource.acm.IAcmTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
								tokenResolver.setOptions(getOptions());
								sare.language.acm.resource.acm.IAcmTokenResolveResult result = getFreshTokenResolveResult();
								tokenResolver.resolve(a5.getText(), element.eClass().getEStructuralFeature(sare.language.acm.AcmPackage.ROLE__PARENT_ROLES), result);
								Object resolvedObject = result.getResolvedToken();
								if (resolvedObject == null) {
									addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_3_0.CommonToken) a5).getLine(), ((org.antlr.runtime3_3_0.CommonToken) a5).getCharPositionInLine(), ((org.antlr.runtime3_3_0.CommonToken) a5).getStartIndex(), ((org.antlr.runtime3_3_0.CommonToken) a5).getStopIndex());
								}
								String resolved = (String) resolvedObject;
								sare.language.acm.Role proxy = sare.language.acm.AcmFactory.eINSTANCE.createRole();
								collectHiddenTokens(element);
								registerContextDependentProxy(new sare.language.acm.resource.acm.mopp.AcmContextDependentURIFragmentFactory<sare.language.acm.Role, sare.language.acm.Role>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getRoleParentRolesReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(sare.language.acm.AcmPackage.ROLE__PARENT_ROLES), resolved, proxy);
								if (proxy != null) {
									Object value = proxy;
									addObjectToList(element, sare.language.acm.AcmPackage.ROLE__PARENT_ROLES, value);
									completedElement(value, false);
								}
								collectHiddenTokens(element);
								retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.ACM_1_0_0_2_0_0_2_0_0_1, proxy, true);
								copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken) a5, element);
								copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken) a5, proxy);
							}
						}
					)
					{
						// expected elements (follow set)
						addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_20, 14);
						addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_18, 14);
					}
					
				)
				
			)*			{
				// expected elements (follow set)
				addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_20, 15);
				addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_18, 15);
			}
			
		)
		
	)?	{
		// expected elements (follow set)
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_18, 16);
	}
	
	a6 = '{' {
		if (element == null) {
			element = sare.language.acm.AcmFactory.eINSTANCE.createRole();
			incompleteObjects.push(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.ACM_1_0_0_3, null, true);
		copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken)a6, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_22, 17, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_3);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_23, 17, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_3);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_24, 17);
	}
	
	(
		(
			a7_0 = parse_sare_language_acm_Permission			{
				if (terminateParsing) {
					throw new sare.language.acm.resource.acm.mopp.AcmTerminateParsingException();
				}
				if (element == null) {
					element = sare.language.acm.AcmFactory.eINSTANCE.createRole();
					incompleteObjects.push(element);
				}
				if (a7_0 != null) {
					if (a7_0 != null) {
						Object value = a7_0;
						addObjectToList(element, sare.language.acm.AcmPackage.ROLE__PERMISSIONS, value);
						completedElement(value, true);
					}
					collectHiddenTokens(element);
					retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.ACM_1_0_0_4, a7_0, true);
					copyLocalizationInfos(a7_0, element);
				}
			}
		)
		
	)*	{
		// expected elements (follow set)
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_22, 18, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_3);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_23, 18, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_3);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_24, 18);
	}
	
	a8 = '}' {
		if (element == null) {
			element = sare.language.acm.AcmFactory.eINSTANCE.createRole();
			incompleteObjects.push(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.ACM_1_0_0_5, null, true);
		copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken)a8, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_5, 19, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_0);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_6, 19, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_0);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_7, 19);
	}
	
;

parse_sare_language_acm_User returns [sare.language.acm.User element = null]
@init{
}
:
	a0 = 'user' {
		if (element == null) {
			element = sare.language.acm.AcmFactory.eINSTANCE.createUser();
			incompleteObjects.push(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.ACM_2_0_0_0, null, true);
		copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken)a0, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_25, 20);
	}
	
	(
		a1 = TEXT		
		{
			if (terminateParsing) {
				throw new sare.language.acm.resource.acm.mopp.AcmTerminateParsingException();
			}
			if (element == null) {
				element = sare.language.acm.AcmFactory.eINSTANCE.createUser();
				incompleteObjects.push(element);
			}
			if (a1 != null) {
				sare.language.acm.resource.acm.IAcmTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
				tokenResolver.setOptions(getOptions());
				sare.language.acm.resource.acm.IAcmTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a1.getText(), element.eClass().getEStructuralFeature(sare.language.acm.AcmPackage.USER__NAME), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_3_0.CommonToken) a1).getLine(), ((org.antlr.runtime3_3_0.CommonToken) a1).getCharPositionInLine(), ((org.antlr.runtime3_3_0.CommonToken) a1).getStartIndex(), ((org.antlr.runtime3_3_0.CommonToken) a1).getStopIndex());
				}
				java.lang.String resolved = (java.lang.String) resolvedObject;
				if (resolved != null) {
					Object value = resolved;
					element.eSet(element.eClass().getEStructuralFeature(sare.language.acm.AcmPackage.USER__NAME), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.ACM_2_0_0_1, resolved, true);
				copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken) a1, element);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_26, 21);
	}
	
	a2 = 'hasRoles' {
		if (element == null) {
			element = sare.language.acm.AcmFactory.eINSTANCE.createUser();
			incompleteObjects.push(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.ACM_2_0_0_2, null, true);
		copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken)a2, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_27, 22);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_5, 22, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_0);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_6, 22, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_0);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_7, 22);
	}
	
	(
		(
			a3 = ',' {
				if (element == null) {
					element = sare.language.acm.AcmFactory.eINSTANCE.createUser();
					incompleteObjects.push(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.ACM_2_0_0_3_0_0_0, null, true);
				copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken)a3, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_28, 23);
			}
			
			(
				a4 = TEXT				
				{
					if (terminateParsing) {
						throw new sare.language.acm.resource.acm.mopp.AcmTerminateParsingException();
					}
					if (element == null) {
						element = sare.language.acm.AcmFactory.eINSTANCE.createUser();
						incompleteObjects.push(element);
					}
					if (a4 != null) {
						sare.language.acm.resource.acm.IAcmTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
						tokenResolver.setOptions(getOptions());
						sare.language.acm.resource.acm.IAcmTokenResolveResult result = getFreshTokenResolveResult();
						tokenResolver.resolve(a4.getText(), element.eClass().getEStructuralFeature(sare.language.acm.AcmPackage.USER__ROLES), result);
						Object resolvedObject = result.getResolvedToken();
						if (resolvedObject == null) {
							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_3_0.CommonToken) a4).getLine(), ((org.antlr.runtime3_3_0.CommonToken) a4).getCharPositionInLine(), ((org.antlr.runtime3_3_0.CommonToken) a4).getStartIndex(), ((org.antlr.runtime3_3_0.CommonToken) a4).getStopIndex());
						}
						String resolved = (String) resolvedObject;
						sare.language.acm.Role proxy = sare.language.acm.AcmFactory.eINSTANCE.createRole();
						collectHiddenTokens(element);
						registerContextDependentProxy(new sare.language.acm.resource.acm.mopp.AcmContextDependentURIFragmentFactory<sare.language.acm.User, sare.language.acm.Role>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getUserRolesReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(sare.language.acm.AcmPackage.USER__ROLES), resolved, proxy);
						if (proxy != null) {
							Object value = proxy;
							addObjectToList(element, sare.language.acm.AcmPackage.USER__ROLES, value);
							completedElement(value, false);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.ACM_2_0_0_3_0_0_1, proxy, true);
						copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken) a4, element);
						copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken) a4, proxy);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_27, 24);
				addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_5, 24, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_0);
				addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_6, 24, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_0);
				addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_7, 24);
			}
			
		)
		
	)*	{
		// expected elements (follow set)
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_27, 25);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_5, 25, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_0);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_6, 25, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_0);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_7, 25);
	}
	
;

parse_sare_language_acm_Permission returns [sare.language.acm.Permission element = null]
@init{
}
:
	(
		(
			a0 = NAMED_ELEMENT_ID			
			{
				if (terminateParsing) {
					throw new sare.language.acm.resource.acm.mopp.AcmTerminateParsingException();
				}
				if (element == null) {
					element = sare.language.acm.AcmFactory.eINSTANCE.createPermission();
					incompleteObjects.push(element);
				}
				if (a0 != null) {
					sare.language.acm.resource.acm.IAcmTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("NAMED_ELEMENT_ID");
					tokenResolver.setOptions(getOptions());
					sare.language.acm.resource.acm.IAcmTokenResolveResult result = getFreshTokenResolveResult();
					tokenResolver.resolve(a0.getText(), element.eClass().getEStructuralFeature(sare.language.acm.AcmPackage.PERMISSION__ELEMENT), result);
					Object resolvedObject = result.getResolvedToken();
					if (resolvedObject == null) {
						addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_3_0.CommonToken) a0).getLine(), ((org.antlr.runtime3_3_0.CommonToken) a0).getCharPositionInLine(), ((org.antlr.runtime3_3_0.CommonToken) a0).getStartIndex(), ((org.antlr.runtime3_3_0.CommonToken) a0).getStopIndex());
					}
					String resolved = (String) resolvedObject;
					org.eclipse.uml2.uml.NamedElement proxy = sare.language.acm.AcmFactory.eINSTANCE.createAccessControlModel();
					collectHiddenTokens(element);
					registerContextDependentProxy(new sare.language.acm.resource.acm.mopp.AcmContextDependentURIFragmentFactory<sare.language.acm.Permission, org.eclipse.uml2.uml.NamedElement>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getPermissionElementReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(sare.language.acm.AcmPackage.PERMISSION__ELEMENT), resolved, proxy);
					if (proxy != null) {
						Object value = proxy;
						element.eSet(element.eClass().getEStructuralFeature(sare.language.acm.AcmPackage.PERMISSION__ELEMENT), value);
						completedElement(value, false);
					}
					collectHiddenTokens(element);
					retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.ACM_3_0_0_0_0_0_0, proxy, true);
					copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken) a0, element);
					copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken) a0, proxy);
				}
			}
		)
		{
			// expected elements (follow set)
			addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_29, 26);
			addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_30, 26);
			addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_22, 26, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_3);
			addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_23, 26, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_3);
			addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_24, 26);
			addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_31, 26);
		}
		
		
		|		(
			a1 = TEXT			
			{
				if (terminateParsing) {
					throw new sare.language.acm.resource.acm.mopp.AcmTerminateParsingException();
				}
				if (element == null) {
					element = sare.language.acm.AcmFactory.eINSTANCE.createPermission();
					incompleteObjects.push(element);
				}
				if (a1 != null) {
					sare.language.acm.resource.acm.IAcmTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
					tokenResolver.setOptions(getOptions());
					sare.language.acm.resource.acm.IAcmTokenResolveResult result = getFreshTokenResolveResult();
					tokenResolver.resolve(a1.getText(), element.eClass().getEStructuralFeature(sare.language.acm.AcmPackage.PERMISSION__ELEMENT), result);
					Object resolvedObject = result.getResolvedToken();
					if (resolvedObject == null) {
						addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_3_0.CommonToken) a1).getLine(), ((org.antlr.runtime3_3_0.CommonToken) a1).getCharPositionInLine(), ((org.antlr.runtime3_3_0.CommonToken) a1).getStartIndex(), ((org.antlr.runtime3_3_0.CommonToken) a1).getStopIndex());
					}
					String resolved = (String) resolvedObject;
					org.eclipse.uml2.uml.NamedElement proxy = sare.language.acm.AcmFactory.eINSTANCE.createAccessControlModel();
					collectHiddenTokens(element);
					registerContextDependentProxy(new sare.language.acm.resource.acm.mopp.AcmContextDependentURIFragmentFactory<sare.language.acm.Permission, org.eclipse.uml2.uml.NamedElement>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getPermissionElementReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(sare.language.acm.AcmPackage.PERMISSION__ELEMENT), resolved, proxy);
					if (proxy != null) {
						Object value = proxy;
						element.eSet(element.eClass().getEStructuralFeature(sare.language.acm.AcmPackage.PERMISSION__ELEMENT), value);
						completedElement(value, false);
					}
					collectHiddenTokens(element);
					retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.ACM_3_0_0_0_0_1_0, proxy, true);
					copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken) a1, element);
					copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken) a1, proxy);
				}
			}
		)
		{
			// expected elements (follow set)
			addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_29, 27);
			addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_30, 27);
			addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_22, 27, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_3);
			addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_23, 27, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_3);
			addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_24, 27);
			addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_31, 27);
		}
		
	)
	{
		// expected elements (follow set)
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_29, 28);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_30, 28);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_22, 28, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_3);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_23, 28, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_3);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_24, 28);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_31, 28);
	}
	
	(
		(
			a2 = 'if' {
				if (element == null) {
					element = sare.language.acm.AcmFactory.eINSTANCE.createPermission();
					incompleteObjects.push(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.ACM_3_0_0_1_0_0_0, null, true);
				copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken)a2, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_32, 29);
			}
			
			(
				a3 = TEXT				
				{
					if (terminateParsing) {
						throw new sare.language.acm.resource.acm.mopp.AcmTerminateParsingException();
					}
					if (element == null) {
						element = sare.language.acm.AcmFactory.eINSTANCE.createPermission();
						incompleteObjects.push(element);
					}
					if (a3 != null) {
						sare.language.acm.resource.acm.IAcmTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
						tokenResolver.setOptions(getOptions());
						sare.language.acm.resource.acm.IAcmTokenResolveResult result = getFreshTokenResolveResult();
						tokenResolver.resolve(a3.getText(), element.eClass().getEStructuralFeature(sare.language.acm.AcmPackage.PERMISSION__CONSTRAINT), result);
						Object resolvedObject = result.getResolvedToken();
						if (resolvedObject == null) {
							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_3_0.CommonToken) a3).getLine(), ((org.antlr.runtime3_3_0.CommonToken) a3).getCharPositionInLine(), ((org.antlr.runtime3_3_0.CommonToken) a3).getStartIndex(), ((org.antlr.runtime3_3_0.CommonToken) a3).getStopIndex());
						}
						java.lang.String resolved = (java.lang.String) resolvedObject;
						if (resolved != null) {
							Object value = resolved;
							element.eSet(element.eClass().getEStructuralFeature(sare.language.acm.AcmPackage.PERMISSION__CONSTRAINT), value);
							completedElement(value, false);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.ACM_3_0_0_1_0_0_1, resolved, true);
						copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken) a3, element);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_30, 30);
				addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_22, 30, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_3);
				addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_23, 30, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_3);
				addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_24, 30);
				addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_31, 30);
			}
			
		)
		
	)?	{
		// expected elements (follow set)
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_30, 31);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_22, 31, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_3);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_23, 31, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_3);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_24, 31);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_31, 31);
	}
	
	(
		(
			a4 = '{' {
				if (element == null) {
					element = sare.language.acm.AcmFactory.eINSTANCE.createPermission();
					incompleteObjects.push(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.ACM_3_0_0_2_0_0_0, null, true);
				copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken)a4, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_22, 32, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_4);
				addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_23, 32, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_4);
				addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_31, 32);
			}
			
			(
				(
					a5_0 = parse_sare_language_acm_Permission					{
						if (terminateParsing) {
							throw new sare.language.acm.resource.acm.mopp.AcmTerminateParsingException();
						}
						if (element == null) {
							element = sare.language.acm.AcmFactory.eINSTANCE.createPermission();
							incompleteObjects.push(element);
						}
						if (a5_0 != null) {
							if (a5_0 != null) {
								Object value = a5_0;
								addObjectToList(element, sare.language.acm.AcmPackage.PERMISSION__CHILD_PERMISSIONS, value);
								completedElement(value, true);
							}
							collectHiddenTokens(element);
							retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.ACM_3_0_0_2_0_0_1, a5_0, true);
							copyLocalizationInfos(a5_0, element);
						}
					}
				)
				
			)*			{
				// expected elements (follow set)
				addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_22, 33, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_4);
				addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_23, 33, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_4);
				addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_31, 33);
			}
			
			a6 = '}' {
				if (element == null) {
					element = sare.language.acm.AcmFactory.eINSTANCE.createPermission();
					incompleteObjects.push(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.ACM_3_0_0_2_0_0_2, null, true);
				copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken)a6, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_22, 34, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_3);
				addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_23, 34, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_3);
				addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_24, 34);
				addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_31, 34);
			}
			
		)
		
	)?	{
		// expected elements (follow set)
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_22, 35, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_3);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_23, 35, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_3);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_24, 35);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_31, 35);
	}
	
;

parse_org_eclipse_uml2_uml_Model returns [org.eclipse.uml2.uml.Model element = null]
@init{
}
:
	(
		(
			a0 = 'import' {
				if (element == null) {
					element = org.eclipse.uml2.uml.UMLFactory.eINSTANCE.createModel();
					incompleteObjects.push(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_0_0_0_0_0_0_0, null, true);
				copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken)a0, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_33, 36, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_5);
			}
			
			(
				a1_0 = parse_org_eclipse_uml2_uml_PackageImport				{
					if (terminateParsing) {
						throw new sare.language.acm.resource.acm.mopp.AcmTerminateParsingException();
					}
					if (element == null) {
						element = org.eclipse.uml2.uml.UMLFactory.eINSTANCE.createModel();
						incompleteObjects.push(element);
					}
					if (a1_0 != null) {
						if (a1_0 != null) {
							Object value = a1_0;
							addObjectToList(element, org.eclipse.uml2.uml.UMLPackage.MODEL__PACKAGE_IMPORT, value);
							completedElement(value, true);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_0_0_0_0_0_0_1, a1_0, true);
						copyLocalizationInfos(a1_0, element);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_8, 37);
				addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_9, 37);
			}
			
		)
		
	)*	{
		// expected elements (follow set)
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_8, 38);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_9, 38);
	}
	
	a2 = 'model' {
		if (element == null) {
			element = org.eclipse.uml2.uml.UMLFactory.eINSTANCE.createModel();
			incompleteObjects.push(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_0_0_0_1, null, true);
		copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken)a2, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_34, 39);
	}
	
	(
		a3 = TEXT		
		{
			if (terminateParsing) {
				throw new sare.language.acm.resource.acm.mopp.AcmTerminateParsingException();
			}
			if (element == null) {
				element = org.eclipse.uml2.uml.UMLFactory.eINSTANCE.createModel();
				incompleteObjects.push(element);
			}
			if (a3 != null) {
				sare.language.acm.resource.acm.IAcmTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
				tokenResolver.setOptions(getOptions());
				sare.language.acm.resource.acm.IAcmTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a3.getText(), element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.MODEL__NAME), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_3_0.CommonToken) a3).getLine(), ((org.antlr.runtime3_3_0.CommonToken) a3).getCharPositionInLine(), ((org.antlr.runtime3_3_0.CommonToken) a3).getStartIndex(), ((org.antlr.runtime3_3_0.CommonToken) a3).getStopIndex());
				}
				java.lang.String resolved = (java.lang.String) resolvedObject;
				if (resolved != null) {
					Object value = resolved;
					element.eSet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.MODEL__NAME), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_0_0_0_2, resolved, true);
				copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken) a3, element);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_35, 40);
	}
	
	a4 = '{' {
		if (element == null) {
			element = org.eclipse.uml2.uml.UMLFactory.eINSTANCE.createModel();
			incompleteObjects.push(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_0_0_0_3, null, true);
		copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken)a4, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_0, 41, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_1);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_1, 41, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_1);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_8, 41, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_1);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_9, 41, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_1);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_10, 41, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_1);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_11, 41, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_2, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_1);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_12, 41, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_2, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_1);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_13, 41, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_1);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_14, 41);
	}
	
	(
		(
			a5_0 = parse_org_eclipse_uml2_uml_PackageableElement			{
				if (terminateParsing) {
					throw new sare.language.acm.resource.acm.mopp.AcmTerminateParsingException();
				}
				if (element == null) {
					element = org.eclipse.uml2.uml.UMLFactory.eINSTANCE.createModel();
					incompleteObjects.push(element);
				}
				if (a5_0 != null) {
					if (a5_0 != null) {
						Object value = a5_0;
						addObjectToList(element, org.eclipse.uml2.uml.UMLPackage.MODEL__PACKAGED_ELEMENT, value);
						completedElement(value, true);
					}
					collectHiddenTokens(element);
					retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_0_0_0_4, a5_0, true);
					copyLocalizationInfos(a5_0, element);
				}
			}
		)
		
	)*	{
		// expected elements (follow set)
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_0, 42, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_1);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_1, 42, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_1);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_8, 42, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_1);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_9, 42, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_1);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_10, 42, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_1);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_11, 42, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_2, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_1);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_12, 42, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_2, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_1);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_13, 42, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_1);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_14, 42);
	}
	
	a6 = '}' {
		if (element == null) {
			element = org.eclipse.uml2.uml.UMLFactory.eINSTANCE.createModel();
			incompleteObjects.push(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_0_0_0_5, null, true);
		copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken)a6, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_0, 43, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_1);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_1, 43, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_1);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_8, 43, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_1);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_9, 43, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_1);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_10, 43, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_1);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_11, 43, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_2, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_1);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_12, 43, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_2, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_1);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_13, 43, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_1);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_14, 43);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_15, 43);
	}
	
;

parse_org_eclipse_uml2_uml_PackageImport returns [org.eclipse.uml2.uml.PackageImport element = null]
@init{
}
:
	(
		a0 = QUOTED_34_34		
		{
			if (terminateParsing) {
				throw new sare.language.acm.resource.acm.mopp.AcmTerminateParsingException();
			}
			if (element == null) {
				element = org.eclipse.uml2.uml.UMLFactory.eINSTANCE.createPackageImport();
				incompleteObjects.push(element);
			}
			if (a0 != null) {
				sare.language.acm.resource.acm.IAcmTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("QUOTED_34_34");
				tokenResolver.setOptions(getOptions());
				sare.language.acm.resource.acm.IAcmTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a0.getText(), element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.PACKAGE_IMPORT__IMPORTED_PACKAGE), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_3_0.CommonToken) a0).getLine(), ((org.antlr.runtime3_3_0.CommonToken) a0).getCharPositionInLine(), ((org.antlr.runtime3_3_0.CommonToken) a0).getStartIndex(), ((org.antlr.runtime3_3_0.CommonToken) a0).getStopIndex());
				}
				String resolved = (String) resolvedObject;
				org.eclipse.uml2.uml.Package proxy = org.eclipse.uml2.uml.UMLFactory.eINSTANCE.createPackage();
				collectHiddenTokens(element);
				registerContextDependentProxy(new sare.language.acm.resource.acm.mopp.AcmContextDependentURIFragmentFactory<org.eclipse.uml2.uml.PackageImport, org.eclipse.uml2.uml.Package>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getPackageImportImportedPackageReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.PACKAGE_IMPORT__IMPORTED_PACKAGE), resolved, proxy);
				if (proxy != null) {
					Object value = proxy;
					element.eSet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.PACKAGE_IMPORT__IMPORTED_PACKAGE), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_1_0_0_0, proxy, true);
				copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken) a0, element);
				copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken) a0, proxy);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_8, 44);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_9, 44);
	}
	
;

parse_org_eclipse_uml2_uml_Package returns [org.eclipse.uml2.uml.Package element = null]
@init{
}
:
	a0 = 'package' {
		if (element == null) {
			element = org.eclipse.uml2.uml.UMLFactory.eINSTANCE.createPackage();
			incompleteObjects.push(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_2_0_0_0, null, true);
		copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken)a0, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_36, 45);
	}
	
	(
		a1 = TEXT		
		{
			if (terminateParsing) {
				throw new sare.language.acm.resource.acm.mopp.AcmTerminateParsingException();
			}
			if (element == null) {
				element = org.eclipse.uml2.uml.UMLFactory.eINSTANCE.createPackage();
				incompleteObjects.push(element);
			}
			if (a1 != null) {
				sare.language.acm.resource.acm.IAcmTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
				tokenResolver.setOptions(getOptions());
				sare.language.acm.resource.acm.IAcmTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a1.getText(), element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.PACKAGE__NAME), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_3_0.CommonToken) a1).getLine(), ((org.antlr.runtime3_3_0.CommonToken) a1).getCharPositionInLine(), ((org.antlr.runtime3_3_0.CommonToken) a1).getStartIndex(), ((org.antlr.runtime3_3_0.CommonToken) a1).getStopIndex());
				}
				java.lang.String resolved = (java.lang.String) resolvedObject;
				if (resolved != null) {
					Object value = resolved;
					element.eSet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.PACKAGE__NAME), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_2_0_0_1, resolved, true);
				copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken) a1, element);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_37, 46);
	}
	
	a2 = '{' {
		if (element == null) {
			element = org.eclipse.uml2.uml.UMLFactory.eINSTANCE.createPackage();
			incompleteObjects.push(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_2_0_0_2, null, true);
		copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken)a2, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_0, 47, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_1);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_1, 47, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_1);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_8, 47, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_1);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_9, 47, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_1);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_10, 47, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_1);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_11, 47, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_2, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_1);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_12, 47, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_2, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_1);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_13, 47, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_1);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_15, 47);
	}
	
	(
		(
			a3_0 = parse_org_eclipse_uml2_uml_PackageableElement			{
				if (terminateParsing) {
					throw new sare.language.acm.resource.acm.mopp.AcmTerminateParsingException();
				}
				if (element == null) {
					element = org.eclipse.uml2.uml.UMLFactory.eINSTANCE.createPackage();
					incompleteObjects.push(element);
				}
				if (a3_0 != null) {
					if (a3_0 != null) {
						Object value = a3_0;
						addObjectToList(element, org.eclipse.uml2.uml.UMLPackage.PACKAGE__PACKAGED_ELEMENT, value);
						completedElement(value, true);
					}
					collectHiddenTokens(element);
					retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_2_0_0_3, a3_0, true);
					copyLocalizationInfos(a3_0, element);
				}
			}
		)
		
	)*	{
		// expected elements (follow set)
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_0, 48, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_1);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_1, 48, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_1);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_8, 48, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_1);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_9, 48, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_1);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_10, 48, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_1);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_11, 48, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_2, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_1);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_12, 48, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_2, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_1);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_13, 48, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_1);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_15, 48);
	}
	
	a4 = '}' {
		if (element == null) {
			element = org.eclipse.uml2.uml.UMLFactory.eINSTANCE.createPackage();
			incompleteObjects.push(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_2_0_0_4, null, true);
		copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken)a4, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_0, 49, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_1);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_1, 49, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_1);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_8, 49, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_1);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_9, 49, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_1);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_10, 49, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_1);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_11, 49, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_2, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_1);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_12, 49, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_2, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_1);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_13, 49, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_1);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_14, 49);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_15, 49);
	}
	
	|//derived choice rules for sub-classes: 
	
	c0 = parse_org_eclipse_uml2_uml_Model{ element = c0; /* this is a subclass or primitive expression choice */ }
	
;

parse_org_eclipse_uml2_uml_Class returns [org.eclipse.uml2.uml.Class element = null]
@init{
}
:
	(
		(
			a0_0 = parse_org_eclipse_uml2_uml_Comment			{
				if (terminateParsing) {
					throw new sare.language.acm.resource.acm.mopp.AcmTerminateParsingException();
				}
				if (element == null) {
					element = org.eclipse.uml2.uml.UMLFactory.eINSTANCE.createClass();
					incompleteObjects.push(element);
				}
				if (a0_0 != null) {
					if (a0_0 != null) {
						Object value = a0_0;
						addObjectToList(element, org.eclipse.uml2.uml.UMLPackage.CLASS__OWNED_COMMENT, value);
						completedElement(value, true);
					}
					collectHiddenTokens(element);
					retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_3_0_0_0, a0_0, true);
					copyLocalizationInfos(a0_0, element);
				}
			}
		)
		
	)*	{
		// expected elements (follow set)
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_11, 50, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_2);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_12, 50, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_2);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_13, 50);
	}
	
	a1 = 'class' {
		if (element == null) {
			element = org.eclipse.uml2.uml.UMLFactory.eINSTANCE.createClass();
			incompleteObjects.push(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_3_0_0_1, null, true);
		copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken)a1, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_38, 51);
	}
	
	(
		a2 = TEXT		
		{
			if (terminateParsing) {
				throw new sare.language.acm.resource.acm.mopp.AcmTerminateParsingException();
			}
			if (element == null) {
				element = org.eclipse.uml2.uml.UMLFactory.eINSTANCE.createClass();
				incompleteObjects.push(element);
			}
			if (a2 != null) {
				sare.language.acm.resource.acm.IAcmTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
				tokenResolver.setOptions(getOptions());
				sare.language.acm.resource.acm.IAcmTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a2.getText(), element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.CLASS__NAME), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_3_0.CommonToken) a2).getLine(), ((org.antlr.runtime3_3_0.CommonToken) a2).getCharPositionInLine(), ((org.antlr.runtime3_3_0.CommonToken) a2).getStartIndex(), ((org.antlr.runtime3_3_0.CommonToken) a2).getStopIndex());
				}
				java.lang.String resolved = (java.lang.String) resolvedObject;
				if (resolved != null) {
					Object value = resolved;
					element.eSet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.CLASS__NAME), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_3_0_0_2, resolved, true);
				copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken) a2, element);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_39, 52);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_40, 52);
	}
	
	(
		(
			a3 = 'extends' {
				if (element == null) {
					element = org.eclipse.uml2.uml.UMLFactory.eINSTANCE.createClass();
					incompleteObjects.push(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_3_0_0_3_0_0_0, null, true);
				copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken)a3, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_41, 53);
			}
			
			(
				a4 = TEXT				
				{
					if (terminateParsing) {
						throw new sare.language.acm.resource.acm.mopp.AcmTerminateParsingException();
					}
					if (element == null) {
						element = org.eclipse.uml2.uml.UMLFactory.eINSTANCE.createClass();
						incompleteObjects.push(element);
					}
					if (a4 != null) {
						sare.language.acm.resource.acm.IAcmTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
						tokenResolver.setOptions(getOptions());
						sare.language.acm.resource.acm.IAcmTokenResolveResult result = getFreshTokenResolveResult();
						tokenResolver.resolve(a4.getText(), element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.CLASS__SUPER_CLASS), result);
						Object resolvedObject = result.getResolvedToken();
						if (resolvedObject == null) {
							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_3_0.CommonToken) a4).getLine(), ((org.antlr.runtime3_3_0.CommonToken) a4).getCharPositionInLine(), ((org.antlr.runtime3_3_0.CommonToken) a4).getStartIndex(), ((org.antlr.runtime3_3_0.CommonToken) a4).getStopIndex());
						}
						String resolved = (String) resolvedObject;
						org.eclipse.uml2.uml.Class proxy = org.eclipse.uml2.uml.UMLFactory.eINSTANCE.createClass();
						collectHiddenTokens(element);
						registerContextDependentProxy(new sare.language.acm.resource.acm.mopp.AcmContextDependentURIFragmentFactory<org.eclipse.uml2.uml.Class, org.eclipse.uml2.uml.Class>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getClassSuperClassReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.CLASS__SUPER_CLASS), resolved, proxy);
						if (proxy != null) {
							Object value = proxy;
							addObjectToList(element, org.eclipse.uml2.uml.UMLPackage.CLASS__SUPER_CLASS, value);
							completedElement(value, false);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_3_0_0_3_0_0_1, proxy, true);
						copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken) a4, element);
						copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken) a4, proxy);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_42, 54);
				addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_40, 54);
			}
			
			(
				(
					a5 = ',' {
						if (element == null) {
							element = org.eclipse.uml2.uml.UMLFactory.eINSTANCE.createClass();
							incompleteObjects.push(element);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_3_0_0_3_0_0_2_0_0_0, null, true);
						copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken)a5, element);
					}
					{
						// expected elements (follow set)
						addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_43, 55);
					}
					
					(
						a6 = TEXT						
						{
							if (terminateParsing) {
								throw new sare.language.acm.resource.acm.mopp.AcmTerminateParsingException();
							}
							if (element == null) {
								element = org.eclipse.uml2.uml.UMLFactory.eINSTANCE.createClass();
								incompleteObjects.push(element);
							}
							if (a6 != null) {
								sare.language.acm.resource.acm.IAcmTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
								tokenResolver.setOptions(getOptions());
								sare.language.acm.resource.acm.IAcmTokenResolveResult result = getFreshTokenResolveResult();
								tokenResolver.resolve(a6.getText(), element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.CLASS__SUPER_CLASS), result);
								Object resolvedObject = result.getResolvedToken();
								if (resolvedObject == null) {
									addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_3_0.CommonToken) a6).getLine(), ((org.antlr.runtime3_3_0.CommonToken) a6).getCharPositionInLine(), ((org.antlr.runtime3_3_0.CommonToken) a6).getStartIndex(), ((org.antlr.runtime3_3_0.CommonToken) a6).getStopIndex());
								}
								String resolved = (String) resolvedObject;
								org.eclipse.uml2.uml.Class proxy = org.eclipse.uml2.uml.UMLFactory.eINSTANCE.createClass();
								collectHiddenTokens(element);
								registerContextDependentProxy(new sare.language.acm.resource.acm.mopp.AcmContextDependentURIFragmentFactory<org.eclipse.uml2.uml.Class, org.eclipse.uml2.uml.Class>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getClassSuperClassReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.CLASS__SUPER_CLASS), resolved, proxy);
								if (proxy != null) {
									Object value = proxy;
									addObjectToList(element, org.eclipse.uml2.uml.UMLPackage.CLASS__SUPER_CLASS, value);
									completedElement(value, false);
								}
								collectHiddenTokens(element);
								retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_3_0_0_3_0_0_2_0_0_1, proxy, true);
								copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken) a6, element);
								copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken) a6, proxy);
							}
						}
					)
					{
						// expected elements (follow set)
						addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_42, 56);
						addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_40, 56);
					}
					
				)
				
			)*			{
				// expected elements (follow set)
				addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_42, 57);
				addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_40, 57);
			}
			
		)
		
	)?	{
		// expected elements (follow set)
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_40, 58);
	}
	
	a7 = '{' {
		if (element == null) {
			element = org.eclipse.uml2.uml.UMLFactory.eINSTANCE.createClass();
			incompleteObjects.push(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_3_0_0_4, null, true);
		copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken)a7, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_11, 59, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_2, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_6);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_12, 59, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_2, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_6);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_44, 59, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_6);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_45, 59, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_7);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_46, 59);
	}
	
	(
		(
			(
				a8_0 = parse_org_eclipse_uml2_uml_Property				{
					if (terminateParsing) {
						throw new sare.language.acm.resource.acm.mopp.AcmTerminateParsingException();
					}
					if (element == null) {
						element = org.eclipse.uml2.uml.UMLFactory.eINSTANCE.createClass();
						incompleteObjects.push(element);
					}
					if (a8_0 != null) {
						if (a8_0 != null) {
							Object value = a8_0;
							addObjectToList(element, org.eclipse.uml2.uml.UMLPackage.CLASS__OWNED_ATTRIBUTE, value);
							completedElement(value, true);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_3_0_0_5_0_0_0, a8_0, true);
						copyLocalizationInfos(a8_0, element);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_11, 60, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_2, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_6);
				addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_12, 60, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_2, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_6);
				addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_44, 60, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_6);
				addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_45, 60, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_7);
				addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_46, 60);
			}
			
			
			|			(
				a9_0 = parse_org_eclipse_uml2_uml_Operation				{
					if (terminateParsing) {
						throw new sare.language.acm.resource.acm.mopp.AcmTerminateParsingException();
					}
					if (element == null) {
						element = org.eclipse.uml2.uml.UMLFactory.eINSTANCE.createClass();
						incompleteObjects.push(element);
					}
					if (a9_0 != null) {
						if (a9_0 != null) {
							Object value = a9_0;
							addObjectToList(element, org.eclipse.uml2.uml.UMLPackage.CLASS__OWNED_OPERATION, value);
							completedElement(value, true);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_3_0_0_5_0_1_0, a9_0, true);
						copyLocalizationInfos(a9_0, element);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_11, 61, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_2, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_6);
				addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_12, 61, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_2, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_6);
				addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_44, 61, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_6);
				addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_45, 61, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_7);
				addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_46, 61);
			}
			
		)
		
	)*	{
		// expected elements (follow set)
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_11, 62, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_2, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_6);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_12, 62, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_2, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_6);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_44, 62, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_6);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_45, 62, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_7);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_46, 62);
	}
	
	a10 = '}' {
		if (element == null) {
			element = org.eclipse.uml2.uml.UMLFactory.eINSTANCE.createClass();
			incompleteObjects.push(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_3_0_0_6, null, true);
		copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken)a10, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_0, 63, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_1);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_1, 63, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_1);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_8, 63, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_1);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_9, 63, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_1);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_10, 63, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_1);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_11, 63, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_2, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_1);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_12, 63, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_2, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_1);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_13, 63, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_1);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_14, 63);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_15, 63);
	}
	
;

parse_org_eclipse_uml2_uml_Operation returns [org.eclipse.uml2.uml.Operation element = null]
@init{
}
:
	(
		(
			a0_0 = parse_org_eclipse_uml2_uml_Comment			{
				if (terminateParsing) {
					throw new sare.language.acm.resource.acm.mopp.AcmTerminateParsingException();
				}
				if (element == null) {
					element = org.eclipse.uml2.uml.UMLFactory.eINSTANCE.createOperation();
					incompleteObjects.push(element);
				}
				if (a0_0 != null) {
					if (a0_0 != null) {
						Object value = a0_0;
						addObjectToList(element, org.eclipse.uml2.uml.UMLPackage.OPERATION__OWNED_COMMENT, value);
						completedElement(value, true);
					}
					collectHiddenTokens(element);
					retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_4_0_0_0, a0_0, true);
					copyLocalizationInfos(a0_0, element);
				}
			}
		)
		
	)*	{
		// expected elements (follow set)
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_11, 64, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_2);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_12, 64, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_2);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_45, 64);
	}
	
	(
		a1 = TEXT		
		{
			if (terminateParsing) {
				throw new sare.language.acm.resource.acm.mopp.AcmTerminateParsingException();
			}
			if (element == null) {
				element = org.eclipse.uml2.uml.UMLFactory.eINSTANCE.createOperation();
				incompleteObjects.push(element);
			}
			if (a1 != null) {
				sare.language.acm.resource.acm.IAcmTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
				tokenResolver.setOptions(getOptions());
				sare.language.acm.resource.acm.IAcmTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a1.getText(), element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.OPERATION__NAME), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_3_0.CommonToken) a1).getLine(), ((org.antlr.runtime3_3_0.CommonToken) a1).getCharPositionInLine(), ((org.antlr.runtime3_3_0.CommonToken) a1).getStartIndex(), ((org.antlr.runtime3_3_0.CommonToken) a1).getStopIndex());
				}
				java.lang.String resolved = (java.lang.String) resolvedObject;
				if (resolved != null) {
					Object value = resolved;
					element.eSet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.OPERATION__NAME), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_4_0_0_1, resolved, true);
				copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken) a1, element);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_47, 65);
	}
	
	a2 = '(' {
		if (element == null) {
			element = org.eclipse.uml2.uml.UMLFactory.eINSTANCE.createOperation();
			incompleteObjects.push(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_4_0_0_2, null, true);
		copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken)a2, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_11, 66, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_2, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_8);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_12, 66, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_2, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_8);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_48, 66, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_8);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_49, 66);
	}
	
	(
		(
			(
				a3_0 = parse_org_eclipse_uml2_uml_Parameter				{
					if (terminateParsing) {
						throw new sare.language.acm.resource.acm.mopp.AcmTerminateParsingException();
					}
					if (element == null) {
						element = org.eclipse.uml2.uml.UMLFactory.eINSTANCE.createOperation();
						incompleteObjects.push(element);
					}
					if (a3_0 != null) {
						if (a3_0 != null) {
							Object value = a3_0;
							addObjectToList(element, org.eclipse.uml2.uml.UMLPackage.OPERATION__OWNED_PARAMETER, value);
							completedElement(value, true);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_4_0_0_3_0_0_0, a3_0, true);
						copyLocalizationInfos(a3_0, element);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_50, 67);
				addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_49, 67);
			}
			
			(
				(
					a4 = ',' {
						if (element == null) {
							element = org.eclipse.uml2.uml.UMLFactory.eINSTANCE.createOperation();
							incompleteObjects.push(element);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_4_0_0_3_0_0_1_0_0_0, null, true);
						copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken)a4, element);
					}
					{
						// expected elements (follow set)
						addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_11, 68, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_2, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_8);
						addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_12, 68, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_2, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_8);
						addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_48, 68, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_8);
					}
					
					(
						a5_0 = parse_org_eclipse_uml2_uml_Parameter						{
							if (terminateParsing) {
								throw new sare.language.acm.resource.acm.mopp.AcmTerminateParsingException();
							}
							if (element == null) {
								element = org.eclipse.uml2.uml.UMLFactory.eINSTANCE.createOperation();
								incompleteObjects.push(element);
							}
							if (a5_0 != null) {
								if (a5_0 != null) {
									Object value = a5_0;
									addObjectToList(element, org.eclipse.uml2.uml.UMLPackage.OPERATION__OWNED_PARAMETER, value);
									completedElement(value, true);
								}
								collectHiddenTokens(element);
								retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_4_0_0_3_0_0_1_0_0_1, a5_0, true);
								copyLocalizationInfos(a5_0, element);
							}
						}
					)
					{
						// expected elements (follow set)
						addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_50, 69);
						addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_49, 69);
					}
					
				)
				
			)*			{
				// expected elements (follow set)
				addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_50, 70);
				addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_49, 70);
			}
			
		)
		
	)?	{
		// expected elements (follow set)
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_49, 71);
	}
	
	a6 = ')' {
		if (element == null) {
			element = org.eclipse.uml2.uml.UMLFactory.eINSTANCE.createOperation();
			incompleteObjects.push(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_4_0_0_4, null, true);
		copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken)a6, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_51, 72);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_11, 72, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_2, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_6);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_12, 72, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_2, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_6);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_44, 72, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_6);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_45, 72, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_7);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_46, 72);
	}
	
	(
		(
			a7 = ':' {
				if (element == null) {
					element = org.eclipse.uml2.uml.UMLFactory.eINSTANCE.createOperation();
					incompleteObjects.push(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_4_0_0_5_0_0_0, null, true);
				copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken)a7, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_52, 73);
			}
			
			(
				a8 = TEXT				
				{
					if (terminateParsing) {
						throw new sare.language.acm.resource.acm.mopp.AcmTerminateParsingException();
					}
					if (element == null) {
						element = org.eclipse.uml2.uml.UMLFactory.eINSTANCE.createOperation();
						incompleteObjects.push(element);
					}
					if (a8 != null) {
						sare.language.acm.resource.acm.IAcmTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
						tokenResolver.setOptions(getOptions());
						sare.language.acm.resource.acm.IAcmTokenResolveResult result = getFreshTokenResolveResult();
						tokenResolver.resolve(a8.getText(), element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.OPERATION__TYPE), result);
						Object resolvedObject = result.getResolvedToken();
						if (resolvedObject == null) {
							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_3_0.CommonToken) a8).getLine(), ((org.antlr.runtime3_3_0.CommonToken) a8).getCharPositionInLine(), ((org.antlr.runtime3_3_0.CommonToken) a8).getStartIndex(), ((org.antlr.runtime3_3_0.CommonToken) a8).getStopIndex());
						}
						String resolved = (String) resolvedObject;
						org.eclipse.uml2.uml.Type proxy = org.eclipse.uml2.uml.UMLFactory.eINSTANCE.createAssociation();
						collectHiddenTokens(element);
						registerContextDependentProxy(new sare.language.acm.resource.acm.mopp.AcmContextDependentURIFragmentFactory<org.eclipse.uml2.uml.Operation, org.eclipse.uml2.uml.Type>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getOperationTypeReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.OPERATION__TYPE), resolved, proxy);
						if (proxy != null) {
							Object value = proxy;
							element.eSet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.OPERATION__TYPE), value);
							completedElement(value, false);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_4_0_0_5_0_0_1, proxy, true);
						copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken) a8, element);
						copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken) a8, proxy);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_11, 74, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_2, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_6);
				addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_12, 74, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_2, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_6);
				addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_44, 74, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_6);
				addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_45, 74, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_7);
				addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_46, 74);
			}
			
		)
		
	)?	{
		// expected elements (follow set)
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_11, 75, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_2, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_6);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_12, 75, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_2, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_6);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_44, 75, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_6);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_45, 75, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_7);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_46, 75);
	}
	
;

parse_org_eclipse_uml2_uml_Property returns [org.eclipse.uml2.uml.Property element = null]
@init{
}
:
	(
		(
			a0_0 = parse_org_eclipse_uml2_uml_Comment			{
				if (terminateParsing) {
					throw new sare.language.acm.resource.acm.mopp.AcmTerminateParsingException();
				}
				if (element == null) {
					element = org.eclipse.uml2.uml.UMLFactory.eINSTANCE.createProperty();
					incompleteObjects.push(element);
				}
				if (a0_0 != null) {
					if (a0_0 != null) {
						Object value = a0_0;
						addObjectToList(element, org.eclipse.uml2.uml.UMLPackage.PROPERTY__OWNED_COMMENT, value);
						completedElement(value, true);
					}
					collectHiddenTokens(element);
					retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_5_0_0_0, a0_0, true);
					copyLocalizationInfos(a0_0, element);
				}
			}
		)
		
	)*	{
		// expected elements (follow set)
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_11, 76, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_2);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_12, 76, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_2);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_44, 76);
	}
	
	(
		a1 = TEXT		
		{
			if (terminateParsing) {
				throw new sare.language.acm.resource.acm.mopp.AcmTerminateParsingException();
			}
			if (element == null) {
				element = org.eclipse.uml2.uml.UMLFactory.eINSTANCE.createProperty();
				incompleteObjects.push(element);
			}
			if (a1 != null) {
				sare.language.acm.resource.acm.IAcmTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
				tokenResolver.setOptions(getOptions());
				sare.language.acm.resource.acm.IAcmTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a1.getText(), element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.PROPERTY__NAME), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_3_0.CommonToken) a1).getLine(), ((org.antlr.runtime3_3_0.CommonToken) a1).getCharPositionInLine(), ((org.antlr.runtime3_3_0.CommonToken) a1).getStartIndex(), ((org.antlr.runtime3_3_0.CommonToken) a1).getStopIndex());
				}
				java.lang.String resolved = (java.lang.String) resolvedObject;
				if (resolved != null) {
					Object value = resolved;
					element.eSet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.PROPERTY__NAME), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_5_0_0_1, resolved, true);
				copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken) a1, element);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_53, 77);
	}
	
	a2 = ':' {
		if (element == null) {
			element = org.eclipse.uml2.uml.UMLFactory.eINSTANCE.createProperty();
			incompleteObjects.push(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_5_0_0_2, null, true);
		copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken)a2, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_54, 78);
	}
	
	(
		a3 = TEXT		
		{
			if (terminateParsing) {
				throw new sare.language.acm.resource.acm.mopp.AcmTerminateParsingException();
			}
			if (element == null) {
				element = org.eclipse.uml2.uml.UMLFactory.eINSTANCE.createProperty();
				incompleteObjects.push(element);
			}
			if (a3 != null) {
				sare.language.acm.resource.acm.IAcmTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
				tokenResolver.setOptions(getOptions());
				sare.language.acm.resource.acm.IAcmTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a3.getText(), element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.PROPERTY__TYPE), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_3_0.CommonToken) a3).getLine(), ((org.antlr.runtime3_3_0.CommonToken) a3).getCharPositionInLine(), ((org.antlr.runtime3_3_0.CommonToken) a3).getStartIndex(), ((org.antlr.runtime3_3_0.CommonToken) a3).getStopIndex());
				}
				String resolved = (String) resolvedObject;
				org.eclipse.uml2.uml.Type proxy = org.eclipse.uml2.uml.UMLFactory.eINSTANCE.createAssociation();
				collectHiddenTokens(element);
				registerContextDependentProxy(new sare.language.acm.resource.acm.mopp.AcmContextDependentURIFragmentFactory<org.eclipse.uml2.uml.TypedElement, org.eclipse.uml2.uml.Type>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getTypedElementTypeReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.PROPERTY__TYPE), resolved, proxy);
				if (proxy != null) {
					Object value = proxy;
					element.eSet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.PROPERTY__TYPE), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_5_0_0_3, proxy, true);
				copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken) a3, element);
				copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken) a3, proxy);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_11, 79, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_2, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_6);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_12, 79, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_2, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_6);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_44, 79, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_6);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_45, 79, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_7);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_46, 79);
	}
	
;

parse_org_eclipse_uml2_uml_Parameter returns [org.eclipse.uml2.uml.Parameter element = null]
@init{
}
:
	(
		(
			a0_0 = parse_org_eclipse_uml2_uml_Comment			{
				if (terminateParsing) {
					throw new sare.language.acm.resource.acm.mopp.AcmTerminateParsingException();
				}
				if (element == null) {
					element = org.eclipse.uml2.uml.UMLFactory.eINSTANCE.createParameter();
					incompleteObjects.push(element);
				}
				if (a0_0 != null) {
					if (a0_0 != null) {
						Object value = a0_0;
						addObjectToList(element, org.eclipse.uml2.uml.UMLPackage.PARAMETER__OWNED_COMMENT, value);
						completedElement(value, true);
					}
					collectHiddenTokens(element);
					retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_6_0_0_0, a0_0, true);
					copyLocalizationInfos(a0_0, element);
				}
			}
		)
		
	)*	{
		// expected elements (follow set)
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_11, 80, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_2);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_12, 80, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_2);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_48, 80);
	}
	
	(
		a1 = TEXT		
		{
			if (terminateParsing) {
				throw new sare.language.acm.resource.acm.mopp.AcmTerminateParsingException();
			}
			if (element == null) {
				element = org.eclipse.uml2.uml.UMLFactory.eINSTANCE.createParameter();
				incompleteObjects.push(element);
			}
			if (a1 != null) {
				sare.language.acm.resource.acm.IAcmTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
				tokenResolver.setOptions(getOptions());
				sare.language.acm.resource.acm.IAcmTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a1.getText(), element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.PARAMETER__NAME), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_3_0.CommonToken) a1).getLine(), ((org.antlr.runtime3_3_0.CommonToken) a1).getCharPositionInLine(), ((org.antlr.runtime3_3_0.CommonToken) a1).getStartIndex(), ((org.antlr.runtime3_3_0.CommonToken) a1).getStopIndex());
				}
				java.lang.String resolved = (java.lang.String) resolvedObject;
				if (resolved != null) {
					Object value = resolved;
					element.eSet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.PARAMETER__NAME), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_6_0_0_1, resolved, true);
				copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken) a1, element);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_55, 81);
	}
	
	a2 = ':' {
		if (element == null) {
			element = org.eclipse.uml2.uml.UMLFactory.eINSTANCE.createParameter();
			incompleteObjects.push(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_6_0_0_2, null, true);
		copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken)a2, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_56, 82);
	}
	
	(
		a3 = TEXT		
		{
			if (terminateParsing) {
				throw new sare.language.acm.resource.acm.mopp.AcmTerminateParsingException();
			}
			if (element == null) {
				element = org.eclipse.uml2.uml.UMLFactory.eINSTANCE.createParameter();
				incompleteObjects.push(element);
			}
			if (a3 != null) {
				sare.language.acm.resource.acm.IAcmTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
				tokenResolver.setOptions(getOptions());
				sare.language.acm.resource.acm.IAcmTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a3.getText(), element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.PARAMETER__TYPE), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_3_0.CommonToken) a3).getLine(), ((org.antlr.runtime3_3_0.CommonToken) a3).getCharPositionInLine(), ((org.antlr.runtime3_3_0.CommonToken) a3).getStartIndex(), ((org.antlr.runtime3_3_0.CommonToken) a3).getStopIndex());
				}
				String resolved = (String) resolvedObject;
				org.eclipse.uml2.uml.Type proxy = org.eclipse.uml2.uml.UMLFactory.eINSTANCE.createAssociation();
				collectHiddenTokens(element);
				registerContextDependentProxy(new sare.language.acm.resource.acm.mopp.AcmContextDependentURIFragmentFactory<org.eclipse.uml2.uml.TypedElement, org.eclipse.uml2.uml.Type>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getTypedElementTypeReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.PARAMETER__TYPE), resolved, proxy);
				if (proxy != null) {
					Object value = proxy;
					element.eSet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.PARAMETER__TYPE), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_6_0_0_3, proxy, true);
				copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken) a3, element);
				copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken) a3, proxy);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_50, 83);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_49, 83);
	}
	
;

parse_org_eclipse_uml2_uml_Comment returns [org.eclipse.uml2.uml.Comment element = null]
@init{
}
:
	a0 = '\u00ab' {
		if (element == null) {
			element = org.eclipse.uml2.uml.UMLFactory.eINSTANCE.createComment();
			incompleteObjects.push(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_7_0_0_0, null, true);
		copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken)a0, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_57, 84);
	}
	
	(
		a1 = TEXT		
		{
			if (terminateParsing) {
				throw new sare.language.acm.resource.acm.mopp.AcmTerminateParsingException();
			}
			if (element == null) {
				element = org.eclipse.uml2.uml.UMLFactory.eINSTANCE.createComment();
				incompleteObjects.push(element);
			}
			if (a1 != null) {
				sare.language.acm.resource.acm.IAcmTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
				tokenResolver.setOptions(getOptions());
				sare.language.acm.resource.acm.IAcmTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a1.getText(), element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.COMMENT__BODY), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_3_0.CommonToken) a1).getLine(), ((org.antlr.runtime3_3_0.CommonToken) a1).getCharPositionInLine(), ((org.antlr.runtime3_3_0.CommonToken) a1).getStartIndex(), ((org.antlr.runtime3_3_0.CommonToken) a1).getStopIndex());
				}
				java.lang.String resolved = (java.lang.String) resolvedObject;
				if (resolved != null) {
					Object value = resolved;
					element.eSet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.COMMENT__BODY), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_7_0_0_1, resolved, true);
				copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken) a1, element);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_58, 85);
	}
	
	a2 = '\u00bb' {
		if (element == null) {
			element = org.eclipse.uml2.uml.UMLFactory.eINSTANCE.createComment();
			incompleteObjects.push(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_7_0_0_2, null, true);
		copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken)a2, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_11, 86, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_2);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_12, 86, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_2);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_13, 86);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_45, 86);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_44, 86);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_48, 86);
	}
	
	
	|	a3 = '<<' {
		if (element == null) {
			element = org.eclipse.uml2.uml.UMLFactory.eINSTANCE.createComment();
			incompleteObjects.push(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_7_0_1_0, null, true);
		copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken)a3, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_59, 87);
	}
	
	(
		a4 = TEXT		
		{
			if (terminateParsing) {
				throw new sare.language.acm.resource.acm.mopp.AcmTerminateParsingException();
			}
			if (element == null) {
				element = org.eclipse.uml2.uml.UMLFactory.eINSTANCE.createComment();
				incompleteObjects.push(element);
			}
			if (a4 != null) {
				sare.language.acm.resource.acm.IAcmTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
				tokenResolver.setOptions(getOptions());
				sare.language.acm.resource.acm.IAcmTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a4.getText(), element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.COMMENT__BODY), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_3_0.CommonToken) a4).getLine(), ((org.antlr.runtime3_3_0.CommonToken) a4).getCharPositionInLine(), ((org.antlr.runtime3_3_0.CommonToken) a4).getStartIndex(), ((org.antlr.runtime3_3_0.CommonToken) a4).getStopIndex());
				}
				java.lang.String resolved = (java.lang.String) resolvedObject;
				if (resolved != null) {
					Object value = resolved;
					element.eSet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.COMMENT__BODY), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_7_0_1_1, resolved, true);
				copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken) a4, element);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_60, 88);
	}
	
	a5 = '>>' {
		if (element == null) {
			element = org.eclipse.uml2.uml.UMLFactory.eINSTANCE.createComment();
			incompleteObjects.push(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_7_0_1_2, null, true);
		copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken)a5, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_11, 89, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_2);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_12, 89, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_2);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_13, 89);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_45, 89);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_44, 89);
		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_48, 89);
	}
	
;

parse_sare_language_acm_Subject returns [sare.language.acm.Subject element = null]
:
	c0 = parse_sare_language_acm_Role{ element = c0; /* this is a subclass or primitive expression choice */ }
	|	c1 = parse_sare_language_acm_User{ element = c1; /* this is a subclass or primitive expression choice */ }
	
;

parse_org_eclipse_uml2_uml_PackageableElement returns [org.eclipse.uml2.uml.PackageableElement element = null]
:
	c0 = parse_sare_language_acm_AccessControlModel{ element = c0; /* this is a subclass or primitive expression choice */ }
	|	c1 = parse_org_eclipse_uml2_uml_Model{ element = c1; /* this is a subclass or primitive expression choice */ }
	|	c2 = parse_org_eclipse_uml2_uml_Package{ element = c2; /* this is a subclass or primitive expression choice */ }
	|	c3 = parse_org_eclipse_uml2_uml_Class{ element = c3; /* this is a subclass or primitive expression choice */ }
	
;

TEXT:
	(('A'..'Z' | 'a'..'z' | '0'..'9' | '_' | '-' )+)
;
NAMED_ELEMENT_ID:
	( (('A'..'Z' | 'a'..'z' | '0'..'9' | '_' | '-' )+|'.'|'('|')'|','|'~')+)
;
WHITESPACE:
	((' ' | '\t' | '\f'))
	{ _channel = 99; }
;
LINEBREAK:
	(('\r\n' | '\r' | '\n'))
	{ _channel = 99; }
;
QUOTED_34_34:
	(('"')(~('"'))*('"'))
;

