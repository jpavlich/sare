/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package sare.language.acm.resource.acm.grammar;

public enum AcmCardinality {
	
	ONE, PLUS, QUESTIONMARK, STAR;
	
}
