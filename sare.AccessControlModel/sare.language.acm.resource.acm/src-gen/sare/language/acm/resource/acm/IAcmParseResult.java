/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package sare.language.acm.resource.acm;

/**
 * An interface used to access the result of parsing a document.
 */
public interface IAcmParseResult {
	
	/**
	 * Returns the root object of the document.
	 */
	public org.eclipse.emf.ecore.EObject getRoot();
	
	/**
	 * Returns a list of commands that must be executed after parsing the document.
	 */
	public java.util.Collection<sare.language.acm.resource.acm.IAcmCommand<sare.language.acm.resource.acm.IAcmTextResource>> getPostParseCommands();
	
}
