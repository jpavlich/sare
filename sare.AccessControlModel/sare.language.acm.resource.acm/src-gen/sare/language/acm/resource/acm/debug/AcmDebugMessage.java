/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package sare.language.acm.resource.acm.debug;

/**
 * DebugMessages are exchanged between the debug server (the Eclipse debug
 * framework) and the debug client (a running process or interpreter). To exchange
 * messages they are serialized and sent over sockets.
 */
public class AcmDebugMessage {
	
	private static final char DELIMITER = ':';
	private sare.language.acm.resource.acm.debug.EAcmDebugMessageTypes messageType;
	private String[] arguments;
	
	public AcmDebugMessage(sare.language.acm.resource.acm.debug.EAcmDebugMessageTypes messageType, String[] arguments) {
		super();
		this.messageType = messageType;
		this.arguments = arguments;
	}
	
	public AcmDebugMessage(sare.language.acm.resource.acm.debug.EAcmDebugMessageTypes messageType, java.util.List<String> arguments) {
		super();
		this.messageType = messageType;
		this.arguments = new String[arguments.size()];
		for (int i = 0; i < arguments.size(); i++) {
			this.arguments[i] = arguments.get(i);
		}
	}
	
	public sare.language.acm.resource.acm.debug.EAcmDebugMessageTypes getMessageType() {
		return messageType;
	}
	
	public String[] getArguments() {
		return arguments;
	}
	
	public String serialize() {
		java.util.List<String> parts = new java.util.ArrayList<String>();
		parts.add(messageType.name());
		for (String argument : arguments) {
			parts.add(argument);
		}
		return sare.language.acm.resource.acm.util.AcmStringUtil.encode(DELIMITER, parts);
	}
	
	public static AcmDebugMessage deserialize(String response) {
		java.util.List<String> parts = sare.language.acm.resource.acm.util.AcmStringUtil.decode(response, DELIMITER);
		String messageType = parts.get(0);
		String[] arguments = new String[parts.size() - 1];
		for (int i = 1; i < parts.size(); i++) {
			arguments[i - 1] = parts.get(i);
		}
		sare.language.acm.resource.acm.debug.EAcmDebugMessageTypes type = sare.language.acm.resource.acm.debug.EAcmDebugMessageTypes.valueOf(messageType);
		AcmDebugMessage message = new AcmDebugMessage(type, arguments);
		return message;
	}
	
	public boolean hasType(sare.language.acm.resource.acm.debug.EAcmDebugMessageTypes type) {
		return this.messageType == type;
	}
	
	public String getArgument(int index) {
		return getArguments()[index];
	}
	
	public String toString() {
		return this.getClass().getSimpleName() + "[" + messageType.name() + ": " + sare.language.acm.resource.acm.util.AcmStringUtil.explode(arguments, ", ") + "]";
	}
	
}
