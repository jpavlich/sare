/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package sare.language.acm.resource.acm;

/**
 * Implementors of this interface can provide a post-processor for text resources.
 */
public interface IAcmResourcePostProcessorProvider {
	
	/**
	 * Returns the processor that shall be called after text resource are successfully
	 * parsed.
	 */
	public sare.language.acm.resource.acm.IAcmResourcePostProcessor getResourcePostProcessor();
	
}
