/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package sare.language.acm.resource.acm.grammar;

public abstract class AcmFormattingElement extends sare.language.acm.resource.acm.grammar.AcmSyntaxElement {
	
	public AcmFormattingElement(sare.language.acm.resource.acm.grammar.AcmCardinality cardinality) {
		super(cardinality, null);
	}
	
}
