/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package sare.language.acm.resource.acm.mopp;

/**
 * A representation for a range in a document where a structural feature (e.g., a
 * reference) is expected.
 */
public class AcmExpectedStructuralFeature extends sare.language.acm.resource.acm.mopp.AcmAbstractExpectedElement {
	
	private sare.language.acm.resource.acm.grammar.AcmPlaceholder placeholder;
	
	public AcmExpectedStructuralFeature(sare.language.acm.resource.acm.grammar.AcmPlaceholder placeholder) {
		super(placeholder.getMetaclass());
		this.placeholder = placeholder;
	}
	
	public org.eclipse.emf.ecore.EStructuralFeature getFeature() {
		return placeholder.getFeature();
	}
	
	public String getTokenName() {
		return placeholder.getTokenName();
	}
	
	public java.util.Set<String> getTokenNames() {
		return java.util.Collections.singleton(getTokenName());
	}
	
	public String toString() {
		return "EFeature " + getFeature().getEContainingClass().getName() + "." + getFeature().getName();
	}
	
	public boolean equals(Object o) {
		if (o instanceof AcmExpectedStructuralFeature) {
			return getFeature().equals(((AcmExpectedStructuralFeature) o).getFeature());
		}
		return false;
	}
}
