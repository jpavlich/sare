/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package sare.language.acm.resource.acm.grammar;

public class AcmLineBreak extends sare.language.acm.resource.acm.grammar.AcmFormattingElement {
	
	private final int tabs;
	
	public AcmLineBreak(sare.language.acm.resource.acm.grammar.AcmCardinality cardinality, int tabs) {
		super(cardinality);
		this.tabs = tabs;
	}
	
	public int getTabs() {
		return tabs;
	}
	
	public String toString() {
		return "!" + getTabs();
	}
	
}
