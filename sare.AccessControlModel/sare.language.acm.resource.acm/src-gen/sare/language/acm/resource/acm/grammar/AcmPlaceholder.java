/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package sare.language.acm.resource.acm.grammar;

/**
 * A class to represent placeholders in a grammar.
 */
public class AcmPlaceholder extends sare.language.acm.resource.acm.grammar.AcmTerminal {
	
	private final String tokenName;
	
	public AcmPlaceholder(org.eclipse.emf.ecore.EStructuralFeature feature, String tokenName, sare.language.acm.resource.acm.grammar.AcmCardinality cardinality, int mandatoryOccurencesAfter) {
		super(feature, cardinality, mandatoryOccurencesAfter);
		this.tokenName = tokenName;
	}
	
	public String getTokenName() {
		return tokenName;
	}
	
}
