/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package sare.language.acm.resource.acm.mopp;

public class AcmProblem implements sare.language.acm.resource.acm.IAcmProblem {
	
	private String message;
	private sare.language.acm.resource.acm.AcmEProblemType type;
	private sare.language.acm.resource.acm.AcmEProblemSeverity severity;
	private java.util.Collection<sare.language.acm.resource.acm.IAcmQuickFix> quickFixes;
	
	public AcmProblem(String message, sare.language.acm.resource.acm.AcmEProblemType type, sare.language.acm.resource.acm.AcmEProblemSeverity severity) {
		this(message, type, severity, java.util.Collections.<sare.language.acm.resource.acm.IAcmQuickFix>emptySet());
	}
	
	public AcmProblem(String message, sare.language.acm.resource.acm.AcmEProblemType type, sare.language.acm.resource.acm.AcmEProblemSeverity severity, sare.language.acm.resource.acm.IAcmQuickFix quickFix) {
		this(message, type, severity, java.util.Collections.singleton(quickFix));
	}
	
	public AcmProblem(String message, sare.language.acm.resource.acm.AcmEProblemType type, sare.language.acm.resource.acm.AcmEProblemSeverity severity, java.util.Collection<sare.language.acm.resource.acm.IAcmQuickFix> quickFixes) {
		super();
		this.message = message;
		this.type = type;
		this.severity = severity;
		this.quickFixes = new java.util.LinkedHashSet<sare.language.acm.resource.acm.IAcmQuickFix>();
		this.quickFixes.addAll(quickFixes);
	}
	
	public sare.language.acm.resource.acm.AcmEProblemType getType() {
		return type;
	}
	
	public sare.language.acm.resource.acm.AcmEProblemSeverity getSeverity() {
		return severity;
	}
	
	public String getMessage() {
		return message;
	}
	
	public java.util.Collection<sare.language.acm.resource.acm.IAcmQuickFix> getQuickFixes() {
		return quickFixes;
	}
	
}
