/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package sare.language.acm.resource.acm.mopp;

public class AcmDynamicTokenStyler {
	
	/**
	 * This method is called to dynamically style tokens.
	 * 
	 * @param resource the TextResource that contains the token
	 * @param token the token to obtain a style for
	 * @param staticStyle the token style as set in the editor preferences (is
	 * <code>null</code> if syntax highlighting for the token is disabled)
	 */
	public sare.language.acm.resource.acm.IAcmTokenStyle getDynamicTokenStyle(sare.language.acm.resource.acm.IAcmTextResource resource, sare.language.acm.resource.acm.IAcmTextToken token, sare.language.acm.resource.acm.IAcmTokenStyle staticStyle) {
		// The default implementation returns the static style without any changes. To
		// implement dynamic token styling, set the overrideDynamicTokenStyler option to
		// <code>false</code> and customize this method.
		return staticStyle;
	}
	
}
