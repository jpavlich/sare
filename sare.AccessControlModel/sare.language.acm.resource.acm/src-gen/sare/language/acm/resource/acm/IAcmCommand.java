/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package sare.language.acm.resource.acm;

/**
 * A simple interface for commands that can be executed and that return
 * information about the success of their execution.
 */
public interface IAcmCommand<ContextType> {
	
	public boolean execute(ContextType context);
}
