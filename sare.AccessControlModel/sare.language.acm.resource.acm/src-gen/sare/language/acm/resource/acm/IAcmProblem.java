/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package sare.language.acm.resource.acm;

public interface IAcmProblem {
	public String getMessage();
	public sare.language.acm.resource.acm.AcmEProblemSeverity getSeverity();
	public sare.language.acm.resource.acm.AcmEProblemType getType();
	public java.util.Collection<sare.language.acm.resource.acm.IAcmQuickFix> getQuickFixes();
}
