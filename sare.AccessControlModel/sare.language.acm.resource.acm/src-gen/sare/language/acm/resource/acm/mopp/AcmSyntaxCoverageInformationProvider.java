/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package sare.language.acm.resource.acm.mopp;

public class AcmSyntaxCoverageInformationProvider {
	
	public org.eclipse.emf.ecore.EClass[] getClassesWithSyntax() {
		return new org.eclipse.emf.ecore.EClass[] {
			sare.language.acm.AcmPackage.eINSTANCE.getAccessControlModel(),
			sare.language.acm.AcmPackage.eINSTANCE.getRole(),
			sare.language.acm.AcmPackage.eINSTANCE.getUser(),
			sare.language.acm.AcmPackage.eINSTANCE.getPermission(),
			org.eclipse.uml2.uml.UMLPackage.eINSTANCE.getModel(),
			org.eclipse.uml2.uml.UMLPackage.eINSTANCE.getPackageImport(),
			org.eclipse.uml2.uml.UMLPackage.eINSTANCE.getPackage(),
			org.eclipse.uml2.uml.UMLPackage.eINSTANCE.getClass_(),
			org.eclipse.uml2.uml.UMLPackage.eINSTANCE.getOperation(),
			org.eclipse.uml2.uml.UMLPackage.eINSTANCE.getProperty(),
			org.eclipse.uml2.uml.UMLPackage.eINSTANCE.getParameter(),
			org.eclipse.uml2.uml.UMLPackage.eINSTANCE.getComment(),
		};
	}
	
	public org.eclipse.emf.ecore.EClass[] getStartSymbols() {
		return new org.eclipse.emf.ecore.EClass[] {
			sare.language.acm.AcmPackage.eINSTANCE.getAccessControlModel(),
		};
	}
	
}
