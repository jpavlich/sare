/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package sare.language.acm.resource.acm.mopp;

/**
 * The AcmTokenResolverFactory class provides access to all generated token
 * resolvers. By giving the name of a defined token, the corresponding resolve can
 * be obtained. Despite the fact that this class is called TokenResolverFactory is
 * does NOT create new token resolvers whenever a client calls methods to obtain a
 * resolver. Rather, this class maintains a map of all resolvers and creates each
 * resolver at most once.
 */
public class AcmTokenResolverFactory implements sare.language.acm.resource.acm.IAcmTokenResolverFactory {
	
	private java.util.Map<String, sare.language.acm.resource.acm.IAcmTokenResolver> tokenName2TokenResolver;
	private java.util.Map<String, sare.language.acm.resource.acm.IAcmTokenResolver> featureName2CollectInTokenResolver;
	private static sare.language.acm.resource.acm.IAcmTokenResolver defaultResolver = new sare.language.acm.resource.acm.analysis.AcmDefaultTokenResolver();
	
	public AcmTokenResolverFactory() {
		tokenName2TokenResolver = new java.util.LinkedHashMap<String, sare.language.acm.resource.acm.IAcmTokenResolver>();
		featureName2CollectInTokenResolver = new java.util.LinkedHashMap<String, sare.language.acm.resource.acm.IAcmTokenResolver>();
		registerTokenResolver("TEXT", new sare.language.acm.resource.acm.analysis.AcmTEXTTokenResolver());
		registerTokenResolver("NAMED_ELEMENT_ID", new sare.language.acm.resource.acm.analysis.AcmNAMED_ELEMENT_IDTokenResolver());
		registerTokenResolver("QUOTED_34_34", new sare.language.acm.resource.acm.analysis.AcmQUOTED_34_34TokenResolver());
	}
	
	public sare.language.acm.resource.acm.IAcmTokenResolver createTokenResolver(String tokenName) {
		return internalCreateResolver(tokenName2TokenResolver, tokenName);
	}
	
	public sare.language.acm.resource.acm.IAcmTokenResolver createCollectInTokenResolver(String featureName) {
		return internalCreateResolver(featureName2CollectInTokenResolver, featureName);
	}
	
	protected boolean registerTokenResolver(String tokenName, sare.language.acm.resource.acm.IAcmTokenResolver resolver){
		return internalRegisterTokenResolver(tokenName2TokenResolver, tokenName, resolver);
	}
	
	protected boolean registerCollectInTokenResolver(String featureName, sare.language.acm.resource.acm.IAcmTokenResolver resolver){
		return internalRegisterTokenResolver(featureName2CollectInTokenResolver, featureName, resolver);
	}
	
	protected sare.language.acm.resource.acm.IAcmTokenResolver deRegisterTokenResolver(String tokenName){
		return tokenName2TokenResolver.remove(tokenName);
	}
	
	private sare.language.acm.resource.acm.IAcmTokenResolver internalCreateResolver(java.util.Map<String, sare.language.acm.resource.acm.IAcmTokenResolver> resolverMap, String key) {
		if (resolverMap.containsKey(key)){
			return resolverMap.get(key);
		} else {
			return defaultResolver;
		}
	}
	
	private boolean internalRegisterTokenResolver(java.util.Map<String, sare.language.acm.resource.acm.IAcmTokenResolver> resolverMap, String key, sare.language.acm.resource.acm.IAcmTokenResolver resolver) {
		if (!resolverMap.containsKey(key)) {
			resolverMap.put(key,resolver);
			return true;
		}
		return false;
	}
	
}
