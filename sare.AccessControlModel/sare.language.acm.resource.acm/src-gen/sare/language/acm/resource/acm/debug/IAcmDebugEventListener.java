/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package sare.language.acm.resource.acm.debug;

public interface IAcmDebugEventListener {
	
	/**
	 * Notification that the given event occurred in the while debugging.
	 */
	public void handleMessage(sare.language.acm.resource.acm.debug.AcmDebugMessage message);
}
