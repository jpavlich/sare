/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package sare.language.acm.resource.acm.util;

/**
 * This class provides basic infrastructure to interpret models. To implement
 * concrete interpreters, subclass this abstract interpreter and override the
 * interprete_* methods. The interpretation can be customized by binding the two
 * type parameters (ResultType, ContextType). The former is returned by all
 * interprete_* methods, while the latter is passed from method to method while
 * traversing the model. The concrete traversal strategy can also be exchanged.
 * One can use a static traversal strategy by pushing all objects to interpret on
 * the interpretation stack (using addObjectToInterprete()) before calling
 * interprete(). Alternatively, the traversal strategy can be dynamic by pushing
 * objects on the interpretation stack during interpretation.
 */
public class AbstractAcmInterpreter<ResultType, ContextType> {
	
	private java.util.Stack<org.eclipse.emf.ecore.EObject> interpretationStack = new java.util.Stack<org.eclipse.emf.ecore.EObject>();
	private java.util.List<sare.language.acm.resource.acm.IAcmInterpreterListener> listeners = new java.util.ArrayList<sare.language.acm.resource.acm.IAcmInterpreterListener>();
	private org.eclipse.emf.ecore.EObject nextObjectToInterprete;
	private Object currentContext;
	
	public ResultType interprete(ContextType context) {
		ResultType result = null;
		org.eclipse.emf.ecore.EObject next = null;
		currentContext = context;
		while (!interpretationStack.empty()) {
			try {
				next = interpretationStack.pop();
			} catch (java.util.EmptyStackException ese) {
				// this can happen when the interpreter was terminated between the call to empty()
				// and pop()
				break;
			}
			nextObjectToInterprete = next;
			notifyListeners(next);
			result = interprete(next, context);
			if (!continueInterpretation(context, result)) {
				break;
			}
		}
		currentContext = null;
		return result;
	}
	
	/**
	 * Override this method to stop the overall interpretation depending on the result
	 * of the interpretation of a single model elements.
	 */
	public boolean continueInterpretation(ContextType context, ResultType result) {
		return true;
	}
	
	public ResultType interprete(org.eclipse.emf.ecore.EObject object, ContextType context) {
		ResultType result = null;
		if (object instanceof org.eclipse.uml2.uml.AssociationClass) {
			result = interprete_org_eclipse_uml2_uml_AssociationClass((org.eclipse.uml2.uml.AssociationClass) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.ProtocolTransition) {
			result = interprete_org_eclipse_uml2_uml_ProtocolTransition((org.eclipse.uml2.uml.ProtocolTransition) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.ExpansionRegion) {
			result = interprete_org_eclipse_uml2_uml_ExpansionRegion((org.eclipse.uml2.uml.ExpansionRegion) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.ExpansionNode) {
			result = interprete_org_eclipse_uml2_uml_ExpansionNode((org.eclipse.uml2.uml.ExpansionNode) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.LoopNode) {
			result = interprete_org_eclipse_uml2_uml_LoopNode((org.eclipse.uml2.uml.LoopNode) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.Clause) {
			result = interprete_org_eclipse_uml2_uml_Clause((org.eclipse.uml2.uml.Clause) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.ConditionalNode) {
			result = interprete_org_eclipse_uml2_uml_ConditionalNode((org.eclipse.uml2.uml.ConditionalNode) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.DataStoreNode) {
			result = interprete_org_eclipse_uml2_uml_DataStoreNode((org.eclipse.uml2.uml.DataStoreNode) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.JoinNode) {
			result = interprete_org_eclipse_uml2_uml_JoinNode((org.eclipse.uml2.uml.JoinNode) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.StartObjectBehaviorAction) {
			result = interprete_org_eclipse_uml2_uml_StartObjectBehaviorAction((org.eclipse.uml2.uml.StartObjectBehaviorAction) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.ReduceAction) {
			result = interprete_org_eclipse_uml2_uml_ReduceAction((org.eclipse.uml2.uml.ReduceAction) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.UnmarshallAction) {
			result = interprete_org_eclipse_uml2_uml_UnmarshallAction((org.eclipse.uml2.uml.UnmarshallAction) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.ReplyAction) {
			result = interprete_org_eclipse_uml2_uml_ReplyAction((org.eclipse.uml2.uml.ReplyAction) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.AcceptCallAction) {
			result = interprete_org_eclipse_uml2_uml_AcceptCallAction((org.eclipse.uml2.uml.AcceptCallAction) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.AcceptEventAction) {
			result = interprete_org_eclipse_uml2_uml_AcceptEventAction((org.eclipse.uml2.uml.AcceptEventAction) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.CreateLinkObjectAction) {
			result = interprete_org_eclipse_uml2_uml_CreateLinkObjectAction((org.eclipse.uml2.uml.CreateLinkObjectAction) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.ReadLinkObjectEndQualifierAction) {
			result = interprete_org_eclipse_uml2_uml_ReadLinkObjectEndQualifierAction((org.eclipse.uml2.uml.ReadLinkObjectEndQualifierAction) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.ReadLinkObjectEndAction) {
			result = interprete_org_eclipse_uml2_uml_ReadLinkObjectEndAction((org.eclipse.uml2.uml.ReadLinkObjectEndAction) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.StartClassifierBehaviorAction) {
			result = interprete_org_eclipse_uml2_uml_StartClassifierBehaviorAction((org.eclipse.uml2.uml.StartClassifierBehaviorAction) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.ReadIsClassifiedObjectAction) {
			result = interprete_org_eclipse_uml2_uml_ReadIsClassifiedObjectAction((org.eclipse.uml2.uml.ReadIsClassifiedObjectAction) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.ReclassifyObjectAction) {
			result = interprete_org_eclipse_uml2_uml_ReclassifyObjectAction((org.eclipse.uml2.uml.ReclassifyObjectAction) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.ReadExtentAction) {
			result = interprete_org_eclipse_uml2_uml_ReadExtentAction((org.eclipse.uml2.uml.ReadExtentAction) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.InformationFlow) {
			result = interprete_org_eclipse_uml2_uml_InformationFlow((org.eclipse.uml2.uml.InformationFlow) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.InformationItem) {
			result = interprete_org_eclipse_uml2_uml_InformationItem((org.eclipse.uml2.uml.InformationItem) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.ActionInputPin) {
			result = interprete_org_eclipse_uml2_uml_ActionInputPin((org.eclipse.uml2.uml.ActionInputPin) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.RaiseExceptionAction) {
			result = interprete_org_eclipse_uml2_uml_RaiseExceptionAction((org.eclipse.uml2.uml.RaiseExceptionAction) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.RemoveVariableValueAction) {
			result = interprete_org_eclipse_uml2_uml_RemoveVariableValueAction((org.eclipse.uml2.uml.RemoveVariableValueAction) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.AddVariableValueAction) {
			result = interprete_org_eclipse_uml2_uml_AddVariableValueAction((org.eclipse.uml2.uml.AddVariableValueAction) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.ClearVariableAction) {
			result = interprete_org_eclipse_uml2_uml_ClearVariableAction((org.eclipse.uml2.uml.ClearVariableAction) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.WriteVariableAction) {
			result = interprete_org_eclipse_uml2_uml_WriteVariableAction((org.eclipse.uml2.uml.WriteVariableAction) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.ReadVariableAction) {
			result = interprete_org_eclipse_uml2_uml_ReadVariableAction((org.eclipse.uml2.uml.ReadVariableAction) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.VariableAction) {
			result = interprete_org_eclipse_uml2_uml_VariableAction((org.eclipse.uml2.uml.VariableAction) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.TimeEvent) {
			result = interprete_org_eclipse_uml2_uml_TimeEvent((org.eclipse.uml2.uml.TimeEvent) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.FinalState) {
			result = interprete_org_eclipse_uml2_uml_FinalState((org.eclipse.uml2.uml.FinalState) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.DurationObservation) {
			result = interprete_org_eclipse_uml2_uml_DurationObservation((org.eclipse.uml2.uml.DurationObservation) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.TimeObservation) {
			result = interprete_org_eclipse_uml2_uml_TimeObservation((org.eclipse.uml2.uml.TimeObservation) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.DurationConstraint) {
			result = interprete_org_eclipse_uml2_uml_DurationConstraint((org.eclipse.uml2.uml.DurationConstraint) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.TimeInterval) {
			result = interprete_org_eclipse_uml2_uml_TimeInterval((org.eclipse.uml2.uml.TimeInterval) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.TimeConstraint) {
			result = interprete_org_eclipse_uml2_uml_TimeConstraint((org.eclipse.uml2.uml.TimeConstraint) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.IntervalConstraint) {
			result = interprete_org_eclipse_uml2_uml_IntervalConstraint((org.eclipse.uml2.uml.IntervalConstraint) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.DurationInterval) {
			result = interprete_org_eclipse_uml2_uml_DurationInterval((org.eclipse.uml2.uml.DurationInterval) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.Interval) {
			result = interprete_org_eclipse_uml2_uml_Interval((org.eclipse.uml2.uml.Interval) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.Duration) {
			result = interprete_org_eclipse_uml2_uml_Duration((org.eclipse.uml2.uml.Duration) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.Observation) {
			result = interprete_org_eclipse_uml2_uml_Observation((org.eclipse.uml2.uml.Observation) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.TimeExpression) {
			result = interprete_org_eclipse_uml2_uml_TimeExpression((org.eclipse.uml2.uml.TimeExpression) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.ValueSpecificationAction) {
			result = interprete_org_eclipse_uml2_uml_ValueSpecificationAction((org.eclipse.uml2.uml.ValueSpecificationAction) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.SendObjectAction) {
			result = interprete_org_eclipse_uml2_uml_SendObjectAction((org.eclipse.uml2.uml.SendObjectAction) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.BroadcastSignalAction) {
			result = interprete_org_eclipse_uml2_uml_BroadcastSignalAction((org.eclipse.uml2.uml.BroadcastSignalAction) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.ClearAssociationAction) {
			result = interprete_org_eclipse_uml2_uml_ClearAssociationAction((org.eclipse.uml2.uml.ClearAssociationAction) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.LinkEndDestructionData) {
			result = interprete_org_eclipse_uml2_uml_LinkEndDestructionData((org.eclipse.uml2.uml.LinkEndDestructionData) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.DestroyLinkAction) {
			result = interprete_org_eclipse_uml2_uml_DestroyLinkAction((org.eclipse.uml2.uml.DestroyLinkAction) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.CreateLinkAction) {
			result = interprete_org_eclipse_uml2_uml_CreateLinkAction((org.eclipse.uml2.uml.CreateLinkAction) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.WriteLinkAction) {
			result = interprete_org_eclipse_uml2_uml_WriteLinkAction((org.eclipse.uml2.uml.WriteLinkAction) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.LinkEndCreationData) {
			result = interprete_org_eclipse_uml2_uml_LinkEndCreationData((org.eclipse.uml2.uml.LinkEndCreationData) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.ReadLinkAction) {
			result = interprete_org_eclipse_uml2_uml_ReadLinkAction((org.eclipse.uml2.uml.ReadLinkAction) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.QualifierValue) {
			result = interprete_org_eclipse_uml2_uml_QualifierValue((org.eclipse.uml2.uml.QualifierValue) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.LinkEndData) {
			result = interprete_org_eclipse_uml2_uml_LinkEndData((org.eclipse.uml2.uml.LinkEndData) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.LinkAction) {
			result = interprete_org_eclipse_uml2_uml_LinkAction((org.eclipse.uml2.uml.LinkAction) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.AddStructuralFeatureValueAction) {
			result = interprete_org_eclipse_uml2_uml_AddStructuralFeatureValueAction((org.eclipse.uml2.uml.AddStructuralFeatureValueAction) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.RemoveStructuralFeatureValueAction) {
			result = interprete_org_eclipse_uml2_uml_RemoveStructuralFeatureValueAction((org.eclipse.uml2.uml.RemoveStructuralFeatureValueAction) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.ClearStructuralFeatureAction) {
			result = interprete_org_eclipse_uml2_uml_ClearStructuralFeatureAction((org.eclipse.uml2.uml.ClearStructuralFeatureAction) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.WriteStructuralFeatureAction) {
			result = interprete_org_eclipse_uml2_uml_WriteStructuralFeatureAction((org.eclipse.uml2.uml.WriteStructuralFeatureAction) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.ReadStructuralFeatureAction) {
			result = interprete_org_eclipse_uml2_uml_ReadStructuralFeatureAction((org.eclipse.uml2.uml.ReadStructuralFeatureAction) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.StructuralFeatureAction) {
			result = interprete_org_eclipse_uml2_uml_StructuralFeatureAction((org.eclipse.uml2.uml.StructuralFeatureAction) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.ReadSelfAction) {
			result = interprete_org_eclipse_uml2_uml_ReadSelfAction((org.eclipse.uml2.uml.ReadSelfAction) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.TestIdentityAction) {
			result = interprete_org_eclipse_uml2_uml_TestIdentityAction((org.eclipse.uml2.uml.TestIdentityAction) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.DestroyObjectAction) {
			result = interprete_org_eclipse_uml2_uml_DestroyObjectAction((org.eclipse.uml2.uml.DestroyObjectAction) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.CreateObjectAction) {
			result = interprete_org_eclipse_uml2_uml_CreateObjectAction((org.eclipse.uml2.uml.CreateObjectAction) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.ConsiderIgnoreFragment) {
			result = interprete_org_eclipse_uml2_uml_ConsiderIgnoreFragment((org.eclipse.uml2.uml.ConsiderIgnoreFragment) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.Continuation) {
			result = interprete_org_eclipse_uml2_uml_Continuation((org.eclipse.uml2.uml.Continuation) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.CombinedFragment) {
			result = interprete_org_eclipse_uml2_uml_CombinedFragment((org.eclipse.uml2.uml.CombinedFragment) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.ExecutionEnvironment) {
			result = interprete_org_eclipse_uml2_uml_ExecutionEnvironment((org.eclipse.uml2.uml.ExecutionEnvironment) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.Device) {
			result = interprete_org_eclipse_uml2_uml_Device((org.eclipse.uml2.uml.Device) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.CommunicationPath) {
			result = interprete_org_eclipse_uml2_uml_CommunicationPath((org.eclipse.uml2.uml.CommunicationPath) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.Node) {
			result = interprete_org_eclipse_uml2_uml_Node((org.eclipse.uml2.uml.Node) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.Component) {
			result = interprete_org_eclipse_uml2_uml_Component((org.eclipse.uml2.uml.Component) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.ComponentRealization) {
			result = interprete_org_eclipse_uml2_uml_ComponentRealization((org.eclipse.uml2.uml.ComponentRealization) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.ActivityFinalNode) {
			result = interprete_org_eclipse_uml2_uml_ActivityFinalNode((org.eclipse.uml2.uml.ActivityFinalNode) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.ObjectFlow) {
			result = interprete_org_eclipse_uml2_uml_ObjectFlow((org.eclipse.uml2.uml.ObjectFlow) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.DecisionNode) {
			result = interprete_org_eclipse_uml2_uml_DecisionNode((org.eclipse.uml2.uml.DecisionNode) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.MergeNode) {
			result = interprete_org_eclipse_uml2_uml_MergeNode((org.eclipse.uml2.uml.MergeNode) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.CentralBufferNode) {
			result = interprete_org_eclipse_uml2_uml_CentralBufferNode((org.eclipse.uml2.uml.CentralBufferNode) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.FlowFinalNode) {
			result = interprete_org_eclipse_uml2_uml_FlowFinalNode((org.eclipse.uml2.uml.FlowFinalNode) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.FinalNode) {
			result = interprete_org_eclipse_uml2_uml_FinalNode((org.eclipse.uml2.uml.FinalNode) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.ForkNode) {
			result = interprete_org_eclipse_uml2_uml_ForkNode((org.eclipse.uml2.uml.ForkNode) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.AnyReceiveEvent) {
			result = interprete_org_eclipse_uml2_uml_AnyReceiveEvent((org.eclipse.uml2.uml.AnyReceiveEvent) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.SignalEvent) {
			result = interprete_org_eclipse_uml2_uml_SignalEvent((org.eclipse.uml2.uml.SignalEvent) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.ChangeEvent) {
			result = interprete_org_eclipse_uml2_uml_ChangeEvent((org.eclipse.uml2.uml.ChangeEvent) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.CallEvent) {
			result = interprete_org_eclipse_uml2_uml_CallEvent((org.eclipse.uml2.uml.CallEvent) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.Actor) {
			result = interprete_org_eclipse_uml2_uml_Actor((org.eclipse.uml2.uml.Actor) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.ReceiveSignalEvent) {
			result = interprete_org_eclipse_uml2_uml_ReceiveSignalEvent((org.eclipse.uml2.uml.ReceiveSignalEvent) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.ReceiveOperationEvent) {
			result = interprete_org_eclipse_uml2_uml_ReceiveOperationEvent((org.eclipse.uml2.uml.ReceiveOperationEvent) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.ExecutionOccurrenceSpecification) {
			result = interprete_org_eclipse_uml2_uml_ExecutionOccurrenceSpecification((org.eclipse.uml2.uml.ExecutionOccurrenceSpecification) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.MessageOccurrenceSpecification) {
			result = interprete_org_eclipse_uml2_uml_MessageOccurrenceSpecification((org.eclipse.uml2.uml.MessageOccurrenceSpecification) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.SendSignalEvent) {
			result = interprete_org_eclipse_uml2_uml_SendSignalEvent((org.eclipse.uml2.uml.SendSignalEvent) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.SendOperationEvent) {
			result = interprete_org_eclipse_uml2_uml_SendOperationEvent((org.eclipse.uml2.uml.SendOperationEvent) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.MessageEvent) {
			result = interprete_org_eclipse_uml2_uml_MessageEvent((org.eclipse.uml2.uml.MessageEvent) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.DestructionEvent) {
			result = interprete_org_eclipse_uml2_uml_DestructionEvent((org.eclipse.uml2.uml.DestructionEvent) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.CreationEvent) {
			result = interprete_org_eclipse_uml2_uml_CreationEvent((org.eclipse.uml2.uml.CreationEvent) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.ExecutionEvent) {
			result = interprete_org_eclipse_uml2_uml_ExecutionEvent((org.eclipse.uml2.uml.ExecutionEvent) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.BehaviorExecutionSpecification) {
			result = interprete_org_eclipse_uml2_uml_BehaviorExecutionSpecification((org.eclipse.uml2.uml.BehaviorExecutionSpecification) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.ActionExecutionSpecification) {
			result = interprete_org_eclipse_uml2_uml_ActionExecutionSpecification((org.eclipse.uml2.uml.ActionExecutionSpecification) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.StateInvariant) {
			result = interprete_org_eclipse_uml2_uml_StateInvariant((org.eclipse.uml2.uml.StateInvariant) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.ExecutionSpecification) {
			result = interprete_org_eclipse_uml2_uml_ExecutionSpecification((org.eclipse.uml2.uml.ExecutionSpecification) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.InteractionConstraint) {
			result = interprete_org_eclipse_uml2_uml_InteractionConstraint((org.eclipse.uml2.uml.InteractionConstraint) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.InteractionOperand) {
			result = interprete_org_eclipse_uml2_uml_InteractionOperand((org.eclipse.uml2.uml.InteractionOperand) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.OccurrenceSpecification) {
			result = interprete_org_eclipse_uml2_uml_OccurrenceSpecification((org.eclipse.uml2.uml.OccurrenceSpecification) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.GeneralOrdering) {
			result = interprete_org_eclipse_uml2_uml_GeneralOrdering((org.eclipse.uml2.uml.GeneralOrdering) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.Gate) {
			result = interprete_org_eclipse_uml2_uml_Gate((org.eclipse.uml2.uml.Gate) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.PartDecomposition) {
			result = interprete_org_eclipse_uml2_uml_PartDecomposition((org.eclipse.uml2.uml.PartDecomposition) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.InteractionUse) {
			result = interprete_org_eclipse_uml2_uml_InteractionUse((org.eclipse.uml2.uml.InteractionUse) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.Lifeline) {
			result = interprete_org_eclipse_uml2_uml_Lifeline((org.eclipse.uml2.uml.Lifeline) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.Interaction) {
			result = interprete_org_eclipse_uml2_uml_Interaction((org.eclipse.uml2.uml.Interaction) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.InteractionFragment) {
			result = interprete_org_eclipse_uml2_uml_InteractionFragment((org.eclipse.uml2.uml.InteractionFragment) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.MessageEnd) {
			result = interprete_org_eclipse_uml2_uml_MessageEnd((org.eclipse.uml2.uml.MessageEnd) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.Message) {
			result = interprete_org_eclipse_uml2_uml_Message((org.eclipse.uml2.uml.Message) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.ValuePin) {
			result = interprete_org_eclipse_uml2_uml_ValuePin((org.eclipse.uml2.uml.ValuePin) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.ActivityParameterNode) {
			result = interprete_org_eclipse_uml2_uml_ActivityParameterNode((org.eclipse.uml2.uml.ActivityParameterNode) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.InitialNode) {
			result = interprete_org_eclipse_uml2_uml_InitialNode((org.eclipse.uml2.uml.InitialNode) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.ControlFlow) {
			result = interprete_org_eclipse_uml2_uml_ControlFlow((org.eclipse.uml2.uml.ControlFlow) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.ControlNode) {
			result = interprete_org_eclipse_uml2_uml_ControlNode((org.eclipse.uml2.uml.ControlNode) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.SequenceNode) {
			result = interprete_org_eclipse_uml2_uml_SequenceNode((org.eclipse.uml2.uml.SequenceNode) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.CallBehaviorAction) {
			result = interprete_org_eclipse_uml2_uml_CallBehaviorAction((org.eclipse.uml2.uml.CallBehaviorAction) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.CallOperationAction) {
			result = interprete_org_eclipse_uml2_uml_CallOperationAction((org.eclipse.uml2.uml.CallOperationAction) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.SendSignalAction) {
			result = interprete_org_eclipse_uml2_uml_SendSignalAction((org.eclipse.uml2.uml.SendSignalAction) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.CallAction) {
			result = interprete_org_eclipse_uml2_uml_CallAction((org.eclipse.uml2.uml.CallAction) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.InvocationAction) {
			result = interprete_org_eclipse_uml2_uml_InvocationAction((org.eclipse.uml2.uml.InvocationAction) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.InputPin) {
			result = interprete_org_eclipse_uml2_uml_InputPin((org.eclipse.uml2.uml.InputPin) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.OutputPin) {
			result = interprete_org_eclipse_uml2_uml_OutputPin((org.eclipse.uml2.uml.OutputPin) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.Pin) {
			result = interprete_org_eclipse_uml2_uml_Pin((org.eclipse.uml2.uml.Pin) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.ObjectNode) {
			result = interprete_org_eclipse_uml2_uml_ObjectNode((org.eclipse.uml2.uml.ObjectNode) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.ExceptionHandler) {
			result = interprete_org_eclipse_uml2_uml_ExceptionHandler((org.eclipse.uml2.uml.ExceptionHandler) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.InterruptibleActivityRegion) {
			result = interprete_org_eclipse_uml2_uml_InterruptibleActivityRegion((org.eclipse.uml2.uml.InterruptibleActivityRegion) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.ActivityPartition) {
			result = interprete_org_eclipse_uml2_uml_ActivityPartition((org.eclipse.uml2.uml.ActivityPartition) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.ActivityEdge) {
			result = interprete_org_eclipse_uml2_uml_ActivityEdge((org.eclipse.uml2.uml.ActivityEdge) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.Variable) {
			result = interprete_org_eclipse_uml2_uml_Variable((org.eclipse.uml2.uml.Variable) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.Activity) {
			result = interprete_org_eclipse_uml2_uml_Activity((org.eclipse.uml2.uml.Activity) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.StructuredActivityNode) {
			result = interprete_org_eclipse_uml2_uml_StructuredActivityNode((org.eclipse.uml2.uml.StructuredActivityNode) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.ActivityGroup) {
			result = interprete_org_eclipse_uml2_uml_ActivityGroup((org.eclipse.uml2.uml.ActivityGroup) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.OpaqueAction) {
			result = interprete_org_eclipse_uml2_uml_OpaqueAction((org.eclipse.uml2.uml.OpaqueAction) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.Action) {
			result = interprete_org_eclipse_uml2_uml_Action((org.eclipse.uml2.uml.Action) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.ExecutableNode) {
			result = interprete_org_eclipse_uml2_uml_ExecutableNode((org.eclipse.uml2.uml.ExecutableNode) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.ActivityNode) {
			result = interprete_org_eclipse_uml2_uml_ActivityNode((org.eclipse.uml2.uml.ActivityNode) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.FunctionBehavior) {
			result = interprete_org_eclipse_uml2_uml_FunctionBehavior((org.eclipse.uml2.uml.FunctionBehavior) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.OpaqueBehavior) {
			result = interprete_org_eclipse_uml2_uml_OpaqueBehavior((org.eclipse.uml2.uml.OpaqueBehavior) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.LiteralUnlimitedNatural) {
			result = interprete_org_eclipse_uml2_uml_LiteralUnlimitedNatural((org.eclipse.uml2.uml.LiteralUnlimitedNatural) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.InstanceValue) {
			result = interprete_org_eclipse_uml2_uml_InstanceValue((org.eclipse.uml2.uml.InstanceValue) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.LiteralNull) {
			result = interprete_org_eclipse_uml2_uml_LiteralNull((org.eclipse.uml2.uml.LiteralNull) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.LiteralBoolean) {
			result = interprete_org_eclipse_uml2_uml_LiteralBoolean((org.eclipse.uml2.uml.LiteralBoolean) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.LiteralString) {
			result = interprete_org_eclipse_uml2_uml_LiteralString((org.eclipse.uml2.uml.LiteralString) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.LiteralInteger) {
			result = interprete_org_eclipse_uml2_uml_LiteralInteger((org.eclipse.uml2.uml.LiteralInteger) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.LiteralSpecification) {
			result = interprete_org_eclipse_uml2_uml_LiteralSpecification((org.eclipse.uml2.uml.LiteralSpecification) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.PrimitiveType) {
			result = interprete_org_eclipse_uml2_uml_PrimitiveType((org.eclipse.uml2.uml.PrimitiveType) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.Slot) {
			result = interprete_org_eclipse_uml2_uml_Slot((org.eclipse.uml2.uml.Slot) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.EnumerationLiteral) {
			result = interprete_org_eclipse_uml2_uml_EnumerationLiteral((org.eclipse.uml2.uml.EnumerationLiteral) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.InstanceSpecification) {
			result = interprete_org_eclipse_uml2_uml_InstanceSpecification((org.eclipse.uml2.uml.InstanceSpecification) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.Enumeration) {
			result = interprete_org_eclipse_uml2_uml_Enumeration((org.eclipse.uml2.uml.Enumeration) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.ProfileApplication) {
			result = interprete_org_eclipse_uml2_uml_ProfileApplication((org.eclipse.uml2.uml.ProfileApplication) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.PackageMerge) {
			result = interprete_org_eclipse_uml2_uml_PackageMerge((org.eclipse.uml2.uml.PackageMerge) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.Usage) {
			result = interprete_org_eclipse_uml2_uml_Usage((org.eclipse.uml2.uml.Usage) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.StringExpression) {
			result = interprete_org_eclipse_uml2_uml_StringExpression((org.eclipse.uml2.uml.StringExpression) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.Expression) {
			result = interprete_org_eclipse_uml2_uml_Expression((org.eclipse.uml2.uml.Expression) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.ClassifierTemplateParameter) {
			result = interprete_org_eclipse_uml2_uml_ClassifierTemplateParameter((org.eclipse.uml2.uml.ClassifierTemplateParameter) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.RedefinableTemplateSignature) {
			result = interprete_org_eclipse_uml2_uml_RedefinableTemplateSignature((org.eclipse.uml2.uml.RedefinableTemplateSignature) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.ExtensionPoint) {
			result = interprete_org_eclipse_uml2_uml_ExtensionPoint((org.eclipse.uml2.uml.ExtensionPoint) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.Extend) {
			result = interprete_org_eclipse_uml2_uml_Extend((org.eclipse.uml2.uml.Extend) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.Include) {
			result = interprete_org_eclipse_uml2_uml_Include((org.eclipse.uml2.uml.Include) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.UseCase) {
			result = interprete_org_eclipse_uml2_uml_UseCase((org.eclipse.uml2.uml.UseCase) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.Collaboration) {
			result = interprete_org_eclipse_uml2_uml_Collaboration((org.eclipse.uml2.uml.Collaboration) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.CollaborationUse) {
			result = interprete_org_eclipse_uml2_uml_CollaborationUse((org.eclipse.uml2.uml.CollaborationUse) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.ConnectableElementTemplateParameter) {
			result = interprete_org_eclipse_uml2_uml_ConnectableElementTemplateParameter((org.eclipse.uml2.uml.ConnectableElementTemplateParameter) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.ExtensionEnd) {
			result = interprete_org_eclipse_uml2_uml_ExtensionEnd((org.eclipse.uml2.uml.ExtensionEnd) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.StructuralFeature) {
			result = interprete_org_eclipse_uml2_uml_StructuralFeature((org.eclipse.uml2.uml.StructuralFeature) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.OperationTemplateParameter) {
			result = interprete_org_eclipse_uml2_uml_OperationTemplateParameter((org.eclipse.uml2.uml.OperationTemplateParameter) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.DataType) {
			result = interprete_org_eclipse_uml2_uml_DataType((org.eclipse.uml2.uml.DataType) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.ParameterSet) {
			result = interprete_org_eclipse_uml2_uml_ParameterSet((org.eclipse.uml2.uml.ParameterSet) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.Model) {
			result = interprete_org_eclipse_uml2_uml_Model((org.eclipse.uml2.uml.Model) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.Profile) {
			result = interprete_org_eclipse_uml2_uml_Profile((org.eclipse.uml2.uml.Profile) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.Image) {
			result = interprete_org_eclipse_uml2_uml_Image((org.eclipse.uml2.uml.Image) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.Stereotype) {
			result = interprete_org_eclipse_uml2_uml_Stereotype((org.eclipse.uml2.uml.Stereotype) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.Extension) {
			result = interprete_org_eclipse_uml2_uml_Extension((org.eclipse.uml2.uml.Extension) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.Connector) {
			result = interprete_org_eclipse_uml2_uml_Connector((org.eclipse.uml2.uml.Connector) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.EncapsulatedClassifier) {
			result = interprete_org_eclipse_uml2_uml_EncapsulatedClassifier((org.eclipse.uml2.uml.EncapsulatedClassifier) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.StructuredClassifier) {
			result = interprete_org_eclipse_uml2_uml_StructuredClassifier((org.eclipse.uml2.uml.StructuredClassifier) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.ProtocolConformance) {
			result = interprete_org_eclipse_uml2_uml_ProtocolConformance((org.eclipse.uml2.uml.ProtocolConformance) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.Pseudostate) {
			result = interprete_org_eclipse_uml2_uml_Pseudostate((org.eclipse.uml2.uml.Pseudostate) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.ConnectionPointReference) {
			result = interprete_org_eclipse_uml2_uml_ConnectionPointReference((org.eclipse.uml2.uml.ConnectionPointReference) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.State) {
			result = interprete_org_eclipse_uml2_uml_State((org.eclipse.uml2.uml.State) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.Port) {
			result = interprete_org_eclipse_uml2_uml_Port((org.eclipse.uml2.uml.Port) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.Event) {
			result = interprete_org_eclipse_uml2_uml_Event((org.eclipse.uml2.uml.Event) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.Trigger) {
			result = interprete_org_eclipse_uml2_uml_Trigger((org.eclipse.uml2.uml.Trigger) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.Transition) {
			result = interprete_org_eclipse_uml2_uml_Transition((org.eclipse.uml2.uml.Transition) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.Vertex) {
			result = interprete_org_eclipse_uml2_uml_Vertex((org.eclipse.uml2.uml.Vertex) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.Region) {
			result = interprete_org_eclipse_uml2_uml_Region((org.eclipse.uml2.uml.Region) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.ProtocolStateMachine) {
			result = interprete_org_eclipse_uml2_uml_ProtocolStateMachine((org.eclipse.uml2.uml.ProtocolStateMachine) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.StateMachine) {
			result = interprete_org_eclipse_uml2_uml_StateMachine((org.eclipse.uml2.uml.StateMachine) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.Signal) {
			result = interprete_org_eclipse_uml2_uml_Signal((org.eclipse.uml2.uml.Signal) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.Reception) {
			result = interprete_org_eclipse_uml2_uml_Reception((org.eclipse.uml2.uml.Reception) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.Interface) {
			result = interprete_org_eclipse_uml2_uml_Interface((org.eclipse.uml2.uml.Interface) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.InterfaceRealization) {
			result = interprete_org_eclipse_uml2_uml_InterfaceRealization((org.eclipse.uml2.uml.InterfaceRealization) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.Behavior) {
			result = interprete_org_eclipse_uml2_uml_Behavior((org.eclipse.uml2.uml.Behavior) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.Class) {
			result = interprete_org_eclipse_uml2_uml_Class((org.eclipse.uml2.uml.Class) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.BehavioredClassifier) {
			result = interprete_org_eclipse_uml2_uml_BehavioredClassifier((org.eclipse.uml2.uml.BehavioredClassifier) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.Operation) {
			result = interprete_org_eclipse_uml2_uml_Operation((org.eclipse.uml2.uml.Operation) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.BehavioralFeature) {
			result = interprete_org_eclipse_uml2_uml_BehavioralFeature((org.eclipse.uml2.uml.BehavioralFeature) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.Manifestation) {
			result = interprete_org_eclipse_uml2_uml_Manifestation((org.eclipse.uml2.uml.Manifestation) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.DeploymentSpecification) {
			result = interprete_org_eclipse_uml2_uml_DeploymentSpecification((org.eclipse.uml2.uml.DeploymentSpecification) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.Artifact) {
			result = interprete_org_eclipse_uml2_uml_Artifact((org.eclipse.uml2.uml.Artifact) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.DeployedArtifact) {
			result = interprete_org_eclipse_uml2_uml_DeployedArtifact((org.eclipse.uml2.uml.DeployedArtifact) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.Deployment) {
			result = interprete_org_eclipse_uml2_uml_Deployment((org.eclipse.uml2.uml.Deployment) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.Property) {
			result = interprete_org_eclipse_uml2_uml_Property((org.eclipse.uml2.uml.Property) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.DeploymentTarget) {
			result = interprete_org_eclipse_uml2_uml_DeploymentTarget((org.eclipse.uml2.uml.DeploymentTarget) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.ConnectorEnd) {
			result = interprete_org_eclipse_uml2_uml_ConnectorEnd((org.eclipse.uml2.uml.ConnectorEnd) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.Parameter) {
			result = interprete_org_eclipse_uml2_uml_Parameter((org.eclipse.uml2.uml.Parameter) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.ConnectableElement) {
			result = interprete_org_eclipse_uml2_uml_ConnectableElement((org.eclipse.uml2.uml.ConnectableElement) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.MultiplicityElement) {
			result = interprete_org_eclipse_uml2_uml_MultiplicityElement((org.eclipse.uml2.uml.MultiplicityElement) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.OpaqueExpression) {
			result = interprete_org_eclipse_uml2_uml_OpaqueExpression((org.eclipse.uml2.uml.OpaqueExpression) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.Substitution) {
			result = interprete_org_eclipse_uml2_uml_Substitution((org.eclipse.uml2.uml.Substitution) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.Realization) {
			result = interprete_org_eclipse_uml2_uml_Realization((org.eclipse.uml2.uml.Realization) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.Abstraction) {
			result = interprete_org_eclipse_uml2_uml_Abstraction((org.eclipse.uml2.uml.Abstraction) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.Feature) {
			result = interprete_org_eclipse_uml2_uml_Feature((org.eclipse.uml2.uml.Feature) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.GeneralizationSet) {
			result = interprete_org_eclipse_uml2_uml_GeneralizationSet((org.eclipse.uml2.uml.GeneralizationSet) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.Generalization) {
			result = interprete_org_eclipse_uml2_uml_Generalization((org.eclipse.uml2.uml.Generalization) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.TemplateParameterSubstitution) {
			result = interprete_org_eclipse_uml2_uml_TemplateParameterSubstitution((org.eclipse.uml2.uml.TemplateParameterSubstitution) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.Association) {
			result = interprete_org_eclipse_uml2_uml_Association((org.eclipse.uml2.uml.Association) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.Classifier) {
			result = interprete_org_eclipse_uml2_uml_Classifier((org.eclipse.uml2.uml.Classifier) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.Type) {
			result = interprete_org_eclipse_uml2_uml_Type((org.eclipse.uml2.uml.Type) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.ValueSpecification) {
			result = interprete_org_eclipse_uml2_uml_ValueSpecification((org.eclipse.uml2.uml.ValueSpecification) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.Package) {
			result = interprete_org_eclipse_uml2_uml_Package((org.eclipse.uml2.uml.Package) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.ParameterableElement) {
			result = interprete_org_eclipse_uml2_uml_ParameterableElement((org.eclipse.uml2.uml.ParameterableElement) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.TemplateParameter) {
			result = interprete_org_eclipse_uml2_uml_TemplateParameter((org.eclipse.uml2.uml.TemplateParameter) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.TemplateSignature) {
			result = interprete_org_eclipse_uml2_uml_TemplateSignature((org.eclipse.uml2.uml.TemplateSignature) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.TemplateBinding) {
			result = interprete_org_eclipse_uml2_uml_TemplateBinding((org.eclipse.uml2.uml.TemplateBinding) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.TemplateableElement) {
			result = interprete_org_eclipse_uml2_uml_TemplateableElement((org.eclipse.uml2.uml.TemplateableElement) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.RedefinableElement) {
			result = interprete_org_eclipse_uml2_uml_RedefinableElement((org.eclipse.uml2.uml.RedefinableElement) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.TypedElement) {
			result = interprete_org_eclipse_uml2_uml_TypedElement((org.eclipse.uml2.uml.TypedElement) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.Constraint) {
			result = interprete_org_eclipse_uml2_uml_Constraint((org.eclipse.uml2.uml.Constraint) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.PackageImport) {
			result = interprete_org_eclipse_uml2_uml_PackageImport((org.eclipse.uml2.uml.PackageImport) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.ElementImport) {
			result = interprete_org_eclipse_uml2_uml_ElementImport((org.eclipse.uml2.uml.ElementImport) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.Namespace) {
			result = interprete_org_eclipse_uml2_uml_Namespace((org.eclipse.uml2.uml.Namespace) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.Dependency) {
			result = interprete_org_eclipse_uml2_uml_Dependency((org.eclipse.uml2.uml.Dependency) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.DirectedRelationship) {
			result = interprete_org_eclipse_uml2_uml_DirectedRelationship((org.eclipse.uml2.uml.DirectedRelationship) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.Relationship) {
			result = interprete_org_eclipse_uml2_uml_Relationship((org.eclipse.uml2.uml.Relationship) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.PackageableElement) {
			result = interprete_org_eclipse_uml2_uml_PackageableElement((org.eclipse.uml2.uml.PackageableElement) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.NamedElement) {
			result = interprete_org_eclipse_uml2_uml_NamedElement((org.eclipse.uml2.uml.NamedElement) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.Comment) {
			result = interprete_org_eclipse_uml2_uml_Comment((org.eclipse.uml2.uml.Comment) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.uml2.uml.Element) {
			result = interprete_org_eclipse_uml2_uml_Element((org.eclipse.uml2.uml.Element) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.emf.ecore.ETypeParameter) {
			result = interprete_org_eclipse_emf_ecore_ETypeParameter((org.eclipse.emf.ecore.ETypeParameter) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.emf.ecore.EGenericType) {
			result = interprete_org_eclipse_emf_ecore_EGenericType((org.eclipse.emf.ecore.EGenericType) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof java.util.Map.Entry<?,?>) {
			result = interprete_java_util_Map_Entry((java.util.Map.Entry<?,?>) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.emf.ecore.EReference) {
			result = interprete_org_eclipse_emf_ecore_EReference((org.eclipse.emf.ecore.EReference) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.emf.ecore.EStructuralFeature) {
			result = interprete_org_eclipse_emf_ecore_EStructuralFeature((org.eclipse.emf.ecore.EStructuralFeature) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.emf.ecore.EParameter) {
			result = interprete_org_eclipse_emf_ecore_EParameter((org.eclipse.emf.ecore.EParameter) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.emf.ecore.EOperation) {
			result = interprete_org_eclipse_emf_ecore_EOperation((org.eclipse.emf.ecore.EOperation) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.emf.ecore.ETypedElement) {
			result = interprete_org_eclipse_emf_ecore_ETypedElement((org.eclipse.emf.ecore.ETypedElement) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.emf.ecore.EPackage) {
			result = interprete_org_eclipse_emf_ecore_EPackage((org.eclipse.emf.ecore.EPackage) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.emf.ecore.EObject) {
			result = interprete_org_eclipse_emf_ecore_EObject((org.eclipse.emf.ecore.EObject) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.emf.ecore.EEnumLiteral) {
			result = interprete_org_eclipse_emf_ecore_EEnumLiteral((org.eclipse.emf.ecore.EEnumLiteral) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.emf.ecore.EEnum) {
			result = interprete_org_eclipse_emf_ecore_EEnum((org.eclipse.emf.ecore.EEnum) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.emf.ecore.EDataType) {
			result = interprete_org_eclipse_emf_ecore_EDataType((org.eclipse.emf.ecore.EDataType) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.emf.ecore.EClass) {
			result = interprete_org_eclipse_emf_ecore_EClass((org.eclipse.emf.ecore.EClass) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.emf.ecore.EClassifier) {
			result = interprete_org_eclipse_emf_ecore_EClassifier((org.eclipse.emf.ecore.EClassifier) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.emf.ecore.ENamedElement) {
			result = interprete_org_eclipse_emf_ecore_ENamedElement((org.eclipse.emf.ecore.ENamedElement) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.emf.ecore.EFactory) {
			result = interprete_org_eclipse_emf_ecore_EFactory((org.eclipse.emf.ecore.EFactory) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.emf.ecore.EAnnotation) {
			result = interprete_org_eclipse_emf_ecore_EAnnotation((org.eclipse.emf.ecore.EAnnotation) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.emf.ecore.EAttribute) {
			result = interprete_org_eclipse_emf_ecore_EAttribute((org.eclipse.emf.ecore.EAttribute) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.emf.ecore.EModelElement) {
			result = interprete_org_eclipse_emf_ecore_EModelElement((org.eclipse.emf.ecore.EModelElement) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof sare.language.acm.Subject) {
			result = interprete_sare_language_acm_Subject((sare.language.acm.Subject) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof sare.language.acm.Permission) {
			result = interprete_sare_language_acm_Permission((sare.language.acm.Permission) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof sare.language.acm.AccessControlModel) {
			result = interprete_sare_language_acm_AccessControlModel((sare.language.acm.AccessControlModel) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof sare.language.acm.User) {
			result = interprete_sare_language_acm_User((sare.language.acm.User) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof sare.language.acm.Role) {
			result = interprete_sare_language_acm_Role((sare.language.acm.Role) object, context);
		}
		if (result != null) {
			return result;
		}
		return result;
	}
	
	public ResultType interprete_sare_language_acm_Role(sare.language.acm.Role object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_sare_language_acm_User(sare.language.acm.User object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_sare_language_acm_AccessControlModel(sare.language.acm.AccessControlModel object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_sare_language_acm_Permission(sare.language.acm.Permission object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_sare_language_acm_Subject(sare.language.acm.Subject object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_emf_ecore_EAttribute(org.eclipse.emf.ecore.EAttribute object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_emf_ecore_EAnnotation(org.eclipse.emf.ecore.EAnnotation object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_emf_ecore_EClass(org.eclipse.emf.ecore.EClass object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_emf_ecore_EClassifier(org.eclipse.emf.ecore.EClassifier object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_emf_ecore_EDataType(org.eclipse.emf.ecore.EDataType object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_emf_ecore_EEnum(org.eclipse.emf.ecore.EEnum object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_emf_ecore_EEnumLiteral(org.eclipse.emf.ecore.EEnumLiteral object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_emf_ecore_EFactory(org.eclipse.emf.ecore.EFactory object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_emf_ecore_EModelElement(org.eclipse.emf.ecore.EModelElement object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_emf_ecore_ENamedElement(org.eclipse.emf.ecore.ENamedElement object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_emf_ecore_EObject(org.eclipse.emf.ecore.EObject object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_emf_ecore_EOperation(org.eclipse.emf.ecore.EOperation object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_emf_ecore_EPackage(org.eclipse.emf.ecore.EPackage object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_emf_ecore_EParameter(org.eclipse.emf.ecore.EParameter object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_emf_ecore_EReference(org.eclipse.emf.ecore.EReference object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_emf_ecore_EStructuralFeature(org.eclipse.emf.ecore.EStructuralFeature object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_emf_ecore_ETypedElement(org.eclipse.emf.ecore.ETypedElement object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_java_util_Map_Entry(java.util.Map.Entry<?,?> object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_emf_ecore_EGenericType(org.eclipse.emf.ecore.EGenericType object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_emf_ecore_ETypeParameter(org.eclipse.emf.ecore.ETypeParameter object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_Comment(org.eclipse.uml2.uml.Comment object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_Element(org.eclipse.uml2.uml.Element object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_Package(org.eclipse.uml2.uml.Package object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_PackageableElement(org.eclipse.uml2.uml.PackageableElement object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_NamedElement(org.eclipse.uml2.uml.NamedElement object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_Dependency(org.eclipse.uml2.uml.Dependency object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_DirectedRelationship(org.eclipse.uml2.uml.DirectedRelationship object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_Relationship(org.eclipse.uml2.uml.Relationship object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_Namespace(org.eclipse.uml2.uml.Namespace object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_ElementImport(org.eclipse.uml2.uml.ElementImport object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_PackageImport(org.eclipse.uml2.uml.PackageImport object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_Constraint(org.eclipse.uml2.uml.Constraint object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_ValueSpecification(org.eclipse.uml2.uml.ValueSpecification object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_TypedElement(org.eclipse.uml2.uml.TypedElement object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_Type(org.eclipse.uml2.uml.Type object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_Association(org.eclipse.uml2.uml.Association object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_Classifier(org.eclipse.uml2.uml.Classifier object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_RedefinableElement(org.eclipse.uml2.uml.RedefinableElement object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_TemplateableElement(org.eclipse.uml2.uml.TemplateableElement object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_TemplateBinding(org.eclipse.uml2.uml.TemplateBinding object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_TemplateSignature(org.eclipse.uml2.uml.TemplateSignature object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_TemplateParameter(org.eclipse.uml2.uml.TemplateParameter object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_ParameterableElement(org.eclipse.uml2.uml.ParameterableElement object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_TemplateParameterSubstitution(org.eclipse.uml2.uml.TemplateParameterSubstitution object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_Generalization(org.eclipse.uml2.uml.Generalization object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_GeneralizationSet(org.eclipse.uml2.uml.GeneralizationSet object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_Feature(org.eclipse.uml2.uml.Feature object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_Substitution(org.eclipse.uml2.uml.Substitution object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_Realization(org.eclipse.uml2.uml.Realization object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_Abstraction(org.eclipse.uml2.uml.Abstraction object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_OpaqueExpression(org.eclipse.uml2.uml.OpaqueExpression object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_Parameter(org.eclipse.uml2.uml.Parameter object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_MultiplicityElement(org.eclipse.uml2.uml.MultiplicityElement object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_ConnectableElement(org.eclipse.uml2.uml.ConnectableElement object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_ConnectorEnd(org.eclipse.uml2.uml.ConnectorEnd object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_Property(org.eclipse.uml2.uml.Property object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_DeploymentTarget(org.eclipse.uml2.uml.DeploymentTarget object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_Deployment(org.eclipse.uml2.uml.Deployment object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_DeployedArtifact(org.eclipse.uml2.uml.DeployedArtifact object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_DeploymentSpecification(org.eclipse.uml2.uml.DeploymentSpecification object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_Artifact(org.eclipse.uml2.uml.Artifact object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_Manifestation(org.eclipse.uml2.uml.Manifestation object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_Operation(org.eclipse.uml2.uml.Operation object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_BehavioralFeature(org.eclipse.uml2.uml.BehavioralFeature object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_Behavior(org.eclipse.uml2.uml.Behavior object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_Class(org.eclipse.uml2.uml.Class object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_BehavioredClassifier(org.eclipse.uml2.uml.BehavioredClassifier object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_InterfaceRealization(org.eclipse.uml2.uml.InterfaceRealization object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_Interface(org.eclipse.uml2.uml.Interface object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_Reception(org.eclipse.uml2.uml.Reception object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_Signal(org.eclipse.uml2.uml.Signal object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_ProtocolStateMachine(org.eclipse.uml2.uml.ProtocolStateMachine object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_StateMachine(org.eclipse.uml2.uml.StateMachine object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_Region(org.eclipse.uml2.uml.Region object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_Vertex(org.eclipse.uml2.uml.Vertex object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_Transition(org.eclipse.uml2.uml.Transition object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_Trigger(org.eclipse.uml2.uml.Trigger object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_Event(org.eclipse.uml2.uml.Event object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_Port(org.eclipse.uml2.uml.Port object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_State(org.eclipse.uml2.uml.State object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_ConnectionPointReference(org.eclipse.uml2.uml.ConnectionPointReference object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_Pseudostate(org.eclipse.uml2.uml.Pseudostate object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_ProtocolConformance(org.eclipse.uml2.uml.ProtocolConformance object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_EncapsulatedClassifier(org.eclipse.uml2.uml.EncapsulatedClassifier object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_StructuredClassifier(org.eclipse.uml2.uml.StructuredClassifier object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_Connector(org.eclipse.uml2.uml.Connector object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_Extension(org.eclipse.uml2.uml.Extension object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_ExtensionEnd(org.eclipse.uml2.uml.ExtensionEnd object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_Stereotype(org.eclipse.uml2.uml.Stereotype object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_Image(org.eclipse.uml2.uml.Image object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_Profile(org.eclipse.uml2.uml.Profile object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_Model(org.eclipse.uml2.uml.Model object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_ParameterSet(org.eclipse.uml2.uml.ParameterSet object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_DataType(org.eclipse.uml2.uml.DataType object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_OperationTemplateParameter(org.eclipse.uml2.uml.OperationTemplateParameter object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_StructuralFeature(org.eclipse.uml2.uml.StructuralFeature object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_ConnectableElementTemplateParameter(org.eclipse.uml2.uml.ConnectableElementTemplateParameter object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_CollaborationUse(org.eclipse.uml2.uml.CollaborationUse object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_Collaboration(org.eclipse.uml2.uml.Collaboration object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_UseCase(org.eclipse.uml2.uml.UseCase object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_Include(org.eclipse.uml2.uml.Include object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_Extend(org.eclipse.uml2.uml.Extend object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_ExtensionPoint(org.eclipse.uml2.uml.ExtensionPoint object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_RedefinableTemplateSignature(org.eclipse.uml2.uml.RedefinableTemplateSignature object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_ClassifierTemplateParameter(org.eclipse.uml2.uml.ClassifierTemplateParameter object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_StringExpression(org.eclipse.uml2.uml.StringExpression object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_Expression(org.eclipse.uml2.uml.Expression object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_Usage(org.eclipse.uml2.uml.Usage object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_PackageMerge(org.eclipse.uml2.uml.PackageMerge object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_ProfileApplication(org.eclipse.uml2.uml.ProfileApplication object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_Enumeration(org.eclipse.uml2.uml.Enumeration object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_EnumerationLiteral(org.eclipse.uml2.uml.EnumerationLiteral object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_InstanceSpecification(org.eclipse.uml2.uml.InstanceSpecification object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_Slot(org.eclipse.uml2.uml.Slot object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_PrimitiveType(org.eclipse.uml2.uml.PrimitiveType object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_LiteralSpecification(org.eclipse.uml2.uml.LiteralSpecification object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_LiteralInteger(org.eclipse.uml2.uml.LiteralInteger object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_LiteralString(org.eclipse.uml2.uml.LiteralString object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_LiteralBoolean(org.eclipse.uml2.uml.LiteralBoolean object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_LiteralNull(org.eclipse.uml2.uml.LiteralNull object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_InstanceValue(org.eclipse.uml2.uml.InstanceValue object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_LiteralUnlimitedNatural(org.eclipse.uml2.uml.LiteralUnlimitedNatural object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_OpaqueBehavior(org.eclipse.uml2.uml.OpaqueBehavior object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_FunctionBehavior(org.eclipse.uml2.uml.FunctionBehavior object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_OpaqueAction(org.eclipse.uml2.uml.OpaqueAction object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_Action(org.eclipse.uml2.uml.Action object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_ExecutableNode(org.eclipse.uml2.uml.ExecutableNode object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_ActivityNode(org.eclipse.uml2.uml.ActivityNode object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_StructuredActivityNode(org.eclipse.uml2.uml.StructuredActivityNode object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_ActivityGroup(org.eclipse.uml2.uml.ActivityGroup object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_Activity(org.eclipse.uml2.uml.Activity object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_Variable(org.eclipse.uml2.uml.Variable object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_ActivityEdge(org.eclipse.uml2.uml.ActivityEdge object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_ActivityPartition(org.eclipse.uml2.uml.ActivityPartition object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_InterruptibleActivityRegion(org.eclipse.uml2.uml.InterruptibleActivityRegion object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_ExceptionHandler(org.eclipse.uml2.uml.ExceptionHandler object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_ObjectNode(org.eclipse.uml2.uml.ObjectNode object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_OutputPin(org.eclipse.uml2.uml.OutputPin object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_Pin(org.eclipse.uml2.uml.Pin object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_InputPin(org.eclipse.uml2.uml.InputPin object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_CallAction(org.eclipse.uml2.uml.CallAction object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_InvocationAction(org.eclipse.uml2.uml.InvocationAction object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_SendSignalAction(org.eclipse.uml2.uml.SendSignalAction object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_CallOperationAction(org.eclipse.uml2.uml.CallOperationAction object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_CallBehaviorAction(org.eclipse.uml2.uml.CallBehaviorAction object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_SequenceNode(org.eclipse.uml2.uml.SequenceNode object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_ControlNode(org.eclipse.uml2.uml.ControlNode object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_ControlFlow(org.eclipse.uml2.uml.ControlFlow object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_InitialNode(org.eclipse.uml2.uml.InitialNode object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_ActivityParameterNode(org.eclipse.uml2.uml.ActivityParameterNode object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_ValuePin(org.eclipse.uml2.uml.ValuePin object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_Message(org.eclipse.uml2.uml.Message object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_MessageEnd(org.eclipse.uml2.uml.MessageEnd object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_Interaction(org.eclipse.uml2.uml.Interaction object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_InteractionFragment(org.eclipse.uml2.uml.InteractionFragment object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_Lifeline(org.eclipse.uml2.uml.Lifeline object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_PartDecomposition(org.eclipse.uml2.uml.PartDecomposition object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_InteractionUse(org.eclipse.uml2.uml.InteractionUse object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_Gate(org.eclipse.uml2.uml.Gate object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_GeneralOrdering(org.eclipse.uml2.uml.GeneralOrdering object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_OccurrenceSpecification(org.eclipse.uml2.uml.OccurrenceSpecification object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_InteractionOperand(org.eclipse.uml2.uml.InteractionOperand object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_InteractionConstraint(org.eclipse.uml2.uml.InteractionConstraint object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_ExecutionSpecification(org.eclipse.uml2.uml.ExecutionSpecification object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_StateInvariant(org.eclipse.uml2.uml.StateInvariant object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_ActionExecutionSpecification(org.eclipse.uml2.uml.ActionExecutionSpecification object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_BehaviorExecutionSpecification(org.eclipse.uml2.uml.BehaviorExecutionSpecification object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_ExecutionEvent(org.eclipse.uml2.uml.ExecutionEvent object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_CreationEvent(org.eclipse.uml2.uml.CreationEvent object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_DestructionEvent(org.eclipse.uml2.uml.DestructionEvent object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_SendOperationEvent(org.eclipse.uml2.uml.SendOperationEvent object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_MessageEvent(org.eclipse.uml2.uml.MessageEvent object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_SendSignalEvent(org.eclipse.uml2.uml.SendSignalEvent object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_MessageOccurrenceSpecification(org.eclipse.uml2.uml.MessageOccurrenceSpecification object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_ExecutionOccurrenceSpecification(org.eclipse.uml2.uml.ExecutionOccurrenceSpecification object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_ReceiveOperationEvent(org.eclipse.uml2.uml.ReceiveOperationEvent object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_ReceiveSignalEvent(org.eclipse.uml2.uml.ReceiveSignalEvent object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_Actor(org.eclipse.uml2.uml.Actor object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_CallEvent(org.eclipse.uml2.uml.CallEvent object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_ChangeEvent(org.eclipse.uml2.uml.ChangeEvent object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_SignalEvent(org.eclipse.uml2.uml.SignalEvent object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_AnyReceiveEvent(org.eclipse.uml2.uml.AnyReceiveEvent object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_ForkNode(org.eclipse.uml2.uml.ForkNode object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_FlowFinalNode(org.eclipse.uml2.uml.FlowFinalNode object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_FinalNode(org.eclipse.uml2.uml.FinalNode object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_CentralBufferNode(org.eclipse.uml2.uml.CentralBufferNode object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_MergeNode(org.eclipse.uml2.uml.MergeNode object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_DecisionNode(org.eclipse.uml2.uml.DecisionNode object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_ObjectFlow(org.eclipse.uml2.uml.ObjectFlow object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_ActivityFinalNode(org.eclipse.uml2.uml.ActivityFinalNode object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_ComponentRealization(org.eclipse.uml2.uml.ComponentRealization object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_Component(org.eclipse.uml2.uml.Component object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_Node(org.eclipse.uml2.uml.Node object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_CommunicationPath(org.eclipse.uml2.uml.CommunicationPath object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_Device(org.eclipse.uml2.uml.Device object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_ExecutionEnvironment(org.eclipse.uml2.uml.ExecutionEnvironment object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_CombinedFragment(org.eclipse.uml2.uml.CombinedFragment object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_Continuation(org.eclipse.uml2.uml.Continuation object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_ConsiderIgnoreFragment(org.eclipse.uml2.uml.ConsiderIgnoreFragment object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_CreateObjectAction(org.eclipse.uml2.uml.CreateObjectAction object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_DestroyObjectAction(org.eclipse.uml2.uml.DestroyObjectAction object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_TestIdentityAction(org.eclipse.uml2.uml.TestIdentityAction object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_ReadSelfAction(org.eclipse.uml2.uml.ReadSelfAction object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_StructuralFeatureAction(org.eclipse.uml2.uml.StructuralFeatureAction object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_ReadStructuralFeatureAction(org.eclipse.uml2.uml.ReadStructuralFeatureAction object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_WriteStructuralFeatureAction(org.eclipse.uml2.uml.WriteStructuralFeatureAction object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_ClearStructuralFeatureAction(org.eclipse.uml2.uml.ClearStructuralFeatureAction object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_RemoveStructuralFeatureValueAction(org.eclipse.uml2.uml.RemoveStructuralFeatureValueAction object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_AddStructuralFeatureValueAction(org.eclipse.uml2.uml.AddStructuralFeatureValueAction object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_LinkAction(org.eclipse.uml2.uml.LinkAction object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_LinkEndData(org.eclipse.uml2.uml.LinkEndData object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_QualifierValue(org.eclipse.uml2.uml.QualifierValue object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_ReadLinkAction(org.eclipse.uml2.uml.ReadLinkAction object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_LinkEndCreationData(org.eclipse.uml2.uml.LinkEndCreationData object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_CreateLinkAction(org.eclipse.uml2.uml.CreateLinkAction object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_WriteLinkAction(org.eclipse.uml2.uml.WriteLinkAction object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_DestroyLinkAction(org.eclipse.uml2.uml.DestroyLinkAction object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_LinkEndDestructionData(org.eclipse.uml2.uml.LinkEndDestructionData object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_ClearAssociationAction(org.eclipse.uml2.uml.ClearAssociationAction object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_BroadcastSignalAction(org.eclipse.uml2.uml.BroadcastSignalAction object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_SendObjectAction(org.eclipse.uml2.uml.SendObjectAction object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_ValueSpecificationAction(org.eclipse.uml2.uml.ValueSpecificationAction object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_TimeExpression(org.eclipse.uml2.uml.TimeExpression object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_Observation(org.eclipse.uml2.uml.Observation object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_Duration(org.eclipse.uml2.uml.Duration object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_DurationInterval(org.eclipse.uml2.uml.DurationInterval object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_Interval(org.eclipse.uml2.uml.Interval object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_TimeConstraint(org.eclipse.uml2.uml.TimeConstraint object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_IntervalConstraint(org.eclipse.uml2.uml.IntervalConstraint object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_TimeInterval(org.eclipse.uml2.uml.TimeInterval object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_DurationConstraint(org.eclipse.uml2.uml.DurationConstraint object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_TimeObservation(org.eclipse.uml2.uml.TimeObservation object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_DurationObservation(org.eclipse.uml2.uml.DurationObservation object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_FinalState(org.eclipse.uml2.uml.FinalState object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_TimeEvent(org.eclipse.uml2.uml.TimeEvent object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_VariableAction(org.eclipse.uml2.uml.VariableAction object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_ReadVariableAction(org.eclipse.uml2.uml.ReadVariableAction object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_WriteVariableAction(org.eclipse.uml2.uml.WriteVariableAction object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_ClearVariableAction(org.eclipse.uml2.uml.ClearVariableAction object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_AddVariableValueAction(org.eclipse.uml2.uml.AddVariableValueAction object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_RemoveVariableValueAction(org.eclipse.uml2.uml.RemoveVariableValueAction object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_RaiseExceptionAction(org.eclipse.uml2.uml.RaiseExceptionAction object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_ActionInputPin(org.eclipse.uml2.uml.ActionInputPin object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_InformationItem(org.eclipse.uml2.uml.InformationItem object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_InformationFlow(org.eclipse.uml2.uml.InformationFlow object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_ReadExtentAction(org.eclipse.uml2.uml.ReadExtentAction object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_ReclassifyObjectAction(org.eclipse.uml2.uml.ReclassifyObjectAction object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_ReadIsClassifiedObjectAction(org.eclipse.uml2.uml.ReadIsClassifiedObjectAction object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_StartClassifierBehaviorAction(org.eclipse.uml2.uml.StartClassifierBehaviorAction object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_ReadLinkObjectEndAction(org.eclipse.uml2.uml.ReadLinkObjectEndAction object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_ReadLinkObjectEndQualifierAction(org.eclipse.uml2.uml.ReadLinkObjectEndQualifierAction object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_CreateLinkObjectAction(org.eclipse.uml2.uml.CreateLinkObjectAction object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_AcceptEventAction(org.eclipse.uml2.uml.AcceptEventAction object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_AcceptCallAction(org.eclipse.uml2.uml.AcceptCallAction object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_ReplyAction(org.eclipse.uml2.uml.ReplyAction object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_UnmarshallAction(org.eclipse.uml2.uml.UnmarshallAction object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_ReduceAction(org.eclipse.uml2.uml.ReduceAction object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_StartObjectBehaviorAction(org.eclipse.uml2.uml.StartObjectBehaviorAction object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_JoinNode(org.eclipse.uml2.uml.JoinNode object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_DataStoreNode(org.eclipse.uml2.uml.DataStoreNode object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_ConditionalNode(org.eclipse.uml2.uml.ConditionalNode object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_Clause(org.eclipse.uml2.uml.Clause object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_LoopNode(org.eclipse.uml2.uml.LoopNode object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_ExpansionNode(org.eclipse.uml2.uml.ExpansionNode object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_ExpansionRegion(org.eclipse.uml2.uml.ExpansionRegion object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_ProtocolTransition(org.eclipse.uml2.uml.ProtocolTransition object, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_uml2_uml_AssociationClass(org.eclipse.uml2.uml.AssociationClass object, ContextType context) {
		return null;
	}
	
	private void notifyListeners(org.eclipse.emf.ecore.EObject element) {
		for (sare.language.acm.resource.acm.IAcmInterpreterListener listener : listeners) {
			listener.handleInterpreteObject(element);
		}
	}
	
	/**
	 * Adds the given object to the interpretation stack. Attention: Objects that are
	 * added first, are interpret last.
	 */
	public void addObjectToInterprete(org.eclipse.emf.ecore.EObject object) {
		interpretationStack.push(object);
	}
	
	/**
	 * Adds the given collection of objects to the interpretation stack. Attention:
	 * Collections that are added first, are interpret last.
	 */
	public void addObjectsToInterprete(java.util.Collection<? extends org.eclipse.emf.ecore.EObject> objects) {
		for (org.eclipse.emf.ecore.EObject object : objects) {
			addObjectToInterprete(object);
		}
	}
	
	/**
	 * Adds the given collection of objects in reverse order to the interpretation
	 * stack.
	 */
	public void addObjectsToInterpreteInReverseOrder(java.util.Collection<? extends org.eclipse.emf.ecore.EObject> objects) {
		java.util.List<org.eclipse.emf.ecore.EObject> reverse = new java.util.ArrayList<org.eclipse.emf.ecore.EObject>(objects.size());
		reverse.addAll(objects);
		java.util.Collections.reverse(reverse);
		addObjectsToInterprete(reverse);
	}
	
	/**
	 * Adds the given object and all its children to the interpretation stack such
	 * that they are interpret in top down order.
	 */
	public void addObjectTreeToInterpreteTopDown(org.eclipse.emf.ecore.EObject root) {
		java.util.List<org.eclipse.emf.ecore.EObject> objects = new java.util.ArrayList<org.eclipse.emf.ecore.EObject>();
		objects.add(root);
		java.util.Iterator<org.eclipse.emf.ecore.EObject> it = root.eAllContents();
		while (it.hasNext()) {
			org.eclipse.emf.ecore.EObject eObject = (org.eclipse.emf.ecore.EObject) it.next();
			objects.add(eObject);
		}
		addObjectsToInterpreteInReverseOrder(objects);
	}
	
	public void addListener(sare.language.acm.resource.acm.IAcmInterpreterListener newListener) {
		listeners.add(newListener);
	}
	
	public boolean removeListener(sare.language.acm.resource.acm.IAcmInterpreterListener listener) {
		return listeners.remove(listener);
	}
	
	public org.eclipse.emf.ecore.EObject getNextObjectToInterprete() {
		return nextObjectToInterprete;
	}
	
	public java.util.Stack<org.eclipse.emf.ecore.EObject> getInterpretationStack() {
		return interpretationStack;
	}
	
	public void terminate() {
		interpretationStack.clear();
	}
	
	public Object getCurrentContext() {
		return currentContext;
	}
	
}
