/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package sare.language.acm.resource.acm.mopp;

/**
 * A basic implementation of the
 * sare.language.acm.resource.acm.IAcmReferenceResolveResult interface that
 * collects mappings in a list.
 * 
 * @param <ReferenceType> the type of the references that can be contained in this
 * result
 */
public class AcmReferenceResolveResult<ReferenceType> implements sare.language.acm.resource.acm.IAcmReferenceResolveResult<ReferenceType> {
	
	private java.util.Collection<sare.language.acm.resource.acm.IAcmReferenceMapping<ReferenceType>> mappings;
	private String errorMessage;
	private boolean resolveFuzzy;
	private java.util.Set<sare.language.acm.resource.acm.IAcmQuickFix> quickFixes;
	
	public AcmReferenceResolveResult(boolean resolveFuzzy) {
		super();
		this.resolveFuzzy = resolveFuzzy;
	}
	
	public String getErrorMessage() {
		return errorMessage;
	}
	
	public java.util.Collection<sare.language.acm.resource.acm.IAcmQuickFix> getQuickFixes() {
		if (quickFixes == null) {
			quickFixes = new java.util.LinkedHashSet<sare.language.acm.resource.acm.IAcmQuickFix>();
		}
		return java.util.Collections.unmodifiableSet(quickFixes);
	}
	
	public void addQuickFix(sare.language.acm.resource.acm.IAcmQuickFix quickFix) {
		if (quickFixes == null) {
			quickFixes = new java.util.LinkedHashSet<sare.language.acm.resource.acm.IAcmQuickFix>();
		}
		quickFixes.add(quickFix);
	}
	
	public java.util.Collection<sare.language.acm.resource.acm.IAcmReferenceMapping<ReferenceType>> getMappings() {
		return mappings;
	}
	
	public boolean wasResolved() {
		return mappings != null;
	}
	
	public boolean wasResolvedMultiple() {
		return mappings != null && mappings.size() > 1;
	}
	
	public boolean wasResolvedUniquely() {
		return mappings != null && mappings.size() == 1;
	}
	
	public void setErrorMessage(String message) {
		errorMessage = message;
	}
	
	public void addMapping(String identifier, ReferenceType target) {
		if (!resolveFuzzy && target == null) {
			throw new IllegalArgumentException("Mapping references to null is only allowed for fuzzy resolution.");
		}
		addMapping(identifier, target, null);
	}
	
	public void addMapping(String identifier, ReferenceType target, String warning) {
		if (mappings == null) {
			mappings = new java.util.ArrayList<sare.language.acm.resource.acm.IAcmReferenceMapping<ReferenceType>>(1);
		}
		mappings.add(new sare.language.acm.resource.acm.mopp.AcmElementMapping<ReferenceType>(identifier, target, warning));
		errorMessage = null;
	}
	
	public void addMapping(String identifier, org.eclipse.emf.common.util.URI uri) {
		addMapping(identifier, uri, null);
	}
	
	public void addMapping(String identifier, org.eclipse.emf.common.util.URI uri, String warning) {
		if (mappings == null) {
			mappings = new java.util.ArrayList<sare.language.acm.resource.acm.IAcmReferenceMapping<ReferenceType>>(1);
		}
		mappings.add(new sare.language.acm.resource.acm.mopp.AcmURIMapping<ReferenceType>(identifier, uri, warning));
	}
}
