/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package sare.language.acm.resource.acm.mopp;

/**
 * An Excpetion to represent invalid content types for parser instances.
 * 
 * @see sare.language.acm.resource.acm.IAcmOptions.RESOURCE_CONTENT_TYPE
 */
public class AcmUnexpectedContentTypeException extends org.antlr.runtime3_3_0.RecognitionException {
	
	private static final long serialVersionUID = 4791359811519433999L;
	
	private Object contentType = null;
	
	public  AcmUnexpectedContentTypeException(Object contentType) {
		this.contentType = contentType;
	}
	
	public Object getContentType() {
		return contentType;
	}
	
}
