/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package sare.language.acm.resource.acm;

public enum AcmEProblemSeverity {
	WARNING, ERROR;
}
