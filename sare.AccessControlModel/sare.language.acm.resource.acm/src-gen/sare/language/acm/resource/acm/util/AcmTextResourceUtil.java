/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package sare.language.acm.resource.acm.util;

/**
 * Class AcmTextResourceUtil can be used to perform common tasks on text
 * resources, such as loading and saving resources, as well as, checking them for
 * errors. This class is deprecated and has been replaced by
 * sare.language.acm.resource.acm.util.AcmResourceUtil.
 */
public class AcmTextResourceUtil {
	
	@Deprecated	
	public static sare.language.acm.resource.acm.mopp.AcmResource getResource(org.eclipse.core.resources.IFile file) {
		return sare.language.acm.resource.acm.util.AcmResourceUtil.getResource(file);
	}
	
	@Deprecated	
	public static sare.language.acm.resource.acm.mopp.AcmResource getResource(java.io.File file, java.util.Map<?,?> options) {
		return sare.language.acm.resource.acm.util.AcmResourceUtil.getResource(file, options);
	}
	
	@Deprecated	
	public static sare.language.acm.resource.acm.mopp.AcmResource getResource(org.eclipse.emf.common.util.URI uri) {
		return sare.language.acm.resource.acm.util.AcmResourceUtil.getResource(uri);
	}
	
	@Deprecated	
	public static sare.language.acm.resource.acm.mopp.AcmResource getResource(org.eclipse.emf.common.util.URI uri, java.util.Map<?,?> options) {
		return sare.language.acm.resource.acm.util.AcmResourceUtil.getResource(uri, options);
	}
	
}
