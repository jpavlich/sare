/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package sare.language.acm.resource.acm.grammar;

/**
 * This class provides the follow sets for all terminals of the grammar. These
 * sets are used during code completion.
 */
public class AcmFollowSetProvider {
	
	public final static sare.language.acm.resource.acm.IAcmExpectedElement TERMINAL_0 = new sare.language.acm.resource.acm.mopp.AcmExpectedCsString(sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.ACM_0_0_0_0_0_0_0);
	public final static sare.language.acm.resource.acm.IAcmExpectedElement TERMINAL_1 = new sare.language.acm.resource.acm.mopp.AcmExpectedCsString(sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.ACM_0_0_0_1);
	public final static sare.language.acm.resource.acm.IAcmExpectedElement TERMINAL_2 = new sare.language.acm.resource.acm.mopp.AcmExpectedStructuralFeature(sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.ACM_0_0_0_0_0_0_1);
	public final static sare.language.acm.resource.acm.IAcmExpectedElement TERMINAL_3 = new sare.language.acm.resource.acm.mopp.AcmExpectedStructuralFeature(sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.ACM_0_0_0_2);
	public final static sare.language.acm.resource.acm.IAcmExpectedElement TERMINAL_4 = new sare.language.acm.resource.acm.mopp.AcmExpectedCsString(sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.ACM_0_0_0_3);
	public final static sare.language.acm.resource.acm.IAcmExpectedElement TERMINAL_5 = new sare.language.acm.resource.acm.mopp.AcmExpectedCsString(sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.ACM_1_0_0_0);
	public final static sare.language.acm.resource.acm.IAcmExpectedElement TERMINAL_6 = new sare.language.acm.resource.acm.mopp.AcmExpectedCsString(sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.ACM_2_0_0_0);
	public final static sare.language.acm.resource.acm.IAcmExpectedElement TERMINAL_7 = new sare.language.acm.resource.acm.mopp.AcmExpectedCsString(sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.ACM_0_0_0_5);
	public final static sare.language.acm.resource.acm.IAcmExpectedElement TERMINAL_8 = new sare.language.acm.resource.acm.mopp.AcmExpectedCsString(sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_0_0_0_0_0_0_0);
	public final static sare.language.acm.resource.acm.IAcmExpectedElement TERMINAL_9 = new sare.language.acm.resource.acm.mopp.AcmExpectedCsString(sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_0_0_0_1);
	public final static sare.language.acm.resource.acm.IAcmExpectedElement TERMINAL_10 = new sare.language.acm.resource.acm.mopp.AcmExpectedCsString(sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_2_0_0_0);
	public final static sare.language.acm.resource.acm.IAcmExpectedElement TERMINAL_11 = new sare.language.acm.resource.acm.mopp.AcmExpectedCsString(sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_7_0_0_0);
	public final static sare.language.acm.resource.acm.IAcmExpectedElement TERMINAL_12 = new sare.language.acm.resource.acm.mopp.AcmExpectedCsString(sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_7_0_1_0);
	public final static sare.language.acm.resource.acm.IAcmExpectedElement TERMINAL_13 = new sare.language.acm.resource.acm.mopp.AcmExpectedCsString(sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_3_0_0_1);
	public final static sare.language.acm.resource.acm.IAcmExpectedElement TERMINAL_14 = new sare.language.acm.resource.acm.mopp.AcmExpectedCsString(sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_0_0_0_5);
	public final static sare.language.acm.resource.acm.IAcmExpectedElement TERMINAL_15 = new sare.language.acm.resource.acm.mopp.AcmExpectedCsString(sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_2_0_0_4);
	public final static sare.language.acm.resource.acm.IAcmExpectedElement TERMINAL_16 = new sare.language.acm.resource.acm.mopp.AcmExpectedStructuralFeature(sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.ACM_1_0_0_1);
	public final static sare.language.acm.resource.acm.IAcmExpectedElement TERMINAL_17 = new sare.language.acm.resource.acm.mopp.AcmExpectedCsString(sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.ACM_1_0_0_2_0_0_0);
	public final static sare.language.acm.resource.acm.IAcmExpectedElement TERMINAL_18 = new sare.language.acm.resource.acm.mopp.AcmExpectedCsString(sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.ACM_1_0_0_3);
	public final static sare.language.acm.resource.acm.IAcmExpectedElement TERMINAL_19 = new sare.language.acm.resource.acm.mopp.AcmExpectedStructuralFeature(sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.ACM_1_0_0_2_0_0_1);
	public final static sare.language.acm.resource.acm.IAcmExpectedElement TERMINAL_20 = new sare.language.acm.resource.acm.mopp.AcmExpectedCsString(sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.ACM_1_0_0_2_0_0_2_0_0_0);
	public final static sare.language.acm.resource.acm.IAcmExpectedElement TERMINAL_21 = new sare.language.acm.resource.acm.mopp.AcmExpectedStructuralFeature(sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.ACM_1_0_0_2_0_0_2_0_0_1);
	public final static sare.language.acm.resource.acm.IAcmExpectedElement TERMINAL_22 = new sare.language.acm.resource.acm.mopp.AcmExpectedStructuralFeature(sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.ACM_3_0_0_0_0_0_0);
	public final static sare.language.acm.resource.acm.IAcmExpectedElement TERMINAL_23 = new sare.language.acm.resource.acm.mopp.AcmExpectedStructuralFeature(sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.ACM_3_0_0_0_0_1_0);
	public final static sare.language.acm.resource.acm.IAcmExpectedElement TERMINAL_24 = new sare.language.acm.resource.acm.mopp.AcmExpectedCsString(sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.ACM_1_0_0_5);
	public final static sare.language.acm.resource.acm.IAcmExpectedElement TERMINAL_25 = new sare.language.acm.resource.acm.mopp.AcmExpectedStructuralFeature(sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.ACM_2_0_0_1);
	public final static sare.language.acm.resource.acm.IAcmExpectedElement TERMINAL_26 = new sare.language.acm.resource.acm.mopp.AcmExpectedCsString(sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.ACM_2_0_0_2);
	public final static sare.language.acm.resource.acm.IAcmExpectedElement TERMINAL_27 = new sare.language.acm.resource.acm.mopp.AcmExpectedCsString(sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.ACM_2_0_0_3_0_0_0);
	public final static sare.language.acm.resource.acm.IAcmExpectedElement TERMINAL_28 = new sare.language.acm.resource.acm.mopp.AcmExpectedStructuralFeature(sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.ACM_2_0_0_3_0_0_1);
	public final static sare.language.acm.resource.acm.IAcmExpectedElement TERMINAL_29 = new sare.language.acm.resource.acm.mopp.AcmExpectedCsString(sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.ACM_3_0_0_1_0_0_0);
	public final static sare.language.acm.resource.acm.IAcmExpectedElement TERMINAL_30 = new sare.language.acm.resource.acm.mopp.AcmExpectedCsString(sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.ACM_3_0_0_2_0_0_0);
	public final static sare.language.acm.resource.acm.IAcmExpectedElement TERMINAL_31 = new sare.language.acm.resource.acm.mopp.AcmExpectedCsString(sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.ACM_3_0_0_2_0_0_2);
	public final static sare.language.acm.resource.acm.IAcmExpectedElement TERMINAL_32 = new sare.language.acm.resource.acm.mopp.AcmExpectedStructuralFeature(sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.ACM_3_0_0_1_0_0_1);
	public final static sare.language.acm.resource.acm.IAcmExpectedElement TERMINAL_33 = new sare.language.acm.resource.acm.mopp.AcmExpectedStructuralFeature(sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_1_0_0_0);
	public final static sare.language.acm.resource.acm.IAcmExpectedElement TERMINAL_34 = new sare.language.acm.resource.acm.mopp.AcmExpectedStructuralFeature(sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_0_0_0_2);
	public final static sare.language.acm.resource.acm.IAcmExpectedElement TERMINAL_35 = new sare.language.acm.resource.acm.mopp.AcmExpectedCsString(sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_0_0_0_3);
	public final static sare.language.acm.resource.acm.IAcmExpectedElement TERMINAL_36 = new sare.language.acm.resource.acm.mopp.AcmExpectedStructuralFeature(sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_2_0_0_1);
	public final static sare.language.acm.resource.acm.IAcmExpectedElement TERMINAL_37 = new sare.language.acm.resource.acm.mopp.AcmExpectedCsString(sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_2_0_0_2);
	public final static sare.language.acm.resource.acm.IAcmExpectedElement TERMINAL_38 = new sare.language.acm.resource.acm.mopp.AcmExpectedStructuralFeature(sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_3_0_0_2);
	public final static sare.language.acm.resource.acm.IAcmExpectedElement TERMINAL_39 = new sare.language.acm.resource.acm.mopp.AcmExpectedCsString(sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_3_0_0_3_0_0_0);
	public final static sare.language.acm.resource.acm.IAcmExpectedElement TERMINAL_40 = new sare.language.acm.resource.acm.mopp.AcmExpectedCsString(sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_3_0_0_4);
	public final static sare.language.acm.resource.acm.IAcmExpectedElement TERMINAL_41 = new sare.language.acm.resource.acm.mopp.AcmExpectedStructuralFeature(sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_3_0_0_3_0_0_1);
	public final static sare.language.acm.resource.acm.IAcmExpectedElement TERMINAL_42 = new sare.language.acm.resource.acm.mopp.AcmExpectedCsString(sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_3_0_0_3_0_0_2_0_0_0);
	public final static sare.language.acm.resource.acm.IAcmExpectedElement TERMINAL_43 = new sare.language.acm.resource.acm.mopp.AcmExpectedStructuralFeature(sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_3_0_0_3_0_0_2_0_0_1);
	public final static sare.language.acm.resource.acm.IAcmExpectedElement TERMINAL_44 = new sare.language.acm.resource.acm.mopp.AcmExpectedStructuralFeature(sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_5_0_0_1);
	public final static sare.language.acm.resource.acm.IAcmExpectedElement TERMINAL_45 = new sare.language.acm.resource.acm.mopp.AcmExpectedStructuralFeature(sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_4_0_0_1);
	public final static sare.language.acm.resource.acm.IAcmExpectedElement TERMINAL_46 = new sare.language.acm.resource.acm.mopp.AcmExpectedCsString(sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_3_0_0_6);
	public final static sare.language.acm.resource.acm.IAcmExpectedElement TERMINAL_47 = new sare.language.acm.resource.acm.mopp.AcmExpectedCsString(sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_4_0_0_2);
	public final static sare.language.acm.resource.acm.IAcmExpectedElement TERMINAL_48 = new sare.language.acm.resource.acm.mopp.AcmExpectedStructuralFeature(sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_6_0_0_1);
	public final static sare.language.acm.resource.acm.IAcmExpectedElement TERMINAL_49 = new sare.language.acm.resource.acm.mopp.AcmExpectedCsString(sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_4_0_0_4);
	public final static sare.language.acm.resource.acm.IAcmExpectedElement TERMINAL_50 = new sare.language.acm.resource.acm.mopp.AcmExpectedCsString(sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_4_0_0_3_0_0_1_0_0_0);
	public final static sare.language.acm.resource.acm.IAcmExpectedElement TERMINAL_51 = new sare.language.acm.resource.acm.mopp.AcmExpectedCsString(sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_4_0_0_5_0_0_0);
	public final static sare.language.acm.resource.acm.IAcmExpectedElement TERMINAL_52 = new sare.language.acm.resource.acm.mopp.AcmExpectedStructuralFeature(sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_4_0_0_5_0_0_1);
	public final static sare.language.acm.resource.acm.IAcmExpectedElement TERMINAL_53 = new sare.language.acm.resource.acm.mopp.AcmExpectedCsString(sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_5_0_0_2);
	public final static sare.language.acm.resource.acm.IAcmExpectedElement TERMINAL_54 = new sare.language.acm.resource.acm.mopp.AcmExpectedStructuralFeature(sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_5_0_0_3);
	public final static sare.language.acm.resource.acm.IAcmExpectedElement TERMINAL_55 = new sare.language.acm.resource.acm.mopp.AcmExpectedCsString(sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_6_0_0_2);
	public final static sare.language.acm.resource.acm.IAcmExpectedElement TERMINAL_56 = new sare.language.acm.resource.acm.mopp.AcmExpectedStructuralFeature(sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_6_0_0_3);
	public final static sare.language.acm.resource.acm.IAcmExpectedElement TERMINAL_57 = new sare.language.acm.resource.acm.mopp.AcmExpectedStructuralFeature(sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_7_0_0_1);
	public final static sare.language.acm.resource.acm.IAcmExpectedElement TERMINAL_58 = new sare.language.acm.resource.acm.mopp.AcmExpectedCsString(sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_7_0_0_2);
	public final static sare.language.acm.resource.acm.IAcmExpectedElement TERMINAL_59 = new sare.language.acm.resource.acm.mopp.AcmExpectedStructuralFeature(sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_7_0_1_1);
	public final static sare.language.acm.resource.acm.IAcmExpectedElement TERMINAL_60 = new sare.language.acm.resource.acm.mopp.AcmExpectedCsString(sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_7_0_1_2);
	
	public final static org.eclipse.emf.ecore.EStructuralFeature FEATURE_0 = sare.language.acm.AcmPackage.eINSTANCE.getAccessControlModel().getEStructuralFeature(sare.language.acm.AcmPackage.ACCESS_CONTROL_MODEL__ELEMENTS);
	public final static org.eclipse.emf.ecore.EStructuralFeature FEATURE_1 = org.eclipse.uml2.uml.UMLPackage.eINSTANCE.getPackage().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.PACKAGE__PACKAGED_ELEMENT);
	public final static org.eclipse.emf.ecore.EStructuralFeature FEATURE_2 = org.eclipse.uml2.uml.UMLPackage.eINSTANCE.getElement().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.ELEMENT__OWNED_COMMENT);
	public final static org.eclipse.emf.ecore.EStructuralFeature FEATURE_3 = sare.language.acm.AcmPackage.eINSTANCE.getRole().getEStructuralFeature(sare.language.acm.AcmPackage.ROLE__PERMISSIONS);
	public final static org.eclipse.emf.ecore.EStructuralFeature FEATURE_4 = sare.language.acm.AcmPackage.eINSTANCE.getPermission().getEStructuralFeature(sare.language.acm.AcmPackage.PERMISSION__CHILD_PERMISSIONS);
	public final static org.eclipse.emf.ecore.EStructuralFeature FEATURE_5 = org.eclipse.uml2.uml.UMLPackage.eINSTANCE.getNamespace().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.NAMESPACE__PACKAGE_IMPORT);
	public final static org.eclipse.emf.ecore.EStructuralFeature FEATURE_6 = org.eclipse.uml2.uml.UMLPackage.eINSTANCE.getStructuredClassifier().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.STRUCTURED_CLASSIFIER__OWNED_ATTRIBUTE);
	public final static org.eclipse.emf.ecore.EStructuralFeature FEATURE_7 = org.eclipse.uml2.uml.UMLPackage.eINSTANCE.getClass_().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.CLASS__OWNED_OPERATION);
	public final static org.eclipse.emf.ecore.EStructuralFeature FEATURE_8 = org.eclipse.uml2.uml.UMLPackage.eINSTANCE.getBehavioralFeature().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.BEHAVIORAL_FEATURE__OWNED_PARAMETER);
	
	public final static org.eclipse.emf.ecore.EStructuralFeature[] EMPTY_FEATURE_ARRAY = new org.eclipse.emf.ecore.EStructuralFeature[0];
	
	public static void wire0() {
		TERMINAL_0.addFollower(TERMINAL_2, EMPTY_FEATURE_ARRAY);
		TERMINAL_2.addFollower(TERMINAL_0, EMPTY_FEATURE_ARRAY);
		TERMINAL_2.addFollower(TERMINAL_1, EMPTY_FEATURE_ARRAY);
		TERMINAL_1.addFollower(TERMINAL_3, EMPTY_FEATURE_ARRAY);
		TERMINAL_3.addFollower(TERMINAL_4, EMPTY_FEATURE_ARRAY);
		TERMINAL_4.addFollower(TERMINAL_5, new org.eclipse.emf.ecore.EStructuralFeature[] {FEATURE_0, });
		TERMINAL_4.addFollower(TERMINAL_6, new org.eclipse.emf.ecore.EStructuralFeature[] {FEATURE_0, });
		TERMINAL_4.addFollower(TERMINAL_7, EMPTY_FEATURE_ARRAY);
		TERMINAL_7.addFollower(TERMINAL_0, new org.eclipse.emf.ecore.EStructuralFeature[] {FEATURE_1, });
		TERMINAL_7.addFollower(TERMINAL_1, new org.eclipse.emf.ecore.EStructuralFeature[] {FEATURE_1, });
		TERMINAL_7.addFollower(TERMINAL_8, new org.eclipse.emf.ecore.EStructuralFeature[] {FEATURE_1, });
		TERMINAL_7.addFollower(TERMINAL_9, new org.eclipse.emf.ecore.EStructuralFeature[] {FEATURE_1, });
		TERMINAL_7.addFollower(TERMINAL_10, new org.eclipse.emf.ecore.EStructuralFeature[] {FEATURE_1, });
		TERMINAL_7.addFollower(TERMINAL_11, new org.eclipse.emf.ecore.EStructuralFeature[] {FEATURE_2, FEATURE_1, });
		TERMINAL_7.addFollower(TERMINAL_12, new org.eclipse.emf.ecore.EStructuralFeature[] {FEATURE_2, FEATURE_1, });
		TERMINAL_7.addFollower(TERMINAL_13, new org.eclipse.emf.ecore.EStructuralFeature[] {FEATURE_1, });
		TERMINAL_7.addFollower(TERMINAL_14, EMPTY_FEATURE_ARRAY);
		TERMINAL_7.addFollower(TERMINAL_15, EMPTY_FEATURE_ARRAY);
		TERMINAL_5.addFollower(TERMINAL_16, EMPTY_FEATURE_ARRAY);
		TERMINAL_16.addFollower(TERMINAL_17, EMPTY_FEATURE_ARRAY);
		TERMINAL_16.addFollower(TERMINAL_18, EMPTY_FEATURE_ARRAY);
		TERMINAL_17.addFollower(TERMINAL_19, EMPTY_FEATURE_ARRAY);
		TERMINAL_19.addFollower(TERMINAL_20, EMPTY_FEATURE_ARRAY);
		TERMINAL_19.addFollower(TERMINAL_18, EMPTY_FEATURE_ARRAY);
		TERMINAL_20.addFollower(TERMINAL_21, EMPTY_FEATURE_ARRAY);
		TERMINAL_21.addFollower(TERMINAL_20, EMPTY_FEATURE_ARRAY);
		TERMINAL_21.addFollower(TERMINAL_18, EMPTY_FEATURE_ARRAY);
		TERMINAL_18.addFollower(TERMINAL_22, new org.eclipse.emf.ecore.EStructuralFeature[] {FEATURE_3, });
		TERMINAL_18.addFollower(TERMINAL_23, new org.eclipse.emf.ecore.EStructuralFeature[] {FEATURE_3, });
		TERMINAL_18.addFollower(TERMINAL_24, EMPTY_FEATURE_ARRAY);
		TERMINAL_24.addFollower(TERMINAL_5, new org.eclipse.emf.ecore.EStructuralFeature[] {FEATURE_0, });
		TERMINAL_24.addFollower(TERMINAL_6, new org.eclipse.emf.ecore.EStructuralFeature[] {FEATURE_0, });
		TERMINAL_24.addFollower(TERMINAL_7, EMPTY_FEATURE_ARRAY);
		TERMINAL_6.addFollower(TERMINAL_25, EMPTY_FEATURE_ARRAY);
		TERMINAL_25.addFollower(TERMINAL_26, EMPTY_FEATURE_ARRAY);
		TERMINAL_26.addFollower(TERMINAL_27, EMPTY_FEATURE_ARRAY);
		TERMINAL_26.addFollower(TERMINAL_5, new org.eclipse.emf.ecore.EStructuralFeature[] {FEATURE_0, });
		TERMINAL_26.addFollower(TERMINAL_6, new org.eclipse.emf.ecore.EStructuralFeature[] {FEATURE_0, });
		TERMINAL_26.addFollower(TERMINAL_7, EMPTY_FEATURE_ARRAY);
		TERMINAL_27.addFollower(TERMINAL_28, EMPTY_FEATURE_ARRAY);
		TERMINAL_28.addFollower(TERMINAL_27, EMPTY_FEATURE_ARRAY);
		TERMINAL_28.addFollower(TERMINAL_5, new org.eclipse.emf.ecore.EStructuralFeature[] {FEATURE_0, });
		TERMINAL_28.addFollower(TERMINAL_6, new org.eclipse.emf.ecore.EStructuralFeature[] {FEATURE_0, });
		TERMINAL_28.addFollower(TERMINAL_7, EMPTY_FEATURE_ARRAY);
		TERMINAL_22.addFollower(TERMINAL_29, EMPTY_FEATURE_ARRAY);
		TERMINAL_22.addFollower(TERMINAL_30, EMPTY_FEATURE_ARRAY);
		TERMINAL_22.addFollower(TERMINAL_22, new org.eclipse.emf.ecore.EStructuralFeature[] {FEATURE_3, });
		TERMINAL_22.addFollower(TERMINAL_23, new org.eclipse.emf.ecore.EStructuralFeature[] {FEATURE_3, });
		TERMINAL_22.addFollower(TERMINAL_24, EMPTY_FEATURE_ARRAY);
		TERMINAL_22.addFollower(TERMINAL_31, EMPTY_FEATURE_ARRAY);
		TERMINAL_23.addFollower(TERMINAL_29, EMPTY_FEATURE_ARRAY);
		TERMINAL_23.addFollower(TERMINAL_30, EMPTY_FEATURE_ARRAY);
		TERMINAL_23.addFollower(TERMINAL_22, new org.eclipse.emf.ecore.EStructuralFeature[] {FEATURE_3, });
		TERMINAL_23.addFollower(TERMINAL_23, new org.eclipse.emf.ecore.EStructuralFeature[] {FEATURE_3, });
		TERMINAL_23.addFollower(TERMINAL_24, EMPTY_FEATURE_ARRAY);
		TERMINAL_23.addFollower(TERMINAL_31, EMPTY_FEATURE_ARRAY);
		TERMINAL_29.addFollower(TERMINAL_32, EMPTY_FEATURE_ARRAY);
		TERMINAL_32.addFollower(TERMINAL_30, EMPTY_FEATURE_ARRAY);
		TERMINAL_32.addFollower(TERMINAL_22, new org.eclipse.emf.ecore.EStructuralFeature[] {FEATURE_3, });
		TERMINAL_32.addFollower(TERMINAL_23, new org.eclipse.emf.ecore.EStructuralFeature[] {FEATURE_3, });
		TERMINAL_32.addFollower(TERMINAL_24, EMPTY_FEATURE_ARRAY);
		TERMINAL_32.addFollower(TERMINAL_31, EMPTY_FEATURE_ARRAY);
		TERMINAL_30.addFollower(TERMINAL_22, new org.eclipse.emf.ecore.EStructuralFeature[] {FEATURE_4, });
		TERMINAL_30.addFollower(TERMINAL_23, new org.eclipse.emf.ecore.EStructuralFeature[] {FEATURE_4, });
		TERMINAL_30.addFollower(TERMINAL_31, EMPTY_FEATURE_ARRAY);
		TERMINAL_31.addFollower(TERMINAL_22, new org.eclipse.emf.ecore.EStructuralFeature[] {FEATURE_3, });
		TERMINAL_31.addFollower(TERMINAL_23, new org.eclipse.emf.ecore.EStructuralFeature[] {FEATURE_3, });
		TERMINAL_31.addFollower(TERMINAL_24, EMPTY_FEATURE_ARRAY);
		TERMINAL_31.addFollower(TERMINAL_31, EMPTY_FEATURE_ARRAY);
		TERMINAL_8.addFollower(TERMINAL_33, new org.eclipse.emf.ecore.EStructuralFeature[] {FEATURE_5, });
		TERMINAL_9.addFollower(TERMINAL_34, EMPTY_FEATURE_ARRAY);
		TERMINAL_34.addFollower(TERMINAL_35, EMPTY_FEATURE_ARRAY);
		TERMINAL_35.addFollower(TERMINAL_0, new org.eclipse.emf.ecore.EStructuralFeature[] {FEATURE_1, });
		TERMINAL_35.addFollower(TERMINAL_1, new org.eclipse.emf.ecore.EStructuralFeature[] {FEATURE_1, });
		TERMINAL_35.addFollower(TERMINAL_8, new org.eclipse.emf.ecore.EStructuralFeature[] {FEATURE_1, });
		TERMINAL_35.addFollower(TERMINAL_9, new org.eclipse.emf.ecore.EStructuralFeature[] {FEATURE_1, });
		TERMINAL_35.addFollower(TERMINAL_10, new org.eclipse.emf.ecore.EStructuralFeature[] {FEATURE_1, });
		TERMINAL_35.addFollower(TERMINAL_11, new org.eclipse.emf.ecore.EStructuralFeature[] {FEATURE_2, FEATURE_1, });
		TERMINAL_35.addFollower(TERMINAL_12, new org.eclipse.emf.ecore.EStructuralFeature[] {FEATURE_2, FEATURE_1, });
		TERMINAL_35.addFollower(TERMINAL_13, new org.eclipse.emf.ecore.EStructuralFeature[] {FEATURE_1, });
		TERMINAL_35.addFollower(TERMINAL_14, EMPTY_FEATURE_ARRAY);
		TERMINAL_14.addFollower(TERMINAL_0, new org.eclipse.emf.ecore.EStructuralFeature[] {FEATURE_1, });
		TERMINAL_14.addFollower(TERMINAL_1, new org.eclipse.emf.ecore.EStructuralFeature[] {FEATURE_1, });
		TERMINAL_14.addFollower(TERMINAL_8, new org.eclipse.emf.ecore.EStructuralFeature[] {FEATURE_1, });
		TERMINAL_14.addFollower(TERMINAL_9, new org.eclipse.emf.ecore.EStructuralFeature[] {FEATURE_1, });
		TERMINAL_14.addFollower(TERMINAL_10, new org.eclipse.emf.ecore.EStructuralFeature[] {FEATURE_1, });
		TERMINAL_14.addFollower(TERMINAL_11, new org.eclipse.emf.ecore.EStructuralFeature[] {FEATURE_2, FEATURE_1, });
		TERMINAL_14.addFollower(TERMINAL_12, new org.eclipse.emf.ecore.EStructuralFeature[] {FEATURE_2, FEATURE_1, });
		TERMINAL_14.addFollower(TERMINAL_13, new org.eclipse.emf.ecore.EStructuralFeature[] {FEATURE_1, });
		TERMINAL_14.addFollower(TERMINAL_14, EMPTY_FEATURE_ARRAY);
		TERMINAL_14.addFollower(TERMINAL_15, EMPTY_FEATURE_ARRAY);
		TERMINAL_33.addFollower(TERMINAL_8, EMPTY_FEATURE_ARRAY);
		TERMINAL_33.addFollower(TERMINAL_9, EMPTY_FEATURE_ARRAY);
		TERMINAL_10.addFollower(TERMINAL_36, EMPTY_FEATURE_ARRAY);
		TERMINAL_36.addFollower(TERMINAL_37, EMPTY_FEATURE_ARRAY);
		TERMINAL_37.addFollower(TERMINAL_0, new org.eclipse.emf.ecore.EStructuralFeature[] {FEATURE_1, });
		TERMINAL_37.addFollower(TERMINAL_1, new org.eclipse.emf.ecore.EStructuralFeature[] {FEATURE_1, });
		TERMINAL_37.addFollower(TERMINAL_8, new org.eclipse.emf.ecore.EStructuralFeature[] {FEATURE_1, });
		TERMINAL_37.addFollower(TERMINAL_9, new org.eclipse.emf.ecore.EStructuralFeature[] {FEATURE_1, });
		TERMINAL_37.addFollower(TERMINAL_10, new org.eclipse.emf.ecore.EStructuralFeature[] {FEATURE_1, });
		TERMINAL_37.addFollower(TERMINAL_11, new org.eclipse.emf.ecore.EStructuralFeature[] {FEATURE_2, FEATURE_1, });
		TERMINAL_37.addFollower(TERMINAL_12, new org.eclipse.emf.ecore.EStructuralFeature[] {FEATURE_2, FEATURE_1, });
		TERMINAL_37.addFollower(TERMINAL_13, new org.eclipse.emf.ecore.EStructuralFeature[] {FEATURE_1, });
		TERMINAL_37.addFollower(TERMINAL_15, EMPTY_FEATURE_ARRAY);
		TERMINAL_15.addFollower(TERMINAL_0, new org.eclipse.emf.ecore.EStructuralFeature[] {FEATURE_1, });
		TERMINAL_15.addFollower(TERMINAL_1, new org.eclipse.emf.ecore.EStructuralFeature[] {FEATURE_1, });
		TERMINAL_15.addFollower(TERMINAL_8, new org.eclipse.emf.ecore.EStructuralFeature[] {FEATURE_1, });
		TERMINAL_15.addFollower(TERMINAL_9, new org.eclipse.emf.ecore.EStructuralFeature[] {FEATURE_1, });
		TERMINAL_15.addFollower(TERMINAL_10, new org.eclipse.emf.ecore.EStructuralFeature[] {FEATURE_1, });
		TERMINAL_15.addFollower(TERMINAL_11, new org.eclipse.emf.ecore.EStructuralFeature[] {FEATURE_2, FEATURE_1, });
		TERMINAL_15.addFollower(TERMINAL_12, new org.eclipse.emf.ecore.EStructuralFeature[] {FEATURE_2, FEATURE_1, });
		TERMINAL_15.addFollower(TERMINAL_13, new org.eclipse.emf.ecore.EStructuralFeature[] {FEATURE_1, });
		TERMINAL_15.addFollower(TERMINAL_14, EMPTY_FEATURE_ARRAY);
		TERMINAL_15.addFollower(TERMINAL_15, EMPTY_FEATURE_ARRAY);
		TERMINAL_13.addFollower(TERMINAL_38, EMPTY_FEATURE_ARRAY);
		TERMINAL_38.addFollower(TERMINAL_39, EMPTY_FEATURE_ARRAY);
		TERMINAL_38.addFollower(TERMINAL_40, EMPTY_FEATURE_ARRAY);
		TERMINAL_39.addFollower(TERMINAL_41, EMPTY_FEATURE_ARRAY);
		TERMINAL_41.addFollower(TERMINAL_42, EMPTY_FEATURE_ARRAY);
		TERMINAL_41.addFollower(TERMINAL_40, EMPTY_FEATURE_ARRAY);
		TERMINAL_42.addFollower(TERMINAL_43, EMPTY_FEATURE_ARRAY);
		TERMINAL_43.addFollower(TERMINAL_42, EMPTY_FEATURE_ARRAY);
		TERMINAL_43.addFollower(TERMINAL_40, EMPTY_FEATURE_ARRAY);
		TERMINAL_40.addFollower(TERMINAL_11, new org.eclipse.emf.ecore.EStructuralFeature[] {FEATURE_2, FEATURE_6, });
		TERMINAL_40.addFollower(TERMINAL_12, new org.eclipse.emf.ecore.EStructuralFeature[] {FEATURE_2, FEATURE_6, });
		TERMINAL_40.addFollower(TERMINAL_44, new org.eclipse.emf.ecore.EStructuralFeature[] {FEATURE_6, });
		TERMINAL_40.addFollower(TERMINAL_45, new org.eclipse.emf.ecore.EStructuralFeature[] {FEATURE_7, });
		TERMINAL_40.addFollower(TERMINAL_46, EMPTY_FEATURE_ARRAY);
		TERMINAL_46.addFollower(TERMINAL_0, new org.eclipse.emf.ecore.EStructuralFeature[] {FEATURE_1, });
		TERMINAL_46.addFollower(TERMINAL_1, new org.eclipse.emf.ecore.EStructuralFeature[] {FEATURE_1, });
		TERMINAL_46.addFollower(TERMINAL_8, new org.eclipse.emf.ecore.EStructuralFeature[] {FEATURE_1, });
		TERMINAL_46.addFollower(TERMINAL_9, new org.eclipse.emf.ecore.EStructuralFeature[] {FEATURE_1, });
		TERMINAL_46.addFollower(TERMINAL_10, new org.eclipse.emf.ecore.EStructuralFeature[] {FEATURE_1, });
		TERMINAL_46.addFollower(TERMINAL_11, new org.eclipse.emf.ecore.EStructuralFeature[] {FEATURE_2, FEATURE_1, });
		TERMINAL_46.addFollower(TERMINAL_12, new org.eclipse.emf.ecore.EStructuralFeature[] {FEATURE_2, FEATURE_1, });
		TERMINAL_46.addFollower(TERMINAL_13, new org.eclipse.emf.ecore.EStructuralFeature[] {FEATURE_1, });
		TERMINAL_46.addFollower(TERMINAL_14, EMPTY_FEATURE_ARRAY);
		TERMINAL_46.addFollower(TERMINAL_15, EMPTY_FEATURE_ARRAY);
		TERMINAL_45.addFollower(TERMINAL_47, EMPTY_FEATURE_ARRAY);
		TERMINAL_47.addFollower(TERMINAL_11, new org.eclipse.emf.ecore.EStructuralFeature[] {FEATURE_2, FEATURE_8, });
		TERMINAL_47.addFollower(TERMINAL_12, new org.eclipse.emf.ecore.EStructuralFeature[] {FEATURE_2, FEATURE_8, });
		TERMINAL_47.addFollower(TERMINAL_48, new org.eclipse.emf.ecore.EStructuralFeature[] {FEATURE_8, });
		TERMINAL_47.addFollower(TERMINAL_49, EMPTY_FEATURE_ARRAY);
		TERMINAL_50.addFollower(TERMINAL_11, new org.eclipse.emf.ecore.EStructuralFeature[] {FEATURE_2, FEATURE_8, });
		TERMINAL_50.addFollower(TERMINAL_12, new org.eclipse.emf.ecore.EStructuralFeature[] {FEATURE_2, FEATURE_8, });
		TERMINAL_50.addFollower(TERMINAL_48, new org.eclipse.emf.ecore.EStructuralFeature[] {FEATURE_8, });
		TERMINAL_49.addFollower(TERMINAL_51, EMPTY_FEATURE_ARRAY);
		TERMINAL_49.addFollower(TERMINAL_11, new org.eclipse.emf.ecore.EStructuralFeature[] {FEATURE_2, FEATURE_6, });
		TERMINAL_49.addFollower(TERMINAL_12, new org.eclipse.emf.ecore.EStructuralFeature[] {FEATURE_2, FEATURE_6, });
		TERMINAL_49.addFollower(TERMINAL_44, new org.eclipse.emf.ecore.EStructuralFeature[] {FEATURE_6, });
		TERMINAL_49.addFollower(TERMINAL_45, new org.eclipse.emf.ecore.EStructuralFeature[] {FEATURE_7, });
		TERMINAL_49.addFollower(TERMINAL_46, EMPTY_FEATURE_ARRAY);
		TERMINAL_51.addFollower(TERMINAL_52, EMPTY_FEATURE_ARRAY);
		TERMINAL_52.addFollower(TERMINAL_11, new org.eclipse.emf.ecore.EStructuralFeature[] {FEATURE_2, FEATURE_6, });
		TERMINAL_52.addFollower(TERMINAL_12, new org.eclipse.emf.ecore.EStructuralFeature[] {FEATURE_2, FEATURE_6, });
		TERMINAL_52.addFollower(TERMINAL_44, new org.eclipse.emf.ecore.EStructuralFeature[] {FEATURE_6, });
		TERMINAL_52.addFollower(TERMINAL_45, new org.eclipse.emf.ecore.EStructuralFeature[] {FEATURE_7, });
		TERMINAL_52.addFollower(TERMINAL_46, EMPTY_FEATURE_ARRAY);
		TERMINAL_44.addFollower(TERMINAL_53, EMPTY_FEATURE_ARRAY);
		TERMINAL_53.addFollower(TERMINAL_54, EMPTY_FEATURE_ARRAY);
		TERMINAL_54.addFollower(TERMINAL_11, new org.eclipse.emf.ecore.EStructuralFeature[] {FEATURE_2, FEATURE_6, });
		TERMINAL_54.addFollower(TERMINAL_12, new org.eclipse.emf.ecore.EStructuralFeature[] {FEATURE_2, FEATURE_6, });
		TERMINAL_54.addFollower(TERMINAL_44, new org.eclipse.emf.ecore.EStructuralFeature[] {FEATURE_6, });
		TERMINAL_54.addFollower(TERMINAL_45, new org.eclipse.emf.ecore.EStructuralFeature[] {FEATURE_7, });
		TERMINAL_54.addFollower(TERMINAL_46, EMPTY_FEATURE_ARRAY);
		TERMINAL_48.addFollower(TERMINAL_55, EMPTY_FEATURE_ARRAY);
		TERMINAL_55.addFollower(TERMINAL_56, EMPTY_FEATURE_ARRAY);
		TERMINAL_56.addFollower(TERMINAL_50, EMPTY_FEATURE_ARRAY);
		TERMINAL_56.addFollower(TERMINAL_49, EMPTY_FEATURE_ARRAY);
		TERMINAL_11.addFollower(TERMINAL_57, EMPTY_FEATURE_ARRAY);
		TERMINAL_57.addFollower(TERMINAL_58, EMPTY_FEATURE_ARRAY);
		TERMINAL_58.addFollower(TERMINAL_11, new org.eclipse.emf.ecore.EStructuralFeature[] {FEATURE_2, });
		TERMINAL_58.addFollower(TERMINAL_12, new org.eclipse.emf.ecore.EStructuralFeature[] {FEATURE_2, });
		TERMINAL_58.addFollower(TERMINAL_13, EMPTY_FEATURE_ARRAY);
		TERMINAL_58.addFollower(TERMINAL_45, EMPTY_FEATURE_ARRAY);
		TERMINAL_58.addFollower(TERMINAL_44, EMPTY_FEATURE_ARRAY);
		TERMINAL_58.addFollower(TERMINAL_48, EMPTY_FEATURE_ARRAY);
		TERMINAL_12.addFollower(TERMINAL_59, EMPTY_FEATURE_ARRAY);
		TERMINAL_59.addFollower(TERMINAL_60, EMPTY_FEATURE_ARRAY);
		TERMINAL_60.addFollower(TERMINAL_11, new org.eclipse.emf.ecore.EStructuralFeature[] {FEATURE_2, });
		TERMINAL_60.addFollower(TERMINAL_12, new org.eclipse.emf.ecore.EStructuralFeature[] {FEATURE_2, });
		TERMINAL_60.addFollower(TERMINAL_13, EMPTY_FEATURE_ARRAY);
		TERMINAL_60.addFollower(TERMINAL_45, EMPTY_FEATURE_ARRAY);
		TERMINAL_60.addFollower(TERMINAL_44, EMPTY_FEATURE_ARRAY);
		TERMINAL_60.addFollower(TERMINAL_48, EMPTY_FEATURE_ARRAY);
	}
	// wire the terminals
	static {
		wire0();
	}
}
