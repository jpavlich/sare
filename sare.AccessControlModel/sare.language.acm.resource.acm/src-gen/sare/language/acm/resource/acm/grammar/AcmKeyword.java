/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package sare.language.acm.resource.acm.grammar;

/**
 * A class to represent a keyword in the grammar.
 */
public class AcmKeyword extends sare.language.acm.resource.acm.grammar.AcmSyntaxElement {
	
	private final String value;
	
	public AcmKeyword(String value, sare.language.acm.resource.acm.grammar.AcmCardinality cardinality) {
		super(cardinality, null);
		this.value = value;
	}
	
	public String getValue() {
		return value;
	}
	
	public String toString() {
		return value;
	}
	
}
