/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package sare.language.acm.resource.acm.grammar;

/**
 * A class to represent a rules in the grammar.
 */
public class AcmRule extends sare.language.acm.resource.acm.grammar.AcmSyntaxElement {
	
	private final org.eclipse.emf.ecore.EClass metaclass;
	
	public AcmRule(org.eclipse.emf.ecore.EClass metaclass, sare.language.acm.resource.acm.grammar.AcmChoice choice, sare.language.acm.resource.acm.grammar.AcmCardinality cardinality) {
		super(cardinality, new sare.language.acm.resource.acm.grammar.AcmSyntaxElement[] {choice});
		this.metaclass = metaclass;
	}
	
	public org.eclipse.emf.ecore.EClass getMetaclass() {
		return metaclass;
	}
	
	public sare.language.acm.resource.acm.grammar.AcmChoice getDefinition() {
		return (sare.language.acm.resource.acm.grammar.AcmChoice) getChildren()[0];
	}
	
}

