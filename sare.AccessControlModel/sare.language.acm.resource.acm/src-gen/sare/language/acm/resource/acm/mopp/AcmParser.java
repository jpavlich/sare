// $ANTLR ${project.version} ${buildNumber}

	package sare.language.acm.resource.acm.mopp;


import org.antlr.runtime3_3_0.*;
import java.util.HashMap;
@SuppressWarnings("unused")
public class AcmParser extends AcmANTLRParserBase {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "QUOTED_34_34", "TEXT", "NAMED_ELEMENT_ID", "WHITESPACE", "LINEBREAK", "'import'", "'policy'", "'{'", "'}'", "'role'", "'extends'", "','", "'user'", "'hasRoles'", "'if'", "'model'", "'package'", "'class'", "'('", "')'", "':'", "'\\u00ab'", "'\\u00bb'", "'<<'", "'>>'"
    };
    public static final int EOF=-1;
    public static final int T__9=9;
    public static final int T__10=10;
    public static final int T__11=11;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__19=19;
    public static final int T__20=20;
    public static final int T__21=21;
    public static final int T__22=22;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int QUOTED_34_34=4;
    public static final int TEXT=5;
    public static final int NAMED_ELEMENT_ID=6;
    public static final int WHITESPACE=7;
    public static final int LINEBREAK=8;

    // delegates
    // delegators


        public AcmParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public AcmParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
            this.state.ruleMemo = new HashMap[45+1];
             
             
        }
        

    public String[] getTokenNames() { return AcmParser.tokenNames; }
    public String getGrammarFileName() { return "Acm.g"; }


    	private sare.language.acm.resource.acm.IAcmTokenResolverFactory tokenResolverFactory = new sare.language.acm.resource.acm.mopp.AcmTokenResolverFactory();
    	
    	/**
    	 * the index of the last token that was handled by collectHiddenTokens()
    	 */
    	private int lastPosition;
    	
    	/**
    	 * A flag that indicates whether the parser should remember all expected elements.
    	 * This flag is set to true when using the parse for code completion. Otherwise it
    	 * is set to false.
    	 */
    	private boolean rememberExpectedElements = false;
    	
    	private Object parseToIndexTypeObject;
    	private int lastTokenIndex = 0;
    	
    	/**
    	 * A list of expected elements the were collected while parsing the input stream.
    	 * This list is only filled if <code>rememberExpectedElements</code> is set to
    	 * true.
    	 */
    	private java.util.List<sare.language.acm.resource.acm.mopp.AcmExpectedTerminal> expectedElements = new java.util.ArrayList<sare.language.acm.resource.acm.mopp.AcmExpectedTerminal>();
    	
    	private int mismatchedTokenRecoveryTries = 0;
    	/**
    	 * A helper list to allow a lexer to pass errors to its parser
    	 */
    	protected java.util.List<org.antlr.runtime3_3_0.RecognitionException> lexerExceptions = java.util.Collections.synchronizedList(new java.util.ArrayList<org.antlr.runtime3_3_0.RecognitionException>());
    	
    	/**
    	 * Another helper list to allow a lexer to pass positions of errors to its parser
    	 */
    	protected java.util.List<Integer> lexerExceptionsPosition = java.util.Collections.synchronizedList(new java.util.ArrayList<Integer>());
    	
    	/**
    	 * A stack for incomplete objects. This stack is used filled when the parser is
    	 * used for code completion. Whenever the parser starts to read an object it is
    	 * pushed on the stack. Once the element was parser completely it is popped from
    	 * the stack.
    	 */
    	protected java.util.Stack<org.eclipse.emf.ecore.EObject> incompleteObjects = new java.util.Stack<org.eclipse.emf.ecore.EObject>();
    	
    	private int stopIncludingHiddenTokens;
    	private int stopExcludingHiddenTokens;
    	private int tokenIndexOfLastCompleteElement;
    	
    	private int expectedElementsIndexOfLastCompleteElement;
    	
    	/**
    	 * The offset indicating the cursor position when the parser is used for code
    	 * completion by calling parseToExpectedElements().
    	 */
    	private int cursorOffset;
    	
    	/**
    	 * The offset of the first hidden token of the last expected element. This offset
    	 * is used to discard expected elements, which are not needed for code completion.
    	 */
    	private int lastStartIncludingHidden;
    	
    	protected void addErrorToResource(final String errorMessage, final int column, final int line, final int startIndex, final int stopIndex) {
    		postParseCommands.add(new sare.language.acm.resource.acm.IAcmCommand<sare.language.acm.resource.acm.IAcmTextResource>() {
    			public boolean execute(sare.language.acm.resource.acm.IAcmTextResource resource) {
    				if (resource == null) {
    					// the resource can be null if the parser is used for code completion
    					return true;
    				}
    				resource.addProblem(new sare.language.acm.resource.acm.IAcmProblem() {
    					public sare.language.acm.resource.acm.AcmEProblemSeverity getSeverity() {
    						return sare.language.acm.resource.acm.AcmEProblemSeverity.ERROR;
    					}
    					public sare.language.acm.resource.acm.AcmEProblemType getType() {
    						return sare.language.acm.resource.acm.AcmEProblemType.SYNTAX_ERROR;
    					}
    					public String getMessage() {
    						return errorMessage;
    					}
    					public java.util.Collection<sare.language.acm.resource.acm.IAcmQuickFix> getQuickFixes() {
    						return null;
    					}
    				}, column, line, startIndex, stopIndex);
    				return true;
    			}
    		});
    	}
    	
    	public void addExpectedElement(sare.language.acm.resource.acm.IAcmExpectedElement terminal, int followSetID, org.eclipse.emf.ecore.EStructuralFeature... containmentTrace) {
    		if (!this.rememberExpectedElements) {
    			return;
    		}
    		sare.language.acm.resource.acm.mopp.AcmExpectedTerminal expectedElement = new sare.language.acm.resource.acm.mopp.AcmExpectedTerminal(terminal, followSetID, containmentTrace);
    		setPosition(expectedElement, input.index());
    		int startIncludingHiddenTokens = expectedElement.getStartIncludingHiddenTokens();
    		if (lastStartIncludingHidden >= 0 && lastStartIncludingHidden < startIncludingHiddenTokens && cursorOffset > startIncludingHiddenTokens) {
    			// clear list of expected elements
    			this.expectedElements.clear();
    		}
    		lastStartIncludingHidden = startIncludingHiddenTokens;
    		this.expectedElements.add(expectedElement);
    	}
    	
    	protected void collectHiddenTokens(org.eclipse.emf.ecore.EObject element) {
    	}
    	
    	protected void copyLocalizationInfos(final org.eclipse.emf.ecore.EObject source, final org.eclipse.emf.ecore.EObject target) {
    		postParseCommands.add(new sare.language.acm.resource.acm.IAcmCommand<sare.language.acm.resource.acm.IAcmTextResource>() {
    			public boolean execute(sare.language.acm.resource.acm.IAcmTextResource resource) {
    				sare.language.acm.resource.acm.IAcmLocationMap locationMap = resource.getLocationMap();
    				if (locationMap == null) {
    					// the locationMap can be null if the parser is used for code completion
    					return true;
    				}
    				locationMap.setCharStart(target, locationMap.getCharStart(source));
    				locationMap.setCharEnd(target, locationMap.getCharEnd(source));
    				locationMap.setColumn(target, locationMap.getColumn(source));
    				locationMap.setLine(target, locationMap.getLine(source));
    				return true;
    			}
    		});
    	}
    	
    	protected void copyLocalizationInfos(final org.antlr.runtime3_3_0.CommonToken source, final org.eclipse.emf.ecore.EObject target) {
    		postParseCommands.add(new sare.language.acm.resource.acm.IAcmCommand<sare.language.acm.resource.acm.IAcmTextResource>() {
    			public boolean execute(sare.language.acm.resource.acm.IAcmTextResource resource) {
    				sare.language.acm.resource.acm.IAcmLocationMap locationMap = resource.getLocationMap();
    				if (locationMap == null) {
    					// the locationMap can be null if the parser is used for code completion
    					return true;
    				}
    				if (source == null) {
    					return true;
    				}
    				locationMap.setCharStart(target, source.getStartIndex());
    				locationMap.setCharEnd(target, source.getStopIndex());
    				locationMap.setColumn(target, source.getCharPositionInLine());
    				locationMap.setLine(target, source.getLine());
    				return true;
    			}
    		});
    	}
    	
    	/**
    	 * Sets the end character index and the last line for the given object in the
    	 * location map.
    	 */
    	protected void setLocalizationEnd(java.util.Collection<sare.language.acm.resource.acm.IAcmCommand<sare.language.acm.resource.acm.IAcmTextResource>> postParseCommands , final org.eclipse.emf.ecore.EObject object, final int endChar, final int endLine) {
    		postParseCommands.add(new sare.language.acm.resource.acm.IAcmCommand<sare.language.acm.resource.acm.IAcmTextResource>() {
    			public boolean execute(sare.language.acm.resource.acm.IAcmTextResource resource) {
    				sare.language.acm.resource.acm.IAcmLocationMap locationMap = resource.getLocationMap();
    				if (locationMap == null) {
    					// the locationMap can be null if the parser is used for code completion
    					return true;
    				}
    				locationMap.setCharEnd(object, endChar);
    				locationMap.setLine(object, endLine);
    				return true;
    			}
    		});
    	}
    	
    	public sare.language.acm.resource.acm.IAcmTextParser createInstance(java.io.InputStream actualInputStream, String encoding) {
    		try {
    			if (encoding == null) {
    				return new AcmParser(new org.antlr.runtime3_3_0.CommonTokenStream(new AcmLexer(new org.antlr.runtime3_3_0.ANTLRInputStream(actualInputStream))));
    			} else {
    				return new AcmParser(new org.antlr.runtime3_3_0.CommonTokenStream(new AcmLexer(new org.antlr.runtime3_3_0.ANTLRInputStream(actualInputStream, encoding))));
    			}
    		} catch (java.io.IOException e) {
    			sare.language.acm.resource.acm.mopp.AcmPlugin.logError("Error while creating parser.", e);
    			return null;
    		}
    	}
    	
    	/**
    	 * This default constructor is only used to call createInstance() on it.
    	 */
    	public AcmParser() {
    		super(null);
    	}
    	
    	protected org.eclipse.emf.ecore.EObject doParse() throws org.antlr.runtime3_3_0.RecognitionException {
    		this.lastPosition = 0;
    		// required because the lexer class can not be subclassed
    		((AcmLexer) getTokenStream().getTokenSource()).lexerExceptions = lexerExceptions;
    		((AcmLexer) getTokenStream().getTokenSource()).lexerExceptionsPosition = lexerExceptionsPosition;
    		Object typeObject = getTypeObject();
    		if (typeObject == null) {
    			return start();
    		} else if (typeObject instanceof org.eclipse.emf.ecore.EClass) {
    			org.eclipse.emf.ecore.EClass type = (org.eclipse.emf.ecore.EClass) typeObject;
    			if (type.getInstanceClass() == sare.language.acm.AccessControlModel.class) {
    				return parse_sare_language_acm_AccessControlModel();
    			}
    			if (type.getInstanceClass() == sare.language.acm.Role.class) {
    				return parse_sare_language_acm_Role();
    			}
    			if (type.getInstanceClass() == sare.language.acm.User.class) {
    				return parse_sare_language_acm_User();
    			}
    			if (type.getInstanceClass() == sare.language.acm.Permission.class) {
    				return parse_sare_language_acm_Permission();
    			}
    			if (type.getInstanceClass() == org.eclipse.uml2.uml.Model.class) {
    				return parse_org_eclipse_uml2_uml_Model();
    			}
    			if (type.getInstanceClass() == org.eclipse.uml2.uml.PackageImport.class) {
    				return parse_org_eclipse_uml2_uml_PackageImport();
    			}
    			if (type.getInstanceClass() == org.eclipse.uml2.uml.Package.class) {
    				return parse_org_eclipse_uml2_uml_Package();
    			}
    			if (type.getInstanceClass() == org.eclipse.uml2.uml.Class.class) {
    				return parse_org_eclipse_uml2_uml_Class();
    			}
    			if (type.getInstanceClass() == org.eclipse.uml2.uml.Operation.class) {
    				return parse_org_eclipse_uml2_uml_Operation();
    			}
    			if (type.getInstanceClass() == org.eclipse.uml2.uml.Property.class) {
    				return parse_org_eclipse_uml2_uml_Property();
    			}
    			if (type.getInstanceClass() == org.eclipse.uml2.uml.Parameter.class) {
    				return parse_org_eclipse_uml2_uml_Parameter();
    			}
    			if (type.getInstanceClass() == org.eclipse.uml2.uml.Comment.class) {
    				return parse_org_eclipse_uml2_uml_Comment();
    			}
    		}
    		throw new sare.language.acm.resource.acm.mopp.AcmUnexpectedContentTypeException(typeObject);
    	}
    	
    	public int getMismatchedTokenRecoveryTries() {
    		return mismatchedTokenRecoveryTries;
    	}
    	
    	public Object getMissingSymbol(org.antlr.runtime3_3_0.IntStream arg0, org.antlr.runtime3_3_0.RecognitionException arg1, int arg2, org.antlr.runtime3_3_0.BitSet arg3) {
    		mismatchedTokenRecoveryTries++;
    		return super.getMissingSymbol(arg0, arg1, arg2, arg3);
    	}
    	
    	public Object getParseToIndexTypeObject() {
    		return parseToIndexTypeObject;
    	}
    	
    	protected Object getTypeObject() {
    		Object typeObject = getParseToIndexTypeObject();
    		if (typeObject != null) {
    			return typeObject;
    		}
    		java.util.Map<?,?> options = getOptions();
    		if (options != null) {
    			typeObject = options.get(sare.language.acm.resource.acm.IAcmOptions.RESOURCE_CONTENT_TYPE);
    		}
    		return typeObject;
    	}
    	
    	/**
    	 * Implementation that calls {@link #doParse()} and handles the thrown
    	 * RecognitionExceptions.
    	 */
    	public sare.language.acm.resource.acm.IAcmParseResult parse() {
    		terminateParsing = false;
    		postParseCommands = new java.util.ArrayList<sare.language.acm.resource.acm.IAcmCommand<sare.language.acm.resource.acm.IAcmTextResource>>();
    		sare.language.acm.resource.acm.mopp.AcmParseResult parseResult = new sare.language.acm.resource.acm.mopp.AcmParseResult();
    		try {
    			org.eclipse.emf.ecore.EObject result =  doParse();
    			if (lexerExceptions.isEmpty()) {
    				parseResult.setRoot(result);
    			}
    		} catch (org.antlr.runtime3_3_0.RecognitionException re) {
    			reportError(re);
    		} catch (java.lang.IllegalArgumentException iae) {
    			if ("The 'no null' constraint is violated".equals(iae.getMessage())) {
    				// can be caused if a null is set on EMF models where not allowed. this will just
    				// happen if other errors occurred before
    			} else {
    				iae.printStackTrace();
    			}
    		}
    		for (org.antlr.runtime3_3_0.RecognitionException re : lexerExceptions) {
    			reportLexicalError(re);
    		}
    		parseResult.getPostParseCommands().addAll(postParseCommands);
    		return parseResult;
    	}
    	
    	public java.util.List<sare.language.acm.resource.acm.mopp.AcmExpectedTerminal> parseToExpectedElements(org.eclipse.emf.ecore.EClass type, sare.language.acm.resource.acm.IAcmTextResource dummyResource, int cursorOffset) {
    		this.rememberExpectedElements = true;
    		this.parseToIndexTypeObject = type;
    		this.cursorOffset = cursorOffset;
    		this.lastStartIncludingHidden = -1;
    		final org.antlr.runtime3_3_0.CommonTokenStream tokenStream = (org.antlr.runtime3_3_0.CommonTokenStream) getTokenStream();
    		sare.language.acm.resource.acm.IAcmParseResult result = parse();
    		for (org.eclipse.emf.ecore.EObject incompleteObject : incompleteObjects) {
    			org.antlr.runtime3_3_0.Lexer lexer = (org.antlr.runtime3_3_0.Lexer) tokenStream.getTokenSource();
    			int endChar = lexer.getCharIndex();
    			int endLine = lexer.getLine();
    			setLocalizationEnd(result.getPostParseCommands(), incompleteObject, endChar, endLine);
    		}
    		if (result != null) {
    			org.eclipse.emf.ecore.EObject root = result.getRoot();
    			if (root != null) {
    				dummyResource.getContentsInternal().add(root);
    			}
    			for (sare.language.acm.resource.acm.IAcmCommand<sare.language.acm.resource.acm.IAcmTextResource> command : result.getPostParseCommands()) {
    				command.execute(dummyResource);
    			}
    		}
    		// remove all expected elements that were added after the last complete element
    		expectedElements = expectedElements.subList(0, expectedElementsIndexOfLastCompleteElement + 1);
    		int lastFollowSetID = expectedElements.get(expectedElementsIndexOfLastCompleteElement).getFollowSetID();
    		java.util.Set<sare.language.acm.resource.acm.mopp.AcmExpectedTerminal> currentFollowSet = new java.util.LinkedHashSet<sare.language.acm.resource.acm.mopp.AcmExpectedTerminal>();
    		java.util.List<sare.language.acm.resource.acm.mopp.AcmExpectedTerminal> newFollowSet = new java.util.ArrayList<sare.language.acm.resource.acm.mopp.AcmExpectedTerminal>();
    		for (int i = expectedElementsIndexOfLastCompleteElement; i >= 0; i--) {
    			sare.language.acm.resource.acm.mopp.AcmExpectedTerminal expectedElementI = expectedElements.get(i);
    			if (expectedElementI.getFollowSetID() == lastFollowSetID) {
    				currentFollowSet.add(expectedElementI);
    			} else {
    				break;
    			}
    		}
    		int followSetID = 90;
    		int i;
    		for (i = tokenIndexOfLastCompleteElement; i < tokenStream.size(); i++) {
    			org.antlr.runtime3_3_0.CommonToken nextToken = (org.antlr.runtime3_3_0.CommonToken) tokenStream.get(i);
    			if (nextToken.getType() < 0) {
    				break;
    			}
    			if (nextToken.getChannel() == 99) {
    				// hidden tokens do not reduce the follow set
    			} else {
    				// now that we have found the next visible token the position for that expected
    				// terminals can be set
    				for (sare.language.acm.resource.acm.mopp.AcmExpectedTerminal nextFollow : newFollowSet) {
    					lastTokenIndex = 0;
    					setPosition(nextFollow, i);
    				}
    				newFollowSet.clear();
    				// normal tokens do reduce the follow set - only elements that match the token are
    				// kept
    				for (sare.language.acm.resource.acm.mopp.AcmExpectedTerminal nextFollow : currentFollowSet) {
    					if (nextFollow.getTerminal().getTokenNames().contains(getTokenNames()[nextToken.getType()])) {
    						// keep this one - it matches
    						java.util.Collection<sare.language.acm.resource.acm.util.AcmPair<sare.language.acm.resource.acm.IAcmExpectedElement, org.eclipse.emf.ecore.EStructuralFeature[]>> newFollowers = nextFollow.getTerminal().getFollowers();
    						for (sare.language.acm.resource.acm.util.AcmPair<sare.language.acm.resource.acm.IAcmExpectedElement, org.eclipse.emf.ecore.EStructuralFeature[]> newFollowerPair : newFollowers) {
    							sare.language.acm.resource.acm.IAcmExpectedElement newFollower = newFollowerPair.getLeft();
    							sare.language.acm.resource.acm.mopp.AcmExpectedTerminal newFollowTerminal = new sare.language.acm.resource.acm.mopp.AcmExpectedTerminal(newFollower, followSetID, newFollowerPair.getRight());
    							newFollowSet.add(newFollowTerminal);
    							expectedElements.add(newFollowTerminal);
    						}
    					}
    				}
    				currentFollowSet.clear();
    				currentFollowSet.addAll(newFollowSet);
    			}
    			followSetID++;
    		}
    		// after the last token in the stream we must set the position for the elements
    		// that were added during the last iteration of the loop
    		for (sare.language.acm.resource.acm.mopp.AcmExpectedTerminal nextFollow : newFollowSet) {
    			lastTokenIndex = 0;
    			setPosition(nextFollow, i);
    		}
    		return this.expectedElements;
    	}
    	
    	public void setPosition(sare.language.acm.resource.acm.mopp.AcmExpectedTerminal expectedElement, int tokenIndex) {
    		int currentIndex = Math.max(0, tokenIndex);
    		for (int index = lastTokenIndex; index < currentIndex; index++) {
    			if (index >= input.size()) {
    				break;
    			}
    			org.antlr.runtime3_3_0.CommonToken tokenAtIndex = (org.antlr.runtime3_3_0.CommonToken) input.get(index);
    			stopIncludingHiddenTokens = tokenAtIndex.getStopIndex() + 1;
    			if (tokenAtIndex.getChannel() != 99) {
    				stopExcludingHiddenTokens = tokenAtIndex.getStopIndex() + 1;
    			}
    		}
    		lastTokenIndex = Math.max(0, currentIndex);
    		expectedElement.setPosition(stopExcludingHiddenTokens, stopIncludingHiddenTokens);
    	}
    	
    	public Object recoverFromMismatchedToken(org.antlr.runtime3_3_0.IntStream input, int ttype, org.antlr.runtime3_3_0.BitSet follow) throws org.antlr.runtime3_3_0.RecognitionException {
    		if (!rememberExpectedElements) {
    			return super.recoverFromMismatchedToken(input, ttype, follow);
    		} else {
    			return null;
    		}
    	}
    	
    	/**
    	 * Translates errors thrown by the parser into human readable messages.
    	 */
    	public void reportError(final org.antlr.runtime3_3_0.RecognitionException e)  {
    		String message = e.getMessage();
    		if (e instanceof org.antlr.runtime3_3_0.MismatchedTokenException) {
    			org.antlr.runtime3_3_0.MismatchedTokenException mte = (org.antlr.runtime3_3_0.MismatchedTokenException) e;
    			String expectedTokenName = formatTokenName(mte.expecting);
    			String actualTokenName = formatTokenName(e.token.getType());
    			message = "Syntax error on token \"" + e.token.getText() + " (" + actualTokenName + ")\", \"" + expectedTokenName + "\" expected";
    		} else if (e instanceof org.antlr.runtime3_3_0.MismatchedTreeNodeException) {
    			org.antlr.runtime3_3_0.MismatchedTreeNodeException mtne = (org.antlr.runtime3_3_0.MismatchedTreeNodeException) e;
    			String expectedTokenName = formatTokenName(mtne.expecting);
    			message = "mismatched tree node: " + "xxx" + "; tokenName " + expectedTokenName;
    		} else if (e instanceof org.antlr.runtime3_3_0.NoViableAltException) {
    			message = "Syntax error on token \"" + e.token.getText() + "\", check following tokens";
    		} else if (e instanceof org.antlr.runtime3_3_0.EarlyExitException) {
    			message = "Syntax error on token \"" + e.token.getText() + "\", delete this token";
    		} else if (e instanceof org.antlr.runtime3_3_0.MismatchedSetException) {
    			org.antlr.runtime3_3_0.MismatchedSetException mse = (org.antlr.runtime3_3_0.MismatchedSetException) e;
    			message = "mismatched token: " + e.token + "; expecting set " + mse.expecting;
    		} else if (e instanceof org.antlr.runtime3_3_0.MismatchedNotSetException) {
    			org.antlr.runtime3_3_0.MismatchedNotSetException mse = (org.antlr.runtime3_3_0.MismatchedNotSetException) e;
    			message = "mismatched token: " +  e.token + "; expecting set " + mse.expecting;
    		} else if (e instanceof org.antlr.runtime3_3_0.FailedPredicateException) {
    			org.antlr.runtime3_3_0.FailedPredicateException fpe = (org.antlr.runtime3_3_0.FailedPredicateException) e;
    			message = "rule " + fpe.ruleName + " failed predicate: {" +  fpe.predicateText + "}?";
    		}
    		// the resource may be null if the parser is used for code completion
    		final String finalMessage = message;
    		if (e.token instanceof org.antlr.runtime3_3_0.CommonToken) {
    			final org.antlr.runtime3_3_0.CommonToken ct = (org.antlr.runtime3_3_0.CommonToken) e.token;
    			addErrorToResource(finalMessage, ct.getCharPositionInLine(), ct.getLine(), ct.getStartIndex(), ct.getStopIndex());
    		} else {
    			addErrorToResource(finalMessage, e.token.getCharPositionInLine(), e.token.getLine(), 1, 5);
    		}
    	}
    	
    	/**
    	 * Translates errors thrown by the lexer into human readable messages.
    	 */
    	public void reportLexicalError(final org.antlr.runtime3_3_0.RecognitionException e)  {
    		String message = "";
    		if (e instanceof org.antlr.runtime3_3_0.MismatchedTokenException) {
    			org.antlr.runtime3_3_0.MismatchedTokenException mte = (org.antlr.runtime3_3_0.MismatchedTokenException) e;
    			message = "Syntax error on token \"" + ((char) e.c) + "\", \"" + (char) mte.expecting + "\" expected";
    		} else if (e instanceof org.antlr.runtime3_3_0.NoViableAltException) {
    			message = "Syntax error on token \"" + ((char) e.c) + "\", delete this token";
    		} else if (e instanceof org.antlr.runtime3_3_0.EarlyExitException) {
    			org.antlr.runtime3_3_0.EarlyExitException eee = (org.antlr.runtime3_3_0.EarlyExitException) e;
    			message = "required (...)+ loop (decision=" + eee.decisionNumber + ") did not match anything; on line " + e.line + ":" + e.charPositionInLine + " char=" + ((char) e.c) + "'";
    		} else if (e instanceof org.antlr.runtime3_3_0.MismatchedSetException) {
    			org.antlr.runtime3_3_0.MismatchedSetException mse = (org.antlr.runtime3_3_0.MismatchedSetException) e;
    			message = "mismatched char: '" + ((char) e.c) + "' on line " + e.line + ":" + e.charPositionInLine + "; expecting set " + mse.expecting;
    		} else if (e instanceof org.antlr.runtime3_3_0.MismatchedNotSetException) {
    			org.antlr.runtime3_3_0.MismatchedNotSetException mse = (org.antlr.runtime3_3_0.MismatchedNotSetException) e;
    			message = "mismatched char: '" + ((char) e.c) + "' on line " + e.line + ":" + e.charPositionInLine + "; expecting set " + mse.expecting;
    		} else if (e instanceof org.antlr.runtime3_3_0.MismatchedRangeException) {
    			org.antlr.runtime3_3_0.MismatchedRangeException mre = (org.antlr.runtime3_3_0.MismatchedRangeException) e;
    			message = "mismatched char: '" + ((char) e.c) + "' on line " + e.line + ":" + e.charPositionInLine + "; expecting set '" + (char) mre.a + "'..'" + (char) mre.b + "'";
    		} else if (e instanceof org.antlr.runtime3_3_0.FailedPredicateException) {
    			org.antlr.runtime3_3_0.FailedPredicateException fpe = (org.antlr.runtime3_3_0.FailedPredicateException) e;
    			message = "rule " + fpe.ruleName + " failed predicate: {" + fpe.predicateText + "}?";
    		}
    		addErrorToResource(message, e.charPositionInLine, e.line, lexerExceptionsPosition.get(lexerExceptions.indexOf(e)), lexerExceptionsPosition.get(lexerExceptions.indexOf(e)));
    	}
    	
    	protected void completedElement(Object object, boolean isContainment) {
    		if (isContainment && !this.incompleteObjects.isEmpty()) {
    			this.incompleteObjects.pop();
    		}
    		if (object instanceof org.eclipse.emf.ecore.EObject) {
    			this.tokenIndexOfLastCompleteElement = getTokenStream().index();
    			this.expectedElementsIndexOfLastCompleteElement = expectedElements.size() - 1;
    		}
    	}
    	



    // $ANTLR start "start"
    // Acm.g:496:1: start returns [ org.eclipse.emf.ecore.EObject element = null] : (c0= parse_sare_language_acm_AccessControlModel ) EOF ;
    public final org.eclipse.emf.ecore.EObject start() throws RecognitionException {
        org.eclipse.emf.ecore.EObject element =  null;
        int start_StartIndex = input.index();
        sare.language.acm.AccessControlModel c0 = null;


        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 1) ) { return element; }
            // Acm.g:497:1: ( (c0= parse_sare_language_acm_AccessControlModel ) EOF )
            // Acm.g:498:2: (c0= parse_sare_language_acm_AccessControlModel ) EOF
            {
            if ( state.backtracking==0 ) {

              		// follow set for start rule(s)
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_0, 0);
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_1, 0);
              		expectedElementsIndexOfLastCompleteElement = 0;
              	
            }
            // Acm.g:504:2: (c0= parse_sare_language_acm_AccessControlModel )
            // Acm.g:505:3: c0= parse_sare_language_acm_AccessControlModel
            {
            pushFollow(FOLLOW_parse_sare_language_acm_AccessControlModel_in_start82);
            c0=parse_sare_language_acm_AccessControlModel();

            state._fsp--;
            if (state.failed) return element;
            if ( state.backtracking==0 ) {
               element = c0; 
            }

            }

            match(input,EOF,FOLLOW_EOF_in_start89); if (state.failed) return element;
            if ( state.backtracking==0 ) {

              		retrieveLayoutInformation(element, null, null, false);
              	
            }

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
            if ( state.backtracking>0 ) { memoize(input, 1, start_StartIndex); }
        }
        return element;
    }
    // $ANTLR end "start"


    // $ANTLR start "parse_sare_language_acm_AccessControlModel"
    // Acm.g:513:1: parse_sare_language_acm_AccessControlModel returns [sare.language.acm.AccessControlModel element = null] : ( (a0= 'import' (a1= QUOTED_34_34 ) ) )* a2= 'policy' (a3= TEXT ) a4= '{' ( (a5_0= parse_sare_language_acm_Subject ) )* a6= '}' ;
    public final sare.language.acm.AccessControlModel parse_sare_language_acm_AccessControlModel() throws RecognitionException {
        sare.language.acm.AccessControlModel element =  null;
        int parse_sare_language_acm_AccessControlModel_StartIndex = input.index();
        Token a0=null;
        Token a1=null;
        Token a2=null;
        Token a3=null;
        Token a4=null;
        Token a6=null;
        sare.language.acm.Subject a5_0 = null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 2) ) { return element; }
            // Acm.g:516:1: ( ( (a0= 'import' (a1= QUOTED_34_34 ) ) )* a2= 'policy' (a3= TEXT ) a4= '{' ( (a5_0= parse_sare_language_acm_Subject ) )* a6= '}' )
            // Acm.g:517:2: ( (a0= 'import' (a1= QUOTED_34_34 ) ) )* a2= 'policy' (a3= TEXT ) a4= '{' ( (a5_0= parse_sare_language_acm_Subject ) )* a6= '}'
            {
            // Acm.g:517:2: ( (a0= 'import' (a1= QUOTED_34_34 ) ) )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==9) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // Acm.g:518:3: (a0= 'import' (a1= QUOTED_34_34 ) )
            	    {
            	    // Acm.g:518:3: (a0= 'import' (a1= QUOTED_34_34 ) )
            	    // Acm.g:519:4: a0= 'import' (a1= QUOTED_34_34 )
            	    {
            	    a0=(Token)match(input,9,FOLLOW_9_in_parse_sare_language_acm_AccessControlModel124); if (state.failed) return element;
            	    if ( state.backtracking==0 ) {

            	      				if (element == null) {
            	      					element = sare.language.acm.AcmFactory.eINSTANCE.createAccessControlModel();
            	      					incompleteObjects.push(element);
            	      				}
            	      				collectHiddenTokens(element);
            	      				retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.ACM_0_0_0_0_0_0_0, null, true);
            	      				copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken)a0, element);
            	      			
            	    }
            	    if ( state.backtracking==0 ) {

            	      				// expected elements (follow set)
            	      				addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_2, 1);
            	      			
            	    }
            	    // Acm.g:533:4: (a1= QUOTED_34_34 )
            	    // Acm.g:534:5: a1= QUOTED_34_34
            	    {
            	    a1=(Token)match(input,QUOTED_34_34,FOLLOW_QUOTED_34_34_in_parse_sare_language_acm_AccessControlModel150); if (state.failed) return element;
            	    if ( state.backtracking==0 ) {

            	      					if (terminateParsing) {
            	      						throw new sare.language.acm.resource.acm.mopp.AcmTerminateParsingException();
            	      					}
            	      					if (element == null) {
            	      						element = sare.language.acm.AcmFactory.eINSTANCE.createAccessControlModel();
            	      						incompleteObjects.push(element);
            	      					}
            	      					if (a1 != null) {
            	      						sare.language.acm.resource.acm.IAcmTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("QUOTED_34_34");
            	      						tokenResolver.setOptions(getOptions());
            	      						sare.language.acm.resource.acm.IAcmTokenResolveResult result = getFreshTokenResolveResult();
            	      						tokenResolver.resolve(a1.getText(), element.eClass().getEStructuralFeature(sare.language.acm.AcmPackage.ACCESS_CONTROL_MODEL__IMPORTS), result);
            	      						Object resolvedObject = result.getResolvedToken();
            	      						if (resolvedObject == null) {
            	      							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_3_0.CommonToken) a1).getLine(), ((org.antlr.runtime3_3_0.CommonToken) a1).getCharPositionInLine(), ((org.antlr.runtime3_3_0.CommonToken) a1).getStartIndex(), ((org.antlr.runtime3_3_0.CommonToken) a1).getStopIndex());
            	      						}
            	      						String resolved = (String) resolvedObject;
            	      						org.eclipse.emf.ecore.EObject proxy = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEObject();
            	      						collectHiddenTokens(element);
            	      						registerContextDependentProxy(new sare.language.acm.resource.acm.mopp.AcmContextDependentURIFragmentFactory<sare.language.acm.AccessControlModel, org.eclipse.emf.ecore.EObject>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getAccessControlModelImportsReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(sare.language.acm.AcmPackage.ACCESS_CONTROL_MODEL__IMPORTS), resolved, proxy);
            	      						if (proxy != null) {
            	      							Object value = proxy;
            	      							addObjectToList(element, sare.language.acm.AcmPackage.ACCESS_CONTROL_MODEL__IMPORTS, value);
            	      							completedElement(value, false);
            	      						}
            	      						collectHiddenTokens(element);
            	      						retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.ACM_0_0_0_0_0_0_1, proxy, true);
            	      						copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken) a1, element);
            	      						copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken) a1, proxy);
            	      					}
            	      				
            	    }

            	    }

            	    if ( state.backtracking==0 ) {

            	      				// expected elements (follow set)
            	      				addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_0, 2);
            	      				addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_1, 2);
            	      			
            	    }

            	    }


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);

            if ( state.backtracking==0 ) {

              		// expected elements (follow set)
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_0, 3);
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_1, 3);
              	
            }
            a2=(Token)match(input,10,FOLLOW_10_in_parse_sare_language_acm_AccessControlModel196); if (state.failed) return element;
            if ( state.backtracking==0 ) {

              		if (element == null) {
              			element = sare.language.acm.AcmFactory.eINSTANCE.createAccessControlModel();
              			incompleteObjects.push(element);
              		}
              		collectHiddenTokens(element);
              		retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.ACM_0_0_0_1, null, true);
              		copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken)a2, element);
              	
            }
            if ( state.backtracking==0 ) {

              		// expected elements (follow set)
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_3, 4);
              	
            }
            // Acm.g:596:2: (a3= TEXT )
            // Acm.g:597:3: a3= TEXT
            {
            a3=(Token)match(input,TEXT,FOLLOW_TEXT_in_parse_sare_language_acm_AccessControlModel214); if (state.failed) return element;
            if ( state.backtracking==0 ) {

              			if (terminateParsing) {
              				throw new sare.language.acm.resource.acm.mopp.AcmTerminateParsingException();
              			}
              			if (element == null) {
              				element = sare.language.acm.AcmFactory.eINSTANCE.createAccessControlModel();
              				incompleteObjects.push(element);
              			}
              			if (a3 != null) {
              				sare.language.acm.resource.acm.IAcmTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
              				tokenResolver.setOptions(getOptions());
              				sare.language.acm.resource.acm.IAcmTokenResolveResult result = getFreshTokenResolveResult();
              				tokenResolver.resolve(a3.getText(), element.eClass().getEStructuralFeature(sare.language.acm.AcmPackage.ACCESS_CONTROL_MODEL__NAME), result);
              				Object resolvedObject = result.getResolvedToken();
              				if (resolvedObject == null) {
              					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_3_0.CommonToken) a3).getLine(), ((org.antlr.runtime3_3_0.CommonToken) a3).getCharPositionInLine(), ((org.antlr.runtime3_3_0.CommonToken) a3).getStartIndex(), ((org.antlr.runtime3_3_0.CommonToken) a3).getStopIndex());
              				}
              				java.lang.String resolved = (java.lang.String) resolvedObject;
              				if (resolved != null) {
              					Object value = resolved;
              					element.eSet(element.eClass().getEStructuralFeature(sare.language.acm.AcmPackage.ACCESS_CONTROL_MODEL__NAME), value);
              					completedElement(value, false);
              				}
              				collectHiddenTokens(element);
              				retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.ACM_0_0_0_2, resolved, true);
              				copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken) a3, element);
              			}
              		
            }

            }

            if ( state.backtracking==0 ) {

              		// expected elements (follow set)
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_4, 5);
              	
            }
            a4=(Token)match(input,11,FOLLOW_11_in_parse_sare_language_acm_AccessControlModel235); if (state.failed) return element;
            if ( state.backtracking==0 ) {

              		if (element == null) {
              			element = sare.language.acm.AcmFactory.eINSTANCE.createAccessControlModel();
              			incompleteObjects.push(element);
              		}
              		collectHiddenTokens(element);
              		retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.ACM_0_0_0_3, null, true);
              		copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken)a4, element);
              	
            }
            if ( state.backtracking==0 ) {

              		// expected elements (follow set)
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_5, 6, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_0);
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_6, 6, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_0);
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_7, 6);
              	
            }
            // Acm.g:648:2: ( (a5_0= parse_sare_language_acm_Subject ) )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0==13||LA2_0==16) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // Acm.g:649:3: (a5_0= parse_sare_language_acm_Subject )
            	    {
            	    // Acm.g:649:3: (a5_0= parse_sare_language_acm_Subject )
            	    // Acm.g:650:4: a5_0= parse_sare_language_acm_Subject
            	    {
            	    pushFollow(FOLLOW_parse_sare_language_acm_Subject_in_parse_sare_language_acm_AccessControlModel258);
            	    a5_0=parse_sare_language_acm_Subject();

            	    state._fsp--;
            	    if (state.failed) return element;
            	    if ( state.backtracking==0 ) {

            	      				if (terminateParsing) {
            	      					throw new sare.language.acm.resource.acm.mopp.AcmTerminateParsingException();
            	      				}
            	      				if (element == null) {
            	      					element = sare.language.acm.AcmFactory.eINSTANCE.createAccessControlModel();
            	      					incompleteObjects.push(element);
            	      				}
            	      				if (a5_0 != null) {
            	      					if (a5_0 != null) {
            	      						Object value = a5_0;
            	      						addObjectToList(element, sare.language.acm.AcmPackage.ACCESS_CONTROL_MODEL__ELEMENTS, value);
            	      						completedElement(value, true);
            	      					}
            	      					collectHiddenTokens(element);
            	      					retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.ACM_0_0_0_4, a5_0, true);
            	      					copyLocalizationInfos(a5_0, element);
            	      				}
            	      			
            	    }

            	    }


            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);

            if ( state.backtracking==0 ) {

              		// expected elements (follow set)
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_5, 7, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_0);
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_6, 7, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_0);
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_7, 7);
              	
            }
            a6=(Token)match(input,12,FOLLOW_12_in_parse_sare_language_acm_AccessControlModel284); if (state.failed) return element;
            if ( state.backtracking==0 ) {

              		if (element == null) {
              			element = sare.language.acm.AcmFactory.eINSTANCE.createAccessControlModel();
              			incompleteObjects.push(element);
              		}
              		collectHiddenTokens(element);
              		retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.ACM_0_0_0_5, null, true);
              		copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken)a6, element);
              	
            }
            if ( state.backtracking==0 ) {

              		// expected elements (follow set)
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_0, 8, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_1);
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_1, 8, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_1);
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_8, 8, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_1);
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_9, 8, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_1);
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_10, 8, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_1);
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_11, 8, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_2, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_1);
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_12, 8, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_2, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_1);
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_13, 8, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_1);
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_14, 8);
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_15, 8);
              	
            }

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
            if ( state.backtracking>0 ) { memoize(input, 2, parse_sare_language_acm_AccessControlModel_StartIndex); }
        }
        return element;
    }
    // $ANTLR end "parse_sare_language_acm_AccessControlModel"


    // $ANTLR start "parse_sare_language_acm_Role"
    // Acm.g:703:1: parse_sare_language_acm_Role returns [sare.language.acm.Role element = null] : a0= 'role' (a1= TEXT ) ( (a2= 'extends' (a3= TEXT ) ( (a4= ',' (a5= TEXT ) ) )* ) )? a6= '{' ( (a7_0= parse_sare_language_acm_Permission ) )* a8= '}' ;
    public final sare.language.acm.Role parse_sare_language_acm_Role() throws RecognitionException {
        sare.language.acm.Role element =  null;
        int parse_sare_language_acm_Role_StartIndex = input.index();
        Token a0=null;
        Token a1=null;
        Token a2=null;
        Token a3=null;
        Token a4=null;
        Token a5=null;
        Token a6=null;
        Token a8=null;
        sare.language.acm.Permission a7_0 = null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 3) ) { return element; }
            // Acm.g:706:1: (a0= 'role' (a1= TEXT ) ( (a2= 'extends' (a3= TEXT ) ( (a4= ',' (a5= TEXT ) ) )* ) )? a6= '{' ( (a7_0= parse_sare_language_acm_Permission ) )* a8= '}' )
            // Acm.g:707:2: a0= 'role' (a1= TEXT ) ( (a2= 'extends' (a3= TEXT ) ( (a4= ',' (a5= TEXT ) ) )* ) )? a6= '{' ( (a7_0= parse_sare_language_acm_Permission ) )* a8= '}'
            {
            a0=(Token)match(input,13,FOLLOW_13_in_parse_sare_language_acm_Role313); if (state.failed) return element;
            if ( state.backtracking==0 ) {

              		if (element == null) {
              			element = sare.language.acm.AcmFactory.eINSTANCE.createRole();
              			incompleteObjects.push(element);
              		}
              		collectHiddenTokens(element);
              		retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.ACM_1_0_0_0, null, true);
              		copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken)a0, element);
              	
            }
            if ( state.backtracking==0 ) {

              		// expected elements (follow set)
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_16, 9);
              	
            }
            // Acm.g:721:2: (a1= TEXT )
            // Acm.g:722:3: a1= TEXT
            {
            a1=(Token)match(input,TEXT,FOLLOW_TEXT_in_parse_sare_language_acm_Role331); if (state.failed) return element;
            if ( state.backtracking==0 ) {

              			if (terminateParsing) {
              				throw new sare.language.acm.resource.acm.mopp.AcmTerminateParsingException();
              			}
              			if (element == null) {
              				element = sare.language.acm.AcmFactory.eINSTANCE.createRole();
              				incompleteObjects.push(element);
              			}
              			if (a1 != null) {
              				sare.language.acm.resource.acm.IAcmTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
              				tokenResolver.setOptions(getOptions());
              				sare.language.acm.resource.acm.IAcmTokenResolveResult result = getFreshTokenResolveResult();
              				tokenResolver.resolve(a1.getText(), element.eClass().getEStructuralFeature(sare.language.acm.AcmPackage.ROLE__NAME), result);
              				Object resolvedObject = result.getResolvedToken();
              				if (resolvedObject == null) {
              					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_3_0.CommonToken) a1).getLine(), ((org.antlr.runtime3_3_0.CommonToken) a1).getCharPositionInLine(), ((org.antlr.runtime3_3_0.CommonToken) a1).getStartIndex(), ((org.antlr.runtime3_3_0.CommonToken) a1).getStopIndex());
              				}
              				java.lang.String resolved = (java.lang.String) resolvedObject;
              				if (resolved != null) {
              					Object value = resolved;
              					element.eSet(element.eClass().getEStructuralFeature(sare.language.acm.AcmPackage.ROLE__NAME), value);
              					completedElement(value, false);
              				}
              				collectHiddenTokens(element);
              				retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.ACM_1_0_0_1, resolved, true);
              				copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken) a1, element);
              			}
              		
            }

            }

            if ( state.backtracking==0 ) {

              		// expected elements (follow set)
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_17, 10);
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_18, 10);
              	
            }
            // Acm.g:758:2: ( (a2= 'extends' (a3= TEXT ) ( (a4= ',' (a5= TEXT ) ) )* ) )?
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==14) ) {
                alt4=1;
            }
            switch (alt4) {
                case 1 :
                    // Acm.g:759:3: (a2= 'extends' (a3= TEXT ) ( (a4= ',' (a5= TEXT ) ) )* )
                    {
                    // Acm.g:759:3: (a2= 'extends' (a3= TEXT ) ( (a4= ',' (a5= TEXT ) ) )* )
                    // Acm.g:760:4: a2= 'extends' (a3= TEXT ) ( (a4= ',' (a5= TEXT ) ) )*
                    {
                    a2=(Token)match(input,14,FOLLOW_14_in_parse_sare_language_acm_Role361); if (state.failed) return element;
                    if ( state.backtracking==0 ) {

                      				if (element == null) {
                      					element = sare.language.acm.AcmFactory.eINSTANCE.createRole();
                      					incompleteObjects.push(element);
                      				}
                      				collectHiddenTokens(element);
                      				retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.ACM_1_0_0_2_0_0_0, null, true);
                      				copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken)a2, element);
                      			
                    }
                    if ( state.backtracking==0 ) {

                      				// expected elements (follow set)
                      				addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_19, 11);
                      			
                    }
                    // Acm.g:774:4: (a3= TEXT )
                    // Acm.g:775:5: a3= TEXT
                    {
                    a3=(Token)match(input,TEXT,FOLLOW_TEXT_in_parse_sare_language_acm_Role387); if (state.failed) return element;
                    if ( state.backtracking==0 ) {

                      					if (terminateParsing) {
                      						throw new sare.language.acm.resource.acm.mopp.AcmTerminateParsingException();
                      					}
                      					if (element == null) {
                      						element = sare.language.acm.AcmFactory.eINSTANCE.createRole();
                      						incompleteObjects.push(element);
                      					}
                      					if (a3 != null) {
                      						sare.language.acm.resource.acm.IAcmTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
                      						tokenResolver.setOptions(getOptions());
                      						sare.language.acm.resource.acm.IAcmTokenResolveResult result = getFreshTokenResolveResult();
                      						tokenResolver.resolve(a3.getText(), element.eClass().getEStructuralFeature(sare.language.acm.AcmPackage.ROLE__PARENT_ROLES), result);
                      						Object resolvedObject = result.getResolvedToken();
                      						if (resolvedObject == null) {
                      							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_3_0.CommonToken) a3).getLine(), ((org.antlr.runtime3_3_0.CommonToken) a3).getCharPositionInLine(), ((org.antlr.runtime3_3_0.CommonToken) a3).getStartIndex(), ((org.antlr.runtime3_3_0.CommonToken) a3).getStopIndex());
                      						}
                      						String resolved = (String) resolvedObject;
                      						sare.language.acm.Role proxy = sare.language.acm.AcmFactory.eINSTANCE.createRole();
                      						collectHiddenTokens(element);
                      						registerContextDependentProxy(new sare.language.acm.resource.acm.mopp.AcmContextDependentURIFragmentFactory<sare.language.acm.Role, sare.language.acm.Role>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getRoleParentRolesReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(sare.language.acm.AcmPackage.ROLE__PARENT_ROLES), resolved, proxy);
                      						if (proxy != null) {
                      							Object value = proxy;
                      							addObjectToList(element, sare.language.acm.AcmPackage.ROLE__PARENT_ROLES, value);
                      							completedElement(value, false);
                      						}
                      						collectHiddenTokens(element);
                      						retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.ACM_1_0_0_2_0_0_1, proxy, true);
                      						copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken) a3, element);
                      						copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken) a3, proxy);
                      					}
                      				
                    }

                    }

                    if ( state.backtracking==0 ) {

                      				// expected elements (follow set)
                      				addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_20, 12);
                      				addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_18, 12);
                      			
                    }
                    // Acm.g:815:4: ( (a4= ',' (a5= TEXT ) ) )*
                    loop3:
                    do {
                        int alt3=2;
                        int LA3_0 = input.LA(1);

                        if ( (LA3_0==15) ) {
                            alt3=1;
                        }


                        switch (alt3) {
                    	case 1 :
                    	    // Acm.g:816:5: (a4= ',' (a5= TEXT ) )
                    	    {
                    	    // Acm.g:816:5: (a4= ',' (a5= TEXT ) )
                    	    // Acm.g:817:6: a4= ',' (a5= TEXT )
                    	    {
                    	    a4=(Token)match(input,15,FOLLOW_15_in_parse_sare_language_acm_Role433); if (state.failed) return element;
                    	    if ( state.backtracking==0 ) {

                    	      						if (element == null) {
                    	      							element = sare.language.acm.AcmFactory.eINSTANCE.createRole();
                    	      							incompleteObjects.push(element);
                    	      						}
                    	      						collectHiddenTokens(element);
                    	      						retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.ACM_1_0_0_2_0_0_2_0_0_0, null, true);
                    	      						copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken)a4, element);
                    	      					
                    	    }
                    	    if ( state.backtracking==0 ) {

                    	      						// expected elements (follow set)
                    	      						addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_21, 13);
                    	      					
                    	    }
                    	    // Acm.g:831:6: (a5= TEXT )
                    	    // Acm.g:832:7: a5= TEXT
                    	    {
                    	    a5=(Token)match(input,TEXT,FOLLOW_TEXT_in_parse_sare_language_acm_Role467); if (state.failed) return element;
                    	    if ( state.backtracking==0 ) {

                    	      							if (terminateParsing) {
                    	      								throw new sare.language.acm.resource.acm.mopp.AcmTerminateParsingException();
                    	      							}
                    	      							if (element == null) {
                    	      								element = sare.language.acm.AcmFactory.eINSTANCE.createRole();
                    	      								incompleteObjects.push(element);
                    	      							}
                    	      							if (a5 != null) {
                    	      								sare.language.acm.resource.acm.IAcmTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
                    	      								tokenResolver.setOptions(getOptions());
                    	      								sare.language.acm.resource.acm.IAcmTokenResolveResult result = getFreshTokenResolveResult();
                    	      								tokenResolver.resolve(a5.getText(), element.eClass().getEStructuralFeature(sare.language.acm.AcmPackage.ROLE__PARENT_ROLES), result);
                    	      								Object resolvedObject = result.getResolvedToken();
                    	      								if (resolvedObject == null) {
                    	      									addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_3_0.CommonToken) a5).getLine(), ((org.antlr.runtime3_3_0.CommonToken) a5).getCharPositionInLine(), ((org.antlr.runtime3_3_0.CommonToken) a5).getStartIndex(), ((org.antlr.runtime3_3_0.CommonToken) a5).getStopIndex());
                    	      								}
                    	      								String resolved = (String) resolvedObject;
                    	      								sare.language.acm.Role proxy = sare.language.acm.AcmFactory.eINSTANCE.createRole();
                    	      								collectHiddenTokens(element);
                    	      								registerContextDependentProxy(new sare.language.acm.resource.acm.mopp.AcmContextDependentURIFragmentFactory<sare.language.acm.Role, sare.language.acm.Role>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getRoleParentRolesReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(sare.language.acm.AcmPackage.ROLE__PARENT_ROLES), resolved, proxy);
                    	      								if (proxy != null) {
                    	      									Object value = proxy;
                    	      									addObjectToList(element, sare.language.acm.AcmPackage.ROLE__PARENT_ROLES, value);
                    	      									completedElement(value, false);
                    	      								}
                    	      								collectHiddenTokens(element);
                    	      								retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.ACM_1_0_0_2_0_0_2_0_0_1, proxy, true);
                    	      								copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken) a5, element);
                    	      								copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken) a5, proxy);
                    	      							}
                    	      						
                    	    }

                    	    }

                    	    if ( state.backtracking==0 ) {

                    	      						// expected elements (follow set)
                    	      						addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_20, 14);
                    	      						addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_18, 14);
                    	      					
                    	    }

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop3;
                        }
                    } while (true);

                    if ( state.backtracking==0 ) {

                      				// expected elements (follow set)
                      				addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_20, 15);
                      				addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_18, 15);
                      			
                    }

                    }


                    }
                    break;

            }

            if ( state.backtracking==0 ) {

              		// expected elements (follow set)
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_18, 16);
              	
            }
            a6=(Token)match(input,11,FOLLOW_11_in_parse_sare_language_acm_Role548); if (state.failed) return element;
            if ( state.backtracking==0 ) {

              		if (element == null) {
              			element = sare.language.acm.AcmFactory.eINSTANCE.createRole();
              			incompleteObjects.push(element);
              		}
              		collectHiddenTokens(element);
              		retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.ACM_1_0_0_3, null, true);
              		copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken)a6, element);
              	
            }
            if ( state.backtracking==0 ) {

              		// expected elements (follow set)
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_22, 17, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_3);
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_23, 17, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_3);
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_24, 17);
              	
            }
            // Acm.g:903:2: ( (a7_0= parse_sare_language_acm_Permission ) )*
            loop5:
            do {
                int alt5=2;
                int LA5_0 = input.LA(1);

                if ( ((LA5_0>=TEXT && LA5_0<=NAMED_ELEMENT_ID)) ) {
                    alt5=1;
                }


                switch (alt5) {
            	case 1 :
            	    // Acm.g:904:3: (a7_0= parse_sare_language_acm_Permission )
            	    {
            	    // Acm.g:904:3: (a7_0= parse_sare_language_acm_Permission )
            	    // Acm.g:905:4: a7_0= parse_sare_language_acm_Permission
            	    {
            	    pushFollow(FOLLOW_parse_sare_language_acm_Permission_in_parse_sare_language_acm_Role571);
            	    a7_0=parse_sare_language_acm_Permission();

            	    state._fsp--;
            	    if (state.failed) return element;
            	    if ( state.backtracking==0 ) {

            	      				if (terminateParsing) {
            	      					throw new sare.language.acm.resource.acm.mopp.AcmTerminateParsingException();
            	      				}
            	      				if (element == null) {
            	      					element = sare.language.acm.AcmFactory.eINSTANCE.createRole();
            	      					incompleteObjects.push(element);
            	      				}
            	      				if (a7_0 != null) {
            	      					if (a7_0 != null) {
            	      						Object value = a7_0;
            	      						addObjectToList(element, sare.language.acm.AcmPackage.ROLE__PERMISSIONS, value);
            	      						completedElement(value, true);
            	      					}
            	      					collectHiddenTokens(element);
            	      					retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.ACM_1_0_0_4, a7_0, true);
            	      					copyLocalizationInfos(a7_0, element);
            	      				}
            	      			
            	    }

            	    }


            	    }
            	    break;

            	default :
            	    break loop5;
                }
            } while (true);

            if ( state.backtracking==0 ) {

              		// expected elements (follow set)
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_22, 18, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_3);
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_23, 18, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_3);
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_24, 18);
              	
            }
            a8=(Token)match(input,12,FOLLOW_12_in_parse_sare_language_acm_Role597); if (state.failed) return element;
            if ( state.backtracking==0 ) {

              		if (element == null) {
              			element = sare.language.acm.AcmFactory.eINSTANCE.createRole();
              			incompleteObjects.push(element);
              		}
              		collectHiddenTokens(element);
              		retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.ACM_1_0_0_5, null, true);
              		copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken)a8, element);
              	
            }
            if ( state.backtracking==0 ) {

              		// expected elements (follow set)
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_5, 19, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_0);
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_6, 19, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_0);
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_7, 19);
              	
            }

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
            if ( state.backtracking>0 ) { memoize(input, 3, parse_sare_language_acm_Role_StartIndex); }
        }
        return element;
    }
    // $ANTLR end "parse_sare_language_acm_Role"


    // $ANTLR start "parse_sare_language_acm_User"
    // Acm.g:951:1: parse_sare_language_acm_User returns [sare.language.acm.User element = null] : a0= 'user' (a1= TEXT ) a2= 'hasRoles' ( (a3= ',' (a4= TEXT ) ) )* ;
    public final sare.language.acm.User parse_sare_language_acm_User() throws RecognitionException {
        sare.language.acm.User element =  null;
        int parse_sare_language_acm_User_StartIndex = input.index();
        Token a0=null;
        Token a1=null;
        Token a2=null;
        Token a3=null;
        Token a4=null;



        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 4) ) { return element; }
            // Acm.g:954:1: (a0= 'user' (a1= TEXT ) a2= 'hasRoles' ( (a3= ',' (a4= TEXT ) ) )* )
            // Acm.g:955:2: a0= 'user' (a1= TEXT ) a2= 'hasRoles' ( (a3= ',' (a4= TEXT ) ) )*
            {
            a0=(Token)match(input,16,FOLLOW_16_in_parse_sare_language_acm_User626); if (state.failed) return element;
            if ( state.backtracking==0 ) {

              		if (element == null) {
              			element = sare.language.acm.AcmFactory.eINSTANCE.createUser();
              			incompleteObjects.push(element);
              		}
              		collectHiddenTokens(element);
              		retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.ACM_2_0_0_0, null, true);
              		copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken)a0, element);
              	
            }
            if ( state.backtracking==0 ) {

              		// expected elements (follow set)
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_25, 20);
              	
            }
            // Acm.g:969:2: (a1= TEXT )
            // Acm.g:970:3: a1= TEXT
            {
            a1=(Token)match(input,TEXT,FOLLOW_TEXT_in_parse_sare_language_acm_User644); if (state.failed) return element;
            if ( state.backtracking==0 ) {

              			if (terminateParsing) {
              				throw new sare.language.acm.resource.acm.mopp.AcmTerminateParsingException();
              			}
              			if (element == null) {
              				element = sare.language.acm.AcmFactory.eINSTANCE.createUser();
              				incompleteObjects.push(element);
              			}
              			if (a1 != null) {
              				sare.language.acm.resource.acm.IAcmTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
              				tokenResolver.setOptions(getOptions());
              				sare.language.acm.resource.acm.IAcmTokenResolveResult result = getFreshTokenResolveResult();
              				tokenResolver.resolve(a1.getText(), element.eClass().getEStructuralFeature(sare.language.acm.AcmPackage.USER__NAME), result);
              				Object resolvedObject = result.getResolvedToken();
              				if (resolvedObject == null) {
              					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_3_0.CommonToken) a1).getLine(), ((org.antlr.runtime3_3_0.CommonToken) a1).getCharPositionInLine(), ((org.antlr.runtime3_3_0.CommonToken) a1).getStartIndex(), ((org.antlr.runtime3_3_0.CommonToken) a1).getStopIndex());
              				}
              				java.lang.String resolved = (java.lang.String) resolvedObject;
              				if (resolved != null) {
              					Object value = resolved;
              					element.eSet(element.eClass().getEStructuralFeature(sare.language.acm.AcmPackage.USER__NAME), value);
              					completedElement(value, false);
              				}
              				collectHiddenTokens(element);
              				retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.ACM_2_0_0_1, resolved, true);
              				copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken) a1, element);
              			}
              		
            }

            }

            if ( state.backtracking==0 ) {

              		// expected elements (follow set)
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_26, 21);
              	
            }
            a2=(Token)match(input,17,FOLLOW_17_in_parse_sare_language_acm_User665); if (state.failed) return element;
            if ( state.backtracking==0 ) {

              		if (element == null) {
              			element = sare.language.acm.AcmFactory.eINSTANCE.createUser();
              			incompleteObjects.push(element);
              		}
              		collectHiddenTokens(element);
              		retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.ACM_2_0_0_2, null, true);
              		copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken)a2, element);
              	
            }
            if ( state.backtracking==0 ) {

              		// expected elements (follow set)
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_27, 22);
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_5, 22, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_0);
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_6, 22, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_0);
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_7, 22);
              	
            }
            // Acm.g:1022:2: ( (a3= ',' (a4= TEXT ) ) )*
            loop6:
            do {
                int alt6=2;
                int LA6_0 = input.LA(1);

                if ( (LA6_0==15) ) {
                    alt6=1;
                }


                switch (alt6) {
            	case 1 :
            	    // Acm.g:1023:3: (a3= ',' (a4= TEXT ) )
            	    {
            	    // Acm.g:1023:3: (a3= ',' (a4= TEXT ) )
            	    // Acm.g:1024:4: a3= ',' (a4= TEXT )
            	    {
            	    a3=(Token)match(input,15,FOLLOW_15_in_parse_sare_language_acm_User688); if (state.failed) return element;
            	    if ( state.backtracking==0 ) {

            	      				if (element == null) {
            	      					element = sare.language.acm.AcmFactory.eINSTANCE.createUser();
            	      					incompleteObjects.push(element);
            	      				}
            	      				collectHiddenTokens(element);
            	      				retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.ACM_2_0_0_3_0_0_0, null, true);
            	      				copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken)a3, element);
            	      			
            	    }
            	    if ( state.backtracking==0 ) {

            	      				// expected elements (follow set)
            	      				addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_28, 23);
            	      			
            	    }
            	    // Acm.g:1038:4: (a4= TEXT )
            	    // Acm.g:1039:5: a4= TEXT
            	    {
            	    a4=(Token)match(input,TEXT,FOLLOW_TEXT_in_parse_sare_language_acm_User714); if (state.failed) return element;
            	    if ( state.backtracking==0 ) {

            	      					if (terminateParsing) {
            	      						throw new sare.language.acm.resource.acm.mopp.AcmTerminateParsingException();
            	      					}
            	      					if (element == null) {
            	      						element = sare.language.acm.AcmFactory.eINSTANCE.createUser();
            	      						incompleteObjects.push(element);
            	      					}
            	      					if (a4 != null) {
            	      						sare.language.acm.resource.acm.IAcmTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
            	      						tokenResolver.setOptions(getOptions());
            	      						sare.language.acm.resource.acm.IAcmTokenResolveResult result = getFreshTokenResolveResult();
            	      						tokenResolver.resolve(a4.getText(), element.eClass().getEStructuralFeature(sare.language.acm.AcmPackage.USER__ROLES), result);
            	      						Object resolvedObject = result.getResolvedToken();
            	      						if (resolvedObject == null) {
            	      							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_3_0.CommonToken) a4).getLine(), ((org.antlr.runtime3_3_0.CommonToken) a4).getCharPositionInLine(), ((org.antlr.runtime3_3_0.CommonToken) a4).getStartIndex(), ((org.antlr.runtime3_3_0.CommonToken) a4).getStopIndex());
            	      						}
            	      						String resolved = (String) resolvedObject;
            	      						sare.language.acm.Role proxy = sare.language.acm.AcmFactory.eINSTANCE.createRole();
            	      						collectHiddenTokens(element);
            	      						registerContextDependentProxy(new sare.language.acm.resource.acm.mopp.AcmContextDependentURIFragmentFactory<sare.language.acm.User, sare.language.acm.Role>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getUserRolesReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(sare.language.acm.AcmPackage.USER__ROLES), resolved, proxy);
            	      						if (proxy != null) {
            	      							Object value = proxy;
            	      							addObjectToList(element, sare.language.acm.AcmPackage.USER__ROLES, value);
            	      							completedElement(value, false);
            	      						}
            	      						collectHiddenTokens(element);
            	      						retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.ACM_2_0_0_3_0_0_1, proxy, true);
            	      						copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken) a4, element);
            	      						copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken) a4, proxy);
            	      					}
            	      				
            	    }

            	    }

            	    if ( state.backtracking==0 ) {

            	      				// expected elements (follow set)
            	      				addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_27, 24);
            	      				addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_5, 24, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_0);
            	      				addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_6, 24, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_0);
            	      				addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_7, 24);
            	      			
            	    }

            	    }


            	    }
            	    break;

            	default :
            	    break loop6;
                }
            } while (true);

            if ( state.backtracking==0 ) {

              		// expected elements (follow set)
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_27, 25);
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_5, 25, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_0);
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_6, 25, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_0);
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_7, 25);
              	
            }

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
            if ( state.backtracking>0 ) { memoize(input, 4, parse_sare_language_acm_User_StartIndex); }
        }
        return element;
    }
    // $ANTLR end "parse_sare_language_acm_User"


    // $ANTLR start "parse_sare_language_acm_Permission"
    // Acm.g:1093:1: parse_sare_language_acm_Permission returns [sare.language.acm.Permission element = null] : ( (a0= NAMED_ELEMENT_ID ) | (a1= TEXT ) ) ( (a2= 'if' (a3= TEXT ) ) )? ( (a4= '{' ( (a5_0= parse_sare_language_acm_Permission ) )* a6= '}' ) )? ;
    public final sare.language.acm.Permission parse_sare_language_acm_Permission() throws RecognitionException {
        sare.language.acm.Permission element =  null;
        int parse_sare_language_acm_Permission_StartIndex = input.index();
        Token a0=null;
        Token a1=null;
        Token a2=null;
        Token a3=null;
        Token a4=null;
        Token a6=null;
        sare.language.acm.Permission a5_0 = null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 5) ) { return element; }
            // Acm.g:1096:1: ( ( (a0= NAMED_ELEMENT_ID ) | (a1= TEXT ) ) ( (a2= 'if' (a3= TEXT ) ) )? ( (a4= '{' ( (a5_0= parse_sare_language_acm_Permission ) )* a6= '}' ) )? )
            // Acm.g:1097:2: ( (a0= NAMED_ELEMENT_ID ) | (a1= TEXT ) ) ( (a2= 'if' (a3= TEXT ) ) )? ( (a4= '{' ( (a5_0= parse_sare_language_acm_Permission ) )* a6= '}' ) )?
            {
            // Acm.g:1097:2: ( (a0= NAMED_ELEMENT_ID ) | (a1= TEXT ) )
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0==NAMED_ELEMENT_ID) ) {
                alt7=1;
            }
            else if ( (LA7_0==TEXT) ) {
                alt7=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return element;}
                NoViableAltException nvae =
                    new NoViableAltException("", 7, 0, input);

                throw nvae;
            }
            switch (alt7) {
                case 1 :
                    // Acm.g:1098:3: (a0= NAMED_ELEMENT_ID )
                    {
                    // Acm.g:1098:3: (a0= NAMED_ELEMENT_ID )
                    // Acm.g:1099:4: a0= NAMED_ELEMENT_ID
                    {
                    a0=(Token)match(input,NAMED_ELEMENT_ID,FOLLOW_NAMED_ELEMENT_ID_in_parse_sare_language_acm_Permission784); if (state.failed) return element;
                    if ( state.backtracking==0 ) {

                      				if (terminateParsing) {
                      					throw new sare.language.acm.resource.acm.mopp.AcmTerminateParsingException();
                      				}
                      				if (element == null) {
                      					element = sare.language.acm.AcmFactory.eINSTANCE.createPermission();
                      					incompleteObjects.push(element);
                      				}
                      				if (a0 != null) {
                      					sare.language.acm.resource.acm.IAcmTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("NAMED_ELEMENT_ID");
                      					tokenResolver.setOptions(getOptions());
                      					sare.language.acm.resource.acm.IAcmTokenResolveResult result = getFreshTokenResolveResult();
                      					tokenResolver.resolve(a0.getText(), element.eClass().getEStructuralFeature(sare.language.acm.AcmPackage.PERMISSION__ELEMENT), result);
                      					Object resolvedObject = result.getResolvedToken();
                      					if (resolvedObject == null) {
                      						addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_3_0.CommonToken) a0).getLine(), ((org.antlr.runtime3_3_0.CommonToken) a0).getCharPositionInLine(), ((org.antlr.runtime3_3_0.CommonToken) a0).getStartIndex(), ((org.antlr.runtime3_3_0.CommonToken) a0).getStopIndex());
                      					}
                      					String resolved = (String) resolvedObject;
                      					org.eclipse.uml2.uml.NamedElement proxy = sare.language.acm.AcmFactory.eINSTANCE.createAccessControlModel();
                      					collectHiddenTokens(element);
                      					registerContextDependentProxy(new sare.language.acm.resource.acm.mopp.AcmContextDependentURIFragmentFactory<sare.language.acm.Permission, org.eclipse.uml2.uml.NamedElement>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getPermissionElementReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(sare.language.acm.AcmPackage.PERMISSION__ELEMENT), resolved, proxy);
                      					if (proxy != null) {
                      						Object value = proxy;
                      						element.eSet(element.eClass().getEStructuralFeature(sare.language.acm.AcmPackage.PERMISSION__ELEMENT), value);
                      						completedElement(value, false);
                      					}
                      					collectHiddenTokens(element);
                      					retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.ACM_3_0_0_0_0_0_0, proxy, true);
                      					copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken) a0, element);
                      					copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken) a0, proxy);
                      				}
                      			
                    }

                    }

                    if ( state.backtracking==0 ) {

                      			// expected elements (follow set)
                      			addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_29, 26);
                      			addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_30, 26);
                      			addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_22, 26, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_3);
                      			addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_23, 26, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_3);
                      			addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_24, 26);
                      			addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_31, 26);
                      		
                    }

                    }
                    break;
                case 2 :
                    // Acm.g:1144:6: (a1= TEXT )
                    {
                    // Acm.g:1144:6: (a1= TEXT )
                    // Acm.g:1145:4: a1= TEXT
                    {
                    a1=(Token)match(input,TEXT,FOLLOW_TEXT_in_parse_sare_language_acm_Permission822); if (state.failed) return element;
                    if ( state.backtracking==0 ) {

                      				if (terminateParsing) {
                      					throw new sare.language.acm.resource.acm.mopp.AcmTerminateParsingException();
                      				}
                      				if (element == null) {
                      					element = sare.language.acm.AcmFactory.eINSTANCE.createPermission();
                      					incompleteObjects.push(element);
                      				}
                      				if (a1 != null) {
                      					sare.language.acm.resource.acm.IAcmTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
                      					tokenResolver.setOptions(getOptions());
                      					sare.language.acm.resource.acm.IAcmTokenResolveResult result = getFreshTokenResolveResult();
                      					tokenResolver.resolve(a1.getText(), element.eClass().getEStructuralFeature(sare.language.acm.AcmPackage.PERMISSION__ELEMENT), result);
                      					Object resolvedObject = result.getResolvedToken();
                      					if (resolvedObject == null) {
                      						addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_3_0.CommonToken) a1).getLine(), ((org.antlr.runtime3_3_0.CommonToken) a1).getCharPositionInLine(), ((org.antlr.runtime3_3_0.CommonToken) a1).getStartIndex(), ((org.antlr.runtime3_3_0.CommonToken) a1).getStopIndex());
                      					}
                      					String resolved = (String) resolvedObject;
                      					org.eclipse.uml2.uml.NamedElement proxy = sare.language.acm.AcmFactory.eINSTANCE.createAccessControlModel();
                      					collectHiddenTokens(element);
                      					registerContextDependentProxy(new sare.language.acm.resource.acm.mopp.AcmContextDependentURIFragmentFactory<sare.language.acm.Permission, org.eclipse.uml2.uml.NamedElement>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getPermissionElementReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(sare.language.acm.AcmPackage.PERMISSION__ELEMENT), resolved, proxy);
                      					if (proxy != null) {
                      						Object value = proxy;
                      						element.eSet(element.eClass().getEStructuralFeature(sare.language.acm.AcmPackage.PERMISSION__ELEMENT), value);
                      						completedElement(value, false);
                      					}
                      					collectHiddenTokens(element);
                      					retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.ACM_3_0_0_0_0_1_0, proxy, true);
                      					copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken) a1, element);
                      					copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken) a1, proxy);
                      				}
                      			
                    }

                    }

                    if ( state.backtracking==0 ) {

                      			// expected elements (follow set)
                      			addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_29, 27);
                      			addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_30, 27);
                      			addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_22, 27, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_3);
                      			addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_23, 27, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_3);
                      			addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_24, 27);
                      			addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_31, 27);
                      		
                    }

                    }
                    break;

            }

            if ( state.backtracking==0 ) {

              		// expected elements (follow set)
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_29, 28);
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_30, 28);
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_22, 28, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_3);
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_23, 28, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_3);
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_24, 28);
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_31, 28);
              	
            }
            // Acm.g:1200:2: ( (a2= 'if' (a3= TEXT ) ) )?
            int alt8=2;
            int LA8_0 = input.LA(1);

            if ( (LA8_0==18) ) {
                alt8=1;
            }
            switch (alt8) {
                case 1 :
                    // Acm.g:1201:3: (a2= 'if' (a3= TEXT ) )
                    {
                    // Acm.g:1201:3: (a2= 'if' (a3= TEXT ) )
                    // Acm.g:1202:4: a2= 'if' (a3= TEXT )
                    {
                    a2=(Token)match(input,18,FOLLOW_18_in_parse_sare_language_acm_Permission865); if (state.failed) return element;
                    if ( state.backtracking==0 ) {

                      				if (element == null) {
                      					element = sare.language.acm.AcmFactory.eINSTANCE.createPermission();
                      					incompleteObjects.push(element);
                      				}
                      				collectHiddenTokens(element);
                      				retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.ACM_3_0_0_1_0_0_0, null, true);
                      				copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken)a2, element);
                      			
                    }
                    if ( state.backtracking==0 ) {

                      				// expected elements (follow set)
                      				addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_32, 29);
                      			
                    }
                    // Acm.g:1216:4: (a3= TEXT )
                    // Acm.g:1217:5: a3= TEXT
                    {
                    a3=(Token)match(input,TEXT,FOLLOW_TEXT_in_parse_sare_language_acm_Permission891); if (state.failed) return element;
                    if ( state.backtracking==0 ) {

                      					if (terminateParsing) {
                      						throw new sare.language.acm.resource.acm.mopp.AcmTerminateParsingException();
                      					}
                      					if (element == null) {
                      						element = sare.language.acm.AcmFactory.eINSTANCE.createPermission();
                      						incompleteObjects.push(element);
                      					}
                      					if (a3 != null) {
                      						sare.language.acm.resource.acm.IAcmTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
                      						tokenResolver.setOptions(getOptions());
                      						sare.language.acm.resource.acm.IAcmTokenResolveResult result = getFreshTokenResolveResult();
                      						tokenResolver.resolve(a3.getText(), element.eClass().getEStructuralFeature(sare.language.acm.AcmPackage.PERMISSION__CONSTRAINT), result);
                      						Object resolvedObject = result.getResolvedToken();
                      						if (resolvedObject == null) {
                      							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_3_0.CommonToken) a3).getLine(), ((org.antlr.runtime3_3_0.CommonToken) a3).getCharPositionInLine(), ((org.antlr.runtime3_3_0.CommonToken) a3).getStartIndex(), ((org.antlr.runtime3_3_0.CommonToken) a3).getStopIndex());
                      						}
                      						java.lang.String resolved = (java.lang.String) resolvedObject;
                      						if (resolved != null) {
                      							Object value = resolved;
                      							element.eSet(element.eClass().getEStructuralFeature(sare.language.acm.AcmPackage.PERMISSION__CONSTRAINT), value);
                      							completedElement(value, false);
                      						}
                      						collectHiddenTokens(element);
                      						retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.ACM_3_0_0_1_0_0_1, resolved, true);
                      						copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken) a3, element);
                      					}
                      				
                    }

                    }

                    if ( state.backtracking==0 ) {

                      				// expected elements (follow set)
                      				addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_30, 30);
                      				addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_22, 30, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_3);
                      				addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_23, 30, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_3);
                      				addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_24, 30);
                      				addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_31, 30);
                      			
                    }

                    }


                    }
                    break;

            }

            if ( state.backtracking==0 ) {

              		// expected elements (follow set)
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_30, 31);
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_22, 31, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_3);
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_23, 31, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_3);
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_24, 31);
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_31, 31);
              	
            }
            // Acm.g:1267:2: ( (a4= '{' ( (a5_0= parse_sare_language_acm_Permission ) )* a6= '}' ) )?
            int alt10=2;
            int LA10_0 = input.LA(1);

            if ( (LA10_0==11) ) {
                alt10=1;
            }
            switch (alt10) {
                case 1 :
                    // Acm.g:1268:3: (a4= '{' ( (a5_0= parse_sare_language_acm_Permission ) )* a6= '}' )
                    {
                    // Acm.g:1268:3: (a4= '{' ( (a5_0= parse_sare_language_acm_Permission ) )* a6= '}' )
                    // Acm.g:1269:4: a4= '{' ( (a5_0= parse_sare_language_acm_Permission ) )* a6= '}'
                    {
                    a4=(Token)match(input,11,FOLLOW_11_in_parse_sare_language_acm_Permission946); if (state.failed) return element;
                    if ( state.backtracking==0 ) {

                      				if (element == null) {
                      					element = sare.language.acm.AcmFactory.eINSTANCE.createPermission();
                      					incompleteObjects.push(element);
                      				}
                      				collectHiddenTokens(element);
                      				retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.ACM_3_0_0_2_0_0_0, null, true);
                      				copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken)a4, element);
                      			
                    }
                    if ( state.backtracking==0 ) {

                      				// expected elements (follow set)
                      				addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_22, 32, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_4);
                      				addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_23, 32, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_4);
                      				addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_31, 32);
                      			
                    }
                    // Acm.g:1285:4: ( (a5_0= parse_sare_language_acm_Permission ) )*
                    loop9:
                    do {
                        int alt9=2;
                        int LA9_0 = input.LA(1);

                        if ( ((LA9_0>=TEXT && LA9_0<=NAMED_ELEMENT_ID)) ) {
                            alt9=1;
                        }


                        switch (alt9) {
                    	case 1 :
                    	    // Acm.g:1286:5: (a5_0= parse_sare_language_acm_Permission )
                    	    {
                    	    // Acm.g:1286:5: (a5_0= parse_sare_language_acm_Permission )
                    	    // Acm.g:1287:6: a5_0= parse_sare_language_acm_Permission
                    	    {
                    	    pushFollow(FOLLOW_parse_sare_language_acm_Permission_in_parse_sare_language_acm_Permission979);
                    	    a5_0=parse_sare_language_acm_Permission();

                    	    state._fsp--;
                    	    if (state.failed) return element;
                    	    if ( state.backtracking==0 ) {

                    	      						if (terminateParsing) {
                    	      							throw new sare.language.acm.resource.acm.mopp.AcmTerminateParsingException();
                    	      						}
                    	      						if (element == null) {
                    	      							element = sare.language.acm.AcmFactory.eINSTANCE.createPermission();
                    	      							incompleteObjects.push(element);
                    	      						}
                    	      						if (a5_0 != null) {
                    	      							if (a5_0 != null) {
                    	      								Object value = a5_0;
                    	      								addObjectToList(element, sare.language.acm.AcmPackage.PERMISSION__CHILD_PERMISSIONS, value);
                    	      								completedElement(value, true);
                    	      							}
                    	      							collectHiddenTokens(element);
                    	      							retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.ACM_3_0_0_2_0_0_1, a5_0, true);
                    	      							copyLocalizationInfos(a5_0, element);
                    	      						}
                    	      					
                    	    }

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop9;
                        }
                    } while (true);

                    if ( state.backtracking==0 ) {

                      				// expected elements (follow set)
                      				addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_22, 33, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_4);
                      				addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_23, 33, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_4);
                      				addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_31, 33);
                      			
                    }
                    a6=(Token)match(input,12,FOLLOW_12_in_parse_sare_language_acm_Permission1019); if (state.failed) return element;
                    if ( state.backtracking==0 ) {

                      				if (element == null) {
                      					element = sare.language.acm.AcmFactory.eINSTANCE.createPermission();
                      					incompleteObjects.push(element);
                      				}
                      				collectHiddenTokens(element);
                      				retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.ACM_3_0_0_2_0_0_2, null, true);
                      				copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken)a6, element);
                      			
                    }
                    if ( state.backtracking==0 ) {

                      				// expected elements (follow set)
                      				addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_22, 34, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_3);
                      				addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_23, 34, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_3);
                      				addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_24, 34);
                      				addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_31, 34);
                      			
                    }

                    }


                    }
                    break;

            }

            if ( state.backtracking==0 ) {

              		// expected elements (follow set)
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_22, 35, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_3);
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_23, 35, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_3);
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_24, 35);
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_31, 35);
              	
            }

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
            if ( state.backtracking>0 ) { memoize(input, 5, parse_sare_language_acm_Permission_StartIndex); }
        }
        return element;
    }
    // $ANTLR end "parse_sare_language_acm_Permission"


    // $ANTLR start "parse_org_eclipse_uml2_uml_Model"
    // Acm.g:1344:1: parse_org_eclipse_uml2_uml_Model returns [org.eclipse.uml2.uml.Model element = null] : ( (a0= 'import' (a1_0= parse_org_eclipse_uml2_uml_PackageImport ) ) )* a2= 'model' (a3= TEXT ) a4= '{' ( (a5_0= parse_org_eclipse_uml2_uml_PackageableElement ) )* a6= '}' ;
    public final org.eclipse.uml2.uml.Model parse_org_eclipse_uml2_uml_Model() throws RecognitionException {
        org.eclipse.uml2.uml.Model element =  null;
        int parse_org_eclipse_uml2_uml_Model_StartIndex = input.index();
        Token a0=null;
        Token a2=null;
        Token a3=null;
        Token a4=null;
        Token a6=null;
        org.eclipse.uml2.uml.PackageImport a1_0 = null;

        org.eclipse.uml2.uml.PackageableElement a5_0 = null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 6) ) { return element; }
            // Acm.g:1347:1: ( ( (a0= 'import' (a1_0= parse_org_eclipse_uml2_uml_PackageImport ) ) )* a2= 'model' (a3= TEXT ) a4= '{' ( (a5_0= parse_org_eclipse_uml2_uml_PackageableElement ) )* a6= '}' )
            // Acm.g:1348:2: ( (a0= 'import' (a1_0= parse_org_eclipse_uml2_uml_PackageImport ) ) )* a2= 'model' (a3= TEXT ) a4= '{' ( (a5_0= parse_org_eclipse_uml2_uml_PackageableElement ) )* a6= '}'
            {
            // Acm.g:1348:2: ( (a0= 'import' (a1_0= parse_org_eclipse_uml2_uml_PackageImport ) ) )*
            loop11:
            do {
                int alt11=2;
                int LA11_0 = input.LA(1);

                if ( (LA11_0==9) ) {
                    alt11=1;
                }


                switch (alt11) {
            	case 1 :
            	    // Acm.g:1349:3: (a0= 'import' (a1_0= parse_org_eclipse_uml2_uml_PackageImport ) )
            	    {
            	    // Acm.g:1349:3: (a0= 'import' (a1_0= parse_org_eclipse_uml2_uml_PackageImport ) )
            	    // Acm.g:1350:4: a0= 'import' (a1_0= parse_org_eclipse_uml2_uml_PackageImport )
            	    {
            	    a0=(Token)match(input,9,FOLLOW_9_in_parse_org_eclipse_uml2_uml_Model1076); if (state.failed) return element;
            	    if ( state.backtracking==0 ) {

            	      				if (element == null) {
            	      					element = org.eclipse.uml2.uml.UMLFactory.eINSTANCE.createModel();
            	      					incompleteObjects.push(element);
            	      				}
            	      				collectHiddenTokens(element);
            	      				retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_0_0_0_0_0_0_0, null, true);
            	      				copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken)a0, element);
            	      			
            	    }
            	    if ( state.backtracking==0 ) {

            	      				// expected elements (follow set)
            	      				addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_33, 36, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_5);
            	      			
            	    }
            	    // Acm.g:1364:4: (a1_0= parse_org_eclipse_uml2_uml_PackageImport )
            	    // Acm.g:1365:5: a1_0= parse_org_eclipse_uml2_uml_PackageImport
            	    {
            	    pushFollow(FOLLOW_parse_org_eclipse_uml2_uml_PackageImport_in_parse_org_eclipse_uml2_uml_Model1102);
            	    a1_0=parse_org_eclipse_uml2_uml_PackageImport();

            	    state._fsp--;
            	    if (state.failed) return element;
            	    if ( state.backtracking==0 ) {

            	      					if (terminateParsing) {
            	      						throw new sare.language.acm.resource.acm.mopp.AcmTerminateParsingException();
            	      					}
            	      					if (element == null) {
            	      						element = org.eclipse.uml2.uml.UMLFactory.eINSTANCE.createModel();
            	      						incompleteObjects.push(element);
            	      					}
            	      					if (a1_0 != null) {
            	      						if (a1_0 != null) {
            	      							Object value = a1_0;
            	      							addObjectToList(element, org.eclipse.uml2.uml.UMLPackage.MODEL__PACKAGE_IMPORT, value);
            	      							completedElement(value, true);
            	      						}
            	      						collectHiddenTokens(element);
            	      						retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_0_0_0_0_0_0_1, a1_0, true);
            	      						copyLocalizationInfos(a1_0, element);
            	      					}
            	      				
            	    }

            	    }

            	    if ( state.backtracking==0 ) {

            	      				// expected elements (follow set)
            	      				addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_8, 37);
            	      				addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_9, 37);
            	      			
            	    }

            	    }


            	    }
            	    break;

            	default :
            	    break loop11;
                }
            } while (true);

            if ( state.backtracking==0 ) {

              		// expected elements (follow set)
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_8, 38);
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_9, 38);
              	
            }
            a2=(Token)match(input,19,FOLLOW_19_in_parse_org_eclipse_uml2_uml_Model1143); if (state.failed) return element;
            if ( state.backtracking==0 ) {

              		if (element == null) {
              			element = org.eclipse.uml2.uml.UMLFactory.eINSTANCE.createModel();
              			incompleteObjects.push(element);
              		}
              		collectHiddenTokens(element);
              		retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_0_0_0_1, null, true);
              		copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken)a2, element);
              	
            }
            if ( state.backtracking==0 ) {

              		// expected elements (follow set)
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_34, 39);
              	
            }
            // Acm.g:1413:2: (a3= TEXT )
            // Acm.g:1414:3: a3= TEXT
            {
            a3=(Token)match(input,TEXT,FOLLOW_TEXT_in_parse_org_eclipse_uml2_uml_Model1161); if (state.failed) return element;
            if ( state.backtracking==0 ) {

              			if (terminateParsing) {
              				throw new sare.language.acm.resource.acm.mopp.AcmTerminateParsingException();
              			}
              			if (element == null) {
              				element = org.eclipse.uml2.uml.UMLFactory.eINSTANCE.createModel();
              				incompleteObjects.push(element);
              			}
              			if (a3 != null) {
              				sare.language.acm.resource.acm.IAcmTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
              				tokenResolver.setOptions(getOptions());
              				sare.language.acm.resource.acm.IAcmTokenResolveResult result = getFreshTokenResolveResult();
              				tokenResolver.resolve(a3.getText(), element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.MODEL__NAME), result);
              				Object resolvedObject = result.getResolvedToken();
              				if (resolvedObject == null) {
              					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_3_0.CommonToken) a3).getLine(), ((org.antlr.runtime3_3_0.CommonToken) a3).getCharPositionInLine(), ((org.antlr.runtime3_3_0.CommonToken) a3).getStartIndex(), ((org.antlr.runtime3_3_0.CommonToken) a3).getStopIndex());
              				}
              				java.lang.String resolved = (java.lang.String) resolvedObject;
              				if (resolved != null) {
              					Object value = resolved;
              					element.eSet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.MODEL__NAME), value);
              					completedElement(value, false);
              				}
              				collectHiddenTokens(element);
              				retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_0_0_0_2, resolved, true);
              				copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken) a3, element);
              			}
              		
            }

            }

            if ( state.backtracking==0 ) {

              		// expected elements (follow set)
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_35, 40);
              	
            }
            a4=(Token)match(input,11,FOLLOW_11_in_parse_org_eclipse_uml2_uml_Model1182); if (state.failed) return element;
            if ( state.backtracking==0 ) {

              		if (element == null) {
              			element = org.eclipse.uml2.uml.UMLFactory.eINSTANCE.createModel();
              			incompleteObjects.push(element);
              		}
              		collectHiddenTokens(element);
              		retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_0_0_0_3, null, true);
              		copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken)a4, element);
              	
            }
            if ( state.backtracking==0 ) {

              		// expected elements (follow set)
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_0, 41, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_1);
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_1, 41, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_1);
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_8, 41, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_1);
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_9, 41, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_1);
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_10, 41, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_1);
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_11, 41, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_2, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_1);
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_12, 41, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_2, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_1);
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_13, 41, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_1);
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_14, 41);
              	
            }
            // Acm.g:1471:2: ( (a5_0= parse_org_eclipse_uml2_uml_PackageableElement ) )*
            loop12:
            do {
                int alt12=2;
                int LA12_0 = input.LA(1);

                if ( ((LA12_0>=9 && LA12_0<=10)||(LA12_0>=19 && LA12_0<=21)||LA12_0==25||LA12_0==27) ) {
                    alt12=1;
                }


                switch (alt12) {
            	case 1 :
            	    // Acm.g:1472:3: (a5_0= parse_org_eclipse_uml2_uml_PackageableElement )
            	    {
            	    // Acm.g:1472:3: (a5_0= parse_org_eclipse_uml2_uml_PackageableElement )
            	    // Acm.g:1473:4: a5_0= parse_org_eclipse_uml2_uml_PackageableElement
            	    {
            	    pushFollow(FOLLOW_parse_org_eclipse_uml2_uml_PackageableElement_in_parse_org_eclipse_uml2_uml_Model1205);
            	    a5_0=parse_org_eclipse_uml2_uml_PackageableElement();

            	    state._fsp--;
            	    if (state.failed) return element;
            	    if ( state.backtracking==0 ) {

            	      				if (terminateParsing) {
            	      					throw new sare.language.acm.resource.acm.mopp.AcmTerminateParsingException();
            	      				}
            	      				if (element == null) {
            	      					element = org.eclipse.uml2.uml.UMLFactory.eINSTANCE.createModel();
            	      					incompleteObjects.push(element);
            	      				}
            	      				if (a5_0 != null) {
            	      					if (a5_0 != null) {
            	      						Object value = a5_0;
            	      						addObjectToList(element, org.eclipse.uml2.uml.UMLPackage.MODEL__PACKAGED_ELEMENT, value);
            	      						completedElement(value, true);
            	      					}
            	      					collectHiddenTokens(element);
            	      					retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_0_0_0_4, a5_0, true);
            	      					copyLocalizationInfos(a5_0, element);
            	      				}
            	      			
            	    }

            	    }


            	    }
            	    break;

            	default :
            	    break loop12;
                }
            } while (true);

            if ( state.backtracking==0 ) {

              		// expected elements (follow set)
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_0, 42, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_1);
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_1, 42, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_1);
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_8, 42, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_1);
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_9, 42, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_1);
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_10, 42, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_1);
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_11, 42, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_2, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_1);
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_12, 42, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_2, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_1);
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_13, 42, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_1);
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_14, 42);
              	
            }
            a6=(Token)match(input,12,FOLLOW_12_in_parse_org_eclipse_uml2_uml_Model1231); if (state.failed) return element;
            if ( state.backtracking==0 ) {

              		if (element == null) {
              			element = org.eclipse.uml2.uml.UMLFactory.eINSTANCE.createModel();
              			incompleteObjects.push(element);
              		}
              		collectHiddenTokens(element);
              		retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_0_0_0_5, null, true);
              		copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken)a6, element);
              	
            }
            if ( state.backtracking==0 ) {

              		// expected elements (follow set)
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_0, 43, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_1);
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_1, 43, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_1);
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_8, 43, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_1);
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_9, 43, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_1);
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_10, 43, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_1);
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_11, 43, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_2, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_1);
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_12, 43, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_2, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_1);
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_13, 43, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_1);
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_14, 43);
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_15, 43);
              	
            }

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
            if ( state.backtracking>0 ) { memoize(input, 6, parse_org_eclipse_uml2_uml_Model_StartIndex); }
        }
        return element;
    }
    // $ANTLR end "parse_org_eclipse_uml2_uml_Model"


    // $ANTLR start "parse_org_eclipse_uml2_uml_PackageImport"
    // Acm.g:1532:1: parse_org_eclipse_uml2_uml_PackageImport returns [org.eclipse.uml2.uml.PackageImport element = null] : (a0= QUOTED_34_34 ) ;
    public final org.eclipse.uml2.uml.PackageImport parse_org_eclipse_uml2_uml_PackageImport() throws RecognitionException {
        org.eclipse.uml2.uml.PackageImport element =  null;
        int parse_org_eclipse_uml2_uml_PackageImport_StartIndex = input.index();
        Token a0=null;



        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 7) ) { return element; }
            // Acm.g:1535:1: ( (a0= QUOTED_34_34 ) )
            // Acm.g:1536:2: (a0= QUOTED_34_34 )
            {
            // Acm.g:1536:2: (a0= QUOTED_34_34 )
            // Acm.g:1537:3: a0= QUOTED_34_34
            {
            a0=(Token)match(input,QUOTED_34_34,FOLLOW_QUOTED_34_34_in_parse_org_eclipse_uml2_uml_PackageImport1264); if (state.failed) return element;
            if ( state.backtracking==0 ) {

              			if (terminateParsing) {
              				throw new sare.language.acm.resource.acm.mopp.AcmTerminateParsingException();
              			}
              			if (element == null) {
              				element = org.eclipse.uml2.uml.UMLFactory.eINSTANCE.createPackageImport();
              				incompleteObjects.push(element);
              			}
              			if (a0 != null) {
              				sare.language.acm.resource.acm.IAcmTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("QUOTED_34_34");
              				tokenResolver.setOptions(getOptions());
              				sare.language.acm.resource.acm.IAcmTokenResolveResult result = getFreshTokenResolveResult();
              				tokenResolver.resolve(a0.getText(), element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.PACKAGE_IMPORT__IMPORTED_PACKAGE), result);
              				Object resolvedObject = result.getResolvedToken();
              				if (resolvedObject == null) {
              					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_3_0.CommonToken) a0).getLine(), ((org.antlr.runtime3_3_0.CommonToken) a0).getCharPositionInLine(), ((org.antlr.runtime3_3_0.CommonToken) a0).getStartIndex(), ((org.antlr.runtime3_3_0.CommonToken) a0).getStopIndex());
              				}
              				String resolved = (String) resolvedObject;
              				org.eclipse.uml2.uml.Package proxy = org.eclipse.uml2.uml.UMLFactory.eINSTANCE.createPackage();
              				collectHiddenTokens(element);
              				registerContextDependentProxy(new sare.language.acm.resource.acm.mopp.AcmContextDependentURIFragmentFactory<org.eclipse.uml2.uml.PackageImport, org.eclipse.uml2.uml.Package>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getPackageImportImportedPackageReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.PACKAGE_IMPORT__IMPORTED_PACKAGE), resolved, proxy);
              				if (proxy != null) {
              					Object value = proxy;
              					element.eSet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.PACKAGE_IMPORT__IMPORTED_PACKAGE), value);
              					completedElement(value, false);
              				}
              				collectHiddenTokens(element);
              				retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_1_0_0_0, proxy, true);
              				copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken) a0, element);
              				copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken) a0, proxy);
              			}
              		
            }

            }

            if ( state.backtracking==0 ) {

              		// expected elements (follow set)
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_8, 44);
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_9, 44);
              	
            }

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
            if ( state.backtracking>0 ) { memoize(input, 7, parse_org_eclipse_uml2_uml_PackageImport_StartIndex); }
        }
        return element;
    }
    // $ANTLR end "parse_org_eclipse_uml2_uml_PackageImport"


    // $ANTLR start "parse_org_eclipse_uml2_uml_Package"
    // Acm.g:1579:1: parse_org_eclipse_uml2_uml_Package returns [org.eclipse.uml2.uml.Package element = null] : (a0= 'package' (a1= TEXT ) a2= '{' ( (a3_0= parse_org_eclipse_uml2_uml_PackageableElement ) )* a4= '}' | c0= parse_org_eclipse_uml2_uml_Model );
    public final org.eclipse.uml2.uml.Package parse_org_eclipse_uml2_uml_Package() throws RecognitionException {
        org.eclipse.uml2.uml.Package element =  null;
        int parse_org_eclipse_uml2_uml_Package_StartIndex = input.index();
        Token a0=null;
        Token a1=null;
        Token a2=null;
        Token a4=null;
        org.eclipse.uml2.uml.PackageableElement a3_0 = null;

        org.eclipse.uml2.uml.Model c0 = null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 8) ) { return element; }
            // Acm.g:1582:1: (a0= 'package' (a1= TEXT ) a2= '{' ( (a3_0= parse_org_eclipse_uml2_uml_PackageableElement ) )* a4= '}' | c0= parse_org_eclipse_uml2_uml_Model )
            int alt14=2;
            int LA14_0 = input.LA(1);

            if ( (LA14_0==20) ) {
                alt14=1;
            }
            else if ( (LA14_0==9||LA14_0==19) ) {
                alt14=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return element;}
                NoViableAltException nvae =
                    new NoViableAltException("", 14, 0, input);

                throw nvae;
            }
            switch (alt14) {
                case 1 :
                    // Acm.g:1583:2: a0= 'package' (a1= TEXT ) a2= '{' ( (a3_0= parse_org_eclipse_uml2_uml_PackageableElement ) )* a4= '}'
                    {
                    a0=(Token)match(input,20,FOLLOW_20_in_parse_org_eclipse_uml2_uml_Package1300); if (state.failed) return element;
                    if ( state.backtracking==0 ) {

                      		if (element == null) {
                      			element = org.eclipse.uml2.uml.UMLFactory.eINSTANCE.createPackage();
                      			incompleteObjects.push(element);
                      		}
                      		collectHiddenTokens(element);
                      		retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_2_0_0_0, null, true);
                      		copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken)a0, element);
                      	
                    }
                    if ( state.backtracking==0 ) {

                      		// expected elements (follow set)
                      		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_36, 45);
                      	
                    }
                    // Acm.g:1597:2: (a1= TEXT )
                    // Acm.g:1598:3: a1= TEXT
                    {
                    a1=(Token)match(input,TEXT,FOLLOW_TEXT_in_parse_org_eclipse_uml2_uml_Package1318); if (state.failed) return element;
                    if ( state.backtracking==0 ) {

                      			if (terminateParsing) {
                      				throw new sare.language.acm.resource.acm.mopp.AcmTerminateParsingException();
                      			}
                      			if (element == null) {
                      				element = org.eclipse.uml2.uml.UMLFactory.eINSTANCE.createPackage();
                      				incompleteObjects.push(element);
                      			}
                      			if (a1 != null) {
                      				sare.language.acm.resource.acm.IAcmTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
                      				tokenResolver.setOptions(getOptions());
                      				sare.language.acm.resource.acm.IAcmTokenResolveResult result = getFreshTokenResolveResult();
                      				tokenResolver.resolve(a1.getText(), element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.PACKAGE__NAME), result);
                      				Object resolvedObject = result.getResolvedToken();
                      				if (resolvedObject == null) {
                      					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_3_0.CommonToken) a1).getLine(), ((org.antlr.runtime3_3_0.CommonToken) a1).getCharPositionInLine(), ((org.antlr.runtime3_3_0.CommonToken) a1).getStartIndex(), ((org.antlr.runtime3_3_0.CommonToken) a1).getStopIndex());
                      				}
                      				java.lang.String resolved = (java.lang.String) resolvedObject;
                      				if (resolved != null) {
                      					Object value = resolved;
                      					element.eSet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.PACKAGE__NAME), value);
                      					completedElement(value, false);
                      				}
                      				collectHiddenTokens(element);
                      				retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_2_0_0_1, resolved, true);
                      				copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken) a1, element);
                      			}
                      		
                    }

                    }

                    if ( state.backtracking==0 ) {

                      		// expected elements (follow set)
                      		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_37, 46);
                      	
                    }
                    a2=(Token)match(input,11,FOLLOW_11_in_parse_org_eclipse_uml2_uml_Package1339); if (state.failed) return element;
                    if ( state.backtracking==0 ) {

                      		if (element == null) {
                      			element = org.eclipse.uml2.uml.UMLFactory.eINSTANCE.createPackage();
                      			incompleteObjects.push(element);
                      		}
                      		collectHiddenTokens(element);
                      		retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_2_0_0_2, null, true);
                      		copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken)a2, element);
                      	
                    }
                    if ( state.backtracking==0 ) {

                      		// expected elements (follow set)
                      		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_0, 47, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_1);
                      		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_1, 47, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_1);
                      		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_8, 47, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_1);
                      		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_9, 47, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_1);
                      		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_10, 47, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_1);
                      		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_11, 47, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_2, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_1);
                      		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_12, 47, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_2, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_1);
                      		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_13, 47, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_1);
                      		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_15, 47);
                      	
                    }
                    // Acm.g:1655:2: ( (a3_0= parse_org_eclipse_uml2_uml_PackageableElement ) )*
                    loop13:
                    do {
                        int alt13=2;
                        int LA13_0 = input.LA(1);

                        if ( ((LA13_0>=9 && LA13_0<=10)||(LA13_0>=19 && LA13_0<=21)||LA13_0==25||LA13_0==27) ) {
                            alt13=1;
                        }


                        switch (alt13) {
                    	case 1 :
                    	    // Acm.g:1656:3: (a3_0= parse_org_eclipse_uml2_uml_PackageableElement )
                    	    {
                    	    // Acm.g:1656:3: (a3_0= parse_org_eclipse_uml2_uml_PackageableElement )
                    	    // Acm.g:1657:4: a3_0= parse_org_eclipse_uml2_uml_PackageableElement
                    	    {
                    	    pushFollow(FOLLOW_parse_org_eclipse_uml2_uml_PackageableElement_in_parse_org_eclipse_uml2_uml_Package1362);
                    	    a3_0=parse_org_eclipse_uml2_uml_PackageableElement();

                    	    state._fsp--;
                    	    if (state.failed) return element;
                    	    if ( state.backtracking==0 ) {

                    	      				if (terminateParsing) {
                    	      					throw new sare.language.acm.resource.acm.mopp.AcmTerminateParsingException();
                    	      				}
                    	      				if (element == null) {
                    	      					element = org.eclipse.uml2.uml.UMLFactory.eINSTANCE.createPackage();
                    	      					incompleteObjects.push(element);
                    	      				}
                    	      				if (a3_0 != null) {
                    	      					if (a3_0 != null) {
                    	      						Object value = a3_0;
                    	      						addObjectToList(element, org.eclipse.uml2.uml.UMLPackage.PACKAGE__PACKAGED_ELEMENT, value);
                    	      						completedElement(value, true);
                    	      					}
                    	      					collectHiddenTokens(element);
                    	      					retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_2_0_0_3, a3_0, true);
                    	      					copyLocalizationInfos(a3_0, element);
                    	      				}
                    	      			
                    	    }

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop13;
                        }
                    } while (true);

                    if ( state.backtracking==0 ) {

                      		// expected elements (follow set)
                      		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_0, 48, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_1);
                      		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_1, 48, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_1);
                      		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_8, 48, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_1);
                      		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_9, 48, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_1);
                      		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_10, 48, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_1);
                      		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_11, 48, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_2, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_1);
                      		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_12, 48, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_2, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_1);
                      		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_13, 48, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_1);
                      		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_15, 48);
                      	
                    }
                    a4=(Token)match(input,12,FOLLOW_12_in_parse_org_eclipse_uml2_uml_Package1388); if (state.failed) return element;
                    if ( state.backtracking==0 ) {

                      		if (element == null) {
                      			element = org.eclipse.uml2.uml.UMLFactory.eINSTANCE.createPackage();
                      			incompleteObjects.push(element);
                      		}
                      		collectHiddenTokens(element);
                      		retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_2_0_0_4, null, true);
                      		copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken)a4, element);
                      	
                    }
                    if ( state.backtracking==0 ) {

                      		// expected elements (follow set)
                      		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_0, 49, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_1);
                      		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_1, 49, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_1);
                      		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_8, 49, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_1);
                      		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_9, 49, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_1);
                      		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_10, 49, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_1);
                      		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_11, 49, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_2, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_1);
                      		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_12, 49, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_2, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_1);
                      		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_13, 49, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_1);
                      		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_14, 49);
                      		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_15, 49);
                      	
                    }

                    }
                    break;
                case 2 :
                    // Acm.g:1716:2: c0= parse_org_eclipse_uml2_uml_Model
                    {
                    pushFollow(FOLLOW_parse_org_eclipse_uml2_uml_Model_in_parse_org_eclipse_uml2_uml_Package1407);
                    c0=parse_org_eclipse_uml2_uml_Model();

                    state._fsp--;
                    if (state.failed) return element;
                    if ( state.backtracking==0 ) {
                       element = c0; /* this is a subclass or primitive expression choice */ 
                    }

                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
            if ( state.backtracking>0 ) { memoize(input, 8, parse_org_eclipse_uml2_uml_Package_StartIndex); }
        }
        return element;
    }
    // $ANTLR end "parse_org_eclipse_uml2_uml_Package"


    // $ANTLR start "parse_org_eclipse_uml2_uml_Class"
    // Acm.g:1720:1: parse_org_eclipse_uml2_uml_Class returns [org.eclipse.uml2.uml.Class element = null] : ( (a0_0= parse_org_eclipse_uml2_uml_Comment ) )* a1= 'class' (a2= TEXT ) ( (a3= 'extends' (a4= TEXT ) ( (a5= ',' (a6= TEXT ) ) )* ) )? a7= '{' ( ( (a8_0= parse_org_eclipse_uml2_uml_Property ) | (a9_0= parse_org_eclipse_uml2_uml_Operation ) ) )* a10= '}' ;
    public final org.eclipse.uml2.uml.Class parse_org_eclipse_uml2_uml_Class() throws RecognitionException {
        org.eclipse.uml2.uml.Class element =  null;
        int parse_org_eclipse_uml2_uml_Class_StartIndex = input.index();
        Token a1=null;
        Token a2=null;
        Token a3=null;
        Token a4=null;
        Token a5=null;
        Token a6=null;
        Token a7=null;
        Token a10=null;
        org.eclipse.uml2.uml.Comment a0_0 = null;

        org.eclipse.uml2.uml.Property a8_0 = null;

        org.eclipse.uml2.uml.Operation a9_0 = null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 9) ) { return element; }
            // Acm.g:1723:1: ( ( (a0_0= parse_org_eclipse_uml2_uml_Comment ) )* a1= 'class' (a2= TEXT ) ( (a3= 'extends' (a4= TEXT ) ( (a5= ',' (a6= TEXT ) ) )* ) )? a7= '{' ( ( (a8_0= parse_org_eclipse_uml2_uml_Property ) | (a9_0= parse_org_eclipse_uml2_uml_Operation ) ) )* a10= '}' )
            // Acm.g:1724:2: ( (a0_0= parse_org_eclipse_uml2_uml_Comment ) )* a1= 'class' (a2= TEXT ) ( (a3= 'extends' (a4= TEXT ) ( (a5= ',' (a6= TEXT ) ) )* ) )? a7= '{' ( ( (a8_0= parse_org_eclipse_uml2_uml_Property ) | (a9_0= parse_org_eclipse_uml2_uml_Operation ) ) )* a10= '}'
            {
            // Acm.g:1724:2: ( (a0_0= parse_org_eclipse_uml2_uml_Comment ) )*
            loop15:
            do {
                int alt15=2;
                int LA15_0 = input.LA(1);

                if ( (LA15_0==25||LA15_0==27) ) {
                    alt15=1;
                }


                switch (alt15) {
            	case 1 :
            	    // Acm.g:1725:3: (a0_0= parse_org_eclipse_uml2_uml_Comment )
            	    {
            	    // Acm.g:1725:3: (a0_0= parse_org_eclipse_uml2_uml_Comment )
            	    // Acm.g:1726:4: a0_0= parse_org_eclipse_uml2_uml_Comment
            	    {
            	    pushFollow(FOLLOW_parse_org_eclipse_uml2_uml_Comment_in_parse_org_eclipse_uml2_uml_Class1441);
            	    a0_0=parse_org_eclipse_uml2_uml_Comment();

            	    state._fsp--;
            	    if (state.failed) return element;
            	    if ( state.backtracking==0 ) {

            	      				if (terminateParsing) {
            	      					throw new sare.language.acm.resource.acm.mopp.AcmTerminateParsingException();
            	      				}
            	      				if (element == null) {
            	      					element = org.eclipse.uml2.uml.UMLFactory.eINSTANCE.createClass();
            	      					incompleteObjects.push(element);
            	      				}
            	      				if (a0_0 != null) {
            	      					if (a0_0 != null) {
            	      						Object value = a0_0;
            	      						addObjectToList(element, org.eclipse.uml2.uml.UMLPackage.CLASS__OWNED_COMMENT, value);
            	      						completedElement(value, true);
            	      					}
            	      					collectHiddenTokens(element);
            	      					retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_3_0_0_0, a0_0, true);
            	      					copyLocalizationInfos(a0_0, element);
            	      				}
            	      			
            	    }

            	    }


            	    }
            	    break;

            	default :
            	    break loop15;
                }
            } while (true);

            if ( state.backtracking==0 ) {

              		// expected elements (follow set)
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_11, 50, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_2);
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_12, 50, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_2);
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_13, 50);
              	
            }
            a1=(Token)match(input,21,FOLLOW_21_in_parse_org_eclipse_uml2_uml_Class1467); if (state.failed) return element;
            if ( state.backtracking==0 ) {

              		if (element == null) {
              			element = org.eclipse.uml2.uml.UMLFactory.eINSTANCE.createClass();
              			incompleteObjects.push(element);
              		}
              		collectHiddenTokens(element);
              		retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_3_0_0_1, null, true);
              		copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken)a1, element);
              	
            }
            if ( state.backtracking==0 ) {

              		// expected elements (follow set)
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_38, 51);
              	
            }
            // Acm.g:1768:2: (a2= TEXT )
            // Acm.g:1769:3: a2= TEXT
            {
            a2=(Token)match(input,TEXT,FOLLOW_TEXT_in_parse_org_eclipse_uml2_uml_Class1485); if (state.failed) return element;
            if ( state.backtracking==0 ) {

              			if (terminateParsing) {
              				throw new sare.language.acm.resource.acm.mopp.AcmTerminateParsingException();
              			}
              			if (element == null) {
              				element = org.eclipse.uml2.uml.UMLFactory.eINSTANCE.createClass();
              				incompleteObjects.push(element);
              			}
              			if (a2 != null) {
              				sare.language.acm.resource.acm.IAcmTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
              				tokenResolver.setOptions(getOptions());
              				sare.language.acm.resource.acm.IAcmTokenResolveResult result = getFreshTokenResolveResult();
              				tokenResolver.resolve(a2.getText(), element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.CLASS__NAME), result);
              				Object resolvedObject = result.getResolvedToken();
              				if (resolvedObject == null) {
              					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_3_0.CommonToken) a2).getLine(), ((org.antlr.runtime3_3_0.CommonToken) a2).getCharPositionInLine(), ((org.antlr.runtime3_3_0.CommonToken) a2).getStartIndex(), ((org.antlr.runtime3_3_0.CommonToken) a2).getStopIndex());
              				}
              				java.lang.String resolved = (java.lang.String) resolvedObject;
              				if (resolved != null) {
              					Object value = resolved;
              					element.eSet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.CLASS__NAME), value);
              					completedElement(value, false);
              				}
              				collectHiddenTokens(element);
              				retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_3_0_0_2, resolved, true);
              				copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken) a2, element);
              			}
              		
            }

            }

            if ( state.backtracking==0 ) {

              		// expected elements (follow set)
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_39, 52);
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_40, 52);
              	
            }
            // Acm.g:1805:2: ( (a3= 'extends' (a4= TEXT ) ( (a5= ',' (a6= TEXT ) ) )* ) )?
            int alt17=2;
            int LA17_0 = input.LA(1);

            if ( (LA17_0==14) ) {
                alt17=1;
            }
            switch (alt17) {
                case 1 :
                    // Acm.g:1806:3: (a3= 'extends' (a4= TEXT ) ( (a5= ',' (a6= TEXT ) ) )* )
                    {
                    // Acm.g:1806:3: (a3= 'extends' (a4= TEXT ) ( (a5= ',' (a6= TEXT ) ) )* )
                    // Acm.g:1807:4: a3= 'extends' (a4= TEXT ) ( (a5= ',' (a6= TEXT ) ) )*
                    {
                    a3=(Token)match(input,14,FOLLOW_14_in_parse_org_eclipse_uml2_uml_Class1515); if (state.failed) return element;
                    if ( state.backtracking==0 ) {

                      				if (element == null) {
                      					element = org.eclipse.uml2.uml.UMLFactory.eINSTANCE.createClass();
                      					incompleteObjects.push(element);
                      				}
                      				collectHiddenTokens(element);
                      				retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_3_0_0_3_0_0_0, null, true);
                      				copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken)a3, element);
                      			
                    }
                    if ( state.backtracking==0 ) {

                      				// expected elements (follow set)
                      				addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_41, 53);
                      			
                    }
                    // Acm.g:1821:4: (a4= TEXT )
                    // Acm.g:1822:5: a4= TEXT
                    {
                    a4=(Token)match(input,TEXT,FOLLOW_TEXT_in_parse_org_eclipse_uml2_uml_Class1541); if (state.failed) return element;
                    if ( state.backtracking==0 ) {

                      					if (terminateParsing) {
                      						throw new sare.language.acm.resource.acm.mopp.AcmTerminateParsingException();
                      					}
                      					if (element == null) {
                      						element = org.eclipse.uml2.uml.UMLFactory.eINSTANCE.createClass();
                      						incompleteObjects.push(element);
                      					}
                      					if (a4 != null) {
                      						sare.language.acm.resource.acm.IAcmTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
                      						tokenResolver.setOptions(getOptions());
                      						sare.language.acm.resource.acm.IAcmTokenResolveResult result = getFreshTokenResolveResult();
                      						tokenResolver.resolve(a4.getText(), element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.CLASS__SUPER_CLASS), result);
                      						Object resolvedObject = result.getResolvedToken();
                      						if (resolvedObject == null) {
                      							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_3_0.CommonToken) a4).getLine(), ((org.antlr.runtime3_3_0.CommonToken) a4).getCharPositionInLine(), ((org.antlr.runtime3_3_0.CommonToken) a4).getStartIndex(), ((org.antlr.runtime3_3_0.CommonToken) a4).getStopIndex());
                      						}
                      						String resolved = (String) resolvedObject;
                      						org.eclipse.uml2.uml.Class proxy = org.eclipse.uml2.uml.UMLFactory.eINSTANCE.createClass();
                      						collectHiddenTokens(element);
                      						registerContextDependentProxy(new sare.language.acm.resource.acm.mopp.AcmContextDependentURIFragmentFactory<org.eclipse.uml2.uml.Class, org.eclipse.uml2.uml.Class>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getClassSuperClassReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.CLASS__SUPER_CLASS), resolved, proxy);
                      						if (proxy != null) {
                      							Object value = proxy;
                      							addObjectToList(element, org.eclipse.uml2.uml.UMLPackage.CLASS__SUPER_CLASS, value);
                      							completedElement(value, false);
                      						}
                      						collectHiddenTokens(element);
                      						retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_3_0_0_3_0_0_1, proxy, true);
                      						copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken) a4, element);
                      						copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken) a4, proxy);
                      					}
                      				
                    }

                    }

                    if ( state.backtracking==0 ) {

                      				// expected elements (follow set)
                      				addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_42, 54);
                      				addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_40, 54);
                      			
                    }
                    // Acm.g:1862:4: ( (a5= ',' (a6= TEXT ) ) )*
                    loop16:
                    do {
                        int alt16=2;
                        int LA16_0 = input.LA(1);

                        if ( (LA16_0==15) ) {
                            alt16=1;
                        }


                        switch (alt16) {
                    	case 1 :
                    	    // Acm.g:1863:5: (a5= ',' (a6= TEXT ) )
                    	    {
                    	    // Acm.g:1863:5: (a5= ',' (a6= TEXT ) )
                    	    // Acm.g:1864:6: a5= ',' (a6= TEXT )
                    	    {
                    	    a5=(Token)match(input,15,FOLLOW_15_in_parse_org_eclipse_uml2_uml_Class1587); if (state.failed) return element;
                    	    if ( state.backtracking==0 ) {

                    	      						if (element == null) {
                    	      							element = org.eclipse.uml2.uml.UMLFactory.eINSTANCE.createClass();
                    	      							incompleteObjects.push(element);
                    	      						}
                    	      						collectHiddenTokens(element);
                    	      						retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_3_0_0_3_0_0_2_0_0_0, null, true);
                    	      						copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken)a5, element);
                    	      					
                    	    }
                    	    if ( state.backtracking==0 ) {

                    	      						// expected elements (follow set)
                    	      						addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_43, 55);
                    	      					
                    	    }
                    	    // Acm.g:1878:6: (a6= TEXT )
                    	    // Acm.g:1879:7: a6= TEXT
                    	    {
                    	    a6=(Token)match(input,TEXT,FOLLOW_TEXT_in_parse_org_eclipse_uml2_uml_Class1621); if (state.failed) return element;
                    	    if ( state.backtracking==0 ) {

                    	      							if (terminateParsing) {
                    	      								throw new sare.language.acm.resource.acm.mopp.AcmTerminateParsingException();
                    	      							}
                    	      							if (element == null) {
                    	      								element = org.eclipse.uml2.uml.UMLFactory.eINSTANCE.createClass();
                    	      								incompleteObjects.push(element);
                    	      							}
                    	      							if (a6 != null) {
                    	      								sare.language.acm.resource.acm.IAcmTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
                    	      								tokenResolver.setOptions(getOptions());
                    	      								sare.language.acm.resource.acm.IAcmTokenResolveResult result = getFreshTokenResolveResult();
                    	      								tokenResolver.resolve(a6.getText(), element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.CLASS__SUPER_CLASS), result);
                    	      								Object resolvedObject = result.getResolvedToken();
                    	      								if (resolvedObject == null) {
                    	      									addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_3_0.CommonToken) a6).getLine(), ((org.antlr.runtime3_3_0.CommonToken) a6).getCharPositionInLine(), ((org.antlr.runtime3_3_0.CommonToken) a6).getStartIndex(), ((org.antlr.runtime3_3_0.CommonToken) a6).getStopIndex());
                    	      								}
                    	      								String resolved = (String) resolvedObject;
                    	      								org.eclipse.uml2.uml.Class proxy = org.eclipse.uml2.uml.UMLFactory.eINSTANCE.createClass();
                    	      								collectHiddenTokens(element);
                    	      								registerContextDependentProxy(new sare.language.acm.resource.acm.mopp.AcmContextDependentURIFragmentFactory<org.eclipse.uml2.uml.Class, org.eclipse.uml2.uml.Class>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getClassSuperClassReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.CLASS__SUPER_CLASS), resolved, proxy);
                    	      								if (proxy != null) {
                    	      									Object value = proxy;
                    	      									addObjectToList(element, org.eclipse.uml2.uml.UMLPackage.CLASS__SUPER_CLASS, value);
                    	      									completedElement(value, false);
                    	      								}
                    	      								collectHiddenTokens(element);
                    	      								retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_3_0_0_3_0_0_2_0_0_1, proxy, true);
                    	      								copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken) a6, element);
                    	      								copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken) a6, proxy);
                    	      							}
                    	      						
                    	    }

                    	    }

                    	    if ( state.backtracking==0 ) {

                    	      						// expected elements (follow set)
                    	      						addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_42, 56);
                    	      						addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_40, 56);
                    	      					
                    	    }

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop16;
                        }
                    } while (true);

                    if ( state.backtracking==0 ) {

                      				// expected elements (follow set)
                      				addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_42, 57);
                      				addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_40, 57);
                      			
                    }

                    }


                    }
                    break;

            }

            if ( state.backtracking==0 ) {

              		// expected elements (follow set)
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_40, 58);
              	
            }
            a7=(Token)match(input,11,FOLLOW_11_in_parse_org_eclipse_uml2_uml_Class1702); if (state.failed) return element;
            if ( state.backtracking==0 ) {

              		if (element == null) {
              			element = org.eclipse.uml2.uml.UMLFactory.eINSTANCE.createClass();
              			incompleteObjects.push(element);
              		}
              		collectHiddenTokens(element);
              		retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_3_0_0_4, null, true);
              		copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken)a7, element);
              	
            }
            if ( state.backtracking==0 ) {

              		// expected elements (follow set)
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_11, 59, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_2, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_6);
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_12, 59, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_2, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_6);
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_44, 59, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_6);
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_45, 59, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_7);
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_46, 59);
              	
            }
            // Acm.g:1952:2: ( ( (a8_0= parse_org_eclipse_uml2_uml_Property ) | (a9_0= parse_org_eclipse_uml2_uml_Operation ) ) )*
            loop19:
            do {
                int alt19=2;
                int LA19_0 = input.LA(1);

                if ( (LA19_0==TEXT||LA19_0==25||LA19_0==27) ) {
                    alt19=1;
                }


                switch (alt19) {
            	case 1 :
            	    // Acm.g:1953:3: ( (a8_0= parse_org_eclipse_uml2_uml_Property ) | (a9_0= parse_org_eclipse_uml2_uml_Operation ) )
            	    {
            	    // Acm.g:1953:3: ( (a8_0= parse_org_eclipse_uml2_uml_Property ) | (a9_0= parse_org_eclipse_uml2_uml_Operation ) )
            	    int alt18=2;
            	    alt18 = dfa18.predict(input);
            	    switch (alt18) {
            	        case 1 :
            	            // Acm.g:1954:4: (a8_0= parse_org_eclipse_uml2_uml_Property )
            	            {
            	            // Acm.g:1954:4: (a8_0= parse_org_eclipse_uml2_uml_Property )
            	            // Acm.g:1955:5: a8_0= parse_org_eclipse_uml2_uml_Property
            	            {
            	            pushFollow(FOLLOW_parse_org_eclipse_uml2_uml_Property_in_parse_org_eclipse_uml2_uml_Class1731);
            	            a8_0=parse_org_eclipse_uml2_uml_Property();

            	            state._fsp--;
            	            if (state.failed) return element;
            	            if ( state.backtracking==0 ) {

            	              					if (terminateParsing) {
            	              						throw new sare.language.acm.resource.acm.mopp.AcmTerminateParsingException();
            	              					}
            	              					if (element == null) {
            	              						element = org.eclipse.uml2.uml.UMLFactory.eINSTANCE.createClass();
            	              						incompleteObjects.push(element);
            	              					}
            	              					if (a8_0 != null) {
            	              						if (a8_0 != null) {
            	              							Object value = a8_0;
            	              							addObjectToList(element, org.eclipse.uml2.uml.UMLPackage.CLASS__OWNED_ATTRIBUTE, value);
            	              							completedElement(value, true);
            	              						}
            	              						collectHiddenTokens(element);
            	              						retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_3_0_0_5_0_0_0, a8_0, true);
            	              						copyLocalizationInfos(a8_0, element);
            	              					}
            	              				
            	            }

            	            }

            	            if ( state.backtracking==0 ) {

            	              				// expected elements (follow set)
            	              				addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_11, 60, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_2, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_6);
            	              				addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_12, 60, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_2, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_6);
            	              				addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_44, 60, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_6);
            	              				addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_45, 60, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_7);
            	              				addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_46, 60);
            	              			
            	            }

            	            }
            	            break;
            	        case 2 :
            	            // Acm.g:1985:8: (a9_0= parse_org_eclipse_uml2_uml_Operation )
            	            {
            	            // Acm.g:1985:8: (a9_0= parse_org_eclipse_uml2_uml_Operation )
            	            // Acm.g:1986:5: a9_0= parse_org_eclipse_uml2_uml_Operation
            	            {
            	            pushFollow(FOLLOW_parse_org_eclipse_uml2_uml_Operation_in_parse_org_eclipse_uml2_uml_Class1773);
            	            a9_0=parse_org_eclipse_uml2_uml_Operation();

            	            state._fsp--;
            	            if (state.failed) return element;
            	            if ( state.backtracking==0 ) {

            	              					if (terminateParsing) {
            	              						throw new sare.language.acm.resource.acm.mopp.AcmTerminateParsingException();
            	              					}
            	              					if (element == null) {
            	              						element = org.eclipse.uml2.uml.UMLFactory.eINSTANCE.createClass();
            	              						incompleteObjects.push(element);
            	              					}
            	              					if (a9_0 != null) {
            	              						if (a9_0 != null) {
            	              							Object value = a9_0;
            	              							addObjectToList(element, org.eclipse.uml2.uml.UMLPackage.CLASS__OWNED_OPERATION, value);
            	              							completedElement(value, true);
            	              						}
            	              						collectHiddenTokens(element);
            	              						retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_3_0_0_5_0_1_0, a9_0, true);
            	              						copyLocalizationInfos(a9_0, element);
            	              					}
            	              				
            	            }

            	            }

            	            if ( state.backtracking==0 ) {

            	              				// expected elements (follow set)
            	              				addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_11, 61, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_2, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_6);
            	              				addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_12, 61, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_2, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_6);
            	              				addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_44, 61, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_6);
            	              				addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_45, 61, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_7);
            	              				addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_46, 61);
            	              			
            	            }

            	            }
            	            break;

            	    }


            	    }
            	    break;

            	default :
            	    break loop19;
                }
            } while (true);

            if ( state.backtracking==0 ) {

              		// expected elements (follow set)
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_11, 62, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_2, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_6);
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_12, 62, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_2, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_6);
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_44, 62, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_6);
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_45, 62, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_7);
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_46, 62);
              	
            }
            a10=(Token)match(input,12,FOLLOW_12_in_parse_org_eclipse_uml2_uml_Class1814); if (state.failed) return element;
            if ( state.backtracking==0 ) {

              		if (element == null) {
              			element = org.eclipse.uml2.uml.UMLFactory.eINSTANCE.createClass();
              			incompleteObjects.push(element);
              		}
              		collectHiddenTokens(element);
              		retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_3_0_0_6, null, true);
              		copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken)a10, element);
              	
            }
            if ( state.backtracking==0 ) {

              		// expected elements (follow set)
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_0, 63, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_1);
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_1, 63, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_1);
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_8, 63, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_1);
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_9, 63, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_1);
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_10, 63, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_1);
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_11, 63, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_2, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_1);
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_12, 63, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_2, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_1);
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_13, 63, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_1);
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_14, 63);
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_15, 63);
              	
            }

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
            if ( state.backtracking>0 ) { memoize(input, 9, parse_org_eclipse_uml2_uml_Class_StartIndex); }
        }
        return element;
    }
    // $ANTLR end "parse_org_eclipse_uml2_uml_Class"


    // $ANTLR start "parse_org_eclipse_uml2_uml_Operation"
    // Acm.g:2051:1: parse_org_eclipse_uml2_uml_Operation returns [org.eclipse.uml2.uml.Operation element = null] : ( (a0_0= parse_org_eclipse_uml2_uml_Comment ) )* (a1= TEXT ) a2= '(' ( ( (a3_0= parse_org_eclipse_uml2_uml_Parameter ) ( (a4= ',' (a5_0= parse_org_eclipse_uml2_uml_Parameter ) ) )* ) )? a6= ')' ( (a7= ':' (a8= TEXT ) ) )? ;
    public final org.eclipse.uml2.uml.Operation parse_org_eclipse_uml2_uml_Operation() throws RecognitionException {
        org.eclipse.uml2.uml.Operation element =  null;
        int parse_org_eclipse_uml2_uml_Operation_StartIndex = input.index();
        Token a1=null;
        Token a2=null;
        Token a4=null;
        Token a6=null;
        Token a7=null;
        Token a8=null;
        org.eclipse.uml2.uml.Comment a0_0 = null;

        org.eclipse.uml2.uml.Parameter a3_0 = null;

        org.eclipse.uml2.uml.Parameter a5_0 = null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 10) ) { return element; }
            // Acm.g:2054:1: ( ( (a0_0= parse_org_eclipse_uml2_uml_Comment ) )* (a1= TEXT ) a2= '(' ( ( (a3_0= parse_org_eclipse_uml2_uml_Parameter ) ( (a4= ',' (a5_0= parse_org_eclipse_uml2_uml_Parameter ) ) )* ) )? a6= ')' ( (a7= ':' (a8= TEXT ) ) )? )
            // Acm.g:2055:2: ( (a0_0= parse_org_eclipse_uml2_uml_Comment ) )* (a1= TEXT ) a2= '(' ( ( (a3_0= parse_org_eclipse_uml2_uml_Parameter ) ( (a4= ',' (a5_0= parse_org_eclipse_uml2_uml_Parameter ) ) )* ) )? a6= ')' ( (a7= ':' (a8= TEXT ) ) )?
            {
            // Acm.g:2055:2: ( (a0_0= parse_org_eclipse_uml2_uml_Comment ) )*
            loop20:
            do {
                int alt20=2;
                int LA20_0 = input.LA(1);

                if ( (LA20_0==25||LA20_0==27) ) {
                    alt20=1;
                }


                switch (alt20) {
            	case 1 :
            	    // Acm.g:2056:3: (a0_0= parse_org_eclipse_uml2_uml_Comment )
            	    {
            	    // Acm.g:2056:3: (a0_0= parse_org_eclipse_uml2_uml_Comment )
            	    // Acm.g:2057:4: a0_0= parse_org_eclipse_uml2_uml_Comment
            	    {
            	    pushFollow(FOLLOW_parse_org_eclipse_uml2_uml_Comment_in_parse_org_eclipse_uml2_uml_Operation1852);
            	    a0_0=parse_org_eclipse_uml2_uml_Comment();

            	    state._fsp--;
            	    if (state.failed) return element;
            	    if ( state.backtracking==0 ) {

            	      				if (terminateParsing) {
            	      					throw new sare.language.acm.resource.acm.mopp.AcmTerminateParsingException();
            	      				}
            	      				if (element == null) {
            	      					element = org.eclipse.uml2.uml.UMLFactory.eINSTANCE.createOperation();
            	      					incompleteObjects.push(element);
            	      				}
            	      				if (a0_0 != null) {
            	      					if (a0_0 != null) {
            	      						Object value = a0_0;
            	      						addObjectToList(element, org.eclipse.uml2.uml.UMLPackage.OPERATION__OWNED_COMMENT, value);
            	      						completedElement(value, true);
            	      					}
            	      					collectHiddenTokens(element);
            	      					retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_4_0_0_0, a0_0, true);
            	      					copyLocalizationInfos(a0_0, element);
            	      				}
            	      			
            	    }

            	    }


            	    }
            	    break;

            	default :
            	    break loop20;
                }
            } while (true);

            if ( state.backtracking==0 ) {

              		// expected elements (follow set)
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_11, 64, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_2);
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_12, 64, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_2);
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_45, 64);
              	
            }
            // Acm.g:2085:2: (a1= TEXT )
            // Acm.g:2086:3: a1= TEXT
            {
            a1=(Token)match(input,TEXT,FOLLOW_TEXT_in_parse_org_eclipse_uml2_uml_Operation1882); if (state.failed) return element;
            if ( state.backtracking==0 ) {

              			if (terminateParsing) {
              				throw new sare.language.acm.resource.acm.mopp.AcmTerminateParsingException();
              			}
              			if (element == null) {
              				element = org.eclipse.uml2.uml.UMLFactory.eINSTANCE.createOperation();
              				incompleteObjects.push(element);
              			}
              			if (a1 != null) {
              				sare.language.acm.resource.acm.IAcmTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
              				tokenResolver.setOptions(getOptions());
              				sare.language.acm.resource.acm.IAcmTokenResolveResult result = getFreshTokenResolveResult();
              				tokenResolver.resolve(a1.getText(), element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.OPERATION__NAME), result);
              				Object resolvedObject = result.getResolvedToken();
              				if (resolvedObject == null) {
              					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_3_0.CommonToken) a1).getLine(), ((org.antlr.runtime3_3_0.CommonToken) a1).getCharPositionInLine(), ((org.antlr.runtime3_3_0.CommonToken) a1).getStartIndex(), ((org.antlr.runtime3_3_0.CommonToken) a1).getStopIndex());
              				}
              				java.lang.String resolved = (java.lang.String) resolvedObject;
              				if (resolved != null) {
              					Object value = resolved;
              					element.eSet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.OPERATION__NAME), value);
              					completedElement(value, false);
              				}
              				collectHiddenTokens(element);
              				retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_4_0_0_1, resolved, true);
              				copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken) a1, element);
              			}
              		
            }

            }

            if ( state.backtracking==0 ) {

              		// expected elements (follow set)
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_47, 65);
              	
            }
            a2=(Token)match(input,22,FOLLOW_22_in_parse_org_eclipse_uml2_uml_Operation1903); if (state.failed) return element;
            if ( state.backtracking==0 ) {

              		if (element == null) {
              			element = org.eclipse.uml2.uml.UMLFactory.eINSTANCE.createOperation();
              			incompleteObjects.push(element);
              		}
              		collectHiddenTokens(element);
              		retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_4_0_0_2, null, true);
              		copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken)a2, element);
              	
            }
            if ( state.backtracking==0 ) {

              		// expected elements (follow set)
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_11, 66, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_2, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_8);
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_12, 66, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_2, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_8);
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_48, 66, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_8);
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_49, 66);
              	
            }
            // Acm.g:2138:2: ( ( (a3_0= parse_org_eclipse_uml2_uml_Parameter ) ( (a4= ',' (a5_0= parse_org_eclipse_uml2_uml_Parameter ) ) )* ) )?
            int alt22=2;
            int LA22_0 = input.LA(1);

            if ( (LA22_0==TEXT||LA22_0==25||LA22_0==27) ) {
                alt22=1;
            }
            switch (alt22) {
                case 1 :
                    // Acm.g:2139:3: ( (a3_0= parse_org_eclipse_uml2_uml_Parameter ) ( (a4= ',' (a5_0= parse_org_eclipse_uml2_uml_Parameter ) ) )* )
                    {
                    // Acm.g:2139:3: ( (a3_0= parse_org_eclipse_uml2_uml_Parameter ) ( (a4= ',' (a5_0= parse_org_eclipse_uml2_uml_Parameter ) ) )* )
                    // Acm.g:2140:4: (a3_0= parse_org_eclipse_uml2_uml_Parameter ) ( (a4= ',' (a5_0= parse_org_eclipse_uml2_uml_Parameter ) ) )*
                    {
                    // Acm.g:2140:4: (a3_0= parse_org_eclipse_uml2_uml_Parameter )
                    // Acm.g:2141:5: a3_0= parse_org_eclipse_uml2_uml_Parameter
                    {
                    pushFollow(FOLLOW_parse_org_eclipse_uml2_uml_Parameter_in_parse_org_eclipse_uml2_uml_Operation1932);
                    a3_0=parse_org_eclipse_uml2_uml_Parameter();

                    state._fsp--;
                    if (state.failed) return element;
                    if ( state.backtracking==0 ) {

                      					if (terminateParsing) {
                      						throw new sare.language.acm.resource.acm.mopp.AcmTerminateParsingException();
                      					}
                      					if (element == null) {
                      						element = org.eclipse.uml2.uml.UMLFactory.eINSTANCE.createOperation();
                      						incompleteObjects.push(element);
                      					}
                      					if (a3_0 != null) {
                      						if (a3_0 != null) {
                      							Object value = a3_0;
                      							addObjectToList(element, org.eclipse.uml2.uml.UMLPackage.OPERATION__OWNED_PARAMETER, value);
                      							completedElement(value, true);
                      						}
                      						collectHiddenTokens(element);
                      						retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_4_0_0_3_0_0_0, a3_0, true);
                      						copyLocalizationInfos(a3_0, element);
                      					}
                      				
                    }

                    }

                    if ( state.backtracking==0 ) {

                      				// expected elements (follow set)
                      				addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_50, 67);
                      				addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_49, 67);
                      			
                    }
                    // Acm.g:2167:4: ( (a4= ',' (a5_0= parse_org_eclipse_uml2_uml_Parameter ) ) )*
                    loop21:
                    do {
                        int alt21=2;
                        int LA21_0 = input.LA(1);

                        if ( (LA21_0==15) ) {
                            alt21=1;
                        }


                        switch (alt21) {
                    	case 1 :
                    	    // Acm.g:2168:5: (a4= ',' (a5_0= parse_org_eclipse_uml2_uml_Parameter ) )
                    	    {
                    	    // Acm.g:2168:5: (a4= ',' (a5_0= parse_org_eclipse_uml2_uml_Parameter ) )
                    	    // Acm.g:2169:6: a4= ',' (a5_0= parse_org_eclipse_uml2_uml_Parameter )
                    	    {
                    	    a4=(Token)match(input,15,FOLLOW_15_in_parse_org_eclipse_uml2_uml_Operation1973); if (state.failed) return element;
                    	    if ( state.backtracking==0 ) {

                    	      						if (element == null) {
                    	      							element = org.eclipse.uml2.uml.UMLFactory.eINSTANCE.createOperation();
                    	      							incompleteObjects.push(element);
                    	      						}
                    	      						collectHiddenTokens(element);
                    	      						retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_4_0_0_3_0_0_1_0_0_0, null, true);
                    	      						copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken)a4, element);
                    	      					
                    	    }
                    	    if ( state.backtracking==0 ) {

                    	      						// expected elements (follow set)
                    	      						addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_11, 68, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_2, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_8);
                    	      						addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_12, 68, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_2, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_8);
                    	      						addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_48, 68, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_8);
                    	      					
                    	    }
                    	    // Acm.g:2185:6: (a5_0= parse_org_eclipse_uml2_uml_Parameter )
                    	    // Acm.g:2186:7: a5_0= parse_org_eclipse_uml2_uml_Parameter
                    	    {
                    	    pushFollow(FOLLOW_parse_org_eclipse_uml2_uml_Parameter_in_parse_org_eclipse_uml2_uml_Operation2007);
                    	    a5_0=parse_org_eclipse_uml2_uml_Parameter();

                    	    state._fsp--;
                    	    if (state.failed) return element;
                    	    if ( state.backtracking==0 ) {

                    	      							if (terminateParsing) {
                    	      								throw new sare.language.acm.resource.acm.mopp.AcmTerminateParsingException();
                    	      							}
                    	      							if (element == null) {
                    	      								element = org.eclipse.uml2.uml.UMLFactory.eINSTANCE.createOperation();
                    	      								incompleteObjects.push(element);
                    	      							}
                    	      							if (a5_0 != null) {
                    	      								if (a5_0 != null) {
                    	      									Object value = a5_0;
                    	      									addObjectToList(element, org.eclipse.uml2.uml.UMLPackage.OPERATION__OWNED_PARAMETER, value);
                    	      									completedElement(value, true);
                    	      								}
                    	      								collectHiddenTokens(element);
                    	      								retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_4_0_0_3_0_0_1_0_0_1, a5_0, true);
                    	      								copyLocalizationInfos(a5_0, element);
                    	      							}
                    	      						
                    	    }

                    	    }

                    	    if ( state.backtracking==0 ) {

                    	      						// expected elements (follow set)
                    	      						addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_50, 69);
                    	      						addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_49, 69);
                    	      					
                    	    }

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop21;
                        }
                    } while (true);

                    if ( state.backtracking==0 ) {

                      				// expected elements (follow set)
                      				addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_50, 70);
                      				addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_49, 70);
                      			
                    }

                    }


                    }
                    break;

            }

            if ( state.backtracking==0 ) {

              		// expected elements (follow set)
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_49, 71);
              	
            }
            a6=(Token)match(input,23,FOLLOW_23_in_parse_org_eclipse_uml2_uml_Operation2081); if (state.failed) return element;
            if ( state.backtracking==0 ) {

              		if (element == null) {
              			element = org.eclipse.uml2.uml.UMLFactory.eINSTANCE.createOperation();
              			incompleteObjects.push(element);
              		}
              		collectHiddenTokens(element);
              		retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_4_0_0_4, null, true);
              		copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken)a6, element);
              	
            }
            if ( state.backtracking==0 ) {

              		// expected elements (follow set)
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_51, 72);
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_11, 72, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_2, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_6);
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_12, 72, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_2, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_6);
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_44, 72, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_6);
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_45, 72, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_7);
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_46, 72);
              	
            }
            // Acm.g:2246:2: ( (a7= ':' (a8= TEXT ) ) )?
            int alt23=2;
            int LA23_0 = input.LA(1);

            if ( (LA23_0==24) ) {
                alt23=1;
            }
            switch (alt23) {
                case 1 :
                    // Acm.g:2247:3: (a7= ':' (a8= TEXT ) )
                    {
                    // Acm.g:2247:3: (a7= ':' (a8= TEXT ) )
                    // Acm.g:2248:4: a7= ':' (a8= TEXT )
                    {
                    a7=(Token)match(input,24,FOLLOW_24_in_parse_org_eclipse_uml2_uml_Operation2104); if (state.failed) return element;
                    if ( state.backtracking==0 ) {

                      				if (element == null) {
                      					element = org.eclipse.uml2.uml.UMLFactory.eINSTANCE.createOperation();
                      					incompleteObjects.push(element);
                      				}
                      				collectHiddenTokens(element);
                      				retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_4_0_0_5_0_0_0, null, true);
                      				copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken)a7, element);
                      			
                    }
                    if ( state.backtracking==0 ) {

                      				// expected elements (follow set)
                      				addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_52, 73);
                      			
                    }
                    // Acm.g:2262:4: (a8= TEXT )
                    // Acm.g:2263:5: a8= TEXT
                    {
                    a8=(Token)match(input,TEXT,FOLLOW_TEXT_in_parse_org_eclipse_uml2_uml_Operation2130); if (state.failed) return element;
                    if ( state.backtracking==0 ) {

                      					if (terminateParsing) {
                      						throw new sare.language.acm.resource.acm.mopp.AcmTerminateParsingException();
                      					}
                      					if (element == null) {
                      						element = org.eclipse.uml2.uml.UMLFactory.eINSTANCE.createOperation();
                      						incompleteObjects.push(element);
                      					}
                      					if (a8 != null) {
                      						sare.language.acm.resource.acm.IAcmTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
                      						tokenResolver.setOptions(getOptions());
                      						sare.language.acm.resource.acm.IAcmTokenResolveResult result = getFreshTokenResolveResult();
                      						tokenResolver.resolve(a8.getText(), element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.OPERATION__TYPE), result);
                      						Object resolvedObject = result.getResolvedToken();
                      						if (resolvedObject == null) {
                      							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_3_0.CommonToken) a8).getLine(), ((org.antlr.runtime3_3_0.CommonToken) a8).getCharPositionInLine(), ((org.antlr.runtime3_3_0.CommonToken) a8).getStartIndex(), ((org.antlr.runtime3_3_0.CommonToken) a8).getStopIndex());
                      						}
                      						String resolved = (String) resolvedObject;
                      						org.eclipse.uml2.uml.Type proxy = org.eclipse.uml2.uml.UMLFactory.eINSTANCE.createAssociation();
                      						collectHiddenTokens(element);
                      						registerContextDependentProxy(new sare.language.acm.resource.acm.mopp.AcmContextDependentURIFragmentFactory<org.eclipse.uml2.uml.Operation, org.eclipse.uml2.uml.Type>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getOperationTypeReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.OPERATION__TYPE), resolved, proxy);
                      						if (proxy != null) {
                      							Object value = proxy;
                      							element.eSet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.OPERATION__TYPE), value);
                      							completedElement(value, false);
                      						}
                      						collectHiddenTokens(element);
                      						retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_4_0_0_5_0_0_1, proxy, true);
                      						copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken) a8, element);
                      						copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken) a8, proxy);
                      					}
                      				
                    }

                    }

                    if ( state.backtracking==0 ) {

                      				// expected elements (follow set)
                      				addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_11, 74, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_2, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_6);
                      				addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_12, 74, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_2, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_6);
                      				addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_44, 74, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_6);
                      				addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_45, 74, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_7);
                      				addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_46, 74);
                      			
                    }

                    }


                    }
                    break;

            }

            if ( state.backtracking==0 ) {

              		// expected elements (follow set)
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_11, 75, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_2, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_6);
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_12, 75, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_2, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_6);
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_44, 75, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_6);
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_45, 75, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_7);
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_46, 75);
              	
            }

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
            if ( state.backtracking>0 ) { memoize(input, 10, parse_org_eclipse_uml2_uml_Operation_StartIndex); }
        }
        return element;
    }
    // $ANTLR end "parse_org_eclipse_uml2_uml_Operation"


    // $ANTLR start "parse_org_eclipse_uml2_uml_Property"
    // Acm.g:2319:1: parse_org_eclipse_uml2_uml_Property returns [org.eclipse.uml2.uml.Property element = null] : ( (a0_0= parse_org_eclipse_uml2_uml_Comment ) )* (a1= TEXT ) a2= ':' (a3= TEXT ) ;
    public final org.eclipse.uml2.uml.Property parse_org_eclipse_uml2_uml_Property() throws RecognitionException {
        org.eclipse.uml2.uml.Property element =  null;
        int parse_org_eclipse_uml2_uml_Property_StartIndex = input.index();
        Token a1=null;
        Token a2=null;
        Token a3=null;
        org.eclipse.uml2.uml.Comment a0_0 = null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 11) ) { return element; }
            // Acm.g:2322:1: ( ( (a0_0= parse_org_eclipse_uml2_uml_Comment ) )* (a1= TEXT ) a2= ':' (a3= TEXT ) )
            // Acm.g:2323:2: ( (a0_0= parse_org_eclipse_uml2_uml_Comment ) )* (a1= TEXT ) a2= ':' (a3= TEXT )
            {
            // Acm.g:2323:2: ( (a0_0= parse_org_eclipse_uml2_uml_Comment ) )*
            loop24:
            do {
                int alt24=2;
                int LA24_0 = input.LA(1);

                if ( (LA24_0==25||LA24_0==27) ) {
                    alt24=1;
                }


                switch (alt24) {
            	case 1 :
            	    // Acm.g:2324:3: (a0_0= parse_org_eclipse_uml2_uml_Comment )
            	    {
            	    // Acm.g:2324:3: (a0_0= parse_org_eclipse_uml2_uml_Comment )
            	    // Acm.g:2325:4: a0_0= parse_org_eclipse_uml2_uml_Comment
            	    {
            	    pushFollow(FOLLOW_parse_org_eclipse_uml2_uml_Comment_in_parse_org_eclipse_uml2_uml_Property2200);
            	    a0_0=parse_org_eclipse_uml2_uml_Comment();

            	    state._fsp--;
            	    if (state.failed) return element;
            	    if ( state.backtracking==0 ) {

            	      				if (terminateParsing) {
            	      					throw new sare.language.acm.resource.acm.mopp.AcmTerminateParsingException();
            	      				}
            	      				if (element == null) {
            	      					element = org.eclipse.uml2.uml.UMLFactory.eINSTANCE.createProperty();
            	      					incompleteObjects.push(element);
            	      				}
            	      				if (a0_0 != null) {
            	      					if (a0_0 != null) {
            	      						Object value = a0_0;
            	      						addObjectToList(element, org.eclipse.uml2.uml.UMLPackage.PROPERTY__OWNED_COMMENT, value);
            	      						completedElement(value, true);
            	      					}
            	      					collectHiddenTokens(element);
            	      					retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_5_0_0_0, a0_0, true);
            	      					copyLocalizationInfos(a0_0, element);
            	      				}
            	      			
            	    }

            	    }


            	    }
            	    break;

            	default :
            	    break loop24;
                }
            } while (true);

            if ( state.backtracking==0 ) {

              		// expected elements (follow set)
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_11, 76, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_2);
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_12, 76, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_2);
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_44, 76);
              	
            }
            // Acm.g:2353:2: (a1= TEXT )
            // Acm.g:2354:3: a1= TEXT
            {
            a1=(Token)match(input,TEXT,FOLLOW_TEXT_in_parse_org_eclipse_uml2_uml_Property2230); if (state.failed) return element;
            if ( state.backtracking==0 ) {

              			if (terminateParsing) {
              				throw new sare.language.acm.resource.acm.mopp.AcmTerminateParsingException();
              			}
              			if (element == null) {
              				element = org.eclipse.uml2.uml.UMLFactory.eINSTANCE.createProperty();
              				incompleteObjects.push(element);
              			}
              			if (a1 != null) {
              				sare.language.acm.resource.acm.IAcmTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
              				tokenResolver.setOptions(getOptions());
              				sare.language.acm.resource.acm.IAcmTokenResolveResult result = getFreshTokenResolveResult();
              				tokenResolver.resolve(a1.getText(), element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.PROPERTY__NAME), result);
              				Object resolvedObject = result.getResolvedToken();
              				if (resolvedObject == null) {
              					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_3_0.CommonToken) a1).getLine(), ((org.antlr.runtime3_3_0.CommonToken) a1).getCharPositionInLine(), ((org.antlr.runtime3_3_0.CommonToken) a1).getStartIndex(), ((org.antlr.runtime3_3_0.CommonToken) a1).getStopIndex());
              				}
              				java.lang.String resolved = (java.lang.String) resolvedObject;
              				if (resolved != null) {
              					Object value = resolved;
              					element.eSet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.PROPERTY__NAME), value);
              					completedElement(value, false);
              				}
              				collectHiddenTokens(element);
              				retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_5_0_0_1, resolved, true);
              				copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken) a1, element);
              			}
              		
            }

            }

            if ( state.backtracking==0 ) {

              		// expected elements (follow set)
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_53, 77);
              	
            }
            a2=(Token)match(input,24,FOLLOW_24_in_parse_org_eclipse_uml2_uml_Property2251); if (state.failed) return element;
            if ( state.backtracking==0 ) {

              		if (element == null) {
              			element = org.eclipse.uml2.uml.UMLFactory.eINSTANCE.createProperty();
              			incompleteObjects.push(element);
              		}
              		collectHiddenTokens(element);
              		retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_5_0_0_2, null, true);
              		copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken)a2, element);
              	
            }
            if ( state.backtracking==0 ) {

              		// expected elements (follow set)
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_54, 78);
              	
            }
            // Acm.g:2403:2: (a3= TEXT )
            // Acm.g:2404:3: a3= TEXT
            {
            a3=(Token)match(input,TEXT,FOLLOW_TEXT_in_parse_org_eclipse_uml2_uml_Property2269); if (state.failed) return element;
            if ( state.backtracking==0 ) {

              			if (terminateParsing) {
              				throw new sare.language.acm.resource.acm.mopp.AcmTerminateParsingException();
              			}
              			if (element == null) {
              				element = org.eclipse.uml2.uml.UMLFactory.eINSTANCE.createProperty();
              				incompleteObjects.push(element);
              			}
              			if (a3 != null) {
              				sare.language.acm.resource.acm.IAcmTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
              				tokenResolver.setOptions(getOptions());
              				sare.language.acm.resource.acm.IAcmTokenResolveResult result = getFreshTokenResolveResult();
              				tokenResolver.resolve(a3.getText(), element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.PROPERTY__TYPE), result);
              				Object resolvedObject = result.getResolvedToken();
              				if (resolvedObject == null) {
              					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_3_0.CommonToken) a3).getLine(), ((org.antlr.runtime3_3_0.CommonToken) a3).getCharPositionInLine(), ((org.antlr.runtime3_3_0.CommonToken) a3).getStartIndex(), ((org.antlr.runtime3_3_0.CommonToken) a3).getStopIndex());
              				}
              				String resolved = (String) resolvedObject;
              				org.eclipse.uml2.uml.Type proxy = org.eclipse.uml2.uml.UMLFactory.eINSTANCE.createAssociation();
              				collectHiddenTokens(element);
              				registerContextDependentProxy(new sare.language.acm.resource.acm.mopp.AcmContextDependentURIFragmentFactory<org.eclipse.uml2.uml.TypedElement, org.eclipse.uml2.uml.Type>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getTypedElementTypeReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.PROPERTY__TYPE), resolved, proxy);
              				if (proxy != null) {
              					Object value = proxy;
              					element.eSet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.PROPERTY__TYPE), value);
              					completedElement(value, false);
              				}
              				collectHiddenTokens(element);
              				retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_5_0_0_3, proxy, true);
              				copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken) a3, element);
              				copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken) a3, proxy);
              			}
              		
            }

            }

            if ( state.backtracking==0 ) {

              		// expected elements (follow set)
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_11, 79, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_2, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_6);
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_12, 79, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_2, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_6);
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_44, 79, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_6);
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_45, 79, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_7);
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_46, 79);
              	
            }

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
            if ( state.backtracking>0 ) { memoize(input, 11, parse_org_eclipse_uml2_uml_Property_StartIndex); }
        }
        return element;
    }
    // $ANTLR end "parse_org_eclipse_uml2_uml_Property"


    // $ANTLR start "parse_org_eclipse_uml2_uml_Parameter"
    // Acm.g:2449:1: parse_org_eclipse_uml2_uml_Parameter returns [org.eclipse.uml2.uml.Parameter element = null] : ( (a0_0= parse_org_eclipse_uml2_uml_Comment ) )* (a1= TEXT ) a2= ':' (a3= TEXT ) ;
    public final org.eclipse.uml2.uml.Parameter parse_org_eclipse_uml2_uml_Parameter() throws RecognitionException {
        org.eclipse.uml2.uml.Parameter element =  null;
        int parse_org_eclipse_uml2_uml_Parameter_StartIndex = input.index();
        Token a1=null;
        Token a2=null;
        Token a3=null;
        org.eclipse.uml2.uml.Comment a0_0 = null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 12) ) { return element; }
            // Acm.g:2452:1: ( ( (a0_0= parse_org_eclipse_uml2_uml_Comment ) )* (a1= TEXT ) a2= ':' (a3= TEXT ) )
            // Acm.g:2453:2: ( (a0_0= parse_org_eclipse_uml2_uml_Comment ) )* (a1= TEXT ) a2= ':' (a3= TEXT )
            {
            // Acm.g:2453:2: ( (a0_0= parse_org_eclipse_uml2_uml_Comment ) )*
            loop25:
            do {
                int alt25=2;
                int LA25_0 = input.LA(1);

                if ( (LA25_0==25||LA25_0==27) ) {
                    alt25=1;
                }


                switch (alt25) {
            	case 1 :
            	    // Acm.g:2454:3: (a0_0= parse_org_eclipse_uml2_uml_Comment )
            	    {
            	    // Acm.g:2454:3: (a0_0= parse_org_eclipse_uml2_uml_Comment )
            	    // Acm.g:2455:4: a0_0= parse_org_eclipse_uml2_uml_Comment
            	    {
            	    pushFollow(FOLLOW_parse_org_eclipse_uml2_uml_Comment_in_parse_org_eclipse_uml2_uml_Parameter2314);
            	    a0_0=parse_org_eclipse_uml2_uml_Comment();

            	    state._fsp--;
            	    if (state.failed) return element;
            	    if ( state.backtracking==0 ) {

            	      				if (terminateParsing) {
            	      					throw new sare.language.acm.resource.acm.mopp.AcmTerminateParsingException();
            	      				}
            	      				if (element == null) {
            	      					element = org.eclipse.uml2.uml.UMLFactory.eINSTANCE.createParameter();
            	      					incompleteObjects.push(element);
            	      				}
            	      				if (a0_0 != null) {
            	      					if (a0_0 != null) {
            	      						Object value = a0_0;
            	      						addObjectToList(element, org.eclipse.uml2.uml.UMLPackage.PARAMETER__OWNED_COMMENT, value);
            	      						completedElement(value, true);
            	      					}
            	      					collectHiddenTokens(element);
            	      					retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_6_0_0_0, a0_0, true);
            	      					copyLocalizationInfos(a0_0, element);
            	      				}
            	      			
            	    }

            	    }


            	    }
            	    break;

            	default :
            	    break loop25;
                }
            } while (true);

            if ( state.backtracking==0 ) {

              		// expected elements (follow set)
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_11, 80, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_2);
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_12, 80, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_2);
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_48, 80);
              	
            }
            // Acm.g:2483:2: (a1= TEXT )
            // Acm.g:2484:3: a1= TEXT
            {
            a1=(Token)match(input,TEXT,FOLLOW_TEXT_in_parse_org_eclipse_uml2_uml_Parameter2344); if (state.failed) return element;
            if ( state.backtracking==0 ) {

              			if (terminateParsing) {
              				throw new sare.language.acm.resource.acm.mopp.AcmTerminateParsingException();
              			}
              			if (element == null) {
              				element = org.eclipse.uml2.uml.UMLFactory.eINSTANCE.createParameter();
              				incompleteObjects.push(element);
              			}
              			if (a1 != null) {
              				sare.language.acm.resource.acm.IAcmTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
              				tokenResolver.setOptions(getOptions());
              				sare.language.acm.resource.acm.IAcmTokenResolveResult result = getFreshTokenResolveResult();
              				tokenResolver.resolve(a1.getText(), element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.PARAMETER__NAME), result);
              				Object resolvedObject = result.getResolvedToken();
              				if (resolvedObject == null) {
              					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_3_0.CommonToken) a1).getLine(), ((org.antlr.runtime3_3_0.CommonToken) a1).getCharPositionInLine(), ((org.antlr.runtime3_3_0.CommonToken) a1).getStartIndex(), ((org.antlr.runtime3_3_0.CommonToken) a1).getStopIndex());
              				}
              				java.lang.String resolved = (java.lang.String) resolvedObject;
              				if (resolved != null) {
              					Object value = resolved;
              					element.eSet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.PARAMETER__NAME), value);
              					completedElement(value, false);
              				}
              				collectHiddenTokens(element);
              				retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_6_0_0_1, resolved, true);
              				copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken) a1, element);
              			}
              		
            }

            }

            if ( state.backtracking==0 ) {

              		// expected elements (follow set)
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_55, 81);
              	
            }
            a2=(Token)match(input,24,FOLLOW_24_in_parse_org_eclipse_uml2_uml_Parameter2365); if (state.failed) return element;
            if ( state.backtracking==0 ) {

              		if (element == null) {
              			element = org.eclipse.uml2.uml.UMLFactory.eINSTANCE.createParameter();
              			incompleteObjects.push(element);
              		}
              		collectHiddenTokens(element);
              		retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_6_0_0_2, null, true);
              		copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken)a2, element);
              	
            }
            if ( state.backtracking==0 ) {

              		// expected elements (follow set)
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_56, 82);
              	
            }
            // Acm.g:2533:2: (a3= TEXT )
            // Acm.g:2534:3: a3= TEXT
            {
            a3=(Token)match(input,TEXT,FOLLOW_TEXT_in_parse_org_eclipse_uml2_uml_Parameter2383); if (state.failed) return element;
            if ( state.backtracking==0 ) {

              			if (terminateParsing) {
              				throw new sare.language.acm.resource.acm.mopp.AcmTerminateParsingException();
              			}
              			if (element == null) {
              				element = org.eclipse.uml2.uml.UMLFactory.eINSTANCE.createParameter();
              				incompleteObjects.push(element);
              			}
              			if (a3 != null) {
              				sare.language.acm.resource.acm.IAcmTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
              				tokenResolver.setOptions(getOptions());
              				sare.language.acm.resource.acm.IAcmTokenResolveResult result = getFreshTokenResolveResult();
              				tokenResolver.resolve(a3.getText(), element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.PARAMETER__TYPE), result);
              				Object resolvedObject = result.getResolvedToken();
              				if (resolvedObject == null) {
              					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_3_0.CommonToken) a3).getLine(), ((org.antlr.runtime3_3_0.CommonToken) a3).getCharPositionInLine(), ((org.antlr.runtime3_3_0.CommonToken) a3).getStartIndex(), ((org.antlr.runtime3_3_0.CommonToken) a3).getStopIndex());
              				}
              				String resolved = (String) resolvedObject;
              				org.eclipse.uml2.uml.Type proxy = org.eclipse.uml2.uml.UMLFactory.eINSTANCE.createAssociation();
              				collectHiddenTokens(element);
              				registerContextDependentProxy(new sare.language.acm.resource.acm.mopp.AcmContextDependentURIFragmentFactory<org.eclipse.uml2.uml.TypedElement, org.eclipse.uml2.uml.Type>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getTypedElementTypeReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.PARAMETER__TYPE), resolved, proxy);
              				if (proxy != null) {
              					Object value = proxy;
              					element.eSet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.PARAMETER__TYPE), value);
              					completedElement(value, false);
              				}
              				collectHiddenTokens(element);
              				retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_6_0_0_3, proxy, true);
              				copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken) a3, element);
              				copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken) a3, proxy);
              			}
              		
            }

            }

            if ( state.backtracking==0 ) {

              		// expected elements (follow set)
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_50, 83);
              		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_49, 83);
              	
            }

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
            if ( state.backtracking>0 ) { memoize(input, 12, parse_org_eclipse_uml2_uml_Parameter_StartIndex); }
        }
        return element;
    }
    // $ANTLR end "parse_org_eclipse_uml2_uml_Parameter"


    // $ANTLR start "parse_org_eclipse_uml2_uml_Comment"
    // Acm.g:2576:1: parse_org_eclipse_uml2_uml_Comment returns [org.eclipse.uml2.uml.Comment element = null] : (a0= '\\u00ab' (a1= TEXT ) a2= '\\u00bb' | a3= '<<' (a4= TEXT ) a5= '>>' );
    public final org.eclipse.uml2.uml.Comment parse_org_eclipse_uml2_uml_Comment() throws RecognitionException {
        org.eclipse.uml2.uml.Comment element =  null;
        int parse_org_eclipse_uml2_uml_Comment_StartIndex = input.index();
        Token a0=null;
        Token a1=null;
        Token a2=null;
        Token a3=null;
        Token a4=null;
        Token a5=null;



        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 13) ) { return element; }
            // Acm.g:2579:1: (a0= '\\u00ab' (a1= TEXT ) a2= '\\u00bb' | a3= '<<' (a4= TEXT ) a5= '>>' )
            int alt26=2;
            int LA26_0 = input.LA(1);

            if ( (LA26_0==25) ) {
                alt26=1;
            }
            else if ( (LA26_0==27) ) {
                alt26=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return element;}
                NoViableAltException nvae =
                    new NoViableAltException("", 26, 0, input);

                throw nvae;
            }
            switch (alt26) {
                case 1 :
                    // Acm.g:2580:2: a0= '\\u00ab' (a1= TEXT ) a2= '\\u00bb'
                    {
                    a0=(Token)match(input,25,FOLLOW_25_in_parse_org_eclipse_uml2_uml_Comment2419); if (state.failed) return element;
                    if ( state.backtracking==0 ) {

                      		if (element == null) {
                      			element = org.eclipse.uml2.uml.UMLFactory.eINSTANCE.createComment();
                      			incompleteObjects.push(element);
                      		}
                      		collectHiddenTokens(element);
                      		retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_7_0_0_0, null, true);
                      		copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken)a0, element);
                      	
                    }
                    if ( state.backtracking==0 ) {

                      		// expected elements (follow set)
                      		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_57, 84);
                      	
                    }
                    // Acm.g:2594:2: (a1= TEXT )
                    // Acm.g:2595:3: a1= TEXT
                    {
                    a1=(Token)match(input,TEXT,FOLLOW_TEXT_in_parse_org_eclipse_uml2_uml_Comment2437); if (state.failed) return element;
                    if ( state.backtracking==0 ) {

                      			if (terminateParsing) {
                      				throw new sare.language.acm.resource.acm.mopp.AcmTerminateParsingException();
                      			}
                      			if (element == null) {
                      				element = org.eclipse.uml2.uml.UMLFactory.eINSTANCE.createComment();
                      				incompleteObjects.push(element);
                      			}
                      			if (a1 != null) {
                      				sare.language.acm.resource.acm.IAcmTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
                      				tokenResolver.setOptions(getOptions());
                      				sare.language.acm.resource.acm.IAcmTokenResolveResult result = getFreshTokenResolveResult();
                      				tokenResolver.resolve(a1.getText(), element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.COMMENT__BODY), result);
                      				Object resolvedObject = result.getResolvedToken();
                      				if (resolvedObject == null) {
                      					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_3_0.CommonToken) a1).getLine(), ((org.antlr.runtime3_3_0.CommonToken) a1).getCharPositionInLine(), ((org.antlr.runtime3_3_0.CommonToken) a1).getStartIndex(), ((org.antlr.runtime3_3_0.CommonToken) a1).getStopIndex());
                      				}
                      				java.lang.String resolved = (java.lang.String) resolvedObject;
                      				if (resolved != null) {
                      					Object value = resolved;
                      					element.eSet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.COMMENT__BODY), value);
                      					completedElement(value, false);
                      				}
                      				collectHiddenTokens(element);
                      				retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_7_0_0_1, resolved, true);
                      				copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken) a1, element);
                      			}
                      		
                    }

                    }

                    if ( state.backtracking==0 ) {

                      		// expected elements (follow set)
                      		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_58, 85);
                      	
                    }
                    a2=(Token)match(input,26,FOLLOW_26_in_parse_org_eclipse_uml2_uml_Comment2458); if (state.failed) return element;
                    if ( state.backtracking==0 ) {

                      		if (element == null) {
                      			element = org.eclipse.uml2.uml.UMLFactory.eINSTANCE.createComment();
                      			incompleteObjects.push(element);
                      		}
                      		collectHiddenTokens(element);
                      		retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_7_0_0_2, null, true);
                      		copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken)a2, element);
                      	
                    }
                    if ( state.backtracking==0 ) {

                      		// expected elements (follow set)
                      		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_11, 86, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_2);
                      		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_12, 86, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_2);
                      		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_13, 86);
                      		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_45, 86);
                      		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_44, 86);
                      		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_48, 86);
                      	
                    }

                    }
                    break;
                case 2 :
                    // Acm.g:2650:4: a3= '<<' (a4= TEXT ) a5= '>>'
                    {
                    a3=(Token)match(input,27,FOLLOW_27_in_parse_org_eclipse_uml2_uml_Comment2476); if (state.failed) return element;
                    if ( state.backtracking==0 ) {

                      		if (element == null) {
                      			element = org.eclipse.uml2.uml.UMLFactory.eINSTANCE.createComment();
                      			incompleteObjects.push(element);
                      		}
                      		collectHiddenTokens(element);
                      		retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_7_0_1_0, null, true);
                      		copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken)a3, element);
                      	
                    }
                    if ( state.backtracking==0 ) {

                      		// expected elements (follow set)
                      		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_59, 87);
                      	
                    }
                    // Acm.g:2664:2: (a4= TEXT )
                    // Acm.g:2665:3: a4= TEXT
                    {
                    a4=(Token)match(input,TEXT,FOLLOW_TEXT_in_parse_org_eclipse_uml2_uml_Comment2494); if (state.failed) return element;
                    if ( state.backtracking==0 ) {

                      			if (terminateParsing) {
                      				throw new sare.language.acm.resource.acm.mopp.AcmTerminateParsingException();
                      			}
                      			if (element == null) {
                      				element = org.eclipse.uml2.uml.UMLFactory.eINSTANCE.createComment();
                      				incompleteObjects.push(element);
                      			}
                      			if (a4 != null) {
                      				sare.language.acm.resource.acm.IAcmTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
                      				tokenResolver.setOptions(getOptions());
                      				sare.language.acm.resource.acm.IAcmTokenResolveResult result = getFreshTokenResolveResult();
                      				tokenResolver.resolve(a4.getText(), element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.COMMENT__BODY), result);
                      				Object resolvedObject = result.getResolvedToken();
                      				if (resolvedObject == null) {
                      					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_3_0.CommonToken) a4).getLine(), ((org.antlr.runtime3_3_0.CommonToken) a4).getCharPositionInLine(), ((org.antlr.runtime3_3_0.CommonToken) a4).getStartIndex(), ((org.antlr.runtime3_3_0.CommonToken) a4).getStopIndex());
                      				}
                      				java.lang.String resolved = (java.lang.String) resolvedObject;
                      				if (resolved != null) {
                      					Object value = resolved;
                      					element.eSet(element.eClass().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.COMMENT__BODY), value);
                      					completedElement(value, false);
                      				}
                      				collectHiddenTokens(element);
                      				retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_7_0_1_1, resolved, true);
                      				copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken) a4, element);
                      			}
                      		
                    }

                    }

                    if ( state.backtracking==0 ) {

                      		// expected elements (follow set)
                      		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_60, 88);
                      	
                    }
                    a5=(Token)match(input,28,FOLLOW_28_in_parse_org_eclipse_uml2_uml_Comment2515); if (state.failed) return element;
                    if ( state.backtracking==0 ) {

                      		if (element == null) {
                      			element = org.eclipse.uml2.uml.UMLFactory.eINSTANCE.createComment();
                      			incompleteObjects.push(element);
                      		}
                      		collectHiddenTokens(element);
                      		retrieveLayoutInformation(element, sare.language.acm.resource.acm.grammar.AcmGrammarInformationProvider.MINIUML_7_0_1_2, null, true);
                      		copyLocalizationInfos((org.antlr.runtime3_3_0.CommonToken)a5, element);
                      	
                    }
                    if ( state.backtracking==0 ) {

                      		// expected elements (follow set)
                      		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_11, 89, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_2);
                      		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_12, 89, sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.FEATURE_2);
                      		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_13, 89);
                      		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_45, 89);
                      		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_44, 89);
                      		addExpectedElement(sare.language.acm.resource.acm.grammar.AcmFollowSetProvider.TERMINAL_48, 89);
                      	
                    }

                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
            if ( state.backtracking>0 ) { memoize(input, 13, parse_org_eclipse_uml2_uml_Comment_StartIndex); }
        }
        return element;
    }
    // $ANTLR end "parse_org_eclipse_uml2_uml_Comment"


    // $ANTLR start "parse_sare_language_acm_Subject"
    // Acm.g:2721:1: parse_sare_language_acm_Subject returns [sare.language.acm.Subject element = null] : (c0= parse_sare_language_acm_Role | c1= parse_sare_language_acm_User );
    public final sare.language.acm.Subject parse_sare_language_acm_Subject() throws RecognitionException {
        sare.language.acm.Subject element =  null;
        int parse_sare_language_acm_Subject_StartIndex = input.index();
        sare.language.acm.Role c0 = null;

        sare.language.acm.User c1 = null;


        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 14) ) { return element; }
            // Acm.g:2722:1: (c0= parse_sare_language_acm_Role | c1= parse_sare_language_acm_User )
            int alt27=2;
            int LA27_0 = input.LA(1);

            if ( (LA27_0==13) ) {
                alt27=1;
            }
            else if ( (LA27_0==16) ) {
                alt27=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return element;}
                NoViableAltException nvae =
                    new NoViableAltException("", 27, 0, input);

                throw nvae;
            }
            switch (alt27) {
                case 1 :
                    // Acm.g:2723:2: c0= parse_sare_language_acm_Role
                    {
                    pushFollow(FOLLOW_parse_sare_language_acm_Role_in_parse_sare_language_acm_Subject2540);
                    c0=parse_sare_language_acm_Role();

                    state._fsp--;
                    if (state.failed) return element;
                    if ( state.backtracking==0 ) {
                       element = c0; /* this is a subclass or primitive expression choice */ 
                    }

                    }
                    break;
                case 2 :
                    // Acm.g:2724:4: c1= parse_sare_language_acm_User
                    {
                    pushFollow(FOLLOW_parse_sare_language_acm_User_in_parse_sare_language_acm_Subject2550);
                    c1=parse_sare_language_acm_User();

                    state._fsp--;
                    if (state.failed) return element;
                    if ( state.backtracking==0 ) {
                       element = c1; /* this is a subclass or primitive expression choice */ 
                    }

                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
            if ( state.backtracking>0 ) { memoize(input, 14, parse_sare_language_acm_Subject_StartIndex); }
        }
        return element;
    }
    // $ANTLR end "parse_sare_language_acm_Subject"


    // $ANTLR start "parse_org_eclipse_uml2_uml_PackageableElement"
    // Acm.g:2728:1: parse_org_eclipse_uml2_uml_PackageableElement returns [org.eclipse.uml2.uml.PackageableElement element = null] : (c0= parse_sare_language_acm_AccessControlModel | c1= parse_org_eclipse_uml2_uml_Model | c2= parse_org_eclipse_uml2_uml_Package | c3= parse_org_eclipse_uml2_uml_Class );
    public final org.eclipse.uml2.uml.PackageableElement parse_org_eclipse_uml2_uml_PackageableElement() throws RecognitionException {
        org.eclipse.uml2.uml.PackageableElement element =  null;
        int parse_org_eclipse_uml2_uml_PackageableElement_StartIndex = input.index();
        sare.language.acm.AccessControlModel c0 = null;

        org.eclipse.uml2.uml.Model c1 = null;

        org.eclipse.uml2.uml.Package c2 = null;

        org.eclipse.uml2.uml.Class c3 = null;


        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 15) ) { return element; }
            // Acm.g:2729:1: (c0= parse_sare_language_acm_AccessControlModel | c1= parse_org_eclipse_uml2_uml_Model | c2= parse_org_eclipse_uml2_uml_Package | c3= parse_org_eclipse_uml2_uml_Class )
            int alt28=4;
            switch ( input.LA(1) ) {
            case 9:
                {
                int LA28_1 = input.LA(2);

                if ( (synpred28_Acm()) ) {
                    alt28=1;
                }
                else if ( (synpred29_Acm()) ) {
                    alt28=2;
                }
                else if ( (synpred30_Acm()) ) {
                    alt28=3;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return element;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 28, 1, input);

                    throw nvae;
                }
                }
                break;
            case 10:
                {
                alt28=1;
                }
                break;
            case 19:
                {
                int LA28_3 = input.LA(2);

                if ( (synpred29_Acm()) ) {
                    alt28=2;
                }
                else if ( (synpred30_Acm()) ) {
                    alt28=3;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return element;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 28, 3, input);

                    throw nvae;
                }
                }
                break;
            case 20:
                {
                alt28=3;
                }
                break;
            case 21:
            case 25:
            case 27:
                {
                alt28=4;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return element;}
                NoViableAltException nvae =
                    new NoViableAltException("", 28, 0, input);

                throw nvae;
            }

            switch (alt28) {
                case 1 :
                    // Acm.g:2730:2: c0= parse_sare_language_acm_AccessControlModel
                    {
                    pushFollow(FOLLOW_parse_sare_language_acm_AccessControlModel_in_parse_org_eclipse_uml2_uml_PackageableElement2571);
                    c0=parse_sare_language_acm_AccessControlModel();

                    state._fsp--;
                    if (state.failed) return element;
                    if ( state.backtracking==0 ) {
                       element = c0; /* this is a subclass or primitive expression choice */ 
                    }

                    }
                    break;
                case 2 :
                    // Acm.g:2731:4: c1= parse_org_eclipse_uml2_uml_Model
                    {
                    pushFollow(FOLLOW_parse_org_eclipse_uml2_uml_Model_in_parse_org_eclipse_uml2_uml_PackageableElement2581);
                    c1=parse_org_eclipse_uml2_uml_Model();

                    state._fsp--;
                    if (state.failed) return element;
                    if ( state.backtracking==0 ) {
                       element = c1; /* this is a subclass or primitive expression choice */ 
                    }

                    }
                    break;
                case 3 :
                    // Acm.g:2732:4: c2= parse_org_eclipse_uml2_uml_Package
                    {
                    pushFollow(FOLLOW_parse_org_eclipse_uml2_uml_Package_in_parse_org_eclipse_uml2_uml_PackageableElement2591);
                    c2=parse_org_eclipse_uml2_uml_Package();

                    state._fsp--;
                    if (state.failed) return element;
                    if ( state.backtracking==0 ) {
                       element = c2; /* this is a subclass or primitive expression choice */ 
                    }

                    }
                    break;
                case 4 :
                    // Acm.g:2733:4: c3= parse_org_eclipse_uml2_uml_Class
                    {
                    pushFollow(FOLLOW_parse_org_eclipse_uml2_uml_Class_in_parse_org_eclipse_uml2_uml_PackageableElement2601);
                    c3=parse_org_eclipse_uml2_uml_Class();

                    state._fsp--;
                    if (state.failed) return element;
                    if ( state.backtracking==0 ) {
                       element = c3; /* this is a subclass or primitive expression choice */ 
                    }

                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
            if ( state.backtracking>0 ) { memoize(input, 15, parse_org_eclipse_uml2_uml_PackageableElement_StartIndex); }
        }
        return element;
    }
    // $ANTLR end "parse_org_eclipse_uml2_uml_PackageableElement"

    // $ANTLR start synpred28_Acm
    public final void synpred28_Acm_fragment() throws RecognitionException {   
        sare.language.acm.AccessControlModel c0 = null;


        // Acm.g:2730:2: (c0= parse_sare_language_acm_AccessControlModel )
        // Acm.g:2730:2: c0= parse_sare_language_acm_AccessControlModel
        {
        pushFollow(FOLLOW_parse_sare_language_acm_AccessControlModel_in_synpred28_Acm2571);
        c0=parse_sare_language_acm_AccessControlModel();

        state._fsp--;
        if (state.failed) return ;

        }
    }
    // $ANTLR end synpred28_Acm

    // $ANTLR start synpred29_Acm
    public final void synpred29_Acm_fragment() throws RecognitionException {   
        org.eclipse.uml2.uml.Model c1 = null;


        // Acm.g:2731:4: (c1= parse_org_eclipse_uml2_uml_Model )
        // Acm.g:2731:4: c1= parse_org_eclipse_uml2_uml_Model
        {
        pushFollow(FOLLOW_parse_org_eclipse_uml2_uml_Model_in_synpred29_Acm2581);
        c1=parse_org_eclipse_uml2_uml_Model();

        state._fsp--;
        if (state.failed) return ;

        }
    }
    // $ANTLR end synpred29_Acm

    // $ANTLR start synpred30_Acm
    public final void synpred30_Acm_fragment() throws RecognitionException {   
        org.eclipse.uml2.uml.Package c2 = null;


        // Acm.g:2732:4: (c2= parse_org_eclipse_uml2_uml_Package )
        // Acm.g:2732:4: c2= parse_org_eclipse_uml2_uml_Package
        {
        pushFollow(FOLLOW_parse_org_eclipse_uml2_uml_Package_in_synpred30_Acm2591);
        c2=parse_org_eclipse_uml2_uml_Package();

        state._fsp--;
        if (state.failed) return ;

        }
    }
    // $ANTLR end synpred30_Acm

    // Delegated rules

    public final boolean synpred29_Acm() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred29_Acm_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred28_Acm() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred28_Acm_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred30_Acm() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred30_Acm_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }


    protected DFA18 dfa18 = new DFA18(this);
    static final String DFA18_eotS =
        "\12\uffff";
    static final String DFA18_eofS =
        "\12\uffff";
    static final String DFA18_minS =
        "\3\5\1\26\1\32\1\34\2\uffff\2\5";
    static final String DFA18_maxS =
        "\1\33\2\5\1\30\1\32\1\34\2\uffff\2\33";
    static final String DFA18_acceptS =
        "\6\uffff\1\1\1\2\2\uffff";
    static final String DFA18_specialS =
        "\12\uffff}>";
    static final String[] DFA18_transitionS = {
            "\1\3\23\uffff\1\1\1\uffff\1\2",
            "\1\4",
            "\1\5",
            "\1\7\1\uffff\1\6",
            "\1\10",
            "\1\11",
            "",
            "",
            "\1\3\23\uffff\1\1\1\uffff\1\2",
            "\1\3\23\uffff\1\1\1\uffff\1\2"
    };

    static final short[] DFA18_eot = DFA.unpackEncodedString(DFA18_eotS);
    static final short[] DFA18_eof = DFA.unpackEncodedString(DFA18_eofS);
    static final char[] DFA18_min = DFA.unpackEncodedStringToUnsignedChars(DFA18_minS);
    static final char[] DFA18_max = DFA.unpackEncodedStringToUnsignedChars(DFA18_maxS);
    static final short[] DFA18_accept = DFA.unpackEncodedString(DFA18_acceptS);
    static final short[] DFA18_special = DFA.unpackEncodedString(DFA18_specialS);
    static final short[][] DFA18_transition;

    static {
        int numStates = DFA18_transitionS.length;
        DFA18_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA18_transition[i] = DFA.unpackEncodedString(DFA18_transitionS[i]);
        }
    }

    class DFA18 extends DFA {

        public DFA18(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 18;
            this.eot = DFA18_eot;
            this.eof = DFA18_eof;
            this.min = DFA18_min;
            this.max = DFA18_max;
            this.accept = DFA18_accept;
            this.special = DFA18_special;
            this.transition = DFA18_transition;
        }
        public String getDescription() {
            return "1953:3: ( (a8_0= parse_org_eclipse_uml2_uml_Property ) | (a9_0= parse_org_eclipse_uml2_uml_Operation ) )";
        }
    }
 

    public static final BitSet FOLLOW_parse_sare_language_acm_AccessControlModel_in_start82 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_start89 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_9_in_parse_sare_language_acm_AccessControlModel124 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_QUOTED_34_34_in_parse_sare_language_acm_AccessControlModel150 = new BitSet(new long[]{0x0000000000000600L});
    public static final BitSet FOLLOW_10_in_parse_sare_language_acm_AccessControlModel196 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_TEXT_in_parse_sare_language_acm_AccessControlModel214 = new BitSet(new long[]{0x0000000000000800L});
    public static final BitSet FOLLOW_11_in_parse_sare_language_acm_AccessControlModel235 = new BitSet(new long[]{0x0000000000013000L});
    public static final BitSet FOLLOW_parse_sare_language_acm_Subject_in_parse_sare_language_acm_AccessControlModel258 = new BitSet(new long[]{0x0000000000013000L});
    public static final BitSet FOLLOW_12_in_parse_sare_language_acm_AccessControlModel284 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_13_in_parse_sare_language_acm_Role313 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_TEXT_in_parse_sare_language_acm_Role331 = new BitSet(new long[]{0x0000000000004800L});
    public static final BitSet FOLLOW_14_in_parse_sare_language_acm_Role361 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_TEXT_in_parse_sare_language_acm_Role387 = new BitSet(new long[]{0x0000000000008800L});
    public static final BitSet FOLLOW_15_in_parse_sare_language_acm_Role433 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_TEXT_in_parse_sare_language_acm_Role467 = new BitSet(new long[]{0x0000000000008800L});
    public static final BitSet FOLLOW_11_in_parse_sare_language_acm_Role548 = new BitSet(new long[]{0x0000000000001060L});
    public static final BitSet FOLLOW_parse_sare_language_acm_Permission_in_parse_sare_language_acm_Role571 = new BitSet(new long[]{0x0000000000001060L});
    public static final BitSet FOLLOW_12_in_parse_sare_language_acm_Role597 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_16_in_parse_sare_language_acm_User626 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_TEXT_in_parse_sare_language_acm_User644 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_17_in_parse_sare_language_acm_User665 = new BitSet(new long[]{0x0000000000008002L});
    public static final BitSet FOLLOW_15_in_parse_sare_language_acm_User688 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_TEXT_in_parse_sare_language_acm_User714 = new BitSet(new long[]{0x0000000000008002L});
    public static final BitSet FOLLOW_NAMED_ELEMENT_ID_in_parse_sare_language_acm_Permission784 = new BitSet(new long[]{0x0000000000040802L});
    public static final BitSet FOLLOW_TEXT_in_parse_sare_language_acm_Permission822 = new BitSet(new long[]{0x0000000000040802L});
    public static final BitSet FOLLOW_18_in_parse_sare_language_acm_Permission865 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_TEXT_in_parse_sare_language_acm_Permission891 = new BitSet(new long[]{0x0000000000000802L});
    public static final BitSet FOLLOW_11_in_parse_sare_language_acm_Permission946 = new BitSet(new long[]{0x0000000000001060L});
    public static final BitSet FOLLOW_parse_sare_language_acm_Permission_in_parse_sare_language_acm_Permission979 = new BitSet(new long[]{0x0000000000001060L});
    public static final BitSet FOLLOW_12_in_parse_sare_language_acm_Permission1019 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_9_in_parse_org_eclipse_uml2_uml_Model1076 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_parse_org_eclipse_uml2_uml_PackageImport_in_parse_org_eclipse_uml2_uml_Model1102 = new BitSet(new long[]{0x0000000000080200L});
    public static final BitSet FOLLOW_19_in_parse_org_eclipse_uml2_uml_Model1143 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_TEXT_in_parse_org_eclipse_uml2_uml_Model1161 = new BitSet(new long[]{0x0000000000000800L});
    public static final BitSet FOLLOW_11_in_parse_org_eclipse_uml2_uml_Model1182 = new BitSet(new long[]{0x000000000A381600L});
    public static final BitSet FOLLOW_parse_org_eclipse_uml2_uml_PackageableElement_in_parse_org_eclipse_uml2_uml_Model1205 = new BitSet(new long[]{0x000000000A381600L});
    public static final BitSet FOLLOW_12_in_parse_org_eclipse_uml2_uml_Model1231 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_QUOTED_34_34_in_parse_org_eclipse_uml2_uml_PackageImport1264 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_20_in_parse_org_eclipse_uml2_uml_Package1300 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_TEXT_in_parse_org_eclipse_uml2_uml_Package1318 = new BitSet(new long[]{0x0000000000000800L});
    public static final BitSet FOLLOW_11_in_parse_org_eclipse_uml2_uml_Package1339 = new BitSet(new long[]{0x000000000A381600L});
    public static final BitSet FOLLOW_parse_org_eclipse_uml2_uml_PackageableElement_in_parse_org_eclipse_uml2_uml_Package1362 = new BitSet(new long[]{0x000000000A381600L});
    public static final BitSet FOLLOW_12_in_parse_org_eclipse_uml2_uml_Package1388 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_org_eclipse_uml2_uml_Model_in_parse_org_eclipse_uml2_uml_Package1407 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_org_eclipse_uml2_uml_Comment_in_parse_org_eclipse_uml2_uml_Class1441 = new BitSet(new long[]{0x000000000A200000L});
    public static final BitSet FOLLOW_21_in_parse_org_eclipse_uml2_uml_Class1467 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_TEXT_in_parse_org_eclipse_uml2_uml_Class1485 = new BitSet(new long[]{0x0000000000004800L});
    public static final BitSet FOLLOW_14_in_parse_org_eclipse_uml2_uml_Class1515 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_TEXT_in_parse_org_eclipse_uml2_uml_Class1541 = new BitSet(new long[]{0x0000000000008800L});
    public static final BitSet FOLLOW_15_in_parse_org_eclipse_uml2_uml_Class1587 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_TEXT_in_parse_org_eclipse_uml2_uml_Class1621 = new BitSet(new long[]{0x0000000000008800L});
    public static final BitSet FOLLOW_11_in_parse_org_eclipse_uml2_uml_Class1702 = new BitSet(new long[]{0x000000000A001020L});
    public static final BitSet FOLLOW_parse_org_eclipse_uml2_uml_Property_in_parse_org_eclipse_uml2_uml_Class1731 = new BitSet(new long[]{0x000000000A001020L});
    public static final BitSet FOLLOW_parse_org_eclipse_uml2_uml_Operation_in_parse_org_eclipse_uml2_uml_Class1773 = new BitSet(new long[]{0x000000000A001020L});
    public static final BitSet FOLLOW_12_in_parse_org_eclipse_uml2_uml_Class1814 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_org_eclipse_uml2_uml_Comment_in_parse_org_eclipse_uml2_uml_Operation1852 = new BitSet(new long[]{0x000000000A000020L});
    public static final BitSet FOLLOW_TEXT_in_parse_org_eclipse_uml2_uml_Operation1882 = new BitSet(new long[]{0x0000000000400000L});
    public static final BitSet FOLLOW_22_in_parse_org_eclipse_uml2_uml_Operation1903 = new BitSet(new long[]{0x000000000A800020L});
    public static final BitSet FOLLOW_parse_org_eclipse_uml2_uml_Parameter_in_parse_org_eclipse_uml2_uml_Operation1932 = new BitSet(new long[]{0x0000000000808000L});
    public static final BitSet FOLLOW_15_in_parse_org_eclipse_uml2_uml_Operation1973 = new BitSet(new long[]{0x000000000A000020L});
    public static final BitSet FOLLOW_parse_org_eclipse_uml2_uml_Parameter_in_parse_org_eclipse_uml2_uml_Operation2007 = new BitSet(new long[]{0x0000000000808000L});
    public static final BitSet FOLLOW_23_in_parse_org_eclipse_uml2_uml_Operation2081 = new BitSet(new long[]{0x0000000001000002L});
    public static final BitSet FOLLOW_24_in_parse_org_eclipse_uml2_uml_Operation2104 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_TEXT_in_parse_org_eclipse_uml2_uml_Operation2130 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_org_eclipse_uml2_uml_Comment_in_parse_org_eclipse_uml2_uml_Property2200 = new BitSet(new long[]{0x000000000A000020L});
    public static final BitSet FOLLOW_TEXT_in_parse_org_eclipse_uml2_uml_Property2230 = new BitSet(new long[]{0x0000000001000000L});
    public static final BitSet FOLLOW_24_in_parse_org_eclipse_uml2_uml_Property2251 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_TEXT_in_parse_org_eclipse_uml2_uml_Property2269 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_org_eclipse_uml2_uml_Comment_in_parse_org_eclipse_uml2_uml_Parameter2314 = new BitSet(new long[]{0x000000000A000020L});
    public static final BitSet FOLLOW_TEXT_in_parse_org_eclipse_uml2_uml_Parameter2344 = new BitSet(new long[]{0x0000000001000000L});
    public static final BitSet FOLLOW_24_in_parse_org_eclipse_uml2_uml_Parameter2365 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_TEXT_in_parse_org_eclipse_uml2_uml_Parameter2383 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_25_in_parse_org_eclipse_uml2_uml_Comment2419 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_TEXT_in_parse_org_eclipse_uml2_uml_Comment2437 = new BitSet(new long[]{0x0000000004000000L});
    public static final BitSet FOLLOW_26_in_parse_org_eclipse_uml2_uml_Comment2458 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_27_in_parse_org_eclipse_uml2_uml_Comment2476 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_TEXT_in_parse_org_eclipse_uml2_uml_Comment2494 = new BitSet(new long[]{0x0000000010000000L});
    public static final BitSet FOLLOW_28_in_parse_org_eclipse_uml2_uml_Comment2515 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_sare_language_acm_Role_in_parse_sare_language_acm_Subject2540 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_sare_language_acm_User_in_parse_sare_language_acm_Subject2550 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_sare_language_acm_AccessControlModel_in_parse_org_eclipse_uml2_uml_PackageableElement2571 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_org_eclipse_uml2_uml_Model_in_parse_org_eclipse_uml2_uml_PackageableElement2581 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_org_eclipse_uml2_uml_Package_in_parse_org_eclipse_uml2_uml_PackageableElement2591 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_org_eclipse_uml2_uml_Class_in_parse_org_eclipse_uml2_uml_PackageableElement2601 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_sare_language_acm_AccessControlModel_in_synpred28_Acm2571 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_org_eclipse_uml2_uml_Model_in_synpred29_Acm2581 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_org_eclipse_uml2_uml_Package_in_synpred30_Acm2591 = new BitSet(new long[]{0x0000000000000002L});

}