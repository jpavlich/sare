/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package sare.language.acm.resource.acm.mopp;

public class AcmTokenStyleInformationProvider {
	
	public sare.language.acm.resource.acm.IAcmTokenStyle getDefaultTokenStyle(String tokenName) {
		if ("import".equals(tokenName)) {
			return new sare.language.acm.resource.acm.mopp.AcmTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("model".equals(tokenName)) {
			return new sare.language.acm.resource.acm.mopp.AcmTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("package".equals(tokenName)) {
			return new sare.language.acm.resource.acm.mopp.AcmTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("class".equals(tokenName)) {
			return new sare.language.acm.resource.acm.mopp.AcmTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("extends".equals(tokenName)) {
			return new sare.language.acm.resource.acm.mopp.AcmTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("QUOTED_34_34".equals(tokenName)) {
			return new sare.language.acm.resource.acm.mopp.AcmTokenStyle(new int[] {0x2A, 0x00, 0xFF}, null, false, false, false, false);
		}
		if ("policy".equals(tokenName)) {
			return new sare.language.acm.resource.acm.mopp.AcmTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("role".equals(tokenName)) {
			return new sare.language.acm.resource.acm.mopp.AcmTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("user".equals(tokenName)) {
			return new sare.language.acm.resource.acm.mopp.AcmTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("hasRoles".equals(tokenName)) {
			return new sare.language.acm.resource.acm.mopp.AcmTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("if".equals(tokenName)) {
			return new sare.language.acm.resource.acm.mopp.AcmTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		return null;
	}
	
}
