/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package sare.language.acm.resource.acm.mopp;

public class AcmResourceFactory implements org.eclipse.emf.ecore.resource.Resource.Factory {
	
	public AcmResourceFactory() {
		super();
	}
	
	public org.eclipse.emf.ecore.resource.Resource createResource(org.eclipse.emf.common.util.URI uri) {
		return new sare.language.acm.resource.acm.mopp.AcmResource(uri);
	}
	
}
