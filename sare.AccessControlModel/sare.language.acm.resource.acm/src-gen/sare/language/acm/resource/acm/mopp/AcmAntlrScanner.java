/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package sare.language.acm.resource.acm.mopp;

public class AcmAntlrScanner implements sare.language.acm.resource.acm.IAcmTextScanner {
	
	private org.antlr.runtime3_3_0.Lexer antlrLexer;
	
	public AcmAntlrScanner(org.antlr.runtime3_3_0.Lexer antlrLexer) {
		this.antlrLexer = antlrLexer;
	}
	
	public sare.language.acm.resource.acm.IAcmTextToken getNextToken() {
		if (antlrLexer.getCharStream() == null) {
			return null;
		}
		final org.antlr.runtime3_3_0.Token current = antlrLexer.nextToken();
		if (current == null || current.getType() < 0) {
			return null;
		}
		sare.language.acm.resource.acm.IAcmTextToken result = new sare.language.acm.resource.acm.mopp.AcmTextToken(current);
		return result;
	}
	
	public void setText(String text) {
		antlrLexer.setCharStream(new org.antlr.runtime3_3_0.ANTLRStringStream(text));
	}
	
}
