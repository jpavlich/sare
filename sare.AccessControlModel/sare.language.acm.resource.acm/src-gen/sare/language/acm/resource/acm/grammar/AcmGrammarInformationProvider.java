/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package sare.language.acm.resource.acm.grammar;

public class AcmGrammarInformationProvider {
	
	public final static org.eclipse.emf.ecore.EStructuralFeature ANONYMOUS_FEATURE = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEAttribute();
	static {
		ANONYMOUS_FEATURE.setName("_");
	}
	
	public final static AcmGrammarInformationProvider INSTANCE = new AcmGrammarInformationProvider();
	
	private java.util.Set<String> keywords;
	
	public final static sare.language.acm.resource.acm.grammar.AcmKeyword ACM_0_0_0_0_0_0_0 = new sare.language.acm.resource.acm.grammar.AcmKeyword("import", sare.language.acm.resource.acm.grammar.AcmCardinality.ONE);
	public final static sare.language.acm.resource.acm.grammar.AcmPlaceholder ACM_0_0_0_0_0_0_1 = new sare.language.acm.resource.acm.grammar.AcmPlaceholder(sare.language.acm.AcmPackage.eINSTANCE.getAccessControlModel().getEStructuralFeature(sare.language.acm.AcmPackage.ACCESS_CONTROL_MODEL__IMPORTS), "QUOTED_34_34", sare.language.acm.resource.acm.grammar.AcmCardinality.ONE, 0);
	public final static sare.language.acm.resource.acm.grammar.AcmSequence ACM_0_0_0_0_0_0 = new sare.language.acm.resource.acm.grammar.AcmSequence(sare.language.acm.resource.acm.grammar.AcmCardinality.ONE, ACM_0_0_0_0_0_0_0, ACM_0_0_0_0_0_0_1);
	public final static sare.language.acm.resource.acm.grammar.AcmChoice ACM_0_0_0_0_0 = new sare.language.acm.resource.acm.grammar.AcmChoice(sare.language.acm.resource.acm.grammar.AcmCardinality.ONE, ACM_0_0_0_0_0_0);
	public final static sare.language.acm.resource.acm.grammar.AcmCompound ACM_0_0_0_0 = new sare.language.acm.resource.acm.grammar.AcmCompound(ACM_0_0_0_0_0, sare.language.acm.resource.acm.grammar.AcmCardinality.STAR);
	public final static sare.language.acm.resource.acm.grammar.AcmKeyword ACM_0_0_0_1 = new sare.language.acm.resource.acm.grammar.AcmKeyword("policy", sare.language.acm.resource.acm.grammar.AcmCardinality.ONE);
	public final static sare.language.acm.resource.acm.grammar.AcmPlaceholder ACM_0_0_0_2 = new sare.language.acm.resource.acm.grammar.AcmPlaceholder(sare.language.acm.AcmPackage.eINSTANCE.getAccessControlModel().getEStructuralFeature(sare.language.acm.AcmPackage.ACCESS_CONTROL_MODEL__NAME), "TEXT", sare.language.acm.resource.acm.grammar.AcmCardinality.ONE, 0);
	public final static sare.language.acm.resource.acm.grammar.AcmKeyword ACM_0_0_0_3 = new sare.language.acm.resource.acm.grammar.AcmKeyword("{", sare.language.acm.resource.acm.grammar.AcmCardinality.ONE);
	public final static sare.language.acm.resource.acm.grammar.AcmContainment ACM_0_0_0_4 = new sare.language.acm.resource.acm.grammar.AcmContainment(sare.language.acm.AcmPackage.eINSTANCE.getAccessControlModel().getEStructuralFeature(sare.language.acm.AcmPackage.ACCESS_CONTROL_MODEL__ELEMENTS), sare.language.acm.resource.acm.grammar.AcmCardinality.STAR, 0);
	public final static sare.language.acm.resource.acm.grammar.AcmKeyword ACM_0_0_0_5 = new sare.language.acm.resource.acm.grammar.AcmKeyword("}", sare.language.acm.resource.acm.grammar.AcmCardinality.ONE);
	public final static sare.language.acm.resource.acm.grammar.AcmSequence ACM_0_0_0 = new sare.language.acm.resource.acm.grammar.AcmSequence(sare.language.acm.resource.acm.grammar.AcmCardinality.ONE, ACM_0_0_0_0, ACM_0_0_0_1, ACM_0_0_0_2, ACM_0_0_0_3, ACM_0_0_0_4, ACM_0_0_0_5);
	public final static sare.language.acm.resource.acm.grammar.AcmChoice ACM_0_0 = new sare.language.acm.resource.acm.grammar.AcmChoice(sare.language.acm.resource.acm.grammar.AcmCardinality.ONE, ACM_0_0_0);
	public final static sare.language.acm.resource.acm.grammar.AcmRule ACM_0 = new sare.language.acm.resource.acm.grammar.AcmRule(sare.language.acm.AcmPackage.eINSTANCE.getAccessControlModel(), ACM_0_0, sare.language.acm.resource.acm.grammar.AcmCardinality.ONE);
	public final static sare.language.acm.resource.acm.grammar.AcmKeyword ACM_1_0_0_0 = new sare.language.acm.resource.acm.grammar.AcmKeyword("role", sare.language.acm.resource.acm.grammar.AcmCardinality.ONE);
	public final static sare.language.acm.resource.acm.grammar.AcmPlaceholder ACM_1_0_0_1 = new sare.language.acm.resource.acm.grammar.AcmPlaceholder(sare.language.acm.AcmPackage.eINSTANCE.getRole().getEStructuralFeature(sare.language.acm.AcmPackage.ROLE__NAME), "TEXT", sare.language.acm.resource.acm.grammar.AcmCardinality.ONE, 0);
	public final static sare.language.acm.resource.acm.grammar.AcmKeyword ACM_1_0_0_2_0_0_0 = new sare.language.acm.resource.acm.grammar.AcmKeyword("extends", sare.language.acm.resource.acm.grammar.AcmCardinality.ONE);
	public final static sare.language.acm.resource.acm.grammar.AcmPlaceholder ACM_1_0_0_2_0_0_1 = new sare.language.acm.resource.acm.grammar.AcmPlaceholder(sare.language.acm.AcmPackage.eINSTANCE.getRole().getEStructuralFeature(sare.language.acm.AcmPackage.ROLE__PARENT_ROLES), "TEXT", sare.language.acm.resource.acm.grammar.AcmCardinality.ONE, 0);
	public final static sare.language.acm.resource.acm.grammar.AcmKeyword ACM_1_0_0_2_0_0_2_0_0_0 = new sare.language.acm.resource.acm.grammar.AcmKeyword(",", sare.language.acm.resource.acm.grammar.AcmCardinality.ONE);
	public final static sare.language.acm.resource.acm.grammar.AcmPlaceholder ACM_1_0_0_2_0_0_2_0_0_1 = new sare.language.acm.resource.acm.grammar.AcmPlaceholder(sare.language.acm.AcmPackage.eINSTANCE.getRole().getEStructuralFeature(sare.language.acm.AcmPackage.ROLE__PARENT_ROLES), "TEXT", sare.language.acm.resource.acm.grammar.AcmCardinality.ONE, 0);
	public final static sare.language.acm.resource.acm.grammar.AcmSequence ACM_1_0_0_2_0_0_2_0_0 = new sare.language.acm.resource.acm.grammar.AcmSequence(sare.language.acm.resource.acm.grammar.AcmCardinality.ONE, ACM_1_0_0_2_0_0_2_0_0_0, ACM_1_0_0_2_0_0_2_0_0_1);
	public final static sare.language.acm.resource.acm.grammar.AcmChoice ACM_1_0_0_2_0_0_2_0 = new sare.language.acm.resource.acm.grammar.AcmChoice(sare.language.acm.resource.acm.grammar.AcmCardinality.ONE, ACM_1_0_0_2_0_0_2_0_0);
	public final static sare.language.acm.resource.acm.grammar.AcmCompound ACM_1_0_0_2_0_0_2 = new sare.language.acm.resource.acm.grammar.AcmCompound(ACM_1_0_0_2_0_0_2_0, sare.language.acm.resource.acm.grammar.AcmCardinality.STAR);
	public final static sare.language.acm.resource.acm.grammar.AcmSequence ACM_1_0_0_2_0_0 = new sare.language.acm.resource.acm.grammar.AcmSequence(sare.language.acm.resource.acm.grammar.AcmCardinality.ONE, ACM_1_0_0_2_0_0_0, ACM_1_0_0_2_0_0_1, ACM_1_0_0_2_0_0_2);
	public final static sare.language.acm.resource.acm.grammar.AcmChoice ACM_1_0_0_2_0 = new sare.language.acm.resource.acm.grammar.AcmChoice(sare.language.acm.resource.acm.grammar.AcmCardinality.ONE, ACM_1_0_0_2_0_0);
	public final static sare.language.acm.resource.acm.grammar.AcmCompound ACM_1_0_0_2 = new sare.language.acm.resource.acm.grammar.AcmCompound(ACM_1_0_0_2_0, sare.language.acm.resource.acm.grammar.AcmCardinality.QUESTIONMARK);
	public final static sare.language.acm.resource.acm.grammar.AcmKeyword ACM_1_0_0_3 = new sare.language.acm.resource.acm.grammar.AcmKeyword("{", sare.language.acm.resource.acm.grammar.AcmCardinality.ONE);
	public final static sare.language.acm.resource.acm.grammar.AcmContainment ACM_1_0_0_4 = new sare.language.acm.resource.acm.grammar.AcmContainment(sare.language.acm.AcmPackage.eINSTANCE.getRole().getEStructuralFeature(sare.language.acm.AcmPackage.ROLE__PERMISSIONS), sare.language.acm.resource.acm.grammar.AcmCardinality.STAR, 0);
	public final static sare.language.acm.resource.acm.grammar.AcmKeyword ACM_1_0_0_5 = new sare.language.acm.resource.acm.grammar.AcmKeyword("}", sare.language.acm.resource.acm.grammar.AcmCardinality.ONE);
	public final static sare.language.acm.resource.acm.grammar.AcmSequence ACM_1_0_0 = new sare.language.acm.resource.acm.grammar.AcmSequence(sare.language.acm.resource.acm.grammar.AcmCardinality.ONE, ACM_1_0_0_0, ACM_1_0_0_1, ACM_1_0_0_2, ACM_1_0_0_3, ACM_1_0_0_4, ACM_1_0_0_5);
	public final static sare.language.acm.resource.acm.grammar.AcmChoice ACM_1_0 = new sare.language.acm.resource.acm.grammar.AcmChoice(sare.language.acm.resource.acm.grammar.AcmCardinality.ONE, ACM_1_0_0);
	public final static sare.language.acm.resource.acm.grammar.AcmRule ACM_1 = new sare.language.acm.resource.acm.grammar.AcmRule(sare.language.acm.AcmPackage.eINSTANCE.getRole(), ACM_1_0, sare.language.acm.resource.acm.grammar.AcmCardinality.ONE);
	public final static sare.language.acm.resource.acm.grammar.AcmKeyword ACM_2_0_0_0 = new sare.language.acm.resource.acm.grammar.AcmKeyword("user", sare.language.acm.resource.acm.grammar.AcmCardinality.ONE);
	public final static sare.language.acm.resource.acm.grammar.AcmPlaceholder ACM_2_0_0_1 = new sare.language.acm.resource.acm.grammar.AcmPlaceholder(sare.language.acm.AcmPackage.eINSTANCE.getUser().getEStructuralFeature(sare.language.acm.AcmPackage.USER__NAME), "TEXT", sare.language.acm.resource.acm.grammar.AcmCardinality.ONE, 0);
	public final static sare.language.acm.resource.acm.grammar.AcmKeyword ACM_2_0_0_2 = new sare.language.acm.resource.acm.grammar.AcmKeyword("hasRoles", sare.language.acm.resource.acm.grammar.AcmCardinality.ONE);
	public final static sare.language.acm.resource.acm.grammar.AcmKeyword ACM_2_0_0_3_0_0_0 = new sare.language.acm.resource.acm.grammar.AcmKeyword(",", sare.language.acm.resource.acm.grammar.AcmCardinality.ONE);
	public final static sare.language.acm.resource.acm.grammar.AcmPlaceholder ACM_2_0_0_3_0_0_1 = new sare.language.acm.resource.acm.grammar.AcmPlaceholder(sare.language.acm.AcmPackage.eINSTANCE.getUser().getEStructuralFeature(sare.language.acm.AcmPackage.USER__ROLES), "TEXT", sare.language.acm.resource.acm.grammar.AcmCardinality.ONE, 0);
	public final static sare.language.acm.resource.acm.grammar.AcmSequence ACM_2_0_0_3_0_0 = new sare.language.acm.resource.acm.grammar.AcmSequence(sare.language.acm.resource.acm.grammar.AcmCardinality.ONE, ACM_2_0_0_3_0_0_0, ACM_2_0_0_3_0_0_1);
	public final static sare.language.acm.resource.acm.grammar.AcmChoice ACM_2_0_0_3_0 = new sare.language.acm.resource.acm.grammar.AcmChoice(sare.language.acm.resource.acm.grammar.AcmCardinality.ONE, ACM_2_0_0_3_0_0);
	public final static sare.language.acm.resource.acm.grammar.AcmCompound ACM_2_0_0_3 = new sare.language.acm.resource.acm.grammar.AcmCompound(ACM_2_0_0_3_0, sare.language.acm.resource.acm.grammar.AcmCardinality.STAR);
	public final static sare.language.acm.resource.acm.grammar.AcmSequence ACM_2_0_0 = new sare.language.acm.resource.acm.grammar.AcmSequence(sare.language.acm.resource.acm.grammar.AcmCardinality.ONE, ACM_2_0_0_0, ACM_2_0_0_1, ACM_2_0_0_2, ACM_2_0_0_3);
	public final static sare.language.acm.resource.acm.grammar.AcmChoice ACM_2_0 = new sare.language.acm.resource.acm.grammar.AcmChoice(sare.language.acm.resource.acm.grammar.AcmCardinality.ONE, ACM_2_0_0);
	public final static sare.language.acm.resource.acm.grammar.AcmRule ACM_2 = new sare.language.acm.resource.acm.grammar.AcmRule(sare.language.acm.AcmPackage.eINSTANCE.getUser(), ACM_2_0, sare.language.acm.resource.acm.grammar.AcmCardinality.ONE);
	public final static sare.language.acm.resource.acm.grammar.AcmPlaceholder ACM_3_0_0_0_0_0_0 = new sare.language.acm.resource.acm.grammar.AcmPlaceholder(sare.language.acm.AcmPackage.eINSTANCE.getPermission().getEStructuralFeature(sare.language.acm.AcmPackage.PERMISSION__ELEMENT), "NAMED_ELEMENT_ID", sare.language.acm.resource.acm.grammar.AcmCardinality.ONE, 0);
	public final static sare.language.acm.resource.acm.grammar.AcmSequence ACM_3_0_0_0_0_0 = new sare.language.acm.resource.acm.grammar.AcmSequence(sare.language.acm.resource.acm.grammar.AcmCardinality.ONE, ACM_3_0_0_0_0_0_0);
	public final static sare.language.acm.resource.acm.grammar.AcmPlaceholder ACM_3_0_0_0_0_1_0 = new sare.language.acm.resource.acm.grammar.AcmPlaceholder(sare.language.acm.AcmPackage.eINSTANCE.getPermission().getEStructuralFeature(sare.language.acm.AcmPackage.PERMISSION__ELEMENT), "TEXT", sare.language.acm.resource.acm.grammar.AcmCardinality.ONE, 0);
	public final static sare.language.acm.resource.acm.grammar.AcmSequence ACM_3_0_0_0_0_1 = new sare.language.acm.resource.acm.grammar.AcmSequence(sare.language.acm.resource.acm.grammar.AcmCardinality.ONE, ACM_3_0_0_0_0_1_0);
	public final static sare.language.acm.resource.acm.grammar.AcmChoice ACM_3_0_0_0_0 = new sare.language.acm.resource.acm.grammar.AcmChoice(sare.language.acm.resource.acm.grammar.AcmCardinality.ONE, ACM_3_0_0_0_0_0, ACM_3_0_0_0_0_1);
	public final static sare.language.acm.resource.acm.grammar.AcmCompound ACM_3_0_0_0 = new sare.language.acm.resource.acm.grammar.AcmCompound(ACM_3_0_0_0_0, sare.language.acm.resource.acm.grammar.AcmCardinality.ONE);
	public final static sare.language.acm.resource.acm.grammar.AcmKeyword ACM_3_0_0_1_0_0_0 = new sare.language.acm.resource.acm.grammar.AcmKeyword("if", sare.language.acm.resource.acm.grammar.AcmCardinality.ONE);
	public final static sare.language.acm.resource.acm.grammar.AcmPlaceholder ACM_3_0_0_1_0_0_1 = new sare.language.acm.resource.acm.grammar.AcmPlaceholder(sare.language.acm.AcmPackage.eINSTANCE.getPermission().getEStructuralFeature(sare.language.acm.AcmPackage.PERMISSION__CONSTRAINT), "TEXT", sare.language.acm.resource.acm.grammar.AcmCardinality.ONE, 0);
	public final static sare.language.acm.resource.acm.grammar.AcmSequence ACM_3_0_0_1_0_0 = new sare.language.acm.resource.acm.grammar.AcmSequence(sare.language.acm.resource.acm.grammar.AcmCardinality.ONE, ACM_3_0_0_1_0_0_0, ACM_3_0_0_1_0_0_1);
	public final static sare.language.acm.resource.acm.grammar.AcmChoice ACM_3_0_0_1_0 = new sare.language.acm.resource.acm.grammar.AcmChoice(sare.language.acm.resource.acm.grammar.AcmCardinality.ONE, ACM_3_0_0_1_0_0);
	public final static sare.language.acm.resource.acm.grammar.AcmCompound ACM_3_0_0_1 = new sare.language.acm.resource.acm.grammar.AcmCompound(ACM_3_0_0_1_0, sare.language.acm.resource.acm.grammar.AcmCardinality.QUESTIONMARK);
	public final static sare.language.acm.resource.acm.grammar.AcmKeyword ACM_3_0_0_2_0_0_0 = new sare.language.acm.resource.acm.grammar.AcmKeyword("{", sare.language.acm.resource.acm.grammar.AcmCardinality.ONE);
	public final static sare.language.acm.resource.acm.grammar.AcmContainment ACM_3_0_0_2_0_0_1 = new sare.language.acm.resource.acm.grammar.AcmContainment(sare.language.acm.AcmPackage.eINSTANCE.getPermission().getEStructuralFeature(sare.language.acm.AcmPackage.PERMISSION__CHILD_PERMISSIONS), sare.language.acm.resource.acm.grammar.AcmCardinality.STAR, 0);
	public final static sare.language.acm.resource.acm.grammar.AcmKeyword ACM_3_0_0_2_0_0_2 = new sare.language.acm.resource.acm.grammar.AcmKeyword("}", sare.language.acm.resource.acm.grammar.AcmCardinality.ONE);
	public final static sare.language.acm.resource.acm.grammar.AcmSequence ACM_3_0_0_2_0_0 = new sare.language.acm.resource.acm.grammar.AcmSequence(sare.language.acm.resource.acm.grammar.AcmCardinality.ONE, ACM_3_0_0_2_0_0_0, ACM_3_0_0_2_0_0_1, ACM_3_0_0_2_0_0_2);
	public final static sare.language.acm.resource.acm.grammar.AcmChoice ACM_3_0_0_2_0 = new sare.language.acm.resource.acm.grammar.AcmChoice(sare.language.acm.resource.acm.grammar.AcmCardinality.ONE, ACM_3_0_0_2_0_0);
	public final static sare.language.acm.resource.acm.grammar.AcmCompound ACM_3_0_0_2 = new sare.language.acm.resource.acm.grammar.AcmCompound(ACM_3_0_0_2_0, sare.language.acm.resource.acm.grammar.AcmCardinality.QUESTIONMARK);
	public final static sare.language.acm.resource.acm.grammar.AcmSequence ACM_3_0_0 = new sare.language.acm.resource.acm.grammar.AcmSequence(sare.language.acm.resource.acm.grammar.AcmCardinality.ONE, ACM_3_0_0_0, ACM_3_0_0_1, ACM_3_0_0_2);
	public final static sare.language.acm.resource.acm.grammar.AcmChoice ACM_3_0 = new sare.language.acm.resource.acm.grammar.AcmChoice(sare.language.acm.resource.acm.grammar.AcmCardinality.ONE, ACM_3_0_0);
	public final static sare.language.acm.resource.acm.grammar.AcmRule ACM_3 = new sare.language.acm.resource.acm.grammar.AcmRule(sare.language.acm.AcmPackage.eINSTANCE.getPermission(), ACM_3_0, sare.language.acm.resource.acm.grammar.AcmCardinality.ONE);
	public final static sare.language.acm.resource.acm.grammar.AcmKeyword MINIUML_0_0_0_0_0_0_0 = new sare.language.acm.resource.acm.grammar.AcmKeyword("import", sare.language.acm.resource.acm.grammar.AcmCardinality.ONE);
	public final static sare.language.acm.resource.acm.grammar.AcmContainment MINIUML_0_0_0_0_0_0_1 = new sare.language.acm.resource.acm.grammar.AcmContainment(org.eclipse.uml2.uml.UMLPackage.eINSTANCE.getModel().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.MODEL__PACKAGE_IMPORT), sare.language.acm.resource.acm.grammar.AcmCardinality.ONE, 0);
	public final static sare.language.acm.resource.acm.grammar.AcmSequence MINIUML_0_0_0_0_0_0 = new sare.language.acm.resource.acm.grammar.AcmSequence(sare.language.acm.resource.acm.grammar.AcmCardinality.ONE, MINIUML_0_0_0_0_0_0_0, MINIUML_0_0_0_0_0_0_1);
	public final static sare.language.acm.resource.acm.grammar.AcmChoice MINIUML_0_0_0_0_0 = new sare.language.acm.resource.acm.grammar.AcmChoice(sare.language.acm.resource.acm.grammar.AcmCardinality.ONE, MINIUML_0_0_0_0_0_0);
	public final static sare.language.acm.resource.acm.grammar.AcmCompound MINIUML_0_0_0_0 = new sare.language.acm.resource.acm.grammar.AcmCompound(MINIUML_0_0_0_0_0, sare.language.acm.resource.acm.grammar.AcmCardinality.STAR);
	public final static sare.language.acm.resource.acm.grammar.AcmKeyword MINIUML_0_0_0_1 = new sare.language.acm.resource.acm.grammar.AcmKeyword("model", sare.language.acm.resource.acm.grammar.AcmCardinality.ONE);
	public final static sare.language.acm.resource.acm.grammar.AcmPlaceholder MINIUML_0_0_0_2 = new sare.language.acm.resource.acm.grammar.AcmPlaceholder(org.eclipse.uml2.uml.UMLPackage.eINSTANCE.getModel().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.MODEL__NAME), "TEXT", sare.language.acm.resource.acm.grammar.AcmCardinality.ONE, 0);
	public final static sare.language.acm.resource.acm.grammar.AcmKeyword MINIUML_0_0_0_3 = new sare.language.acm.resource.acm.grammar.AcmKeyword("{", sare.language.acm.resource.acm.grammar.AcmCardinality.ONE);
	public final static sare.language.acm.resource.acm.grammar.AcmContainment MINIUML_0_0_0_4 = new sare.language.acm.resource.acm.grammar.AcmContainment(org.eclipse.uml2.uml.UMLPackage.eINSTANCE.getModel().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.MODEL__PACKAGED_ELEMENT), sare.language.acm.resource.acm.grammar.AcmCardinality.STAR, 0);
	public final static sare.language.acm.resource.acm.grammar.AcmKeyword MINIUML_0_0_0_5 = new sare.language.acm.resource.acm.grammar.AcmKeyword("}", sare.language.acm.resource.acm.grammar.AcmCardinality.ONE);
	public final static sare.language.acm.resource.acm.grammar.AcmSequence MINIUML_0_0_0 = new sare.language.acm.resource.acm.grammar.AcmSequence(sare.language.acm.resource.acm.grammar.AcmCardinality.ONE, MINIUML_0_0_0_0, MINIUML_0_0_0_1, MINIUML_0_0_0_2, MINIUML_0_0_0_3, MINIUML_0_0_0_4, MINIUML_0_0_0_5);
	public final static sare.language.acm.resource.acm.grammar.AcmChoice MINIUML_0_0 = new sare.language.acm.resource.acm.grammar.AcmChoice(sare.language.acm.resource.acm.grammar.AcmCardinality.ONE, MINIUML_0_0_0);
	public final static sare.language.acm.resource.acm.grammar.AcmRule MINIUML_0 = new sare.language.acm.resource.acm.grammar.AcmRule(org.eclipse.uml2.uml.UMLPackage.eINSTANCE.getModel(), MINIUML_0_0, sare.language.acm.resource.acm.grammar.AcmCardinality.ONE);
	public final static sare.language.acm.resource.acm.grammar.AcmPlaceholder MINIUML_1_0_0_0 = new sare.language.acm.resource.acm.grammar.AcmPlaceholder(org.eclipse.uml2.uml.UMLPackage.eINSTANCE.getPackageImport().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.PACKAGE_IMPORT__IMPORTED_PACKAGE), "QUOTED_34_34", sare.language.acm.resource.acm.grammar.AcmCardinality.ONE, 0);
	public final static sare.language.acm.resource.acm.grammar.AcmSequence MINIUML_1_0_0 = new sare.language.acm.resource.acm.grammar.AcmSequence(sare.language.acm.resource.acm.grammar.AcmCardinality.ONE, MINIUML_1_0_0_0);
	public final static sare.language.acm.resource.acm.grammar.AcmChoice MINIUML_1_0 = new sare.language.acm.resource.acm.grammar.AcmChoice(sare.language.acm.resource.acm.grammar.AcmCardinality.ONE, MINIUML_1_0_0);
	public final static sare.language.acm.resource.acm.grammar.AcmRule MINIUML_1 = new sare.language.acm.resource.acm.grammar.AcmRule(org.eclipse.uml2.uml.UMLPackage.eINSTANCE.getPackageImport(), MINIUML_1_0, sare.language.acm.resource.acm.grammar.AcmCardinality.ONE);
	public final static sare.language.acm.resource.acm.grammar.AcmKeyword MINIUML_2_0_0_0 = new sare.language.acm.resource.acm.grammar.AcmKeyword("package", sare.language.acm.resource.acm.grammar.AcmCardinality.ONE);
	public final static sare.language.acm.resource.acm.grammar.AcmPlaceholder MINIUML_2_0_0_1 = new sare.language.acm.resource.acm.grammar.AcmPlaceholder(org.eclipse.uml2.uml.UMLPackage.eINSTANCE.getPackage().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.PACKAGE__NAME), "TEXT", sare.language.acm.resource.acm.grammar.AcmCardinality.ONE, 0);
	public final static sare.language.acm.resource.acm.grammar.AcmKeyword MINIUML_2_0_0_2 = new sare.language.acm.resource.acm.grammar.AcmKeyword("{", sare.language.acm.resource.acm.grammar.AcmCardinality.ONE);
	public final static sare.language.acm.resource.acm.grammar.AcmContainment MINIUML_2_0_0_3 = new sare.language.acm.resource.acm.grammar.AcmContainment(org.eclipse.uml2.uml.UMLPackage.eINSTANCE.getPackage().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.PACKAGE__PACKAGED_ELEMENT), sare.language.acm.resource.acm.grammar.AcmCardinality.STAR, 0);
	public final static sare.language.acm.resource.acm.grammar.AcmKeyword MINIUML_2_0_0_4 = new sare.language.acm.resource.acm.grammar.AcmKeyword("}", sare.language.acm.resource.acm.grammar.AcmCardinality.ONE);
	public final static sare.language.acm.resource.acm.grammar.AcmSequence MINIUML_2_0_0 = new sare.language.acm.resource.acm.grammar.AcmSequence(sare.language.acm.resource.acm.grammar.AcmCardinality.ONE, MINIUML_2_0_0_0, MINIUML_2_0_0_1, MINIUML_2_0_0_2, MINIUML_2_0_0_3, MINIUML_2_0_0_4);
	public final static sare.language.acm.resource.acm.grammar.AcmChoice MINIUML_2_0 = new sare.language.acm.resource.acm.grammar.AcmChoice(sare.language.acm.resource.acm.grammar.AcmCardinality.ONE, MINIUML_2_0_0);
	public final static sare.language.acm.resource.acm.grammar.AcmRule MINIUML_2 = new sare.language.acm.resource.acm.grammar.AcmRule(org.eclipse.uml2.uml.UMLPackage.eINSTANCE.getPackage(), MINIUML_2_0, sare.language.acm.resource.acm.grammar.AcmCardinality.ONE);
	public final static sare.language.acm.resource.acm.grammar.AcmContainment MINIUML_3_0_0_0 = new sare.language.acm.resource.acm.grammar.AcmContainment(org.eclipse.uml2.uml.UMLPackage.eINSTANCE.getClass_().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.CLASS__OWNED_COMMENT), sare.language.acm.resource.acm.grammar.AcmCardinality.STAR, 0);
	public final static sare.language.acm.resource.acm.grammar.AcmKeyword MINIUML_3_0_0_1 = new sare.language.acm.resource.acm.grammar.AcmKeyword("class", sare.language.acm.resource.acm.grammar.AcmCardinality.ONE);
	public final static sare.language.acm.resource.acm.grammar.AcmPlaceholder MINIUML_3_0_0_2 = new sare.language.acm.resource.acm.grammar.AcmPlaceholder(org.eclipse.uml2.uml.UMLPackage.eINSTANCE.getClass_().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.CLASS__NAME), "TEXT", sare.language.acm.resource.acm.grammar.AcmCardinality.ONE, 0);
	public final static sare.language.acm.resource.acm.grammar.AcmKeyword MINIUML_3_0_0_3_0_0_0 = new sare.language.acm.resource.acm.grammar.AcmKeyword("extends", sare.language.acm.resource.acm.grammar.AcmCardinality.ONE);
	public final static sare.language.acm.resource.acm.grammar.AcmPlaceholder MINIUML_3_0_0_3_0_0_1 = new sare.language.acm.resource.acm.grammar.AcmPlaceholder(org.eclipse.uml2.uml.UMLPackage.eINSTANCE.getClass_().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.CLASS__SUPER_CLASS), "TEXT", sare.language.acm.resource.acm.grammar.AcmCardinality.ONE, 0);
	public final static sare.language.acm.resource.acm.grammar.AcmKeyword MINIUML_3_0_0_3_0_0_2_0_0_0 = new sare.language.acm.resource.acm.grammar.AcmKeyword(",", sare.language.acm.resource.acm.grammar.AcmCardinality.ONE);
	public final static sare.language.acm.resource.acm.grammar.AcmPlaceholder MINIUML_3_0_0_3_0_0_2_0_0_1 = new sare.language.acm.resource.acm.grammar.AcmPlaceholder(org.eclipse.uml2.uml.UMLPackage.eINSTANCE.getClass_().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.CLASS__SUPER_CLASS), "TEXT", sare.language.acm.resource.acm.grammar.AcmCardinality.ONE, 0);
	public final static sare.language.acm.resource.acm.grammar.AcmSequence MINIUML_3_0_0_3_0_0_2_0_0 = new sare.language.acm.resource.acm.grammar.AcmSequence(sare.language.acm.resource.acm.grammar.AcmCardinality.ONE, MINIUML_3_0_0_3_0_0_2_0_0_0, MINIUML_3_0_0_3_0_0_2_0_0_1);
	public final static sare.language.acm.resource.acm.grammar.AcmChoice MINIUML_3_0_0_3_0_0_2_0 = new sare.language.acm.resource.acm.grammar.AcmChoice(sare.language.acm.resource.acm.grammar.AcmCardinality.ONE, MINIUML_3_0_0_3_0_0_2_0_0);
	public final static sare.language.acm.resource.acm.grammar.AcmCompound MINIUML_3_0_0_3_0_0_2 = new sare.language.acm.resource.acm.grammar.AcmCompound(MINIUML_3_0_0_3_0_0_2_0, sare.language.acm.resource.acm.grammar.AcmCardinality.STAR);
	public final static sare.language.acm.resource.acm.grammar.AcmSequence MINIUML_3_0_0_3_0_0 = new sare.language.acm.resource.acm.grammar.AcmSequence(sare.language.acm.resource.acm.grammar.AcmCardinality.ONE, MINIUML_3_0_0_3_0_0_0, MINIUML_3_0_0_3_0_0_1, MINIUML_3_0_0_3_0_0_2);
	public final static sare.language.acm.resource.acm.grammar.AcmChoice MINIUML_3_0_0_3_0 = new sare.language.acm.resource.acm.grammar.AcmChoice(sare.language.acm.resource.acm.grammar.AcmCardinality.ONE, MINIUML_3_0_0_3_0_0);
	public final static sare.language.acm.resource.acm.grammar.AcmCompound MINIUML_3_0_0_3 = new sare.language.acm.resource.acm.grammar.AcmCompound(MINIUML_3_0_0_3_0, sare.language.acm.resource.acm.grammar.AcmCardinality.QUESTIONMARK);
	public final static sare.language.acm.resource.acm.grammar.AcmKeyword MINIUML_3_0_0_4 = new sare.language.acm.resource.acm.grammar.AcmKeyword("{", sare.language.acm.resource.acm.grammar.AcmCardinality.ONE);
	public final static sare.language.acm.resource.acm.grammar.AcmContainment MINIUML_3_0_0_5_0_0_0 = new sare.language.acm.resource.acm.grammar.AcmContainment(org.eclipse.uml2.uml.UMLPackage.eINSTANCE.getClass_().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.CLASS__OWNED_ATTRIBUTE), sare.language.acm.resource.acm.grammar.AcmCardinality.ONE, 0);
	public final static sare.language.acm.resource.acm.grammar.AcmSequence MINIUML_3_0_0_5_0_0 = new sare.language.acm.resource.acm.grammar.AcmSequence(sare.language.acm.resource.acm.grammar.AcmCardinality.ONE, MINIUML_3_0_0_5_0_0_0);
	public final static sare.language.acm.resource.acm.grammar.AcmContainment MINIUML_3_0_0_5_0_1_0 = new sare.language.acm.resource.acm.grammar.AcmContainment(org.eclipse.uml2.uml.UMLPackage.eINSTANCE.getClass_().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.CLASS__OWNED_OPERATION), sare.language.acm.resource.acm.grammar.AcmCardinality.ONE, 0);
	public final static sare.language.acm.resource.acm.grammar.AcmSequence MINIUML_3_0_0_5_0_1 = new sare.language.acm.resource.acm.grammar.AcmSequence(sare.language.acm.resource.acm.grammar.AcmCardinality.ONE, MINIUML_3_0_0_5_0_1_0);
	public final static sare.language.acm.resource.acm.grammar.AcmChoice MINIUML_3_0_0_5_0 = new sare.language.acm.resource.acm.grammar.AcmChoice(sare.language.acm.resource.acm.grammar.AcmCardinality.ONE, MINIUML_3_0_0_5_0_0, MINIUML_3_0_0_5_0_1);
	public final static sare.language.acm.resource.acm.grammar.AcmCompound MINIUML_3_0_0_5 = new sare.language.acm.resource.acm.grammar.AcmCompound(MINIUML_3_0_0_5_0, sare.language.acm.resource.acm.grammar.AcmCardinality.STAR);
	public final static sare.language.acm.resource.acm.grammar.AcmKeyword MINIUML_3_0_0_6 = new sare.language.acm.resource.acm.grammar.AcmKeyword("}", sare.language.acm.resource.acm.grammar.AcmCardinality.ONE);
	public final static sare.language.acm.resource.acm.grammar.AcmSequence MINIUML_3_0_0 = new sare.language.acm.resource.acm.grammar.AcmSequence(sare.language.acm.resource.acm.grammar.AcmCardinality.ONE, MINIUML_3_0_0_0, MINIUML_3_0_0_1, MINIUML_3_0_0_2, MINIUML_3_0_0_3, MINIUML_3_0_0_4, MINIUML_3_0_0_5, MINIUML_3_0_0_6);
	public final static sare.language.acm.resource.acm.grammar.AcmChoice MINIUML_3_0 = new sare.language.acm.resource.acm.grammar.AcmChoice(sare.language.acm.resource.acm.grammar.AcmCardinality.ONE, MINIUML_3_0_0);
	public final static sare.language.acm.resource.acm.grammar.AcmRule MINIUML_3 = new sare.language.acm.resource.acm.grammar.AcmRule(org.eclipse.uml2.uml.UMLPackage.eINSTANCE.getClass_(), MINIUML_3_0, sare.language.acm.resource.acm.grammar.AcmCardinality.ONE);
	public final static sare.language.acm.resource.acm.grammar.AcmContainment MINIUML_4_0_0_0 = new sare.language.acm.resource.acm.grammar.AcmContainment(org.eclipse.uml2.uml.UMLPackage.eINSTANCE.getOperation().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.OPERATION__OWNED_COMMENT), sare.language.acm.resource.acm.grammar.AcmCardinality.STAR, 0);
	public final static sare.language.acm.resource.acm.grammar.AcmPlaceholder MINIUML_4_0_0_1 = new sare.language.acm.resource.acm.grammar.AcmPlaceholder(org.eclipse.uml2.uml.UMLPackage.eINSTANCE.getOperation().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.OPERATION__NAME), "TEXT", sare.language.acm.resource.acm.grammar.AcmCardinality.ONE, 0);
	public final static sare.language.acm.resource.acm.grammar.AcmKeyword MINIUML_4_0_0_2 = new sare.language.acm.resource.acm.grammar.AcmKeyword("(", sare.language.acm.resource.acm.grammar.AcmCardinality.ONE);
	public final static sare.language.acm.resource.acm.grammar.AcmContainment MINIUML_4_0_0_3_0_0_0 = new sare.language.acm.resource.acm.grammar.AcmContainment(org.eclipse.uml2.uml.UMLPackage.eINSTANCE.getOperation().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.OPERATION__OWNED_PARAMETER), sare.language.acm.resource.acm.grammar.AcmCardinality.ONE, 0);
	public final static sare.language.acm.resource.acm.grammar.AcmKeyword MINIUML_4_0_0_3_0_0_1_0_0_0 = new sare.language.acm.resource.acm.grammar.AcmKeyword(",", sare.language.acm.resource.acm.grammar.AcmCardinality.ONE);
	public final static sare.language.acm.resource.acm.grammar.AcmContainment MINIUML_4_0_0_3_0_0_1_0_0_1 = new sare.language.acm.resource.acm.grammar.AcmContainment(org.eclipse.uml2.uml.UMLPackage.eINSTANCE.getOperation().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.OPERATION__OWNED_PARAMETER), sare.language.acm.resource.acm.grammar.AcmCardinality.ONE, 0);
	public final static sare.language.acm.resource.acm.grammar.AcmSequence MINIUML_4_0_0_3_0_0_1_0_0 = new sare.language.acm.resource.acm.grammar.AcmSequence(sare.language.acm.resource.acm.grammar.AcmCardinality.ONE, MINIUML_4_0_0_3_0_0_1_0_0_0, MINIUML_4_0_0_3_0_0_1_0_0_1);
	public final static sare.language.acm.resource.acm.grammar.AcmChoice MINIUML_4_0_0_3_0_0_1_0 = new sare.language.acm.resource.acm.grammar.AcmChoice(sare.language.acm.resource.acm.grammar.AcmCardinality.ONE, MINIUML_4_0_0_3_0_0_1_0_0);
	public final static sare.language.acm.resource.acm.grammar.AcmCompound MINIUML_4_0_0_3_0_0_1 = new sare.language.acm.resource.acm.grammar.AcmCompound(MINIUML_4_0_0_3_0_0_1_0, sare.language.acm.resource.acm.grammar.AcmCardinality.STAR);
	public final static sare.language.acm.resource.acm.grammar.AcmSequence MINIUML_4_0_0_3_0_0 = new sare.language.acm.resource.acm.grammar.AcmSequence(sare.language.acm.resource.acm.grammar.AcmCardinality.ONE, MINIUML_4_0_0_3_0_0_0, MINIUML_4_0_0_3_0_0_1);
	public final static sare.language.acm.resource.acm.grammar.AcmChoice MINIUML_4_0_0_3_0 = new sare.language.acm.resource.acm.grammar.AcmChoice(sare.language.acm.resource.acm.grammar.AcmCardinality.ONE, MINIUML_4_0_0_3_0_0);
	public final static sare.language.acm.resource.acm.grammar.AcmCompound MINIUML_4_0_0_3 = new sare.language.acm.resource.acm.grammar.AcmCompound(MINIUML_4_0_0_3_0, sare.language.acm.resource.acm.grammar.AcmCardinality.QUESTIONMARK);
	public final static sare.language.acm.resource.acm.grammar.AcmKeyword MINIUML_4_0_0_4 = new sare.language.acm.resource.acm.grammar.AcmKeyword(")", sare.language.acm.resource.acm.grammar.AcmCardinality.ONE);
	public final static sare.language.acm.resource.acm.grammar.AcmKeyword MINIUML_4_0_0_5_0_0_0 = new sare.language.acm.resource.acm.grammar.AcmKeyword(":", sare.language.acm.resource.acm.grammar.AcmCardinality.ONE);
	public final static sare.language.acm.resource.acm.grammar.AcmPlaceholder MINIUML_4_0_0_5_0_0_1 = new sare.language.acm.resource.acm.grammar.AcmPlaceholder(org.eclipse.uml2.uml.UMLPackage.eINSTANCE.getOperation().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.OPERATION__TYPE), "TEXT", sare.language.acm.resource.acm.grammar.AcmCardinality.ONE, 0);
	public final static sare.language.acm.resource.acm.grammar.AcmSequence MINIUML_4_0_0_5_0_0 = new sare.language.acm.resource.acm.grammar.AcmSequence(sare.language.acm.resource.acm.grammar.AcmCardinality.ONE, MINIUML_4_0_0_5_0_0_0, MINIUML_4_0_0_5_0_0_1);
	public final static sare.language.acm.resource.acm.grammar.AcmChoice MINIUML_4_0_0_5_0 = new sare.language.acm.resource.acm.grammar.AcmChoice(sare.language.acm.resource.acm.grammar.AcmCardinality.ONE, MINIUML_4_0_0_5_0_0);
	public final static sare.language.acm.resource.acm.grammar.AcmCompound MINIUML_4_0_0_5 = new sare.language.acm.resource.acm.grammar.AcmCompound(MINIUML_4_0_0_5_0, sare.language.acm.resource.acm.grammar.AcmCardinality.QUESTIONMARK);
	public final static sare.language.acm.resource.acm.grammar.AcmSequence MINIUML_4_0_0 = new sare.language.acm.resource.acm.grammar.AcmSequence(sare.language.acm.resource.acm.grammar.AcmCardinality.ONE, MINIUML_4_0_0_0, MINIUML_4_0_0_1, MINIUML_4_0_0_2, MINIUML_4_0_0_3, MINIUML_4_0_0_4, MINIUML_4_0_0_5);
	public final static sare.language.acm.resource.acm.grammar.AcmChoice MINIUML_4_0 = new sare.language.acm.resource.acm.grammar.AcmChoice(sare.language.acm.resource.acm.grammar.AcmCardinality.ONE, MINIUML_4_0_0);
	public final static sare.language.acm.resource.acm.grammar.AcmRule MINIUML_4 = new sare.language.acm.resource.acm.grammar.AcmRule(org.eclipse.uml2.uml.UMLPackage.eINSTANCE.getOperation(), MINIUML_4_0, sare.language.acm.resource.acm.grammar.AcmCardinality.ONE);
	public final static sare.language.acm.resource.acm.grammar.AcmContainment MINIUML_5_0_0_0 = new sare.language.acm.resource.acm.grammar.AcmContainment(org.eclipse.uml2.uml.UMLPackage.eINSTANCE.getProperty().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.PROPERTY__OWNED_COMMENT), sare.language.acm.resource.acm.grammar.AcmCardinality.STAR, 0);
	public final static sare.language.acm.resource.acm.grammar.AcmPlaceholder MINIUML_5_0_0_1 = new sare.language.acm.resource.acm.grammar.AcmPlaceholder(org.eclipse.uml2.uml.UMLPackage.eINSTANCE.getProperty().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.PROPERTY__NAME), "TEXT", sare.language.acm.resource.acm.grammar.AcmCardinality.ONE, 0);
	public final static sare.language.acm.resource.acm.grammar.AcmKeyword MINIUML_5_0_0_2 = new sare.language.acm.resource.acm.grammar.AcmKeyword(":", sare.language.acm.resource.acm.grammar.AcmCardinality.ONE);
	public final static sare.language.acm.resource.acm.grammar.AcmPlaceholder MINIUML_5_0_0_3 = new sare.language.acm.resource.acm.grammar.AcmPlaceholder(org.eclipse.uml2.uml.UMLPackage.eINSTANCE.getProperty().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.PROPERTY__TYPE), "TEXT", sare.language.acm.resource.acm.grammar.AcmCardinality.ONE, 0);
	public final static sare.language.acm.resource.acm.grammar.AcmSequence MINIUML_5_0_0 = new sare.language.acm.resource.acm.grammar.AcmSequence(sare.language.acm.resource.acm.grammar.AcmCardinality.ONE, MINIUML_5_0_0_0, MINIUML_5_0_0_1, MINIUML_5_0_0_2, MINIUML_5_0_0_3);
	public final static sare.language.acm.resource.acm.grammar.AcmChoice MINIUML_5_0 = new sare.language.acm.resource.acm.grammar.AcmChoice(sare.language.acm.resource.acm.grammar.AcmCardinality.ONE, MINIUML_5_0_0);
	public final static sare.language.acm.resource.acm.grammar.AcmRule MINIUML_5 = new sare.language.acm.resource.acm.grammar.AcmRule(org.eclipse.uml2.uml.UMLPackage.eINSTANCE.getProperty(), MINIUML_5_0, sare.language.acm.resource.acm.grammar.AcmCardinality.ONE);
	public final static sare.language.acm.resource.acm.grammar.AcmContainment MINIUML_6_0_0_0 = new sare.language.acm.resource.acm.grammar.AcmContainment(org.eclipse.uml2.uml.UMLPackage.eINSTANCE.getParameter().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.PARAMETER__OWNED_COMMENT), sare.language.acm.resource.acm.grammar.AcmCardinality.STAR, 0);
	public final static sare.language.acm.resource.acm.grammar.AcmPlaceholder MINIUML_6_0_0_1 = new sare.language.acm.resource.acm.grammar.AcmPlaceholder(org.eclipse.uml2.uml.UMLPackage.eINSTANCE.getParameter().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.PARAMETER__NAME), "TEXT", sare.language.acm.resource.acm.grammar.AcmCardinality.ONE, 0);
	public final static sare.language.acm.resource.acm.grammar.AcmKeyword MINIUML_6_0_0_2 = new sare.language.acm.resource.acm.grammar.AcmKeyword(":", sare.language.acm.resource.acm.grammar.AcmCardinality.ONE);
	public final static sare.language.acm.resource.acm.grammar.AcmPlaceholder MINIUML_6_0_0_3 = new sare.language.acm.resource.acm.grammar.AcmPlaceholder(org.eclipse.uml2.uml.UMLPackage.eINSTANCE.getParameter().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.PARAMETER__TYPE), "TEXT", sare.language.acm.resource.acm.grammar.AcmCardinality.ONE, 0);
	public final static sare.language.acm.resource.acm.grammar.AcmSequence MINIUML_6_0_0 = new sare.language.acm.resource.acm.grammar.AcmSequence(sare.language.acm.resource.acm.grammar.AcmCardinality.ONE, MINIUML_6_0_0_0, MINIUML_6_0_0_1, MINIUML_6_0_0_2, MINIUML_6_0_0_3);
	public final static sare.language.acm.resource.acm.grammar.AcmChoice MINIUML_6_0 = new sare.language.acm.resource.acm.grammar.AcmChoice(sare.language.acm.resource.acm.grammar.AcmCardinality.ONE, MINIUML_6_0_0);
	public final static sare.language.acm.resource.acm.grammar.AcmRule MINIUML_6 = new sare.language.acm.resource.acm.grammar.AcmRule(org.eclipse.uml2.uml.UMLPackage.eINSTANCE.getParameter(), MINIUML_6_0, sare.language.acm.resource.acm.grammar.AcmCardinality.ONE);
	public final static sare.language.acm.resource.acm.grammar.AcmKeyword MINIUML_7_0_0_0 = new sare.language.acm.resource.acm.grammar.AcmKeyword("\u00ab", sare.language.acm.resource.acm.grammar.AcmCardinality.ONE);
	public final static sare.language.acm.resource.acm.grammar.AcmPlaceholder MINIUML_7_0_0_1 = new sare.language.acm.resource.acm.grammar.AcmPlaceholder(org.eclipse.uml2.uml.UMLPackage.eINSTANCE.getComment().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.COMMENT__BODY), "TEXT", sare.language.acm.resource.acm.grammar.AcmCardinality.ONE, 0);
	public final static sare.language.acm.resource.acm.grammar.AcmKeyword MINIUML_7_0_0_2 = new sare.language.acm.resource.acm.grammar.AcmKeyword("\u00bb", sare.language.acm.resource.acm.grammar.AcmCardinality.ONE);
	public final static sare.language.acm.resource.acm.grammar.AcmSequence MINIUML_7_0_0 = new sare.language.acm.resource.acm.grammar.AcmSequence(sare.language.acm.resource.acm.grammar.AcmCardinality.ONE, MINIUML_7_0_0_0, MINIUML_7_0_0_1, MINIUML_7_0_0_2);
	public final static sare.language.acm.resource.acm.grammar.AcmKeyword MINIUML_7_0_1_0 = new sare.language.acm.resource.acm.grammar.AcmKeyword("<<", sare.language.acm.resource.acm.grammar.AcmCardinality.ONE);
	public final static sare.language.acm.resource.acm.grammar.AcmPlaceholder MINIUML_7_0_1_1 = new sare.language.acm.resource.acm.grammar.AcmPlaceholder(org.eclipse.uml2.uml.UMLPackage.eINSTANCE.getComment().getEStructuralFeature(org.eclipse.uml2.uml.UMLPackage.COMMENT__BODY), "TEXT", sare.language.acm.resource.acm.grammar.AcmCardinality.ONE, 0);
	public final static sare.language.acm.resource.acm.grammar.AcmKeyword MINIUML_7_0_1_2 = new sare.language.acm.resource.acm.grammar.AcmKeyword(">>", sare.language.acm.resource.acm.grammar.AcmCardinality.ONE);
	public final static sare.language.acm.resource.acm.grammar.AcmSequence MINIUML_7_0_1 = new sare.language.acm.resource.acm.grammar.AcmSequence(sare.language.acm.resource.acm.grammar.AcmCardinality.ONE, MINIUML_7_0_1_0, MINIUML_7_0_1_1, MINIUML_7_0_1_2);
	public final static sare.language.acm.resource.acm.grammar.AcmChoice MINIUML_7_0 = new sare.language.acm.resource.acm.grammar.AcmChoice(sare.language.acm.resource.acm.grammar.AcmCardinality.ONE, MINIUML_7_0_0, MINIUML_7_0_1);
	public final static sare.language.acm.resource.acm.grammar.AcmRule MINIUML_7 = new sare.language.acm.resource.acm.grammar.AcmRule(org.eclipse.uml2.uml.UMLPackage.eINSTANCE.getComment(), MINIUML_7_0, sare.language.acm.resource.acm.grammar.AcmCardinality.ONE);
	
	public final static sare.language.acm.resource.acm.grammar.AcmRule[] RULES = new sare.language.acm.resource.acm.grammar.AcmRule[] {
		ACM_0,
		ACM_1,
		ACM_2,
		ACM_3,
		MINIUML_0,
		MINIUML_1,
		MINIUML_2,
		MINIUML_3,
		MINIUML_4,
		MINIUML_5,
		MINIUML_6,
		MINIUML_7,
	};
	
	/**
	 * Returns all keywords of the grammar. This includes all literals for boolean and
	 * enumeration terminals.
	 */
	public java.util.Set<String> getKeywords() {
		if (this.keywords == null) {
			this.keywords = new java.util.LinkedHashSet<String>();
			for (sare.language.acm.resource.acm.grammar.AcmRule rule : RULES) {
				findKeywords(rule, this.keywords);
			}
		}
		return keywords;
	}
	
	/**
	 * Finds all keywords in the given element and its children and adds them to the
	 * set. This includes all literals for boolean and enumeration terminals.
	 */
	private void findKeywords(sare.language.acm.resource.acm.grammar.AcmSyntaxElement element, java.util.Set<String> keywords) {
		if (element instanceof sare.language.acm.resource.acm.grammar.AcmKeyword) {
			keywords.add(((sare.language.acm.resource.acm.grammar.AcmKeyword) element).getValue());
		} else if (element instanceof sare.language.acm.resource.acm.grammar.AcmBooleanTerminal) {
			keywords.add(((sare.language.acm.resource.acm.grammar.AcmBooleanTerminal) element).getTrueLiteral());
			keywords.add(((sare.language.acm.resource.acm.grammar.AcmBooleanTerminal) element).getFalseLiteral());
		} else if (element instanceof sare.language.acm.resource.acm.grammar.AcmEnumerationTerminal) {
			sare.language.acm.resource.acm.grammar.AcmEnumerationTerminal terminal = (sare.language.acm.resource.acm.grammar.AcmEnumerationTerminal) element;
			for (String key : terminal.getLiteralMapping().keySet()) {
				keywords.add(key);
			}
		}
		for (sare.language.acm.resource.acm.grammar.AcmSyntaxElement child : element.getChildren()) {
			findKeywords(child, this.keywords);
		}
	}
	
}
