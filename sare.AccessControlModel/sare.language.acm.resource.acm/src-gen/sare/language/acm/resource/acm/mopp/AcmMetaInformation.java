/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package sare.language.acm.resource.acm.mopp;

public class AcmMetaInformation implements sare.language.acm.resource.acm.IAcmMetaInformation {
	
	public String getSyntaxName() {
		return "acm";
	}
	
	public String getURI() {
		return "http://sare/language/acm";
	}
	
	public sare.language.acm.resource.acm.IAcmTextScanner createLexer() {
		return new sare.language.acm.resource.acm.mopp.AcmAntlrScanner(new sare.language.acm.resource.acm.mopp.AcmLexer());
	}
	
	public sare.language.acm.resource.acm.IAcmTextParser createParser(java.io.InputStream inputStream, String encoding) {
		return new sare.language.acm.resource.acm.mopp.AcmParser().createInstance(inputStream, encoding);
	}
	
	public sare.language.acm.resource.acm.IAcmTextPrinter createPrinter(java.io.OutputStream outputStream, sare.language.acm.resource.acm.IAcmTextResource resource) {
		return new sare.language.acm.resource.acm.mopp.AcmPrinter2(outputStream, resource);
	}
	
	public org.eclipse.emf.ecore.EClass[] getClassesWithSyntax() {
		return new sare.language.acm.resource.acm.mopp.AcmSyntaxCoverageInformationProvider().getClassesWithSyntax();
	}
	
	public org.eclipse.emf.ecore.EClass[] getStartSymbols() {
		return new sare.language.acm.resource.acm.mopp.AcmSyntaxCoverageInformationProvider().getStartSymbols();
	}
	
	public sare.language.acm.resource.acm.IAcmReferenceResolverSwitch getReferenceResolverSwitch() {
		return new sare.language.acm.resource.acm.mopp.AcmReferenceResolverSwitch();
	}
	
	public sare.language.acm.resource.acm.IAcmTokenResolverFactory getTokenResolverFactory() {
		return new sare.language.acm.resource.acm.mopp.AcmTokenResolverFactory();
	}
	
	public String getPathToCSDefinition() {
		return "sare.language.acm/metamodel/acm.cs";
	}
	
	public String[] getTokenNames() {
		return new sare.language.acm.resource.acm.mopp.AcmParser(null).getTokenNames();
	}
	
	public sare.language.acm.resource.acm.IAcmTokenStyle getDefaultTokenStyle(String tokenName) {
		return new sare.language.acm.resource.acm.mopp.AcmTokenStyleInformationProvider().getDefaultTokenStyle(tokenName);
	}
	
	public java.util.Collection<sare.language.acm.resource.acm.IAcmBracketPair> getBracketPairs() {
		return new sare.language.acm.resource.acm.mopp.AcmBracketInformationProvider().getBracketPairs();
	}
	
	public org.eclipse.emf.ecore.EClass[] getFoldableClasses() {
		return new sare.language.acm.resource.acm.mopp.AcmFoldingInformationProvider().getFoldableClasses();
	}
	
	public org.eclipse.emf.ecore.resource.Resource.Factory createResourceFactory() {
		return new sare.language.acm.resource.acm.mopp.AcmResourceFactory();
	}
	
	public sare.language.acm.resource.acm.mopp.AcmNewFileContentProvider getNewFileContentProvider() {
		return new sare.language.acm.resource.acm.mopp.AcmNewFileContentProvider();
	}
	
	public void registerResourceFactory() {
		org.eclipse.emf.ecore.resource.Resource.Factory.Registry.INSTANCE.getExtensionToFactoryMap().put(getSyntaxName(), new sare.language.acm.resource.acm.mopp.AcmResourceFactory());
	}
	
	/**
	 * Returns the key of the option that can be used to register a preprocessor that
	 * is used as a pipe when loading resources. This key is language-specific. To
	 * register one preprocessor for multiple resource types, it must be registered
	 * individually using all keys.
	 */
	public String getInputStreamPreprocessorProviderOptionKey() {
		return getSyntaxName() + "_" + "INPUT_STREAM_PREPROCESSOR_PROVIDER";
	}
	
	/**
	 * Returns the key of the option that can be used to register a post-processors
	 * that are invoked after loading resources. This key is language-specific. To
	 * register one post-processor for multiple resource types, it must be registered
	 * individually using all keys.
	 */
	public String getResourcePostProcessorProviderOptionKey() {
		return getSyntaxName() + "_" + "RESOURCE_POSTPROCESSOR_PROVIDER";
	}
	
	public String getLaunchConfigurationType() {
		return "sare.language.acm.resource.acm.ui.launchConfigurationType";
	}
	
}
