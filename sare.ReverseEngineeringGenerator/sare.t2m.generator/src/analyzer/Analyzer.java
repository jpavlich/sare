package analyzer;

import java.util.ArrayList;
import java.util.List;
import java.io.File;
import java.io.IOException;


import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.emftext.commons.layout.LayoutInformation;
import org.emftext.language.java.annotations.impl.AnnotationInstanceImpl;
import org.emftext.language.java.classifiers.impl.ClassImpl;
import org.emftext.language.java.containers.impl.CompilationUnitImpl;
import org.emftext.language.java.members.Method;
import org.emftext.language.java.modifiers.AnnotationInstanceOrModifier;

import acm.Role;
import acm.impl.AccessControlModelImpl;
import analyzer.constraints.AbsentPermission;
import analyzer.constraints.AbsentRole;
import analyzer.constraints.DuplicatedPermission;
import analyzer.constraints.InconsistentPermission;
import analyzer.constraints.Syntax;
import analyzer.constraints.UnprotectedMethod;
import analyzer.constraints.UnassignedPermission;

public class Analyzer {
    
    private List<ClassImpl> classes = new ArrayList<ClassImpl>();
    private AccessControlModelImpl accessControlModel;
    private List<Boolean> errors=new ArrayList<Boolean>();
    private List<Boolean> warnings=new ArrayList<Boolean>();
    
    public void analyze(ResourceSet rs,File file,String OriginalModelFile) throws IOException{
        
        SetUp(rs,file,OriginalModelFile);// carga Roles y Clases
        
        System.out.println("#############################################################################");
        System.out.println("-----------------------------------------------------------------------------");
        System.out.println("--->          Static Code Analyzer for Access Control Policies           <---");
        System.out.println("-----------------------------------------------------------------------------");
        for(ClassImpl currentClass:classes){
        System.out.println("-----------------------------------------------------------------------------");
        System.out.println("CLASS: "+currentClass.getName()); 
        System.out.println("-----------------------------------------------------------------------------");
          EList<Method> methods=currentClass.getMethods();
          for (Method method:methods){
              
              //Constrain: Annotation syntax (ERROR)
              analyzer.constraints.Constraint constrainSE = new Syntax();
              errors.add(constrainSE.execute(method, accessControlModel));
              //Constrain: Annotation syntax (ERROR)
             
              //Constrain: Duplicate permission  (ERROR)
              analyzer.constraints.Constraint constrainDP = new DuplicatedPermission();
              errors.add(constrainDP.execute(method, accessControlModel));
              //Constrain: Duplicate permission  (ERROR)
              
              //Constrain: Inconsistent permission (ERROR)
              analyzer.constraints.Constraint constrainIP = new InconsistentPermission();
              errors.add(constrainIP.execute(method, accessControlModel));
              //Constrain: Inconsistent permission (ERROR)
              
              //Constrain: Absent Permission (ERROR)
              analyzer.constraints.Constraint constrainAP = new AbsentPermission();
              errors.add(constrainAP.execute(method, accessControlModel));
              //Constrain: Absent Permission (ERROR)
          }
          
          for (Method method:methods){
         
              //Constrain: find unprotected methods (WARNING)
              analyzer.constraints.Constraint constrainUM = new UnprotectedMethod();
              warnings.add(constrainUM.execute(method, null));
              //Constrain: find unprotected methods (WARNING)
     
              //Constrain: Role doest not exist in original model (WARNING)
              analyzer.constraints.Constraint constrainAR = new AbsentRole();
              warnings.add(constrainAR.execute(method, accessControlModel));
              //Constrain: Role doest not exist in original model (WARNING)
              
              //Constrain: Permission not assigned to the role in original model(WARNING)
              analyzer.constraints.Constraint constrainUP = new UnassignedPermission();
              warnings.add( constrainUP.execute(method, accessControlModel));
              //Constrain: Permission not assigned to the role in original model(WARNING)
              
          }
          
        }
        System.out.println("#############################################################################");
    }
    
    // Carga las clases del modelo generado por JaMoPP y los roles existentes en el modelo original 
    public void SetUp(ResourceSet rs,File file,String OriginalModelFile) throws IOException {
        LoadModel loadModel=new LoadModel();
        Resource originalModel=loadModel.doLoadXMIFromAbsolutePath(OriginalModelFile);
        List<EObject> contents=originalModel.getContents();
        for(EObject eObj :contents){
            if(eObj.getClass().equals(AccessControlModelImpl.class)){
                accessControlModel=(AccessControlModelImpl)eObj;
            }
        }
        URI outUri = URI.createFileURI(file.getCanonicalPath());
        Resource jamoppcModel = rs.getResource(outUri, true);
        List<EObject> classes= loadClasses(jamoppcModel);
        classesTransformation(classes);
    }
    

    //Carga las clases del modelo generado por JaMoPP
    public List<EObject> loadClasses(Resource resource){
        List<EObject> list=new ArrayList<EObject>();
        for(EObject eObj: resource.getContents()){
            if(eObj.getClass().equals(CompilationUnitImpl.class) ){
                CompilationUnitImpl compilationUnit=(CompilationUnitImpl)eObj;
                if (isClass(compilationUnit.getName())){
                    if (!list.contains(eObj)){
                        list.add(eObj);
                    }
                }
            } 
        }
        return list;
    }
    
    //Transforma todas las EObjects que respresntan clases a Objetos de tipo ClassImp
    public void classesTransformation(List<EObject> classesList){
        for(EObject obj: classesList){
            for(EObject objContent: obj.eContents()){
                if(objContent.getClass().equals(ClassImpl.class)){   
                    ClassImpl clase=(ClassImpl)objContent;
                    if(hasComment(clase,"Stateful")){
                        classes.add(clase);
                    }
                }
            }
        }
    }
    
    public boolean hasComment(ClassImpl claseimp,String comment){
        EList<AnnotationInstanceOrModifier> anotations=claseimp.getAnnotationsAndModifiers();
        for(AnnotationInstanceOrModifier anotationOrModifier: anotations){ 
            if(anotationOrModifier.getClass().equals(AnnotationInstanceImpl.class)){
                AnnotationInstanceImpl anotationInstance=(AnnotationInstanceImpl)anotationOrModifier;
                EList<LayoutInformation> layourInformationList= anotationInstance.getLayoutInformations();
                for(LayoutInformation ly:layourInformationList){
                    if(ly.getVisibleTokenText().equals(comment)){
                        return true;
                    }
                }
            }
        }
        return false;
    }
    
    //Determina si una clase existente en el resource es una clase del codigo que se debe analizar
    public boolean isClass(String nombre){
        if(nombre.contains("entities")){
            return true;
        }
        return false;
    }

    public List<Boolean> getErrors() {
        return errors;
    }

    public void setErrors(List<Boolean> errors) {
        this.errors = errors;
    }

    public List<Boolean> getWarnings() {
        return warnings;
    }

    public void setWarnings(List<Boolean> warnings) {
        this.warnings = warnings;
    }   
    
    
}