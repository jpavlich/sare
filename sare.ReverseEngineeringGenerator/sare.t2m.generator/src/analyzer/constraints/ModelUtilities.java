package analyzer.constraints;


import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.eclipse.emf.common.util.EList;
import org.emftext.commons.layout.LayoutInformation;
import org.emftext.commons.layout.impl.AttributeLayoutInformationImpl;
import org.emftext.language.java.generics.QualifiedTypeArgument;
import org.emftext.language.java.generics.TypeArgument;
import org.emftext.language.java.members.impl.ClassMethodImpl;
import org.emftext.language.java.modifiers.AnnotationInstanceOrModifier;
import org.emftext.language.java.modifiers.impl.PrivateImpl;
import org.emftext.language.java.modifiers.impl.ProtectedImpl;
import org.emftext.language.java.modifiers.impl.PublicImpl;
import org.emftext.language.java.parameters.Parameter;
import org.emftext.language.java.types.ClassifierReference;
import org.emftext.language.java.types.TypeReference;
import org.emftext.language.java.types.impl.NamespaceClassifierReferenceImpl;
import acm.AccessControlModel;
import acm.Role;
import acm.Subject;
import acm.impl.RoleImpl;


public class ModelUtilities {

    
    // carga la lista de roles que exiten en el modelo original 
    public List<Role>  loadRoles(AccessControlModel accessControlModel) throws IOException {
        List<Role> roles = new ArrayList<Role>();
        accessControlModel.getElements();
        List<Subject> rolesAux=accessControlModel.getElements();
        for (Subject subject:rolesAux){
            if (subject.getClass().equals(RoleImpl.class)){
                RoleImpl role = (RoleImpl)subject;
                roles.add(role);
            }
        }  
        return roles;
    }
    
    //retorna la firma completa de un metodo 
    public String getMethodFirm(ClassMethodImpl method){
        String firm=getVisibilityMethod(method)+" "+getTypeMehod(method)+" "+getMethodName(method)+"("+buildParameter(method)+")";
        return firm;
    }

    
    //###############################################################################
    //retornam el nombre de un metodo 
    public String getMethodName(ClassMethodImpl method){
        EList<LayoutInformation> layoutInformations=  method.getLayoutInformations();
        for(LayoutInformation layoutInformation: layoutInformations){
            if (layoutInformation.getClass().equals(AttributeLayoutInformationImpl.class)){
               return layoutInformation.getVisibleTokenText();
            }
        }
        return null;
    }
    
    //retorna el tipo de metodo como String
    public String getTypeMehod(ClassMethodImpl method){
        TypeReference typeReference=method.getTypeReference();
        String tipo =getTypeReference(typeReference);
        if (isList(method)){
            tipo= "List<"+tipo+">";
        }
        return tipo;
    }
    
    // retorna el tipo de un metodo, usado para tipo de retrno y tipo del parametro 
    public String getTypeReference(TypeReference typeReference){
        EList<LayoutInformation> typeReferenceLayoutInformations=typeReference.getLayoutInformations();
        if (typeReferenceLayoutInformations.size()>0){
            for(LayoutInformation layoutInformation:typeReferenceLayoutInformations){
                return layoutInformation.getVisibleTokenText();
            }   
        }
        if (typeReference.getClass().equals(NamespaceClassifierReferenceImpl.class)){
            NamespaceClassifierReferenceImpl namespaceClassifierReference=(NamespaceClassifierReferenceImpl)typeReference;
            EList<ClassifierReference> nmly=namespaceClassifierReference.getClassifierReferences();
            for (ClassifierReference classifierReference:nmly){
                EList<LayoutInformation> classifierReferenceLayoutInformations= classifierReference.getLayoutInformations();
                if (classifierReferenceLayoutInformations.size()==1){
                    for(LayoutInformation layoutInformation:classifierReferenceLayoutInformations){
                        return layoutInformation.getVisibleTokenText();
                    }   
                }else{
                    EList<TypeArgument> typeArguments= classifierReference.getTypeArguments();
                    for(TypeArgument typeArgument:typeArguments){
                        QualifiedTypeArgument qualifiedTypeArgument=(QualifiedTypeArgument)typeArgument;
                        TypeReference typeReference2 =qualifiedTypeArgument.getTypeReference();
                        if (typeReference2.getClass().equals(NamespaceClassifierReferenceImpl.class)){
                            NamespaceClassifierReferenceImpl namespaceClassifierReference2=(NamespaceClassifierReferenceImpl)typeReference2;
                            EList<ClassifierReference> classifierReferences=namespaceClassifierReference2.getClassifierReferences();
                            for (ClassifierReference classifierReference2:classifierReferences){
                                EList<LayoutInformation> layoutInforations= classifierReference2.getLayoutInformations();
                                if (layoutInforations.size()==1){
                                    for(LayoutInformation layoutInformation:layoutInforations){
                                        return layoutInformation.getVisibleTokenText();
                                    }   
                                }
                        }  
                    } 
                }   
             }
           }
        }
        return null;
    }
    
    //retorna si el parametro o el tipo de retorno de un parametro o de un metodo es una lista
    public boolean isList(ClassMethodImpl method){
        TypeReference typeReference=method.getTypeReference();
        return isListParamentro(typeReference);
    }
    
    //retorna si el parametro o el tipo de retorno de un parametro o de un metodo es una lista
    public boolean  isListParamentro(TypeReference typeReference){
        if (typeReference.getClass().equals(NamespaceClassifierReferenceImpl.class)){
            NamespaceClassifierReferenceImpl namespaceClassifier=(NamespaceClassifierReferenceImpl)typeReference;
            EList<ClassifierReference> classifierReferences=namespaceClassifier.getClassifierReferences();
            for (ClassifierReference classifierReferece:classifierReferences){
               EList<LayoutInformation> classifierReferenceLayoutInformations= classifierReferece.getLayoutInformations();
                 for(LayoutInformation layoutInformation: classifierReferenceLayoutInformations){
                     if(layoutInformation.getVisibleTokenText().equals("List")){
                        return true;
                     }
                 }
            }
        }
        return false;
    }
    
    //retorna la visibilidada de un metodo
    public String getVisibilityMethod(ClassMethodImpl method){
        EList<AnnotationInstanceOrModifier> anotations=method.getAnnotationsAndModifiers();
        String type="";
        for(AnnotationInstanceOrModifier anot: anotations){
            if(anot.getClass().equals(PrivateImpl.class)) {
                type="private";
            }
            if(anot.getClass().equals(PublicImpl.class)) {
                type="public";
            }
            if(anot.getClass().equals(ProtectedImpl.class)) {
                type="protected";
            }
        }
            return type;  
    }
    
    //construye los parametros de un metodo en String
    public String buildParameter(ClassMethodImpl method){
        String parameter="";
        EList<Parameter> parameters =method.getParameters();
        for (Parameter param: parameters){
            String name =param.getName();
            TypeReference typeReference=param.getTypeReference();
            String typeString=getTypeReference(typeReference);
            
            if( isListParamentro(typeReference)){
                parameter="List<"+typeString+"> "+name;
            }else{
                parameter = typeString+" "+name;
            }
        }
        return parameter;
     }
    
    
    
}
