package analyzer.constraints;


import java.util.ArrayList;
import java.util.List;
import org.eclipse.emf.common.util.EList;
import org.emftext.commons.layout.LayoutInformation;
import org.emftext.language.java.annotations.SingleAnnotationParameter;
import org.emftext.language.java.annotations.impl.AnnotationInstanceImpl;
import org.emftext.language.java.classifiers.impl.ClassImpl;
import org.emftext.language.java.containers.impl.CompilationUnitImpl;
import org.emftext.language.java.members.Method;
import org.emftext.language.java.members.impl.ClassMethodImpl;
import org.emftext.language.java.modifiers.AnnotationInstanceOrModifier;
import org.emftext.language.java.references.impl.StringReferenceImpl;
import acm.AccessControlModel;
import acm.Permission;
import acm.Role;
import acm.Subject;
import acm.impl.RoleImpl;

public class UnassignedPermission implements Constraint <Method, AccessControlModel>{

    public boolean execute(Method method, AccessControlModel model) {
        ModelUtilities modelutilities=new ModelUtilities();
        AnnotationInstanceOrModifier anotationOrModifier=getRestrictAnotation(method.getAnnotationsAndModifiers());
        if(anotationOrModifier!=null){ 
            String rolString=getRolName(anotationOrModifier);
            int limit=rolesNumber(rolString)/2;
            if(validateString(rolString))
            {
            List<String> roleList =splitRolString(rolString,limit);
            for(String roleString : roleList){
                return validatePermission(model, roleString,method, modelutilities);     
            }
            }
        }
        return true;
    }
    
    // se encarga de buscar en cada rol en el modelo de control de acceso si existe todos los permisos est�n referenciados en la anotacion
    public boolean validatePermission(acm.AccessControlModel accessControlModel,String roleName,Method method,ModelUtilities modelutilities){
        char[] string= roleName.toCharArray();
        int flag=0;;
        
        for(int i=0;i<roleName.length();i++){
            if(string[i]=='_'){
                flag=i;
            }
        }
        String name=roleName.substring(0, flag);
        String methodName=roleName.substring(flag+1);
        
        if (!method.getName().equals(methodName)){
            return true;
        }else{

        ClassImpl clase=(ClassImpl)method.getParentConcreteClassifier();
        CompilationUnitImpl container=(CompilationUnitImpl)clase.eContainer();
        List<String> permissions=getPermissionsNames(container.getName());
        permissions.add(method.getName());
        EList<Subject> subjects=accessControlModel.getElements();
        for (Subject subject:subjects){
            if(subject.getClass().equals(acm.impl.RoleImpl.class)){
                RoleImpl role=(RoleImpl)subject;
                if(role.getName().equals(name)){
                    return findPermission(role, permissions,modelutilities, method);
                }
            }
        }
        }
        return true;
    }
   
    //se encarga de buscar si el permiso presente en la anotaci�n existe en el modelo de control de acceso
    public boolean findPermission(Role role, List<String> permissions,ModelUtilities modelutilities,Method method){
        EList<Permission> ListPermission= role.getPermissions();
        String permissionName= permissions.get(0);
        permissions.remove(0);
        Permission perm=getPermission(ListPermission,permissionName);
            if(perm==null){
                return false;
            }else{
                return findPermissionRecursion(perm, permissions,role, modelutilities, method);
            }
    }
    
    //se encarga de buscar si el permiso presente en la anotaci�n existe en el modelo de control de acceso
    public boolean findPermissionRecursion(Permission permission, List<String> permissions,Role role, ModelUtilities modelutilities,Method method){
        EList<Permission> ListPermission= permission.getChildPermissions();
        String permissionName= permissions.get(0);
        permissions.remove(0);
        Permission perm=getPermission(ListPermission,permissionName);
        if(perm==null){
            System.out.println("WARNING!: The role "+role.getName() +" does not have permission over the method ->"+"  Method: "+ modelutilities.getMethodFirm((ClassMethodImpl) method));
            return false;
        }
        if (permissions.isEmpty() && !perm.getElement().getName().equals(permissionName)){
            return false;
        }
        if (permissions.isEmpty() && perm.getElement().getName().equals(permissionName)){
            return true;
        }
         return  findPermissionRecursion(perm, permissions,role, modelutilities, method);
        
    }
    
    public Permission getPermission(EList<Permission> ListPermission,String name){
        for (Permission permission :ListPermission){
            if(permission.getElement().getName().equals(name)){
                return permission;
            }
        }
        return null;
    }
    
    //se encarga de obtener la anotaci�n de la restricci�n de seguridad existente en el m�todo
    public AnnotationInstanceImpl getRestrictAnotation(EList<AnnotationInstanceOrModifier> anotations){
        for(AnnotationInstanceOrModifier anotation: anotations){ 
        if(anotation.getClass().equals(AnnotationInstanceImpl.class)){
            AnnotationInstanceImpl anotatationImpl=(AnnotationInstanceImpl)anotation;
            EList<LayoutInformation> anotationStrings= anotatationImpl.getLayoutInformations();
            for(LayoutInformation layoutInformation :anotationStrings){
                if(layoutInformation.getVisibleTokenText().equals("Restrict")){
                    return anotatationImpl;
                }
            }
        }
        }
        return null;
    }
    
    //Identifica el numero de roles en una anotacion 
    protected static int rolesNumber(String roleName){
        char[] string= roleName.toCharArray();
        int token=0;
        for(int i=0;i<string.length;i++){
            if (string[i]=='\''){
                token++;
            } 
        }
        return token;
    }
    
    //se encarga de dividir cada uno de los roles finos presentes en la anotaci�n de seguridad
    protected static List<String> splitRolString(String roleName, int limit){
        List<String> rolesList=new ArrayList<String>();
        String name="";
        for(int i=0;i<limit;i++){
            int startFlag=0;
            int indicator=0;
            int endFlag=0;
            for(int j=0;j<roleName.length();j++){
                char[] string= roleName.toCharArray();
                if (string[j]=='\'' && indicator==0){
                    startFlag=j+1;
                    indicator++;
                }
                if (string[j]==','|| string[j]==')'){
                    endFlag=j;
                    j=string.length;
                }
            }
        name=roleName.substring(startFlag, endFlag-1);
        rolesList.add(name);
        roleName= roleName.substring(endFlag+1,roleName.length());
        }
       return rolesList;  
    }
    
    //se encarga de identificar el numero de roles finos en una anotaci�n
    public String getRolName(AnnotationInstanceOrModifier anotation){
        String rolname="";
        if(anotation.getClass().equals(AnnotationInstanceImpl.class)){
            AnnotationInstanceImpl anotationInstance=(AnnotationInstanceImpl)anotation;
            EList<LayoutInformation> layourInformationList= anotation.getLayoutInformations();
            for(LayoutInformation layoutinformation :layourInformationList){
                if(layoutinformation.getVisibleTokenText().equals("Restrict")){
                    SingleAnnotationParameter ap= (SingleAnnotationParameter) anotationInstance.getParameter();
                    StringReferenceImpl sri=(StringReferenceImpl)ap.getValue();
                    rolname=sri.getValue();
                }
            }
        }
        return rolname;
    }
    
    //se encarga de construir una lista con los nombre de los m�todos existentes en las anotaciones
    public List<String> getPermissionsNames(String path){
       char[] string= path.toCharArray();
       path=path+".";
       List<Integer> points=new ArrayList<Integer>();
       points.add(0);
       List<String> names=new ArrayList<String>();
       int counter=2;
       for(int i=0;i<string.length;i++){
           if(string[i]=='.'){
               points.add(i+1);
               counter++;
           }
       } 
       counter =counter-2;
       for (int i=0;i<(counter);i++ ){
           String name=path.substring(points.get(i), (points.get(i+1))-1);
           names.add(name);
       }
       return names;
    }
    
    //se encarga de validar que la sintaxis de la anotaci�n sea suficientemente correcta para no causar errores en la ejecuci�n de la restricci�n
    public boolean validateString(String anotation){
        if(rolesNumber(anotation)%2!=0){
            return false;
        }
        for(int j=0;j<anotation.length();j++){
            char[] string= anotation.toCharArray();
            if (string[j]=='(' && string[j+1]!='\''){
                return false;
            }
            if (string[j]==',' && string[j-1]!='\'' && string[j+1]!='\''){
              return false;
            }
            if (string[j]==')' && string[j-1]!='\'' && string[j+1]!='}'){
                return false;
              }
            if (string[j]=='\'' && string[j+1]=='\''){
                return false;
              }
        }
        return true; 
    }
    

}
