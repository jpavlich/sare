package analyzer.constraints;

import java.util.ArrayList;
import org.eclipse.emf.common.util.EList;
import org.emftext.commons.layout.LayoutInformation;
import org.emftext.language.java.annotations.impl.AnnotationInstanceImpl;
import org.emftext.language.java.members.Method;
import org.emftext.language.java.members.impl.ClassMethodImpl;
import org.emftext.language.java.modifiers.AnnotationInstanceOrModifier;
import acm.AccessControlModel;

public class UnprotectedMethod implements Constraint<Method, AccessControlModel>{
    
    //se encarga de ejecutar la verificaci�n de una restricci�n sobre un m�todo
    public boolean execute(Method method, AccessControlModel model) {
        ArrayList<Boolean> warnings=new ArrayList<Boolean>();
        EList<AnnotationInstanceOrModifier> anotations=method.getAnnotationsAndModifiers();
        ModelUtilities modelutilities=new ModelUtilities();
        for(AnnotationInstanceOrModifier anototation: anotations){ 
            if(anototation.getClass().equals(AnnotationInstanceImpl.class)){   
                warnings.add(isThereAnotation(anototation));
            }
        }
        if(!warnings.contains(true)){
          System.out.println("WARNING!: Unprotected method ->"+"  Method: "+ modelutilities.getMethodFirm((ClassMethodImpl) method));
          return false;
        }
       return true;
    }
    
    // se encarga de buscar si existen anotaciones de seguridad sobre un m�todo
    public Boolean isThereAnotation(AnnotationInstanceOrModifier anotation){
        if(anotation.getClass().equals(AnnotationInstanceImpl.class)){
            AnnotationInstanceImpl anotatationImpl=(AnnotationInstanceImpl)anotation;
            EList<LayoutInformation> anotationStrings= anotatationImpl.getLayoutInformations();
            for(LayoutInformation layoutInformation :anotationStrings){
                if(layoutInformation.getVisibleTokenText().equals("Restrict")){
                    return true;
                }
            }
        }
        return false;
    }
    


}
