package analyzer.constraints;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.util.EList;
import org.emftext.commons.layout.LayoutInformation;
import org.emftext.language.java.annotations.SingleAnnotationParameter;
import org.emftext.language.java.annotations.impl.AnnotationInstanceImpl;
import org.emftext.language.java.classifiers.impl.ClassImpl;
import org.emftext.language.java.containers.impl.CompilationUnitImpl;
import org.emftext.language.java.members.Method;
import org.emftext.language.java.members.impl.ClassMethodImpl;
import org.emftext.language.java.modifiers.AnnotationInstanceOrModifier;
import org.emftext.language.java.references.impl.StringReferenceImpl;
import acm.AccessControlModel;
import acm.Permission;
import acm.Role;

public class AbsentPermission implements Constraint <Method, AccessControlModel>{
    
    private List<String> permissionsModel =new ArrayList<String>();
    
    //Este m�todo se encarga de ejecutar la verificaci�n de una restricci�n sobre un metodo
    public boolean execute(Method method, AccessControlModel model) {
        AnnotationInstanceOrModifier anotationOrModifier=getRestrictAnotation(method.getAnnotationsAndModifiers());
        ClassImpl clase=(ClassImpl)method.getParentConcreteClassifier();
        CompilationUnitImpl container=(CompilationUnitImpl)clase.eContainer();
        ModelUtilities modelutilities=new ModelUtilities();
        
        try {
            List<Role> roles=modelutilities.loadRoles(model);
            for(int i=0;i<roles.size();i++){
                List<String> permissions=getPermissionsNames(container.getName());
                permissions.add(method.getName());
                findPermission(roles.get(i),permissions,model,modelutilities);
            }
            if(permissionsModel.size()!=0 && anotationOrModifier==null){
                for (String sentenceModel:permissionsModel){
                System.out.println("ERROR!:   Missing Permision \""+sentenceModel+"\" over the method ->"+"  Method: "+ modelutilities.getMethodFirm((ClassMethodImpl)method));   
                }
                return false;
            }else{
            if(anotationOrModifier!=null){ 
                String anotationString=getRolName(anotationOrModifier);
                if(validateString(anotationString))
                {
                List<String> permissionsMethod=splitRolString(anotationString,rolesNumber(anotationString)/2);
                for (String permissionModel:permissionsModel){
                    int counter=0;
                    for (String permissionMethod:permissionsMethod){
                        if (permissionModel.equals(permissionMethod)){
                            counter++;
                        }   
                    } 
                    if (counter==0){
                        System.out.println("ERROR!:   Missing Permision \""+permissionModel+"\" over the method ->"+"  Method: "+ modelutilities.getMethodFirm((ClassMethodImpl)method));   
                        return false;
                    }
                }
                }
            }      
            }

        } catch (IOException e) {
            e.printStackTrace();
        } 
        return true;
        
    }
    
    //se encarga de buscar si el permiso presente en la anotaci�n existe en el modelo de control de acceso
    public void findPermission(Role role,List<String> permissionsL, AccessControlModel model,ModelUtilities modelutilities) {
        List<String> permissionsAux=permissionsL;
        EList<Permission> ListPermission=role.getPermissions();
        String permissionName=permissionsAux.get(0);
        Permission perm=getPermission(ListPermission,permissionName); 
        if(perm!=null){
           permissionsAux.remove(0);
           findPermissionRecursion(perm, permissionsAux,role);
        }
    }
    
    
    public boolean findPermissionRecursion(Permission permission, List<String> permissionsList,Role role){
        EList<Permission> ListPermission= permission.getChildPermissions();
        String permissionName= permissionsList.get(0);
        Permission perm=getPermission(ListPermission,permissionName);
        if(perm==null){
            return false;
        }
        if (permissionsList.size()==1 && !perm.getElement().getName().equals(permissionName)){
            return false;
        }
        else{
        if (permissionsList.size()==1 && perm.getElement().getName().equals(permissionName)){
            String sentences=role.getName()+"_"+perm.getElement().getName();
            this.permissionsModel.add(sentences);
            return false;
        }else{
            permissionsList.remove(0);
            return findPermissionRecursion(perm, permissionsList,role);
            
        }
    }
    }
    
    public Permission getPermission(EList<Permission> ListPermission,String name){
        for (Permission permission :ListPermission){
            if(permission.getElement().getName().equals(name)){
                return permission;
            }
        }
        return null;
    }
    
    //se encarga de dividir cada uno de los roles finos presentes en la anotaci�n de seguridad
    protected  List<String> splitRolString(String roleName, int limit){
        List<String> rolesList=new ArrayList<String>();
        String name="";
        for(int i=0;i<limit;i++){
            int startFlag=0;
            int indicator=0;
            int endFlag=0;
            for(int j=0;j<roleName.length();j++){
                char[] string= roleName.toCharArray();
                if (string[j]=='\'' && indicator==0){
                    startFlag=j+1;
                    indicator++;
                }
                if (string[j]==','|| string[j]==')'){
                    endFlag=j;
                    j=string.length;
                }
            }
        name=roleName.substring(startFlag, endFlag-1);
        rolesList.add(name);
        roleName= roleName.substring(endFlag+1,roleName.length());
        }
       return rolesList;  
    }
    
    //se encarga de obtener la anotaci�n de la restricci�n de seguridad existente en el metodo
    public AnnotationInstanceImpl getRestrictAnotation(EList<AnnotationInstanceOrModifier> anotations){
        for(AnnotationInstanceOrModifier anotation: anotations){ 
        if(anotation.getClass().equals(AnnotationInstanceImpl.class)){
            AnnotationInstanceImpl anotatationImpl=(AnnotationInstanceImpl)anotation;
            EList<LayoutInformation> anotationStrings= anotatationImpl.getLayoutInformations();
            for(LayoutInformation layoutInformation :anotationStrings){
                if(layoutInformation.getVisibleTokenText().equals("Restrict")){
                    return anotatationImpl;
                }
            }
        }
        }
        return null;
    }
    
    //se encarga de obtener la cadena de roles finos de la anotaci�n
    public String getRolName(AnnotationInstanceOrModifier anotation){
        String rolname="";
        if(anotation.getClass().equals(AnnotationInstanceImpl.class)){
            AnnotationInstanceImpl anotationInstance=(AnnotationInstanceImpl)anotation;
            EList<LayoutInformation> layourInformationList= anotation.getLayoutInformations();
            for(LayoutInformation layoutinformation :layourInformationList){
                if(layoutinformation.getVisibleTokenText().equals("Restrict")){
                    SingleAnnotationParameter ap= (SingleAnnotationParameter) anotationInstance.getParameter();
                    StringReferenceImpl sri=(StringReferenceImpl)ap.getValue();
                    rolname=sri.getValue();
                }
            }
        }
        return rolname;
    }
    
    //identifica el numero de roles en una anotacion 
    protected  int rolesNumber(String roleName){
        char[] string= roleName.toCharArray();
        int token=0;
        for(int i=0;i<string.length;i++){
            if (string[i]=='\''){
                token++;
            } 
        }
        return token;
    } 
    
    //se encarga de construir una lista con los nombre de los m�todos existentes en las anotaciones
    public List<String> getPermissionsNames(String path){
        char[] string= path.toCharArray();
        path=path+".";
        List<Integer> points=new ArrayList<Integer>();
        points.add(0);
        List<String> names=new ArrayList<String>();
        int counter=2;
        for(int i=0;i<string.length;i++){
            if(string[i]=='.'){
                points.add(i+1);
                counter++;
            }
        } 
        counter =counter-2;
        for (int i=0;i<(counter);i++ ){
            String name=path.substring(points.get(i), (points.get(i+1))-1);
            names.add(name);
        }
        return names;
     }
    
    //se encarga de validar que la sintaxis de la anotaci�n sea suficientemente correcta para no causar errores en la ejecuci�n de la restricci�n
    public boolean validateString(String anotation){
        if(rolesNumber(anotation)%2!=0){
            return false;
        }
        for(int j=0;j<anotation.length();j++){
            char[] string= anotation.toCharArray();
            if (string[j]=='(' && string[j+1]!='\''){
                return false;
            }
            if (string[j]==',' && string[j-1]!='\'' && string[j+1]!='\''){
              return false;
            }
            if (string[j]==')' && string[j-1]!='\'' && string[j+1]!='}'){
                return false;
              }
            if (string[j]=='\'' && string[j+1]=='\''){
                return false;
              }
        }
        return true; 
    }
}
