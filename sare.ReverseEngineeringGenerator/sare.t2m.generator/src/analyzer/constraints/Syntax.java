package analyzer.constraints;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.common.util.EList;
import org.emftext.commons.layout.LayoutInformation;
import org.emftext.language.java.annotations.SingleAnnotationParameter;
import org.emftext.language.java.annotations.impl.AnnotationInstanceImpl;
import org.emftext.language.java.members.Method;
import org.emftext.language.java.members.impl.ClassMethodImpl;
import org.emftext.language.java.modifiers.AnnotationInstanceOrModifier;
import org.emftext.language.java.references.impl.StringReferenceImpl;

import acm.AccessControlModel;


public class Syntax implements Constraint <Method, AccessControlModel>{

           // Este m�todo se encarga de ejecutar la verificaci�n de una restricci�n sobre un metodo
        public boolean execute(Method method, AccessControlModel model) {
            AnnotationInstanceOrModifier anotationOrModifier=getRestrictAnotation(method.getAnnotationsAndModifiers());
            if(anotationOrModifier!=null){ 
                String rolString=getRolName(anotationOrModifier);
                int limit=rolesNumber(rolString)/2;
                ModelUtilities modelutilities=new ModelUtilities();
                if (!validateString(rolString) ){
                    System.out.println("ERROR!:   Syntax Error \""+rolString+"\" over the method ->"+"  Method: "+ modelutilities.getMethodFirm((ClassMethodImpl)method));
                    return false;
                }else{ 
                List<String> roleList =splitRolString(rolString,limit);
                for(int i=0; i<roleList.size(); i++) {         
                       if (!validatePermission(model,roleList.get(i),method) ){
                           System.out.println("ERROR!:   Syntax Error \""+rolString+"\" over the method ->"+"  Method: "+ modelutilities.getMethodFirm((ClassMethodImpl)method));
                           return false;
                       } 
                }
                }
            }
            return true;
        }
        
        //se encarga  verificar si el nombre del m�todo y el nombre del rol no son cadenas vacias
        public boolean validatePermission(acm.AccessControlModel accessControlModel,String roleName,Method method){
            char[] string= roleName.toCharArray();
            int flag=-1;;
            for(int i=0;i<roleName.length();i++){
                if(string[i]=='_'){
                    flag=i;
                }
            }
            if(flag==-1){
                return false;
            }else{
                String name=roleName.substring(0, flag);                
                String methodName=roleName.substring(flag+1);
                if(name.length()==0 || methodName.length()==0){
                    return false;
                }
            }
            return true;
        }
        
        //se encarga de validar que la sintaxis de la anotaci�n sea suficientemente correcta para no causar errores en la ejecuci�n de la restricci�n
        public boolean validateString(String anotation){
            if (!anotation.substring(0, 12).equals("#{s:hasRole(")){
                return false;
            }
            if(rolesNumber(anotation)%2!=0){
                return false;
            }
            for(int j=0;j<anotation.length();j++){
                char[] string= anotation.toCharArray();
                if (string[j]=='(' && string[j+1]!='\''){
                    return false;
                }
                if (string[j]==',' && string[j-1]!='\'' && string[j+1]!='\''){
                  return false;
                }
                if (string[j]==')' && string[j-1]!='\'' && string[j+1]!='}'){
                    return false;
                  }
                if (string[j]=='\'' && string[j+1]=='\''){
                    return false;
                  }
            }
            return true; 
        }
        
        //se encarga de dividir cada uno de los roles finos presentes en la anotaci�n de seguridad
        protected  List<String> splitRolString(String roleName, int limit){
            List<String> rolesList=new ArrayList<String>();
            String name="";
            for(int i=0;i<limit;i++){
                int startFlag=0;
                int indicator=0;
                int endFlag=0;
                for(int j=0;j<roleName.length();j++){
                    char[] string= roleName.toCharArray();
                    if (string[j]=='\'' && indicator==0){
                        startFlag=j+1;
                        indicator++;
                    }
                    if (string[j]==','|| string[j]==')'){
                        endFlag=j;
                        j=string.length;
                    }
                }
            name=roleName.substring(startFlag, endFlag-1);
            rolesList.add(name);
            roleName= roleName.substring(endFlag+1,roleName.length());
            }
           return rolesList;  
        }
        
        // se encarga de obtener la anotaci�n de la restricci�n de seguridad existente en el metodo
        public AnnotationInstanceImpl getRestrictAnotation(EList<AnnotationInstanceOrModifier> anotations){
            for(AnnotationInstanceOrModifier anotation: anotations){ 
            if(anotation.getClass().equals(AnnotationInstanceImpl.class)){
                AnnotationInstanceImpl anotatationImpl=(AnnotationInstanceImpl)anotation;
                EList<LayoutInformation> anotationStrings= anotatationImpl.getLayoutInformations();
                for(LayoutInformation layoutInformation :anotationStrings){
                    if(layoutInformation.getVisibleTokenText().equals("Restrict")){
                        return anotatationImpl;
                    }
                }
            }
            }
            return null;
        }
        
        // se encarga de obtener la cadena de roles finos de la anotaci�n. anotation hace referencia a la anotaci�n de seguridad
        public String getRolName(AnnotationInstanceOrModifier anotation){
            String rolname="";
            if(anotation.getClass().equals(AnnotationInstanceImpl.class)){
                AnnotationInstanceImpl anotationInstance=(AnnotationInstanceImpl)anotation;
                EList<LayoutInformation> layourInformationList= anotation.getLayoutInformations();
                for(LayoutInformation layoutinformation :layourInformationList){
                    if(layoutinformation.getVisibleTokenText().equals("Restrict")){
                        SingleAnnotationParameter ap= (SingleAnnotationParameter) anotationInstance.getParameter();
                        StringReferenceImpl sri=(StringReferenceImpl)ap.getValue();
                        rolname=sri.getValue();
                    }
                }
            }
            return rolname;
        }
        
        //Identifica el numero de roles en una anotacion 
        protected  int rolesNumber(String roleName){
            char[] string= roleName.toCharArray();
            int token=0;
            for(int i=0;i<string.length;i++){
                if (string[i]=='\''){
                    token++;
                } 
            }
            return token;
        }    
    
}
