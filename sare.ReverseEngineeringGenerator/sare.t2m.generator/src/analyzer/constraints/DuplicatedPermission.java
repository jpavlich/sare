package analyzer.constraints;

import java.util.ArrayList;
import java.util.List;
import org.eclipse.emf.common.util.EList;
import org.emftext.commons.layout.LayoutInformation;
import org.emftext.language.java.annotations.SingleAnnotationParameter;
import org.emftext.language.java.annotations.impl.AnnotationInstanceImpl;
import org.emftext.language.java.members.Method;
import org.emftext.language.java.members.impl.ClassMethodImpl;
import org.emftext.language.java.modifiers.AnnotationInstanceOrModifier;
import org.emftext.language.java.references.impl.StringReferenceImpl;
import acm.AccessControlModel;
import acm.Subject;
import acm.impl.RoleImpl;

public class DuplicatedPermission implements Constraint <Method, AccessControlModel>{
    
    // se encarga de ejecutar la verificaci�n de una restricci�n sobre un metodo. 
    public boolean execute(Method method, AccessControlModel model) {
        AnnotationInstanceOrModifier anotationOrModifier=getRestrictAnotation(method.getAnnotationsAndModifiers());
        if(anotationOrModifier!=null){ 
            String rolString=getRolName(anotationOrModifier);
            if(validateString(rolString))
            {
            int limit=rolesNumber(rolString)/2;
            List<String> roleList =splitRolString(rolString,limit);
            for(int i=0; i<roleList.size(); i++) {
                String permission1 =  roleList.get(i);
                for(int j=i+1; j<roleList.size(); j++) {
                    String permission2 = roleList.get(j);
                    if (permission1.equals(permission2)) {
                        return validatePermission(model,permission1,method);
                    }
                }
            }
            }
        }
        return true;
    }
    
    // se encarga imprimir la el error si el rol existe en el modelo de control de acceso
    public boolean validatePermission(acm.AccessControlModel accessControlModel,String roleName,Method method){
        char[] string= roleName.toCharArray();
        int flag=0;;
        for(int i=0;i<roleName.length();i++){
            if(string[i]=='_'){
                flag=i;
            }
        }
        String name=roleName.substring(0, flag);
        ModelUtilities modelutilities=new ModelUtilities();
        if(existRole(accessControlModel,name)){
            System.out.println("ERROR!:   Duplicated permission over the method ->"+"  Method: "+ modelutilities.getMethodFirm((ClassMethodImpl)method));
           return false;
        }
      return true;
    }
   
    // se encarga de obtener la anotaci�n de la restricci�n de seguridad existente en el metodo
    public AnnotationInstanceImpl getRestrictAnotation(EList<AnnotationInstanceOrModifier> anotations){
        for(AnnotationInstanceOrModifier anotation: anotations){ 
        if(anotation.getClass().equals(AnnotationInstanceImpl.class)){
            AnnotationInstanceImpl anotatationImpl=(AnnotationInstanceImpl)anotation;
            EList<LayoutInformation> anotationStrings= anotatationImpl.getLayoutInformations();
            for(LayoutInformation layoutInformation :anotationStrings){
                if(layoutInformation.getVisibleTokenText().equals("Restrict")){
                    return anotatationImpl;
                }
            }
        }
        }
        return null;
    }
    
    //Identifica el numero de roles en una anotacion 
    protected static int rolesNumber(String roleName){
        char[] string= roleName.toCharArray();
        int token=0;
        for(int i=0;i<string.length;i++){
            if (string[i]=='\''){
                token++;
            } 
        }
        return token;
    }
    
    // se encarga de dividir cada uno de los roles finos presentes en la anotaci�n de seguridad
    protected static List<String> splitRolString(String roleName, int limit){
        List<String> rolesList=new ArrayList<String>();
        String name="";
        for(int i=0;i<limit;i++){
            int startFlag=0;
            int indicator=0;
            int endFlag=0;
            for(int j=0;j<roleName.length();j++){
                char[] string= roleName.toCharArray();
                if (string[j]=='\'' && indicator==0){
                    startFlag=j+1;
                    indicator++;
                }
                if (string[j]==','|| string[j]==')'){
                    endFlag=j;
                    j=string.length;
                }
            }
        name=roleName.substring(startFlag, endFlag-1);
        rolesList.add(name);
        roleName= roleName.substring(endFlag+1,roleName.length());
        }
       return rolesList;  
    }
    
    //se encarga de obtener la cadena de roles finos de la anotaci�n
    public String getRolName(AnnotationInstanceOrModifier anotation){
        String rolname="";
        if(anotation.getClass().equals(AnnotationInstanceImpl.class)){
            AnnotationInstanceImpl anotationInstance=(AnnotationInstanceImpl)anotation;
            EList<LayoutInformation> layourInformationList= anotation.getLayoutInformations();
            for(LayoutInformation layoutinformation :layourInformationList){
                if(layoutinformation.getVisibleTokenText().equals("Restrict")){
                    SingleAnnotationParameter ap= (SingleAnnotationParameter) anotationInstance.getParameter();
                    StringReferenceImpl sri=(StringReferenceImpl)ap.getValue();
                    rolname=sri.getValue();
                }
            }
        }
        return rolname;
    }
    
    //se encarga de construir una lista con los nombre de los m�todos existentes en las anotaciones
    public List<String> getPermissionsNames(String path){
       char[] string= path.toCharArray();
       path=path+".";
       List<Integer> points=new ArrayList<Integer>();
       points.add(0);
       List<String> names=new ArrayList<String>();
       int counter=2;
       for(int i=0;i<string.length;i++){
           if(string[i]=='.'){
               points.add(i+1);
               counter++;
           }
       } 
       counter =counter-2;
       for (int i=0;i<(counter);i++ ){
           String name=path.substring(points.get(i), (points.get(i+1))-1);
           names.add(name);
       }
       return names;
    }
    
    // se encarga de verificar si un rol existe o no en el modelo de pol�ticas de control de acceso
    public boolean existRole(acm.AccessControlModel accessControlModel,String rolname){
        EList<Subject> subjects=accessControlModel.getElements();
        for (Subject subject:subjects){
            if(subject.getClass().equals(acm.impl.RoleImpl.class)){
                RoleImpl role=(RoleImpl)subject;
                if(role.getName().equals(rolname)){
                    return true;
                }
            }
        }
        return false;
    }
    
    //se encarga de validar que la sintaxis de la anotaci�n sea suficientemente correcta para no causar errores en la ejecuci�n de la restricci�n
    public boolean validateString(String anotation){
        if(rolesNumber(anotation)%2!=0){
            return false;
        }
        for(int j=0;j<anotation.length();j++){
            char[] string= anotation.toCharArray();
            if (string[j]=='(' && string[j+1]!='\''){
                return false;
            }
            if (string[j]==',' && string[j-1]!='\'' && string[j+1]!='\''){
              return false;
            }
            if (string[j]==')' && string[j-1]!='\'' && string[j+1]!='}'){
                return false;
              }
            if (string[j]=='\'' && string[j+1]=='\''){
                return false;
              }
        }
        return true; 
    }

}

