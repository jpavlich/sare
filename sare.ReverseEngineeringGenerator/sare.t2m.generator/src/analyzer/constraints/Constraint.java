package analyzer.constraints;

import org.eclipse.emf.ecore.EObject;

public interface Constraint<T extends EObject, U extends EObject> {
 public abstract boolean execute(T obj, U model);
}
