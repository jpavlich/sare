package analyzer;

import java.io.IOException;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;

public class LoadModel {
    
    private Resource resource;
    
    public Resource doLoadXMIFromAbsolutePath(String xmiFileName) {  
        ResourceSet rset = new ResourceSetImpl();
        rset.getPackageRegistry().put(acm.AcmPackage.eINSTANCE.getNsURI(), acm.AcmPackage.eINSTANCE);
        rset.getPackageRegistry().put(org.eclipse.uml2.uml.UMLPackage.eINSTANCE.getNsURI(), org.eclipse.uml2.uml.UMLPackage.eINSTANCE);
        rset.getResourceFactoryRegistry().getExtensionToFactoryMap().put("xmi", new XMIResourceFactoryImpl());
        resource = rset.createResource(URI.createFileURI(xmiFileName));
        try {
            resource.load(null);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return resource;
    }

    public Resource getResource() {
        return resource;
    }

    public void setResource(Resource resource) {
        this.resource = resource;
    }
}
