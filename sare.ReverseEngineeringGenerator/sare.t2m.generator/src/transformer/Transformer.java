package transformer;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.uml2.uml.Association;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Comment;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Model;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.Operation;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.PrimitiveType;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.Type;
import org.eclipse.uml2.uml.UMLFactory;
import org.eclipse.uml2.uml.UMLPackage;
import org.eclipse.uml2.uml.VisibilityKind;
import org.eclipse.uml2.uml.internal.impl.CommentImpl;
import org.eclipse.uml2.uml.internal.impl.PackageImpl;
import org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl;
import org.emftext.commons.layout.LayoutInformation;
import org.emftext.commons.layout.impl.AttributeLayoutInformationImpl;
import org.emftext.language.java.annotations.SingleAnnotationParameter;
import org.emftext.language.java.annotations.impl.AnnotationAttributeSettingImpl;
import org.emftext.language.java.annotations.impl.AnnotationInstanceImpl;
import org.emftext.language.java.classifiers.impl.ClassImpl;
import org.emftext.language.java.containers.impl.CompilationUnitImpl;
import org.emftext.language.java.generics.QualifiedTypeArgument;
import org.emftext.language.java.generics.TypeArgument;
import org.emftext.language.java.literals.impl.BooleanLiteralImpl;
import org.emftext.language.java.members.impl.ClassMethodImpl;
import org.emftext.language.java.members.impl.ConstructorImpl;
import org.emftext.language.java.members.impl.FieldImpl;
import org.emftext.language.java.modifiers.AnnotationInstanceOrModifier;
import org.emftext.language.java.modifiers.impl.PrivateImpl;
import org.emftext.language.java.modifiers.impl.ProtectedImpl;
import org.emftext.language.java.modifiers.impl.PublicImpl;
import org.emftext.language.java.parameters.Parameter;
import org.emftext.language.java.references.Reference;
import org.emftext.language.java.references.impl.IdentifierReferenceImpl;
import org.emftext.language.java.references.impl.StringReferenceImpl;
import org.emftext.language.java.types.ClassifierReference;
import org.emftext.language.java.types.TypeReference;
import org.emftext.language.java.types.impl.ClassifierReferenceImpl;
import org.emftext.language.java.types.impl.NamespaceClassifierReferenceImpl;

import acm.AccessControlModel;
import acm.AcmFactory;
import acm.Permission;
import acm.Role;
import acm.Subject;
import acm.impl.RoleImpl;

public class Transformer {

    protected static final ResourceSet UMLrs = new ResourceSetImpl();
    
    public Resource transformModel( ResourceSet jamopprs, URI srcUri,File file) throws IOException {
        
        System.out.println("Begining transformation");
        //Para el modelo generado por JaMMop
        URI outUri = URI.createFileURI(file.getCanonicalPath());
        Resource resource = jamopprs.getResource(outUri, true);
        System.out.println("Registering input model");

        //Para el modelo de salida
        String stringUri=modifyUri(outUri.toString());
        URI outUriuml = URI.createFileURI((stringUri));
        Resource outResource = UMLrs.createResource(outUriuml);
         
        //crear modelo de control de acceso y modelo uml, raices de modelo destino
        Model model = createModel("Modelo");
        
        Package modelPackage= model.createNestedPackage("entities");
        acm.AccessControlModel accessControlModel =AcmFactory.eINSTANCE.createAccessControlModel();
        accessControlModel.setName("ACM Model");
        
        List<Association> associations=new ArrayList<Association>();
        List<EObject> eClasses= loadClasses(resource);
        filterAssociations(associations,eClasses,modelPackage);
        createClases(eClasses,modelPackage,associations,accessControlModel);

        accessControlModel.getPackagedElements().add(model);
        outResource.getContents().add(accessControlModel);
        return outResource;
    }
   
    //crea las clases del modelo
    public void createClases(List<EObject> eClases,Package modelPackage,List<Association> associations, acm.AccessControlModel accessControlModel ){
        for(EObject eObj: eClases){
            for(EObject eObj1: eObj.eContents()){
                if(eObj1.getClass().equals(ClassImpl.class)){   
                    ClassImpl claseimp=(ClassImpl)eObj1;
                    if(hasComment( claseimp, "Entity")){
                    if (!existClass(claseimp.getName(),modelPackage)){
                        org.eclipse.uml2.uml.Class umlClass=modelPackage.createOwnedClass(claseimp.getName(), false);
                        umlClass.createOwnedComment().setBody("Entity");
                        createAttributes(claseimp,umlClass,modelPackage,associations);
                        createMethods(claseimp,umlClass,modelPackage,accessControlModel);
                    }else{
                       Class clase= getUmlClass(claseimp.getName(),modelPackage);
                       clase.createOwnedComment().setBody("Entity");
                       createAttributes(claseimp,clase,modelPackage,associations);
                       createMethods(claseimp,clase,modelPackage,accessControlModel);
                    }
                    }
                    if(hasComment( claseimp, "Stateful")){
                        if (!existClass(claseimp.getName(),modelPackage)){
                        org.eclipse.uml2.uml.Class umlClass=modelPackage.createOwnedClass(claseimp.getName(), false);
                        umlClass.createOwnedComment().setBody("EJB");
                        createAttributesEJB(claseimp,umlClass,modelPackage,associations);
                        createMethods(claseimp,umlClass,modelPackage,accessControlModel);
                        }else{
                        Class clase= getUmlClass(claseimp.getName(),modelPackage);
                        clase.createOwnedComment().setBody("EJB");
                        createAttributesEJB(claseimp,clase,modelPackage,associations);
                        createMethods(claseimp,clase,modelPackage,accessControlModel);
                        }
                    }
                        
                }
            }
        }
    }

    public boolean hasComment(ClassImpl claseimp,String comment){
        EList<AnnotationInstanceOrModifier> anotations=claseimp.getAnnotationsAndModifiers();
        for(AnnotationInstanceOrModifier anotationOrModifier: anotations){ 
            if(anotationOrModifier.getClass().equals(AnnotationInstanceImpl.class)){
                AnnotationInstanceImpl anotationInstance=(AnnotationInstanceImpl)anotationOrModifier;
                EList<LayoutInformation> layourInformationList= anotationInstance.getLayoutInformations();
                for(LayoutInformation ly:layourInformationList){
                    if(ly.getVisibleTokenText().equals(comment)){
                        return true;
                    }
                }
            }
        }
        return false;
    }
    
    public void setAnotationsMethod(ClassMethodImpl method,Operation operation){
        EList<AnnotationInstanceOrModifier> anotations=method.getAnnotationsAndModifiers();
        for(AnnotationInstanceOrModifier anotationOrModifier: anotations){ 
            if(anotationOrModifier.getClass().equals(AnnotationInstanceImpl.class)){
                AnnotationInstanceImpl anotationInstance=(AnnotationInstanceImpl)anotationOrModifier;
                EList<LayoutInformation> layourInformationList= anotationInstance.getLayoutInformations();
                for(LayoutInformation ly:layourInformationList){
                    
                    if(!ly.getVisibleTokenText().equals("Restrict")&& !ly.getVisibleTokenText().equals("@")){
                      Comment comment=operation.createOwnedComment();
                      comment.setBody(ly.getVisibleTokenText()); 
                      if(anotationInstance.getParameter()!=null){
                          EList<EObject>  anotationParameters= anotationInstance.getParameter().eContents();
                          for (EObject eObj: anotationParameters){
                              String commentString="";
                              if (eObj.getClass().equals(AnnotationAttributeSettingImpl.class)){
                                  AnnotationAttributeSettingImpl anotationAttributeSetting=(AnnotationAttributeSettingImpl)eObj;
                                  EList<LayoutInformation> parameters=anotationAttributeSetting.getLayoutInformations();
                                  for(LayoutInformation layoutInformation: parameters){
                                      commentString+=layoutInformation.getVisibleTokenText();
                                  }                       
                                  if (anotationAttributeSetting.getValue().getClass().equals(StringReferenceImpl.class)){
                                      StringReferenceImpl stringReference=(StringReferenceImpl)anotationAttributeSetting.getValue();
                                      commentString+=stringReference.getValue();
                                  }
                                  if (anotationAttributeSetting.getValue().getClass().equals(BooleanLiteralImpl.class)){
                                      BooleanLiteralImpl stringReference=(BooleanLiteralImpl)anotationAttributeSetting.getValue();
                                      EList<LayoutInformation> ply=stringReference.getLayoutInformations();
                                      for(LayoutInformation lyp: ply){
                                          commentString+=lyp.getVisibleTokenText();
                                      } 
                                  }
                                  
                                  if (anotationAttributeSetting.getValue().getClass().equals(IdentifierReferenceImpl.class)){
                                      IdentifierReferenceImpl identifierReference=(IdentifierReferenceImpl)anotationAttributeSetting.getValue();
                                      EList<LayoutInformation> references=identifierReference.getLayoutInformations();
                                      for(LayoutInformation layoutInformation: references){
                                          commentString+=layoutInformation.getVisibleTokenText();
                                      }
                                      Reference next=identifierReference.getNext();
                                      EList<LayoutInformation> layoutInformationNext=next.getLayoutInformations();
                                      for(LayoutInformation layoutInformation2: layoutInformationNext){
                                          commentString+=layoutInformation2.getVisibleTokenText();
                                      }
                                  }
                                  comment.createOwnedComment().setBody(commentString);
                              } 
                          }
                      } 
                   }
                }
            }
        }
;
    }
    // dado un string que representa el nombre de una clase, el metodo retorna si dicha clase existe o no en el paquete
    public boolean existClass(String name, Package modelPackage){
        EList<Element> elements= modelPackage.getOwnedElements();
        for(Element element: elements){ 
            if (element.getClass().equals(org.eclipse.uml2.uml.internal.impl.ClassImpl.class)){
                Class umlClass=(Class)element;
                if (umlClass.getName().equals(name)){
                    return true;
                }
            }
        }
        return false;
    }
    
    // dado un string que representa el nombre de un tipo de dato primitivo, el metodo retorna si dicho tipo existe o no en el paquete
    public boolean existType(String type, Package modelPackage){
        EList<Element> elements= modelPackage.getOwnedElements();
        for(Element element: elements){ 
            if (element.getClass().equals(org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl.class)){
                PrimitiveTypeImpl primitiveType=(PrimitiveTypeImpl)element;
                if (primitiveType.getName().equals(type)){
                    return true;
                }
            }
        }
        return false;
    }
    
    //dado un string, el metodo retorna el typo de dato primitivo correspondiente
    public Type getType(String type, Package modelPackage){
        EList<Element> elements= modelPackage.getOwnedElements();
        for(Element element: elements){ 
            if (element.getClass().equals(org.eclipse.uml2.uml.internal.impl.PrimitiveTypeImpl.class)){
                PrimitiveTypeImpl primitiveType=(PrimitiveTypeImpl)element;
                if (primitiveType.getName().equals(type)){
                    return primitiveType;
                }
            }
        }
        return null;
    }
    
    //retorna la clase existente en un paquete dado el nombre 
    public Class getUmlClass(String name, Package modelPackage){
        EList<Element> elements= modelPackage.getOwnedElements();
        for(Element element: elements){ 
            if (element.getClass().equals(org.eclipse.uml2.uml.internal.impl.ClassImpl.class)){
                Class umlClass=(Class)element;
                if (umlClass.getName().equals(name)){
                    return umlClass;
                }
            }
        }
        return null;
    }
    
    public void setRoles(ClassMethodImpl method, acm.AccessControlModel accessControlModel,Operation operation){
       EList<AnnotationInstanceOrModifier> anotations=method.getAnnotationsAndModifiers();
       for(AnnotationInstanceOrModifier anotationOrModifier: anotations){ 
           if(anotationOrModifier.getClass().equals(AnnotationInstanceImpl.class)){
               String rolString=getRolName(anotationOrModifier);
               int limit=rolesNumber(rolString);
               List<String> roleList =splitRolString(rolString,limit);
               for(String rolName:roleList){
                   if (existRole(accessControlModel,rolName)){
                       acm.Role role= getRole(accessControlModel, rolName); 
                       List<Element> levels=getlevels(operation);
                       Element element=getNullValue(levels,role);
                       newLeaf(accessControlModel,rolName,element,levels);
                   }else{
                       acm.Role role= acm.AcmFactory.eINSTANCE.createRole();
                       role.setName(rolName);
                       accessControlModel.getElements().add(role);
                       role.getPermissions().add(createPermission(getlevels(operation)));
                   }
               }
           }
       }   
   }
   
   //Dado el nombre de un rol, busca y retorna el rol correspontiente en el modelo de control de acceso
    public Role getRole(AccessControlModel accessControlModel, String name){
       for (Subject s: accessControlModel.getElements()){
           if(s.getName().equals(name)){
               return (Role)s;
           }
       }
       return null;
   }
   
   // Construye un nuevo rama del arbol de control de acceso. 
    public void newLeaf(AccessControlModel accessControlModel,String rolName, Element element,List<Element> levels){
       List<Element> postLevels=new ArrayList<Element>();
       int position=0;
       for(int i=0; i<levels.size();i++){
           if(levels.get(i).equals(element)){
               i=levels.size();
           }else{
               position++;
           }
       }
       Element father=levels.get(position+1);
       postLevels=levels.subList(0, position+1);
       Permission permission=createPermission(postLevels);
       acm.Role role= getRole(accessControlModel, rolName); 
       Permission fatherPermission=getPermission(father,role);
       int i=role.getPermissions().indexOf(fatherPermission);
        if (i==-1){
           for (Permission p: role.getPermissions()){
           addBranch(p,fatherPermission,permission);
           }
       } 
       else{
          role.getPermissions().get(i).getChildPermissions().add(permission);
       }
   }
   
   //Asoscia la rama creada en el metodo anterior al arbol 
    public void addBranch(Permission permission, Permission fatherPermission,Permission currentPermission){
        int i=permission.getChildPermissions().indexOf(fatherPermission);

      if (i==-1){
           for (Permission p: permission.getChildPermissions()){
               addBranch(p,fatherPermission,currentPermission);
           }
      }
      else{
          permission.getChildPermissions().get(i).getChildPermissions().add(currentPermission);
      }
   }
  
   //retorna el permiso  que no esta en el rol 
    public Element getNullValue(List<Element> levels, Role role){
       for (int i=levels.size()-1; i>=0; i--){ 
           Permission permission= getPermission(levels.get(i),role);
           if (permission==null){
               return levels.get(i);
           }
       }
       return null;
   }
    //el error aparente esta en la recursion, estudiar ests 
    public Permission getPermission(Element element, Role role){
       Permission perm=null;
        for(Permission permission:role.getPermissions()){
           if(permission.getElement().equals(element)){
               perm=permission;
           }else{
               perm= getPermissionRecursion(element,permission);
           }
       }
       return perm;
   }
   
    public Permission getPermissionRecursion(Element element, Permission permission){
        Permission perm=null;
        if (permission.getChildPermissions().size()>0){
           
       for(Permission childPermission:permission.getChildPermissions()){
           NamedElement ne=(NamedElement)element;
           if(childPermission.getElement().getName().equals(ne.getName())){
               return childPermission;
           }else{
               perm= getPermissionRecursion(element,childPermission);
           }
       }
       }
       return perm; 
   }

    public Permission createPermission(List<Element> elements){
       List<Permission> permissions=new ArrayList<Permission>();
       for(Element element: elements){
           acm.Permission permission=AcmFactory.eINSTANCE.createPermission();
           NamedElement namedElement=(NamedElement)element;
           permission.setElement(namedElement);
           permissions.add(permission);
       }
       for(int i=permissions.size()-1;i>=1;i--){
          permissions.get(i).getChildPermissions().add(permissions.get(i-1));   
       }
       return permissions.get(permissions.size()-1);
   }
   
    public List<Element> getlevels(Element element){
       List<Element> parents=new ArrayList<Element>();
       while(!element.getClass().equals(PackageImpl.class)){
           parents.add(element);
           element=element.getOwner();
       }
       parents.add(element);
       return parents;
   }
  
    public boolean existRole(acm.AccessControlModel accessControlModel,String rolname){
       EList<Subject> subjects=accessControlModel.getElements();
       for (Subject subject:subjects){
           if(subject.getClass().equals(acm.impl.RoleImpl.class)){
               RoleImpl role=(RoleImpl)subject;
               if(role.getName().equals(rolname)){
                   return true;
               }
           }
       }
       return false;
   }
    
    //Identifica el numero de roles en una anotacion 
    protected static int rolesNumber(String roleName){
        char[] string= roleName.toCharArray();
        int token=0;
        for(int i=0;i<string.length;i++){
            if (string[i]=='\''){
                token++;
            } 
        }
        return token/2;
    }
    
    
    protected static List<String> splitRolString(String roleName, int limit){
        List<String> rolesList=new ArrayList<String>();
        String name="";
        for(int i=0;i<limit;i++){
            int startFlag=0;
            int indicator=0;
            int endFlag=0;
            int mediumFlag=0;
            for(int j=0;j<roleName.length();j++){
                char[] string= roleName.toCharArray();
                if (string[j]=='\'' && indicator==0){
                    startFlag=j+1;
                    indicator++;
                }
                if (string[j]=='_'){
                    mediumFlag=j;
                }
                if (string[j]==','|| string[j]==')'){
                    endFlag=j;
                    j=string.length;
                }
            }
        name=roleName.substring(startFlag, mediumFlag);
        rolesList.add(name);
        roleName= roleName.substring(endFlag+1,roleName.length());
        }
       return rolesList;  
    }
   
    public String getRolName(AnnotationInstanceOrModifier anotation){
       String rolname="";
       if(anotation.getClass().equals(AnnotationInstanceImpl.class)){
           AnnotationInstanceImpl anotationInstance=(AnnotationInstanceImpl)anotation;
           EList<LayoutInformation> layourInformationList= anotation.getLayoutInformations();
           for(LayoutInformation layoutinformation :layourInformationList){
               if(layoutinformation.getVisibleTokenText().equals("Restrict")){
                   SingleAnnotationParameter ap= (SingleAnnotationParameter) anotationInstance.getParameter();
                   StringReferenceImpl sri=(StringReferenceImpl)ap.getValue();
                   rolname=sri.getValue();
               }
           }
       }
       return rolname;
   }
    
    //crea los atributos de una clase
    public void createAttributes(ClassImpl eClass,org.eclipse.uml2.uml.Class umlClass, Package modelPackage, List<Association> aasociations){
        List<EObject> contents=eClass.eContents();
        for(EObject eObj:contents){
            if (eObj.getClass().equals(FieldImpl.class)){
                FieldImpl attribute=(FieldImpl)eObj;
                Type type=getAttributeType(attribute,umlClass,modelPackage,aasociations,"Entity");
                Property property= umlClass.createOwnedAttribute(attribute.getName(),type);
                property.setUpper(getCardinallity(attribute));
                anotateAttribute( property,attribute);   
            }
        }  
    }
    
    public void createAttributesEJB(ClassImpl eClass,org.eclipse.uml2.uml.Class umlClass, Package modelPackage, List<Association> aasociations){
        List<EObject> contents=eClass.eContents();
        for(EObject eObj:contents){
            if (eObj.getClass().equals(FieldImpl.class)){
                FieldImpl attribute=(FieldImpl)eObj;
                Type type=getAttributeType(attribute,umlClass,modelPackage,aasociations,"Stateful");
                Property property= umlClass.createOwnedAttribute(attribute.getName(),type);
                property.setUpper(getCardinallity(attribute));
                anotateAttribute( property,attribute);   
            }
        }  
    }
    
    // crea los metodos de una clase
    public void createMethods(ClassImpl eClass,org.eclipse.uml2.uml.Class umlClass, Package modelPackage,acm.AccessControlModel accessControlModel){
        List<EObject> contents=eClass.eContents();
        for(EObject eObj:contents){
            if (eObj.getClass().equals(ClassMethodImpl.class)){
                ClassMethodImpl method=(ClassMethodImpl)eObj;
                String methodName=getMethodName(method);
                String methodType= getTypeMehod(method);
                Type type=getMethodType(methodType,modelPackage);
                Operation operation=umlClass.createOwnedOperation(methodName, null, null, type);
                if (isList(method)){
                    operation.setUpper(-1);
                }
                if (type==null){
                    seachParameters(operation,method,modelPackage);
                }
                setRoles(method, accessControlModel,operation);
                setAnotationsMethod(method,operation);
            }
            if (eObj.getClass().equals(ConstructorImpl.class)){
                ConstructorImpl constructor = (ConstructorImpl)eObj;
                String methodName=constructor.getName();
                Operation operation=umlClass.createOwnedOperation(methodName, null, null, null);
            }
        }  
    }
    
    //Busca  y crea los parametros de un metodo.
    public void seachParameters(Operation operation,ClassMethodImpl method, Package modelPackage){
       EList<Parameter> parameters =method.getParameters();
       for (Parameter param: parameters){
           String name =param.getName();
           TypeReference typeReference=param.getTypeReference();
           String typeString=getTypeReference(typeReference);
           Type type=getMethodType(typeString,modelPackage);
           boolean islist=isListParamentro(typeReference);
           org.eclipse.uml2.uml.Parameter parameter=operation.createOwnedParameter(name, type);
           if(islist){
               parameter.setUpper(-1);
           }
       }
    }
    
    //Dado un string, el metodo retorno tipo que representa ese string
    public Type getMethodType(String typeName, Package modelPackage){
        if (typeName.equals("int") || typeName.equals("Integer")){
            if (!existType("Integer", modelPackage)){
                PrimitiveType primitiveType = (PrimitiveType) modelPackage.createOwnedPrimitiveType("Integer");
                return primitiveType;
            }else{
                return getType("Integer", modelPackage);
            }
        }
        if (typeName.equals("String") ){
            if (!existType("String", modelPackage)){
                PrimitiveType primitiveType = (PrimitiveType) modelPackage.createOwnedPrimitiveType(typeName);
                return primitiveType;
            }else{
                return getType("String", modelPackage);
            }
        }
        if (typeName.equals("float") || typeName.equals("Float")){
            if (!existType("Float", modelPackage)){
                PrimitiveType primitiveType = (PrimitiveType) modelPackage.createOwnedPrimitiveType("Float");
                return primitiveType;
            }else{
                return getType("Float", modelPackage);
            }
        }
        if (typeName.equals("long") || typeName.equals("Long")){
            if (!existType("Long", modelPackage)){
                PrimitiveType primitiveType = (PrimitiveType) modelPackage.createOwnedPrimitiveType("Long");
                return primitiveType;
            }else{
                return getType("Long", modelPackage);
            }
        }
        if (typeName.equals("double") || typeName.equals("Double") ){
            if (!existType("Double", modelPackage)){
                PrimitiveType primitiveType = (PrimitiveType) modelPackage.createOwnedPrimitiveType("Double");
                return primitiveType;
            }else{
                return getType("Double", modelPackage);
            }
        }
        if (typeName.equals("boolean") || typeName.equals("Boolean") ){
            if (!existType("Boolean", modelPackage)){
                PrimitiveType primitiveType = (PrimitiveType) modelPackage.createOwnedPrimitiveType("Boolean");
                return primitiveType;
            }else{
                return getType("Boolean", modelPackage);
            }
        }
       if (typeName.equals("void")){
           return null;
       }
       EList<Element> elements =modelPackage.getOwnedElements();
       for(Element element:elements){
           if (element.getClass().equals(org.eclipse.uml2.uml.internal.impl.ClassImpl.class)){
               Class umlCLass=(Class)element;
               if (umlCLass.getName().equals(typeName)){
                   Type type=(Type)umlCLass;
                   return type;
               }
           }
       }
      return null;
    }
    
    //llama isListParamentro para optener si el retorno del metodo es list o no  
    public boolean isList(ClassMethodImpl method){
        TypeReference typeReference=method.getTypeReference();
        return isListParamentro(typeReference);
    }
    
    //retorna si el parametro  o el retorno de un metodo set es list o no
    public boolean  isListParamentro(TypeReference typeReference){
        if (typeReference.getClass().equals(NamespaceClassifierReferenceImpl.class)){
            NamespaceClassifierReferenceImpl namespaceClassifier=(NamespaceClassifierReferenceImpl)typeReference;
            EList<ClassifierReference> classifierReferences=namespaceClassifier.getClassifierReferences();
            for (ClassifierReference classifierReferece:classifierReferences){
               EList<LayoutInformation> classifierReferenceLayoutInformations= classifierReferece.getLayoutInformations();
                 for(LayoutInformation layoutInformation: classifierReferenceLayoutInformations){
                     if(layoutInformation.getVisibleTokenText().equals("List")){
                        return true;
                     }
                 }
            }
        }
        return false;
    }
    
    // llama al metodo getTypeRef para obtener el nombre del tipo de de retorno del metodo
    public String getTypeMehod(ClassMethodImpl method){
        TypeReference typeReference=method.getTypeReference();
        return getTypeReference(typeReference);
    }
    
    // retorna el tipo de un metodo, usado para tipo de retrno y tipo del parametro 
    public String getTypeReference(TypeReference typeReference){
        EList<LayoutInformation> typeReferenceLayoutInformations=typeReference.getLayoutInformations();
        if (typeReferenceLayoutInformations.size()>0){
            for(LayoutInformation layoutInformation:typeReferenceLayoutInformations){
                return layoutInformation.getVisibleTokenText();
            }   
        }
        if (typeReference.getClass().equals(NamespaceClassifierReferenceImpl.class)){
            NamespaceClassifierReferenceImpl namespaceClassifierReference=(NamespaceClassifierReferenceImpl)typeReference;
            EList<ClassifierReference> nmly=namespaceClassifierReference.getClassifierReferences();
            for (ClassifierReference classifierReference:nmly){
                EList<LayoutInformation> classifierReferenceLayoutInformations= classifierReference.getLayoutInformations();
                if (classifierReferenceLayoutInformations.size()==1){
                    for(LayoutInformation layoutInformation:classifierReferenceLayoutInformations){
                        return layoutInformation.getVisibleTokenText();
                    }   
                }else{
                    EList<TypeArgument> typeArguments= classifierReference.getTypeArguments();
                    for(TypeArgument typeArgument:typeArguments){
                        QualifiedTypeArgument qualifiedTypeArgument=(QualifiedTypeArgument)typeArgument;
                        TypeReference typeReference2 =qualifiedTypeArgument.getTypeReference();
                        if (typeReference2.getClass().equals(NamespaceClassifierReferenceImpl.class)){
                            NamespaceClassifierReferenceImpl namespaceClassifierReference2=(NamespaceClassifierReferenceImpl)typeReference2;
                            EList<ClassifierReference> classifierReferences=namespaceClassifierReference2.getClassifierReferences();
                            for (ClassifierReference classifierReference2:classifierReferences){
                                EList<LayoutInformation> layoutInforations= classifierReference2.getLayoutInformations();
                                if (layoutInforations.size()==1){
                                    for(LayoutInformation layoutInformation:layoutInforations){
                                        return layoutInformation.getVisibleTokenText();
                                    }   
                                }
                        }  
                    } 
                }   
             }
           }
        }
        return null;
    }
    
    //Retorna el nombre del metodo pasado por parametro
    public String getMethodName(ClassMethodImpl method){
        EList<LayoutInformation> layoutInformations=  method.getLayoutInformations();
        for(LayoutInformation layoutInformation: layoutInformations){
            if (layoutInformation.getClass().equals(AttributeLayoutInformationImpl.class)){
               return layoutInformation.getVisibleTokenText();
            }
        }
        return null;
    }
    
    // pone todas las anotaciones en los atributos y su visivilidad
    public void anotateAttribute(Property property,FieldImpl attribute){
        EList<AnnotationInstanceOrModifier> anotations=attribute.getAnnotationsAndModifiers();
        for(AnnotationInstanceOrModifier anotation: anotations){ 
            if(anotation.getClass().equals(AnnotationInstanceImpl.class)){
                AnnotationInstanceImpl anotationInstance=(AnnotationInstanceImpl)anotation;
                EList<LayoutInformation> layoutInformations= anotation.getLayoutInformations();
                Comment comment= property.createOwnedComment();
                for(LayoutInformation layoutInformation :layoutInformations){
                    if(!layoutInformation.getVisibleTokenText().equals("@")){
                        comment.setBody(layoutInformation.getVisibleTokenText());  
                    }
                } 
                if(anotationInstance.getParameter()!=null){
                    EList<EObject>  anotationParameters= anotationInstance.getParameter().eContents();
                    for (EObject eObj: anotationParameters){
                        String commentString="";
                        if (eObj.getClass().equals(AnnotationAttributeSettingImpl.class)){
                            AnnotationAttributeSettingImpl anotationAttributeSetting=(AnnotationAttributeSettingImpl)eObj;
                            EList<LayoutInformation> parameters=anotationAttributeSetting.getLayoutInformations();
                            for(LayoutInformation layoutInformation: parameters){
                                commentString+=layoutInformation.getVisibleTokenText();
                            }                       
                            if (anotationAttributeSetting.getValue().getClass().equals(StringReferenceImpl.class)){
                                StringReferenceImpl stringReference=(StringReferenceImpl)anotationAttributeSetting.getValue();
                                commentString+='"'+stringReference.getValue()+'"';
                            }
                            if (anotationAttributeSetting.getValue().getClass().equals(IdentifierReferenceImpl.class)){
                                IdentifierReferenceImpl identifierReference=(IdentifierReferenceImpl)anotationAttributeSetting.getValue();
                                EList<LayoutInformation> references=identifierReference.getLayoutInformations();
                                for(LayoutInformation layoutInformation: references){
                                    commentString+=layoutInformation.getVisibleTokenText();
                                }
                                Reference next=identifierReference.getNext();
                                EList<LayoutInformation> layoutInformationNext=next.getLayoutInformations();
                                for(LayoutInformation layoutInformation2: layoutInformationNext){
                                    commentString+=layoutInformation2.getVisibleTokenText();
                                }
                            }
                            comment.createOwnedComment().setBody(commentString);
                        } 
                    }
                } 
            }
            if(anotation.getClass().equals(PrivateImpl.class)){           
                property.setVisibility(VisibilityKind.PRIVATE_LITERAL);
            } 
            if(anotation.getClass().equals(PublicImpl.class)){
                property.setVisibility(VisibilityKind.PUBLIC_LITERAL);
               
            } 
            if(anotation.getClass().equals(ProtectedImpl.class)){
                property.setVisibility(VisibilityKind.PROTECTED_LITERAL); 
            }   
        } 
    }
    
    
    
    //Crea todas las asociaciones presentes en el modelo
    public void filterAssociations(List<Association> associations,List<EObject> eClasses, Package modelPackage){ 
        String attributeOneType="";
        for(EObject eObj: eClasses){
            for(EObject eObj1: eObj.eContents()){
                if(eObj1.getClass().equals(ClassImpl.class)){   
                    ClassImpl classImp=(ClassImpl)eObj1;
                    if (hasComment(classImp,"Entity")){
                    List<EObject> classContents=eObj1.eContents();
                    for(EObject eObj2:classContents){
                        if (eObj2.getClass().equals(FieldImpl.class)){
                            FieldImpl attributeOne=(FieldImpl)eObj2;
                            TypeReference typeReferenceOne= attributeOne.getTypeReference();
                            attributeOneType=getTypeReference(typeReferenceOne);
                            EList<AnnotationInstanceOrModifier> anotations=attributeOne.getAnnotationsAndModifiers();
                            for(AnnotationInstanceOrModifier anotation: anotations){
                                if(anotation.getClass().equals(AnnotationInstanceImpl.class)){
                                    AnnotationInstanceImpl anotationInstance=(AnnotationInstanceImpl)anotation;
                                    String fullAnotation=getFullAnotation(anotationInstance);
                                    if(fullAnotation.contains(".")){
                                        int point=separateAnotation(fullAnotation);
                                        String nameA=fullAnotation.substring(0,point);
                                        String nameP=fullAnotation.substring(point+1,fullAnotation.length());
                                        String name = nameP.substring(0,3).toLowerCase()+"-"+attributeOne.getName().substring(0,3).toLowerCase();
                                        Association association=(Association)modelPackage.createPackagedElement("asociation",UMLPackage.eINSTANCE.getAssociation()); 
                                        association.setName(name);
                                        Property propertyA;
                                        Property propertyB;
                                        if(existClass(classImp.getName(),modelPackage)){
                                            Class classA = getUmlClass(classImp.getName(),modelPackage);
                                            propertyA=association.createOwnedEnd(nameP,classA, UMLPackage.eINSTANCE.getProperty());
                                        }else{
                                            Class classA = modelPackage.createOwnedClass(classImp.getName(), false);
                                            propertyA=association.createOwnedEnd(nameP,classA, UMLPackage.eINSTANCE.getProperty());
                                        }
                                        if(existClass(attributeOneType,modelPackage)){
                                            Class classB = getUmlClass(attributeOneType,modelPackage);
                                            propertyB=association.createOwnedEnd(attributeOne.getName(),classB, UMLPackage.eINSTANCE.getProperty());   
                                            
                                        }else{
                                            Class classB = modelPackage.createOwnedClass(attributeOneType, false);
                                            propertyB=association.createOwnedEnd(attributeOne.getName(),classB, UMLPackage.eINSTANCE.getProperty());   
                                        }
                                       
                                          int upper=0;
                                        if (nameA.equals("OneToMany")){
                                            upper=-1;
                                            propertyB.setUpper(upper);
                                        }        
                                        if(nameA.equals("ManyToMany") ){
                                            upper=-1;
                                            propertyA.setUpper(upper);
                                            propertyB.setUpper(upper);
                                        }
                                        if (nameA.equals("OneToOne ")){
                                            upper=1;
                                            propertyA.setUpper(upper);
                                            propertyA.setUpper(upper);
                                        }
                                        associations.add(association);
                                    }
                                }
                            }  
                        }
                    }
                }
              }
            }
        } 
    }
    
    // retorna si el atributo es list o no
    public int getCardinallity(FieldImpl attribute){
        TypeReference typeReference=attribute.getTypeReference();
        String type="";
        EList<LayoutInformation> layoutInformation=typeReference.getLayoutInformations();
        if(layoutInformation.size()==1){
            return 1;
        }else{
           EList<EObject> typeReferenceContents=typeReference.eContents();
           if (typeReferenceContents.size()==1){
               if(typeReferenceContents.get(0).getClass().equals(ClassifierReferenceImpl.class)){
                   ClassifierReferenceImpl classifierReference=(ClassifierReferenceImpl) typeReferenceContents.get(0);
                   EList<LayoutInformation> crly=classifierReference.getLayoutInformations();
                   if(crly.size()==1){
                       return 1;  
                   }else{
                       return -1;
                   }
               }
           }
        }
     return 0;   
    }
    
    //retorna el tipo de un atributo, si el tipo no esta creado, se  crea el tipo de dato
    public Type getAttributeType(FieldImpl attribute,org.eclipse.uml2.uml.Class umlClass, Package modelPackage, List<Association> associations,String type){
       // String tipo=getTipoAtributo(at1);
        TypeReference typeReference= attribute.getTypeReference();
        String typeName=getTypeReference(typeReference);
        if (typeName.equals("int") || typeName.equals("Integer")){
            if (!existType("Integer", modelPackage)){
                PrimitiveType primitiveType = (PrimitiveType) modelPackage.createOwnedPrimitiveType("Integer");
                return primitiveType;
            }else{
                return getType("Integer", modelPackage);
            }
        }
        if (typeName.equals("String") ){
            if (!existType("String", modelPackage)){
                PrimitiveType primitiveType = (PrimitiveType) modelPackage.createOwnedPrimitiveType(typeName);
                return primitiveType;
            }else{
                return getType("String", modelPackage);
            }
        }
        if (typeName.equals("float") || typeName.equals("Float")){
            if (!existType("Float", modelPackage)){
                PrimitiveType primitiveType = (PrimitiveType) modelPackage.createOwnedPrimitiveType("Float");
                return primitiveType;
            }else{
                return getType("Float", modelPackage);
            }
        }
        if (typeName.equals("long") || typeName.equals("Long")){
            if (!existType("Long", modelPackage)){
                PrimitiveType primitiveType = (PrimitiveType) modelPackage.createOwnedPrimitiveType("Long");
                return primitiveType;
            }else{
                return getType("Long", modelPackage);
            }
        }
        if (typeName.equals("double") || typeName.equals("Double") ){
            if (!existType("Double", modelPackage)){
                PrimitiveType primitiveType = (PrimitiveType) modelPackage.createOwnedPrimitiveType("Double");
                return primitiveType;
            }else{
                return getType("Double", modelPackage);
            }
        }
        if (typeName.equals("boolean") || typeName.equals("Boolean") ){
            if (!existType("Boolean", modelPackage)){
                PrimitiveType primitiveType = (PrimitiveType) modelPackage.createOwnedPrimitiveType("Boolean");
                return primitiveType;
            }else{
                return getType("Boolean", modelPackage);
            }
        }
        if(type.equals("Stateful")){
            if(!existClass(typeName, modelPackage)){
                org.eclipse.uml2.uml.Class Class=modelPackage.createOwnedClass(typeName, false);
                return Class;
            }else{
                return getUmlClass(typeName,modelPackage);
            }
        }
        return findAssociation(typeName,umlClass.getName(),attribute.getName(),associations);
    }
    
   // Retorna la asociaciones segun su nombre. 
    public Association findAssociation(String typeNameA, String typeNameB, String attributeName, List<Association> associations){
        for(Association asociation:associations){
           EList<Property> properties= asociation.getOwnedEnds();
           int counter=0;
           for(Property property: properties){
               if (property.getName().equals(attributeName)){
                   counter++;
               }
               if (property.getType().getName().equals(typeNameA)){
                   counter++;
               }
               if (property.getType().getName().equals(typeNameB)){
                   counter++;
               }         
           }
           if(counter==3){
               return asociation;
           } 
        }
        return null; 
    }

   //retorna la visibilidad de un atributo 
    public String getVisibilityAttribute(FieldImpl attribute){
       EList<AnnotationInstanceOrModifier> anotations=attribute.getAnnotationsAndModifiers();
       String type="";
       for(AnnotationInstanceOrModifier anot: anotations){
           if(anot.getClass().equals(PrivateImpl.class)) {
               type="private";
           }
           if(anot.getClass().equals(PublicImpl.class)) {
               type="public";
           }
           if(anot.getClass().equals(ProtectedImpl.class)) {
               type="protected";
           }
       }
           return type;  
   }
   
    //Se cargan las clases que deben ser transformadas
    public List<EObject> loadClasses(Resource resourse){
        List<EObject> classList=new ArrayList<EObject>();
        for(EObject eo: resourse.getContents()){
            if(eo.getClass().equals(CompilationUnitImpl.class) ){
                CompilationUnitImpl compilationUnit=(CompilationUnitImpl)eo;
                if (isClass(compilationUnit.getName())){
                    if (!classList.contains(eo)){
                        classList.add(eo);
                    }
                }
            } 
        }
        return classList;
    }
    
    //encuentra en tipo de la anotacion y concatena el parametro
    public String getFullAnotation(AnnotationInstanceImpl anotation){
        String result="";
        EList<LayoutInformation> anotationLayoutInformations= anotation.getLayoutInformations();
        for(LayoutInformation layoutInformation :anotationLayoutInformations){
            if(!layoutInformation.getVisibleTokenText().equals("@")){
                result=layoutInformation.getVisibleTokenText();   
            }
            if(anotation.getParameter()!=null){
                EList<EObject>  parameterContents= anotation.getParameter().eContents();
                for (EObject eObj: parameterContents){
                    if (eObj.getClass().equals(AnnotationAttributeSettingImpl.class)){
                        AnnotationAttributeSettingImpl setting=(AnnotationAttributeSettingImpl)eObj;
                        //extender para sacar los otros atributos de la anotacion
                        if (setting.getValue().getClass().equals(StringReferenceImpl.class)){
                            StringReferenceImpl mb=(StringReferenceImpl)setting.getValue();
                            result=result+"."+mb.getValue();
                        }
                    }
                }
            }
        }
        return result;
    }
    
    // se determina si un clase del resourse es una de las que se deben transformar
    public boolean isClass(String className){
        if(className.contains("entities")){
            return true;
        }
        return false;
    }
    
    
    public int separateAnotation(String anotation){
        char anotationChar[]=anotation.toCharArray();
        int i;
        for (i=0;i< anotationChar.length;i++ ){
           if (anotationChar[i]=='.'){
               return i;
           }   
        }    
        return 0;
    }
    
    public org.eclipse.uml2.uml.Model createModel(String name) {
        org.eclipse.uml2.uml.Model model = UMLFactory.eINSTANCE.createModel();
        model.setName(name);
        return model;
    }

    public String modifyUri(String uri){
        String newUri=(String) uri.subSequence(5,uri.length()-4);
        newUri=newUri+"Uml.xmi";
        return newUri;
    }
}
