
package com.prosofi.usme.model;



/**
   * Este codigo fue generado por sare.m2t.generator 
   * http://code.google.com/p/sare/
   * 
   */

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.util.*;
import javax.persistence.*;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@Entity
@Name("datosEconomico")
@Scope(ScopeType.CONVERSATION)
@Table(name="datosEconomico")
public class DatosEconomico implements Serializable {

     private Double aportesMensuales;

     private String funteDeIngreso;

     private String entidadDondeAhorra;

     private Double ingresosMensuales;

     @Id
     @GeneratedValue
     private Integer id;

     @OneToOne
     private Persona persona;

     private String descripcionDeLaFormaDeAhorro;


    /**
    * Return entidadDondeAhorra
    */
     
    public  String getEntidadDondeAhorra() {
        return entidadDondeAhorra;
    }

    /**
    * Set a value to parameter entidadDondeAhorra
    */
     
    public void setEntidadDondeAhorra(String  entidadDondeAhorra) {
        this.entidadDondeAhorra= entidadDondeAhorra;
    }

    /**
    * Return persona
    */
     
    public  Persona getPersona() {
        return persona;
    }

    /**
    * Set a value to parameter persona
    */
     
    public void setPersona(Persona  persona) {
        this.persona= persona;
    }

    /**
    * Return id
    */
     
    public  Integer getId() {
        return id;
    }

    /**
    * Set a value to parameter id
    */
     
    public void setId(Integer  id) {
        this.id= id;
    }

    /**
    * Return funteDeIngreso
    */
     
    public  String getFunteDeIngreso() {
        return funteDeIngreso;
    }

    /**
    * Set a value to parameter funteDeIngreso
    */
     
    public void setFunteDeIngreso(String  funteDeIngreso) {
        this.funteDeIngreso= funteDeIngreso;
    }

    /**
    * Return ingresosMensuales
    */
     
    public  Double getIngresosMensuales() {
        return ingresosMensuales;
    }

    /**
    * Set a value to parameter ingresosMensuales
    */
     
    public void setIngresosMensuales(Double  ingresosMensuales) {
        this.ingresosMensuales= ingresosMensuales;
    }

    /**
    * Return descripcionDeLaFormaDeAhorro
    */
     
    public  String getDescripcionDeLaFormaDeAhorro() {
        return descripcionDeLaFormaDeAhorro;
    }

    /**
    * Set a value to parameter descripcionDeLaFormaDeAhorro
    */
     
    public void setDescripcionDeLaFormaDeAhorro(String  descripcionDeLaFormaDeAhorro) {
        this.descripcionDeLaFormaDeAhorro= descripcionDeLaFormaDeAhorro;
    }

    /**
    * Return aportesMensuales
    */
     
    public  Double getAportesMensuales() {
        return aportesMensuales;
    }

    /**
    * Set a value to parameter aportesMensuales
    */
     
    public void setAportesMensuales(Double  aportesMensuales) {
        this.aportesMensuales= aportesMensuales;
    }
}
