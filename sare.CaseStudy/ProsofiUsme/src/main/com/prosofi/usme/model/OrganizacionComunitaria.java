
package com.prosofi.usme.model;



/**
   * Este codigo fue generado por sare.m2t.generator 
   * http://code.google.com/p/sare/
   * 
   */

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.util.*;
import javax.persistence.*;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@Entity
@Name("organizacionComunitaria")
@Scope(ScopeType.CONVERSATION)
@Table(name="organizacionComunitaria")
public class OrganizacionComunitaria implements Serializable {

     private String nombreOrganizacion;

     @ManyToOne
     private DatosPsicoSocial datoPsicoSocial;

     @Id
     @GeneratedValue
     private Integer id;


    /**
    * Return id
    */
     
    public  Integer getId() {
        return id;
    }

    /**
    * Set a value to parameter id
    */
     
    public void setId(Integer  id) {
        this.id= id;
    }

    /**
    * Return nombreOrganizacion
    */
     
    public  String getNombreOrganizacion() {
        return nombreOrganizacion;
    }

    /**
    * Set a value to parameter nombreOrganizacion
    */
     
    public void setNombreOrganizacion(String  nombreOrganizacion) {
        this.nombreOrganizacion= nombreOrganizacion;
    }

    /**
    * Return datoPsicoSocial
    */
     
    public  DatosPsicoSocial getDatoPsicoSocial() {
        return datoPsicoSocial;
    }

    /**
    * Set a value to parameter datoPsicoSocial
    */
     
    public void setDatoPsicoSocial(DatosPsicoSocial  datoPsicoSocial) {
        this.datoPsicoSocial= datoPsicoSocial;
    }
}
