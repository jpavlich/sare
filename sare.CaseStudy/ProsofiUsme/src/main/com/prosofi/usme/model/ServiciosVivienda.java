
package com.prosofi.usme.model;



/**
   * Este codigo fue generado por sare.m2t.generator 
   * http://code.google.com/p/sare/
   * 
   */

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.util.*;
import javax.persistence.*;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@Entity
@Name("serviciosVivienda")
@Scope(ScopeType.CONVERSATION)
@Table(name="serviciosVivienda")
public class ServiciosVivienda implements Serializable {

     private String calidad;

     private String disponibiliadadServicioPorSemana;

     private String tipoServicio;

     @Id
     @GeneratedValue
     private Integer id;

     @ManyToOne
     private Vivienda vivienda;


    /**
    * Return calidad
    */
     
    public  String getCalidad() {
        return calidad;
    }

    /**
    * Set a value to parameter calidad
    */
     
    public void setCalidad(String  calidad) {
        this.calidad= calidad;
    }

    /**
    * Return tipoServicio
    */
     
    public  String getTipoServicio() {
        return tipoServicio;
    }

    /**
    * Set a value to parameter tipoServicio
    */
     
    public void setTipoServicio(String  tipoServicio) {
        this.tipoServicio= tipoServicio;
    }

    /**
    * Return id
    */
     
    public  Integer getId() {
        return id;
    }

    /**
    * Set a value to parameter id
    */
     
    public void setId(Integer  id) {
        this.id= id;
    }

    /**
    * Return vivienda
    */
     
    public  Vivienda getVivienda() {
        return vivienda;
    }

    /**
    * Set a value to parameter vivienda
    */
     
    public void setVivienda(Vivienda  vivienda) {
        this.vivienda= vivienda;
    }

    /**
    * Return disponibiliadadServicioPorSemana
    */
     
    public  String getDisponibiliadadServicioPorSemana() {
        return disponibiliadadServicioPorSemana;
    }

    /**
    * Set a value to parameter disponibiliadadServicioPorSemana
    */
     
    public void setDisponibiliadadServicioPorSemana(String  disponibiliadadServicioPorSemana) {
        this.disponibiliadadServicioPorSemana= disponibiliadadServicioPorSemana;
    }
}
