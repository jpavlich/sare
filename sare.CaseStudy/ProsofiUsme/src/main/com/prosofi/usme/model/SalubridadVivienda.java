
package com.prosofi.usme.model;



/**
   * Este codigo fue generado por sare.m2t.generator 
   * http://code.google.com/p/sare/
   * 
   */

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.util.*;
import javax.persistence.*;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@Entity
@Name("salubridadVivienda")
@Scope(ScopeType.CONVERSATION)
@Table(name="salubridadVivienda")
public class SalubridadVivienda implements Serializable {

     private Boolean espacioReducido;

     private Boolean comodidadesModeradas;

     private String viasDeAcceso;

     @OneToOne
     private Vivienda vivienda;

     private Boolean condicionesSanitariasOptimas;

     private Boolean deficienciasSanitarias;

     private Boolean condicionesRegulares;

     private Boolean condicionesSanitariasMarcadamenteInadecuadas;

     @Id
     @GeneratedValue
     private Integer id;


    /**
    * Return condicionesSanitariasMarcadamenteInadecuadas
    */
     
    public  Boolean getCondicionesSanitariasMarcadamenteInadecuadas() {
        return condicionesSanitariasMarcadamenteInadecuadas;
    }

    /**
    * Set a value to parameter condicionesSanitariasMarcadamenteInadecuadas
    */
     
    public void setCondicionesSanitariasMarcadamenteInadecuadas(Boolean  condicionesSanitariasMarcadamenteInadecuadas) {
        this.condicionesSanitariasMarcadamenteInadecuadas= condicionesSanitariasMarcadamenteInadecuadas;
    }

    /**
    * Return vivienda
    */
     
    public  Vivienda getVivienda() {
        return vivienda;
    }

    /**
    * Set a value to parameter vivienda
    */
     
    public void setVivienda(Vivienda  vivienda) {
        this.vivienda= vivienda;
    }

    /**
    * Return condicionesSanitariasOptimas
    */
     
    public  Boolean getCondicionesSanitariasOptimas() {
        return condicionesSanitariasOptimas;
    }

    /**
    * Set a value to parameter condicionesSanitariasOptimas
    */
     
    public void setCondicionesSanitariasOptimas(Boolean  condicionesSanitariasOptimas) {
        this.condicionesSanitariasOptimas= condicionesSanitariasOptimas;
    }

    /**
    * Return id
    */
     
    public  Integer getId() {
        return id;
    }

    /**
    * Set a value to parameter id
    */
     
    public void setId(Integer  id) {
        this.id= id;
    }

    /**
    * Return comodidadesModeradas
    */
     
    public  Boolean getComodidadesModeradas() {
        return comodidadesModeradas;
    }

    /**
    * Set a value to parameter comodidadesModeradas
    */
     
    public void setComodidadesModeradas(Boolean  comodidadesModeradas) {
        this.comodidadesModeradas= comodidadesModeradas;
    }

    /**
    * Return viasDeAcceso
    */
     
    public  String getViasDeAcceso() {
        return viasDeAcceso;
    }

    /**
    * Set a value to parameter viasDeAcceso
    */
     
    public void setViasDeAcceso(String  viasDeAcceso) {
        this.viasDeAcceso= viasDeAcceso;
    }

    /**
    * Return espacioReducido
    */
     
    public  Boolean getEspacioReducido() {
        return espacioReducido;
    }

    /**
    * Set a value to parameter espacioReducido
    */
     
    public void setEspacioReducido(Boolean  espacioReducido) {
        this.espacioReducido= espacioReducido;
    }

    /**
    * Return deficienciasSanitarias
    */
     
    public  Boolean getDeficienciasSanitarias() {
        return deficienciasSanitarias;
    }

    /**
    * Set a value to parameter deficienciasSanitarias
    */
     
    public void setDeficienciasSanitarias(Boolean  deficienciasSanitarias) {
        this.deficienciasSanitarias= deficienciasSanitarias;
    }

    /**
    * Return condicionesRegulares
    */
     
    public  Boolean getCondicionesRegulares() {
        return condicionesRegulares;
    }

    /**
    * Set a value to parameter condicionesRegulares
    */
     
    public void setCondicionesRegulares(Boolean  condicionesRegulares) {
        this.condicionesRegulares= condicionesRegulares;
    }
}
