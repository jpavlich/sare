
package com.prosofi.usme.model;



/**
   * Este codigo fue generado por sare.m2t.generator 
   * http://code.google.com/p/sare/
   * 
   */

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.util.*;
import javax.persistence.*;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@Entity
@Name("tiempoDisponible")
@Scope(ScopeType.CONVERSATION)
@Table(name="tiempoDisponible")
public class TiempoDisponible implements Serializable {

     private String dia;

     @ManyToOne
     private Persona persona;

     private String observaciones;

     @Id
     @GeneratedValue
     private Integer id;

     private String jornada;


    /**
    * Return dia
    */
     
    public  String getDia() {
        return dia;
    }

    /**
    * Set a value to parameter dia
    */
     
    public void setDia(String  dia) {
        this.dia= dia;
    }

    /**
    * Return jornada
    */
     
    public  String getJornada() {
        return jornada;
    }

    /**
    * Set a value to parameter jornada
    */
     
    public void setJornada(String  jornada) {
        this.jornada= jornada;
    }

    /**
    * Return persona
    */
     
    public  Persona getPersona() {
        return persona;
    }

    /**
    * Set a value to parameter persona
    */
     
    public void setPersona(Persona  persona) {
        this.persona= persona;
    }

    /**
    * Return observaciones
    */
     
    public  String getObservaciones() {
        return observaciones;
    }

    /**
    * Set a value to parameter observaciones
    */
     
    public void setObservaciones(String  observaciones) {
        this.observaciones= observaciones;
    }

    /**
    * Return id
    */
     
    public  Integer getId() {
        return id;
    }

    /**
    * Set a value to parameter id
    */
     
    public void setId(Integer  id) {
        this.id= id;
    }
}
