
package com.prosofi.usme.model;



/**
   * Este codigo fue generado por sare.m2t.generator 
   * http://code.google.com/p/sare/
   * 
   */

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.util.*;
import javax.persistence.*;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@Entity
@Name("necesidad")
@Scope(ScopeType.CONVERSATION)
@Table(name="necesidad")
public class Necesidad implements Serializable {

     private String tipoNecesidad;

     @Id
     @GeneratedValue
     private Integer id;

     @ManyToOne
     private DatosPsicoSocial datoPsicoSocial;


    /**
    * Return tipoNecesidad
    */
     
    public  String getTipoNecesidad() {
        return tipoNecesidad;
    }

    /**
    * Set a value to parameter tipoNecesidad
    */
     
    public void setTipoNecesidad(String  tipoNecesidad) {
        this.tipoNecesidad= tipoNecesidad;
    }

    /**
    * Return id
    */
     
    public  Integer getId() {
        return id;
    }

    /**
    * Set a value to parameter id
    */
     
    public void setId(Integer  id) {
        this.id= id;
    }

    /**
    * Return datoPsicoSocial
    */
     
    public  DatosPsicoSocial getDatoPsicoSocial() {
        return datoPsicoSocial;
    }

    /**
    * Set a value to parameter datoPsicoSocial
    */
     
    public void setDatoPsicoSocial(DatosPsicoSocial  datoPsicoSocial) {
        this.datoPsicoSocial= datoPsicoSocial;
    }
}
