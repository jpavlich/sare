
package com.prosofi.usme.model;



/**
   * Este codigo fue generado por sare.m2t.generator 
   * http://code.google.com/p/sare/
   * 
   */

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.util.*;
import javax.persistence.*;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@Entity
@Name("controlPrenatal")
@Scope(ScopeType.CONVERSATION)
@Table(name="controlPrenatal")
public class ControlPrenatal implements Serializable {

     private String observaciones;

     private Boolean asistenciaControlPrenatal;

     @OneToOne
     private Persona persona;

     @Id
     @GeneratedValue
     private Integer id;


    /**
    * Return persona
    */
     
    public  Persona getPersona() {
        return persona;
    }

    /**
    * Set a value to parameter persona
    */
     
    public void setPersona(Persona  persona) {
        this.persona= persona;
    }

    /**
    * Return id
    */
     
    public  Integer getId() {
        return id;
    }

    /**
    * Set a value to parameter id
    */
     
    public void setId(Integer  id) {
        this.id= id;
    }

    /**
    * Return asistenciaControlPrenatal
    */
     
    public  Boolean getAsistenciaControlPrenatal() {
        return asistenciaControlPrenatal;
    }

    /**
    * Set a value to parameter asistenciaControlPrenatal
    */
     
    public void setAsistenciaControlPrenatal(Boolean  asistenciaControlPrenatal) {
        this.asistenciaControlPrenatal= asistenciaControlPrenatal;
    }

    /**
    * Return observaciones
    */
     
    public  String getObservaciones() {
        return observaciones;
    }

    /**
    * Set a value to parameter observaciones
    */
     
    public void setObservaciones(String  observaciones) {
        this.observaciones= observaciones;
    }
}
