
package com.prosofi.usme.model;



/**
   * Este codigo fue generado por sare.m2t.generator 
   * http://code.google.com/p/sare/
   * 
   */

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.util.*;
import javax.persistence.*;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@Entity
@Name("catastrofe")
@Scope(ScopeType.CONVERSATION)
@Table(name="catastrofe")
public class Catastrofe implements Serializable {

     private String fechaDelEvento;

     @Id
     @GeneratedValue
     private Integer id;

     private String lugar;

     private String descripcion;


    /**
    * Return lugar
    */
     
    public  String getLugar() {
        return lugar;
    }

    /**
    * Set a value to parameter lugar
    */
     
    public void setLugar(String  lugar) {
        this.lugar= lugar;
    }

    /**
    * Return descripcion
    */
     
    public  String getDescripcion() {
        return descripcion;
    }

    /**
    * Set a value to parameter descripcion
    */
     
    public void setDescripcion(String  descripcion) {
        this.descripcion= descripcion;
    }

    /**
    * Return fechaDelEvento
    */
     
    public  String getFechaDelEvento() {
        return fechaDelEvento;
    }

    /**
    * Set a value to parameter fechaDelEvento
    */
     
    public void setFechaDelEvento(String  fechaDelEvento) {
        this.fechaDelEvento= fechaDelEvento;
    }

    /**
    * Return id
    */
     
    public  Integer getId() {
        return id;
    }

    /**
    * Set a value to parameter id
    */
     
    public void setId(Integer  id) {
        this.id= id;
    }
}
