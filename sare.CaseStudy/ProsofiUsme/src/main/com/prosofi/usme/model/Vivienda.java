
package com.prosofi.usme.model;



/**
   * Este codigo fue generado por sare.m2t.generator 
   * http://code.google.com/p/sare/
   * 
   */

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.util.*;
import javax.persistence.*;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@Entity
@Name("vivienda")
@Scope(ScopeType.CONVERSATION)
@Table(name="vivienda")
public class Vivienda implements Serializable {

     @OneToOne(mappedBy="vivienda")
     private SalubridadVivienda salubridad;

     @OneToMany(cascade=CascadeType.ALL,mappedBy="vivienda")
     private List< Familia > familias=new ArrayList<Familia>();

     @Id
     @GeneratedValue
     private Integer id;

     private String direccionDomicilio;

     private Integer estrato;

     private Integer numeroFamiliasEnVivienda;

     @OneToMany(cascade=CascadeType.ALL,mappedBy="vivienda")
     private List< ServiciosVivienda > servicios=new ArrayList<ServiciosVivienda>();

     private String telefono;

     private String tipoVivienda;

     private String barrio;

     @OneToMany(cascade=CascadeType.ALL,mappedBy="vivienda")
     private List< AlrededoresVivienda > alrededores=new ArrayList<AlrededoresVivienda>();


   /**
   * Return alrededores
   */
    
   public List< AlrededoresVivienda > getAlrededores() {
       return alrededores;
   }

   /**
   * Set a value to parameter alrededores
   */
    
   public void setAlrededores(List < AlrededoresVivienda > alrededores) {
       this.alrededores= alrededores;
   } 

    /**
    * Return id
    */
     
    public  Integer getId() {
        return id;
    }

    /**
    * Set a value to parameter id
    */
     
    public void setId(Integer  id) {
        this.id= id;
    }

    /**
    * Return tipoVivienda
    */
     
    public  String getTipoVivienda() {
        return tipoVivienda;
    }

    /**
    * Set a value to parameter tipoVivienda
    */
     
    public void setTipoVivienda(String  tipoVivienda) {
        this.tipoVivienda= tipoVivienda;
    }

    /**
    * Return numeroFamiliasEnVivienda
    */
     
    public  Integer getNumeroFamiliasEnVivienda() {
        return numeroFamiliasEnVivienda;
    }

    /**
    * Set a value to parameter numeroFamiliasEnVivienda
    */
     
    public void setNumeroFamiliasEnVivienda(Integer  numeroFamiliasEnVivienda) {
        this.numeroFamiliasEnVivienda= numeroFamiliasEnVivienda;
    }

   /**
   * Return servicios
   */
    
   public List< ServiciosVivienda > getServicios() {
       return servicios;
   }

   /**
   * Set a value to parameter servicios
   */
    
   public void setServicios(List < ServiciosVivienda > servicios) {
       this.servicios= servicios;
   } 

    /**
    * Return telefono
    */
     
    public  String getTelefono() {
        return telefono;
    }

    /**
    * Set a value to parameter telefono
    */
     
    public void setTelefono(String  telefono) {
        this.telefono= telefono;
    }

    /**
    * Return direccionDomicilio
    */
     
    public  String getDireccionDomicilio() {
        return direccionDomicilio;
    }

    /**
    * Set a value to parameter direccionDomicilio
    */
     
    public void setDireccionDomicilio(String  direccionDomicilio) {
        this.direccionDomicilio= direccionDomicilio;
    }

    /**
    * Return estrato
    */
     
    public  Integer getEstrato() {
        return estrato;
    }

    /**
    * Set a value to parameter estrato
    */
     
    public void setEstrato(Integer  estrato) {
        this.estrato= estrato;
    }

   /**
   * Return familias
   */
    
   public List< Familia > getFamilias() {
       return familias;
   }

   /**
   * Set a value to parameter familias
   */
    
   public void setFamilias(List < Familia > familias) {
       this.familias= familias;
   } 

    /**
    * Return barrio
    */
     
    public  String getBarrio() {
        return barrio;
    }

    /**
    * Set a value to parameter barrio
    */
     
    public void setBarrio(String  barrio) {
        this.barrio= barrio;
    }

    /**
    * Return salubridad
    */
     
    public  SalubridadVivienda getSalubridad() {
        return salubridad;
    }

    /**
    * Set a value to parameter salubridad
    */
     
    public void setSalubridad(SalubridadVivienda  salubridad) {
        this.salubridad= salubridad;
    }
}
