
package com.prosofi.usme.model;



/**
   * Este codigo fue generado por sare.m2t.generator 
   * http://code.google.com/p/sare/
   * 
   */

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.util.*;
import javax.persistence.*;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@Entity
@Name("enfermedad")
@Scope(ScopeType.CONVERSATION)
@Table(name="enfermedad")
public class Enfermedad implements Serializable {

     private String nombreEnfermedad;

     private String tipo;

     @ManyToOne
     private Persona persona;

     @Id
     @GeneratedValue
     private Integer id;


    /**
    * Return tipo
    */
     
    public  String getTipo() {
        return tipo;
    }

    /**
    * Set a value to parameter tipo
    */
     
    public void setTipo(String  tipo) {
        this.tipo= tipo;
    }

    /**
    * Return persona
    */
     
    public  Persona getPersona() {
        return persona;
    }

    /**
    * Set a value to parameter persona
    */
     
    public void setPersona(Persona  persona) {
        this.persona= persona;
    }

    /**
    * Return id
    */
     
    public  Integer getId() {
        return id;
    }

    /**
    * Set a value to parameter id
    */
     
    public void setId(Integer  id) {
        this.id= id;
    }

    /**
    * Return nombreEnfermedad
    */
     
    public  String getNombreEnfermedad() {
        return nombreEnfermedad;
    }

    /**
    * Set a value to parameter nombreEnfermedad
    */
     
    public void setNombreEnfermedad(String  nombreEnfermedad) {
        this.nombreEnfermedad= nombreEnfermedad;
    }
}
