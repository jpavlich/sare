
package com.prosofi.usme.model;



/**
   * Este codigo fue generado por sare.m2t.generator 
   * http://code.google.com/p/sare/
   * 
   */

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.util.*;
import javax.persistence.*;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@Entity
@Name("percepcionVivienda")
@Scope(ScopeType.CONVERSATION)
@Table(name="percepcionVivienda")
public class PercepcionVivienda implements Serializable {

     private String loQueLeGusta;

     @Id
     @GeneratedValue
     private Integer id;

     private String loQueNoLeGusta;

     private Boolean seSienteAGusto;

     @OneToOne
     private Persona persona;


    /**
    * Return id
    */
     
    public  Integer getId() {
        return id;
    }

    /**
    * Set a value to parameter id
    */
     
    public void setId(Integer  id) {
        this.id= id;
    }

    /**
    * Return seSienteAGusto
    */
     
    public  Boolean getSeSienteAGusto() {
        return seSienteAGusto;
    }

    /**
    * Set a value to parameter seSienteAGusto
    */
     
    public void setSeSienteAGusto(Boolean  seSienteAGusto) {
        this.seSienteAGusto= seSienteAGusto;
    }

    /**
    * Return loQueNoLeGusta
    */
     
    public  String getLoQueNoLeGusta() {
        return loQueNoLeGusta;
    }

    /**
    * Set a value to parameter loQueNoLeGusta
    */
     
    public void setLoQueNoLeGusta(String  loQueNoLeGusta) {
        this.loQueNoLeGusta= loQueNoLeGusta;
    }

    /**
    * Return persona
    */
     
    public  Persona getPersona() {
        return persona;
    }

    /**
    * Set a value to parameter persona
    */
     
    public void setPersona(Persona  persona) {
        this.persona= persona;
    }

    /**
    * Return loQueLeGusta
    */
     
    public  String getLoQueLeGusta() {
        return loQueLeGusta;
    }

    /**
    * Set a value to parameter loQueLeGusta
    */
     
    public void setLoQueLeGusta(String  loQueLeGusta) {
        this.loQueLeGusta= loQueLeGusta;
    }
}
