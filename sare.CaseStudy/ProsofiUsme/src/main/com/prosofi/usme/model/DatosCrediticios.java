
package com.prosofi.usme.model;



/**
   * Este codigo fue generado por sare.m2t.generator 
   * http://code.google.com/p/sare/
   * 
   */

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.util.*;
import javax.persistence.*;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@Entity
@Name("datosCrediticios")
@Scope(ScopeType.CONVERSATION)
@Table(name="datosCrediticios")
public class DatosCrediticios implements Serializable {

     @Id
     @GeneratedValue
     private Integer id;

     @ManyToOne
     private Persona persona;

     private String entidadDeCredito;

     private Double montoDeLaDeuda;


    /**
    * Return montoDeLaDeuda
    */
     
    public  Double getMontoDeLaDeuda() {
        return montoDeLaDeuda;
    }

    /**
    * Set a value to parameter montoDeLaDeuda
    */
     
    public void setMontoDeLaDeuda(Double  montoDeLaDeuda) {
        this.montoDeLaDeuda= montoDeLaDeuda;
    }

    /**
    * Return id
    */
     
    public  Integer getId() {
        return id;
    }

    /**
    * Set a value to parameter id
    */
     
    public void setId(Integer  id) {
        this.id= id;
    }

    /**
    * Return persona
    */
     
    public  Persona getPersona() {
        return persona;
    }

    /**
    * Set a value to parameter persona
    */
     
    public void setPersona(Persona  persona) {
        this.persona= persona;
    }

    /**
    * Return entidadDeCredito
    */
     
    public  String getEntidadDeCredito() {
        return entidadDeCredito;
    }

    /**
    * Set a value to parameter entidadDeCredito
    */
     
    public void setEntidadDeCredito(String  entidadDeCredito) {
        this.entidadDeCredito= entidadDeCredito;
    }
}
