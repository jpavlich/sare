
package com.prosofi.usme.model;



/**
   * Este codigo fue generado por sare.m2t.generator 
   * http://code.google.com/p/sare/
   * 
   */

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.util.*;
import javax.persistence.*;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@Entity
@Name("gastoMensuales")
@Scope(ScopeType.CONVERSATION)
@Table(name="gastoMensuales")
public class GastoMensuales implements Serializable {

     private Double valorGasto;

     @ManyToOne
     private Familia familia;

     private String gastosPorCategoria;

     @Id
     @GeneratedValue
     private Integer id;


    /**
    * Return familia
    */
     
    public  Familia getFamilia() {
        return familia;
    }

    /**
    * Set a value to parameter familia
    */
     
    public void setFamilia(Familia  familia) {
        this.familia= familia;
    }

    /**
    * Return valorGasto
    */
     
    public  Double getValorGasto() {
        return valorGasto;
    }

    /**
    * Set a value to parameter valorGasto
    */
     
    public void setValorGasto(Double  valorGasto) {
        this.valorGasto= valorGasto;
    }

    /**
    * Return id
    */
     
    public  Integer getId() {
        return id;
    }

    /**
    * Set a value to parameter id
    */
     
    public void setId(Integer  id) {
        this.id= id;
    }

    /**
    * Return gastosPorCategoria
    */
     
    public  String getGastosPorCategoria() {
        return gastosPorCategoria;
    }

    /**
    * Set a value to parameter gastosPorCategoria
    */
     
    public void setGastosPorCategoria(String  gastosPorCategoria) {
        this.gastosPorCategoria= gastosPorCategoria;
    }
}
