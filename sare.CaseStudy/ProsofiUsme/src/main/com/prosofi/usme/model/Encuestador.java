
package com.prosofi.usme.model;



/**
   * Este codigo fue generado por sare.m2t.generator 
   * http://code.google.com/p/sare/
   * 
   */

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.util.*;
import javax.persistence.*;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@Entity
@Name("encuestador")
@Scope(ScopeType.CONVERSATION)
@Table(name="encuestador")
public class Encuestador implements Serializable {

     private String fechaEncuesta;

     private String nombre;

     @Id
     @GeneratedValue
     private Integer id;

     @OneToMany(mappedBy="encuestador")
     private List< Familia > familias=new ArrayList<Familia>();

     private String horaInicio;

     private String horaFin;


   /**
   * Return familias
   */
    
   public List< Familia > getFamilias() {
       return familias;
   }

   /**
   * Set a value to parameter familias
   */
    
   public void setFamilias(List < Familia > familias) {
       this.familias= familias;
   } 

    /**
    * Return horaInicio
    */
     
    public  String getHoraInicio() {
        return horaInicio;
    }

    /**
    * Set a value to parameter horaInicio
    */
     
    public void setHoraInicio(String  horaInicio) {
        this.horaInicio= horaInicio;
    }

    /**
    * Return nombre
    */
     
    public  String getNombre() {
        return nombre;
    }

    /**
    * Set a value to parameter nombre
    */
     
    public void setNombre(String  nombre) {
        this.nombre= nombre;
    }

    /**
    * Return fechaEncuesta
    */
     
    public  String getFechaEncuesta() {
        return fechaEncuesta;
    }

    /**
    * Set a value to parameter fechaEncuesta
    */
     
    public void setFechaEncuesta(String  fechaEncuesta) {
        this.fechaEncuesta= fechaEncuesta;
    }

    /**
    * Return horaFin
    */
     
    public  String getHoraFin() {
        return horaFin;
    }

    /**
    * Set a value to parameter horaFin
    */
     
    public void setHoraFin(String  horaFin) {
        this.horaFin= horaFin;
    }

    /**
    * Return id
    */
     
    public  Integer getId() {
        return id;
    }

    /**
    * Set a value to parameter id
    */
     
    public void setId(Integer  id) {
        this.id= id;
    }
}
