
package com.prosofi.usme.model;



/**
   * Este codigo fue generado por sare.m2t.generator 
   * http://code.google.com/p/sare/
   * 
   */

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.util.*;
import javax.persistence.*;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@Entity
@Name("salud")
@Scope(ScopeType.CONVERSATION)
@Table(name="salud")
public class Salud implements Serializable {

     private String entidadDeSalud;

     @OneToOne
     private Persona persona;

     @Id
     @GeneratedValue
     private Integer id;

     private String nombreEntidadDeSalud;


    /**
    * Return persona
    */
     
    public  Persona getPersona() {
        return persona;
    }

    /**
    * Set a value to parameter persona
    */
     
    public void setPersona(Persona  persona) {
        this.persona= persona;
    }

    /**
    * Return entidadDeSalud
    */
     
    public  String getEntidadDeSalud() {
        return entidadDeSalud;
    }

    /**
    * Set a value to parameter entidadDeSalud
    */
     
    public void setEntidadDeSalud(String  entidadDeSalud) {
        this.entidadDeSalud= entidadDeSalud;
    }

    /**
    * Return nombreEntidadDeSalud
    */
     
    public  String getNombreEntidadDeSalud() {
        return nombreEntidadDeSalud;
    }

    /**
    * Set a value to parameter nombreEntidadDeSalud
    */
     
    public void setNombreEntidadDeSalud(String  nombreEntidadDeSalud) {
        this.nombreEntidadDeSalud= nombreEntidadDeSalud;
    }

    /**
    * Return id
    */
     
    public  Integer getId() {
        return id;
    }

    /**
    * Set a value to parameter id
    */
     
    public void setId(Integer  id) {
        this.id= id;
    }
}
