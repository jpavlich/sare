
package com.prosofi.usme.model;



/**
   * Este codigo fue generado por sare.m2t.generator 
   * http://code.google.com/p/sare/
   * 
   */

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.util.*;
import javax.persistence.*;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@Entity
@Name("alrededoresVivienda")
@Scope(ScopeType.CONVERSATION)
@Table(name="alrededoresVivienda")
public class AlrededoresVivienda implements Serializable {

     private String tipo;

     @Id
     @GeneratedValue
     private Integer id;

     @ManyToOne
     private Vivienda vivienda;


    /**
    * Return id
    */
     
    public  Integer getId() {
        return id;
    }

    /**
    * Set a value to parameter id
    */
     
    public void setId(Integer  id) {
        this.id= id;
    }

    /**
    * Return tipo
    */
     
    public  String getTipo() {
        return tipo;
    }

    /**
    * Set a value to parameter tipo
    */
     
    public void setTipo(String  tipo) {
        this.tipo= tipo;
    }

    /**
    * Return vivienda
    */
     
    public  Vivienda getVivienda() {
        return vivienda;
    }

    /**
    * Set a value to parameter vivienda
    */
     
    public void setVivienda(Vivienda  vivienda) {
        this.vivienda= vivienda;
    }
}
