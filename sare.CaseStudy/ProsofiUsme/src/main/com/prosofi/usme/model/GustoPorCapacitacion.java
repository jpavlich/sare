
package com.prosofi.usme.model;



/**
   * Este codigo fue generado por sare.m2t.generator 
   * http://code.google.com/p/sare/
   * 
   */

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.util.*;
import javax.persistence.*;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@Entity
@Name("gustoPorCapacitacion")
@Scope(ScopeType.CONVERSATION)
@Table(name="gustoPorCapacitacion")
public class GustoPorCapacitacion implements Serializable {

     @ManyToOne
     private Persona persona;

     private String tipoGusto;

     @Id
     @GeneratedValue
     private Integer id;


    /**
    * Return persona
    */
     
    public  Persona getPersona() {
        return persona;
    }

    /**
    * Set a value to parameter persona
    */
     
    public void setPersona(Persona  persona) {
        this.persona= persona;
    }

    /**
    * Return id
    */
     
    public  Integer getId() {
        return id;
    }

    /**
    * Set a value to parameter id
    */
     
    public void setId(Integer  id) {
        this.id= id;
    }

    /**
    * Return tipoGusto
    */
     
    public  String getTipoGusto() {
        return tipoGusto;
    }

    /**
    * Set a value to parameter tipoGusto
    */
     
    public void setTipoGusto(String  tipoGusto) {
        this.tipoGusto= tipoGusto;
    }
}
