
package com.prosofi.usme.model;



/**
   * Este codigo fue generado por sare.m2t.generator 
   * http://code.google.com/p/sare/
   * 
   */

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.util.*;
import javax.persistence.*;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@Entity
@Name("persona")
@Scope(ScopeType.CONVERSATION)
@Table(name="persona")
public class Persona implements Serializable {

     private String actividadesTiempoLibre;

     private String genero;

     private String condicionesDeVidaEnUnAnio;

     private String rolEnLaFamilia;

     private String lugarRegistroNumeroIdentificacion;

     @OneToOne(mappedBy="persona")
     private Salud salud;

     private Integer aniosViviendoEnBarrio;

     private String habilidadesEducativas;

     private String estadoCivil;

     private String hablidades;

     private Integer numeroIdentificacion;

     @OneToOne(mappedBy="persona")
     private PercepcionVivienda percepcionVivienda;

     private String nombres;

     @OneToMany(cascade=CascadeType.ALL,mappedBy="persona")
     private List< DatosCrediticios > datosCrediticios=new ArrayList<DatosCrediticios>();

     private String apellidos;

     private String expectativasProgramaProsofi;

     private String ocupacion;

     private Integer aniosViviendoEnDomicilio;

     @OneToOne(mappedBy="persona")
     private ControlPrenatal controlPrenatal;

     @OneToMany(cascade=CascadeType.ALL,mappedBy="persona")
     private List< GustoPorCapacitacion > gustosPorCapacitacion=new ArrayList<GustoPorCapacitacion>();

     @OneToMany(cascade=CascadeType.ALL,mappedBy="persona")
     private List< TiempoDisponible > tiempoDisponible=new ArrayList<TiempoDisponible>();

     @OneToOne(mappedBy="persona")
     private DatosEconomico datosEconomicos;

     @Id
     @GeneratedValue
     private Integer id;

     private String aporteCompromisoConProsofi;

     @ManyToOne
     private Familia familia;

     private String nivelEstudios;

     private String fechaDeNacimiento;

     @OneToMany(mappedBy="persona")
     private List< Enfermedad > enfermedades=new ArrayList<Enfermedad>();


    /**
    * Return expectativasProgramaProsofi
    */
     
    public  String getExpectativasProgramaProsofi() {
        return expectativasProgramaProsofi;
    }

    /**
    * Set a value to parameter expectativasProgramaProsofi
    */
     
    public void setExpectativasProgramaProsofi(String  expectativasProgramaProsofi) {
        this.expectativasProgramaProsofi= expectativasProgramaProsofi;
    }

    /**
    * Return datosEconomicos
    */
     
    public  DatosEconomico getDatosEconomicos() {
        return datosEconomicos;
    }

    /**
    * Set a value to parameter datosEconomicos
    */
     
    public void setDatosEconomicos(DatosEconomico  datosEconomicos) {
        this.datosEconomicos= datosEconomicos;
    }

    /**
    * Return estadoCivil
    */
     
    public  String getEstadoCivil() {
        return estadoCivil;
    }

    /**
    * Set a value to parameter estadoCivil
    */
     
    public void setEstadoCivil(String  estadoCivil) {
        this.estadoCivil= estadoCivil;
    }

    /**
    * Return genero
    */
     
    public  String getGenero() {
        return genero;
    }

    /**
    * Set a value to parameter genero
    */
     
    public void setGenero(String  genero) {
        this.genero= genero;
    }

    /**
    * Return aniosViviendoEnBarrio
    */
     
    public  Integer getAniosViviendoEnBarrio() {
        return aniosViviendoEnBarrio;
    }

    /**
    * Set a value to parameter aniosViviendoEnBarrio
    */
     
    public void setAniosViviendoEnBarrio(Integer  aniosViviendoEnBarrio) {
        this.aniosViviendoEnBarrio= aniosViviendoEnBarrio;
    }

    /**
    * Return fechaDeNacimiento
    */
     
    public  String getFechaDeNacimiento() {
        return fechaDeNacimiento;
    }

    /**
    * Set a value to parameter fechaDeNacimiento
    */
     
    public void setFechaDeNacimiento(String  fechaDeNacimiento) {
        this.fechaDeNacimiento= fechaDeNacimiento;
    }

    /**
    * Return lugarRegistroNumeroIdentificacion
    */
     
    public  String getLugarRegistroNumeroIdentificacion() {
        return lugarRegistroNumeroIdentificacion;
    }

    /**
    * Set a value to parameter lugarRegistroNumeroIdentificacion
    */
     
    public void setLugarRegistroNumeroIdentificacion(String  lugarRegistroNumeroIdentificacion) {
        this.lugarRegistroNumeroIdentificacion= lugarRegistroNumeroIdentificacion;
    }

    /**
    * Return aniosViviendoEnDomicilio
    */
     
    public  Integer getAniosViviendoEnDomicilio() {
        return aniosViviendoEnDomicilio;
    }

    /**
    * Set a value to parameter aniosViviendoEnDomicilio
    */
     
    public void setAniosViviendoEnDomicilio(Integer  aniosViviendoEnDomicilio) {
        this.aniosViviendoEnDomicilio= aniosViviendoEnDomicilio;
    }

    /**
    * Return habilidadesEducativas
    */
     
    public  String getHabilidadesEducativas() {
        return habilidadesEducativas;
    }

    /**
    * Set a value to parameter habilidadesEducativas
    */
     
    public void setHabilidadesEducativas(String  habilidadesEducativas) {
        this.habilidadesEducativas= habilidadesEducativas;
    }

    /**
    * Return ocupacion
    */
     
    public  String getOcupacion() {
        return ocupacion;
    }

    /**
    * Set a value to parameter ocupacion
    */
     
    public void setOcupacion(String  ocupacion) {
        this.ocupacion= ocupacion;
    }

    /**
    * Return condicionesDeVidaEnUnAnio
    */
     
    public  String getCondicionesDeVidaEnUnAnio() {
        return condicionesDeVidaEnUnAnio;
    }

    /**
    * Set a value to parameter condicionesDeVidaEnUnAnio
    */
     
    public void setCondicionesDeVidaEnUnAnio(String  condicionesDeVidaEnUnAnio) {
        this.condicionesDeVidaEnUnAnio= condicionesDeVidaEnUnAnio;
    }

    /**
    * Return salud
    */
     
    public  Salud getSalud() {
        return salud;
    }

    /**
    * Set a value to parameter salud
    */
     
    public void setSalud(Salud  salud) {
        this.salud= salud;
    }

    /**
    * Return aporteCompromisoConProsofi
    */
     
    public  String getAporteCompromisoConProsofi() {
        return aporteCompromisoConProsofi;
    }

    /**
    * Set a value to parameter aporteCompromisoConProsofi
    */
     
    public void setAporteCompromisoConProsofi(String  aporteCompromisoConProsofi) {
        this.aporteCompromisoConProsofi= aporteCompromisoConProsofi;
    }

    /**
    * Return apellidos
    */
     
    public  String getApellidos() {
        return apellidos;
    }

    /**
    * Set a value to parameter apellidos
    */
     
    public void setApellidos(String  apellidos) {
        this.apellidos= apellidos;
    }

    /**
    * Return controlPrenatal
    */
     
    public  ControlPrenatal getControlPrenatal() {
        return controlPrenatal;
    }

    /**
    * Set a value to parameter controlPrenatal
    */
     
    public void setControlPrenatal(ControlPrenatal  controlPrenatal) {
        this.controlPrenatal= controlPrenatal;
    }

    /**
    * Return percepcionVivienda
    */
     
    public  PercepcionVivienda getPercepcionVivienda() {
        return percepcionVivienda;
    }

    /**
    * Set a value to parameter percepcionVivienda
    */
     
    public void setPercepcionVivienda(PercepcionVivienda  percepcionVivienda) {
        this.percepcionVivienda= percepcionVivienda;
    }

    /**
    * Return id
    */
     
    public  Integer getId() {
        return id;
    }

    /**
    * Set a value to parameter id
    */
     
    public void setId(Integer  id) {
        this.id= id;
    }

   /**
   * Return datosCrediticios
   */
    
   public List< DatosCrediticios > getDatosCrediticios() {
       return datosCrediticios;
   }

   /**
   * Set a value to parameter datosCrediticios
   */
    
   public void setDatosCrediticios(List < DatosCrediticios > datosCrediticios) {
       this.datosCrediticios= datosCrediticios;
   } 

    /**
    * Return familia
    */
     
    public  Familia getFamilia() {
        return familia;
    }

    /**
    * Set a value to parameter familia
    */
     
    public void setFamilia(Familia  familia) {
        this.familia= familia;
    }

    /**
    * Return actividadesTiempoLibre
    */
     
    public  String getActividadesTiempoLibre() {
        return actividadesTiempoLibre;
    }

    /**
    * Set a value to parameter actividadesTiempoLibre
    */
     
    public void setActividadesTiempoLibre(String  actividadesTiempoLibre) {
        this.actividadesTiempoLibre= actividadesTiempoLibre;
    }

    /**
    * Return nombres
    */
     
    public  String getNombres() {
        return nombres;
    }

    /**
    * Set a value to parameter nombres
    */
     
    public void setNombres(String  nombres) {
        this.nombres= nombres;
    }

    /**
    * Return rolEnLaFamilia
    */
     
    public  String getRolEnLaFamilia() {
        return rolEnLaFamilia;
    }

    /**
    * Set a value to parameter rolEnLaFamilia
    */
     
    public void setRolEnLaFamilia(String  rolEnLaFamilia) {
        this.rolEnLaFamilia= rolEnLaFamilia;
    }

   /**
   * Return gustosPorCapacitacion
   */
    
   public List< GustoPorCapacitacion > getGustosPorCapacitacion() {
       return gustosPorCapacitacion;
   }

   /**
   * Set a value to parameter gustosPorCapacitacion
   */
    
   public void setGustosPorCapacitacion(List < GustoPorCapacitacion > gustosPorCapacitacion) {
       this.gustosPorCapacitacion= gustosPorCapacitacion;
   } 

   /**
   * Return enfermedades
   */
    
   public List< Enfermedad > getEnfermedades() {
       return enfermedades;
   }

   /**
   * Set a value to parameter enfermedades
   */
    
   public void setEnfermedades(List < Enfermedad > enfermedades) {
       this.enfermedades= enfermedades;
   } 

   /**
   * Return tiempoDisponible
   */
    
   public List< TiempoDisponible > getTiempoDisponible() {
       return tiempoDisponible;
   }

   /**
   * Set a value to parameter tiempoDisponible
   */
    
   public void setTiempoDisponible(List < TiempoDisponible > tiempoDisponible) {
       this.tiempoDisponible= tiempoDisponible;
   } 

    /**
    * Return numeroIdentificacion
    */
     
    public  Integer getNumeroIdentificacion() {
        return numeroIdentificacion;
    }

    /**
    * Set a value to parameter numeroIdentificacion
    */
     
    public void setNumeroIdentificacion(Integer  numeroIdentificacion) {
        this.numeroIdentificacion= numeroIdentificacion;
    }

    /**
    * Return hablidades
    */
     
    public  String getHablidades() {
        return hablidades;
    }

    /**
    * Set a value to parameter hablidades
    */
     
    public void setHablidades(String  hablidades) {
        this.hablidades= hablidades;
    }

    /**
    * Return nivelEstudios
    */
     
    public  String getNivelEstudios() {
        return nivelEstudios;
    }

    /**
    * Set a value to parameter nivelEstudios
    */
     
    public void setNivelEstudios(String  nivelEstudios) {
        this.nivelEstudios= nivelEstudios;
    }
}
