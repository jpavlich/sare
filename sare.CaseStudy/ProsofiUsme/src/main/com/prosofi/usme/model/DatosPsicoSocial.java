
package com.prosofi.usme.model;



/**
   * Este codigo fue generado por sare.m2t.generator 
   * http://code.google.com/p/sare/
   * 
   */

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.util.*;
import javax.persistence.*;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@Entity
@Name("datosPsicoSocial")
@Scope(ScopeType.CONVERSATION)
@Table(name="datosPsicoSocial")
public class DatosPsicoSocial implements Serializable {

     @OneToOne
     private Familia familia;

     @Id
     @GeneratedValue
     private Integer id;

     @OneToMany(cascade=CascadeType.ALL,mappedBy="datoPsicoSocial")
     private List< Necesidad > necesidades=new ArrayList<Necesidad>();

     private String solucionProblemasComunidad;

     @OneToMany(cascade=CascadeType.ALL,mappedBy="datoPsicoSocial")
     private List< OrganizacionComunitaria > organizacionesComunitarias=new ArrayList<OrganizacionComunitaria>();

     private String solucionDeProblemas;


    /**
    * Return id
    */
     
    public  Integer getId() {
        return id;
    }

    /**
    * Set a value to parameter id
    */
     
    public void setId(Integer  id) {
        this.id= id;
    }

   /**
   * Return necesidades
   */
    
   public List< Necesidad > getNecesidades() {
       return necesidades;
   }

   /**
   * Set a value to parameter necesidades
   */
    
   public void setNecesidades(List < Necesidad > necesidades) {
       this.necesidades= necesidades;
   } 

    /**
    * Return familia
    */
     
    public  Familia getFamilia() {
        return familia;
    }

    /**
    * Set a value to parameter familia
    */
     
    public void setFamilia(Familia  familia) {
        this.familia= familia;
    }

   /**
   * Return organizacionesComunitarias
   */
    
   public List< OrganizacionComunitaria > getOrganizacionesComunitarias() {
       return organizacionesComunitarias;
   }

   /**
   * Set a value to parameter organizacionesComunitarias
   */
    
   public void setOrganizacionesComunitarias(List < OrganizacionComunitaria > organizacionesComunitarias) {
       this.organizacionesComunitarias= organizacionesComunitarias;
   } 

    /**
    * Return solucionDeProblemas
    */
     
    public  String getSolucionDeProblemas() {
        return solucionDeProblemas;
    }

    /**
    * Set a value to parameter solucionDeProblemas
    */
     
    public void setSolucionDeProblemas(String  solucionDeProblemas) {
        this.solucionDeProblemas= solucionDeProblemas;
    }

    /**
    * Return solucionProblemasComunidad
    */
     
    public  String getSolucionProblemasComunidad() {
        return solucionProblemasComunidad;
    }

    /**
    * Set a value to parameter solucionProblemasComunidad
    */
     
    public void setSolucionProblemasComunidad(String  solucionProblemasComunidad) {
        this.solucionProblemasComunidad= solucionProblemasComunidad;
    }
}
