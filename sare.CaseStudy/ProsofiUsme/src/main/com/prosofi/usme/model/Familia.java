
package com.prosofi.usme.model;



/**
   * Este codigo fue generado por sare.m2t.generator 
   * http://code.google.com/p/sare/
   * 
   */

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.util.*;
import javax.persistence.*;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@Entity
@Name("familia")
@Scope(ScopeType.CONVERSATION)
@Table(name="familia")
public class Familia implements Serializable {

     @OneToOne(mappedBy="familia")
     private DatosPsicoSocial datosPsicoSocial;

     @OneToMany(cascade=CascadeType.ALL,mappedBy="familia")
     private List< Persona > personas=new ArrayList<Persona>();

     @ManyToOne
     private Vivienda vivienda;

     @ManyToOne
     private Encuestador encuestador;

     @Id
     @GeneratedValue
     private Integer id;

     @OneToMany(cascade=CascadeType.ALL,mappedBy="familia")
     private List< Animal > animales=new ArrayList<Animal>();

     @OneToMany(cascade=CascadeType.ALL,mappedBy="familia")
     private List< GastoMensuales > gastosMensuales=new ArrayList<GastoMensuales>();


    /**
    * Return encuestador
    */
     
    public  Encuestador getEncuestador() {
        return encuestador;
    }

    /**
    * Set a value to parameter encuestador
    */
     
    public void setEncuestador(Encuestador  encuestador) {
        this.encuestador= encuestador;
    }

   /**
   * Return animales
   */
    
   public List< Animal > getAnimales() {
       return animales;
   }

   /**
   * Set a value to parameter animales
   */
    
   public void setAnimales(List < Animal > animales) {
       this.animales= animales;
   } 

    /**
    * Return vivienda
    */
     
    public  Vivienda getVivienda() {
        return vivienda;
    }

    /**
    * Set a value to parameter vivienda
    */
     
    public void setVivienda(Vivienda  vivienda) {
        this.vivienda= vivienda;
    }

   /**
   * Return personas
   */
    
   public List< Persona > getPersonas() {
       return personas;
   }

   /**
   * Set a value to parameter personas
   */
    
   public void setPersonas(List < Persona > personas) {
       this.personas= personas;
   } 

    /**
    * Return datosPsicoSocial
    */
     
    public  DatosPsicoSocial getDatosPsicoSocial() {
        return datosPsicoSocial;
    }

    /**
    * Set a value to parameter datosPsicoSocial
    */
     
    public void setDatosPsicoSocial(DatosPsicoSocial  datosPsicoSocial) {
        this.datosPsicoSocial= datosPsicoSocial;
    }

   /**
   * Return gastosMensuales
   */
    
   public List< GastoMensuales > getGastosMensuales() {
       return gastosMensuales;
   }

   /**
   * Set a value to parameter gastosMensuales
   */
    
   public void setGastosMensuales(List < GastoMensuales > gastosMensuales) {
       this.gastosMensuales= gastosMensuales;
   } 

    /**
    * Return id
    */
     
    public  Integer getId() {
        return id;
    }

    /**
    * Set a value to parameter id
    */
     
    public void setId(Integer  id) {
        this.id= id;
    }
}
