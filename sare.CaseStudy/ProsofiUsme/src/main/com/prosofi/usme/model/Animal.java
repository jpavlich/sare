
package com.prosofi.usme.model;



/**
   * Este codigo fue generado por sare.m2t.generator 
   * http://code.google.com/p/sare/
   * 
   */

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.util.*;
import javax.persistence.*;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@Entity
@Name("animal")
@Scope(ScopeType.CONVERSATION)
@Table(name="animal")
public class Animal implements Serializable {

     @ManyToOne
     private Familia familia;

     private Boolean estaVacunado;

     private String tipoAnimal;

     private Integer cantidad;

     @Id
     @GeneratedValue
     private Integer id;


    /**
    * Return familia
    */
     
    public  Familia getFamilia() {
        return familia;
    }

    /**
    * Set a value to parameter familia
    */
     
    public void setFamilia(Familia  familia) {
        this.familia= familia;
    }

    /**
    * Return estaVacunado
    */
     
    public  Boolean getEstaVacunado() {
        return estaVacunado;
    }

    /**
    * Set a value to parameter estaVacunado
    */
     
    public void setEstaVacunado(Boolean  estaVacunado) {
        this.estaVacunado= estaVacunado;
    }

    /**
    * Return cantidad
    */
     
    public  Integer getCantidad() {
        return cantidad;
    }

    /**
    * Set a value to parameter cantidad
    */
     
    public void setCantidad(Integer  cantidad) {
        this.cantidad= cantidad;
    }

    /**
    * Return tipoAnimal
    */
     
    public  String getTipoAnimal() {
        return tipoAnimal;
    }

    /**
    * Set a value to parameter tipoAnimal
    */
     
    public void setTipoAnimal(String  tipoAnimal) {
        this.tipoAnimal= tipoAnimal;
    }

    /**
    * Return id
    */
     
    public  Integer getId() {
        return id;
    }

    /**
    * Set a value to parameter id
    */
     
    public void setId(Integer  id) {
        this.id= id;
    }
}
