//$Id$
/*
 * Copyright (c) 2003 - 2009 by CincoSOFT Ltda. (cincosoft@cincosoft.com)
 * Colombia, South America.
 *
 * This code is provided free "AS IS", without a warranty of any kind.
 * ALL EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED.
 *
 * This software is in process of being published under de GPL License during 2009.
 * Meanwhile, CincoSOFT maintains all the rights over this software.
 *
 */
package com.cincosoft.project.security.menuList;

import java.util.ArrayList;
import java.util.List;

import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.framework.EntityHome;

import com.cincosoft.project.Menu;
import com.cincosoft.project.Module;
import com.cincosoft.project.Usecase;
import com.cincosoft.project.security.moduleList.ModuleHome;
import com.cincosoft.project.security.usecaseList.UsecaseHome;

@Name("menuHome")
@Restrict("#{s:hasRole('menuList')}")
public class MenuHome extends EntityHome<Menu> {

  @In(create = true)
  ModuleHome moduleHome;
  @In(create = true)
  MenuHome menuHome;
  @In(create = true)
  UsecaseHome usecaseHome;

  public void setMenuId(Long id) {
    setId(id);
  }

  public Long getMenuId() {
    return (Long) getId();
  }

  @Override
  protected Menu createInstance() {
    Menu menu = new Menu();
    return menu;
  }

  public void wire() {
    Module module = moduleHome.getDefinedInstance();
    if (module != null) {
      getInstance().setModule(module);
    }
    Usecase usecase = usecaseHome.getDefinedInstance();
    if (usecase != null) {
      getInstance().setUsecase(usecase);
    }
  }

  public boolean isWired() {
    if (getInstance().getModule() == null)
      return false;
    return true;
  }

  public Menu getDefinedInstance() {
    return isIdDefined() ? getInstance() : null;
  }

  public List<Menu> getSubmenus() {
    return getInstance() == null ? null : new ArrayList<Menu>(getInstance()
        .getSubmenus());
  }

}
