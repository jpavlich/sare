//$Id$
/*
 * Copyright (c) 2003 - 2009 by CincoSOFT Ltda. (cincosoft@cincosoft.com)
 * Colombia, South America.
 *
 * This code is provided free "AS IS", without a warranty of any kind.
 * ALL EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED.
 *
 * This software is in process of being published under de GPL License during 2009.
 * Meanwhile, CincoSOFT maintains all the rights over this software.
 *
 */
package com.cincosoft.project.security.usecaseList;

import java.util.Arrays;
import java.util.List;

import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.framework.EntityQuery;
import org.jboss.seam.log.Log;

import com.cincosoft.project.Module;
import com.cincosoft.project.Service;
import com.cincosoft.project.Usecase;
import com.cincosoft.project.security.profileGestion.ProfileGestion;
import com.cincosoft.project.security.profileGestion.ProfileGestionAction;

@Name("usecaseList")
@Restrict("#{s:hasRole('usecaseList')}")
public class UsecaseList extends EntityQuery<Usecase> {

  private static final String[] RESTRICTIONS = {
      "lower(usecase.name) like concat(lower(#{usecaseList.usecase.name}),'%')",
      "lower(usecase.display) like concat(lower(#{usecaseList.usecase.display}),'%')", };

  private static final String EJBQL = "select usecase from Usecase usecase";
  
  private Usecase usecase = new Usecase();

  public UsecaseList() {
      setEjbql(EJBQL);
      setRestrictionExpressionStrings(Arrays.asList(RESTRICTIONS));
      setMaxResults(25);
  }
  
  public Usecase getUsecase() {
    return usecase;
  }


}
