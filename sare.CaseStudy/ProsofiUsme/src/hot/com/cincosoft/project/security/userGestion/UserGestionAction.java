//$Id$
/*
 * Copyright (c) 2003 - 2009 by CincoSOFT Ltda. (cincosoft@cincosoft.com)
 * Colombia, South America.
 *
 * This code is provided free "AS IS", without a warranty of any kind.
 * ALL EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED.
 *
 * This software is in process of being published under de GPL License during 2009.
 * Meanwhile, CincoSOFT maintains all the rights over this software.
 *
 */
package com.cincosoft.project.security.userGestion;

import static javax.ejb.TransactionAttributeType.REQUIRED;
import static org.jboss.seam.ScopeType.SESSION;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.ejb.TransactionAttribute;
import javax.faces.model.SelectItem;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.log4j.MDC;
import org.jboss.seam.annotations.Factory;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Observer;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.datamodel.DataModel;
import org.jboss.seam.annotations.datamodel.DataModelSelection;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.core.Events;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.log.Log;
//import org.jboss.security.Util;
import org.jboss.crypto.CryptoUtil;

import com.cincosoft.project.Profile;
import com.cincosoft.project.User;
import com.cincosoft.project.Userprofile;

@Stateful
@Scope(SESSION)
@Name("userGestion")
@Restrict("#{s:hasRole('userGestion')}")
public class UserGestionAction implements UserGestion {

  @PersistenceContext
  private EntityManager em;

  @In(create = true)
  private transient FacesMessages facesMessages;

  @In
  private Events events;

  @Logger
  private Log log;

  private String clearPassword;
  private String verify;

  @DataModel
  private List<User> users;

  @DataModelSelection
  @Out(required = false)
  private User selectedUser;

  @Out(required = false)
  private List<Profile> systemProfiles;

  private List<Userprofile> userProfiles;

  private boolean addingNewUser = false;

  private Long profileToAdd;

  private Long profileToDelete;

  // for controlling what to show in screen:
  private boolean showUser = false;

  private boolean changePass = false;



  public boolean isShowUser() {
    return showUser;
  }

  public boolean isChangePass() {
    return changePass;
  }

  public String getClearPassword() {
    return clearPassword;
  }

  public void setClearPassword(String clearPassword) {
    this.clearPassword = clearPassword;
  }

  public String getVerify() {
    return verify;
  }

  public void setVerify(String verify) {
    this.verify = verify;
  }

  @Factory
  @Observer("profileEvent")
  @TransactionAttribute(REQUIRED)
  public void getUsers() {
    log.info("getUsers");
    users = em.createNamedQuery("User.findUsers").getResultList();

    systemProfiles = em.createNamedQuery("Profile.findProfiles").getResultList();
  }


  @Restrict("#{s:hasRole('userGestion_selectUser')}")
  public void selectUser() {
    log.info("selectUser");

    selectedUser = em.find(User.class, selectedUser.getUsername());
    userProfiles = selectedUser.getUserprofilesList();

    addingNewUser = false;
    showUser = true;
    changePass = false;
  }


  @Restrict("#{s:hasRole('userGestion_changePassword')}")
  public void changePassword() {
    log.info("changePassword");

    selectedUser = em.find(User.class, selectedUser.getUsername());
    userProfiles = selectedUser.getUserprofilesList();

    addingNewUser = false;
    showUser = true;
    changePass = true;
  }


  @Restrict("#{s:hasRole('userGestion_update')}")
  public void update() {
    log.info("update");

    if (clearPassword != null) {
      if (clearPassword.length() < 5 || clearPassword.length() > 15) {
        facesMessages.addToControl("password", "length must be between 5 and 15 ");
        verify = null;
        return;
      }

      if (!clearPassword.equals(verify)) {
        facesMessages.addToControl("verify", "Re-enter your password");
        verify = null;
        return;
      }

      //String hashedPassword = Util.createPasswordHash("MD5", Util.BASE64_ENCODING, null, null, clearPassword);
      String hashedPassword = CryptoUtil.createPasswordHash ("MD5",
              CryptoUtil.BASE64_ENCODING, null, null, clearPassword);
      selectedUser.setPassword(hashedPassword);
    }

    // insert or update the current profile:
    if (addingNewUser) { // new user
      em.persist(selectedUser);
      facesMessages.add("Please associate the new user to a profile at least");
    }
    else if (showUser) { // old user
      userProfiles = em.createNamedQuery("Userprofile.findProfilesOfUser")
          .setParameter("selectedUser", selectedUser).getResultList();

      Set<Userprofile> userProfilesSet = new HashSet<Userprofile>(userProfiles);
      selectedUser.setUserprofiles(userProfilesSet);
      em.merge(selectedUser);
    }

    users = em.createNamedQuery("User.findUsers").getResultList();
    addingNewUser = false;
    showUser = true;
    changePass = false;

  }


  public void cancelUpdates() {
    log.info("cancelUpdates");

    addingNewUser = false;
    showUser = false;
    changePass = false;
  }


  @Restrict("#{s:hasRole('userGestion_newUser')}")
  public void newUser() {
    log.info("newUser");
    selectedUser = new User();
    userProfiles = null;

    addingNewUser = true;
    showUser = true;
    changePass = true;
  }


  @Restrict("#{s:hasRole('userGestion_deleteUser')}")
  public void deleteUser() {
    log.info("deleteUser");

    selectedUser = em.find(User.class, selectedUser.getUsername());
    em.remove(selectedUser); // cascade =>  delete associated profiles

    users = em.createNamedQuery("User.findUsers").getResultList();

    addingNewUser = false;
    showUser = false;
    changePass = false;

    events.raiseEvent("changeUserProfile");
  }


  /**
   * items of Select: <f:selectItems value="#{userGestion.systemProfiles}"/>
   */
  public List<SelectItem> getSystemProfiles() {
    log.info("getSystemProfiles");

    List<SelectItem> selectItems = new ArrayList<SelectItem>();
    for (Profile p : systemProfiles) {
      if (!foundSystemProfileInUserProfiles(p)) {
        selectItems.add(new SelectItem(p.getId(), p.getName())); // value, display
      }
    }
    return selectItems;
  }


  /**
   * initial selected item (its value) in the Select:
   * value="#{userGestion.systemProfile}"
   */
  public Long getSystemProfile() {
    log.info("getSystemProfile");
    // return ((Profile)systemProfiles.get(0)).getId();
    return null;
  }


  /**
   * selected item by the user (its value): value="#{userGestion.systemProfile}"
   */
  public void setSystemProfile(Long s) {
    log.info("selected system profile: " + s);
    profileToAdd = s;
  }

  /**
   * items of Select: <f:selectItems value="#{userGestion.userProfiles}"/>
   */
  public List<SelectItem> getUserProfiles() {
    log.info("getUserProfiles");

    List<SelectItem> selectItems = new ArrayList<SelectItem>();
    if (userProfiles == null) {
      return selectItems;
    }

    for (Userprofile u : userProfiles) {
      selectItems.add(new SelectItem(u.getId(), u.getProfile().getName())); // value, display
    }
    return selectItems;
  }


  /**
   * initial selected item (its value) in the Select:
   * value="#{userGestion.userProfile}"
   */
  public Long getUserProfile() {
    log.info("getUserProfile");
    // return ((UserProfile)userProfiles.get(0)).getId();
    return null;
  }


  /**
   * selected item by the user (its value): value="#{userGestion.userProfile}"
   */
  public void setUserProfile(Long s) {
    log.info("selected user profile: " + s);
    profileToDelete = s;
  }


  @Restrict("#{s:hasRole('userGestion_addProfile')}")
  public void addProfile() {
    log.info("addProfile: " + profileToAdd);

    if (profileToAdd != null) {
      Profile systemProfile = null;
      boolean found = false;
      for (int i = 0; i < systemProfiles.size() && !found; i++) {
        systemProfile = (Profile) systemProfiles.get(i);
        if (systemProfile.getId().equals(profileToAdd)) {
          found = true;
        }
      }

      Userprofile userProfile = new Userprofile(selectedUser, systemProfile);
      em.persist(userProfile);

      userProfiles = em.createNamedQuery("Userprofile.findProfilesOfUser")
          .setParameter("selectedUser", selectedUser).getResultList();

      events.raiseEvent("changeUserProfile");
    }

    addingNewUser = false;
    showUser = true;
    changePass = false;

  }

  @Restrict("#{s:hasRole('userGestion_deleteProfile')}")
  public void deleteProfile() {
    log.info("deleteProfile: " + profileToDelete);

    if (profileToDelete != null) {
      Userprofile up = em.find(Userprofile.class, profileToDelete);
      em.remove(up);
      em.flush();

      events.raiseEvent("changeUserProfile");
      userProfiles = em.createNamedQuery("Userprofile.findProfilesOfUser")
          .setParameter("selectedUser", selectedUser).getResultList();
    }

    addingNewUser = false;
    showUser = true;
    changePass = false;
  }


  @Remove
  public void destroy() {
  }


  private boolean foundSystemProfileInUserProfiles(Profile systemProfile) {
    boolean found = false;
    if (userProfiles == null) {
      return found;
    }

    for (int i = 0; i < userProfiles.size() && !found; i++) {
      Profile userProfile = ((Userprofile) userProfiles.get(i)).getProfile();
      if (systemProfile.getId().equals(userProfile.getId())) {
        found = true;
      }
    }
    return found;
  }

}
