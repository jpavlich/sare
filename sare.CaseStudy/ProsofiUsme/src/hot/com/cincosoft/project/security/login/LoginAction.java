//$Id$
/*
 * Copyright (c) 2003 - 2009 by CincoSOFT Ltda. (cincosoft@cincosoft.com)
 * Colombia, South America.
 *
 * This code is provided free "AS IS", without a warranty of any kind.
 * ALL EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED.
 *
 * This software is in process of being published under de GPL License during 2009.
 * Meanwhile, CincoSOFT maintains all the rights over this software.
 *
 */
package com.cincosoft.project.security.login;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;
import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.el.ExpressionFactory;
import javax.el.MethodExpression;
import javax.faces.component.UIComponentBase;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpSession;

import org.apache.log4j.MDC;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.Synchronized;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.log.Log;
import org.jboss.seam.security.Credentials;
import org.jboss.seam.security.Identity;
import org.jboss.seam.ui.component.html.HtmlLink;

//import org.jboss.security.Util;
import org.jboss.crypto.CryptoUtil;

import org.richfaces.component.html.HtmlDropDownMenu;
import org.richfaces.component.html.HtmlMenuGroup;
import org.richfaces.component.html.HtmlMenuItem;
import org.richfaces.component.html.HtmlMenuSeparator;
import org.richfaces.component.html.HtmlToolBar;
import org.richfaces.component.html.HtmlToolBarGroup;

import com.cincosoft.project.Menu;
import com.cincosoft.project.Module;
import com.cincosoft.project.Parameter;
import com.cincosoft.project.Usecase;
import com.cincosoft.project.User;
import com.cincosoft.project.Menu.Status;

@Stateful
@Scope(ScopeType.SESSION)
@Synchronized
@Name("login")
//@Restrict ("#{s:hasRole('login')}")
public class LoginAction implements Login {

  @Resource
  javax.ejb.SessionContext ctx;

  @PersistenceContext
  private EntityManager em;

  @In
  private FacesMessages facesMessages;

  @Logger
  private Log log;

  @In
  private Identity identity;
  @In 
  private Credentials credentials;

  @In(required = false)
  @Out(required = false, scope = ScopeType.SESSION)
  private User user;

  private boolean loggedIn = false;

  @In(required = false)
  @Out(required = false, scope = ScopeType.SESSION)
  private List<Menu> mainMenus = new ArrayList<Menu>();

  @In(required = false)
  @Out(required = false, scope = ScopeType.SESSION)
  private List<Menu> systemMenus = new ArrayList<Menu>();
  
  @In(required = false)
  @Out(required = false, scope = ScopeType.SESSION)
  private HtmlToolBarGroup menusBarGroup;

  // for menu items
  private String prefixImages = "/common/img/";


  @Restrict ("#{s:hasRole('login')}")
  public boolean isLoggedIn() {
    log.info("isLoggedIn=" + loggedIn);
    return loggedIn;
  }


  public boolean authenticate() {
    log.info("**authenticate");

    try {
      
      String hashedPassword = CryptoUtil.createPasswordHash ("MD5",
              CryptoUtil.BASE64_ENCODING, null, null, credentials.getPassword());

      user = (User) em.createQuery(
          "select u from User u where u.username=#{credentials.username} and u.password=:hashedPassword")
          .setParameter("hashedPassword", hashedPassword).getSingleResult();
      log.info("user=" + user);

      List<String> roles = em.createNativeQuery(
          "select name_role from sec_user_role where username= ? ",
          "User.RS.Role").setParameter(1, credentials.getUsername())
          .getResultList();
      log.info("size of roles=" + roles.size());

      for (String role : roles) {
        identity.addRole(role);
      }

      log.info("identity.isLoggedIn()=" + identity.isLoggedIn());
      log.info("authenticate returns true");
      return true;
    }
    catch (Exception e) {
      log.info("Invalid username/password" + e.getMessage());
      facesMessages.add("Invalid username/password");
      return false;
    }
  }

  /**
   * this method is executed as event reaction before rendering main screen or
   * approvalTasks screen
   */
  @Restrict ("#{s:hasRole('login_login')}")
  public void login() {
    log.info("**login");

    if (loggedIn) {
      log.info("user is already in the session");
      return;
    }

    // obtain logged user
    log.info("username=#{identity.username}");
    user = (User) em.createQuery("select u from User u where u.username=#{identity.username}")
      .getSingleResult();

    String userName = user.getUsername();
    log.info("userName from identity=" + userName);
    Contexts.getSessionContext().set("userName", userName);
    Contexts.getSessionContext().set("application", "skeleton");

    MDC.put("user", Contexts.getSessionContext().get("userName"));
    MDC.put("application", Contexts.getSessionContext().get("application"));
    log.info("===================== NEW LOGIN FOR USER=" + userName);
    loggedIn = true;

    // web session timeout:
    obtainSessionTmeout();

    // MENU corresponding to user profiles
    menusForUser();

    // building toolBar with RichFaces menu
    buildToolBarWithMenu();
  }


  @Restrict ("#{s:hasRole('login')}")
  public void logout() {
    log.info("logout");
    MDC.put("user", "");
    MDC.put("application", "");
    identity.logout();
  }

  @Remove
  public void destroy() {
  }

  /**
   * Obtain session timeout
   */
  private void obtainSessionTmeout() {
    log.info("obtainSessionTmeout");

    ExternalContext externalContext = (ExternalContext) FacesContext
      .getCurrentInstance().getExternalContext();

    HttpSession session = null;
    try {
      session = (HttpSession) externalContext.getSession(true);
    }
    catch (Exception e) {
      log.info("it is a PortletSession or Unknown type of session ");
    }
    log.info("maxInactiveInterval in seconds="+ session.getMaxInactiveInterval());

    List<Parameter> parameters = em.createNamedQuery("Parameter.findParameters").getResultList();

    String sessionTimeoutValue = null; // in minutes
    for (int i = 0; i < parameters.size() && sessionTimeoutValue == null; i++) {
      Parameter p = (Parameter) parameters.get(i);
      if (p.getName().equals("sessionTimeout")) {
        sessionTimeoutValue = p.getValue();
        log.info("sessionTimeout parameter found, value="+ sessionTimeoutValue);
      }
    }
    if (sessionTimeoutValue != null) {
      // new timeout in seconds
      session.setMaxInactiveInterval(Integer.valueOf(sessionTimeoutValue) * 60);
    }
    log.info("new maxInactiveInterval in seconds="+ session.getMaxInactiveInterval());
  }


  /**
   * Build dynamic menu for user, corresponding to his/her profiles
   */
  private void menusForUser() {
    log.info("menusForUser");

    mainMenus = em.createQuery(
        " select m from Menu m where m.parent is null "
      + " and m.status = :status "
      + " order by m.titulo")
      .setParameter("status", Status.ACTIVE).getResultList();

    systemMenus = em.createQuery(
        " select m from Menu m where m.parent is not null "
      + " and m.status = :status " + " order by m.parent.id, m.id")
      .setParameter("status", Status.ACTIVE).getResultList();

    // add mainMenus to systemMenus
    for (int i = 0; i < mainMenus.size(); i++) {
      Menu m = (Menu) mainMenus.get(i);
      systemMenus.add(i, m);
    }

    // get parent and submenus of each element of systemMenus;
    // delete items that do not belong to user roles
    for (int i = 0; i < systemMenus.size(); i++) {
      Menu m = (Menu) systemMenus.get(i);
      Module module = m.getModule();
      Usecase usecase = m.getUsecase();
      Menu parent = m.getParent();

      Set<Menu> submenus = m.getSubmenus();
      m.setNumChilds(submenus.size());

      if (usecase != null && !identity.hasRole(usecase.getName())) {
        int numChildsParent = m.getParent().getNumChilds();
        numChildsParent--;
        m.getParent().setNumChilds(numChildsParent);
        systemMenus.remove(i);
        i--;
      }
    }

    // delete submenus with 0 childs
    boolean foundEmptySubmenus = true;
    while (foundEmptySubmenus) {
      foundEmptySubmenus = false;
      for (int i = 0; i < systemMenus.size(); i++) {
        Menu m = (Menu) systemMenus.get(i);
        if (m.getNumChilds() == 0 && m.getUsecase() == null
            && m.getParent() != null) {
          int numChildsParent = m.getParent().getNumChilds();
          numChildsParent--;
          m.getParent().setNumChilds(numChildsParent);
          systemMenus.remove(i);
          i--;
          foundEmptySubmenus = true;
        }
      }
    }
  }

  /**
   * Builds toolBar wiht Richfaces menu
   */
  private void buildToolBarWithMenu() {
    log.info("buildToolBarWithMenu");
    FacesContext fc = FacesContext.getCurrentInstance();
    ExpressionFactory ef = fc.getApplication().getExpressionFactory();
    
    menusBarGroup = new HtmlToolBarGroup();

    // dynamic menu
    List<HtmlDropDownMenu> theMenu = getMenuRich();
    for (HtmlDropDownMenu mainMenu : theMenu) {
      menusBarGroup.getChildren().add(mainMenu);
    }

  }


  /**
   * builds all the menu as a list of HtmlDropDownMenu (Richfaces elements)
   *
   * @return
   */
  private List<HtmlDropDownMenu> getMenuRich() {
    log.info("getTheMenu");

    List<HtmlDropDownMenu> theMenu = new ArrayList();
    HtmlDropDownMenu mainMenu = null;
    List<Menu> subSystemMenus = null;

    for (int i = 0; i < systemMenus.size(); i++) {
      Menu m = (Menu) systemMenus.get(i);
      if (m.getParent() == null) {
        mainMenu = new HtmlDropDownMenu();
        mainMenu.setDirection("bottom-right");
        mainMenu.setSubmitMode("ajax");
        mainMenu.setValue(m.getTitulo());
        log.info("mainMenu:" + mainMenu.getValue());
        theMenu.add(mainMenu);

        subSystemMenus = systemMenus.subList(i + 1, systemMenus.size());
        getSubMenusRich(mainMenu, m.getId(), m.getNumChilds(), subSystemMenus);
      }
    }
    return theMenu;
  }

  private void getSubMenusRich(UIComponentBase parentMenu, Long idParent,
      int numParentChilds, List<Menu> subSystemMenus) {
    log.info("getSubMenusRich for idParent=" + idParent);

    FacesContext fc = FacesContext.getCurrentInstance();
    ExpressionFactory ef = fc.getApplication().getExpressionFactory();

    int numberSeparators = 0;

    for (int i = 0; i < subSystemMenus.size(); i++) {
      Menu m = (Menu) subSystemMenus.get(i);

      if (m.getParent() != null && m.getParent().getId() == idParent && m.getNumChilds() == 0) {
        // terminal node
        HtmlMenuItem menuItem = new HtmlMenuItem();
       
        menuItem.setValue(m.getUsecase() != null
            ? m.getUsecase().getDisplay()
            : m.getTitulo());
        
        menuItem.setSubmitMode("ajax");

        MethodExpression me = ef.createMethodExpression
          (fc.getELContext(), m.getAction(), null, new Class[0]);
        menuItem.setActionExpression(me);
        menuItem.setIcon(prefixImages + m.getIcon());

        log.info("----menuItem:" + menuItem.getValue());
        parentMenu.getChildren().add(menuItem);

        numberSeparators = (parentMenu.getChildCount() - 1) / 2;
        if ((parentMenu.getChildCount() - numberSeparators) < numParentChilds) {
          parentMenu.getChildren().add(new HtmlMenuSeparator());
        }
      }
      else if (m.getParent() != null && m.getParent().getId() == idParent && m.getNumChilds() != 0) {
        // no terminal node
        HtmlMenuGroup menuGroup = new HtmlMenuGroup();
        menuGroup.setValue(m.getTitulo());
        menuGroup.setIcon(prefixImages + m.getIcon());
        menuGroup.setDirection("right-down");
        log.info("--menuGroup:" + menuGroup.getValue());

        parentMenu.getChildren().add(menuGroup);

        numberSeparators = (parentMenu.getChildCount() - 1) / 2;
        if ((parentMenu.getChildCount() - numberSeparators) < numParentChilds) {
          parentMenu.getChildren().add(new HtmlMenuSeparator());
        }

        List<Menu> newSubSystemMenus = subSystemMenus.subList(i + 1,
            subSystemMenus.size());
        getSubMenusRich(menuGroup, m.getId(), m.getNumChilds(),
            newSubSystemMenus);

      }
    }
  }

}
