//$Id$
/*
 * Copyright (c) 2003 - 2009 by CincoSOFT Ltda. (cincosoft@cincosoft.com)
 * Colombia, South America.
 *
 * This code is provided free "AS IS", without a warranty of any kind.
 * ALL EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED.
 *
 * This software is in process of being published under de GPL License during 2009.
 * Meanwhile, CincoSOFT maintains all the rights over this software.
 *
 */
package com.cincosoft.project.security.changePassword;

import javax.ejb.Local;

@Local
public interface ChangePassword {

  public void changePassword();

  public boolean isChanged();

  public String getClearPassword();

  public void setClearPassword(String clearPassword);

  public String getVerify();

  public void setVerify(String verify);

  public void destroy();
}