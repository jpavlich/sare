//$Id$
/*
 * Copyright (c) 2003 - 2009 by CincoSOFT Ltda. (cincosoft@cincosoft.com)
 * Colombia, South America.
 *
 * This code is provided free "AS IS", without a warranty of any kind.
 * ALL EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED.
 *
 * This software is in process of being published under de GPL License during 2009.
 * Meanwhile, CincoSOFT maintains all the rights over this software.
 *
 */
package com.cincosoft.project.security.parameterList;

import java.util.Arrays;
import java.util.List;

import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.framework.EntityQuery;

import com.cincosoft.project.Module;
import com.cincosoft.project.Parameter;

@Name("parameterList")
@Restrict("#{s:hasRole('parameterList')}")
public class ParameterList extends EntityQuery<Parameter> {

  private static final String[] RESTRICTIONS = {
      "lower(parameter.name) like concat(lower(#{parameterList.parameter.name}),'%')",
      "lower(parameter.value) like concat(lower(#{parameterList.parameter.value}),'%')",
      "lower(parameter.description) like concat(lower(#{parameterList.parameter.description}),'%')", };
  
  private static final String EJBQL = "select parameter from Parameter parameter";

  private Parameter parameter = new Parameter();
  
  public ParameterList() {
      setEjbql(EJBQL);
      setRestrictionExpressionStrings(Arrays.asList(RESTRICTIONS));
      setMaxResults(25);
  }


  public Parameter getParameter() {
    return parameter;
  }


}
