//$Id$
/*
 * Copyright (c) 2003 - 2009 by CincoSOFT Ltda. (cincosoft@cincosoft.com)
 * Colombia, South America.
 *
 * This code is provided free "AS IS", without a warranty of any kind.
 * ALL EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED.
 *
 * This software is in process of being published under de GPL License during 2009.
 * Meanwhile, CincoSOFT maintains all the rights over this software.
 *
 */
package com.cincosoft.project.security.profileGestion;

import static org.jboss.seam.ScopeType.SESSION;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;
import javax.ejb.Remove;
import javax.ejb.SessionContext;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.log4j.MDC;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.datamodel.DataModel;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.core.Events;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.log.Log;

import com.cincosoft.project.Module;
import com.cincosoft.project.Profile;
import com.cincosoft.project.Service;
import com.cincosoft.project.Usecase;
import com.cincosoft.project.Userprofile;
import com.cincosoft.project.security.serviceList.ServiceHome;
import com.cincosoft.project.security.usecaseList.UsecaseHome;

@Stateful
@Scope(SESSION)
@Name("profileGestion")
@Restrict("#{s:hasRole('profileGestion')}")
public class ProfileGestionAction implements ProfileGestion {

  @PersistenceContext
  private EntityManager em;

  @Resource
  SessionContext ctx;

  @In
  private FacesMessages facesMessages;

  @In
  private Events events;

  @Logger
  private Log log;

  private boolean showProfile = false;
  private boolean profileSelected = false;

  @DataModel
  private List<Profile> profiles;

  @Out(required = false)
  private Profile profile;

  @Out(required = false)
  private List<Module> modules;

  private List<Service> profileServices;

  @In(create = true)
  private Map<String, String> messages;

  @In(required = false)
  @Out(required = false)
  private UsecaseHome usecaseHome;

  @In(required = false)
  @Out(required = false)
  private ServiceHome serviceHome;



  public boolean isShowProfile() {
    return showProfile;
  }


  public void getProfiles() {
    log.info("getProfiles: getting profiles");

    profiles = em.createNamedQuery("Profile.findProfiles").getResultList();

    if (modules == null || profileSelected) {
      modules = em.createNamedQuery("Module.findModules").getResultList();

      // get use cases and services of each module
      for (Module m : modules) {
        List<Usecase> usecases = new ArrayList<Usecase>(m.getUsecases());

        for (Usecase u : usecases) {
          List<Service> services = new ArrayList<Service>(u.getServices());

          // put enter service as first in the usecase:
          Service enter = null;
          boolean found = false;
          int iFound = -1;

          for (int k = 0; k < services.size() && !found; k++) {
            enter = (Service) services.get(k);
            if (enter.getDisplay().equals("enter")) {
              found = true;
              iFound = k;
            }
          }
          if (found) {
            services.remove(iFound);
            services.add(0, enter);
          }

        }
      }
    }

    // debugModules();
  }


  @Restrict("#{s:hasRole('profileGestion_selectProfile')}")
  public void selectProfile(Profile selectedProfile) {
    log.info("selectProfile");

    profileSelected = true;

    getProfiles(); // refresh for new services

    profile = em.find(Profile.class, selectedProfile.getId());
    profileServices = profile.getServices();

    // for avoid lazy problems in reportProfilesUsers usecase; force evaluation
    // of userProfiles of the profile
    Set<Userprofile> up = profile.getUserprofiles();
    int numUserprofiles = up.size();

    // mark as active each service of the selected profile:
    for (Module m : modules) {
      List<Usecase> usecases = new ArrayList<Usecase>(m.getUsecases());

      for (Usecase u : usecases) {
        List<Service> services = new ArrayList<Service>(u.getServices());

        for (Service s : services) {
          s.setDisplayBis(messages.get(s.getDisplay()));
          s.setActiveInProfile(false);
          if (foundServiceInProfile(s)) {
            s.setActiveInProfile(true);
          }
        }
      }
    }

    profileSelected = false;
    showProfile = true;

  }

  @Restrict("#{s:hasRole('profileGestion_update')}")
  public void update() {
    log.info("update");

    // validate in each use case that enter service is active prior to activate other services :
    boolean allCorrect = true;

    for (Module m : modules) {
      List<Usecase> usecases = new ArrayList<Usecase>(m.getUsecases());

      for (Usecase u : usecases) {
        List<Service> services = new ArrayList<Service>(u.getServices());

        Service enterService = null;
        boolean found = false;
        int iFound = -1;
        for (int k = 0; k < services.size() && !found; k++) {
          enterService = (Service) services.get(k);
          if (enterService.getDisplay().equals("enter")) {
            found = true;
            iFound = k;
          }
        }

        if (!found) {
          FacesMessages.instance().add("error in usecase "  + u.getDisplay()
              + " : enter does not exist ");
          return;
        }

        boolean useCaseCorrect = true;
        for (int n = 0; n < services.size() && useCaseCorrect; n++) {
          Service s = (Service) services.get(n);
          if (s.isActiveInProfile()) {
            if (!enterService.isActiveInProfile()) {
              FacesMessages.instance().add(
                  "enter must be active prior to activate other services of use case "
                  + u.getDisplay());
              useCaseCorrect = false;
              allCorrect = false;
            }
          }
        }
      }
    }
    if (!allCorrect) {
      return;
    }

    // save in DB the profile (master)
    if (profile.getId() == null) { // new profile
      em.persist(profile);
    }
    else { // old profile
      List<Long> versions = em.createQuery(
          "select p.version from Profile p where p.id = :id")
          .setParameter("id", profile.getId()).getResultList();
      Long version = (versions.size() == 0) ? null : versions.get(0);

      // check version:
      log.info("version=" + version + "--profile.getVersion()="+ profile.getVersion());
      if (version == null || !version.equals(profile.getVersion())) {
        FacesMessages.instance().add(
                "update of profile is refused because another user modified or deleted it meanwhile; review and try again ");

        if (version != null) {
          selectProfile(profile);
        }
        else {
          showProfile = false;
        }

        ctx.setRollbackOnly();
        return;
      }

      profile.setVersion(version + 1);
      profile = em.merge(profile); // select master and details
    }

    // save in DB the details of the profile:
    profile.getServices().clear();

    // insert in current profile each active service set by the user :
    for (Module m : modules) {
      List<Usecase> usecases = new ArrayList<Usecase>(m.getUsecases());
      for (Usecase u : usecases) {
        List<Service> services = new ArrayList<Service>(u.getServices());
        for (Service s : services) {
          if (s.isActiveInProfile()) {
            profile.getServices().add(s);
          }
        }
      }
    }

    events.raiseEvent("profileEvent");
    showProfile = true;

  }

  @Restrict("#{s:hasRole('profileGestion_cancelUpdates')}")
  public void cancelUpdates() {
    log.info("cancelUpdates");

    if (profile.getId() == null) { // cancel new profile
      showProfile = false;
    }
    else { // cancel updates of old profile: get current profile from database
      selectProfile(profile);
    }
  }

  @Restrict("#{s:hasRole('profileGestion_newProfile')}")
  public void newProfile() {
    log.info("newProfile");
    profile = new Profile();

    // mark as inactive each service for the new profile:
    for (Module m : modules) {
      List<Usecase> usecases = new ArrayList<Usecase>(m.getUsecases());

      for (Usecase u : usecases) {
        List<Service> services = new ArrayList<Service>(u.getServices());
        for (Service s : services) {
          s.setDisplayBis(messages.get(s.getDisplay()));
          s.setActiveInProfile(false);
        }
      }
    }

    showProfile = true;
  }

  @Restrict("#{s:hasRole('profileGestion_deleteProfile')}")
  public void deleteProfile(Profile selectedProfile) {
    log.info("deleteProfile");

    try {
      profile = em.find(Profile.class, selectedProfile.getId());

      // delete all services of current profile (because of no cascade)
      profile.getServices().clear();

      // delete profile (master)
      em.remove(profile);

      profiles = em.createNamedQuery("Profile.findProfiles").getResultList();
      events.raiseEvent("profileEvent");  // for userGestion usecase
      showProfile = false;
    }
    catch (Exception e) {
      log.error("excepcion eliminando profile " + profile.getName(), e);
      facesMessages.add("Profile #{profile.name} cannot be deleted: probably it has associated users");
    }

  }

  @Restrict("#{s:hasRole('usecaseList_delete')}")
  public void removeUsecase() {
    log.info("removeUsecase");

    List<Service> servicesOfusecase = usecaseHome.getServices();

    try {
      // remove from profiles the services of this usecase :
      for (Service service : servicesOfusecase) {
        log.info("service " + service.getName() + " with id " + service.getId());

        // alternative with native query:
        /* em.createNativeQuery("delete from profile_service where service_id = ? ")
          .setParameter(1,service.getId()).executeUpdate();
        */

        List<Profile> profilesRelated = em.createNamedQuery(
            "Profile.findProfilesRelatedToService").setParameter("service", service).getResultList();

        for (Profile profile : profilesRelated) {
          List<Service> servicesOfProfile = profile.getServices();
          //debugServiceList("initial, servicesOfProfile=", servicesOfProfile);

          // FAILS !! servicesOfProfile.remove(service)
          for (Service s : servicesOfProfile) {
            if (s.getId() == service.getId()) {
              servicesOfProfile.remove(s);
              break;
            }
          }
          em.flush(); // necessary for an order of sql actions in the commit
        }
      }

      // remove this usecase (cascade => remove its services)
      usecaseHome.remove();
    }
    catch (Exception e) {
      log.error("exception deleting usecase " + usecaseHome.getInstance().getName(), e);
      facesMessages.add
        ("Usecase #{usecaseHome.instance.name} cannot be deleted because of relations with profiles");
    }
  }

  @Restrict("#{s:hasRole('serviceList_delete')}")
  public void removeService() {
    log.info("removeService");

    try {
       // remove from profiles the current service
      List<Profile> profilesRelated = em.createNamedQuery("Profile.findProfilesRelatedToService")
        .setParameter("service", serviceHome.getInstance()).getResultList();

      for (Profile profile : profilesRelated) {
        List<Service> servicesOfProfile = profile.getServices();

        for (Service s : servicesOfProfile) {
          if (s.getId() == serviceHome.getInstance().getId()) {
            servicesOfProfile.remove(s);
            break;
          }
        }
        em.flush();
      }

      // remove current service
      serviceHome.remove();
    }
    catch (Exception e) {
      log.error("exception deleting service " + serviceHome.getInstance().getName(), e);
      facesMessages.add("Service #{serviceHome.instance.name} cannot be deleted because of relations with profiles");
    }
  }


  @Remove
  public void destroy() {
  }


  private boolean foundServiceInProfile(Service service) {
    boolean found = false;

    for (int i = 0; i < profileServices.size() && !found; i++) {
      Long profileServiceId = ((Service) profileServices.get(i)).getId();
      if (profileServiceId.equals(service.getId())) {
        found = true;
      }
    }
    return found;
  }


  private void debugModules() {
    log.info("debugModules");

    for (Module m : modules) {
      log.info("Module=" + m.getName());

      List<Usecase> usecases = new ArrayList<Usecase>(m.getUsecases());
      for (Usecase u : usecases) {
        log.info("--Usecase=" + u.getDisplay());

        List<Service> services = new ArrayList<Service>(u.getServices());
        for (Service s : services) {
          log.info("----service=" + s.getDisplay());
        }
      }
    }
  }

  private void debugServiceList(String msg, List<Service> l) {
    log.info(msg + "--size=" + l.size());
    for (Service serv : l) {
      log.info("----service=" + serv.getName());
    }
  }
}
