//$Id$
/*
 * Copyright (c) 2003 - 2009 by CincoSOFT Ltda. (cincosoft@cincosoft.com)
 * Colombia, South America.
 *
 * This code is provided free "AS IS", without a warranty of any kind.
 * ALL EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED.
 *
 * This software is in process of being published under de GPL License during 2009.
 * Meanwhile, CincoSOFT maintains all the rights over this software.
 *
 */
package com.cincosoft.project.security.userGestion;

import java.util.List;

import javax.ejb.Local;
import javax.faces.model.SelectItem;

@Local
public interface UserGestion {

  public boolean isShowUser();

  public boolean isChangePass();

  public String getVerify();

  public void setVerify(String prueba);

  public String getClearPassword();

  public void setClearPassword(String clearPassword);

  public void getUsers();

  public void selectUser();

  public void changePassword();

  public void update();

  public void cancelUpdates();

  public void newUser();

  public void deleteUser();

  public Long getSystemProfile();

  public void setSystemProfile(Long s);

  public List<SelectItem> getSystemProfiles();

  public Long getUserProfile();

  public void setUserProfile(Long s);

  public List<SelectItem> getUserProfiles();

  public void addProfile();

  public void deleteProfile();

  public void destroy();
}