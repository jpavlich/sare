//$Id$
/*
 * Copyright (c) 2003 - 2009 by CincoSOFT Ltda. (cincosoft@cincosoft.com)
 * Colombia, South America.
 *
 * This code is provided free "AS IS", without a warranty of any kind.
 * ALL EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED.
 *
 * This software is in process of being published under de GPL License during 2009.
 * Meanwhile, CincoSOFT maintains all the rights over this software.
 *
 */
package com.cincosoft.project.security.reportUsersProfiles;

import static org.jboss.seam.ScopeType.SESSION;

import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.log4j.MDC;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Factory;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Observer;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.log.Log;

import com.cincosoft.project.User;

@Stateful
@Scope(SESSION)
@Name("reportUsersProfiles")
@Restrict("#{s:hasRole('reportUsersProfiles')}")
public class ReportUsersProfilesAction implements ReportUsersProfiles {

  @PersistenceContext
  private EntityManager em;

  @Logger
  private Log log;

  @Out(scope = ScopeType.APPLICATION)
  private List<User> repUsers;

  @Factory
  @Observer("changeUserProfile")
  public void getRepUsers() {
    log.info("getRepUsers");
    repUsers = em.createNamedQuery("User.findUsers").getResultList();

    // it is not necessary the evaluation of profiles associated to each user
    // because of fetch=FetchType.EAGER in User
  }

  @Remove
  public void destroy() {
  }

}
