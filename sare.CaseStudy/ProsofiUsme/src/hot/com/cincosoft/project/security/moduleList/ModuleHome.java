//$Id$
/*
 * Copyright (c) 2003 - 2009 by CincoSOFT Ltda. (cincosoft@cincosoft.com)
 * Colombia, South America.
 *
 * This code is provided free "AS IS", without a warranty of any kind.
 * ALL EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED.
 *
 * This software is in process of being published under de GPL License during 2009.
 * Meanwhile, CincoSOFT maintains all the rights over this software.
 *
 */
package com.cincosoft.project.security.moduleList;

import java.util.ArrayList;
import java.util.List;

import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.framework.EntityHome;

import com.cincosoft.project.Menu;
import com.cincosoft.project.Module;
import com.cincosoft.project.Usecase;

@Name("moduleHome")
@Restrict("#{s:hasRole('moduleList')}")
public class ModuleHome extends EntityHome<Module> {

  public void setModuleId(Long id) {
    setId(id);
  }

  public Long getModuleId() {
    return (Long) getId();
  }

  @Override
  protected Module createInstance() {
    Module module = new Module();
    return module;
  }

  public void wire() {
  }

  public boolean isWired() {
    return true;
  }

  public Module getDefinedInstance() {
    return isIdDefined() ? getInstance() : null;
  }

  public List<Menu> getMenus() {
    return getInstance() == null ? null : new ArrayList<Menu>(getInstance()
        .getMenus());
  }

  public List<Usecase> getUsecases() {
    return getInstance() == null ? null : new ArrayList<Usecase>(getInstance()
        .getUsecases());
  }

}
