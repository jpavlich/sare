//$Id$
/*
 * Copyright (c) 2003 - 2009 by CincoSOFT Ltda. (cincosoft@cincosoft.com)
 * Colombia, South America.
 *
 * This code is provided free "AS IS", without a warranty of any kind.
 * ALL EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED.
 *
 * This software is in process of being published under de GPL License during 2009.
 * Meanwhile, CincoSOFT maintains all the rights over this software.
 *
 */
package com.cincosoft.project.security.changePassword;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.log4j.MDC;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.log.Log;
//import org.jboss.security.Util;
import org.jboss.crypto.CryptoUtil;

import com.cincosoft.project.User;

@Stateful
@Scope(ScopeType.EVENT)
@Name("changePassword")
@Restrict("#{s:hasRole('changePassword')}")
public class ChangePasswordAction implements ChangePassword {

  @PersistenceContext
  private EntityManager em;

  @In
  private FacesMessages facesMessages;

  @Logger
  private Log log;

  @In @Out
  private User user;

  private String clearPassword;
  private String verify;
  private boolean changed;


  public boolean isChanged() {
    return changed;
  }

  public String getVerify() {
    return verify;
  }
  public void setVerify(String verify) {
    this.verify = verify;
  }

  public String getClearPassword() {
    return clearPassword;
  }
  public void setClearPassword(String clearPassword) {
    this.clearPassword = clearPassword;
  }

  @Restrict("#{s:hasRole('changePassword_changePassword')}")
  public void changePassword() {
    log.info("changePassword, value of changed=" + changed);

    changed = false;

    if (clearPassword.length() < 5 || clearPassword.length() > 15) {
      facesMessages.addToControl("password", "length must be between 5 and 15 ");
      revertUser();
      verify = null;
      return;
    }

    if (!clearPassword.equals(verify)) {
      facesMessages.addToControl("verify", "Re-enter your password");
      revertUser();
      verify = null;
      return;
    }

    //String hashedPassword = Util.createPasswordHash("MD5", Util.BASE64_ENCODING, null, null, clearPassword);
    String hashedPassword = CryptoUtil.createPasswordHash ("MD5",
            CryptoUtil.BASE64_ENCODING, null, null, clearPassword);
    
    user.setPassword(hashedPassword);  

    user = em.merge(user);
    changed = true;
    facesMessages.add("Your password has been changed");
  }


  @Remove
  public void destroy() {
  }

  private void revertUser() {
    user = em.find(User.class, user.getUsername());
  }

}
