//$Id$
/*
 * Copyright (c) 2003 - 2009 by CincoSOFT Ltda. (cincosoft@cincosoft.com)
 * Colombia, South America.
 *
 * This code is provided free "AS IS", without a warranty of any kind.
 * ALL EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED.
 *
 * This software is in process of being published under de GPL License during 2009.
 * Meanwhile, CincoSOFT maintains all the rights over this software.
 *
 */
package com.cincosoft.project.security.parameterList;

import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.framework.EntityHome;

import com.cincosoft.project.Parameter;

@Name("parameterHome")
@Restrict("#{s:hasRole('parameterList')}")
public class ParameterHome extends EntityHome<Parameter> {

  public void setParameterId(Long id) {
    setId(id);
  }

  public Long getParameterId() {
    return (Long) getId();
  }

  @Override
  protected Parameter createInstance() {
    Parameter parameter = new Parameter();
    return parameter;
  }

  public void wire() {
  }

  public boolean isWired() {
    return true;
  }

  public Parameter getDefinedInstance() {
    return isIdDefined() ? getInstance() : null;
  }

}
