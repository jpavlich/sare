//$Id$
/*
 * Copyright (c) 2003 - 2009 by CincoSOFT Ltda. (cincosoft@cincosoft.com)
 * Colombia, South America.
 *
 * This code is provided free "AS IS", without a warranty of any kind.
 * ALL EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED.
 *
 * This software is in process of being published under de GPL License during 2009.
 * Meanwhile, CincoSOFT maintains all the rights over this software.
 *
 */
package com.cincosoft.project.security.moduleList;

import java.util.Arrays;
import java.util.List;

import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.framework.EntityQuery;

import com.cincosoft.project.Module;

@Name("moduleList")
@Restrict("#{s:hasRole('moduleList')}")
public class ModuleList extends EntityQuery<Module> {

  private static final String[] RESTRICTIONS = { "lower(module.name) like concat(lower(#{moduleList.module.name}),'%')", };
  private static final String EJBQL = "select module from Module module";
  
  private Module module = new Module();
  
  public ModuleList() {
      setEjbql(EJBQL);
      setRestrictionExpressionStrings(Arrays.asList(RESTRICTIONS));
      setMaxResults(25);
  }

  public Module getModule() {
    return module;
  }


}
