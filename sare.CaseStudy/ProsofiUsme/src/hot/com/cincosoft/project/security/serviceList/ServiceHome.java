//$Id$
/*
 * Copyright (c) 2003 - 2009 by CincoSOFT Ltda. (cincosoft@cincosoft.com)
 * Colombia, South America.
 *
 * This code is provided free "AS IS", without a warranty of any kind.
 * ALL EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED.
 *
 * This software is in process of being published under de GPL License during 2009.
 * Meanwhile, CincoSOFT maintains all the rights over this software.
 *
 */
package com.cincosoft.project.security.serviceList;

import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.framework.EntityHome;

import com.cincosoft.project.Service;
import com.cincosoft.project.Usecase;
import com.cincosoft.project.security.usecaseList.UsecaseHome;

@Name("serviceHome")
@Restrict("#{s:hasRole('serviceList')}")
public class ServiceHome extends EntityHome<Service> {

  @In(create = true)
  UsecaseHome usecaseHome;

  public void setServiceId(Long id) {
    setId(id);
  }

  public Long getServiceId() {
    return (Long) getId();
  }

  @Override
  protected Service createInstance() {
    Service service = new Service();
    return service;
  }

  public void wire() {
    Usecase usecase = usecaseHome.getDefinedInstance();
    if (usecase != null) {
      getInstance().setUsecase(usecase);
    }
  }

  public boolean isWired() {
    if (getInstance().getUsecase() == null)
      return false;
    return true;
  }

  public Service getDefinedInstance() {
    return isIdDefined() ? getInstance() : null;
  }

}
