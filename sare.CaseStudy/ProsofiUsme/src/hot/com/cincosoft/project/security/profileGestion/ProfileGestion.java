//$Id$
/*
 * Copyright (c) 2003 - 2009 by CincoSOFT Ltda. (cincosoft@cincosoft.com)
 * Colombia, South America.
 *
 * This code is provided free "AS IS", without a warranty of any kind.
 * ALL EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED.
 *
 * This software is in process of being published under de GPL License during 2009.
 * Meanwhile, CincoSOFT maintains all the rights over this software.
 *
 */
package com.cincosoft.project.security.profileGestion;

import java.util.List;

import javax.ejb.Local;

import com.cincosoft.project.Profile;
import com.cincosoft.project.Service;

@Local
public interface ProfileGestion {

  public boolean isShowProfile();

  public void getProfiles();

  public void selectProfile(Profile selectedProfile);

  public void update();

  public void cancelUpdates();

  public void newProfile();

  public void deleteProfile(Profile selectedProfile);

  public void removeUsecase();
  
  public void removeService();

  public void destroy();
}