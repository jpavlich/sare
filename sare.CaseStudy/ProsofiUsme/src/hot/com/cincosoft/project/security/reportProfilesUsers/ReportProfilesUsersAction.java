//$Id$
/*
 * Copyright (c) 2003 - 2009 by CincoSOFT Ltda. (cincosoft@cincosoft.com)
 * Colombia, South America.
 *
 * This code is provided free "AS IS", without a warranty of any kind.
 * ALL EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED.
 *
 * This software is in process of being published under de GPL License during 2009.
 * Meanwhile, CincoSOFT maintains all the rights over this software.
 *
 */
package com.cincosoft.project.security.reportProfilesUsers;

import static org.jboss.seam.ScopeType.SESSION;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.log4j.MDC;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Factory;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Observer;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.log.Log;

import com.cincosoft.project.Profile;
import com.cincosoft.project.Userprofile;

@Stateful
@Scope(SESSION)
@Name("reportProfilesUsers")
@Restrict("#{s:hasRole('reportProfilesUsers')}")
public class ReportProfilesUsersAction implements ReportProfilesUsers {

  @PersistenceContext
  private EntityManager em;

  @Logger
  private Log log;

  @Out(scope = ScopeType.APPLICATION)
  private List<Profile> repProfiles;

  @Factory
  @Observer("changeUserProfile")
  public void getRepProfiles() {
    log.info("getRepProfiles");

    repProfiles = em.createNamedQuery("Profile.findProfiles").getResultList();

    // evaluation of user profiles associated to each profile:
    for (Profile p : repProfiles) {
      Set<Userprofile> userprofiles = p.getUserprofiles();
      int numUserprofiles = userprofiles.size();
      log.info("profile = " + p.getName() + "--size of its userprofiles="
          + numUserprofiles);
    }
  }

  @Remove
  public void destroy() {
  }

}
