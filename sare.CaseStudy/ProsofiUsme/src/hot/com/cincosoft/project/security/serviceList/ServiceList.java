//$Id$
/*
 * Copyright (c) 2003 - 2009 by CincoSOFT Ltda. (cincosoft@cincosoft.com)
 * Colombia, South America.
 *
 * This code is provided free "AS IS", without a warranty of any kind.
 * ALL EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED.
 *
 * This software is in process of being published under de GPL License during 2009.
 * Meanwhile, CincoSOFT maintains all the rights over this software.
 *
 */
package com.cincosoft.project.security.serviceList;

import java.util.Arrays;
import java.util.List;

import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.framework.EntityQuery;

import com.cincosoft.project.Module;
import com.cincosoft.project.Service;

@Name("serviceList")
@Restrict("#{s:hasRole('serviceList')}")
public class ServiceList extends EntityQuery<Service> {

  private static final String[] RESTRICTIONS = {
      "lower(service.name) like concat(lower(#{serviceList.service.name}),'%')",
      "lower(service.display) like concat(lower(#{serviceList.service.display}),'%')", };

  private static final String EJBQL = "select service from Service service";
  
  private Service service = new Service();

  public ServiceList() {
      setEjbql(EJBQL);
      setRestrictionExpressionStrings(Arrays.asList(RESTRICTIONS));
      setMaxResults(25);
  }


  public Service getService() {
    return service;
  }


}
