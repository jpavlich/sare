//$Id$
/*
 * Copyright (c) 2003 - 2009 by CincoSOFT Ltda. (cincosoft@cincosoft.com)
 * Colombia, South America.
 *
 * This code is provided free "AS IS", without a warranty of any kind.
 * ALL EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED.
 *
 * This software is in process of being published under de GPL License during 2009.
 * Meanwhile, CincoSOFT maintains all the rights over this software.
 *
 */
package com.cincosoft.project.security.menuList;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.faces.model.SelectItem;

import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.framework.EntityQuery;
import org.jboss.seam.log.Log;

import com.cincosoft.project.Menu;

@Name("menuList")
@Restrict("#{s:hasRole('menuList')}")
public class MenuList extends EntityQuery<Menu> {

  // 5S: ADDED----------
  @Logger
  private Log log;

  @In(required = false)
  @Out(required = false)
  private MenuHome menuHome;
  // -----------------------

  private static final String EJBQL = "select menu from Menu menu";
  
  // 5S: new restriction"get only active menus  
  private static final String[] RESTRICTIONS = {
      "lower(menu.titulo) like concat(lower(#{menuList.menu.titulo}),'%')",
      "lower(menu.icon) like concat(lower(#{menuList.menu.icon}),'%')",
      "lower(menu.action) like concat(lower(#{menuList.menu.action}),'%')",
      "menu.status = #{menuList.menu.status}", };

  private Menu menu = new Menu();
  
  public MenuList() {
      setEjbql(EJBQL);
      setRestrictionExpressionStrings(Arrays.asList(RESTRICTIONS));
      setMaxResults(25);
  }
  
  public Menu getMenu() {
      return menu;
  }  


  // 5S: ADDED
  // -------------------------------------------------------------------

  /** items of Select: <f:selectItems value="#{menuList.listParentMenus}"/> */
  public List<SelectItem> getlistParentMenus() {
    log.info("getlistParentMenus");

    List<SelectItem> selectItems = new ArrayList<SelectItem>();
    List<Menu> menuList = (List<Menu>) getResultList();
    for (Menu m : menuList) {
      if (m.getTitulo() != null && !m.getTitulo().equals("")) {
        selectItems.add(new SelectItem(m.getId(), m.getTitulo())); // value,
                                                                    // display
      }
    }
    return selectItems;
  }

  /**
   * initial selected item (its value) in the Select:
   * value="#{menuList.parentMenu}"
   */
  public Long getParentMenu() {
    log.info("getParentMenu");
    Long idParent = menuHome.getInstance().getParent() != null ? menuHome
        .getInstance().getParent().getId() : null;
    return idParent;
  }

  /** selected item by the user (its value): value=value="#{menuList.parentMenu}" */
  public void setParentMenu(Long m) {
    log.info("selected parent menu: " + m);
    Menu parent = getEntityManager().find(Menu.class, m);
    menuHome.getInstance().setParent(parent);

  }

  /** action for submit of select parent */
  public void selectParent() {
    log.info("selectParent ");
  }

}
