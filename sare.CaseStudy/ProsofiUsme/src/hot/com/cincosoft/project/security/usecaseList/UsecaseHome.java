//$Id$
/*
 * Copyright (c) 2003 - 2009 by CincoSOFT Ltda. (cincosoft@cincosoft.com)
 * Colombia, South America.
 *
 * This code is provided free "AS IS", without a warranty of any kind.
 * ALL EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED.
 *
 * This software is in process of being published under de GPL License during 2009.
 * Meanwhile, CincoSOFT maintains all the rights over this software.
 *
 */
package com.cincosoft.project.security.usecaseList;

import java.util.ArrayList;
import java.util.List;

import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.framework.EntityHome;

import com.cincosoft.project.Menu;
import com.cincosoft.project.Module;
import com.cincosoft.project.Service;
import com.cincosoft.project.Usecase;
import com.cincosoft.project.security.moduleList.ModuleHome;

@Name("usecaseHome")
@Restrict("#{s:hasRole('usecaseList')}")
public class UsecaseHome extends EntityHome<Usecase> {

  @In(create = true)
  ModuleHome moduleHome;

  public void setUsecaseId(Long id) {
    setId(id);
  }

  public Long getUsecaseId() {
    return (Long) getId();
  }

  @Override
  protected Usecase createInstance() {
    Usecase usecase = new Usecase();
    return usecase;
  }

  public void wire() {
    Module module = moduleHome.getDefinedInstance();
    if (module != null) {
      getInstance().setModule(module);
    }
  }

  public boolean isWired() {
    if (getInstance().getModule() == null)
      return false;
    return true;
  }

  public Usecase getDefinedInstance() {
    return isIdDefined() ? getInstance() : null;
  }

  public List<Service> getServices() {
    return getInstance() == null ? null : new ArrayList<Service>(getInstance()
        .getServices());
  }

  public List<Menu> getMenus() {
    return getInstance() == null ? null : new ArrayList<Menu>(getInstance()
        .getMenus());
  }

}
