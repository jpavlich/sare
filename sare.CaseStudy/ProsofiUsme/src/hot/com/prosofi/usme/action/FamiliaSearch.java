package com.prosofi.usme.action;

/**
   * Este codigo fue generado por sare.m2t.generator 
   * http://code.google.com/p/sare/
   * 
   */

import javax.ejb.Local;

import com.prosofi.usme.model.Familia;

import java.util.*;

@Local
public interface FamiliaSearch {

   public void find( );
   public void queryFamilia( );
   public List<Familia> getFamiliasC();
   public void setFamiliasC(List <Familia> familiasC);
   public void destroy( );
}
