package com.prosofi.usme.action;

/**
   * Este codigo fue generado por sare.m2t.generator 
   * http://code.google.com/p/sare/
   * 
   */

import javax.ejb.Local;

import com.prosofi.usme.model.DatosPsicoSocial;
import com.prosofi.usme.model.Encuestador;
import com.prosofi.usme.model.Familia;

import java.util.*;

@Local
public interface FamiliaInscription {

   public void cancel( );
   public void destroy( );
   public void confirm( );
   public void delete( );
   public void newFamilia( );
   public void selectFamilia(Familia selectedFamilia );
   public void editFamilia( );
   public void selectEncuestador(Encuestador selectedEncuestador);
   public void insertInVivienda(Familia selectedFamilia);
   public List<Familia> getFamiliaInViviendaByID();
   public void selectDatosPsicoSocial(DatosPsicoSocial selectedDatosPsicoSocial);
}
