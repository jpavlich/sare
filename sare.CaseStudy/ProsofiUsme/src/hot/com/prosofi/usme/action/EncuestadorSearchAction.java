package com.prosofi.usme.action;


/**
   * Este codigo fue generado por sare.m2t.generator 
   * http://code.google.com/p/sare/
   * 
   */

import java.util.List;
import java.util.ArrayList;
import javax.ejb.*;
import javax.persistence.*;
import org.jboss.seam.annotations.*;
import org.jboss.seam.annotations.datamodel.DataModel;
import org.jboss.seam.annotations.datamodel.DataModelSelection;
import org.jboss.seam.annotations.faces.*;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.annotations.security.Restrict;

import com.prosofi.usme.model.Encuestador;


@Stateful
@Name ("EncuestadorSearch")
public class EncuestadorSearchAction implements EncuestadorSearch {

    @PersistenceContext
    private EntityManager em;

    @In
    private FacesMessages facesMessages;
    

    @DataModel
    private List<Encuestador> encuestadorsC=new ArrayList<Encuestador>();
    
    @Restrict("#{s:hasRole('Inscription_find','Search_find')}") 
    public void find( ){
        queryEncuestador();
    }
    @Restrict("#{s:hasRole('Inscription_queryEncuestador','Search_queryEncuestador')}") 
    public void queryEncuestador( ){
        List<Encuestador> results = em.createQuery("select h from Encuestador h").getResultList();
        encuestadorsC = results;
    }
    @Restrict("#{s:hasRole('Inscription_getEncuestadorsC','Search_getEncuestadorsC')}") 
    public  List<Encuestador>  getEncuestadorsC(){
        List<Encuestador> results = em.createQuery("select h from Encuestador h").getResultList();  
        encuestadorsC = results;
        return encuestadorsC;
    }
    @Restrict("#{s:hasRole('Inscription_setEncuestadorsC','Search_setEncuestadorsC')}") 
    public void setEncuestadorsC(List <Encuestador> encuestadorsC){
        this.encuestadorsC = encuestadorsC;
    }
    @Restrict("#{s:hasRole('Inscription_destroy','Search_destroy')}") 
    @Remove
    public void destroy( ){

    }
}
