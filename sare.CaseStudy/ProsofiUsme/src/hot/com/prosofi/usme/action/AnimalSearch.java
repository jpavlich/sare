package com.prosofi.usme.action;

/**
   * Este codigo fue generado por sare.m2t.generator 
   * http://code.google.com/p/sare/
   * 
   */

import javax.ejb.Local;

import com.prosofi.usme.model.Animal;

import java.util.*;

@Local
public interface AnimalSearch {

   public void find( );
   public void queryAnimal( );
   public List<Animal> getAnimalsC();
   public void setAnimalsC(List <Animal> animalsC);
   public void destroy( );
}
