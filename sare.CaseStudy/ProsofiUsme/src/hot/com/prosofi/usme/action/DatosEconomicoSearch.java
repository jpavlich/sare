package com.prosofi.usme.action;

/**
   * Este codigo fue generado por sare.m2t.generator 
   * http://code.google.com/p/sare/
   * 
   */

import javax.ejb.Local;

import com.prosofi.usme.model.DatosEconomico;

import java.util.*;

@Local
public interface DatosEconomicoSearch {

   public void find( );
   public void queryDatosEconomico( );
   public List<DatosEconomico> getDatosEconomicosC();
   public void setDatosEconomicosC(List <DatosEconomico> datosEconomicosC);
   public void destroy( );
}
