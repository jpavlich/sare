package com.prosofi.usme.action;


/**
   * Este codigo fue generado por sare.m2t.generator 
   * http://code.google.com/p/sare/
   * 
   */

import java.util.List;
import java.util.ArrayList;
import javax.ejb.*;
import javax.persistence.*;
import org.jboss.seam.annotations.*;
import org.jboss.seam.annotations.datamodel.DataModel;
import org.jboss.seam.annotations.datamodel.DataModelSelection;
import org.jboss.seam.annotations.faces.*;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.annotations.security.Restrict;

import com.prosofi.usme.model.Animal;



@Stateful
@Name ("AnimalSearch")
public class AnimalSearchAction implements AnimalSearch {

    @PersistenceContext
    private EntityManager em;

    @In
    private FacesMessages facesMessages;
    

    @DataModel
    private List<Animal> animalsC=new ArrayList<Animal>();
    
    @Restrict("#{s:hasRole('Inscription_find','Search_find')}") 
    public void find( ){
        queryAnimal();
    }
    @Restrict("#{s:hasRole('Inscription_queryAnimal','Search_queryAnimal')}") 
    public void queryAnimal( ){
        List<Animal> results = em.createQuery("select h from Animal h").getResultList();
        animalsC = results;
    }
    @Restrict("#{s:hasRole('Inscription_getAnimalsC','Search_getAnimalsC')}") 
    public  List<Animal>  getAnimalsC(){
        List<Animal> results = em.createQuery("select h from Animal h").getResultList();  
        animalsC = results;
        return animalsC;
    }
    @Restrict("#{s:hasRole('Inscription_setAnimalsC','Search_setAnimalsC')}") 
    public void setAnimalsC(List <Animal> animalsC){
        this.animalsC = animalsC;
    }
    @Restrict("#{s:hasRole('Inscription_destroy','Search_destroy')}") 
    @Remove
    public void destroy( ){

    }
}
