package com.prosofi.usme.action;

/**
   * Este codigo fue generado por sare.m2t.generator 
   * http://code.google.com/p/sare/
   * 
   */

import javax.ejb.Local;

import com.prosofi.usme.model.Animal;

import java.util.*;

@Local
public interface AnimalInscription {

   public void cancel( );
   public void destroy( );
   public void confirm( );
   public void delete( );
   public void newAnimal( );
   public void selectAnimal( Animal selectedAnimal);
   public void editAnimal( );
   public void insertInFamilia(Animal selectedAnimal);
   public List<Animal> getAnimalInFamiliaByID();
}
