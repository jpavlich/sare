package com.prosofi.usme.action;


/**
   * Este codigo fue generado por sare.m2t.generator 
   * http://code.google.com/p/sare/
   * 
   */

import java.util.List;
import java.util.ArrayList;
import javax.ejb.*;
import javax.persistence.*;
import org.jboss.seam.annotations.*;
import org.jboss.seam.annotations.datamodel.DataModel;
import org.jboss.seam.annotations.datamodel.DataModelSelection;
import org.jboss.seam.annotations.faces.*;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.annotations.security.Restrict;

import com.prosofi.usme.model.ServiciosVivienda;
import com.prosofi.usme.model.Vivienda;

@Stateful
@Name ("ServiciosViviendaInscription")
public class ServiciosViviendaInscriptionAction implements ServiciosViviendaInscription {

    @PersistenceContext
    private EntityManager em;

    @In
    private FacesMessages facesMessages;
    

    @In(required=false)
    @Out(required=false)
    private Vivienda vivienda;

    @DataModel
    private List<ServiciosVivienda> viviendaByID=new ArrayList<ServiciosVivienda>();

    @In(required=false)
    @Out(required=false)
    private ServiciosVivienda serviciosVivienda;
    
    @Restrict("#{s:hasRole('Inscription_cancel','Search_cancel')}") 
    @End
    public void cancel( ){

    }
    @Restrict("#{s:hasRole('Inscription_destroy','Search_destroy')}") 
    @Remove
    public void destroy( ){

    }
    @Restrict("#{s:hasRole('Inscription_confirm','Search_confirm')}") 
    @End
    public void confirm( ){
        em.persist(serviciosVivienda);
        facesMessages.add("serviciosVivienda added");
    }
    @Restrict("#{s:hasRole('Inscription_delete')}") 
    public void delete( ){
        serviciosVivienda=em.merge(serviciosVivienda);
        em.remove(serviciosVivienda);  
        facesMessages.add("serviciosVivienda deleted");
    }
    @Restrict("#{s:hasRole('Inscription_newServiciosVivienda')}") 
    @Begin
    public void newServiciosVivienda( ){
        serviciosVivienda=new ServiciosVivienda();
    }
    @Restrict("#{s:hasRole('Inscription_selectServiciosVivienda','Search_selectServiciosVivienda')}") 
    @Begin(join=true)
    public void selectServiciosVivienda(ServiciosVivienda selectedServiciosVivienda ){
        serviciosVivienda=em.find(ServiciosVivienda.class, selectedServiciosVivienda.getId());
    }
    @Restrict("#{s:hasRole('Inscription_insertInVivienda')}") 
    public void insertInVivienda(ServiciosVivienda selectedServiciosVivienda){
        serviciosVivienda.setVivienda(vivienda);
        //em.persist(serviciosVivienda);
        serviciosVivienda =em.merge(selectedServiciosVivienda);
        facesMessages.add("serviciosVivienda added");
    }
    @Restrict("#{s:hasRole('Inscription_getServiciosViviendaInViviendaByID','Search_getServiciosViviendaInViviendaByID')}") 
    public  List<ServiciosVivienda>  getServiciosViviendaInViviendaByID(){
        List<ServiciosVivienda> results = em.createQuery("select h from ServiciosVivienda h where h.vivienda.id = ?1").setParameter(1,vivienda.getId()).getResultList();  
        viviendaByID = results;
        return viviendaByID;
    }
    @Restrict("#{s:hasRole('Inscription_editServiciosVivienda')}") 
    @End
    public void editServiciosVivienda( ){
        serviciosVivienda=em.merge(serviciosVivienda); 
        facesMessages.add("serviciosVivienda edited");
    }
}
