package com.prosofi.usme.action;

/**
   * Este codigo fue generado por sare.m2t.generator 
   * http://code.google.com/p/sare/
   * 
   */

import javax.ejb.Local;

import com.prosofi.usme.model.ControlPrenatal;

import java.util.*;

@Local
public interface ControlPrenatalSearch {

   public void find( );
   public void queryControlPrenatal( );
   public List<ControlPrenatal> getControlPrenatalsC();
   public void setControlPrenatalsC(List <ControlPrenatal> controlPrenatalsC);
   public void destroy( );
}
