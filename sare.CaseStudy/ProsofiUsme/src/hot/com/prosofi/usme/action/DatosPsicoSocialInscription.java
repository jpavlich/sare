package com.prosofi.usme.action;

/**
   * Este codigo fue generado por sare.m2t.generator 
   * http://code.google.com/p/sare/
   * 
   */

import javax.ejb.Local;

import com.prosofi.usme.model.DatosPsicoSocial;
import com.prosofi.usme.model.Familia;

import java.util.*;

@Local
public interface DatosPsicoSocialInscription {

   public void newDatosPsicoSocial( );
   public void cancel( );
   public void destroy( );
   public void selectDatosPsicoSocial(DatosPsicoSocial selectedDatosPsicoSocial );
   public void selectFamilia(Familia selectedFamilia);
   public void confirm( );
   public void editDatosPsicoSocial( );
   public void delete( );
}
