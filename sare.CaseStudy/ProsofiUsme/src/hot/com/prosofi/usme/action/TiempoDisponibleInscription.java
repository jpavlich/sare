package com.prosofi.usme.action;

/**
   * Este codigo fue generado por sare.m2t.generator 
   * http://code.google.com/p/sare/
   * 
   */

import javax.ejb.Local;

import com.prosofi.usme.model.Persona;
import com.prosofi.usme.model.TiempoDisponible;

import java.util.*;

@Local
public interface TiempoDisponibleInscription {

   public void cancel( );
   public void destroy( );
   public void confirm( );
   public void delete( );
   public void newTiempoDisponible( );
   public void selectTiempoDisponible(TiempoDisponible selectedTiempoDisponible );
   public void insertInPersona(TiempoDisponible selectedTiempoDisponible);
   public List<TiempoDisponible> getTiempoDisponibleInPersonaByID();
   public void editTiempoDisponible( );
   public void selectPersona(Persona selectedPersona);
}
