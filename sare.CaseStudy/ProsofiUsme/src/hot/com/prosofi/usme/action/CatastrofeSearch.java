package com.prosofi.usme.action;

/**
   * Este codigo fue generado por sare.m2t.generator 
   * http://code.google.com/p/sare/
   * 
   */

import javax.ejb.Local;

import com.prosofi.usme.model.Catastrofe;

import java.util.*;

@Local
public interface CatastrofeSearch {

   public void find( );
   public void queryCatastrofe( );
   public List<Catastrofe> getCatastrofesC();
   public void setCatastrofesC(List <Catastrofe> catastrofesC);
   public void destroy( );
}
