package com.prosofi.usme.action;


/**
   * Este codigo fue generado por sare.m2t.generator 
   * http://code.google.com/p/sare/
   * 
   */

import java.util.List;
import java.util.ArrayList;
import javax.ejb.*;
import javax.persistence.*;
import org.jboss.seam.annotations.*;
import org.jboss.seam.annotations.datamodel.DataModel;
import org.jboss.seam.annotations.datamodel.DataModelSelection;
import org.jboss.seam.annotations.faces.*;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.annotations.security.Restrict;

import com.prosofi.usme.model.Enfermedad;


@Stateful
@Name ("EnfermedadSearch")
public class EnfermedadSearchAction implements EnfermedadSearch {

    @PersistenceContext
    private EntityManager em;

    @In
    private FacesMessages facesMessages;
    

    @DataModel
    private List<Enfermedad> enfermedadsC=new ArrayList<Enfermedad>();
    
    @Restrict("#{s:hasRole('Inscription_find','Search_find')}") 
    public void find( ){
        queryEnfermedad();
    }
    @Restrict("#{s:hasRole('Inscription_queryEnfermedad','Search_queryEnfermedad')}") 
    public void queryEnfermedad( ){
        List<Enfermedad> results = em.createQuery("select h from Enfermedad h").getResultList();
        enfermedadsC = results;
    }
    @Restrict("#{s:hasRole('Inscription_getEnfermedadsC','Search_getEnfermedadsC')}") 
    public  List<Enfermedad>  getEnfermedadsC(){
        List<Enfermedad> results = em.createQuery("select h from Enfermedad h").getResultList();  
        enfermedadsC = results;
        return enfermedadsC;
    }
    @Restrict("#{s:hasRole('Inscription_setEnfermedadsC','Search_setEnfermedadsC')}") 
    public void setEnfermedadsC(List <Enfermedad> enfermedadsC){
        this.enfermedadsC = enfermedadsC;
    }
    @Restrict("#{s:hasRole('Inscription_destroy','Search_destroy')}") 
    @Remove
    public void destroy( ){

    }
}
