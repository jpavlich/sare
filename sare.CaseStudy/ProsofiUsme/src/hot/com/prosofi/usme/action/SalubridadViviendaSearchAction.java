package com.prosofi.usme.action;


/**
   * Este codigo fue generado por sare.m2t.generator 
   * http://code.google.com/p/sare/
   * 
   */

import java.util.List;
import java.util.ArrayList;
import javax.ejb.*;
import javax.persistence.*;
import org.jboss.seam.annotations.*;
import org.jboss.seam.annotations.datamodel.DataModel;
import org.jboss.seam.annotations.datamodel.DataModelSelection;
import org.jboss.seam.annotations.faces.*;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.annotations.security.Restrict;

import com.prosofi.usme.model.SalubridadVivienda;


@Stateful
@Name ("SalubridadViviendaSearch")
public class SalubridadViviendaSearchAction implements SalubridadViviendaSearch {

    @PersistenceContext
    private EntityManager em;

    @In
    private FacesMessages facesMessages;
    

    @DataModel
    private List<SalubridadVivienda> salubridadViviendasC=new ArrayList<SalubridadVivienda>();
    
    @Restrict("#{s:hasRole('Inscription_find','Search_find')}") 
    public void find( ){
        querySalubridadVivienda();
    }
    @Restrict("#{s:hasRole('Inscription_querySalubridadVivienda','Search_querySalubridadVivienda')}") 
    public void querySalubridadVivienda( ){
        List<SalubridadVivienda> results = em.createQuery("select h from SalubridadVivienda h").getResultList();
        salubridadViviendasC = results;
    }
    @Restrict("#{s:hasRole('Inscription_getSalubridadViviendasC','Search_getSalubridadViviendasC')}") 
    public  List<SalubridadVivienda>  getSalubridadViviendasC(){
        List<SalubridadVivienda> results = em.createQuery("select h from SalubridadVivienda h").getResultList();  
        salubridadViviendasC = results;
        return salubridadViviendasC;
    }
    @Restrict("#{s:hasRole('Inscription_setSalubridadViviendasC','Search_setSalubridadViviendasC')}") 
    public void setSalubridadViviendasC(List <SalubridadVivienda> salubridadViviendasC){
        this.salubridadViviendasC = salubridadViviendasC;
    }
    @Restrict("#{s:hasRole('Inscription_destroy','Search_destroy')}") 
    @Remove
    public void destroy( ){

    }
}
