package com.prosofi.usme.action;


/**
   * Este codigo fue generado por sare.m2t.generator 
   * http://code.google.com/p/sare/
   * 
   */

import java.util.List;
import java.util.ArrayList;
import javax.ejb.*;
import javax.persistence.*;
import org.jboss.seam.annotations.*;
import org.jboss.seam.annotations.datamodel.DataModel;
import org.jboss.seam.annotations.datamodel.DataModelSelection;
import org.jboss.seam.annotations.faces.*;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.annotations.security.Restrict;

import com.prosofi.usme.model.Necesidad;



@Stateful
@Name ("NecesidadSearch")
public class NecesidadSearchAction implements NecesidadSearch {

    @PersistenceContext
    private EntityManager em;

    @In
    private FacesMessages facesMessages;
    

    @DataModel
    private List<Necesidad> necesidadsC=new ArrayList<Necesidad>();
    
    @Restrict("#{s:hasRole('Inscription_find','Search_find')}") 
    public void find( ){
        queryNecesidad();
    }
    @Restrict("#{s:hasRole('Inscription_queryNecesidad','Search_queryNecesidad')}") 
    public void queryNecesidad( ){
        List<Necesidad> results = em.createQuery("select h from Necesidad h").getResultList();
        necesidadsC = results;
    }
    @Restrict("#{s:hasRole('Inscription_getNecesidadsC','Search_getNecesidadsC')}") 
    public  List<Necesidad>  getNecesidadsC(){
        List<Necesidad> results = em.createQuery("select h from Necesidad h").getResultList();  
        necesidadsC = results;
        return necesidadsC;
    }
    @Restrict("#{s:hasRole('Inscription_setNecesidadsC','Search_setNecesidadsC')}") 
    public void setNecesidadsC(List <Necesidad> necesidadsC){
        this.necesidadsC = necesidadsC;
    }
    @Restrict("#{s:hasRole('Inscription_destroy','Search_destroy')}") 
    @Remove
    public void destroy( ){

    }
}
