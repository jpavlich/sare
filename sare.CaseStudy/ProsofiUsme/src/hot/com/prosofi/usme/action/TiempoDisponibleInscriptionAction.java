package com.prosofi.usme.action;


/**
   * Este codigo fue generado por sare.m2t.generator 
   * http://code.google.com/p/sare/
   * 
   */

import java.util.List;
import java.util.ArrayList;
import javax.ejb.*;
import javax.persistence.*;
import org.jboss.seam.annotations.*;
import org.jboss.seam.annotations.datamodel.DataModel;
import org.jboss.seam.annotations.datamodel.DataModelSelection;
import org.jboss.seam.annotations.faces.*;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.annotations.security.Restrict;

import com.prosofi.usme.model.Persona;
import com.prosofi.usme.model.TiempoDisponible;

@Stateful
@Name ("TiempoDisponibleInscription")
public class TiempoDisponibleInscriptionAction implements TiempoDisponibleInscription {

    @PersistenceContext
    private EntityManager em;

    @In
    private FacesMessages facesMessages;
    

    @In(required=false)
    @Out(required=false)
    private Persona persona;

    @In(required=false)
    @Out(required=false)
    private TiempoDisponible tiempoDisponible;

    @DataModel
    private List<TiempoDisponible> personaByID=new ArrayList<TiempoDisponible>();
    
    @Restrict("#{s:hasRole('Inscription_cancel','Search_cancel')}") 
    @End
    public void cancel( ){

    }
    @Restrict("#{s:hasRole('Inscription_destroy','Search_destroy')}") 
    @Remove
    public void destroy( ){

    }
    @Restrict("#{s:hasRole('Inscription_confirm','Search_confirm')}") 
    @End
    public void confirm( ){
        em.persist(tiempoDisponible);
        facesMessages.add("tiempoDisponible added");
    }
    @Restrict("#{s:hasRole('Inscription_delete')}") 
    public void delete( ){
        tiempoDisponible=em.merge(tiempoDisponible);
        em.remove(tiempoDisponible);  
        facesMessages.add("tiempoDisponible deleted");
    }
    @Restrict("#{s:hasRole('Inscription_newTiempoDisponible')}") 
    @Begin
    public void newTiempoDisponible( ){
        tiempoDisponible=new TiempoDisponible();
    }
    @Restrict("#{s:hasRole('Inscription_selectTiempoDisponible','Search_selectTiempoDisponible')}") 
    @Begin(join=true)
    public void selectTiempoDisponible(TiempoDisponible selectedTiempoDisponible ){
        tiempoDisponible=em.find(TiempoDisponible.class, selectedTiempoDisponible.getId());
    }
    @Restrict("#{s:hasRole('Inscription_insertInPersona')}") 
    public void insertInPersona(TiempoDisponible selectedTiempoDisponible){
        tiempoDisponible.setPersona(persona);
        //em.persist(tiempoDisponible);
        tiempoDisponible =em.merge(selectedTiempoDisponible);
        facesMessages.add("tiempoDisponible added");
    }
    @Restrict("#{s:hasRole('Inscription_getTiempoDisponibleInPersonaByID','Search_getTiempoDisponibleInPersonaByID')}") 
    public  List<TiempoDisponible>  getTiempoDisponibleInPersonaByID(){
        List<TiempoDisponible> results = em.createQuery("select h from TiempoDisponible h where h.persona.id = ?1").setParameter(1,persona.getId()).getResultList();  
        personaByID = results;
        return personaByID;
    }
    @Restrict("#{s:hasRole('Inscription_editTiempoDisponible')}") 
    @End
    public void editTiempoDisponible( ){
        tiempoDisponible=em.merge(tiempoDisponible); 
        facesMessages.add("tiempoDisponible edited");
    }
    @Restrict("#{s:hasRole('Inscription_selectPersona','Search_selectPersona')}") 
    public void selectPersona(Persona selectedPersona){

    }
}
