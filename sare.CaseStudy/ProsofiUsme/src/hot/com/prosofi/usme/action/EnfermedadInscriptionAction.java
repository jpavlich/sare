package com.prosofi.usme.action;


/**
   * Este codigo fue generado por sare.m2t.generator 
   * http://code.google.com/p/sare/
   * 
   */

import java.util.List;
import java.util.ArrayList;
import javax.ejb.*;
import javax.persistence.*;
import org.jboss.seam.annotations.*;
import org.jboss.seam.annotations.datamodel.DataModel;
import org.jboss.seam.annotations.datamodel.DataModelSelection;
import org.jboss.seam.annotations.faces.*;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.annotations.security.Restrict;

import com.prosofi.usme.model.Enfermedad;
import com.prosofi.usme.model.Persona;


@Stateful
@Name ("EnfermedadInscription")
public class EnfermedadInscriptionAction implements EnfermedadInscription {

    @PersistenceContext
    private EntityManager em;

    @In
    private FacesMessages facesMessages;
    

    @In(required=false)
    @Out(required=false)
    private Enfermedad enfermedad;

    @In(required=false)
    @Out(required=false)
    private Persona persona;

    @DataModel
    private List<Enfermedad> personaByID=new ArrayList<Enfermedad>();
    
    @Restrict("#{s:hasRole('Inscription_cancel','Search_cancel')}") 
    @End
    public void cancel( ){

    }
    @Restrict("#{s:hasRole('Inscription_destroy','Search_destroy')}") 
    @Remove
    public void destroy( ){

    }
    @Restrict("#{s:hasRole('Inscription_confirm','Search_confirm')}") 
    @End
    public void confirm( ){
        em.persist(enfermedad);
        facesMessages.add("enfermedad added");
    }
    @Restrict("#{s:hasRole('Inscription_delete')}") 
    public void delete( ){
        enfermedad=em.merge(enfermedad);
        em.remove(enfermedad);  
        facesMessages.add("enfermedad deleted");
    }
    @Restrict("#{s:hasRole('Inscription_newEnfermedad')}") 
    @Begin
    public void newEnfermedad( ){
        enfermedad=new Enfermedad();
    }
    @Restrict("#{s:hasRole('Inscription_selectEnfermedad','Search_selectEnfermedad')}") 
    @Begin(join=true)
    public void selectEnfermedad(Enfermedad selectedEnfermedad ){
        enfermedad=em.find(Enfermedad.class, selectedEnfermedad.getId());
    }
    @Restrict("#{s:hasRole('Inscription_insertInPersona')}") 
    public void insertInPersona(Enfermedad selectedEnfermedad){
        enfermedad.setPersona(persona);
        //em.persist(enfermedad);
        enfermedad =em.merge(selectedEnfermedad);
        facesMessages.add("enfermedad added");
    }
    @Restrict("#{s:hasRole('Inscription_getEnfermedadInPersonaByID','Search_getEnfermedadInPersonaByID')}") 
    public  List<Enfermedad>  getEnfermedadInPersonaByID(){
        List<Enfermedad> results = em.createQuery("select h from Enfermedad h where h.persona.id = ?1").setParameter(1,persona.getId()).getResultList();  
        personaByID = results;
        return personaByID;
    }
    @Restrict("#{s:hasRole('Inscription_editEnfermedad')}") 
    @End
    public void editEnfermedad( ){
        enfermedad=em.merge(enfermedad); 
        facesMessages.add("enfermedad edited");
    }
    @Restrict("#{s:hasRole('Inscription_selectPersona','Search_selectPersona')}") 
    public void selectPersona(Persona selectedPersona){

    }
}
