package com.prosofi.usme.action;

/**
   * Este codigo fue generado por sare.m2t.generator 
   * http://code.google.com/p/sare/
   * 
   */

import javax.ejb.Local;

import com.prosofi.usme.model.SalubridadVivienda;
import com.prosofi.usme.model.Vivienda;

import java.util.*;

@Local
public interface ViviendaInscription {

   public void cancel( );
   public void destroy( );
   public void confirm( );
   public void delete( );
   public void newVivienda( );
   public void selectVivienda(Vivienda selectVivienda);
   public void selectSalubridadVivienda(SalubridadVivienda selectedSalubridadVivienda);
   public void editVivienda( );
}
