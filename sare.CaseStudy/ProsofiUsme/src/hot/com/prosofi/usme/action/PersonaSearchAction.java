package com.prosofi.usme.action;


/**
   * Este codigo fue generado por sare.m2t.generator 
   * http://code.google.com/p/sare/
   * 
   */

import java.util.List;
import java.util.ArrayList;
import javax.ejb.*;
import javax.persistence.*;
import org.jboss.seam.annotations.*;
import org.jboss.seam.annotations.datamodel.DataModel;
import org.jboss.seam.annotations.datamodel.DataModelSelection;
import org.jboss.seam.annotations.faces.*;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.annotations.security.Restrict;

import com.prosofi.usme.model.Persona;


@Stateful
@Name ("PersonaSearch")
public class PersonaSearchAction implements PersonaSearch {

    @PersistenceContext
    private EntityManager em;

    @In
    private FacesMessages facesMessages;
    

    @DataModel
    private List<Persona> personasC=new ArrayList<Persona>();
    
    @Restrict("#{s:hasRole('Inscription_find','Search_find')}") 
    public void find( ){
        queryPersona();
    }
    @Restrict("#{s:hasRole('Inscription_queryPersona','Search_queryPersona')}") 
    public void queryPersona( ){
        List<Persona> results = em.createQuery("select h from Persona h").getResultList();
        personasC = results;
    }
    @Restrict("#{s:hasRole('Inscription_getPersonasC','Search_getPersonasC')}") 
    public  List<Persona>  getPersonasC(){
        List<Persona> results = em.createQuery("select h from Persona h").getResultList();  
        personasC = results;
        return personasC;
    }
    @Restrict("#{s:hasRole('Inscription_setPersonasC','Search_setPersonasC')}") 
    public void setPersonasC(List <Persona> personasC){
        this.personasC = personasC;
    }
    @Restrict("#{s:hasRole('Inscription_destroy','Search_destroy')}") 
    @Remove
    public void destroy( ){

    }
}
