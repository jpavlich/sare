package com.prosofi.usme.action;

/**
   * Este codigo fue generado por sare.m2t.generator 
   * http://code.google.com/p/sare/
   * 
   */

import javax.ejb.Local;

import com.prosofi.usme.model.Necesidad;

import java.util.*;

@Local
public interface NecesidadInscription {

   public void cancel( );
   public void destroy( );
   public void confirm( );
   public void delete( );
   public void newNecesidad( );
   public void selectNecesidad(Necesidad selectedNecesidad );
   public List<Necesidad> getNecesidadInDatosPsicoSocialByID();
   public void editNecesidad( );
   public void insertInDatoPsicoSocial(Necesidad selectedNecesidad);
}
