package com.prosofi.usme.action;


/**
   * Este codigo fue generado por sare.m2t.generator 
   * http://code.google.com/p/sare/
   * 
   */

import java.util.List;
import java.util.ArrayList;
import javax.ejb.*;
import javax.persistence.*;
import org.jboss.seam.annotations.*;
import org.jboss.seam.annotations.datamodel.DataModel;
import org.jboss.seam.annotations.datamodel.DataModelSelection;
import org.jboss.seam.annotations.faces.*;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.annotations.security.Restrict;

import com.prosofi.usme.model.SalubridadVivienda;
import com.prosofi.usme.model.Vivienda;


@Stateful
@Name ("SalubridadViviendaInscription")
public class SalubridadViviendaInscriptionAction implements SalubridadViviendaInscription {

    @PersistenceContext
    private EntityManager em;

    @In
    private FacesMessages facesMessages;
    

    @In(required=false)
    @Out(required=false)
    private SalubridadVivienda salubridadVivienda;

    @In(required=false)
    @Out(required=false)
    private Vivienda vivienda;
    
    @Restrict("#{s:hasRole('Inscription_cancel','Search_cancel')}") 
    @End
    public void cancel( ){

    }
    @Restrict("#{s:hasRole('Inscription_destroy','Search_destroy')}") 
    @Remove
    public void destroy( ){

    }
    @Restrict("#{s:hasRole('Inscription_confirm','Search_confirm')}") 
    @End
    public void confirm( ){
        em.persist(salubridadVivienda);
        facesMessages.add("salubridadVivienda added");
    }
    @Restrict("#{s:hasRole('Inscription_delete')}") 
    public void delete( ){
        salubridadVivienda=em.merge(salubridadVivienda);
        em.remove(salubridadVivienda);  
        facesMessages.add("salubridadVivienda deleted");
    }
    @Restrict("#{s:hasRole('Inscription_newSalubridadVivienda')}") 
    @Begin
    public void newSalubridadVivienda( ){
        salubridadVivienda=new SalubridadVivienda();
    }
    @Restrict("#{s:hasRole('Inscription_selectSalubridadVivienda','Search_selectSalubridadVivienda')}") 
    @Begin(join=true)
    public void selectSalubridadVivienda(SalubridadVivienda selectedSalubridadVivienda ){
        salubridadVivienda=em.find(SalubridadVivienda.class, selectedSalubridadVivienda.getId());
    }
    @Restrict("#{s:hasRole('Inscription_selectVivienda','Search_selectVivienda')}") 
    public void selectVivienda(Vivienda selectedVivienda){
        vivienda = em.merge(selectedVivienda);
    }
    @Restrict("#{s:hasRole('Inscription_editSalubridadVivienda')}") 
    @End
    public void editSalubridadVivienda( ){
        if(vivienda!=null){
            salubridadVivienda.setVivienda(vivienda);  
         }
        salubridadVivienda=em.merge(salubridadVivienda); 
        facesMessages.add("salubridadVivienda edited");
    }
}
