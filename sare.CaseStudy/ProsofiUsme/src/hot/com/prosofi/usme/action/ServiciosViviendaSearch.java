package com.prosofi.usme.action;

/**
   * Este codigo fue generado por sare.m2t.generator 
   * http://code.google.com/p/sare/
   * 
   */

import javax.ejb.Local;

import com.prosofi.usme.model.ServiciosVivienda;

import java.util.*;

@Local
public interface ServiciosViviendaSearch {

   public void find( );
   public void queryServiciosVivienda( );
   public List<ServiciosVivienda> getServiciosViviendasC();
   public void setServiciosViviendasC(List <ServiciosVivienda> serviciosViviendasC);
   public void destroy( );
}
