package com.prosofi.usme.action;

/**
   * Este codigo fue generado por sare.m2t.generator 
   * http://code.google.com/p/sare/
   * 
   */

import javax.ejb.Local;

import com.prosofi.usme.model.AlrededoresVivienda;

import java.util.*;

@Local
public interface AlrededoresViviendaSearch {

   public void find( );
   public void queryAlrededoresVivienda( );
   public List<AlrededoresVivienda> getAlrededoresViviendasC();
   public void setAlrededoresViviendasC(List <AlrededoresVivienda> alrededoresViviendasC);
   public void destroy( );
}
