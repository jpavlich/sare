package com.prosofi.usme.action;


/**
   * Este codigo fue generado por sare.m2t.generator 
   * http://code.google.com/p/sare/
   * 
   */

import java.util.List;
import java.util.ArrayList;
import javax.ejb.*;
import javax.persistence.*;
import org.jboss.seam.annotations.*;
import org.jboss.seam.annotations.datamodel.DataModel;
import org.jboss.seam.annotations.datamodel.DataModelSelection;
import org.jboss.seam.annotations.faces.*;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.annotations.security.Restrict;

import com.prosofi.usme.model.DatosEconomico;
import com.prosofi.usme.model.Persona;


@Stateful
@Name ("DatosEconomicoInscription")
public class DatosEconomicoInscriptionAction implements DatosEconomicoInscription {

    @PersistenceContext
    private EntityManager em;

    @In
    private FacesMessages facesMessages;
    

    @In(required=false)
    @Out(required=false)
    private Persona persona;

    @In(required=false)
    @Out(required=false)
    private DatosEconomico datosEconomico;
    
    @Restrict("#{s:hasRole('Inscription_cancel','Search_cancel')}") 
    @End
    public void cancel( ){

    }
    @Restrict("#{s:hasRole('Inscription_destroy','Search_destroy')}") 
    @Remove
    public void destroy( ){

    }
    @Restrict("#{s:hasRole('Inscription_confirm','Search_confirm')}") 
    @End
    public void confirm( ){
        em.persist(datosEconomico);
        facesMessages.add("datosEconomico added");
    }
    @Restrict("#{s:hasRole('Inscription_delete')}") 
    public void delete( ){
        datosEconomico=em.merge(datosEconomico);
        em.remove(datosEconomico);  
        facesMessages.add("datosEconomico deleted");
    }
    @Restrict("#{s:hasRole('Inscription_newDatosEconomico')}") 
    @Begin
    public void newDatosEconomico( ){
        datosEconomico=new DatosEconomico();
    }
    @Restrict("#{s:hasRole('Inscription_selectDatosEconomico','Search_selectDatosEconomico')}") 
    @Begin(join=true)
    public void selectDatosEconomico( DatosEconomico selectedDatosEconomico){
        datosEconomico=em.find(DatosEconomico.class,selectedDatosEconomico.getId());
    }
    @Restrict("#{s:hasRole('Inscription_editDatosEconomico')}") 
    @End
    public void editDatosEconomico( ){
        if(persona!=null){
            datosEconomico.setPersona(persona);  
         }
        datosEconomico=em.merge(datosEconomico); 
        facesMessages.add("datosEconomico edited");
    }
    @Restrict("#{s:hasRole('Inscription_selectPersona','Search_selectPersona')}") 
    public void selectPersona(Persona selectedPersona){
        persona = em.merge(selectedPersona);
    }
}
