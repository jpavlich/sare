package com.prosofi.usme.action;


/**
   * Este codigo fue generado por sare.m2t.generator 
   * http://code.google.com/p/sare/
   * 
   */

import java.util.List;
import java.util.ArrayList;
import javax.ejb.*;
import javax.persistence.*;
import org.jboss.seam.annotations.*;
import org.jboss.seam.annotations.datamodel.DataModel;
import org.jboss.seam.annotations.datamodel.DataModelSelection;
import org.jboss.seam.annotations.faces.*;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.annotations.security.Restrict;

import com.prosofi.usme.model.ControlPrenatal;



@Stateful
@Name ("ControlPrenatalSearch")
public class ControlPrenatalSearchAction implements ControlPrenatalSearch {

    @PersistenceContext
    private EntityManager em;

    @In
    private FacesMessages facesMessages;
    

    @DataModel
    private List<ControlPrenatal> controlPrenatalsC=new ArrayList<ControlPrenatal>();
    
    @Restrict("#{s:hasRole('Inscription_find','Search_find')}") 
    public void find( ){
       queryControlPrenatal();
    }
    @Restrict("#{s:hasRole('Inscription_queryControlPrenatal','Search_queryControlPrenatal')}") 
    public void queryControlPrenatal( ){
        List<ControlPrenatal> results = em.createQuery("select h from ControlPrenatal h").getResultList();
        controlPrenatalsC = results;
    }
    @Restrict("#{s:hasRole('Inscription_getControlPrenatalsC','Search_getControlPrenatalsC')}") 
    public  List<ControlPrenatal>  getControlPrenatalsC(){
        List<ControlPrenatal> results = em.createQuery("select h from ControlPrenatal h").getResultList();  
        controlPrenatalsC = results;
        return controlPrenatalsC;
    }
    @Restrict("#{s:hasRole('Inscription_setControlPrenatalsC','Search_setControlPrenatalsC')}") 
    public void setControlPrenatalsC(List <ControlPrenatal> controlPrenatalsC){
        this.controlPrenatalsC = controlPrenatalsC;
    }
    @Restrict("#{s:hasRole('Inscription_destroy','Search_destroy')}") 
    @Remove
    public void destroy( ){

    }
}
