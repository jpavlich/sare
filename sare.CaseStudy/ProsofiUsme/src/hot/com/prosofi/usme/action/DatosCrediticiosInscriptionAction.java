package com.prosofi.usme.action;


/**
   * Este codigo fue generado por sare.m2t.generator 
   * http://code.google.com/p/sare/
   * 
   */

import java.util.List;
import java.util.ArrayList;
import javax.ejb.*;
import javax.persistence.*;
import org.jboss.seam.annotations.*;
import org.jboss.seam.annotations.datamodel.DataModel;
import org.jboss.seam.annotations.datamodel.DataModelSelection;
import org.jboss.seam.annotations.faces.*;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.annotations.security.Restrict;

import com.prosofi.usme.model.DatosCrediticios;
import com.prosofi.usme.model.Persona;


@Stateful
@Name ("DatosCrediticiosInscription")
public class DatosCrediticiosInscriptionAction implements DatosCrediticiosInscription {

    @PersistenceContext
    private EntityManager em;

    @In
    private FacesMessages facesMessages;
    

    @In(required=false)
    @Out(required=false)
    private DatosCrediticios datosCrediticios;

    @In(required=false)
    @Out(required=false)
    private Persona persona;

    @DataModel
    private List<DatosCrediticios> personaByID=new ArrayList<DatosCrediticios>();
    
    @Restrict("#{s:hasRole('Inscription_cancel','Search_cancel')}") 
    @End
    public void cancel( ){

    }
    @Restrict("#{s:hasRole('Inscription_destroy','Search_destroy')}") 
    @Remove
    public void destroy( ){

    }
    @Restrict("#{s:hasRole('Inscription_confirm','Search_confirm')}") 
    @End
    public void confirm( ){
        em.persist(datosCrediticios);
        facesMessages.add("datosCrediticios added");
    }
    @Restrict("#{s:hasRole('Inscription_delete')}") 
    public void delete( ){
        datosCrediticios=em.merge(datosCrediticios);
        em.remove(datosCrediticios);  
        facesMessages.add("datosCrediticios deleted");
    }
    @Restrict("#{s:hasRole('Inscription_newDatosCrediticios')}") 
    @Begin
    public void newDatosCrediticios( ){
        datosCrediticios=new DatosCrediticios();
    }
    @Restrict("#{s:hasRole('Inscription_selectDatosCrediticios','Search_selectDatosCrediticios')}") 
    @Begin(join=true)
    public void selectDatosCrediticios(DatosCrediticios selectedDatosCrediticios ){
        datosCrediticios=em.find(DatosCrediticios.class, selectedDatosCrediticios.getId());
    }
    @Restrict("#{s:hasRole('Inscription_insertInPersona')}") 
    public void insertInPersona(DatosCrediticios selectedDatosCrediticios){
        datosCrediticios.setPersona(persona);
        //em.persist(datosCrediticios);
        datosCrediticios =em.merge(selectedDatosCrediticios);
        facesMessages.add("datosCrediticios added");
    }
    @Restrict("#{s:hasRole('Inscription_getDatosCrediticiosInPersonaByID','Search_getDatosCrediticiosInPersonaByID')}") 
    public  List<DatosCrediticios>  getDatosCrediticiosInPersonaByID(){
        List<DatosCrediticios> results = em.createQuery("select h from DatosCrediticios h where h.persona.id = ?1").setParameter(1,persona.getId()).getResultList();  
        personaByID = results;
        return personaByID;
    }
    @Restrict("#{s:hasRole('Inscription_editDatosCrediticios')}") 
    @End
    public void editDatosCrediticios( ){
        datosCrediticios=em.merge(datosCrediticios); 
        facesMessages.add("datosCrediticios edited");
    }
    @Restrict("#{s:hasRole('Inscription_selectPersona','Search_selectPersona')}") 
    public void selectPersona(Persona selectedPersona){
        persona=em.merge(selectedPersona);
    }
}
