package com.prosofi.usme.action;

/**
   * Este codigo fue generado por sare.m2t.generator 
   * http://code.google.com/p/sare/
   * 
   */

import javax.ejb.Local;

import com.prosofi.usme.model.PercepcionVivienda;

import java.util.*;

@Local
public interface PercepcionViviendaSearch {

   public void find( );
   public void queryPercepcionVivienda( );
   public List<PercepcionVivienda> getPercepcionViviendasC();
   public void setPercepcionViviendasC(List <PercepcionVivienda> percepcionViviendasC);
   public void destroy( );
}
