package com.prosofi.usme.action;


/**
   * Este codigo fue generado por sare.m2t.generator 
   * http://code.google.com/p/sare/
   * 
   */

import java.util.List;
import java.util.ArrayList;
import javax.ejb.*;
import javax.persistence.*;
import org.jboss.seam.annotations.*;
import org.jboss.seam.annotations.datamodel.DataModel;
import org.jboss.seam.annotations.datamodel.DataModelSelection;
import org.jboss.seam.annotations.faces.*;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.annotations.security.Restrict;

import com.prosofi.usme.model.Salud;


@Stateful
@Name ("SaludSearch")
public class SaludSearchAction implements SaludSearch {

    @PersistenceContext
    private EntityManager em;

    @In
    private FacesMessages facesMessages;
    

    @DataModel
    private List<Salud> saludsC=new ArrayList<Salud>();
    
    @Restrict("#{s:hasRole('Inscription_find','Search_find')}") 
    public void find( ){
        querySalud();
    }
    @Restrict("#{s:hasRole('Inscription_querySalud','Search_querySalud')}") 
    public void querySalud( ){
        List<Salud> results = em.createQuery("select h from Salud h").getResultList();
        saludsC = results;
    }
    @Restrict("#{s:hasRole('Inscription_getSaludsC','Search_getSaludsC')}") 
    public  List<Salud>  getSaludsC(){
        List<Salud> results = em.createQuery("select h from Salud h").getResultList();  
        saludsC = results;
        return saludsC;
    }
    @Restrict("#{s:hasRole('Inscription_setSaludsC','Search_setSaludsC')}") 
    public void setSaludsC(List <Salud> saludsC){
        this.saludsC = saludsC;
    }
    @Restrict("#{s:hasRole('Inscription_destroy','Search_destroy')}") 
    @Remove
    public void destroy( ){

    }
}
