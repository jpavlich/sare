package com.prosofi.usme.action;


/**
   * Este codigo fue generado por sare.m2t.generator 
   * http://code.google.com/p/sare/
   * 
   */

import java.util.List;
import java.util.ArrayList;
import javax.ejb.*;
import javax.persistence.*;
import org.jboss.seam.annotations.*;
import org.jboss.seam.annotations.datamodel.DataModel;
import org.jboss.seam.annotations.datamodel.DataModelSelection;
import org.jboss.seam.annotations.faces.*;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.annotations.security.Restrict;

import com.prosofi.usme.model.GastoMensuales;


@Stateful
@Name ("GastoMensualesSearch")
public class GastoMensualesSearchAction implements GastoMensualesSearch {

    @PersistenceContext
    private EntityManager em;

    @In
    private FacesMessages facesMessages;
    

    @DataModel
    private List<GastoMensuales> gastoMensualessC=new ArrayList<GastoMensuales>();
    
    @Restrict("#{s:hasRole('Inscription_find','Search_find')}") 
    public void find( ){
        queryGastoMensuales();
    }
    @Restrict("#{s:hasRole('Inscription_queryGastoMensuales','Search_queryGastoMensuales')}") 
    public void queryGastoMensuales( ){
        List<GastoMensuales> results = em.createQuery("select h from GastoMensuales h").getResultList();
        gastoMensualessC = results;
    }
    @Restrict("#{s:hasRole('Inscription_getGastoMensualessC','Search_getGastoMensualessC')}") 
    public  List<GastoMensuales>  getGastoMensualessC(){
        List<GastoMensuales> results = em.createQuery("select h from GastoMensuales h").getResultList();  
        gastoMensualessC = results;
        return gastoMensualessC;
    }
    @Restrict("#{s:hasRole('Inscription_setGastoMensualessC','Search_setGastoMensualessC')}") 
    public void setGastoMensualessC(List <GastoMensuales> gastoMensualessC){
        this.gastoMensualessC = gastoMensualessC;
    }
    @Restrict("#{s:hasRole('Inscription_destroy','Search_destroy')}") 
    @Remove
    public void destroy( ){

    }
}
