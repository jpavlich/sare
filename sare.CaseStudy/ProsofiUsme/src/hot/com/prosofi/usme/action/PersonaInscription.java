package com.prosofi.usme.action;

/**
   * Este codigo fue generado por sare.m2t.generator 
   * http://code.google.com/p/sare/
   * 
   */

import javax.ejb.Local;

import com.prosofi.usme.model.ControlPrenatal;
import com.prosofi.usme.model.DatosEconomico;
import com.prosofi.usme.model.PercepcionVivienda;
import com.prosofi.usme.model.Persona;
import com.prosofi.usme.model.Salud;

import java.util.*;

@Local
public interface PersonaInscription {

   public void cancel( );
   public void destroy( );
   public void confirm( );
   public void delete( );
   public void newPersona( );
   public void selectPersona(Persona selectedPersona );
   public void editPersona( );
   public void selectDatosEconomico(DatosEconomico selectedDatosEconomico);
   public void selectSalud(Salud selectedSalud);
   public void selectControlPrenatal(ControlPrenatal selectedControlPrenatal);
   public void selectPercepcionVivienda(PercepcionVivienda selectedPercepcionVivienda);
   public void insertInFamilia(Persona selectedPersona);
   public List<Persona> getPersonaInFamiliaByID();
}
