package com.prosofi.usme.action;


/**
   * Este codigo fue generado por sare.m2t.generator 
   * http://code.google.com/p/sare/
   * 
   */

import java.util.List;
import java.util.ArrayList;
import javax.ejb.*;
import javax.persistence.*;
import org.jboss.seam.annotations.*;
import org.jboss.seam.annotations.datamodel.DataModel;
import org.jboss.seam.annotations.datamodel.DataModelSelection;
import org.jboss.seam.annotations.faces.*;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.annotations.security.Restrict;

import com.prosofi.usme.model.PercepcionVivienda;
import com.prosofi.usme.model.Persona;


@Stateful
@Name ("PercepcionViviendaInscription")
public class PercepcionViviendaInscriptionAction implements PercepcionViviendaInscription {

    @PersistenceContext
    private EntityManager em;

    @In
    private FacesMessages facesMessages;
    

    @In(required=false)
    @Out(required=false)
    private Persona persona;

    @In(required=false)
    @Out(required=false)
    private PercepcionVivienda percepcionVivienda;
    
    @Restrict("#{s:hasRole('Inscription_cancel','Search_cancel')}") 
    @End
    public void cancel( ){

    }
    @Restrict("#{s:hasRole('Inscription_destroy','Search_destroy')}") 
    @Remove
    public void destroy( ){

    }
    @Restrict("#{s:hasRole('Inscription_confirm','Search_confirm')}") 
    @End
    public void confirm( ){
        em.persist(percepcionVivienda);
        facesMessages.add("percepcionVivienda added");
    }
    @Restrict("#{s:hasRole('Inscription_delete')}") 
    public void delete( ){
        percepcionVivienda=em.merge(percepcionVivienda);
        em.remove(percepcionVivienda);  
        facesMessages.add("percepcionVivienda deleted");
    }
    @Restrict("#{s:hasRole('Inscription_newPercepcionVivienda')}") 
    @Begin
    public void newPercepcionVivienda( ){
        percepcionVivienda=new PercepcionVivienda();
    }
    @Restrict("#{s:hasRole('Inscription_selectPercepcionVivienda','Search_selectPercepcionVivienda')}") 
    @Begin(join=true)
    public void selectPercepcionVivienda(PercepcionVivienda selectedPercepcionVivienda ){
        percepcionVivienda=em.find(PercepcionVivienda.class, selectedPercepcionVivienda.getId());
    }
    @Restrict("#{s:hasRole('Inscription_editPercepcionVivienda')}") 
    @End
    public void editPercepcionVivienda( ){
        if(persona!=null){
            percepcionVivienda.setPersona(persona);  
         }
        percepcionVivienda=em.merge(percepcionVivienda); 
        facesMessages.add("percepcionVivienda edited");
    }
    @Restrict("#{s:hasRole('Inscription_selectPersona','Search_selectPersona')}") 
    public void selectPersona(Persona selectedPersona){
        persona = em.merge(selectedPersona);
    }
}
