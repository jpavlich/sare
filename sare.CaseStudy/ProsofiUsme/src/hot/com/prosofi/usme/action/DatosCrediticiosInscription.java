package com.prosofi.usme.action;

/**
   * Este codigo fue generado por sare.m2t.generator 
   * http://code.google.com/p/sare/
   * 
   */

import javax.ejb.Local;

import com.prosofi.usme.model.DatosCrediticios;
import com.prosofi.usme.model.Persona;

import java.util.*;

@Local
public interface DatosCrediticiosInscription {

   public void cancel( );
   public void destroy( );
   public void confirm( );
   public void delete( );
   public void newDatosCrediticios( );
   public void selectDatosCrediticios(DatosCrediticios selectedDatosCrediticios );
   public void insertInPersona(DatosCrediticios selectedDatosCrediticios);
   public List<DatosCrediticios> getDatosCrediticiosInPersonaByID();
   public void editDatosCrediticios( );
   public void selectPersona(Persona selectedPersona);
}
