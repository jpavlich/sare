package com.prosofi.usme.action;

/**
   * Este codigo fue generado por sare.m2t.generator 
   * http://code.google.com/p/sare/
   * 
   */

import java.util.List;
import java.util.ArrayList;
import javax.ejb.*;
import javax.persistence.*;
import org.jboss.seam.annotations.*;
import org.jboss.seam.annotations.datamodel.DataModel;
import org.jboss.seam.annotations.datamodel.DataModelSelection;
import org.jboss.seam.annotations.faces.*;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.annotations.security.Restrict;

import com.prosofi.usme.model.OrganizacionComunitaria;


@Stateful
@Name ("OrganizacionComunitariaSearch")
public class OrganizacionComunitariaSearchAction implements OrganizacionComunitariaSearch {

    @PersistenceContext
    private EntityManager em;

    @In
    private FacesMessages facesMessages;
    

    @DataModel
    private List<OrganizacionComunitaria> organizacionComunitariasC=new ArrayList<OrganizacionComunitaria>();
    
    @Restrict("#{s:hasRole('Inscription_find','Search_find')}") 
    public void find( ){
        queryOrganizacionComunitaria();
    }
    @Restrict("#{s:hasRole('Inscription_queryOrganizacionComunitaria','Search_queryOrganizacionComunitaria')}") 
    public void queryOrganizacionComunitaria( ){
        List<OrganizacionComunitaria> results = em.createQuery("select h from OrganizacionComunitaria h").getResultList();
        organizacionComunitariasC = results;
    }
    @Restrict("#{s:hasRole('Inscription_getOrganizacionComunitariasC','Search_getOrganizacionComunitariasC')}") 
    public  List<OrganizacionComunitaria>  getOrganizacionComunitariasC(){
        List<OrganizacionComunitaria> results = em.createQuery("select h from OrganizacionComunitaria h").getResultList();  
        organizacionComunitariasC = results;
        return organizacionComunitariasC;
    }
    @Restrict("#{s:hasRole('Inscription_setOrganizacionComunitariasC','Search_setOrganizacionComunitariasC')}") 
    public void setOrganizacionComunitariasC(List <OrganizacionComunitaria> organizacionComunitariasC){
        this.organizacionComunitariasC = organizacionComunitariasC;
    }
    @Restrict("#{s:hasRole('Inscription_destroy','Search_destroy')}") 
    @Remove
    public void destroy( ){

    }
}
