package com.prosofi.usme.action;

/**
   * Este codigo fue generado por sare.m2t.generator 
   * http://code.google.com/p/sare/
   * 
   */

import javax.ejb.Local;

import com.prosofi.usme.model.Enfermedad;
import com.prosofi.usme.model.Persona;

import java.util.*;

@Local
public interface EnfermedadInscription {

   public void cancel( );
   public void destroy( );
   public void confirm( );
   public void delete( );
   public void newEnfermedad( );
   public void selectEnfermedad(Enfermedad selectedEnfermedad);
   public void insertInPersona(Enfermedad selectedEnfermedad);
   public List<Enfermedad> getEnfermedadInPersonaByID();
   public void editEnfermedad( );
   public void selectPersona(Persona selectedPersona);
}
