package com.prosofi.usme.action;


/**
   * Este codigo fue generado por sare.m2t.generator 
   * http://code.google.com/p/sare/
   * 
   */

import java.util.List;
import java.util.ArrayList;
import javax.ejb.*;
import javax.persistence.*;
import org.jboss.seam.annotations.*;
import org.jboss.seam.annotations.datamodel.DataModel;
import org.jboss.seam.annotations.datamodel.DataModelSelection;
import org.jboss.seam.annotations.faces.*;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.annotations.security.Restrict;

import com.prosofi.usme.model.Animal;
import com.prosofi.usme.model.Familia;




@Stateful
@Name ("AnimalInscription")
public class AnimalInscriptionAction implements AnimalInscription {

    @PersistenceContext
    private EntityManager em;

    @In
    private FacesMessages facesMessages;
    

    @DataModel
    private List<Animal> familiaByID;

    @In(required=false)
    @Out(required=false)
    private Animal animal;

    @In(required=false)
    @Out(required=false)
    private Familia familia;
    
    @Restrict("#{s:hasRole('Inscription_cancel','Search_cancel')}") 
    @End
    public void cancel( ){

    }
    @Restrict("#{s:hasRole('Inscription_destroy','Search_destroy')}") 
    @Remove
    public void destroy( ){

    }
    @Restrict("#{s:hasRole('Inscription_confirm','Search_confirm')}") 
    @End
    public void confirm( ){
        em.persist(animal);
        facesMessages.add("animal added");
    }
    @Restrict("#{s:hasRole('Inscription_delete')}") 
    public void delete( ){
        animal=em.merge(animal);
        em.remove(animal);  
        facesMessages.add("animal deleted");
    }
    @Restrict("#{s:hasRole('Inscription_newAnimal')}") 
    @Begin
    public void newAnimal( ){
        animal=new Animal();
    }
    @Restrict("#{s:hasRole('Inscription_selectAnimal','Search_selectAnimal')}") 
    @Begin(join=true)
    public void selectAnimal(Animal selectedAnimal ){
        animal = em.find(Animal.class, selectedAnimal.getId());
    }
    @Restrict("#{s:hasRole('Inscription_editAnimal')}") 
    @End
    public void editAnimal( ){
        animal=em.merge(animal); 
        facesMessages.add("animal edited");
    }
    @Restrict("#{s:hasRole('Inscription_insertInFamilia')}") 
    public void insertInFamilia(Animal selectedAnimal){
        animal.setFamilia(familia);
        //em.persist(animal);
        animal =em.merge(selectedAnimal);
        facesMessages.add("animal added");
    }
    @Restrict("#{s:hasRole('Inscription_getAnimalInFamiliaByID','Search_getAnimalInFamiliaByID')}") 
    public  List<Animal>  getAnimalInFamiliaByID(){
        List<Animal> results = em.createQuery("select h from Animal h where h.familia.id = ?1").setParameter(1,familia.getId()).getResultList();  
        familiaByID = results;
        return familiaByID;
    }
}
