package com.prosofi.usme.action;


/**
   * Este codigo fue generado por sare.m2t.generator 
   * http://code.google.com/p/sare/
   * 
   */

import java.util.List;
import java.util.ArrayList;
import javax.ejb.*;
import javax.persistence.*;
import org.jboss.seam.annotations.*;
import org.jboss.seam.annotations.datamodel.DataModel;
import org.jboss.seam.annotations.datamodel.DataModelSelection;
import org.jboss.seam.annotations.faces.*;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.annotations.security.Restrict;

import com.prosofi.usme.model.AlrededoresVivienda;


@Stateful
@Name ("AlrededoresViviendaSearch")
public class AlrededoresViviendaSearchAction implements AlrededoresViviendaSearch {

    @PersistenceContext
    private EntityManager em;

    @In
    private FacesMessages facesMessages;
    

    @DataModel
    private List<AlrededoresVivienda> alrededoresViviendasC=new ArrayList<AlrededoresVivienda>();
    
    //@Restrict("#{s:hasRole('Inscription_find','Search_find')}") 
    public void find( ){
        queryAlrededoresVivienda();
    }
    //@Restrict("#{s:hasRole('Inscription_queryAlrededoresVivienda','Search_queryAlrededoresVivienda')}") 
    public void queryAlrededoresVivienda( ){
        List<AlrededoresVivienda> results = em.createQuery("select h from AlrededoresVivienda h").getResultList();
        alrededoresViviendasC = results;
    }
    //@Restrict("#{s:hasRole('Inscription_getAlrededoresViviendasC','Search_getAlrededoresViviendasC')}") 
    public  List<AlrededoresVivienda>  getAlrededoresViviendasC(){
        List<AlrededoresVivienda> results = em.createQuery("select h from AlrededoresVivienda h").getResultList();  
        alrededoresViviendasC = results;
        return alrededoresViviendasC;
    }
    @Restrict("#{s:hasRole('Inscription_setAlrededoresViviendasC','Search_setAlrededoresViviendasC')}") 
    public void setAlrededoresViviendasC(List <AlrededoresVivienda> alrededoresViviendasC){
        this.alrededoresViviendasC = alrededoresViviendasC;
    }
    //@Restrict("#{s:hasRole('Inscription_destroy','Search_destroy')}") 
    @Remove
    public void destroy( ){

    }
}
