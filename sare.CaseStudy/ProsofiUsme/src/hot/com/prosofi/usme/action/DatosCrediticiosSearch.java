package com.prosofi.usme.action;

/**
   * Este codigo fue generado por sare.m2t.generator 
   * http://code.google.com/p/sare/
   * 
   */

import javax.ejb.Local;

import com.prosofi.usme.model.DatosCrediticios;

import java.util.*;

@Local
public interface DatosCrediticiosSearch {

   public void find( );
   public void queryDatosCrediticios( );
   public List<DatosCrediticios> getDatosCrediticiossC();
   public void setDatosCrediticiossC(List <DatosCrediticios> datosCrediticiossC);
   public void destroy( );
}
