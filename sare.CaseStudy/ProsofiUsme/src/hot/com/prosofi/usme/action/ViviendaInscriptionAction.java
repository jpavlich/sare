package com.prosofi.usme.action;


/**
   * Este codigo fue generado por sare.m2t.generator 
   * http://code.google.com/p/sare/
   * 
   */

import java.util.List;
import java.util.ArrayList;
import javax.ejb.*;
import javax.persistence.*;
import org.jboss.seam.annotations.*;
import org.jboss.seam.annotations.datamodel.DataModel;
import org.jboss.seam.annotations.datamodel.DataModelSelection;
import org.jboss.seam.annotations.faces.*;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.annotations.security.Restrict;

import com.prosofi.usme.model.SalubridadVivienda;
import com.prosofi.usme.model.Vivienda;


@Stateful
@Name ("ViviendaInscription")
@Restrict("#{s:hasRole('Inscription')}") 
public class ViviendaInscriptionAction implements ViviendaInscription {

    @PersistenceContext
    private EntityManager em;

    @In
    private FacesMessages facesMessages;
    

    @In(required=false)
    @Out(required=false)
    private Vivienda vivienda;

    @In(required=false)
    @Out(required=false)
    private SalubridadVivienda salubridad;
    
    @Restrict("#{s:hasRole('Inscription_cancel')}") 
    @End
    public void cancel( ){

    }
    @Restrict("#{s:hasRole('Inscription_destroy')}") 
    @Remove
    public void destroy( ){

    }
    @Restrict("#{s:hasRole('Inscription_confirm')}") 
    @End
    public void confirm( ){
        em.persist(vivienda);
        facesMessages.add("vivienda added");
    }
    @Restrict("#{s:hasRole('Inscription_delete')}") 
    public void delete( ){
        vivienda=em.merge(vivienda);
        em.remove(vivienda);  
        facesMessages.add("vivienda deleted");
    }
    @Restrict("#{s:hasRole('Inscription_newVivienda')}") 
    @Begin
    public void newVivienda( ){
        vivienda=new Vivienda();
    }
    @Restrict("#{s:hasRole('Inscription_selectVivienda')}") 
    @Begin(join=true)
    public void selectVivienda(Vivienda selectVivienda){
        vivienda=em.merge(selectVivienda);
    }
    @Restrict("#{s:hasRole('Inscription_selectSalubridadVivienda')}") 
    public void selectSalubridadVivienda(SalubridadVivienda selectedSalubridadVivienda){
        salubridad = em.merge(selectedSalubridadVivienda);
    }
    @Restrict("#{s:hasRole('Inscription_editVivienda')}") 
    @End
    public void editVivienda( ){
        if(salubridad!=null){
            vivienda.setSalubridad(salubridad);  
         }
        vivienda=em.merge(vivienda); 
        facesMessages.add("vivienda edited");  
    }
}
