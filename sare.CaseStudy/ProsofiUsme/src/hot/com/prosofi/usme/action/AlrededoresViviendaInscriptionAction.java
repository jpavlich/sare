package com.prosofi.usme.action;


/**
   * Este codigo fue generado por sare.m2t.generator 
   * http://code.google.com/p/sare/
   * 
   */

import java.util.List;
import java.util.ArrayList;
import javax.ejb.*;
import javax.persistence.*;
import org.jboss.seam.annotations.*;
import org.jboss.seam.annotations.datamodel.DataModel;
import org.jboss.seam.annotations.datamodel.DataModelSelection;
import org.jboss.seam.annotations.faces.*;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.annotations.security.Restrict;

import com.prosofi.usme.model.AlrededoresVivienda;
import com.prosofi.usme.model.Vivienda;



@Stateful
@Name ("AlrededoresViviendaInscription")
public class AlrededoresViviendaInscriptionAction implements AlrededoresViviendaInscription {

    @PersistenceContext
    private EntityManager em;

    @In
    private FacesMessages facesMessages;
    

    @In(required=false)
    @Out(required=false)
    private AlrededoresVivienda alrededoresVivienda;

    @DataModel
    private List<AlrededoresVivienda> viviendaByID=new ArrayList<AlrededoresVivienda>();

    @In(required=false)
    @Out(required=false)
    private Vivienda vivienda;
    
    @Restrict("#{s:hasRole('Inscription_cancel','Search_cancel')}") 
    @End
    public void cancel( ){

    }
    @Restrict("#{s:hasRole('Inscription_destroy','Search_destroy')}") 
    @Remove
    public void destroy( ){

    }
    @Restrict("#{s:hasRole('Inscription_confirm','Search_confirm')}") 
    @End
    public void confirm( ){
        em.persist(alrededoresVivienda);
        facesMessages.add("alrededoresVivienda added");
    }
    @Restrict("#{s:hasRole('Inscription_delete','Search_delete')}") 
    public void delete( ){
        alrededoresVivienda=em.merge(alrededoresVivienda);
        em.remove(alrededoresVivienda);  
        facesMessages.add("alrededoresVivienda deleted");
    }
    @Restrict("#{s:hasRole('Inscription_newAlrededoresVivienda')}") 
    @Begin
    public void newAlrededoresVivienda( ){
        alrededoresVivienda=new AlrededoresVivienda();
    }
    @Restrict("#{s:hasRole('Inscription_selectAlrededoresVivienda','Search_selectAlrededoresVivienda')}") 
    @Begin(join=true)
    public void selectAlrededoresVivienda(AlrededoresVivienda selectedAlrededoresVivienda ){
        alrededoresVivienda = em.find(AlrededoresVivienda.class, selectedAlrededoresVivienda.getId());
    }
    @Restrict("#{s:hasRole('Inscription_insertInVivienda')}") 
    public void insertInVivienda(AlrededoresVivienda selectedAlrededoresVivienda){
        alrededoresVivienda.setVivienda(vivienda);
        //em.persist(alrededoresVivienda);
        alrededoresVivienda =em.merge(selectedAlrededoresVivienda);
        facesMessages.add("alrededoresVivienda added");
    }
    @Restrict("#{s:hasRole('Inscription_getAlrededoresViviendaInViviendaByID','Search_getAlrededoresViviendaInViviendaByID')}") 
    public  List<AlrededoresVivienda>  getAlrededoresViviendaInViviendaByID(){
        List<AlrededoresVivienda> results = em.createQuery("select h from AlrededoresVivienda h where h.vivienda.id = ?1").setParameter(1,vivienda.getId()).getResultList();  
        viviendaByID = results;
        return viviendaByID;
    }
    @Restrict("#{s:hasRole('Inscription_editAlrededoresVivienda')}") 
    @End
    public void editAlrededoresVivienda( ){
        alrededoresVivienda=em.merge(alrededoresVivienda); 
        facesMessages.add("alrededoresVivienda edited");
    }
}
