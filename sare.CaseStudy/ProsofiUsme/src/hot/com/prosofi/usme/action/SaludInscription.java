package com.prosofi.usme.action;

/**
   * Este codigo fue generado por sare.m2t.generator 
   * http://code.google.com/p/sare/
   * 
   */

import javax.ejb.Local;

import com.prosofi.usme.model.Persona;
import com.prosofi.usme.model.Salud;

import java.util.*;

@Local
public interface SaludInscription {

   public void cancel( );
   public void destroy( );
   public void confirm( );
   public void delete( );
   public void newSalud( );
   public void selectSalud(Salud selectSalud );
   public void editSalud( );
   public void selectPersona(Persona selectedPersona);
}
