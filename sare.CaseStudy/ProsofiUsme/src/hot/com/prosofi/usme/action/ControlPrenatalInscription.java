package com.prosofi.usme.action;

/**
   * Este codigo fue generado por sare.m2t.generator 
   * http://code.google.com/p/sare/
   * 
   */

import javax.ejb.Local;

import com.prosofi.usme.model.ControlPrenatal;
import com.prosofi.usme.model.Persona;

import java.util.*;

@Local
public interface ControlPrenatalInscription {

   public void cancel( );
   public void destroy( );
   public void confirm( );
   public void delete( );
   public void newControlPrenatal( );
   public void selectControlPrenatal(ControlPrenatal selectedControlPrenatal );
   public void editControlPrenatal( );
   public void selectPersona(Persona selectedPersona);
}
