package com.prosofi.usme.action;


/**
   * Este codigo fue generado por sare.m2t.generator 
   * http://code.google.com/p/sare/
   * 
   */

import java.util.List;
import java.util.ArrayList;
import javax.ejb.*;
import javax.persistence.*;
import org.jboss.seam.annotations.*;
import org.jboss.seam.annotations.datamodel.DataModel;
import org.jboss.seam.annotations.datamodel.DataModelSelection;
import org.jboss.seam.annotations.faces.*;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.annotations.security.Restrict;

import com.prosofi.usme.model.DatosPsicoSocial;
import com.prosofi.usme.model.Encuestador;
import com.prosofi.usme.model.Familia;
import com.prosofi.usme.model.Vivienda;



@Stateful
@Name ("FamiliaInscription")
public class FamiliaInscriptionAction implements FamiliaInscription {

    @PersistenceContext
    private EntityManager em;

    @In
    private FacesMessages facesMessages;
    

    @In(required=false)
    @Out(required=false)
    private DatosPsicoSocial datosPsicoSocial;

    @In(required=false)
    @Out(required=false)
    private Vivienda vivienda;

    @DataModel
    private List<Familia> viviendaByID=new ArrayList<Familia>();

    @In(required=false)
    @Out(required=false)
    private Familia familia;

    @In(required=false)
    @Out(required=false)
    private Encuestador encuestador;
    
    @Restrict("#{s:hasRole('Inscription_cancel','Search_cancel')}") 
    @End
    public void cancel( ){

    }
    @Restrict("#{s:hasRole('Inscription_destroy','Search_destroy')}") 
    @Remove
    public void destroy( ){

    }
    @Restrict("#{s:hasRole('Inscription_confirm','Search_confirm')}") 
    @End
    public void confirm( ){
        em.persist(familia);
        facesMessages.add("familia added");
    }
    @Restrict("#{s:hasRole('Inscription_delete')}") 
    public void delete( ){
        familia=em.merge(familia);
        em.remove(familia);  
        facesMessages.add("familia deleted");
    }
    @Restrict("#{s:hasRole('Inscription_newFamilia')}") 
    @Begin
    public void newFamilia( ){
        familia=new Familia();
    }
    @Restrict("#{s:hasRole('Inscription_selectFamilia','Search_selectFamilia')}") 
    @Begin(join=true)
    public void selectFamilia(Familia selectedFamilia ){
        familia=em.find(Familia.class, selectedFamilia.getId());
    }
    @Restrict("#{s:hasRole('Inscription_editFamilia')}") 
    @End
    public void editFamilia( ){
        if(encuestador!=null){
            familia.setEncuestador(encuestador);  
         }
         if(datosPsicoSocial!=null){
            familia.setDatosPsicoSocial(datosPsicoSocial);  
         }
        familia=em.merge(familia); 
        facesMessages.add("familia edited");
    }
    @Restrict("#{s:hasRole('Inscription_selectEncuestador','Search_selectEncuestador')}") 
    public void selectEncuestador(Encuestador selectedEncuestador){

    }
    @Restrict("#{s:hasRole('Inscription_insertInVivienda')}") 
    public void insertInVivienda(Familia selectedFamilia){
        familia.setVivienda(vivienda);
        //em.persist(familia);
        familia =em.merge(selectedFamilia);
        facesMessages.add("familia added");
    }
    @Restrict("#{s:hasRole('Inscription_getFamiliaInViviendaByID','Search_getFamiliaInViviendaByID')}") 
    public  List<Familia>  getFamiliaInViviendaByID(){
        List<Familia> results = em.createQuery("select h from Familia h where h.vivienda.id = ?1").setParameter(1,vivienda.getId()).getResultList();  
        viviendaByID = results;
        return viviendaByID;
    }
    @Restrict("#{s:hasRole('Inscription_selectDatosPsicoSocial','Search_selectDatosPsicoSocial')}") 
    public void selectDatosPsicoSocial(DatosPsicoSocial selectedDatosPsicoSocial){
        datosPsicoSocial = em.merge(selectedDatosPsicoSocial);
    }
}
