package com.prosofi.usme.action;


/**
   * Este codigo fue generado por sare.m2t.generator 
   * http://code.google.com/p/sare/
   * 
   */

import java.util.List;
import java.util.ArrayList;
import javax.ejb.*;
import javax.persistence.*;
import org.jboss.seam.annotations.*;
import org.jboss.seam.annotations.datamodel.DataModel;
import org.jboss.seam.annotations.datamodel.DataModelSelection;
import org.jboss.seam.annotations.faces.*;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.annotations.security.Restrict;

import com.prosofi.usme.model.Catastrofe;


@Stateful
@Name ("CatastrofeInscription")
public class CatastrofeInscriptionAction implements CatastrofeInscription {

    @PersistenceContext
    private EntityManager em;

    @In
    private FacesMessages facesMessages;
    

    @In(required=false)
    @Out(required=false)
    private Catastrofe catastrofe;
    
    @Restrict("#{s:hasRole('Inscription_cancel','Search_cancel')}") 
    @End
    public void cancel( ){

    }
    @Restrict("#{s:hasRole('Inscription_destroy','Search_destroy')}") 
    @Remove
    public void destroy( ){

    }
    @Restrict("#{s:hasRole('Inscription_confirm','Search_confirm')}") 
    @End
    public void confirm( ){
        catastrofe = em.merge(catastrofe);
        em.persist(catastrofe);
        facesMessages.add("catastrofe added");
    }
    @Restrict("#{s:hasRole('Inscription_delete')}") 
    public void delete( ){
        catastrofe=em.merge(catastrofe);
        em.remove(catastrofe);  
        facesMessages.add("catastrofe deleted");
    }
    @Restrict("#{s:hasRole('Inscription_newCatastrofe')}") 
    @Begin
    public void newCatastrofe( ){
        catastrofe=new Catastrofe();
    }
    @Restrict("#{s:hasRole('Inscription_selectCatastrofe','Search_selectCatastrofe')}") 
    @Begin(join=true)
    public void selectCatastrofe( Catastrofe selesctedCatastrode ){
        catastrofe = em.find(Catastrofe.class, selesctedCatastrode.getId());
    }
    @Restrict("#{s:hasRole('Inscription_editCatastrofe')}") 
    @End
    public void editCatastrofe( ){
        catastrofe=em.merge(catastrofe); 
        facesMessages.add("catastrofe edited");
    }
}
