package com.prosofi.usme.action;

/**
   * Este codigo fue generado por sare.m2t.generator 
   * http://code.google.com/p/sare/
   * 
   */

import javax.ejb.Local;

import com.prosofi.usme.model.Vivienda;

import java.util.*;

@Local
public interface ViviendaSearch {

   public void find( );
   public void queryVivienda( );
   public List<Vivienda> getViviendasC();
   public void setViviendasC(List <Vivienda> viviendasC);
   public void destroy( );
}
