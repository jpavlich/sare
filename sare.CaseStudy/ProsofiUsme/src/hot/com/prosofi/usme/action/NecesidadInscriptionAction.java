package com.prosofi.usme.action;


/**
   * Este codigo fue generado por sare.m2t.generator 
   * http://code.google.com/p/sare/
   * 
   */

import java.util.List;
import java.util.ArrayList;
import javax.ejb.*;
import javax.persistence.*;
import org.jboss.seam.annotations.*;
import org.jboss.seam.annotations.datamodel.DataModel;
import org.jboss.seam.annotations.datamodel.DataModelSelection;
import org.jboss.seam.annotations.faces.*;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.annotations.security.Restrict;

import com.prosofi.usme.model.DatosPsicoSocial;
import com.prosofi.usme.model.Necesidad;


@Stateful
@Name ("NecesidadInscription")
public class NecesidadInscriptionAction implements NecesidadInscription {

    @PersistenceContext
    private EntityManager em;

    @In
    private FacesMessages facesMessages;
    

    @In(required=false)
    @Out(required=false)
    private DatosPsicoSocial datoPsicoSocial;

    @In(required=false)
    @Out(required=false)
    private Necesidad necesidad;

    @DataModel
    private List<Necesidad> datoPsicoSocialByID=new ArrayList<Necesidad>();
    
    @Restrict("#{s:hasRole('Inscription_cancel','Search_cancel')}") 
    @End
    public void cancel( ){

    }
    @Restrict("#{s:hasRole('Inscription_destroy','Search_destroy')}") 
    @Remove
    public void destroy( ){

    }
    @Restrict("#{s:hasRole('Search_confirm')}") 
    @End
    public void confirm( ){

    }
    @Restrict("#{s:hasRole('Inscription_delete')}") 
    public void delete( ){

    }
    @Restrict("#{s:hasRole('Inscription_newNecesidad')}") 
    @Begin
    public void newNecesidad( ){
        necesidad=new Necesidad();
    }
    @Restrict("#{s:hasRole('Inscription_selectNecesidad','Search_selectNecesidad')}") 
    @Begin(join=true)
    public void selectNecesidad( Necesidad selectedNecesidad){
        necesidad=em.find(Necesidad.class, selectedNecesidad.getId());
    }
    @Restrict("#{s:hasRole('Inscription_getNecesidadInDatosPsicoSocialByID','Search_getNecesidadInDatosPsicoSocialByID')}") 
    public  List<Necesidad>  getNecesidadInDatosPsicoSocialByID(){
        List<Necesidad> results = em.createQuery("select h from Necesidad h where h.datoPsicoSocial.id = ?1").setParameter(1,datoPsicoSocial.getId()).getResultList();  
        datoPsicoSocialByID = results;
        return datoPsicoSocialByID;
    }
    @Restrict("#{s:hasRole('Inscription_editNecesidad')}") 
    @End
    public void editNecesidad( ){

    }
    @Restrict("#{s:hasRole('Inscription_selectNecesidad')}") 
    public void insertInDatoPsicoSocial(Necesidad selectedNecesidad){
        necesidad.setDatoPsicoSocial(datoPsicoSocial);
        //em.persist(necesidad);
        necesidad =em.merge(selectedNecesidad);
        facesMessages.add("necesidad added");
    }
     
    
}
