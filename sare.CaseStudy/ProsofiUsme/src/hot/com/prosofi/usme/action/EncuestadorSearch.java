package com.prosofi.usme.action;

/**
   * Este codigo fue generado por sare.m2t.generator 
   * http://code.google.com/p/sare/
   * 
   */

import javax.ejb.Local;

import com.prosofi.usme.model.Encuestador;

import java.util.*;

@Local
public interface EncuestadorSearch {

   public void find( );
   public void queryEncuestador( );
   public List<Encuestador> getEncuestadorsC();
   public void setEncuestadorsC(List <Encuestador> encuestadorsC);
   public void destroy( );
}
