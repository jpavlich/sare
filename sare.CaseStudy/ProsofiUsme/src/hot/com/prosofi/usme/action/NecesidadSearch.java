package com.prosofi.usme.action;

/**
   * Este codigo fue generado por sare.m2t.generator 
   * http://code.google.com/p/sare/
   * 
   */

import javax.ejb.Local;

import com.prosofi.usme.model.Necesidad;

import java.util.*;

@Local
public interface NecesidadSearch {

   public void find( );
   public void queryNecesidad( );
   public List<Necesidad> getNecesidadsC();
   public void setNecesidadsC(List <Necesidad> necesidadsC);
   public void destroy( );
}
