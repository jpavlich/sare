package com.prosofi.usme.action;


/**
   * Este codigo fue generado por sare.m2t.generator 
   * http://code.google.com/p/sare/
   * 
   */

import java.util.List;
import java.util.ArrayList;
import javax.ejb.*;
import javax.persistence.*;
import org.jboss.seam.annotations.*;
import org.jboss.seam.annotations.datamodel.DataModel;
import org.jboss.seam.annotations.datamodel.DataModelSelection;
import org.jboss.seam.annotations.faces.*;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.annotations.security.Restrict;

import com.prosofi.usme.model.ServiciosVivienda;


@Stateful
@Name ("ServiciosViviendaSearch")
public class ServiciosViviendaSearchAction implements ServiciosViviendaSearch {

    @PersistenceContext
    private EntityManager em;

    @In
    private FacesMessages facesMessages;
    

    @DataModel
    private List<ServiciosVivienda> serviciosViviendasC=new ArrayList<ServiciosVivienda>();
    
    @Restrict("#{s:hasRole('Inscription_find','Search_find')}") 
    public void find( ){
        queryServiciosVivienda();
    }
    @Restrict("#{s:hasRole('Inscription_queryServiciosVivienda','Search_queryServiciosVivienda')}") 
    public void queryServiciosVivienda( ){
        List<ServiciosVivienda> results = em.createQuery("select h from ServiciosVivienda h").getResultList();
        serviciosViviendasC = results;
    }
    @Restrict("#{s:hasRole('Inscription_getServiciosViviendasC','Search_getServiciosViviendasC')}") 
    public  List<ServiciosVivienda>  getServiciosViviendasC(){
        List<ServiciosVivienda> results = em.createQuery("select h from ServiciosVivienda h").getResultList();  
        serviciosViviendasC = results;
        return serviciosViviendasC;
    }
    @Restrict("#{s:hasRole('Inscription_setServiciosViviendasC','Search_setServiciosViviendasC')}") 
    public void setServiciosViviendasC(List <ServiciosVivienda> serviciosViviendasC){
        this.serviciosViviendasC = serviciosViviendasC;
    }
    @Restrict("#{s:hasRole('Inscription_destroy','Search_destroy')}") 
    @Remove
    public void destroy( ){

    }
}
