package com.prosofi.usme.action;

/**
   * Este codigo fue generado por sare.m2t.generator 
   * http://code.google.com/p/sare/
   * 
   */

import javax.ejb.Local;

import com.prosofi.usme.model.ServiciosVivienda;

import java.util.*;

@Local
public interface ServiciosViviendaInscription {

   public void cancel( );
   public void destroy( );
   public void confirm( );
   public void delete( );
   public void newServiciosVivienda( );
   public void selectServiciosVivienda(ServiciosVivienda selectedServiciosVivienda );
   public void insertInVivienda(ServiciosVivienda selectedServiciosVivienda);
   public List<ServiciosVivienda> getServiciosViviendaInViviendaByID();
   public void editServiciosVivienda( );
}
