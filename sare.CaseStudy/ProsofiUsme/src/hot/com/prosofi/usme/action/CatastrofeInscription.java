package com.prosofi.usme.action;

/**
   * Este codigo fue generado por sare.m2t.generator 
   * http://code.google.com/p/sare/
   * 
   */

import javax.ejb.Local;

import com.prosofi.usme.model.Catastrofe;

import java.util.*;

@Local
public interface CatastrofeInscription {

   public void cancel( );
   public void destroy( );
   public void confirm( );
   public void delete( );
   public void newCatastrofe( );
   public void selectCatastrofe(Catastrofe selesctedCatastrode );
   public void editCatastrofe( );
}
