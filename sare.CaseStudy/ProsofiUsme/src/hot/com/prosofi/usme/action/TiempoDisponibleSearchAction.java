package com.prosofi.usme.action;


/**
   * Este codigo fue generado por sare.m2t.generator 
   * http://code.google.com/p/sare/
   * 
   */

import java.util.List;
import java.util.ArrayList;
import javax.ejb.*;
import javax.persistence.*;
import org.jboss.seam.annotations.*;
import org.jboss.seam.annotations.datamodel.DataModel;
import org.jboss.seam.annotations.datamodel.DataModelSelection;
import org.jboss.seam.annotations.faces.*;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.annotations.security.Restrict;

import com.prosofi.usme.model.TiempoDisponible;


@Stateful
@Name ("TiempoDisponibleSearch")
public class TiempoDisponibleSearchAction implements TiempoDisponibleSearch {

    @PersistenceContext
    private EntityManager em;

    @In
    private FacesMessages facesMessages;
    

    @DataModel
    private List<TiempoDisponible> tiempoDisponiblesC=new ArrayList<TiempoDisponible>();
    
    @Restrict("#{s:hasRole('Inscription_find','Search_find')}") 
    public void find( ){
        queryTiempoDisponible();
    }
    @Restrict("#{s:hasRole('Inscription_queryTiempoDisponible','Search_queryTiempoDisponible')}") 
    public void queryTiempoDisponible( ){
        List<TiempoDisponible> results = em.createQuery("select h from TiempoDisponible h").getResultList();
        tiempoDisponiblesC = results;
    }
    @Restrict("#{s:hasRole('Inscription_getTiempoDisponiblesC','Search_getTiempoDisponiblesC')}") 
    public  List<TiempoDisponible>  getTiempoDisponiblesC(){
        List<TiempoDisponible> results = em.createQuery("select h from TiempoDisponible h").getResultList();  
        tiempoDisponiblesC = results;
        return tiempoDisponiblesC;
    }
    @Restrict("#{s:hasRole('Inscription_setTiempoDisponiblesC','Search_setTiempoDisponiblesC')}") 
    public void setTiempoDisponiblesC(List <TiempoDisponible> tiempoDisponiblesC){
        this.tiempoDisponiblesC = tiempoDisponiblesC;
    }
    @Restrict("#{s:hasRole('Inscription_destroy','Search_destroy')}") 
    @Remove
    public void destroy( ){

    }
}
