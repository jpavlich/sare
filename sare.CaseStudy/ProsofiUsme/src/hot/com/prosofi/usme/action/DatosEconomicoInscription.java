package com.prosofi.usme.action;

/**
   * Este codigo fue generado por sare.m2t.generator 
   * http://code.google.com/p/sare/
   * 
   */

import javax.ejb.Local;

import com.prosofi.usme.model.DatosEconomico;
import com.prosofi.usme.model.Persona;

import java.util.*;

@Local
public interface DatosEconomicoInscription {

   public void cancel( );
   public void destroy( );
   public void confirm( );
   public void delete( );
   public void newDatosEconomico( );
   public void selectDatosEconomico(DatosEconomico selectedDatosEconomico );
   public void editDatosEconomico( );
   public void selectPersona(Persona selectedPersona);
}
