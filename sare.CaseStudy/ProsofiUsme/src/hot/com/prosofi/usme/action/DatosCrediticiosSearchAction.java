package com.prosofi.usme.action;


/**
   * Este codigo fue generado por sare.m2t.generator 
   * http://code.google.com/p/sare/
   * 
   */

import java.util.List;
import java.util.ArrayList;
import javax.ejb.*;
import javax.persistence.*;
import org.jboss.seam.annotations.*;
import org.jboss.seam.annotations.datamodel.DataModel;
import org.jboss.seam.annotations.datamodel.DataModelSelection;
import org.jboss.seam.annotations.faces.*;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.annotations.security.Restrict;

import com.prosofi.usme.model.DatosCrediticios;


@Stateful
@Name ("DatosCrediticiosSearch")
public class DatosCrediticiosSearchAction implements DatosCrediticiosSearch {

    @PersistenceContext
    private EntityManager em;

    @In
    private FacesMessages facesMessages;
    

    @DataModel
    private List<DatosCrediticios> datosCrediticiossC=new ArrayList<DatosCrediticios>();
    
    @Restrict("#{s:hasRole('Inscription_find','Search_find')}") 
    public void find( ){
        queryDatosCrediticios();
    }
    @Restrict("#{s:hasRole('Inscription_queryDatosCrediticios','Search_queryDatosCrediticios')}") 
    public void queryDatosCrediticios( ){
        List<DatosCrediticios> results = em.createQuery("select h from DatosCrediticios h").getResultList();
        datosCrediticiossC = results;
    }
    @Restrict("#{s:hasRole('Inscription_getDatosCrediticiossC','Search_getDatosCrediticiossC')}") 
    public  List<DatosCrediticios>  getDatosCrediticiossC(){
        List<DatosCrediticios> results = em.createQuery("select h from DatosCrediticios h").getResultList();  
        datosCrediticiossC = results;
        return datosCrediticiossC;
    }
    @Restrict("#{s:hasRole('Inscription_setDatosCrediticiossC','Search_setDatosCrediticiossC')}") 
    public void setDatosCrediticiossC(List <DatosCrediticios> datosCrediticiossC){
        this.datosCrediticiossC = datosCrediticiossC;
    }
    @Restrict("#{s:hasRole('Inscription_destroy','Search_destroy')}") 
    @Remove
    public void destroy( ){

    }
}
