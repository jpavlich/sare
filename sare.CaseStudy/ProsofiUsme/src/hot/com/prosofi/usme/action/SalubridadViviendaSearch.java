package com.prosofi.usme.action;

/**
   * Este codigo fue generado por sare.m2t.generator 
   * http://code.google.com/p/sare/
   * 
   */

import javax.ejb.Local;

import com.prosofi.usme.model.SalubridadVivienda;

import java.util.*;

@Local
public interface SalubridadViviendaSearch {

   public void find( );
   public void querySalubridadVivienda( );
   public List<SalubridadVivienda> getSalubridadViviendasC();
   public void setSalubridadViviendasC(List <SalubridadVivienda> salubridadViviendasC);
   public void destroy( );
}
