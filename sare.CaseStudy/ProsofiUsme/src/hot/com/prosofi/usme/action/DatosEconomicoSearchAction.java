package com.prosofi.usme.action;


/**
   * Este codigo fue generado por sare.m2t.generator 
   * http://code.google.com/p/sare/
   * 
   */

import java.util.List;
import java.util.ArrayList;
import javax.ejb.*;
import javax.persistence.*;
import org.jboss.seam.annotations.*;
import org.jboss.seam.annotations.datamodel.DataModel;
import org.jboss.seam.annotations.datamodel.DataModelSelection;
import org.jboss.seam.annotations.faces.*;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.annotations.security.Restrict;

import com.prosofi.usme.model.DatosEconomico;



@Stateful
@Name ("DatosEconomicoSearch")
public class DatosEconomicoSearchAction implements DatosEconomicoSearch {

    @PersistenceContext
    private EntityManager em;

    @In
    private FacesMessages facesMessages;
    

    @DataModel
    private List<DatosEconomico> datosEconomicosC=new ArrayList<DatosEconomico>();
    
    @Restrict("#{s:hasRole('Inscription_find','Search_find')}") 
    public void find( ){
        queryDatosEconomico();
    }
    @Restrict("#{s:hasRole('Inscription_queryDatosEconomico','Search_queryDatosEconomico')}") 
    public void queryDatosEconomico( ){
        List<DatosEconomico> results = em.createQuery("select h from DatosEconomico h").getResultList();
        datosEconomicosC = results;
    }
    @Restrict("#{s:hasRole('Inscription_getDatosEconomicosC','Search_getDatosEconomicosC')}") 
    public  List<DatosEconomico>  getDatosEconomicosC(){
        List<DatosEconomico> results = em.createQuery("select h from DatosEconomico h").getResultList();  
        datosEconomicosC = results;
        return datosEconomicosC;
    }
    @Restrict("#{s:hasRole('Inscription_setDatosEconomicosC','Search_setDatosEconomicosC')}") 
    public void setDatosEconomicosC(List <DatosEconomico> datosEconomicosC){
        this.datosEconomicosC = datosEconomicosC;
    }
    @Restrict("#{s:hasRole('Inscription_destroy','Search_destroy')}") 
    @Remove
    public void destroy( ){

    }
}
