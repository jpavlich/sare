package com.prosofi.usme.action;

/**
   * Este codigo fue generado por sare.m2t.generator 
   * http://code.google.com/p/sare/
   * 
   */

import javax.ejb.Local;

import com.prosofi.usme.model.PercepcionVivienda;
import com.prosofi.usme.model.Persona;

import java.util.*;

@Local
public interface PercepcionViviendaInscription {

   public void cancel( );
   public void destroy( );
   public void confirm( );
   public void delete( );
   public void newPercepcionVivienda( );
   public void selectPercepcionVivienda(PercepcionVivienda selectedPercepcionVivienda );
   public void editPercepcionVivienda( );
   public void selectPersona(Persona selectedPersona);
}
