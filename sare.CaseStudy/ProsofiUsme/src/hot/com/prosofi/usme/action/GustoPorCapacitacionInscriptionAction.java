package com.prosofi.usme.action;


/**
   * Este codigo fue generado por sare.m2t.generator 
   * http://code.google.com/p/sare/
   * 
   */

import java.util.List;
import java.util.ArrayList;
import javax.ejb.*;
import javax.persistence.*;
import org.jboss.seam.annotations.*;
import org.jboss.seam.annotations.datamodel.DataModel;
import org.jboss.seam.annotations.datamodel.DataModelSelection;
import org.jboss.seam.annotations.faces.*;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.annotations.security.Restrict;

import com.prosofi.usme.model.GustoPorCapacitacion;
import com.prosofi.usme.model.Persona;


@Stateful
@Name ("GustoPorCapacitacionInscription")
public class GustoPorCapacitacionInscriptionAction implements GustoPorCapacitacionInscription {

    @PersistenceContext
    private EntityManager em;

    @In
    private FacesMessages facesMessages;
    

    @DataModel
    private List<GustoPorCapacitacion> personaByID=new ArrayList<GustoPorCapacitacion>();

    @In(required=false)
    @Out(required=false)
    private GustoPorCapacitacion gustoPorCapacitacion;

    @In(required=false)
    @Out(required=false)
    private Persona persona;
    
    @Restrict("#{s:hasRole('Inscription_cancel','Search_cancel')}") 
    @End
    public void cancel( ){

    }
    @Restrict("#{s:hasRole('Inscription_destroy','Search_destroy')}") 
    @Remove
    public void destroy( ){

    }
    @Restrict("#{s:hasRole('Inscription_confirm','Search_confirm')}") 
    @End
    public void confirm( ){
        em.persist(gustoPorCapacitacion);
        facesMessages.add("gustoPorCapacitacion added");
    }
    @Restrict("#{s:hasRole('Inscription_delete')}") 
    public void delete( ){
        gustoPorCapacitacion=em.merge(gustoPorCapacitacion);
        em.remove(gustoPorCapacitacion);  
        facesMessages.add("gustoPorCapacitacion deleted");
    }
    @Restrict("#{s:hasRole('Inscription_newGustoPorCapacitacion')}") 
    @Begin
    public void newGustoPorCapacitacion( ){
        gustoPorCapacitacion=new GustoPorCapacitacion();
    }
    @Restrict("#{s:hasRole('Inscription_selectGustoPorCapacitacion','Search_selectGustoPorCapacitacion')}") 
    @Begin(join=true)
    public void selectGustoPorCapacitacion(GustoPorCapacitacion selectedGustoPorCapacitacion ){
        gustoPorCapacitacion=em.find(GustoPorCapacitacion.class, selectedGustoPorCapacitacion.getId());
    }
    @Restrict("#{s:hasRole('Inscription_insertInPersona')}") 
    public void insertInPersona(GustoPorCapacitacion selectedGustoPorCapacitacion){
        gustoPorCapacitacion.setPersona(persona);
        //em.persist(gustoPorCapacitacion);
        gustoPorCapacitacion =em.merge(selectedGustoPorCapacitacion);
        facesMessages.add("gustoPorCapacitacion added");
    }
    @Restrict("#{s:hasRole('Inscription_getGustoPorCapacitacionInPersonaByID','Search_getGustoPorCapacitacionInPersonaByID')}") 
    public  List<GustoPorCapacitacion>  getGustoPorCapacitacionInPersonaByID(){
        List<GustoPorCapacitacion> results = em.createQuery("select h from GustoPorCapacitacion h where h.persona.id = ?1").setParameter(1,persona.getId()).getResultList();  
        personaByID = results;
        return personaByID;
    }
    @Restrict("#{s:hasRole('Inscription_editGustoPorCapacitacion')}") 
    @End
    public void editGustoPorCapacitacion( ){
        gustoPorCapacitacion=em.merge(gustoPorCapacitacion); 
        facesMessages.add("gustoPorCapacitacion edited");
    }
    @Restrict("#{s:hasRole('Inscription_selectPersona','Search_selectPersona')}") 
    public void selectPersona(Persona selectedPersona){

    }
}
