package com.prosofi.usme.action;


/**
   * Este codigo fue generado por sare.m2t.generator 
   * http://code.google.com/p/sare/
   * 
   */

import java.util.List;
import java.util.ArrayList;
import javax.ejb.*;
import javax.persistence.*;
import org.jboss.seam.annotations.*;
import org.jboss.seam.annotations.datamodel.DataModel;
import org.jboss.seam.annotations.datamodel.DataModelSelection;
import org.jboss.seam.annotations.faces.*;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.annotations.security.Restrict;

import com.prosofi.usme.model.Encuestador;
import com.prosofi.usme.model.Familia;


@Stateful
@Name ("EncuestadorInscription")
public class EncuestadorInscriptionAction implements EncuestadorInscription {

    @PersistenceContext
    private EntityManager em;

    @In
    private FacesMessages facesMessages;
    

    @In(required=false)
    @Out(required=false)
    private Encuestador encuestador;

    @In(required=false)
    @Out(required=false)
    private Familia familia;
    
    @Restrict("#{s:hasRole('Inscription_cancel','Search_cancel')}") 
    @End
    public void cancel( ){

    }
    @Restrict("#{s:hasRole('Inscription_destroy','Search_destroy')}") 
    @Remove
    public void destroy( ){

    }
    @Restrict("#{s:hasRole('Inscription_confirm','Search_confirm')}") 
    @End
    public void confirm( ){
        em.persist(encuestador);
        facesMessages.add("encuestador added");
    }
    @Restrict("#{s:hasRole('Inscription_delete')}") 
    public void delete( ){
        encuestador=em.merge(encuestador);
        em.remove(encuestador);  
        facesMessages.add("encuestador deleted");
    }
    @Restrict("#{s:hasRole('Inscription_newEncuestador')}") 
    @Begin
    public void newEncuestador( ){
        encuestador=new Encuestador();
    }
    @Restrict("#{s:hasRole('Inscription_selectEncuestador','Search_selectEncuestador')}") 
    @Begin(join=true)
    public void selectEncuestador(Encuestador selectedEncuestador ){
        encuestador=em.find(Encuestador.class, selectedEncuestador.getId());
    }
    @Restrict("#{s:hasRole('Inscription_selectFamilia','Search_selectFamilia')}") 
    public  void  selectFamilia(Familia selectedFamilia){
        familia = em.merge(selectedFamilia);
    }
    @Restrict("#{s:hasRole('Inscription_editEncuestador')}") 
    @End
    public void editEncuestador( ){
    }
    /*
    if(familia!=null){
        encuestador.setFamilias(familia);  
     }
    encuestador=em.merge(encuestador); 
    facesMessages.add("encuestador edited");
}
*/
}
