package com.prosofi.usme.action;

/**
   * Este codigo fue generado por sare.m2t.generator 
   * http://code.google.com/p/sare/
   * 
   */

import javax.ejb.Local;

import com.prosofi.usme.model.AlrededoresVivienda;

import java.util.*;

@Local
public interface AlrededoresViviendaInscription {

   public void cancel( );
   public void destroy( );
   public void confirm( );
   public void delete( );
   public void newAlrededoresVivienda( );
   public void selectAlrededoresVivienda(AlrededoresVivienda selectedAlrededoresVivienda);
   public void insertInVivienda(AlrededoresVivienda selectedAlrededoresVivienda);
   public List<AlrededoresVivienda> getAlrededoresViviendaInViviendaByID();
   public void editAlrededoresVivienda( );
}
