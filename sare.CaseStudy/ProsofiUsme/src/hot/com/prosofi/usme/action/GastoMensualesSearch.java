package com.prosofi.usme.action;

/**
   * Este codigo fue generado por sare.m2t.generator 
   * http://code.google.com/p/sare/
   * 
   */

import javax.ejb.Local;

import com.prosofi.usme.model.GastoMensuales;

import java.util.*;

@Local
public interface GastoMensualesSearch {

   public void find( );
   public void queryGastoMensuales( );
   public List<GastoMensuales> getGastoMensualessC();
   public void setGastoMensualessC(List <GastoMensuales> gastoMensualessC);
   public void destroy( );
}
