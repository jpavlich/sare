package com.prosofi.usme.action;


/**
   * Este codigo fue generado por sare.m2t.generator 
   * http://code.google.com/p/sare/
   * 
   */

import java.util.List;
import java.util.ArrayList;
import javax.ejb.*;
import javax.persistence.*;
import org.jboss.seam.annotations.*;
import org.jboss.seam.annotations.datamodel.DataModel;
import org.jboss.seam.annotations.datamodel.DataModelSelection;
import org.jboss.seam.annotations.faces.*;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.annotations.security.Restrict;

import com.prosofi.usme.model.DatosPsicoSocial;
import com.prosofi.usme.model.OrganizacionComunitaria;


@Stateful
@Name ("OrganizacionComunitariaInscription")
public class OrganizacionComunitariaInscriptionAction implements OrganizacionComunitariaInscription {

    @PersistenceContext
    private EntityManager em;

    @In
    private FacesMessages facesMessages;
    

    @In(required=false)
    @Out(required=false)
    private DatosPsicoSocial datoPsicoSocial;

    @DataModel
    private List<OrganizacionComunitaria> datoPsicoSocialByID=new ArrayList<OrganizacionComunitaria>();

    @In(required=false)
    @Out(required=false)
    private OrganizacionComunitaria organizacionComunitaria;
    
    @Restrict("#{s:hasRole('Inscription_cancel','Search_cancel')}") 
    @End
    public void cancel( ){

    }
    @Restrict("#{s:hasRole('Inscription_destroy','Search_destroy')}") 
    @Remove
    public void destroy( ){

    }
    @Restrict("#{s:hasRole('Inscription_confirm','Search_confirm')}") 
    @End
    public void confirm( ){
        em.persist(organizacionComunitaria);
        facesMessages.add("organizacionComunitaria added");
    }
    @Restrict("#{s:hasRole('Inscription_delete')}") 
    public void delete( ){
        organizacionComunitaria=em.merge(organizacionComunitaria);
        em.remove(organizacionComunitaria);  
        facesMessages.add("organizacionComunitaria deleted");
    }
    @Restrict("#{s:hasRole('Inscription_newOrganizacionComunitaria')}") 
    @Begin
    public void newOrganizacionComunitaria( ){
        organizacionComunitaria=new OrganizacionComunitaria();
    }
    @Restrict("#{s:hasRole('Inscription_selectOrganizacionComunitaria','Search_selectOrganizacionComunitaria')}") 
    @Begin(join=true)
    public void selectOrganizacionComunitaria(OrganizacionComunitaria selectedOrganizacionComunitaria ){
        organizacionComunitaria=em.find(OrganizacionComunitaria.class, selectedOrganizacionComunitaria.getId());
    }
    @Restrict("#{s:hasRole('Inscription_getOrganizacionComunitariaInDatosPsicoSocialByID','Search_getOrganizacionComunitariaInDatosPsicoSocialByID')}") 
    public  List<OrganizacionComunitaria>  getOrganizacionComunitariaInDatosPsicoSocialByID(){
        List<OrganizacionComunitaria> results = em.createQuery("select h from OrganizacionComunitaria h where h.datoPsicoSocial.id = ?1").setParameter(1,datoPsicoSocial.getId()).getResultList();  
        datoPsicoSocialByID = results;
        return datoPsicoSocialByID;
    }
    @Restrict("#{s:hasRole('Inscription_editOrganizacionComunitaria')}") 
    @End
    public void editOrganizacionComunitaria( ){
        organizacionComunitaria=em.merge(organizacionComunitaria); 
        facesMessages.add("organizacionComunitaria edited");
    }
    @Restrict("#{s:hasRole('Inscription_insertInDatoPsicoSocial')}") 
    public void insertInDatoPsicoSocial(OrganizacionComunitaria selectedOrganizacionComunitaria){
        organizacionComunitaria.setDatoPsicoSocial(datoPsicoSocial);
        //em.persist(organizacionComunitaria);
        organizacionComunitaria =em.merge(selectedOrganizacionComunitaria);
        facesMessages.add("organizacionComunitaria added");
    }
}
