package com.prosofi.usme.action;


/**
   * Este codigo fue generado por sare.m2t.generator 
   * http://code.google.com/p/sare/
   * 
   */

import java.util.List;
import java.util.ArrayList;
import javax.ejb.*;
import javax.persistence.*;
import org.jboss.seam.annotations.*;
import org.jboss.seam.annotations.datamodel.DataModel;
import org.jboss.seam.annotations.datamodel.DataModelSelection;
import org.jboss.seam.annotations.faces.*;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.annotations.security.Restrict;

import com.prosofi.usme.model.DatosPsicoSocial;
import com.prosofi.usme.model.Familia;


@Stateful
@Name ("DatosPsicoSocialInscription")
public class DatosPsicoSocialInscriptionAction implements DatosPsicoSocialInscription {

    @PersistenceContext
    private EntityManager em;

    @In
    private FacesMessages facesMessages;
    

    @In(required=false)
    @Out(required=false)
    private DatosPsicoSocial datosPsicoSocial;

    @In(required=false)
    @Out(required=false)
    private Familia familia;
    
    @Restrict("#{s:hasRole('Inscription_newDatosPsicoSocial')}") 
    @Begin
    public void newDatosPsicoSocial( ){
        datosPsicoSocial=new DatosPsicoSocial();
    }
    @Restrict("#{s:hasRole('Inscription_cancel','Search_cancel')}") 
    @End
    public void cancel( ){

    }
    @Restrict("#{s:hasRole('Inscription_destroy','Search_destroy')}") 
    @Remove
    public void destroy( ){

    }
    @Restrict("#{s:hasRole('Inscription_selectDatosPsicoSocial','Search_selectDatosPsicoSocial')}") 
    @Begin(join=true)
    public void selectDatosPsicoSocial(DatosPsicoSocial selectedDatosPsicoSocial ){
        datosPsicoSocial=em.find(DatosPsicoSocial.class, selectedDatosPsicoSocial.getId());
    }
    @Restrict("#{s:hasRole('Inscription_selectFamilia','Search_selectFamilia')}") 
    @Begin(join=true)
    public void selectFamilia(Familia selectedFamilia){
        familia = em.merge(selectedFamilia);
    }
    @Restrict("#{s:hasRole('Inscription_confirm','Search_confirm')}") 
    @End
    public void confirm( ){
        em.persist(datosPsicoSocial);
        facesMessages.add("datosPsicoSocial added");
    }
    @Restrict("#{s:hasRole('Inscription_editDatosPsicoSocial')}") 
    @End
    public void editDatosPsicoSocial( ){
        if(familia!=null){
            datosPsicoSocial.setFamilia(familia);  
         }
        datosPsicoSocial=em.merge(datosPsicoSocial); 
        facesMessages.add("datosPsicoSocial edited");
    }
    @Restrict("#{s:hasRole('Inscription_delete')}") 
    public void delete( ){
        datosPsicoSocial=em.merge(datosPsicoSocial);
        em.remove(datosPsicoSocial);  
        facesMessages.add("datosPsicoSocial deleted");
    }
}
