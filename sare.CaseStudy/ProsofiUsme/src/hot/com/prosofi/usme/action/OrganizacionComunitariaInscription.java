package com.prosofi.usme.action;

/**
   * Este codigo fue generado por sare.m2t.generator 
   * http://code.google.com/p/sare/
   * 
   */

import javax.ejb.Local;

import com.prosofi.usme.model.OrganizacionComunitaria;

import java.util.*;

@Local
public interface OrganizacionComunitariaInscription {

   public void cancel( );
   public void destroy( );
   public void confirm( );
   public void delete( );
   public void newOrganizacionComunitaria( );
   public void selectOrganizacionComunitaria(OrganizacionComunitaria selectedOrganizacionComunitaria );
   public List<OrganizacionComunitaria> getOrganizacionComunitariaInDatosPsicoSocialByID();
   public void editOrganizacionComunitaria( );
   public void insertInDatoPsicoSocial(OrganizacionComunitaria selectedOrganizacionComunitaria);
}
