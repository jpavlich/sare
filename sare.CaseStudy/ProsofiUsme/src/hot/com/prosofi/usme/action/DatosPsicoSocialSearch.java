package com.prosofi.usme.action;

/**
   * Este codigo fue generado por sare.m2t.generator 
   * http://code.google.com/p/sare/
   * 
   */

import javax.ejb.Local;

import com.prosofi.usme.model.DatosPsicoSocial;

import java.util.*;

@Local
public interface DatosPsicoSocialSearch {

   public void find( );
   public void queryDatosPsicoSocial( );
   public List<DatosPsicoSocial> getDatosPsicoSocialsC();
   public void setDatosPsicoSocialsC(List <DatosPsicoSocial> datosPsicoSocialsC);
   public void destroy( );
}
