package com.prosofi.usme.action;

import javax.ejb.Local;

@Local
public interface Authenticator {

    boolean authenticate();

}
