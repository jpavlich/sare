package com.prosofi.usme.action;


/**
   * Este codigo fue generado por sare.m2t.generator 
   * http://code.google.com/p/sare/
   * 
   */

import java.util.List;
import java.util.ArrayList;
import javax.ejb.*;
import javax.persistence.*;
import org.jboss.seam.annotations.*;
import org.jboss.seam.annotations.datamodel.DataModel;
import org.jboss.seam.annotations.datamodel.DataModelSelection;
import org.jboss.seam.annotations.faces.*;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.annotations.security.Restrict;

import com.prosofi.usme.model.ControlPrenatal;
import com.prosofi.usme.model.DatosEconomico;
import com.prosofi.usme.model.Familia;
import com.prosofi.usme.model.PercepcionVivienda;
import com.prosofi.usme.model.Persona;
import com.prosofi.usme.model.Salud;


@Stateful
@Name ("PersonaInscription")
public class PersonaInscriptionAction implements PersonaInscription {

    @PersistenceContext
    private EntityManager em;

    @In
    private FacesMessages facesMessages;
    

    @In(required=false)
    @Out(required=false)
    private PercepcionVivienda percepcionVivienda;

    @In(required=false)
    @Out(required=false)
    private DatosEconomico datosEconomicos;

    @DataModel
    private List<Persona> familiaByID=new ArrayList<Persona>();

    @In(required=false)
    @Out(required=false)
    private ControlPrenatal controlPrenatal;

    @In(required=false)
    @Out(required=false)
    private Familia familia;

    @In(required=false)
    @Out(required=false)
    private Salud salud;

    @In(required=false)
    @Out(required=false)
    private Persona persona;
    
    @Restrict("#{s:hasRole('Inscription_cancel','Search_cancel')}") 
    @End
    public void cancel( ){

    }
    @Restrict("#{s:hasRole('Inscription_destroy','Search_destroy')}") 
    @Remove
    public void destroy( ){

    }
    @Restrict("#{s:hasRole('Inscription_confirm','Search_confirm')}") 
    @End
    public void confirm( ){
        em.persist(persona);
        facesMessages.add("persona added");
    }
    @Restrict("#{s:hasRole('Inscription_delete')}") 
    public void delete( ){
        persona=em.merge(persona);
        em.remove(persona);  
        facesMessages.add("persona deleted");
    }
    @Restrict("#{s:hasRole('Inscription_newPersona')}") 
    @Begin
    public void newPersona( ){
        persona=new Persona();
    }
    @Restrict("#{s:hasRole('Inscription_selectPersona','Search_selectPersona')}") 
    @Begin(join=true)
    public void selectPersona(Persona selectedPersona  ){
        persona=em.find(Persona.class, selectedPersona.getId());
    }
    @Restrict("#{s:hasRole('Inscription_editPersona')}") 
    @End
    public void editPersona( ){
        if(datosEconomicos!=null){
            persona.setDatosEconomicos(datosEconomicos);  
         }
         if(salud!=null){
            persona.setSalud(salud);  
         }
         if(controlPrenatal!=null){
            persona.setControlPrenatal(controlPrenatal);  
         }
         if(percepcionVivienda!=null){
            persona.setPercepcionVivienda(percepcionVivienda);  
         }
        persona=em.merge(persona); 
        facesMessages.add("persona edited");
    }
    @Restrict("#{s:hasRole('Inscription_selectDatosEconomico','Search_selectDatosEconomico')}") 
    public void selectDatosEconomico(DatosEconomico selectedDatosEconomico){
        datosEconomicos = em.merge(selectedDatosEconomico);
    }
    @Restrict("#{s:hasRole('Inscription_selectSalud','Search_selectSalud')}") 
    public void selectSalud(Salud selectedSalud){
        salud = em.merge(selectedSalud);
    }
    @Restrict("#{s:hasRole('Inscription_selectControlPrenatal','Search_selectControlPrenatal')}") 
    public void selectControlPrenatal(ControlPrenatal selectedControlPrenatal){
        controlPrenatal = em.merge(selectedControlPrenatal);
    }
    @Restrict("#{s:hasRole('Inscription_selectPercepcionVivienda','Search_selectPercepcionVivienda')}") 
    public void selectPercepcionVivienda(PercepcionVivienda selectedPercepcionVivienda){
        percepcionVivienda = em.merge(selectedPercepcionVivienda);
    }
    @Restrict("#{s:hasRole('Inscription_insertInFamilia')}") 
    public void insertInFamilia(Persona selectedPersona){
        persona.setFamilia(familia);
        //em.persist(persona);
        persona =em.merge(selectedPersona);
        facesMessages.add("persona added");
    }
    @Restrict("#{s:hasRole('Inscription_getPersonaInFamiliaByID','Search_getPersonaInFamiliaByID')}") 
    public  List<Persona>  getPersonaInFamiliaByID(){
        List<Persona> results = em.createQuery("select h from Persona h where h.familia.id = ?1").setParameter(1,familia.getId()).getResultList();  
        familiaByID = results;
        return familiaByID;
    }
}
