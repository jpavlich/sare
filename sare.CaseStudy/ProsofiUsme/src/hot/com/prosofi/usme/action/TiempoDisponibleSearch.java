package com.prosofi.usme.action;

/**
   * Este codigo fue generado por sare.m2t.generator 
   * http://code.google.com/p/sare/
   * 
   */

import javax.ejb.Local;

import com.prosofi.usme.model.TiempoDisponible;

import java.util.*;

@Local
public interface TiempoDisponibleSearch {

   public void find( );
   public void queryTiempoDisponible( );
   public List<TiempoDisponible> getTiempoDisponiblesC();
   public void setTiempoDisponiblesC(List <TiempoDisponible> tiempoDisponiblesC);
   public void destroy( );
}
