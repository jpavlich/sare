package com.prosofi.usme.action;

/**
   * Este codigo fue generado por sare.m2t.generator 
   * http://code.google.com/p/sare/
   * 
   */

import javax.ejb.Local;

import com.prosofi.usme.model.GastoMensuales;

import java.util.*;

@Local
public interface GastoMensualesInscription {

   public void cancel( );
   public void destroy( );
   public void confirm( );
   public void delete( );
   public void newGastoMensuales( );
   public void selectGastoMensuales(GastoMensuales selectedGastoMensuales );
   public void insertInFamilia(GastoMensuales selectedGastoMensuales);
   public List<GastoMensuales> getGastoMensualesInFamiliaByID();
   public void editGastoMensuales( );
}
