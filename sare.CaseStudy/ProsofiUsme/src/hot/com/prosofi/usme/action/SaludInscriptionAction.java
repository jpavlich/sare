package com.prosofi.usme.action;


/**
   * Este codigo fue generado por sare.m2t.generator 
   * http://code.google.com/p/sare/
   * 
   */

import java.util.List;
import java.util.ArrayList;
import javax.ejb.*;
import javax.persistence.*;
import org.jboss.seam.annotations.*;
import org.jboss.seam.annotations.datamodel.DataModel;
import org.jboss.seam.annotations.datamodel.DataModelSelection;
import org.jboss.seam.annotations.faces.*;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.annotations.security.Restrict;

import com.prosofi.usme.model.Persona;
import com.prosofi.usme.model.Salud;


@Stateful
@Name ("SaludInscription")
public class SaludInscriptionAction implements SaludInscription {

    @PersistenceContext
    private EntityManager em;

    @In
    private FacesMessages facesMessages;
    

    @In(required=false)
    @Out(required=false)
    private Persona persona;

    @In(required=false)
    @Out(required=false)
    private Salud salud;
    
    @Restrict("#{s:hasRole('Inscription_cancel','Search_cancel')}") 
    @End
    public void cancel( ){

    }
    @Restrict("#{s:hasRole('Inscription_destroy','Search_destroy')}") 
    @Remove
    public void destroy( ){

    }
    @Restrict("#{s:hasRole('Inscription_confirm','Search_confirm')}") 
    @End
    public void confirm( ){
        em.persist(salud);
        facesMessages.add("salud added");
    }
    @Restrict("#{s:hasRole('Inscription_delete')}") 
    public void delete( ){
        salud=em.merge(salud);
        em.remove(salud);  
        facesMessages.add("salud deleted");
    }
    @Restrict("#{s:hasRole('Inscription_newSalud')}") 
    @Begin
    public void newSalud( ){
        salud=new Salud();
    }
    @Restrict("#{s:hasRole('Inscription_selectSalud','Search_selectSalud')}") 
    @Begin(join=true)
    public void selectSalud(Salud selectSalud ){
        salud=em.find(Salud.class, selectSalud.getId());
    }
    @Restrict("#{s:hasRole('Inscription_editSalud')}") 
    @End
    public void editSalud( ){
        if(persona!=null){
            salud.setPersona(persona);  
         }
        salud=em.merge(salud); 
        facesMessages.add("salud edited");
    }
    @Restrict("#{s:hasRole('Inscription_selectPersona','Search_selectPersona')}") 
    public void selectPersona(Persona selectedPersona){
        persona = em.merge(selectedPersona);
    }
}
