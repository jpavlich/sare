package com.prosofi.usme.action;

/**
   * Este codigo fue generado por sare.m2t.generator 
   * http://code.google.com/p/sare/
   * 
   */

import javax.ejb.Local;

import com.prosofi.usme.model.OrganizacionComunitaria;

import java.util.*;

@Local
public interface OrganizacionComunitariaSearch {

   public void find( );
   public void queryOrganizacionComunitaria( );
   public List<OrganizacionComunitaria> getOrganizacionComunitariasC();
   public void setOrganizacionComunitariasC(List <OrganizacionComunitaria> organizacionComunitariasC);
   public void destroy( );
}
