package com.prosofi.usme.action;


/**
   * Este codigo fue generado por sare.m2t.generator 
   * http://code.google.com/p/sare/
   * 
   */



import com.prosofi.usme.model.Vivienda;


import java.util.ArrayList;
import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Begin;
import org.jboss.seam.annotations.Factory;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.datamodel.DataModel;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.faces.FacesMessages;




@Stateful
@Name("ViviendaSearch")
@Scope(ScopeType.SESSION)
public class ViviendaSearchAction implements ViviendaSearch {

    @PersistenceContext
    private EntityManager em;

    @In
    private FacesMessages facesMessages;
    

    @DataModel
    private List<Vivienda> viviendasC=new ArrayList<Vivienda>();
    
    @Restrict("#{s:hasRole('Inscription_find')}") 
    public void find( ){
        queryVivienda();
    }
    @Restrict("#{s:hasRole('Inscription_queryVivienda')}") 
    public void queryVivienda( ){
        List<Vivienda> results = em.createQuery("select h from Vivienda h").getResultList();
        viviendasC = results;
    }
    @Restrict("#{s:hasRole('Inscription_getViviendasC')}") 
    public  List<Vivienda>  getViviendasC(){
        List<Vivienda> results = em.createQuery("select h from Vivienda h").getResultList();  
        viviendasC = results;
        return viviendasC;
    }
    @Restrict("#{s:hasRole('Inscription_setViviendasC')}") 
    public void setViviendasC(List <Vivienda> viviendasC){
        this.viviendasC = viviendasC;
    }
    @Restrict("#{s:hasRole('Inscription_destroy')}") 
    @Remove
    public void destroy( ){

    }
}
