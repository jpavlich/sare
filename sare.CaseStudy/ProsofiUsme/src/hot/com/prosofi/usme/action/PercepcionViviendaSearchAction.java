package com.prosofi.usme.action;


/**
   * Este codigo fue generado por sare.m2t.generator 
   * http://code.google.com/p/sare/
   * 
   */

import java.util.List;
import java.util.ArrayList;
import javax.ejb.*;
import javax.persistence.*;
import org.jboss.seam.annotations.*;
import org.jboss.seam.annotations.datamodel.DataModel;
import org.jboss.seam.annotations.datamodel.DataModelSelection;
import org.jboss.seam.annotations.faces.*;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.annotations.security.Restrict;

import com.prosofi.usme.model.PercepcionVivienda;


@Stateful
@Name ("PercepcionViviendaSearch")
public class PercepcionViviendaSearchAction implements PercepcionViviendaSearch {

    @PersistenceContext
    private EntityManager em;

    @In
    private FacesMessages facesMessages;
    

    @DataModel
    private List<PercepcionVivienda> percepcionViviendasC=new ArrayList<PercepcionVivienda>();
    
    @Restrict("#{s:hasRole('Inscription_find','Search_find')}") 
    public void find( ){
        queryPercepcionVivienda();
    }
    @Restrict("#{s:hasRole('Inscription_queryPercepcionVivienda','Search_queryPercepcionVivienda')}") 
    public void queryPercepcionVivienda( ){
        List<PercepcionVivienda> results = em.createQuery("select h from PercepcionVivienda h").getResultList();
        percepcionViviendasC = results;
    }
    @Restrict("#{s:hasRole('Inscription_getPercepcionViviendasC','Search_getPercepcionViviendasC')}") 
    public  List<PercepcionVivienda>  getPercepcionViviendasC(){
        List<PercepcionVivienda> results = em.createQuery("select h from PercepcionVivienda h").getResultList();  
        percepcionViviendasC = results;
        return percepcionViviendasC;
    }
    @Restrict("#{s:hasRole('Inscription_setPercepcionViviendasC','Search_setPercepcionViviendasC')}") 
    public void setPercepcionViviendasC(List <PercepcionVivienda> percepcionViviendasC){
        this.percepcionViviendasC = percepcionViviendasC;
    }
    @Restrict("#{s:hasRole('Inscription_destroy','Search_destroy')}") 
    @Remove
    public void destroy( ){

    }
}
