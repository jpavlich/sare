package com.prosofi.usme.action;

/**
   * Este codigo fue generado por sare.m2t.generator 
   * http://code.google.com/p/sare/
   * 
   */

import javax.ejb.Local;

import com.prosofi.usme.model.Enfermedad;

import java.util.*;

@Local
public interface EnfermedadSearch {

   public void find( );
   public void queryEnfermedad( );
   public List<Enfermedad> getEnfermedadsC();
   public void setEnfermedadsC(List <Enfermedad> enfermedadsC);
   public void destroy( );
}
