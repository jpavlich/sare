package com.prosofi.usme.action;


/**
   * Este codigo fue generado por sare.m2t.generator 
   * http://code.google.com/p/sare/
   * 
   */

import java.util.List;
import java.util.ArrayList;
import javax.ejb.*;
import javax.persistence.*;
import org.jboss.seam.annotations.*;
import org.jboss.seam.annotations.datamodel.DataModel;
import org.jboss.seam.annotations.datamodel.DataModelSelection;
import org.jboss.seam.annotations.faces.*;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.annotations.security.Restrict;

import com.prosofi.usme.model.ControlPrenatal;
import com.prosofi.usme.model.Persona;


@Stateful
@Name ("ControlPrenatalInscription")
public class ControlPrenatalInscriptionAction implements ControlPrenatalInscription {

    @PersistenceContext
    private EntityManager em;

    @In
    private FacesMessages facesMessages;
    

    @In(required=false)
    @Out(required=false)
    private Persona persona;

    @In(required=false)
    @Out(required=false)
    private ControlPrenatal controlPrenatal;
    
    @Restrict("#{s:hasRole('Inscription_cancel','Search_cancel')}") 
    @End
    public void cancel( ){

    }
    @Restrict("#{s:hasRole('Inscription_destroy','Search_destroy')}") 
    @Remove
    public void destroy( ){

    }
    @Restrict("#{s:hasRole('Inscription_confirm','Search_confirm')}") 
    @End
    public void confirm( ){
        em.persist(controlPrenatal);
        facesMessages.add("controlPrenatal added");
    }
    @Restrict("#{s:hasRole('Inscription_delete')}") 
    public void delete( ){
        controlPrenatal=em.merge(controlPrenatal);
        em.remove(controlPrenatal);  
        facesMessages.add("controlPrenatal deleted");
    }
    @Restrict("#{s:hasRole('Inscription_newControlPrenatal')}") 
    @Begin
    public void newControlPrenatal( ){
        controlPrenatal=new ControlPrenatal();
    }
    @Restrict("#{s:hasRole('Inscription_selectControlPrenatal','Search_selectControlPrenatal')}") 
    @Begin(join=true)
    public void selectControlPrenatal(ControlPrenatal selectedControlPrenatal  ){
        controlPrenatal = em.find(ControlPrenatal.class, selectedControlPrenatal.getId());
    }
    @Restrict("#{s:hasRole('Inscription_editControlPrenatal')}") 
    @End
    public void editControlPrenatal( ){
        if(persona!=null){
            controlPrenatal.setPersona(persona);  
         }
        controlPrenatal=em.merge(controlPrenatal); 
        facesMessages.add("controlPrenatal edited");
    }
    @Restrict("#{s:hasRole('Inscription_selectPersona','Search_selectPersona')}") 
    public void selectPersona(Persona selectedPersona){
        persona = em.merge(selectedPersona);
    }
}
