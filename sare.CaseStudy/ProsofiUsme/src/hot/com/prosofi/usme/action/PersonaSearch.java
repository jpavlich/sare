package com.prosofi.usme.action;

/**
   * Este codigo fue generado por sare.m2t.generator 
   * http://code.google.com/p/sare/
   * 
   */

import javax.ejb.Local;

import com.prosofi.usme.model.Persona;

import java.util.*;

@Local
public interface PersonaSearch {

   public void find( );
   public void queryPersona( );
   public List<Persona> getPersonasC();
   public void setPersonasC(List <Persona> personasC);
   public void destroy( );
}
