package com.prosofi.usme.action;
/**
   * Este codigo fue generado por sare.m2t.generator 
   * http://code.google.com/p/sare/
   * 
   */

import javax.ejb.Local;

import com.prosofi.usme.model.GustoPorCapacitacion;
import com.prosofi.usme.model.Persona;

import java.util.*;

@Local
public interface GustoPorCapacitacionInscription {

   public void cancel( );
   public void destroy( );
   public void confirm( );
   public void delete( );
   public void newGustoPorCapacitacion( );
   public void selectGustoPorCapacitacion(GustoPorCapacitacion selectedGustoPorCapacitacion );
   public void insertInPersona(GustoPorCapacitacion selectedGustoPorCapacitacion);
   public List<GustoPorCapacitacion> getGustoPorCapacitacionInPersonaByID();
   public void editGustoPorCapacitacion( );
   public void selectPersona(Persona selectedPersona);
}
