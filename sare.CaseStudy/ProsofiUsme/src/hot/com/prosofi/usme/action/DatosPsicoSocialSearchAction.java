package com.prosofi.usme.action;


/**
   * Este codigo fue generado por sare.m2t.generator 
   * http://code.google.com/p/sare/
   * 
   */

import java.util.List;
import java.util.ArrayList;
import javax.ejb.*;
import javax.persistence.*;
import org.jboss.seam.annotations.*;
import org.jboss.seam.annotations.datamodel.DataModel;
import org.jboss.seam.annotations.datamodel.DataModelSelection;
import org.jboss.seam.annotations.faces.*;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.annotations.security.Restrict;

import com.prosofi.usme.model.DatosPsicoSocial;



@Stateful
@Name ("DatosPsicoSocialSearch")
public class DatosPsicoSocialSearchAction implements DatosPsicoSocialSearch {

    @PersistenceContext
    private EntityManager em;

    @In
    private FacesMessages facesMessages;
    

    @DataModel
    private List<DatosPsicoSocial> datosPsicoSocialsC=new ArrayList<DatosPsicoSocial>();
    
    @Restrict("#{s:hasRole('Inscription_find','Search_find')}") 
    public void find( ){
        queryDatosPsicoSocial();
    }
    @Restrict("#{s:hasRole('Inscription_queryDatosPsicoSocial','Search_queryDatosPsicoSocial')}") 
    public void queryDatosPsicoSocial( ){
        List<DatosPsicoSocial> results = em.createQuery("select h from DatosPsicoSocial h").getResultList();
        datosPsicoSocialsC = results;
    }
    @Restrict("#{s:hasRole('Inscription_getDatosPsicoSocialsC','Search_getDatosPsicoSocialsC')}") 
    public  List<DatosPsicoSocial>  getDatosPsicoSocialsC(){
        List<DatosPsicoSocial> results = em.createQuery("select h from DatosPsicoSocial h").getResultList();  
        datosPsicoSocialsC = results;
        return datosPsicoSocialsC;
    }
    @Restrict("#{s:hasRole('Inscription_setDatosPsicoSocialsC','Search_setDatosPsicoSocialsC')}") 
    public void setDatosPsicoSocialsC(List <DatosPsicoSocial> datosPsicoSocialsC){
        this.datosPsicoSocialsC = datosPsicoSocialsC;
    }
    @Restrict("#{s:hasRole('Inscription_destroy','Search_destroy')}") 
    @Remove
    public void destroy( ){

    }
}
