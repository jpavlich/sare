package com.prosofi.usme.action;

/**
   * Este codigo fue generado por sare.m2t.generator 
   * http://code.google.com/p/sare/
   * 
   */

import javax.ejb.Local;

import com.prosofi.usme.model.Salud;

import java.util.*;

@Local
public interface SaludSearch {

   public void find( );
   public void querySalud( );
   public List<Salud> getSaludsC();
   public void setSaludsC(List <Salud> saludsC);
   public void destroy( );
}
