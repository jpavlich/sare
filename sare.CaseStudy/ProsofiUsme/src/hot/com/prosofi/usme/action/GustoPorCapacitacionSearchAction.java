package com.prosofi.usme.action;


/**
   * Este codigo fue generado por sare.m2t.generator 
   * http://code.google.com/p/sare/
   * 
   */

import java.util.List;
import java.util.ArrayList;
import javax.ejb.*;
import javax.persistence.*;
import org.jboss.seam.annotations.*;
import org.jboss.seam.annotations.datamodel.DataModel;
import org.jboss.seam.annotations.datamodel.DataModelSelection;
import org.jboss.seam.annotations.faces.*;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.annotations.security.Restrict;

import com.prosofi.usme.model.GustoPorCapacitacion;


@Stateful
@Name ("GustoPorCapacitacionSearch")
public class GustoPorCapacitacionSearchAction implements GustoPorCapacitacionSearch {

    @PersistenceContext
    private EntityManager em;

    @In
    private FacesMessages facesMessages;
    

    @DataModel
    private List<GustoPorCapacitacion> gustoPorCapacitacionsC=new ArrayList<GustoPorCapacitacion>();
    
    @Restrict("#{s:hasRole('Inscription_find','Search_find')}") 
    public void find( ){
        queryGustoPorCapacitacion();
    }
    @Restrict("#{s:hasRole('Inscription_queryGustoPorCapacitacion','Search_queryGustoPorCapacitacion')}") 
    public void queryGustoPorCapacitacion( ){
        List<GustoPorCapacitacion> results = em.createQuery("select h from GustoPorCapacitacion h").getResultList();
        gustoPorCapacitacionsC = results;
    }
    @Restrict("#{s:hasRole('Inscription_getGustoPorCapacitacionsC','Search_getGustoPorCapacitacionsC')}") 
    public  List<GustoPorCapacitacion>  getGustoPorCapacitacionsC(){
        List<GustoPorCapacitacion> results = em.createQuery("select h from GustoPorCapacitacion h").getResultList();  
        gustoPorCapacitacionsC = results;
        return gustoPorCapacitacionsC;
    }
    @Restrict("#{s:hasRole('Inscription_setGustoPorCapacitacionsC','Search_setGustoPorCapacitacionsC')}") 
    public void setGustoPorCapacitacionsC(List <GustoPorCapacitacion> gustoPorCapacitacionsC){
        this.gustoPorCapacitacionsC = gustoPorCapacitacionsC;
    }
    @Restrict("#{s:hasRole('Inscription_destroy','Search_destroy')}") 
    @Remove
    public void destroy( ){

    }
}
