package com.prosofi.usme.action;

/**
   * Este codigo fue generado por sare.m2t.generator 
   * http://code.google.com/p/sare/
   * 
   */

import javax.ejb.Local;

import com.prosofi.usme.model.GustoPorCapacitacion;

import java.util.*;

@Local
public interface GustoPorCapacitacionSearch {

   public void find( );
   public void queryGustoPorCapacitacion( );
   public List<GustoPorCapacitacion> getGustoPorCapacitacionsC();
   public void setGustoPorCapacitacionsC(List <GustoPorCapacitacion> gustoPorCapacitacionsC);
   public void destroy( );
}
