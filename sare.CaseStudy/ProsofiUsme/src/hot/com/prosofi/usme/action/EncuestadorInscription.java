package com.prosofi.usme.action;

/**
   * Este codigo fue generado por sare.m2t.generator 
   * http://code.google.com/p/sare/
   * 
   */

import javax.ejb.Local;

import com.prosofi.usme.model.Encuestador;
import com.prosofi.usme.model.Familia;

import java.util.*;

@Local
public interface EncuestadorInscription {

   public void cancel( );
   public void destroy( );
   public void confirm( );
   public void delete( );
   public void newEncuestador( );
   public void selectEncuestador(Encuestador selectedEncuestador );
   public void selectFamilia(Familia selectedFamilia);
   public void editEncuestador( );
}
