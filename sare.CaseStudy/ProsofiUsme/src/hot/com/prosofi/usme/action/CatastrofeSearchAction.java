package com.prosofi.usme.action;


/**
   * Este codigo fue generado por sare.m2t.generator 
   * http://code.google.com/p/sare/
   * 
   */

import java.util.List;
import java.util.ArrayList;
import javax.ejb.*;
import javax.persistence.*;
import org.jboss.seam.annotations.*;
import org.jboss.seam.annotations.datamodel.DataModel;
import org.jboss.seam.annotations.datamodel.DataModelSelection;
import org.jboss.seam.annotations.faces.*;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.annotations.security.Restrict;

import com.prosofi.usme.model.Catastrofe;


@Stateful
@Name ("CatastrofeSearch")
public class CatastrofeSearchAction implements CatastrofeSearch {

    @PersistenceContext
    private EntityManager em;

    @In
    private FacesMessages facesMessages;
    

    @DataModel
    private List<Catastrofe> catastrofesC=new ArrayList<Catastrofe>();
    
    @Restrict("#{s:hasRole('Inscription_find','Search_find')}") 
    public void find( ){
        queryCatastrofe();
    }
    @Restrict("#{s:hasRole('Inscription_queryCatastrofe','Search_queryCatastrofe')}") 
    public void queryCatastrofe( ){
        List<Catastrofe> results = em.createQuery("select h from Catastrofe h").getResultList();
        catastrofesC = results;
    }
    @Restrict("#{s:hasRole('Inscription_getCatastrofesC','Search_getCatastrofesC')}") 
    public  List<Catastrofe>  getCatastrofesC(){
        List<Catastrofe> results = em.createQuery("select h from Catastrofe h").getResultList();  
        catastrofesC = results;
        return catastrofesC;
    }
    @Restrict("#{s:hasRole('Inscription_setCatastrofesC','Search_setCatastrofesC')}") 
    public void setCatastrofesC(List <Catastrofe> catastrofesC){
        this.catastrofesC = catastrofesC;
    }
    @Restrict("#{s:hasRole('Inscription_destroy','Search_destroy')}") 
    @Remove
    public void destroy( ){

    }
}
