package com.prosofi.usme.action;


/**
   * Este codigo fue generado por sare.m2t.generator 
   * http://code.google.com/p/sare/
   * 
   */

import java.util.List;
import java.util.ArrayList;
import javax.ejb.*;
import javax.persistence.*;
import org.jboss.seam.annotations.*;
import org.jboss.seam.annotations.datamodel.DataModel;
import org.jboss.seam.annotations.datamodel.DataModelSelection;
import org.jboss.seam.annotations.faces.*;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.annotations.security.Restrict;

import com.prosofi.usme.model.Familia;
import com.prosofi.usme.model.GastoMensuales;


@Stateful
@Name ("GastoMensualesInscription")
public class GastoMensualesInscriptionAction implements GastoMensualesInscription {

    @PersistenceContext
    private EntityManager em;

    @In
    private FacesMessages facesMessages;
    

    @In(required=false)
    @Out(required=false)
    private GastoMensuales gastoMensuales;

    @DataModel
    private List<GastoMensuales> familiaByID=new ArrayList<GastoMensuales>();

    @In(required=false)
    @Out(required=false)
    private Familia familia;
    
    @Restrict("#{s:hasRole('Inscription_cancel','Search_cancel')}") 
    @End
    public void cancel( ){

    }
    @Restrict("#{s:hasRole('Inscription_destroy','Search_destroy')}") 
    @Remove
    public void destroy( ){

    }
    @Restrict("#{s:hasRole('Inscription_confirm','Search_confirm')}") 
    @End
    public void confirm( ){
        em.persist(gastoMensuales);
        facesMessages.add("gastoMensuales added");
    }
    @Restrict("#{s:hasRole('Inscription_delete')}") 
    public void delete( ){
        gastoMensuales=em.merge(gastoMensuales);
        em.remove(gastoMensuales);  
        facesMessages.add("gastoMensuales deleted");
    }
    @Restrict("#{s:hasRole('Inscription_newGastoMensuales')}") 
    @Begin
    public void newGastoMensuales( ){
        gastoMensuales=new GastoMensuales();
    }
    @Restrict("#{s:hasRole('Inscription_selectGastoMensuales','Search_selectGastoMensuales')}") 
    @Begin(join=true)
    public void selectGastoMensuales(GastoMensuales selectedGastoMensuales ){
        gastoMensuales=em.find(GastoMensuales.class, selectedGastoMensuales.getId());
    }
    @Restrict("#{s:hasRole('Inscription_insertInFamilia')}") 
    public void insertInFamilia(GastoMensuales selectedGastoMensuales){
        gastoMensuales.setFamilia(familia);
        //em.persist(gastoMensuales);
        gastoMensuales =em.merge(selectedGastoMensuales);
        facesMessages.add("gastoMensuales added");
    }
    @Restrict("#{s:hasRole('Inscription_getGastoMensualesInFamiliaByID','Search_getGastoMensualesInFamiliaByID')}") 
    public  List<GastoMensuales>  getGastoMensualesInFamiliaByID(){
        List<GastoMensuales> results = em.createQuery("select h from GastoMensuales h where h.familia.id = ?1").setParameter(1,familia.getId()).getResultList();  
        familiaByID = results;
        return familiaByID;
    }
    @Restrict("#{s:hasRole('Inscription_editGastoMensuales')}") 
    @End
    public void editGastoMensuales( ){
        gastoMensuales=em.merge(gastoMensuales); 
        facesMessages.add("gastoMensuales edited");
    }
}
